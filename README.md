# Ecobba

Ecobba is a platform built to support all kinds of savings and investment groups,
from SACCOS organizations to informal community societies like Chamas, ViCoBa, Stockvels and the likes, 
eCOBbA understands your needs.
