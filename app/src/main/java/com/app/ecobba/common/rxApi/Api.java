package com.app.ecobba.common.rxApi;

import com.app.ecobba.Fragments.Shop.CreateShopKwea;
import com.app.ecobba.Fragments.Shop.MyKweaProducts.PatchKweaItem;

import com.app.ecobba.Models.AddMember.AddGroupMemberResponse;
import com.app.ecobba.Models.AvailableLoans.AvailableLoansResponse;

import com.app.ecobba.Models.Currencies.currenciesResponse;
import com.app.ecobba.Models.GetResourceItem.GetResourceReponse;
import com.app.ecobba.Models.GroupDetails.GroupDetailsResponse;
import com.app.ecobba.Models.GroupLoans.getGroupLoansResponse;
import com.app.ecobba.Models.GroupLoansBorrowed.groupLoansBorrowedResponse;
import com.app.ecobba.Models.GroupMeetings.GroupMeetingsResponse;
import com.app.ecobba.Models.GroupMember.groupMmemberResponse;
import com.app.ecobba.Models.GroupMmembers.groupMembersResponse;
import com.app.ecobba.Models.GroupWallet.groupWalletResponse;
import com.app.ecobba.Models.Group_Settings.groupSettingsResponse;
import com.app.ecobba.Models.Group_Transaction.groupTransactionResponse;
import com.app.ecobba.Models.Groups.groupsResponse;
import com.app.ecobba.Models.Lending.lendingLoansResponse;
import com.app.ecobba.Models.LendingData.lendingDataResponse;
import com.app.ecobba.Models.LendingPackageDetails.packageLendingDetailsResponse;

import com.app.ecobba.Models.MeetingDetailsData.Test;
import com.app.ecobba.Models.MeetingsSettingsContibutions.contributionFeesSettingsResponse;
import com.app.ecobba.Models.MeetingsSettingsData.meetingsSettingsResponse;
import com.app.ecobba.Models.MeetingsSettingsFees.MeetingsSettingsFeesResponse;
import com.app.ecobba.Models.MeetingsSettingsFines.finesSettingsResponse;
import com.app.ecobba.Models.MeetingsSettingsLoans.LoansSettingsResponse;
import com.app.ecobba.Models.PaymentSources.paymentSourcesResponse;

import com.app.ecobba.Models.ProcessLoan.ProcessLoanResponse;
import com.app.ecobba.Models.RegisterData.GetRegistrationDataResponse;
import com.app.ecobba.Models.RegistrationData.RegistrationDataResponse;
import com.app.ecobba.Models.Resources.Articles.ArticlesResponse;
import com.app.ecobba.Models.Resources.News.newsResponse;
import com.app.ecobba.Models.Resources.Trainings.TrainingResponse;
import com.app.ecobba.Models.Roles.getRolesResponse;
import com.app.ecobba.Models.SetWalletPreference.setWalletPreferenceResponse;
import com.app.ecobba.Models.SingleShopItems.SingleShoItemsResponse;
import com.app.ecobba.Models.UpdateUserRole.UpdateUserRoleResponse;
import com.app.ecobba.Models.User;
import com.app.ecobba.Models.UserLoans.userLoansResponse;
import com.app.ecobba.Models.UserProfile.UserProfileResponse;
import com.app.ecobba.Models.UserSummary.UserSummaryResponse;
import com.app.ecobba.Models.UserWallet.walletResponse;
import com.app.ecobba.Models.adapay.adapaySourcesResponse;
import com.app.ecobba.Models.adapay.telcos.telcosResponse;
import com.app.ecobba.Models.payment.contributionResponse;
import com.app.ecobba.Models.shopsdashboard.shopsDataResponse;
import com.app.ecobba.recyclerview_models.AdaPay.Sources;
import com.app.ecobba.recyclerview_models.services.ServicesListResponse;
import com.google.gson.JsonObject;

import io.reactivex.Observable;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.PATCH;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;

public interface Api {

    @Multipart
    @POST("kwea-items")
    Call<ResponseBody> uploadKweaitem(
            @Part MultipartBody.Part[] item_images,
            @Part("item_name") RequestBody itemname,
            @Part("item_category") RequestBody category,
            @Part("item_price") RequestBody price,
            @Part("item_currency") RequestBody currency,
            @Part("item_description") RequestBody desc,
            @Part("kwea_id") RequestBody kweaid

    );

    @Multipart
    @POST("postChangeAvatar")
    Call<ResponseBody> postChangeAvatar(
            @Part MultipartBody.Part avatar

    );

    @Multipart
    @POST("addGroupMember/{path}")
    Call<AddGroupMemberResponse> addgroupmember(
            @Path("path") String group_id,
            @Part("name") String name,
            @Part("other_names") String lastname,
            @Part("email") String email,
            @Part("msisdn") String msisdn,
            @Part("occupation") String occupation,
            @Part("dob") String dob,
            @Part("income") String income,
            @Part("gender") String gender,
            @Part("role") int role
    );

    @FormUrlEncoded
    @POST("updateUserRole")
    Call<UpdateUserRoleResponse> updateUserRole(
            @Field("role") int role,
            @Field("user_id") int user_id,
            @Field("group_id") int group_id
    );

    @FormUrlEncoded
    @POST("ProcessLoan/{loan_id}")
    Call<ProcessLoanResponse> ProcessUserLoan(
            @Path("loan_id") int loan_id,
            @Field("action") String action,
            @Field("message") String message
    );


    @GET("getUserSummary")
    Observable<UserSummaryResponse> getUserSummary();

    @GET("getProfile")
    Call<UserProfileResponse> getUserProfile();

    @GET("getRoles")
    Call<getRolesResponse> getRoles();

    @GET("getRegisterData")
    Call<GetRegistrationDataResponse> getUserRegisterData();

    @GET("payment/adapay/sources")
    Call<paymentSourcesResponse> getUserPayPrefences();

    @GET("getUserLoans")
    Call<userLoansResponse> getUserLoans();

    @GET("group/contributions/{group_id}")
    Call<contributionResponse> getAvailableContributions(
            @Path("group_id") String group_id
    );

    @GET("getGroupTransactions/{group_id}")
    Call<groupTransactionResponse> getGroupTransactions(
            @Path("group_id") String group_id
    );
 @GET("getGroupSettings/{group_id}")
    Call<groupSettingsResponse> GetgroupSettings(
            @Path("group_id") String group_id
    );

    @GET("payment/adapay/sources/group/{group_id}")
    Call<groupWalletResponse> getGroupWalletPaymentSources(
            @Path("group_id") String group_id
    );

    @GET("getGroupLoansMobile/{group_id}")
    Call<getGroupLoansResponse> getGroupLoans(
            @Path("group_id") String group_id
    );

    @GET("getGroupLoansborrowed/{group_id}")
    Call<groupLoansBorrowedResponse> getGroupLoansborrowed(
            @Path("group_id") String group_id
    );

    @GET("group/contributions/{group_id}")
    Call<contributionResponse> getContributions(
            @Path("group_id") String group_id
    );

    @GET("configureSettings/{setting_id}/{group_id}")
    Call<contributionFeesSettingsResponse> getContributionsSettings(
            @Path("setting_id") String setting_id,
            @Path("group_id") String group_id
    );

    @GET("configureSettings/{setting_id}/{group_id}")
    Call<finesSettingsResponse> getFinesSettings(
            @Path("setting_id") String setting_id,
            @Path("group_id") String group_id
    );

    @GET("configureSettings/{setting_id}/{group_id}")
    Call<LoansSettingsResponse> getLoansSettings(
            @Path("setting_id") String setting_id,
            @Path("group_id") String group_id
    );

    @GET("configureSettings/{setting_id}/{group_id}")
    Call<meetingsSettingsResponse> getMeetingsSettings(
            @Path("setting_id") String setting_id,
            @Path("group_id") String group_id
    );

    @GET("configureSettings/{setting_id}/{group_id}")
    Call<MeetingsSettingsFeesResponse> getFeesSettings(
            @Path("setting_id") String setting_id,
            @Path("group_id") String group_id
    );

    @GET("getGroupMembers/{group_id}")
    Call<groupMembersResponse> getGroupMmebers(
            @Path("group_id") String group_id
    );

    @GET("viewmembers/{member_id}")
    Call<groupMmemberResponse> getGroupMeber(
            @Path("member_id") String member_id
    );

    @GET("getProfile")
    Observable<UserProfileResponse> userProfile();

    @GET("getRegisterData")
    Observable<RegistrationDataResponse> getRegistrationData();

    @GET("getUserGroupsSummary")
    Call<groupsResponse> getUserGroups();

    @GET("getLatestTransactions")
    Call<walletResponse> getUserWalletTransaction();

    @GET("services")
    Call<ServicesListResponse> getservices();

    @GET("getAvailablePackages")
    Call<AvailableLoansResponse> getLatestPackages();

    @GET("getUserLoanPackages")
    Call<lendingLoansResponse> getUserLendingPackages();


    @GET("getUserPackageDetailsByID/{id}")
    Call<packageLendingDetailsResponse> getPackageDetails(
            @Path("id") String id
    );

    @GET("kwea")
    Call<shopsDataResponse> fetchshops();

    @GET("currencies")
    Call<currenciesResponse> fetchCurrencies();

    @GET("getLendingData")
    Call<lendingDataResponse> getLendingData();

    @GET("payment/adapay/sources/types")
    Call<adapaySourcesResponse> fetchAdapaySources();

    @GET("payment/adapay/telcos")
    Call<telcosResponse> fetchAdapayTelcos();

    @GET("resources/news")
    Call<newsResponse> getNewsResources();

    @GET("resources/article")
    Call<ArticlesResponse> getArticleResources();

    @GET("resources/training")
    Call<TrainingResponse> getTrainingResources();

    @GET("resources/{type}/{id}")
    Call<GetResourceReponse> getResourceItem(
            @Path("type") String type,
            @Path("id") String id
    );

    @GET("kwea/{id}")
    Call<SingleShoItemsResponse> fetchsingleshop(
            @Path("id") String id
    );

    @GET("getGroupMeetings/{id}")
    Call<GroupMeetingsResponse> fetchGroupMeetings(
            @Path("id") String id
    );

    @GET("getSingleUserGroup/{id}")
    Call<GroupDetailsResponse> fetchGroupDetails(
            @Path("id") String id
    );

    @GET("getMeetingDetail/{id}")
    Call<Test> fetchmeetingdeatils(
            @Path("id") String id
    );

    @DELETE("kwea/{id}")
    Call<ResponseBody> deletesingleshop(
            @Path("id") int id
    );


    @PATCH("kwea-items/{id}")
    Call<GroupMeetingsResponse> editShopitem(
            @Path("id") String id,
            @Body PatchKweaItem patchKweaItem

    );

    @FormUrlEncoded
    @POST("payment/adapay/preference")
    Call<setWalletPreferenceResponse> postWalletPrefrence(

            @Field("group_id") String group_id,
            @Field("type") String type,
            @Field("telco") String telco,
            @Field("msisdn") String msisdn


    );

//    @FormUrlEncoded
    @POST("kwea")
    Call<ResponseBody> CreateShop(
            @Body CreateShopKwea createShopKwea

    );


}
