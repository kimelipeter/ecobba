package com.app.ecobba.common;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.provider.Settings;

import androidx.appcompat.app.AlertDialog;

import com.app.ecobba.Activities.MainActivity;
import com.app.ecobba.R;

public class CheckInternetMethod {

    public static final String DISPLAY_MESSAGE_ACTION =
            "com.app.ecobba.common";

    public static final String EXTRA_MESSAGE = "message";

    public static boolean isNetworkAvailable(Context ctx) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public static void showAlert(String message, Activity context) {


        final AlertDialog.Builder builder = new AlertDialog.Builder(context);

        builder.setTitle(R.string.network_unavailable);
        builder.setMessage(R.string.turn_on_net).setCancelable(false)
                .setPositiveButton(R.string.enable_network, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        context.startActivity(new Intent(Settings.ACTION_WIRELESS_SETTINGS));
                    }
                });

        try {
            builder.show();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
