package com.app.ecobba.common

const val INITIATOR_LOGIN = "initiator_login"
const val INITIATOR_RECOVER ="initiator_recover"
const val INITIATOR_UPDATE_USER = "initiator_update_user"
const val INITIATOR_GETERGISTER_DATA = "initiator_get_register_data"
const val INITIATOR_GETLENDING_DATA = "initiator_get_lending_data"
const val INITIATOR_GETPROFILE = "initiator_load_profile"
const val INITIATOR_GETUSERSUMMARY = "initiator_load_user_summary"
const val INITIATOR_GET_TRANSACTIONS = "initiator_load_transactions"
const val INITIATOR_GET_LATEST_TRANSACTIONS = "initiator_get_latest_transactions"
const val INITIATOR_GET_USER_LOANS = "initiator_load_user_loans"
const val INITIATOR_APPLY_GROUP_LOAN = "initiator_apply_group_loan"
const val INITIATOR_STORE_LOAN_SETTINGS = "initiator_store_loan_settings"
const val INITIATOR_APPLY_LOAN = "initiator_apply_available_loans"
const val INITIATOR_CONFIRM_APPLY_LOAN = "initiator_confirm_apply_available_loans"
const val INITIATOR_GET_USER_LOANS_PACKAGES = "initiator_get_userloans_packages"
const val INITIATOR_GET_GROUP_REGISTRATION_DATA = "initiator_get_group_registration_data"
const val INITIATOR_GET_USER_LOANS_PACKAGES_DETAILS = "initiator_get_userloans_packages_details"
const val INITIATOR_GETCREATE_LOAN_DATA = "initiator_get_loan_data"
const val INITIATOR_CREATE_GROUP = "initiator_create_group"
const val INITIATOR_CREATE_GROUP_MEMBER = "initiator_create_group_member"
const val INITIATOR_CREATE_LOAN_PACKAGE = "initiator_create_loan_package"
const val INITIATOR_GETERGISTER_DATA_REG = "initiator_get_register_data_sign_up"
const val INITIATOR_GET_USERS_GROUP = "initiator_get_users_groups"
const val INITIATOR_GET_GROUP_MEMBERS = "initiator_get_group_members"
const val INITIATOR_GET_ALL_GROUP_MEMBERS = "initiator_get_group_members_all"
const val INITIATOR_GET_GROUP_MEMBER_DETAILS = "initiator_get_group_members_details"
const val INITIATOR_GET_GROUP_TRANSACTIONS = "initiator_get_group_transactions"
const val INITIATOR_ALL_GET_GROUP_TRANSACTIONS = "initiator_get_all_group_transactions"
const val INITIATOR_GET_GROUP_DETAILS = "initiator_get_group_details"
const val INITIATOR_GET_GROUP_SETTINGS = "initiator_get_group_settings"
const val INITIATOR_GET_GROUP_MEETINGS_SETTINGS = "initiator_get_group_meeting_settings"
const val INITIATOR_PUSH_GROUP_SETTINGS = "initiator_push_group_settings"
const val INITIATOR_ENABLE_SETTING = "initiator_enable_setting"
const val INITIATOR_DISABLE_SETTING = "initiator_disable_setting"
const val INITIATOR_GET_GROUP_MEETINGS = "initiator_get_group_meetings"
const val INITIATOR_GET_GROUP_MEETINGS_DETAILS = "initiator_get_group_meetings_details"
const val INITIATOR_GET_GROUP_SETTINGS_DETAILS = "initiator_get_group_settings_details"
const val INITIATOR_GET_LEVEL_ONE_DATA = "initiator_get_level_one_data"
const val INITIATOR_GET_LEVEL_TWO_DATA = "initiator_get_level_two_data"
const val INITIATOR_GET_LEVEL_THREE_DATA = "initiator_get_level_three_data"
const val INITIATOR_GET_LEVEL_FOUR_DATA = "initiator_get_level_four_data"
const val INITIATOR_GET_GROUP_LOANS = "initiator_get_group_loans"
const val INITIATOR_GET_GROUP_LOANS_BORROWERS = "initiator_get_group_loans_borrowers"
const val INITIATOR_WITHDRAW_FROM_WALLET = "initiator_withdraw_from_wallet"
const val INITIATOR_DEPOSIT_TO_WALLET = "initiator_deposit_to_wallet"
const val INITIATOR_PUSH_COLLECTIONS = "initiator_push_collections"
const val INITIATOR_REPORTS = "initiator_reports"
const val INITIATOR_GET_USER_NOTIFICATIONS = "initiator_get_user_notifications"
const val INITIATOR_STORE_HISA_SETTING = "initiator store hisa setting"
const val INITIATOR_STORE_JAMII_SETTING = "initiator store jamii setting"
const val INITIATOR_STORE_FINE_SETTING = "initiator store fine setting"
const val INITIATOR_STORE_MEETING_SETTING = "initiator store meeting setting"
const val INITIATOR_PROCESS_LOAN = "initiator process loan"
const val INITIATOR_STORE_LOAN_SETTING = "initiator_store_loan_setting"
const val INITIATOR_GET_GROUP_MEMBER = "initiator_get_group_member"
const val INITIATOR_GET_USER_ROLES = "initiator_get_user_roles"
const val INITIATOR_POST_USER_ROLE = "initiator_post_user_role"
const val INITIATOR_GET_NETWORKS = "initiator_get_networks"

const val INITIATOR_KWEA ="initiator_create_kwea"
const val INITIATOR_FETCH_SHOPS="initiator_fetch_shops"
const val INITIATOR_ADD_SHOP="initiator_create_new_shop"
const val INITATOR_GET_ADMINSTRATIVE_REGIONS="initiator_get_gegions"
const val INITIATOR_CREATE_KWEA_SHOP_ITEM="initiator_create_kwea_shop_item"
const val INITIATOR_FETCH_KWEA_ITEMS="initiator_fetch_kwea_items"
const val INITIATOR_BUY_AIRTIME="initiator_buy_airtime"
const val INITIATOR_FETCH_SHOPS_ITEMS="initiator_fetch_shops_items"
const val INITIATOR_SEND_MESSAGE="initiator_send_message"
const val INITIATOR_RECEIVE_MESSAGE="initiator_receive_message"

//Generate reports
const val INITIATOR_USER_REPORTS="initiator_user_reports"
const val INITIATOR_GET_USER_REPORTS_DATA="initiator_get_user_reports_data"
const val INITIATOR_GET_USER_CURRENCY_DATA="initiator_get_user_currency_data"

/***********************************************************************************/

const val NETBROADCAST = "NETBROADCAST_INTENT"
const val INITIATOR_SCHEDULE_MEETING="initiator_schedule_meeting"
const val INITIATOR_ADA_PAY_SOURCES="initiator_ada_pay_sources"
const val INITIATOR_ADA_PAY_TELCOS="initiator_ada_pay_telcos"
const val INITIATOR_ADA_PAY_WITHDRAW="initiator_ada_pay_withdraw"
const val INITIATOR_ADA_PAY_DEPOSIT="initiator_ada_pay_deposit"
const val INITIATOR_ADA_PAY_ADAPAY_SOURCE_TYPES="initiator_ada_pay_source_types"
const val INITIATOR_GET_SERVICES="initiator_services"
const val INITIATOR_GET_SERVICES_CATEGORIES="initiator_get_services_categories"
const val INITIATOR_GET_RESOURCES_ARTICLES="initiator_get_resources_articles"
const val INITIATOR_GET_RESOURCES_NEWS="initiator_get_resources_news"
const val INITIATOR_GET_RESOURCES_TRAININGS="initiator_get_resources_training"
const val INITIATOR_CREATE_RESOURCE="initiator_create_resource"
const val INITIATOR_GET_RESOURCES_TYPES="initiator_get_resources_types"
const val INITIATOR_GET_RESOURCES_ATTACHMENT_TYPES="initiator_get_resources_attachment_types"
const val INITIATOR_GET_RESOURCES_CATEGORIES="initiator_get_resources_categories"
const val INITIATOR_MAKE_CONTRIBUTION="initiator_make_contribution"
const val INITIATOR_SET_PAYMENT_METHOD="initiator_set_payment_method"


