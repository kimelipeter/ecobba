package com.app.ecobba.common.rxApi

import com.app.ecobba.Fragments.chat.data.ChatMessagesResponse
import com.app.ecobba.Fragments.chat.data.SendMessage
import com.app.ecobba.Fragments.chat.data.SendMessageResponse
import com.app.ecobba.Models.Wallet.DepositRequest
import com.app.ecobba.Models.Wallet.DepositResponse
import com.app.ecobba.recyclerview_models.AdaPay.Sources
import com.app.ecobba.recyclerview_models.AdaPay.types.SourceTypes
import com.app.ecobba.recyclerview_models.CountryCurrencies
import com.app.ecobba.recyclerview_models.KweaRes.KweaShopResponse
import io.reactivex.Observable
import okhttp3.ResponseBody
import retrofit2.http.*

interface ApiService {

    @GET("chats")
    fun getChatMessages(): Observable<ChatMessagesResponse>

    @POST("chats")
    fun sendMessage(@Body request : SendMessage):Observable<SendMessageResponse>

    @GET("kwea-items")
    fun kweaItemSearch(@Query("search") search : String):Observable<KweaShopResponse>

    @GET("payment/adapay/sources")
    fun getPaymentSources() : Observable<Sources>

    @GET("currencies")
    fun getCurrencies() : Observable<CountryCurrencies>
    ///api/payment/adapay/sources/types

    @GET("payment/adapay/sources/types")
    fun getSourceTypes() : Observable<SourceTypes>

    @POST("payment/adapay/deposit")
    fun postDeposit(@Body request : DepositRequest) : Observable<DepositResponse>



}