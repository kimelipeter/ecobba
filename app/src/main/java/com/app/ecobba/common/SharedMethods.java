package com.app.ecobba.common;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.provider.CalendarContract;
import android.util.Base64;
import android.util.Log;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.app.ecobba.Models.GenericRequest;
import com.app.ecobba.Models.Message;
import com.app.ecobba.Models.MessageList;
import com.app.ecobba.Models.RequestsList;
import com.app.ecobba.R;
import com.google.android.gms.common.util.Hex;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import cn.pedant.SweetAlert.SweetAlertDialog;
import kotlin.Pair;

//import android.icu.util.Calendar;

public class SharedMethods extends AppCompatActivity {
    Context ctx;
    public static final String TAG = SharedMethods.class.getSimpleName();
    private static NetworkObjectListener listener;
    public static final String HTTP_GET = "GET";
    public static final String HTTP_POST = "POST";
    public static String global_error_message= "We couldn't process your Request at this time. Please try again later";

    public static  android.app.AlertDialog progressDialog;
    public static ProgressDialog pd = null;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ctx=this;

    }

    public static Handler UIHandler;

    static {
        UIHandler = new Handler(Looper.getMainLooper());
    }
    public static void runOnUI(Runnable runnable) {
        UIHandler.post(runnable);
    }

    public static boolean isConnected(Context context) {
        ConnectivityManager cm =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();

    }
    public static void getFromServer(final String url, final String token, final Map<String, String> params, final String initiator, final String method, final Context context, final NetworkObjectListener listener ){

        getFromServer(url,token,params,initiator,method,context,listener,true);
//        if(isConnected(context)){
//
//            new Thread() {
//                public void run() {
//                        HTTP_GET(url, token, params,initiator, context,listener);
//                }
//            }.start();
////            showProgressBarDialog(context);
//            animateIcon(context);
//
//        }else {
//            try {
//                Toast.makeText(context, context.getResources().getString(R.string.no_internet), Toast.LENGTH_LONG).show();
//            }catch (Exception e){
//                e.printStackTrace();
//            }
//        }
    }

    public static void postToServer(final String url, final String token, final String params, final String initiator, final String method, final Context context, final NetworkObjectListener listener ){
        if(isConnected(context)){

            new Thread() {
                public void run() {
                    String _params = params.replaceAll("[\\r\\n]+", " ");
                    Log.e("POST_PARAMS",_params);
                    HTTP_POST(url,token, _params ,initiator, context,listener);
                }
            }.start();
            showProgressBarDialog(context);
//            animateIcon(context);
        }else {
            try {
                Toast.makeText(context, context.getResources().getString(R.string.no_internet), Toast.LENGTH_LONG).show();
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    public static void getFromServer(final String url,
                                     final String token,
                                     final Map<String, String> params,
                                     final String initiator,
                                     final String method,
                                     final Context context,
                                     final NetworkObjectListener listener ,
                                     final Boolean cache){


        String raw_id = stringToHex(
                        url+
                        params.toString()+
                        initiator
                );
        String id = raw_id;
        Log.e("STORE_ID",id);
        if(isConnected(context)){

            new Thread() {
                public void run() {
                    HTTP_GET(url, token, params,initiator, context,listener,id);
                }
            }.start();
//            showProgressBarDialog(context);
//            animateIcon(context);

        }else {

            new Thread(){
                @Override
                public void run() {
                    String response = getDefaults(id,context);
                    if (response == null){
//                fireNetworkErrorListener(initiator,"Data not in cache","Data not in cache");
                        try {
                            ((Activity) context).runOnUiThread(()->{Toast.makeText(context, "Data not on device, Internet required", Toast.LENGTH_SHORT).show();});
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }else{
                        if (listener!=null){
                            listener.onDataReady(initiator,response);
                            try {
                                ((Activity) context).runOnUiThread(()->{Toast.makeText(context, "Fetched from cache", Toast.LENGTH_SHORT).show();});
                            }catch (Exception e){
                                e.printStackTrace();
                            }
                        }
                    }
                    //fetch response from storage
                }
            }.start();



        }


    }

    public static void postToServer(final String url,
                                    final String token,
                                    final String params,
                                    final String initiator,
                                    final String method,
                                    final Context context,
                                    final NetworkObjectListener listener,
                                    final Boolean cache,
                                    final String key){

        //generate identifier and cache request and response

        String raw_id = stringToHex(
                        url+
                        key+
                        initiator
        );



        String id = Hex.bytesToStringLowercase(raw_id.getBytes());


        Log.e("STORE_ID",id);

        if(isConnected(context)){

            new Thread() {
                public void run() {
                    String _params = params.replaceAll("[\\r\\n]+", " ");
                    Log.e("POST_PARAMS",_params);
                    HTTP_POST(url,token, _params ,initiator, context,listener);
                }
            }.start();
            showProgressBarDialog(context);
//            animateIcon(context);
        }else {

            new Thread(){
                @Override
                public void run() {



                    String raw_req = getDefaults("HTTP_REQUESTS",context);
                    RequestsList requests;

                    if (raw_req == null){
                        requests = new RequestsList(new ArrayList<>());
                    }else{
                        requests = new Gson().fromJson(raw_req,RequestsList.class);
                    }
                    String _params = params.replaceAll("[\\r\\n]+", " ");
                    boolean add = true;
                    for (Pair<String,GenericRequest> reqMap:requests.getRequests()) {
                        if (reqMap.getFirst().equals(id)){
                            Log.e("DUPLICATE_ERROR","DUPLICATE FOUND");

                            if (listener!=null){
                                String response = "{\"success\":false,\"message\":\"This request is pending internet\"}";
                                listener.onDataReady(initiator,response);
                            }

//                            fireNetworkErrorListener(initiator,"DUPLICATE ERROR","This request is pending internet");
//
                            add = false;
                            break;
                        }
                    }
                    if (add) {
                        requests.getRequests().add(new Pair<String, GenericRequest>(id, new GenericRequest(url, token, _params, initiator, method)));
                        Log.e("HTTP_REQUESTS","---> "+requests.getRequests().size());
                        storePrefs("HTTP_REQUESTS", new Gson().toJson(requests), context);
                        try {
                            ((Activity) context).runOnUiThread(()->{Toast.makeText(context,"Request pending internet",Toast.LENGTH_SHORT).show();});
                        }catch (Exception e){
                            e.printStackTrace();
                        }

                        if (listener!=null){
                            String response = "{\"success\":true,\"message\":\"Application Pending Internet Connectivity\"}";
                            listener.onDataReady(initiator,response);
                        }

                    }

                }
            }.start();
            //Store to pending task refuse duplicate tasks

        }

    }



    public static void HTTP_GET(String url,
                                final String token,
                                final Map<String, String> parameters_tosend,
                                final String initiator,
                                final Context context,
                                NetworkObjectListener _listener,
                                final String id){
        // Instantiate the RequestQueue.
        Log.e(initiator, "TOKEN IS " + token);
        RequestQueue queue = Volley.newRequestQueue(context);

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.e("RAW_RESPOSE",response);
                        response = response.replace("null","\"\"");
                        response = response.replace("\"\"\"\"","\"\"");
                        response = response.replace(",\"\"","");

                        if (listener != null)
                            listener.onDataReady(initiator, response);

                        final String _response = response;
                        new Thread(){
                            @Override
                            public void run() {
                                storePrefs(id,_response,context);
                            }
                        }.start();



//                            Toast.makeText(context,"Fetched Online",Toast.LENGTH_SHORT).show();

                        hideDialog(context);
//                        stopIconOk(context);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String json = null;

                NetworkResponse response = error.networkResponse;
                if(response != null && response.data != null){
                    switch(response.statusCode){

                        case HttpURLConnection.HTTP_GATEWAY_TIMEOUT:
                            json = new String(response.data);
                            json = trimMessage(json, "message");
                            Log.e(initiator, "**Timeout - 504**");
                            fireNetworkErrorListener(initiator, global_error_message, json);
                            break;// retry
                        case HttpURLConnection.HTTP_BAD_REQUEST:
                            json = new String(response.data);
                            json = trimMessage(json, "message");
                            fireNetworkErrorListener(initiator, global_error_message, json);
                            Log.e(initiator, "**400 NOT_OK**");
                            break;
                        case HttpURLConnection.HTTP_UNAUTHORIZED://
                            Log.e(initiator, "**401 NOT_OK**");
                            json = new String(response.data);
                            json = trimMessage(json, "message");
                            fireNetworkErrorListener(initiator, global_error_message, json);                            break;
                        case HttpURLConnection.HTTP_NOT_FOUND://
                            Log.e(initiator, "**404 NOT_OK**");
                            json = new String(response.data);
                            json = trimMessage(json, "message");
                            fireNetworkErrorListener(initiator, global_error_message, json);                            break;
                        case HttpURLConnection.HTTP_BAD_METHOD://
                            Log.e(initiator, "**405 NOT_OK**");
                            json = new String(response.data);
                            json = trimMessage(json, "message");
                            fireNetworkErrorListener(initiator, global_error_message, json);                            break;
                        case HttpURLConnection.HTTP_INTERNAL_ERROR:
                            Log.e(initiator, "**500 NOT_OK**");
                            json = new String(response.data);
                            json = trimMessage(json, "message");
//                            fireNetworkErrorListener(initiator, "**500 NOT_OK**", ""+json);
                            fireNetworkErrorListener(initiator, global_error_message, json);                            break;
                        case HttpURLConnection.HTTP_UNAVAILABLE:
                            Log.e(initiator, "**Server Unavailable - 503**");
                            json = new String(response.data);
                            json = trimMessage(json, "message");
                            fireNetworkErrorListener(initiator, global_error_message, json);
                            break;
                        default:
                            Log.e(initiator, "**unknown response code**");
                            json = new String(response.data);
                            json = trimMessage(json, "message");
                            fireNetworkErrorListener(initiator, global_error_message, json);                            break;

                    }
                }
                hideDialog(context);

            }
        }){

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
//                params.put("Content-Length",String.valueOf(parameters_tosend.toString().getBytes().length));
                params.put("Content-Type", "application/json;");
                params.put("Accept", "application/json;");
                params.put("Authorization", "Bearer " +token);
                return params;
            }

//            @Override
//            protected Map<String, String> getParams()
//            {
//                Map<String, String>  params = new HashMap<String, String>();
//                params.put("data", parameters_tosend);
//                return params;
//            }
        };
        DefaultRetryPolicy policy = new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);

        setCustomNetworkListener(_listener);
        // Add the request to the RequestQueue.
        queue.add(stringRequest);
    }

    public static void HTTP_POST(String url,
                                 final String token,
                                 final String parameters_tosend,
                                 final String initiator,
                                 final Context context,
                                 NetworkObjectListener _listener, final String cache){
        // Instantiate the RequestQueue.

        RequestQueue queue = Volley.newRequestQueue(context);

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        response = response.replace("null","\"\"");
                        response = response.replace("\"\"\"\"","\"\"");
                        response = response.replace(",\"\"","");
                        hideDialog(context);
                        if (listener != null)
                            listener.onDataReady(initiator, response);
                        hideDialog(context);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String json = null;

                NetworkResponse response = error.networkResponse;
                if(response != null && response.data != null){
                    switch(response.statusCode){
                        case HttpURLConnection.HTTP_GATEWAY_TIMEOUT:
                            json = new String(response.data);
                            json = trimMessage(json, "message");
                            Log.e(initiator, "**Timeout - 504**");
                            fireNetworkErrorListener(initiator, global_error_message, json);                            break;// retry
                        case HttpURLConnection.HTTP_BAD_REQUEST:
                            json = new String(response.data);
                            json = trimMessage(json, "message");
                            fireNetworkErrorListener(initiator, global_error_message, json);
                            Log.e(initiator, "**400 NOT_OK**" + json );
                            break;
                        case HttpURLConnection.HTTP_UNAUTHORIZED://
                            Log.e(initiator, "**401 NOT_OK**");
                            json = new String(response.data);
                            json = trimMessage(json, "message");
                            fireNetworkErrorListener(initiator, global_error_message, json);                            break;
                        case HttpURLConnection.HTTP_NOT_FOUND://
                            Log.e(initiator, "**404 NOT_OK**");
                            json = new String(response.data);
                            json = trimMessage(json, "message");
                            fireNetworkErrorListener(initiator, global_error_message, json);                            break;
                        case HttpURLConnection.HTTP_BAD_METHOD://
                            Log.e(initiator, "**405 NOT_OK**");
                            json = new String(response.data);
                            json = trimMessage(json, "message");
                            fireNetworkErrorListener(initiator, global_error_message, json);                            break;
                        case HttpURLConnection.HTTP_INTERNAL_ERROR:
                            Log.e(initiator, "**500 NOT_OK**");
                            json = new String(response.data);
                            json = trimMessage(json, "message");
//                            fireNetworkErrorListener(initiator, "**500 NOT_OK**", ""+json);
                            fireNetworkErrorListener(initiator, global_error_message, json);                            break;
                        case HttpURLConnection.HTTP_UNAVAILABLE:
                            Log.e(initiator, "**Server Unavailable - 503**");
                            json = new String(response.data);
                            json = trimMessage(json, "message");
                            fireNetworkErrorListener(initiator, global_error_message, json);
                            break;
                        default:
                            Log.e(initiator, "**unknown response code**");
                            json = new String(response.data);
                            json = trimMessage(json, "message");
                            fireNetworkErrorListener(initiator, global_error_message, json);                            break;

                    }
                }
//                stopIconFail(context);
            }
        }){
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json;");
                params.put("Accept", "application/json;");
                params.put("Authorization", "Bearer " +token);
                return params;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return parameters_tosend == null ? null : parameters_tosend.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", parameters_tosend, "utf-8");
                    return null;
                }
            }

        };

        DefaultRetryPolicy policy = new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);

        setCustomNetworkListener(_listener);
        // Add the request to the RequestQueue.
        queue.add(stringRequest);
    }


    public static void performPendingTast(){
        //do all pending tasks from storage
        // initiated by is network recover listener
    }

    public static void storeTask(){
        //Store task to storage
    }

    public static void getPendingTast(){
        //Get task from storage
    }

    public static void HTTP_GET(String url, final String token, final Map<String, String> parameters_tosend, final String initiator, final Context context, NetworkObjectListener _listener){
        // Instantiate the RequestQueue.
        Log.e(initiator, "TOKEN IS " + token);
        RequestQueue queue = Volley.newRequestQueue(context);

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("RAW_RESPOSE",response);
                        response = response.replace("null","\"\"");
                        response = response.replace("\"\"\"\"","\"\"");
                        response = response.replace(",\"\"","");
                        if (listener != null)
                            listener.onDataReady(initiator, response);

                        hideDialog(context);
//                        stopIconOk(context);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String json = null;

                NetworkResponse response = error.networkResponse;
                if(response != null && response.data != null){
                    switch(response.statusCode){

                        case HttpURLConnection.HTTP_GATEWAY_TIMEOUT:
                            json = new String(response.data);
                            json = trimMessage(json, "message");
                            Log.e(initiator, "**Timeout - 504**");
                            fireNetworkErrorListener(initiator, global_error_message, json);
                            break;// retry
                        case HttpURLConnection.HTTP_BAD_REQUEST:
                            json = new String(response.data);
                            json = trimMessage(json, "message");
                            fireNetworkErrorListener(initiator, global_error_message, json);
                            Log.e(initiator, "**400 NOT_OK**");
                            break;
                        case HttpURLConnection.HTTP_UNAUTHORIZED://
                            Log.e(initiator, "**401 NOT_OK**");
                            json = new String(response.data);
                            json = trimMessage(json, "message");
                            fireNetworkErrorListener(initiator, global_error_message, json);                            break;
                        case HttpURLConnection.HTTP_NOT_FOUND://
                            Log.e(initiator, "**404 NOT_OK**");
                            json = new String(response.data);
                            json = trimMessage(json, "message");
                            fireNetworkErrorListener(initiator, global_error_message, json);                            break;
                        case HttpURLConnection.HTTP_BAD_METHOD://
                            Log.e(initiator, "**405 NOT_OK**");
                            json = new String(response.data);
                            json = trimMessage(json, "message");
                            fireNetworkErrorListener(initiator, global_error_message, json);                            break;
                        case HttpURLConnection.HTTP_INTERNAL_ERROR:
                            Log.e(initiator, "**500 NOT_OK**");
                            json = new String(response.data);
                            json = trimMessage(json, "message");
//                            fireNetworkErrorListener(initiator, "**500 NOT_OK**", ""+json);
                            fireNetworkErrorListener(initiator, global_error_message, json);                            break;
                        case HttpURLConnection.HTTP_UNAVAILABLE:
                            Log.e(initiator, "**Server Unavailable - 503**");
                            json = new String(response.data);
                            json = trimMessage(json, "message");
                            fireNetworkErrorListener(initiator, global_error_message, json);
                            break;
                        default:
                            Log.e(initiator, "**unknown response code**");
                            json = new String(response.data);
                            json = trimMessage(json, "message");
                            fireNetworkErrorListener(initiator, global_error_message, json);                            break;

                    }
                }
                hideDialog(context);

            }
        }){

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json;");
                params.put("Accept", "application/json;");
                params.put("Authorization", "Bearer " +token);
                return params;
            }

//            @Override
//            protected Map<String, String> getParams()
//            {
//                Map<String, String>  params = new HashMap<String, String>();
//                params.put("data", parameters_tosend);
//                return params;
//            }
        };
        DefaultRetryPolicy policy = new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);

        setCustomNetworkListener(_listener);
        // Add the request to the RequestQueue.
        queue.add(stringRequest);
    }

    public static void HTTP_POST(String url, final String token, final String parameters_tosend, final String initiator, final Context context, NetworkObjectListener _listener){
        // Instantiate the RequestQueue.

        RequestQueue queue = Volley.newRequestQueue(context);

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        response = response.replace("null","\"\"");
                        response = response.replace("\"\"\"\"","\"\"");
                        response = response.replace(",\"\"","");
                        hideDialog(context);
                        if (listener != null)
                            listener.onDataReady(initiator, response);
                        hideDialog(context);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String json = null;

                NetworkResponse response = error.networkResponse;
                if(response != null && response.data != null){
                    switch(response.statusCode){
                        case HttpURLConnection.HTTP_GATEWAY_TIMEOUT:
                            json = new String(response.data);
                            json = trimMessage(json, "message");
                            Log.e(initiator, "**Timeout - 504**");
                            fireNetworkErrorListener(initiator, global_error_message, json);                            break;// retry
                        case HttpURLConnection.HTTP_BAD_REQUEST:
                            json = new String(response.data);
                            json = trimMessage(json, "message");
                            fireNetworkErrorListener(initiator, global_error_message, json);
                            Log.e(initiator, "**400 NOT_OK**" + json );
                            break;
                        case HttpURLConnection.HTTP_UNAUTHORIZED://
                            Log.e(initiator, "**401 NOT_OK**");
                            json = new String(response.data);
                            json = trimMessage(json, "message");
                            fireNetworkErrorListener(initiator, global_error_message, json);                            break;
                        case HttpURLConnection.HTTP_NOT_FOUND://
                            Log.e(initiator, "**404 NOT_OK**");
                            json = new String(response.data);
                            json = trimMessage(json, "message");
                            fireNetworkErrorListener(initiator, global_error_message, json);                            break;
                        case HttpURLConnection.HTTP_BAD_METHOD://
                            Log.e(initiator, "**405 NOT_OK**");
                            json = new String(response.data);
                            json = trimMessage(json, "message");
                            fireNetworkErrorListener(initiator, global_error_message, json);                            break;
                        case HttpURLConnection.HTTP_INTERNAL_ERROR:
                            Log.e(initiator, "**500 NOT_OK**");
                            json = new String(response.data);
                            json = trimMessage(json, "message");
//                            fireNetworkErrorListener(initiator, "**500 NOT_OK**", ""+json);
                            fireNetworkErrorListener(initiator, global_error_message, json);                            break;
                        case HttpURLConnection.HTTP_UNAVAILABLE:
                            Log.e(initiator, "**Server Unavailable - 503**");
                            json = new String(response.data);
                            json = trimMessage(json, "message");
                            fireNetworkErrorListener(initiator, global_error_message, json);
                            break;
                        default:
                            Log.e(initiator, "**unknown response code**");
                            json = new String(response.data);
                            json = trimMessage(json, "message");
                            fireNetworkErrorListener(initiator, global_error_message, json);                            break;

                    }
                }
//                stopIconFail(context);
            }
        }){
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json;");
                params.put("Accept", "application/json;");
                params.put("Authorization", "Bearer " +token);
                return params;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return parameters_tosend == null ? null : parameters_tosend.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", parameters_tosend, "utf-8");
                    return null;
                }
            }

        };

        DefaultRetryPolicy policy = new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);

        setCustomNetworkListener(_listener);
        // Add the request to the RequestQueue.
        queue.add(stringRequest);
    }



    public static String trimMessage(String json, String key){
        String trimmedString = null;

        try{
            JSONObject obj = new JSONObject(json);
            trimmedString = obj.getString(key);
        } catch(JSONException e){
            e.printStackTrace();
            return null;
        }

        return trimmedString;
    }

    //    public void cacheData(){
//        File httpCacheDirectory = new File(context.getCacheDir(), "httpCache");
//        Cache cache = new Cache(httpCacheDirectory, 10 * 1024 * 1024);
//
//        OkHttpClient httpClient = new OkHttpClient.Builder()
//                .cache(cache)
//                .addInterceptor(chain -> {
//                    try {
//                        return chain.proceed(chain.request());
//                    } catch (Exception e) {
//                        Request offlineRequest = chain.request().newBuilder()
//                                .header("Cache-Control", "public, only-if-cached," +
//                                        "max-stale=" + 60 * 60 * 24)
//                                .build();
//                        return chain.proceed(offlineRequest);
//                    }
//                })
//                .build();
//
//        Retrofit retrofit;
//        Retrofit.Builder builder = new Retrofit.Builder();
//        builder.baseUrl(ApisKt.BASE_URL);
//        builder.addConverterFactory(GsonConverterFactory.create());
//
//        retrofit = builder.build();
//    }showDialog
    public static void showProgressBarDialog(Context context) {
        try {
            pd = new ProgressDialog(context);
            pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            pd.setMessage("Please Wait...");
            pd.setIndeterminate(true);
            pd.setCancelable(true);
            pd.show();
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    //Somewhere that has access to a context
    public static void displayMessage(String toastString, Context context ){
        Toast.makeText(context, toastString, Toast.LENGTH_LONG).show();
        showSweetAlertDialogError(toastString,context);
    }
    public static void showSweetAlertDialogError(String message, Context context){
        message = parseMessage(message);
        SweetAlertDialog sweetAlertDialogView;
        sweetAlertDialogView = new SweetAlertDialog(context,SweetAlertDialog.ERROR_TYPE);
        sweetAlertDialogView.setTitleText(context.getResources().getString(R.string.error_msg_oops_title))
                .setContentText("" + message + " \n");
        sweetAlertDialogView.show();
        Button viewGroup = (Button) sweetAlertDialogView.findViewById(cn.pedant.SweetAlert.R.id.confirm_button);
        if (viewGroup != null) {
            Log.e(TAG, "showErrorMsg: Button view Found yep");
            viewGroup.setBackgroundColor(viewGroup.getResources().getColor(R.color.colorSecondary));
            viewGroup.getLayoutParams().height = ViewGroup.LayoutParams.WRAP_CONTENT;
        } else {
            Log.e(TAG, "showErrorMsg: Button view Null :( ");
        }
    }


    public static String perfectInternationalPhoneNumber(String phone_number) {

        phone_number = phone_number.replaceAll("[\\D]", "");
        phone_number = phone_number.contains("+") ? phone_number : "+" + phone_number;
        Log.d("app_perfect_phoneNo..", phone_number);
        return phone_number;

    }


    public static void hideSoftKeyboard(AppCompatActivity activity) {
        try {


            InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(AppCompatActivity.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
        } catch (NullPointerException e) {
            Log.e("hideSoftKeyboard", e.toString());
        }
    }

    public static void hideSoftKeyboard(FragmentActivity activity) {
        try {


            InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(FragmentActivity.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);

        } catch (Exception e) {
            Log.e("hideSoftKeyboard", e.toString());
        }
    }

    public  static void showSweetAlertDialogSuccess(String message, Context context){
        SweetAlertDialog sweetAlertDialogView;
        sweetAlertDialogView = new SweetAlertDialog(context,SweetAlertDialog.SUCCESS_TYPE);
        sweetAlertDialogView.setTitleText(context.getResources().getString(R.string.success_msg_title))
                .setContentText("" + message + " \n");
        sweetAlertDialogView.show();
        Button viewGroup =  sweetAlertDialogView.findViewById(cn.pedant.SweetAlert.R.id.confirm_button);
        if (viewGroup != null) {
            Log.e(TAG, "showErrorMsg: Button view Found yep");
            viewGroup.setBackgroundColor(viewGroup.getResources().getColor(R.color.colorSecondary));
            viewGroup.getLayoutParams().height = ViewGroup.LayoutParams.WRAP_CONTENT;
        } else {
            Log.e(TAG, "showErrorMsg: Button view Null :( ");
        }
    }
    public static void showAlertAndMoveToPage(String message, final Class nextPage, final Context context){
        SweetAlertDialog sweetAlertDialogView;
        sweetAlertDialogView = new SweetAlertDialog(context,SweetAlertDialog.SUCCESS_TYPE);
        sweetAlertDialogView.setTitleText(context.getResources().getString(R.string.success_msg_title))
                .setContentText("" + message + " \n")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        Intent intent = new Intent(context,nextPage);
                        context.startActivity(intent);
                        sDialog.dismissWithAnimation();
                    }
                })
                .setCanceledOnTouchOutside(false);
        sweetAlertDialogView.show();
        Button viewGroup =  sweetAlertDialogView.findViewById(cn.pedant.SweetAlert.R.id.confirm_button);
        if (viewGroup != null) {
            Log.e(TAG, "showErrorMsg: Button view Found yep");
            viewGroup.setBackgroundColor(viewGroup.getResources().getColor(R.color.colorSecondary));
            viewGroup.getLayoutParams().height = ViewGroup.LayoutParams.WRAP_CONTENT;
        } else {
            Log.e(TAG, "showErrorMsg: Button view Null :( ");
        }
    }
    public static void showWarningAlertAndMoveToPage(String message, final Class nextPage, final String params, final Context context){
        SweetAlertDialog sweetAlertDialogView;
        sweetAlertDialogView = new SweetAlertDialog(context,SweetAlertDialog.WARNING_TYPE);
        sweetAlertDialogView.setTitleText(context.getResources().getString(R.string.confirm_msg_title))
                .setContentText("" + message + " \n")
                .setConfirmText("Yes")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        Intent intent = new Intent(context,nextPage);
                        intent.putExtra("extra_data",params );
                        context.startActivity(intent);
                        sDialog.dismissWithAnimation();
                    }
                })
                .setCancelText("Cancel")
                .showCancelButton(true)
                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener(){

                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        sweetAlertDialog.cancel();
                    }
                });
        sweetAlertDialogView.show();

    }

    public static void hideDialog(Context context){
        try{
            pd.dismiss();
        } catch (Exception e){
            e.printStackTrace();
        }

    }


    public static void showWarningOKAlertAndMoveToPage(String message, final Class nextPage, final String params, final Context context){
        SweetAlertDialog sweetAlertDialogView;
        sweetAlertDialogView = new SweetAlertDialog(context,SweetAlertDialog.WARNING_TYPE);
        sweetAlertDialogView.setTitleText(context.getResources().getString(R.string.error_msg_oops_title))
                .setContentText("" + message + " \n")
                .setConfirmText("OK")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        Intent intent = new Intent(context,nextPage);
                        intent.putExtra("extra_data",params );
                        context.startActivity(intent);
                        sDialog.dismissWithAnimation();
                    }
                });
        sweetAlertDialogView.show();

    }

    public static void errorDialogNotifier(String Title, String message, Context context) {
        new SweetAlertDialog(context, SweetAlertDialog.ERROR_TYPE)
                .setTitleText(Title)
                .setContentText(message)
                .show();
    }

    public static SweetAlertDialog returnErrorDialogSweetDialog(String Title, String message, Context context) {
        return new SweetAlertDialog(context, SweetAlertDialog.ERROR_TYPE)
                .setTitleText(Title)
                .setContentText(message);
    }

    public static void warningDialogNotifier(String Title, String message, Context context) {
        new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE)
                .setTitleText(Title)
                .setContentText(message)
                .show();
    }

    // Step 1 - This interface defines the type of messages I want to communicate to my owner
    public interface NetworkObjectListener {
        // These methods are the different events and
        // need to pass relevant arguments related to the event triggered
        void onDataReady(String initiator, String response);

        // or when data has been loaded
        void onDataLoaded(String initiator, JSONArray data);

        //or when an error

        void onNetworkError(String initiator, String errorCode, String errorMessage);
    }
    private static void fireNetworkErrorListener(String initiator, String errorCode, String errorMessage) {
        if (listener != null)

            listener.onNetworkError(initiator, errorCode, errorMessage);
        else
            Log.d(initiator, "ListenerError");

    }
    // Assign the listener implementing events interface that will receive the events
    public static void setCustomNetworkListener(NetworkObjectListener listener_r) {
        listener = listener_r;
    }
    public static void storePrefs(String key, String value, Context context) {
        deletePrefs(key,context);
        try {
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
            SharedPreferences.Editor editor = prefs.edit();
            editor.putString(key, value);
            editor.apply();
            Log.e("storePref", key+"->"+value);
        } catch (Exception se) {
            Log.e("storePref", se.toString());
        }
    }
    public static void storeUser(String key, int value, Context context) {
        deletePrefs(key,context);
        try {
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
            SharedPreferences.Editor editor = prefs.edit();
            editor.putInt(key, value);
            editor.apply();
        } catch (Exception se) {
            Log.e("storePref", se.toString());
        }
    }

    public static int getRole(Context context){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getInt("role_int", 0);
    }

    public static int getUser(Context context){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getInt("user", 0);
    }
    public static boolean isPrefsAvailable(String key, Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);

        return (preferences.contains(key));

    }
    public static String getDefaults(String key, Context context) {

        if (context == null) return "";

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);

        return preferences.getString(key, null);
    }

    public static void deletePrefs(String key, Context context) {
        try {
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
            SharedPreferences.Editor editor = prefs.edit();
            editor.remove(key);
            Log.e("deletePrefs", key);
            editor.apply();
        } catch (Exception se) {
            Log.e("deletePrefs", se.toString());
        }
    }
    public static void clearAllPrefs(Context context){
        try{
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
            SharedPreferences.Editor editor = prefs.edit();
            editor.clear();
            editor.apply();
        }catch (Exception e){
            Log.e("deletePrefs", e.toString());
        }

    }

    public static String CheckIfEmptyOrNull(String value){
        assert value != null;
        if(value.equals("null") || value.isEmpty() ){
            value = "0";
        }
        return value;
    }

    public static String GroupDigits(int num){

        try{
            String number = String.valueOf(num);
            return  GroupDigits(number);
        }catch (Exception e){
            e.printStackTrace();
        }
        return "0";
    }

    public static String GroupDigits(String in_digit){

        String res = "";

        final int input_len = in_digit.length();
        for(int i=0 ; i< input_len ; i++)
        {

            if( (i % 3 == 0) && i > 0 )
                res = "," + res;

            res = in_digit.charAt(input_len - i - 1) + res;

        }

        return res;
    }

    public static String parseMessage(String list){
        String message = "";
        if (list!=null) {

            list = list.replace("null", "\"\"");
            message = list.replace("[", "");
            message = message.replace("]", "");
            message = message.replace("\"", "");
            message = message.replace(",", "\n");
        }

        return message;
    }

    static String stringToHex(String string) {
        String base64=string;
        try {

            byte[] data = string.getBytes("UTF-8");

            base64 = android.util.Base64.encodeToString(data, Base64.DEFAULT);
//            int min = base64.length()/2;
//            int max = base64.length();
//            base64 = base64.substring(min,max);

//            Log.i("Base 64 ", base64);

        } catch (UnsupportedEncodingException e) {

            e.printStackTrace();

        }

        return base64;
    }

    public static void storeSystemMessages(final Message message,Context ctx){

                String raw_messages = getDefaults("SYSMSG",ctx);
                MessageList messages;
                if (raw_messages!=null){
                    messages = new Gson().fromJson(raw_messages,MessageList.class);
                }else{
                    messages = new MessageList(new ArrayList<>());
                }

                messages.getMessages().add(message);
                storePrefs("SYSMSG",new Gson().toJson(messages),ctx);

    }


    public static void storeReminder(Context context, Calendar beginTime,Calendar endTime,String title,String description){

        if (Build.VERSION.SDK_INT >= 14) {
            Intent intent = new Intent(Intent.ACTION_INSERT)
                    .setData(CalendarContract.Events.CONTENT_URI)
                    .putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, beginTime.getTimeInMillis())
                    .putExtra(CalendarContract.EXTRA_EVENT_END_TIME, endTime.getTimeInMillis())
                    .putExtra(CalendarContract.Events.TITLE, title)
                    .putExtra(CalendarContract.Events.DESCRIPTION, description);
//                    .putExtra(CalendarContract.Events.EVENT_LOCATION, "The gym")
//                    .putExtra(CalendarContract.Events.AVAILABILITY, CalendarContract.Events.AVAILABILITY_BUSY)
//                    .putExtra(Intent.EXTRA_EMAIL, "rowan@example.com,trevor@example.com");
            context.startActivity(intent);
        }

    }

    public static Date stringToDateParser(String string) throws ParseException {

        //1. Create a Date from String
        SimpleDateFormat sdf = new SimpleDateFormat("dd-M-yyyy hh:mm:ss");
        String dateInString = "22-01-2015 10:20:56";
        Date date = sdf.parse(dateInString);
//        DateAndCalendar obj = new DateAndCalendar();

        //2. Test - Convert Date to Calendar
        Calendar calendar = dateToCalendar(date);
//        System.out.println(calendar.getTime());

        //3. Test - Convert Calendar to Date
        Date newDate = calendarToDate(calendar);
//        System.out.println(newDate);
        return newDate;
    }


    public static Calendar stringToCalenderParser(String string) throws ParseException {

        //1. Create a Date from String
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
//        String dateInString = "22-01-2015 10:20:56";
        Date date = sdf.parse(string);
//        DateAndCalendar obj = new DateAndCalendar();

        //2. Test - Convert Date to Calendar
        Calendar calendar = dateToCalendar(date);
        System.out.println(calendar.getTime());

        //3. Test - Convert Calendar to Date
//        Date newDate = calendarToDate(calendar);
//        System.out.println(newDate);
        return calendar;
    }

    //Convert Date to Calendar
    private static Calendar dateToCalendar(Date date) {

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return calendar;

    }

    //Convert Calendar to Date
    private static Date calendarToDate(Calendar calendar) {
        return calendar.getTime();
    }



}

