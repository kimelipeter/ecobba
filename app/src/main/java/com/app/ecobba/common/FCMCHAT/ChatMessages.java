package com.app.ecobba.common.FCMCHAT;

import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.ecobba.R;
import com.app.ecobba.Utils.BaseFragment;
import com.app.ecobba.common.SharedMethods;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.snov.timeagolibrary.PrettyTimeAgo;

import java.text.ParseException;
import java.util.ArrayList;

import kotlin.Unit;

public class ChatMessages extends Fragment {
    private static final String TAG = "ChatMessages";
    GenericFirebaseAdapter<Message> adapter;
    ImageView topimage;
    Chat chat;
    TextView send;
    TextView username;
    RecyclerView recyclerView;
    private ChatViewModel chatViewModel;
    int userId;
    EditText message;
    ImageView profilepicture;
    ArrayList<String> images = new ArrayList<>();
    ArrayList<Integer> readby = new ArrayList<>();
    String chatId;
    ImageButton sendmessage;
    ImageView btnBack;

    public ChatMessages() {
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_chat_messages, container, false);
        recyclerView = view.findViewById(R.id.rvChat);

        try {
            chat = new Gson().fromJson(getArguments().getString("CHATM"), Chat.class);
            Log.e("DATA_RECEIVED", String.valueOf(chat));
            userId = SharedMethods.getUser(requireContext());
        } catch (Exception e) {
            e.getLocalizedMessage();
        }

        chatViewModel = ViewModelProviders.of(this).get(ChatViewModel.class);
        sendmessage = view.findViewById(R.id.btnSend);
        message = view.findViewById(R.id.tiMessage);


        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager lm = new LinearLayoutManager(requireContext());
        lm.setStackFromEnd(true);
        recyclerView.setLayoutManager(lm);
        requireActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);

        adapter = new GenericFirebaseAdapter<Message>(
                Message.class,
                chatViewModel.chatMessages(chat.getId()),
                (viewGroup, itemViewType) -> {
                    switch (itemViewType) {
                        case 0:
                            return new MessagesViewHolder(viewGroup);
                        case 1:
                            return new ReceivedMessageViewHolder(viewGroup);
                        default:
                            return null;
                    }
                },
                (viewholder, position, type, message) -> {
                    switch (type) {
                        case 0:
                            TextView messagein;
                            TextView timein;
                            TextView username;
                            ImageView person;

                            messagein = viewholder.itemView.findViewById(R.id.chat_item_rcv);
                            messagein.setText(message.getMessage());


                            break;
                        case 1:
                            TextView messageout, time;
                            ImageView blues;
                            messageout = viewholder.itemView.findViewById(R.id.chat_item_sent);

                            messageout.setText(message.getMessage());

                            break;
                        default:
                            return null;
                    }

                    viewholder.itemView.setTag(message);
                    //logic to display message
                    String messages = message.getMessage();


                    return Unit.INSTANCE;
                },
                (position, message) -> {
                    return message.getCreatedBy() == SharedMethods.getUser(requireContext()) ? 1 : 0;  /** logged in user id**/
                }, (viewHolder, visible) -> {
            if (visible) {
                Message readMessage = (Message) viewHolder.itemView.getTag();
                chatViewModel.readMessage(userId, chat.getId(), readMessage);
            }
            return Unit.INSTANCE;
        }
        );
        recyclerView.setAdapter(adapter);
        sendmessage.setOnClickListener(v -> {
            if (!TextUtils.isEmpty(message.getText().toString())) {
                String messagesent = message.getText().toString();
                chatId = chat.getId();

                readby.add(SharedMethods.getUser(requireContext()));
                int userId = SharedMethods.getUser(requireContext());
                String messageId = String.valueOf(System.currentTimeMillis());
                chatViewModel.sendMessage(chatId, new Message(
                        String.valueOf(System.currentTimeMillis()), messagesent, images, userId, readby, false
                ));
                message.getText().clear();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        recyclerView.scrollToPosition(adapter.getItemCount() - 1);
                    }
                }, 100);
            } else {
                Toast.makeText(requireContext(), "Enter a message", Toast.LENGTH_LONG).show();
            }

            //scrolltoposition();
        });

    }

    private class MessagesViewHolder extends RecyclerView.ViewHolder {
        TextView messageIn;

        public MessagesViewHolder(ViewGroup viewGroup) {
            super(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.chat_item_rcv, viewGroup, false));
            messageIn = itemView.findViewById(R.id.chat_item_rcv);
        }
    }

    private class ReceivedMessageViewHolder extends RecyclerView.ViewHolder {
        TextView messageout;

        public ReceivedMessageViewHolder(ViewGroup viewGroup) {
            super(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.chat_item_sent, viewGroup, false));
            messageout = itemView.findViewById(R.id.chat_item_sent);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        adapter.startListening();
    }
}
