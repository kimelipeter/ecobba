package com.app.ecobba.common.rxApi

import com.app.ecobba.Application.MainApplication
import com.app.ecobba.common.BASE_URL
import com.app.ecobba.common.SharedMethods
import com.google.gson.GsonBuilder
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory


class RxApi {

    // params.put("Authorization", "Bearer " +token);
    private val token = SharedMethods.getDefaults("token", MainApplication.context)
    var interceptors = HttpLoggingInterceptor()
   private var gson = GsonBuilder()
            .setLenient()
            .create()
    private var logger : HttpLoggingInterceptor = HttpLoggingInterceptor().apply { level = HttpLoggingInterceptor.Level.BODY }
    private var interceptor : Interceptor = Interceptor {
        if (token.isNullOrEmpty()){
            it.proceed(it.request())
        }else {
            it.proceed(it.request().newBuilder().addHeader("Authorization", "Bearer $token").build())
        }
    }

    private var client : OkHttpClient = OkHttpClient.Builder()
            .addInterceptor(interceptor)
            .addInterceptor(logger)
            .addInterceptor(interceptors)
            .build()


    val retrofitInstance : Retrofit = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .client(client)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()

    open var apiService : ApiService = retrofitInstance.create(ApiService::class.java)
    var api :Api = retrofitInstance.create(Api::class.java)

}