package com.app.ecobba.common.FCMCHAT

import android.os.Handler
import android.util.Log
import android.view.ViewGroup
import androidx.lifecycle.ViewModel
import androidx.recyclerview.widget.RecyclerView
import com.firebase.ui.firestore.FirestoreRecyclerAdapter
import com.firebase.ui.firestore.FirestoreRecyclerOptions
import com.google.android.gms.tasks.Task
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query
import com.google.firebase.firestore.WriteBatch

class ChatViewModel : ViewModel() {

    companion object {
        val db = FirebaseFirestore.getInstance()
        val batchWrite: WriteBatch get() = db.batch()
    }

    /**
     * Setup the chat collection
     * Structure
     */

    /**
     * References
     */
    val chatListRef = db.collection("chat")
    fun singleChatRef(chatId: String) = chatListRef.document(chatId)

    fun chatParticipantsRef(chatId: String) = singleChatRef(chatId).collection("participants")
    fun singleChatParticipant(chatId: String, participantId: String) =
            chatParticipantsRef(chatId).document(participantId)


    fun chatMessages(chatId: String) = singleChatRef(chatId).collection("messages")
    fun singleChatMessage(chatId: String, messageId: String) =
            chatMessages(chatId).document(messageId)


    fun sendMessage(chatId: String, message: Message) =
            singleChatMessage(chatId, message.id).set(message).listener(success =
            {
                Log.e(this.javaClass.name, "Message sent ")


                updateChatUpdateTime(chatId)

            }, error = {
                Log.e(this.javaClass.name, "error creating message", it)
            })

    fun getUserChats(userId: Int) = chatListRef.whereArrayContains("participantIds", userId)
            .orderBy("updatedAt", Query.Direction.DESCENDING)

    fun createChat(
            chId: String?="",
            participants: List<Participants>,
            chatName: String? = "",
            chtImage:String? ="",

            callback: ((Chat?) -> Unit)? = null
    ) {
        if (participants.size > 2 && chatName.isNullOrEmpty()) {
            throw IllegalArgumentException("Group name required")

        } else {


            getChatContaining(chId!!) {
                if (!(it.size > 0 && it.any { it.participantIds.size == participants.size })) {
                    val newChat = Chat().apply {
                        participantIds = ArrayList(participants.map { it.id }.toHashSet())
                        name = chatName!!
                        chatImage = chtImage!!
                        id = chId!!


                    }
                    db.runTransaction {
                        it.set(singleChatRef(newChat.id), newChat)
                        participants.forEach { singleParticipant ->
                            it.set(
                                    singleChatParticipant(newChat.id, singleParticipant.id.toString()),
                                    singleParticipant
                            )
                        }
                    }.listener(success = {
                        Log.e(this.javaClass.name, "Chat Created")
                        callback?.invoke(newChat)
                    }, error = {
                        Log.e(this.javaClass.name, "error creating chat", it)
                        callback?.invoke(null)
                    })
                } else {
                    Log.e(this.javaClass.name, "chat already exists")
                    callback?.invoke(it.first { it.id == chId })
                }
            }

        }
    }

    fun readMessage(userId: Int, chatId: String, message: Message) {
        if (!(message.createdBy == userId || message.readBy.contains(userId))) {
            db.runTransaction {
                val messageFromDb =
                        it.get(singleChatMessage(chatId, message.id)).toObject(Message::class.java)
                messageFromDb?.let { m ->
                    m.readBy.add(userId)
                    m.read = true
                    it.set(singleChatMessage(chatId, message.id), m)
                }
            }
        }
    }

    fun getChatDetails(userId: Int, chat: Chat, callback: (String, String) -> Unit) {
        if (chat.name.isNotBlank())
            callback.invoke(chat.name, chat.chatImage)
        else
            chatParticipantsRef(chat.id).get().listener(success = { it ->
                val participants = it.toObjects(Participants::class.java)
                var chatName = ""
                var chatImage = ""
                if (participants.isNotEmpty()) {
                    with(participants.first { it.id != userId }) {
                        chatName = displayName
                        chatImage = displayImage
                    }
                }
                callback.invoke(chatName, chatImage)
            }, error = {
                callback.invoke("Error", "")
            })
    }


    fun getUnreadOrLastMessage(userId: Int, chatId: String, callback: (List<Message>) -> Unit) {
        chatMessages(chatId)
                .whereNotIn("readBy", listOf(userId))
                .let {
                    val firebaseListener = it.addSnapshotListener { value, error ->
                        value?.toObjects(Message::class.java)?.run {
                            sortByDescending { it.createdAt }
                            callback.invoke(this)
                        }
                    }
                    it
                }
                .get().listener(success = {
                    val unreadMessages = it.toObjects(Message::class.java)
                    unreadMessages.sortByDescending { it.createdAt }
                    if (unreadMessages.isEmpty()) {
                        getLastMessage(chatId, callback)
                    } else {
                        callback.invoke(unreadMessages)
                    }
                })
    }

    fun getLastMessage(chatId: String, callback: (List<Message>) -> Unit) {
        chatMessages(chatId)
                .orderBy("createdAt", Query.Direction.DESCENDING)
                .limit(1).get().listener(success = {
                    val lastMessage = it.toObjects(Message::class.java)
                    callback.invoke(lastMessage ?: emptyList())
                })
    }

    fun updateChat(chat: Chat) {
        db.runTransaction {
            it.set(
                    singleChatRef(chat.id), chat
            )
        }.listener(success = {

        }, error = {

        })
    }

    fun updateChatUpdateTime(chatId: String) {
        db.runTransaction {
            val chatFromDb = it.get(singleChatRef(chatId)).toObject(Chat::class.java)?.apply {
                updatedAt = System.currentTimeMillis()
            }
            chatFromDb?.run {
                it.set(singleChatRef(id), this)
            }
        }.listener(success = {

        }, error = {

        })
    }

    fun istyping(chatId: String){

        // is typing feature
        val handler = Handler()
        val last_text_edit = 0
        val  delay = 0

//      val  input_finish_checker = Runnable {
//            if (System.currentTimeMillis() > last_text_edit + delay - 500) {
//                singleChatRef(chatId).
////                mRootRef.child("messages").child(mCurrentUser.getUid()).child("typing")
////                    .setValue(false)
//            }
//        }
    }

    private fun getChatContaining(chatId: String, callback: (List<Chat>) -> Unit) {
        // Log.e("GET_CHAT", participants.toString())
        chatListRef
                .whereEqualTo("id", chatId)
                .get().listener(success = {
                    Log.e("GET_CHAT", it.size().toString())
                    val chat = it.toObjects(Chat::class.java).also {
                        Log.e("GET_CHAT", it.size.toString())
                    }
                    Log.e("GET_CHAT", chat.firstOrNull()?.toString() ?: "not chat found")
                    callback.invoke(chat)
                }, error = {
                    Log.e("GET_CHAT", it.localizedMessage)

                })
    }

    /**
     * View chats
     * Get Chat participants
     * Add message to chat
     * List chat messages
     * Get Contacts
     */

}

/**
 * @param typeClass example [Class::class.java]
 * @param query example [ChatViewModel.getChatDetails]
 * @param itemView example [RecyclerView.ViewHolder]
 * @param binder @see [RecyclerView.Adapter.onBindViewHolder]
 *      emit ViewHolder, position, itemViewType, Object of T
 * @param getViewType @see [RecyclerView.Adapter.getItemViewType] emits position and Object if T
 *      return ItemViewType as Int
 * @param itemVisible callback that returns [ViewHolder] and [Boolean] true if viewholder isVisible, false if viewholder is not visible
 *
 */
class GenericFirebaseAdapter<T>(
        val typeClass: Class<T>,
        val query: Query,
        val itemView: (ViewGroup, Int) -> RecyclerView.ViewHolder,
        val binder: (RecyclerView.ViewHolder, Int, Int, T) -> Unit,
        val getViewType: (Int, T) -> Int,
        val itemVisible: ((RecyclerView.ViewHolder, Boolean) -> Unit)? = null
) : FirestoreRecyclerAdapter<T, RecyclerView.ViewHolder>(
        FirestoreRecyclerOptions.Builder<T>().setQuery(query, typeClass).build()
) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return itemView.invoke(parent, viewType)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int, model: T) {
        binder.invoke(holder, position, holder.itemViewType, model)
    }

    override fun getItemViewType(position: Int): Int {
        return getViewType.invoke(position, getItem(position))
    }

    override fun onViewAttachedToWindow(holder: RecyclerView.ViewHolder) {
        super.onViewAttachedToWindow(holder)
        itemVisible?.invoke(holder, true)
    }

    override fun onViewDetachedFromWindow(holder: RecyclerView.ViewHolder) {
        super.onViewDetachedFromWindow(holder)
        itemVisible?.invoke(holder, false)
    }
}

open class GenericData {
    val createdAt: Long = System.currentTimeMillis()
    var updatedAt: Long = System.currentTimeMillis()
}

class Chat() : GenericData() {

    var name: String = ""
    var id: String = ""
    var chatImage: String = ""
    var participantIds: ArrayList<Int> = ArrayList()
    val isGroup: Boolean get() = participantIds.size > 2

    constructor(
            name: String = "",
            id: String = "",
            chatImage: String = "",
            participantIds: ArrayList<Int> = ArrayList()
    ) : this() {

        this.name = name
        this.id = id
        this.chatImage = chatImage
        this.participantIds = participantIds
    }


}

/**
 * @see [User]
 */
data class Participants(
        var id: Int = 0,
        var displayName: String = "",
        var displayImage: String = ""
) : GenericData()

data class Message(
        var id: String = "${System.currentTimeMillis()}",
        var message: String = "",
        var imageUrl: List<String> = emptyList(),
        var createdBy: Int = 0,
        var readBy: ArrayList<Int> = ArrayList(),
        var read:Boolean = false
) : GenericData()

fun <T> Task<T>.listener(success: ((T) -> Unit)? = null, error: ((Exception) -> Unit)? = null) {
    this.addOnSuccessListener {
        success?.invoke(it)
    }.addOnFailureListener {
        error?.invoke(it)
        Log.e("FIREBASE_TASK", it.localizedMessage, it)
    }
}