package com.app.ecobba.common.FCMCHAT;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.NavController;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.ecobba.R;
import com.app.ecobba.Utils.BaseFragment;
import com.app.ecobba.common.SharedMethods;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;

import kotlin.Unit;

public class ChatContact  extends BaseFragment {
    private RecyclerView recyclerView;
    private  GenericFirebaseAdapter<Chat> adapter;
    private  ChatViewModel chatViewModel;
    int userId = 0;
    String  username,userprofilepicture;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_chat_contact,container,false);
        recyclerView = view.findViewById(R.id.rvContacts);
        chatViewModel = ViewModelProviders.of(this).get(ChatViewModel.class);
        userId = SharedMethods.getUser(this.requireContext());
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(requireContext());
        recyclerView.setLayoutManager(layoutManager);


        adapter = new GenericFirebaseAdapter<Chat>(
                Chat.class, chatViewModel.getUserChats(userId),(parent,type)->{
                    return new ContactViewHolder(parent);
        },((viewHolder, position, viewtype, chat) -> {
            ContactViewHolder item = (ContactViewHolder) viewHolder;
            chatViewModel.getChatDetails(userId,chat,(name,photo)->{
                /**
                 * setname from name
                 * set photo from photo
                 */
                item.name.setText(name);
                Glide.with(requireContext()).load(photo).circleCrop().into(item.photo);
                username = name;
                userprofilepicture = photo;
                return Unit.INSTANCE;
            });

            chatViewModel.getUnreadOrLastMessage(userId,chat.getId(),messages -> {
                /**
                 * add logic on the last message
                 */

                if (messages.size()>0){
                    Message lastmessage = messages.get(0);
                    item.message.setText(lastmessage.getMessage());
                    item.message.setOnClickListener(v -> {
                        Bundle args = new Bundle();
                        String photos = userprofilepicture;
                        String twaaname = username;
                        args.putString("PHOTO",photos);
                        args.putString("NAME",twaaname);
                        args.putString("CHATM",new Gson().toJson(chat));
                        NavHostFragment.findNavController(requireParentFragment()).navigate(R.id.chatMessages,args);
                    });
                }
                return Unit.INSTANCE;
            });
            return Unit.INSTANCE;
        }),(position,chat)->{
            return  1;
        },null
        );
       recyclerView.setAdapter(adapter);



    }

  class ContactViewHolder extends RecyclerView.ViewHolder {
        ImageView photo;
        TextView name,message;
        public ContactViewHolder(ViewGroup parent) {
            super(LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_chat_participants,parent,false));
            photo = itemView.findViewById(R.id.imageView9);
            name = itemView.findViewById(R.id.tvName);
            message = itemView.findViewById(R.id.textViewMessage);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        adapter.startListening();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        adapter.stopListening();
    }

    @Override
    public void onResume() {
        super.onResume();
        adapter.stopListening();
        adapter.notifyDataSetChanged();
    }
}
