package com.app.ecobba.recyclerview_models.ServiceCategories

data class servicesCategoriesResponse(
    val data: List<String>,
    val message: String,
    val success: Boolean
)