package com.app.ecobba.recyclerview_models;

public class UserTransactionsAdapter {

    private String trx_code, amount, trx_type, date,status;
    public UserTransactionsAdapter(){
        // required empty constructor
    }

    public UserTransactionsAdapter(String trx_code, String amount, String trx_type, String date,String status) {
        this.trx_code = trx_code;
        this.amount = amount;
        this.trx_type = trx_type;
        this.date = date;
        this.status = status;
    }

    public String getTrx_code() {
        return trx_code;
    }

    public void setTrx_code(String trx_code) {
        this.trx_code = trx_code;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getTrx_type() {
        return trx_type;
    }

    public void setTrx_type(String trx_type) {
        this.trx_type = trx_type;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
