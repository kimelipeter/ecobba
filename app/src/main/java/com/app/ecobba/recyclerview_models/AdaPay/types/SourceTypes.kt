package com.app.ecobba.recyclerview_models.AdaPay.types

data class SourceTypes(
    val adapay_source_types: Map<String,String>, //val adapay_source_types: Map<String,String>,
    val message: String,
    val success: Boolean
)