package com.app.ecobba.recyclerview_models.nextLoan

data class Organization(
    val name: String
)