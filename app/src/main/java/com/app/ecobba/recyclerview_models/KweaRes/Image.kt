package com.app.ecobba.recyclerview_models.KweaRes

import lombok.Data

@Data
class Image {
    var createdAt: String? = null
    var id: Long = 0
    var image: String? = null
    var updatedAt: String? = null
}