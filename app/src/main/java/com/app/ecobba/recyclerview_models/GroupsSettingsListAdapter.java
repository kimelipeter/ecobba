package com.app.ecobba.recyclerview_models;

public class GroupsSettingsListAdapter  {

    private String id,settings_name,settings_description,status;
    private Boolean is_enabled;

    public GroupsSettingsListAdapter(String id, String settings_name, String settings_description, String status, Boolean is_enabled) {
        this.id = id;
        this.settings_name = settings_name;
        this.settings_description = settings_description;
        this.status = status;
        this.is_enabled = is_enabled;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSettings_name() {
        return settings_name;
    }

    public void setSettings_name(String settings_name) {
        this.settings_name = settings_name;
    }

    public String getSettings_description() {
        return settings_description;
    }

    public void setSettings_description(String settings_description) {
        this.settings_description = settings_description;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Boolean getIs_enabled() {
        return is_enabled;
    }

    public void setIs_enabled(Boolean is_enabled) {
        this.is_enabled = is_enabled;
    }
}
