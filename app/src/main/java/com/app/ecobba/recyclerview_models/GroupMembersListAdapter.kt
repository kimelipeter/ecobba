package com.app.ecobba.recyclerview_models

class GroupMembersListAdapter(
        val user_id: String,
        var group_id: String,
        var member_id: String,
        var member_name: String,
        var member_email: String,
        var role: String,
        var phone_number: String,
        var member_since: String,
         var avatar: String)