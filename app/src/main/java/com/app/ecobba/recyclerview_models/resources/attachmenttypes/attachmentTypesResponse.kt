package com.app.ecobba.recyclerview_models.resources.attachmenttypes

data class attachmentTypesResponse(
    val `data`: List<String>,
    val message: String,
    val success: Boolean
)