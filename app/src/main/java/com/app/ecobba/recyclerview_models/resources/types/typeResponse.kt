package com.app.ecobba.recyclerview_models.resources.types

data class typeResponse(
    val data: List<String>,
    val message: String,
    val success: Boolean
)