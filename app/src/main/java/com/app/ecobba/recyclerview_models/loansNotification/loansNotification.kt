package com.app.ecobba.recyclerview_models.loansNotification

data class loansNotification(
    val loans: Loans,
    val message: String,
    val success: Boolean
)