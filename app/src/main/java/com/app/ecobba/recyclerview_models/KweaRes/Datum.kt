package com.app.ecobba.recyclerview_models.KweaRes

import lombok.Data

@Data
class Datum {
    var created_at: String? = null
    var id: Long = 0
    var images: List<Image>? = null
    var item_category: String? = null
    var item_currency: String? = null
    var item_description: String? = null
    var item_name: String? = null
    var item_price: Long = 0
    var shop: Shop? = null
    var updated_at: String? = null
}