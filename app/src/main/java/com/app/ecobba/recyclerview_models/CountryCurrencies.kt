package com.app.ecobba.recyclerview_models

data class CountryCurrencies(
    val data: List<Data>,
    val success: Boolean
)