package com.app.ecobba.recyclerview_models.nextLoan

data class Loan(
    val amount: Int,
    val borrower_credit_score: Int,
    val created_at: String,
    val group_id: Any,
    val guarantors: Any,
    val id: Int,
    val loan_package_id: Int,
    val loan_title: String,
    val org_id: Any,
    val status: Int,
    val updated_at: String,
    val user_id: Any
)