package com.app.ecobba.recyclerview_models.loansNotification

data class Loans(
    val date: String,
    val message: String
)