package com.app.ecobba.recyclerview_models.resources

data class X22(
    val attachments: String?,
    val attachmenttype: String?,
    val category: String?,
    val content: String?,
    val created_at: String?,
    val datesingle: String?,
    val id: Int?,
    val identification_document: Any?,
    val imageurl: String?,
    val link: String?,
    val name: String?,
    val type: String?,
    val updated_at: String?,
    val user: UserX?,
    val user_id: Int?,
    val videourl: Any?
)