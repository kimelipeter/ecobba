package com.app.ecobba.recyclerview_models.KweaRes

import com.app.ecobba.Models.KweaItemsShop.User
import lombok.Data

@Data
class Shop (
//    var created_at: String? = null
//    var id: Long = 0
//    var shop_address: String? = null
//    var shop_email: String? = null
//    var shop_msisdn: String? = null
//    var shop_name: String? = null
//    var updated_at: String? = null
//    var user_id: Long = 0
        val created_at: String?,
        val id: Int?,
        val shop_address: String?,
        val shop_email: String?,
        val shop_msisdn: String?,
        val shop_name: String?,
        val updated_at: String?,
        val user: User?,
        val user_id: Int?
)