package com.app.ecobba.recyclerview_models.nextLoan

data class Currency(
    val name: String,
    val prefix: String
)