package com.app.ecobba.recyclerview_models.AdaPay

data class AdapaySource(
    val created_at: String?,
    val group_id: Int?,
    val id: Int?,
    val msisdn: String? =null,
    val telco: String? = null,
    val token: String?,
    val type: String?,
    val updated_at: String?,
    val usage: String?,
    val user_id: Int?
)