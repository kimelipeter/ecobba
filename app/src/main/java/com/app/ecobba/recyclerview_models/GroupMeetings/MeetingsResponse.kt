package com.app.ecobba.recyclerview_models.GroupMeetings

import lombok.Data

@Data
class MeetingsResponse {
    var group: Group? = null
    var meetings: List<Meeting>? = null
    var message: String? = null
    var success: Boolean? = null
}