package com.app.ecobba.recyclerview_models;

public class GroupsFinesAdapter {

    private String name,amount;


    public GroupsFinesAdapter(String name, String amount) {
        this.name = name;
        this.amount = amount;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }
}
