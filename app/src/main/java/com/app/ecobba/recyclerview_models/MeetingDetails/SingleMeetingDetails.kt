package com.app.ecobba.recyclerview_models.MeetingDetails

data class SingleMeetingDetails(
    val `data`: Data,
    val message: String,
    val success: Boolean
)