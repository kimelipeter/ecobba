package com.app.ecobba.recyclerview_models;

public class MyLoansPackagesBorrowersListAdapter {

    String user_names,amount,transaction_status,borrowed_date;

    public MyLoansPackagesBorrowersListAdapter(String user_names, String amount, String transaction_status, String borrowed_date) {
        this.user_names = user_names;
        this.amount = amount;
        this.transaction_status = transaction_status;
        this.borrowed_date = borrowed_date;
    }

    public String getUser_names() {
        return user_names;
    }

    public void setUser_names(String user_names) {
        this.user_names = user_names;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getStatus() {
        return transaction_status;
    }

    public void setStatus(String transaction_status) {
        this.transaction_status = transaction_status;
    }

    public String getBorrowed_date() {
        return borrowed_date;
    }

    public void setBorrowed_date(String borrowed_date) {
        this.borrowed_date = borrowed_date;
    }
}
