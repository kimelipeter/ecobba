package com.app.ecobba.recyclerview_models;

public class UserLoansAdapter {

    private String id,loan_name, amount,  date;
    private int loan_status;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public UserLoansAdapter(String id, String loan_name, String amount, String date, int loan_status) {
        this.loan_name = loan_name;
        this.amount = amount;
        this.date = date;
        this.id = id;
        this.loan_status = loan_status;
    }

    public UserLoansAdapter(){
        // required empty constructor
    }



    public int getLoan_status() {
        return loan_status;
    }

    public void setLoan_status(int loan_status) {
        this.loan_status = loan_status;
    }

    public String getLoan_name() {
        return loan_name;
    }

    public void setLoan_name(String loan_name) {
        this.loan_name = loan_name;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
