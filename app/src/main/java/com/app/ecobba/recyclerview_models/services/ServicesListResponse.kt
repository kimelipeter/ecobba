package com.app.ecobba.recyclerview_models.services

data class ServicesListResponse(
    val `data`: List<Data>?,
    val message: String?,
    val success: Boolean?
)