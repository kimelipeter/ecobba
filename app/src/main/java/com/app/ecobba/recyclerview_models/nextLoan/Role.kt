package com.app.ecobba.recyclerview_models.nextLoan

data class Role(
    val created_at: String,
    val description: String,
    val display_name: Any,
    val id: Int,
    val name: String,
    val pivot: Pivot,
    val updated_at: String
)