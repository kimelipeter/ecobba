package com.app.ecobba.recyclerview_models.GroupMeetings

import com.google.gson.annotations.SerializedName
import lombok.Data

@Data
class Group {
    @SerializedName(value = "account_no", alternate = ["accountNo"])
    var accountNo: String? = null
    var accountVerified: Long = 0
    var associationId: String? = null
    var bank: Long = 0
    var bankBranch: String? = null
    var bankName: String? = null
    var branchId: String? = null
    var comment: String? = null
    var createdAt: String? = null
    var groupCertificate: String? = null
    var groupData: String? = null
    var groupNumber: String? = null
    var groupSettings: String? = null
    var id: Long = 0
    var levelFour: String? = null
    var levelOne: String? = null
    var levelThree: String? = null
    var levelTwo: String? = null
    var name: String? = null
    var orgId: String? = null
    var status: Long = 0
    var trainerId: String? = null

    @SerializedName(value = "updated_at", alternate = ["updatedAt"])
    var updatedAt: String? = null
    var userId: Long = 0
}