package com.app.ecobba.recyclerview_models.nextLoan

data class Pivot(
    val role_id: Int,
    val user_id: Int,
    val user_type: String
)