package com.app.ecobba.recyclerview_models

import org.json.JSONArray

class GroupMeetingsMembersAdapter(var group_id: String,
                                  var meeting_id: String,
                                  var memberId: String,
                                  var memberName: String,
                                  var totalHisa: String,
                                  var totalJamii: String,
                                  var totalMemberShipfee: String,
                                  var attendance_status: String,
                                  var attendance_reason: String,
                                  var meeting_status: String,
                                  var totalFines: Int,
                                  var meeting_fines: JSONArray,
                                  var ready_for_collection: String,
                                  var status: Boolean)