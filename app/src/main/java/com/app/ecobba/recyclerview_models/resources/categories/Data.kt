package com.app.ecobba.recyclerview_models.resources.categories

data class Data(
    val educational: String,
    val facts: String,
    val finance: String,
    val other: String
)