package com.app.ecobba.recyclerview_models

class GroupMeetingsListAdapter {
    var meeting_id: String? = null
    var meeting_date: String? = null
    var meeting_time: String? = null
    var meeting_time_difference: String? = null
    var meeting_venue: String? = null
    var meeting_status: String? = null
    var ready_for_collection: String? = null
    var group_id: String? = null
    var attendance_array: String? = null
    var meeting_name: String? = null

    // JSONArray attendance_array;
    private constructor() {}
    constructor(meeting_id: String?, meeting_date: String?, meeting_time: String?, meeting_time_difference: String?, meeting_venue: String?, meeting_status: String?, ready_for_collection: String?, group_id: String?, attendance_array: String? ,meeting_name: String?) {
        this.meeting_id = meeting_id
        this.meeting_date = meeting_date
        this.meeting_time = meeting_time
        this.meeting_time_difference = meeting_time_difference
        this.meeting_venue = meeting_venue
        this.meeting_status = meeting_status
        this.ready_for_collection = ready_for_collection
        this.group_id = group_id
        this.attendance_array = attendance_array
        this.meeting_name=meeting_name
    }
}