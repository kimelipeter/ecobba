package com.app.ecobba.recyclerview_models.AdaPay.types

data class AdapaySourceTypes(
    val card_hosted: String,
    val mobile_money: String
)