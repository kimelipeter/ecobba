package com.app.ecobba.recyclerview_models.services

data class Data(
    val category: String?,
    val description: String?,
    val duration: String?,
    val id: Int?,
    val image: String?,
    val price: String?,
    val service_image: String?,
    val title: String?,
    val user: User?,
    val user_id: Int?
)