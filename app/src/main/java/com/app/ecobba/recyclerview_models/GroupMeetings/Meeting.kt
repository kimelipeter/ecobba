package com.app.ecobba.recyclerview_models.GroupMeetings

import com.google.gson.annotations.SerializedName
import lombok.Data

@Data
class Meeting {
    var attendance: Long = 0

    @SerializedName(value = "created_at", alternate = ["createdAt"])
    var createdAt: String? = null

    @SerializedName(value = "group_id", alternate = ["groupId"])
    var groupId: Long = 0
    var id: Long = 0

    @SerializedName(value = "meeting_date", alternate = ["meetingDate"])
    var meetingDate: String? = null

    @SerializedName(value = "meeting_minutes", alternate = ["meetingName"])
    var meetingMinutes: String? = null

    @SerializedName(value = "meeting_name", alternate = ["meetingName"])
    var meetingName: String? = null

    @SerializedName(value = "ready_for_collection", alternate = ["readyForCollection"])
    var readyForCollection: Long = 0
    var status: Long = 0
    var time: String? = null

    @SerializedName(value = "updated_at", alternate = ["updatedAt"])
    var updatedAt: String? = null
    var venue: String? = null
}