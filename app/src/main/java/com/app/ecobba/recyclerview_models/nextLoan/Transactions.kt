package com.app.ecobba.recyclerview_models.nextLoan

data class Transactions(
    val credit: Int,
    val debit: Int
)