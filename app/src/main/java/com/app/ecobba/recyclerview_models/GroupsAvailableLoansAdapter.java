package com.app.ecobba.recyclerview_models;

public class GroupsAvailableLoansAdapter {

    private String loan_id,group_id,group_name,loan_name,interest_rate,repay_plan,repay_period,fine_charged;

    public GroupsAvailableLoansAdapter(String loan_id, String group_id, String group_name, String loan_name, String interest_rate, String repay_plan, String repay_period, String fine_charged) {
        this.loan_id = loan_id;
        this.group_id = group_id;
        this.group_name = group_name;
        this.loan_name = loan_name;
        this.interest_rate = interest_rate;
        this.repay_plan = repay_plan;
        this.repay_period = repay_period;
        this.fine_charged = fine_charged;
    }

    public String getLoan_id() {
        return loan_id;
    }

    public void setLoan_id(String loan_id) {
        this.loan_id = loan_id;
    }

    public String getGroup_id() {
        return group_id;
    }

    public void setGroup_id(String group_id) {
        this.group_id = group_id;
    }

    public String getGroup_name() {
        return group_name;
    }

    public void setGroup_name(String group_name) {
        this.group_name = group_name;
    }

    public String getLoan_name() {
        return loan_name;
    }

    public void setLoan_name(String loan_name) {
        this.loan_name = loan_name;
    }

    public String getInterest_rate() {
        return interest_rate;
    }

    public void setInterest_rate(String interest_rate) {
        this.interest_rate = interest_rate;
    }

    public String getRepay_plan() {
        return repay_plan;
    }

    public void setRepay_plan(String repay_plan) {
        this.repay_plan = repay_plan;
    }

    public String getRepay_period() {
        return repay_period;
    }

    public void setRepay_period(String repay_period) {
        this.repay_period = repay_period;
    }

    public String getFine_charged() {
        return fine_charged;
    }

    public void setFine_charged(String fine_charged) {
        this.fine_charged = fine_charged;
    }
}
