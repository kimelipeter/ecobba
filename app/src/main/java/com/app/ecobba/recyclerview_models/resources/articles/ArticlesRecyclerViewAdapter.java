package com.app.ecobba.recyclerview_models.resources.articles;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.VideoView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.ecobba.Fragments.Resources.Articles;
import com.app.ecobba.Fragments.Resources.ResourceActivity;
import com.app.ecobba.R;
import com.app.ecobba.recyclerview_models.resources.*;
import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class ArticlesRecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<Data> list = new ArrayList<>();
    int imageView = 1;
    int videoView = 2;
    Context ctx;
    public ClickListener clickListener;
    private MediaController ctlr;
    private ProgressDialog bar;

    public ArticlesRecyclerViewAdapter(Context ctx, ClickListener clickListener) {
        this.ctx = ctx;
        this.clickListener = clickListener;
    }

    public void setList(List<Data> newList) {
        list.clear();
        list.addAll(newList);
       // list.get(0).setAttachmenttype("video");  //was testing videos
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        LayoutInflater mInflater = LayoutInflater.from(ctx);

        if (viewType == imageView) {
            view = mInflater.inflate(R.layout.article_list_item, parent, false);
            return new ImageViewHolder(view);
        } else {
            view = mInflater.inflate(R.layout.article_list_item_video, parent, false);
            return new VideoViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        Data data = list.get(position);

        if (holder.getItemViewType() == imageView) {
            ImageViewHolder imageViewHolder = (ImageViewHolder) holder;
            imageViewHolder.bind(data);
        } else {
            VideoViewHolder videoViewHolder = (VideoViewHolder) holder;
            videoViewHolder.bind(data);
        }

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public int getItemViewType(int position) {
        Data data = list.get(position);

        if (data.getAttachmenttype().equals("image")) {
            return imageView;
        } else {
            return videoView;
        }
    }

    public class VideoViewHolder extends RecyclerView.ViewHolder {
        TextView tv_productname;
        TextView tv_category;
        VideoView videoView;
        LinearLayout action_shops;
        androidx.constraintlayout.widget.ConstraintLayout view_container;

        public VideoViewHolder(@NonNull View itemView) {
            super(itemView);
            action_shops = itemView.findViewById(R.id.action_shops);
            view_container = itemView.findViewById(R.id.shop_container);
            tv_productname = itemView.findViewById(R.id.productname);
            tv_category = itemView.findViewById(R.id.category);
            videoView = itemView.findViewById(R.id.thumbnail_image);
        }

        public void bind(Data data) {
            bar=new ProgressDialog(ctx);
            bar.setTitle("Connecting server");
            bar.setMessage("Please Wait... ");
            bar.setCancelable(false);
            bar.show();

            action_shops.setAnimation(AnimationUtils.loadAnimation(ctx, R.anim.fade_transition_animation));
            tv_productname.setText(data.getName());
            tv_category.setText(data.getCategory());
          //  String path_url = "https://commondatastorage.googleapis.com/gtv-videos-bucket/sample/Sintel.mp4";
            String path_url=data.getAttachments();
            Log.e("DATA_VIDEO_URL", path_url);

            String product_id;
            product_id = String.valueOf(data.getId());
            Log.e("product_id_is", product_id);


            if (bar.isShowing()) {
                videoView.setVideoURI(Uri.parse(path_url)); //the string of the URL mentioned above
                videoView.start();
                ctlr = new MediaController(ctx);
                ctlr.setMediaPlayer(videoView);
                videoView.setMediaController(ctlr);
                videoView.requestFocus();
            }
            bar.dismiss();
            itemView.setOnClickListener(v -> {
                clickListener.passData(data);
            });

        }

    }


    public class ImageViewHolder extends RecyclerView.ViewHolder {
        TextView tv_productname;
        TextView tv_category;
        TextView tv_owner_name;
        ImageView product_img;
        ImageView profile_image;
        LinearLayout action_shops;
        androidx.constraintlayout.widget.ConstraintLayout view_container;

        public ImageViewHolder(@NonNull View itemView) {
            super(itemView);
            action_shops = itemView.findViewById(R.id.action_shops);
            view_container = itemView.findViewById(R.id.shop_container);
            tv_productname = itemView.findViewById(R.id.productname);
            tv_category = itemView.findViewById(R.id.category);
            product_img = itemView.findViewById(R.id.thumbnail_image);
            tv_owner_name= itemView.findViewById(R.id.owner_name);
            profile_image = itemView.findViewById(R.id.profile_image);
        }

        public void bind(Data data) {
            action_shops.setAnimation(AnimationUtils.loadAnimation(ctx, R.anim.fade_transition_animation));
            tv_productname.setText(data.getName());
            tv_category.setText(data.getCategory());
            String owner=data.getUser().getName()+" " +data.getUser().getOther_names();
            tv_owner_name.setText(owner);
            String service_image_url = data.getAttachments();

            //  load image from the internet using Picasso
            String owner_profile=data.getUser().getAvatar();
            Picasso.get().load(owner_profile).fit().centerCrop().into(profile_image);

            Log.e("DATA_IMAGE_ARICLES", service_image_url);

            String product_id;
            product_id = String.valueOf(data.getId());
            Log.e("product_id_is", product_id);

            Picasso.get()
                    .load(new File(service_image_url))
                    .placeholder(R.drawable.background_gradient_black)

                    .into(product_img);
            Picasso.get()
                    .load(service_image_url)
                    .placeholder(R.drawable.background_gradient_black)
                    .into(product_img);

            itemView.setOnClickListener(v -> {
                clickListener.passData(data);
            });


        }
    }

    public interface ClickListener {

        void passData(Data data);
    }
}
