package com.app.ecobba.recyclerview_models.resources.categories

data class CategoriesResponse(
    val data: Data,
    val message: String,
    val success: Boolean
)