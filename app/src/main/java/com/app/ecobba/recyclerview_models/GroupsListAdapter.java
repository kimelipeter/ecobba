package com.app.ecobba.recyclerview_models;

public class GroupsListAdapter {
    String id, group_number,group_name,group_status,group_comment,group_creation_date,group_balance,group_totalFines,group_totalFees,group_totalMembers,group_totalHisa,group_totalJamii;


    public GroupsListAdapter(){

    }

    public GroupsListAdapter(String id, String group_number, String group_name, String group_status, String group_comment, String group_creation_date, String group_balance, String group_totalFines, String group_totalFees, String group_totalMembers, String group_totalHisa, String group_totalJamii) {
        this.id = id;
        this.group_number = group_number;
        this.group_name = group_name;
        this.group_status = group_status;
        this.group_comment = group_comment;
        this.group_creation_date = group_creation_date;
        this.group_balance = group_balance;
        this.group_totalFines = group_totalFines;
        this.group_totalFees = group_totalFees;
        this.group_totalMembers = group_totalMembers;
        this.group_totalHisa = group_totalHisa;
        this.group_totalJamii = group_totalJamii;
    }

    public String getGroup_totalHisa() {
        return group_totalHisa;
    }

    public void setGroup_totalHisa(String group_totalHisa) {
        this.group_totalHisa = group_totalHisa;
    }

    public String getGroup_totalJamii() {
        return group_totalJamii;
    }

    public void setGroup_totalJamii(String group_totalJamii) {
        this.group_totalJamii = group_totalJamii;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getGroup_number() {
        return group_number;
    }

    public void setGroup_number(String group_number) {
        this.group_number = group_number;
    }

    public String getGroup_name() {
        return group_name;
    }

    public void setGroup_name(String group_name) {
        this.group_name = group_name;
    }

    public String getGroup_status() {
        return group_status;
    }

    public void setGroup_status(String group_status) {
        this.group_status = group_status;
    }

    public String getGroup_comment() {
        return group_comment;
    }

    public void setGroup_comment(String group_comment) {
        this.group_comment = group_comment;
    }

    public String getGroup_creation_date() {
        return group_creation_date;
    }

    public void setGroup_creation_date(String group_creation_date) {
        this.group_creation_date = group_creation_date;
    }

    public String getGroup_balance() {
        return group_balance;
    }

    public void setGroup_balance(String group_balance) {
        this.group_balance = group_balance;
    }

    public String getGroup_totalFines() {
        return group_totalFines;
    }

    public void setGroup_totalFines(String group_totalFines) {
        this.group_totalFines = group_totalFines;
    }

    public String getGroup_totalFees() {
        return group_totalFees;
    }

    public void setGroup_totalFees(String group_totalFees) {
        this.group_totalFees = group_totalFees;
    }

    public String getGroup_totalMembers() {
        return group_totalMembers;
    }

    public void setGroup_totalMembers(String group_totalMembers) {
        this.group_totalMembers = group_totalMembers;
    }
}
