package com.app.ecobba.recyclerview_models.nextLoan

data class NextLoanRepaymentResponse(
    val message: String,
    val success: Boolean,
    val user: User
)