package com.app.ecobba.recyclerview_models.MeetingDetails

data class Meetingsummary(
    val absent: Int,
    val absent_with_reason: Int,
    val absent_without_reason: Int,
    val group_fines: Any,
    val present: Int,
    val total: Int,
    val total_fees: Int,
    val total_fines: Int,
    val total_jamii: Int,
    val total_shares: Int,
    val total_shares_value: Int
)