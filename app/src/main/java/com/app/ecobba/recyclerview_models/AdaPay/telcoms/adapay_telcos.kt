package com.app.ecobba.recyclerview_models.AdaPay.telcoms

data class adapay_telcos(
    val adapay_telcos: AdapayTelcos,
    val message: String,
    val success: Boolean
)