package com.app.ecobba.recyclerview_models.AdaPay

data class Sources(
    val adapay_sources: List<AdapaySource>,
    val message: String,
    val success: Boolean
)