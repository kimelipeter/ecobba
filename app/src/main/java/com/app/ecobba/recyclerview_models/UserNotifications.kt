package com.app.ecobba.recyclerview_models

data class UserNotifications (
        var message:String,
        var date:String
)