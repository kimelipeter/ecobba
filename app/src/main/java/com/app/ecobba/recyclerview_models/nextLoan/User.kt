package com.app.ecobba.recyclerview_models.nextLoan

data class User(
    val account_no: Any,
    val account_verified: Int,
    val avatar: String,
    val created_at: String,
    val credit_score: Int,
    val customer_username: Any,
    val deleted_at: Any,
    val detail: Detail,
    val email: String,
    val email_verified_at: Any,
    val id: Int,
    val identification_document: Any,
    val msisdn: String,
    val name: String,
    val next_loan_repayment: NextLoanRepayment,
    val organization: Organization,
    val other_names: String,
    val roles: List<Role>,
    val status: Int,
    val token: String,
    val transactions: Transactions,
    val updated_at: String,
    val verified: Int,
    val wallet: Wallet
)