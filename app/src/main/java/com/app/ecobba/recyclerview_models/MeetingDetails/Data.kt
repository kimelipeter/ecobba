package com.app.ecobba.recyclerview_models.MeetingDetails

data class Data(
    val attendance: List<Any>,
    val meetingsummary: Meetingsummary
)