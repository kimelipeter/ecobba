package com.app.ecobba.recyclerview_models;

public class GroupTransactionsAdapter {
    private String id,full_names,amount,tx_code,transaction_date_time,transaction_type,currencyValue;


    public GroupTransactionsAdapter(String id, String full_names, String amount, String tx_code, String transaction_date_time, String transaction_type,String currencyValue) {
        this.id = id;
        this.full_names = full_names;
        this.amount = amount;
        this.tx_code = tx_code;
        this.transaction_date_time = transaction_date_time;
        this.transaction_type = transaction_type;
        this.currencyValue=currencyValue;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFull_names() {
        return full_names;
    }

    public void setFull_names(String full_names) {
        this.full_names = full_names;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getTx_code() {
        return tx_code;
    }

    public void setTx_code(String tx_code) {
        this.tx_code = tx_code;
    }

    public String getTransaction_date_time() {
        return transaction_date_time;
    }

    public void setTransaction_date_time(String transaction_date_time) {
        this.transaction_date_time = transaction_date_time;
    }

    public String getTransaction_type() {
        return transaction_type;
    }

    public void setTransaction_type(String transaction_type) {
        this.transaction_type = transaction_type;
    }

    public String getCurrencyValue() {
        return currencyValue;
    }

    public void setCurrency(String currencyValue) {
        this.currencyValue = currencyValue;
    }
}
