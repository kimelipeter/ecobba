package com.app.ecobba.recyclerview_models.KweaRes

import lombok.Data

@Data
class KweaShopResponse {
    var data: List<Datum>? = null
    var success: Boolean? = null
}