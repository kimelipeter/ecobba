package com.app.ecobba.recyclerview_models;

public class GroupsBorrowedLoansAdapter {

    private String loan_id,loan_name,borrower_name,group_id,group_name,loan_amount,borrowed_date,loan_status;

    public GroupsBorrowedLoansAdapter(String loan_id, String loan_name, String borrower_name, String group_id, String group_name, String loan_amount, String borrowed_date, String loan_status) {
        this.loan_id = loan_id;
        this.loan_name = loan_name;
        this.borrower_name = borrower_name;
        this.group_id = group_id;
        this.group_name = group_name;
        this.loan_amount = loan_amount;
        this.borrowed_date = borrowed_date;
        this.loan_status = loan_status;
    }

    public String getLoan_id() {
        return loan_id;
    }

    public void setLoan_id(String loan_id) {
        this.loan_id = loan_id;
    }

    public String getLoan_name() {
        return loan_name;
    }

    public void setLoan_name(String loan_name) {
        this.loan_name = loan_name;
    }

    public String getGroup_id() {
        return group_id;
    }

    public void setGroup_id(String group_id) {
        this.group_id = group_id;
    }

    public String getGroup_name() {
        return group_name;
    }

    public void setGroup_name(String group_name) {
        this.group_name = group_name;
    }

    public String getLoan_amount() {
        return loan_amount;
    }

    public void setLoan_amount(String loan_amount) {
        this.loan_amount = loan_amount;
    }

    public String getBorrowed_date() {
        return borrowed_date;
    }

    public void setBorrowed_date(String borrowed_date) {
        this.borrowed_date = borrowed_date;
    }

    public String getLoan_status() {
        return loan_status;
    }

    public void setLoan_status(String loan_status) {
        this.loan_status = loan_status;
    }

    public String getBorrower_name() {
        return borrower_name;
    }

    public void setBorrower_name(String borrower_name) {
        this.borrower_name = borrower_name;
    }
}
