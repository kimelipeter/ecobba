package com.app.ecobba.recyclerview_models.resources

data class resourcesResponse(
    val `data`: List<Data>?,
    val message: String?,
    val success: Boolean?
)