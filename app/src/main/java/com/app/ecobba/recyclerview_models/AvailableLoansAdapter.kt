package com.app.ecobba.recyclerview_models

class AvailableLoansAdapter {
    //    public String getRepayment_plan_id() {
    //        return repayment_plan_id;
    //    }
    //    public void setRepayment_plan_id(String repayment_plan_id) {
    //        this.repayment_plan_id = repayment_plan_id;
    //    }
    var id: String? = null
    var loan_name: String? = null
    var min_amount: String? = null
    var max_amount: String? = null
    var interest_per_annum: String? = null
    var repayment_plan: String? = null
    var repayment_plan_id: String? = null
    var insured_status: String? = null

    constructor(id: String?, loan_name: String?, min_amount: String?, max_amount: String?, interest_per_annum: String?, repayment_plan: String?, insured_status: String?) {
        this.id = id
        this.loan_name = loan_name
        this.min_amount = min_amount
        this.max_amount = max_amount
        this.interest_per_annum = interest_per_annum
        this.repayment_plan = repayment_plan
        //        this.repayment_plan_id = repayment_plan_id;
        this.insured_status = insured_status
    }

    constructor() {
        // required empty constructor
    }

    constructor(id: String?, loan_name: String?, min_amount: String?, max_amount: String?, interest_rate: String?, repayment_plan: String?, repayment_plan_id: String?, insured_status: String?) {
        this.id = id
        this.loan_name = loan_name
        this.min_amount = min_amount
        this.max_amount = max_amount
        interest_per_annum = interest_per_annum
        this.repayment_plan = repayment_plan
        //        this.repayment_plan_id = repayment_plan_id;
        this.insured_status = insured_status
    }
}