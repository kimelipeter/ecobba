package com.app.ecobba.Adapter;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.app.ecobba.Fragments.Profile.AccountProfile;
import com.app.ecobba.Fragments.Profile.AddressProfileFragment;
import com.app.ecobba.Fragments.Profile.ContactProfileFragment;
import com.app.ecobba.Fragments.Profile.IdentificationProfileFragment;
import com.app.ecobba.Fragments.Profile.OccupationProfileFragment;
import com.app.ecobba.Fragments.Profile.PersonalProfileFragment;
import com.app.ecobba.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class ProfileViewPagerAdapter extends FragmentPagerAdapter {

    ArrayList<Fragment> fragments;
    AccountProfile accountProfile;
    AddressProfileFragment addressProfileFragment;
    ContactProfileFragment contactProfileFragment;
    IdentificationProfileFragment identificationProfileFragment;
    OccupationProfileFragment occupationProfileFragment;
    PersonalProfileFragment personalProfileFragment;
    String[] title;
    public ProfileViewPagerAdapter(FragmentManager fm, Bundle bundle){
        super(fm);
    }

    public ProfileViewPagerAdapter(FragmentManager fm, Context ctx) {
        super(fm);
        fragments = new ArrayList();
        accountProfile = new AccountProfile(ctx);
        addressProfileFragment = new AddressProfileFragment(ctx);
        contactProfileFragment = new ContactProfileFragment(ctx);
        identificationProfileFragment = new IdentificationProfileFragment(ctx);
        occupationProfileFragment = new OccupationProfileFragment(ctx);
        personalProfileFragment = new PersonalProfileFragment(ctx);

        fragments.add(personalProfileFragment);
        fragments.add(contactProfileFragment);
        fragments.add(identificationProfileFragment);
        fragments.add(addressProfileFragment);
        fragments.add(occupationProfileFragment);
        fragments.add(accountProfile);
        title = ctx.getResources().getStringArray(R.array.edit_titles);

    }

    @Override
    public Fragment getItem(int position) {
        return fragments.get(position);
    }

    @Override
    public int getCount() {
        return fragments.size();
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return title[position];
    }


    public String getData() throws NullPointerException{
        String data ="{" +
                personalProfileFragment.getData()+
                occupationProfileFragment.getData()+
                identificationProfileFragment.getData()+
                contactProfileFragment.getData()+
                addressProfileFragment.getData()+
                accountProfile.getData()+
                "}";
        return data;
    }

}