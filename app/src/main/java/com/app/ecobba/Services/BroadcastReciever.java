package com.app.ecobba.Services;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;

import com.app.ecobba.common.SharedMethods;

import static com.app.ecobba.common.AppConstantsKt.NETBROADCAST;


public class BroadcastReciever extends BroadcastReceiver {



    @Override
    public void onReceive(Context context, Intent intent)
    {

        Intent notificationService = new Intent(context, BackgroundService.class);

        try {
            String background = SharedMethods.getDefaults("background",context);

//            if (background!=null) {

                ConnectivityManager connectivityManager = (ConnectivityManager) context
                        .getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo networkInfo = connectivityManager
                        .getActiveNetworkInfo();

                // Check internet connection and accrding to state change the
                // text of activity by calling method

                    if (networkInfo != null && networkInfo.isConnected()) {
                        //Check for Notifications

                        Bundle extras = intent.getExtras();
                        Intent iMsg = new Intent(NETBROADCAST);
                        iMsg.putExtra("message","work");
                        context.sendBroadcast(iMsg);

                        context.startService(notificationService);
//                        context.service

                    } else {
                        //Stop checker
                        context.stopService(notificationService);
                    }
//            }else{
//                Log.e("BG","SLEEP");
//            }
        } catch (Exception e) {
            e.printStackTrace();
        }


    }


}
