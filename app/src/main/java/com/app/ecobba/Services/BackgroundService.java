package com.app.ecobba.Services;

import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;

import androidx.navigation.NavDeepLinkBuilder;

import com.app.ecobba.Models.GenericRequest;
import com.app.ecobba.Models.Message;
import com.app.ecobba.Models.RequestsList;
import com.app.ecobba.R;
import com.app.ecobba.Utils.NotifierV2;
import com.app.ecobba.common.SharedMethods;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import static com.app.ecobba.Utils.NotifierV2.BG;
import static com.app.ecobba.Utils.NotifierV2.showNotification;
import static com.app.ecobba.common.ApisKtKt.getUserNotifications;
import static com.app.ecobba.common.AppConstantsKt.INITIATOR_GET_USER_NOTIFICATIONS;
import static com.app.ecobba.common.SharedMethods.HTTP_GET;
import static com.app.ecobba.common.SharedMethods.getDefaults;
import static com.app.ecobba.common.SharedMethods.getFromServer;
import static com.app.ecobba.common.SharedMethods.postToServer;
import static com.app.ecobba.common.SharedMethods.storePrefs;
import static com.app.ecobba.common.SharedMethods.storeSystemMessages;

public class BackgroundService extends Service implements SharedMethods.NetworkObjectListener {

    private final IBinder binder = new LocalBinder();
    private final Timer timer = new Timer();
    Context ctx;

    public class LocalBinder extends Binder {
        public BackgroundService getService() {
            // Return this instance of LocalService so clients can call public methods
            return BackgroundService.this;
        }
    }

    long time = 60*60*1000;

    public BackgroundService() {
        ctx = this;
        Log.e("BackgroundService", "Running");
        final TimerTask task = new TimerTask() {
            @Override
            public void run() {
                postData();
                getData();
            }
        };
        timer.schedule(task, 3000, time);
    }

    public void postData(){
        String raw_req = getDefaults("HTTP_REQUESTS",this);
        RequestsList requests;
        if (raw_req == null){
            requests = new RequestsList(new ArrayList<>());
        }else{
            requests = new Gson().fromJson(raw_req,RequestsList.class);
        }
        for (int i = 0;i<requests.getRequests().size();i++){
//            new Notifier(this,"BG","BACKGROUND SERVICE",0)
//                    .show("Background process","eCOBbA Application is running in the background");
//            //do post
            showNotification(this,BG,BG,"Working","eCOBbA running","eCOBbA Application is running in the background");
            GenericRequest req = requests.getRequests().get(i).getSecond();
            postToServer(
                    req.getUrl(),
                    req.getToken(),
                    req.getParams(),
                    req.getInitiator(),
                    req.getMethod(),
                    this,
                    new SharedMethods.NetworkObjectListener() {
                        @Override
                        public void onDataReady(String initiator, String response) {
                            if (initiator.equals(req.getInitiator())){
                                //Store Success Message
                                Log.e("BACKGROUND_RESPONSE", "Response is " + response);
//                                progressBar.setVisibility(View.GONE);
                                JSONObject jObj = null;
                                try {
                                    jObj = new JSONObject(response);
                                    if (jObj.has("errors")) {
                                        String errors = jObj.getString("errors");
                                        Message message = new Message(
                                                "System Message\n"+
                                                        initiator+"\n"+errors,
                                                new Date().toString()
                                        );
                                        storeSystemMessages(message,ctx);
                                        showNotification(ctx,BG,BG,initiator,errors,message.getMessage()+"\n"+message.getDate());
                                    } else { // if no error message was found in the response,
                                        boolean success = jObj.getBoolean("success");
                                        if (success) {
                                            String message = jObj.getString("message");
                                            Message _message = new Message(
                                                    "System Message\n"+
                                                            initiator+"\n"+message,
                                                    new Date().toString()
                                            );
                                            storeSystemMessages(_message,ctx);
                                            showNotification(ctx,BG,BG,initiator,message,_message.getMessage()+"\n"+_message.getDate());
                                        } else {
                                            String api_error_message = jObj.getString("message");
                                            Message __message = new Message(
                                                    "System Message\n"+
                                                            initiator+"\n"+api_error_message,
                                                    new Date().toString()
                                            );
                                            storeSystemMessages(__message,ctx);
                                            showNotification(ctx,BG,BG,initiator,api_error_message,__message.getMessage()+"\n"+__message.getDate());
                                        }
                                    }
                                } catch (JSONException e) {
                                    Log.e("Exception-DATA", e.toString());
//                                    showSweetAlertDialogError(global_error_message, ctx);
                                    e.printStackTrace();
                                }
                            }
                        }
                        @Override
                        public void onDataLoaded(String initiator, JSONArray data) {

                        }
                        @Override
                        public void onNetworkError(String initiator, String errorCode, String errorMessage) {
                                //Store Error Message
                           if (initiator.equals(req.getInitiator())){
                               showNotification(ctx,BG,BG,"Error while working",errorCode,errorMessage);
                           }
                        }
                    }
            );
            requests.getRequests().remove(i);
            storePrefs("HTTP_REQUESTS",new Gson().toJson(requests),this);
        }
    }

    private void getData(){
        String background = SharedMethods.getDefaults("background",this);
        if (background==null){
            stopSelf();
            return;
        }
        Log.e("BackgroundService","ping");
        String token = SharedMethods.getDefaults("token",ctx);
        if (token==null){
            this.onDestroy();
            Log.e("BackgroundService","not logged in");
        }else {
            getNotifications(token);
        }
    }

    private void getNotifications(String token){
        Map<String, String> params = new HashMap<>();
        params.put("token", token);
        getFromServer(getUserNotifications,token,params,INITIATOR_GET_USER_NOTIFICATIONS,HTTP_GET,this,this);
    }

    @Override
    public void onDestroy() {
        Log.e("BackgroundService","Destroyed");
        super.onDestroy();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return binder;
    }

    @Override
    public void onDataReady(String initiator, String response) {
        switch (initiator) {
            case INITIATOR_GET_USER_NOTIFICATIONS:
                JSONObject jObj = null;
                try {
                    jObj = new JSONObject(response);
                    if (jObj.has("errors")) {
                        String errors = jObj.getString("errors");
//                        showSweetAlertDialogError(errors, ctx);
                    } else { // if no error message was found in the response,
                        boolean success = jObj.getBoolean("success");
                        if (success) { // if the request was successfuls, got forward, else show error message
                            if (jObj.has("loans")) {
                                JSONArray loans = jObj.getJSONArray("loans");
                                SharedMethods.storePrefs("notifications",String.valueOf(loans.length()),ctx);
                                for (int i = 0; i < loans.length();i++) {
                                    String message = loans.getJSONObject(i).getString("message");
                                    String date = loans.getJSONObject(i).getString("date");
                                    Bundle args = new Bundle();
                                    PendingIntent intent = new NavDeepLinkBuilder(this)
                                            .setGraph(R.navigation.nav_graph)
                                            .setDestination(R.id.notificationFragment)
                                            .setArguments(args)
                                            .createPendingIntent();
                                    args.putString("response",response);

                                    NotifierV2.showNotification(ctx,
                                            BG,
                                            BG,
                                            "eCOBbA",
                                            "Notice",
                                            message+"\n"+date);

//                                    new Notifier(this, "loans","Loans",0)
//                                            .setIntent("Open App",intent,-1)
//                                            .setMessage("Loans","Recieved: "+ date,message)
//                                    .show("Loans",message);

                                }
                            }
                        } else {
                            String api_error_message = jObj.getString("message");
                        }
                    }

                } catch (JSONException e) {
                    Log.e("ExceptionLOGIN-DATA", e.toString());
//                            showSweetAlertDialogError(global_error_message, ctx);
                    e.printStackTrace();
                }
                break;
        }
    }

    @Override
    public void onDataLoaded(String initiator, JSONArray data) {

    }

    @Override
    public void onNetworkError(String initiator, String errorCode, String errorMessage) {
        switch (initiator){
            case INITIATOR_GET_USER_NOTIFICATIONS:
                try{

                }catch (Exception e){
                    Log.e(initiator, e.toString());
                }

                break;
        }
    }

}


