package com.app.ecobba.Database

import androidx.room.*
import com.app.ecobba.Fragments.chat.data.Message
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single

@Dao
interface ChatMessagesDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertMessage(message : Message):Completable
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    @JvmSuppressWildcards
    fun insertMessages(messages: List<Message>):Completable
    @Delete
    fun deleteMessage(vararg message: Message):Completable
    @Query("SELECT * from chat_messages WHERE id IN(:ids)")
    fun getMessagesById(vararg ids : Int) : Single<List<Message>>
    @Query("SELECT * from chat_messages")
    fun getAllMessages():Single<List<Message>>
    @Query("SELECT EXISTS( SELECT * FROM chat_messages WHERE id = :id )" )
    fun exists(id : Int):Single<Boolean>
    @Query("SELECT * from chat_messages")
    fun getChatMessages():Observable<List<Message>>
}