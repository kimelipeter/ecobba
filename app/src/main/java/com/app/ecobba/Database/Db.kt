package com.app.ecobba.Database

import androidx.room.*
import com.app.ecobba.Application.MainApplication
import com.app.ecobba.Fragments.chat.data.Message
import com.app.ecobba.Fragments.chat.data.Participant
import com.google.gson.Gson

@Database(entities =[Message::class], version = 1, exportSchema = false)
@TypeConverters(Converters::class)
public abstract class Db : RoomDatabase() {
    abstract fun chatMessagesDao (): ChatMessagesDao
    companion object{
        val instance : Db  by lazy {
            Room.databaseBuilder(MainApplication.context,Db::class.java,"ecobba_db").fallbackToDestructiveMigration().allowMainThreadQueries().build()
        }
    }
}

class Converters{
    @TypeConverter
    fun stringToParticipant(participant:String) : Participant = Gson().fromJson<Participant>(participant,Participant::class.java)
    @TypeConverter
    fun participantToString(participant : Participant):String = Gson().toJson(participant)
}