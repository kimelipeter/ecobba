package com.app.ecobba.Utils

import java.io.File
import java.io.IOException

//package com.app.ecobba.Utils
//
//import android.app.Activity
//import android.widget.ImageView
//import com.github.dhaval2404.imagepicker.ImagePicker
//import java.io.File
//import java.io.IOException
//
object CommonUtils {
//    fun imagePicker(imageView: ImageView,activity: Activity) {
//
//        var image :File?=null
//
//        ImagePicker.with(activity)
//                .compress(1024)
//                .start { resultCode, data ->
//                    imageView.setImageURI(data?.data)
//                    image = ImagePicker.getFile(data)
//                }
//
////        return imageToBase64String(image)
//    }
//
    fun imageToBase64String(image: File?): String? {
        var imageString: String? = null

        try {
            var bytes = image?.readBytes()
            val base64 = android.util.Base64.encodeToString(bytes, android.util.Base64.DEFAULT)
            imageString = base64
        } catch (e: IOException) {
            e.printStackTrace()
        }

        return imageString
    }

//    fun showProfileProgress(userDetails: UserDetails): Float {
//        Log.e("PROFILE_PROGRESS", Gson().toJson(userDetails))
//
//        val fields = userDetails.javaClass.declaredFields
//        val total = fields.size
//        var filled = 0
//        for (field in fields) {
//            field.isAccessible = true
//            try {
//                val `val` = field.get(userDetails).toString()
//                if (!`val`.isEmpty()) {
//                    filled++
//                }
//            } catch (e: IllegalAccessException) {
//                e.printStackTrace()
//            }
//
//        }
//        Log.e("PROFILE_PROGRESS", "$filled/$total")
//        return (filled / total).toFloat()
//    }
//
}
