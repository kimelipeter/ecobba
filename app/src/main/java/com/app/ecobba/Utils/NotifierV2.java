package com.app.ecobba.Utils;

import android.app.Notification;
import android.content.Context;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.app.ecobba.R;

import ir.zadak.zadaknotify.notification.ZadakNotification;

public class NotifierV2 extends AppCompatActivity {

    Context ctx;
    public static String BG = "Background_Process";


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ctx=this;

    }


    public static void showNotification(Context context,
                                        String ID,
                                        String NAME,
                                        String title,
                                        String message,
                                        String content){
        ZadakNotification.with(context)
                .load()
                .notificationChannelId(ID)
                .title(title)
                .message(message)
                .bigTextStyle(content)
                .smallIcon(R.mipmap.ic_launcher)
                .largeIcon(R.drawable.ic_notification2)
                .flags(Notification.DEFAULT_ALL)
                .group(ID)
                .simple()

                .build();
    }


}
