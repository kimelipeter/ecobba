package com.app.ecobba.Utils;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;

import com.app.ecobba.R;

public class Notifier {
    String Text = "";
    Context context;
    String CHANNEL_ID;
    Intent intent;
    PendingIntent pendingIntent;
    NotificationCompat.Builder builder;
    NotificationManager notifManager;
    //    @TargetApi(Build.VERSION_CODES.O)
    public Notifier(Context context, String ID, String NAME, int IMPORTANCE){
        this.context = context;
        NotificationChannel mChannel = null;
        notifManager = (NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE);
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            mChannel = new NotificationChannel(ID, NAME, NotificationManager.IMPORTANCE_DEFAULT);
            if (notifManager.getNotificationChannel(ID)==null){
                notifManager.createNotificationChannel(mChannel);
                CHANNEL_ID = ID;
            }else {
                CHANNEL_ID = ID;
            }
        }

        builder = new NotificationCompat.Builder(context, CHANNEL_ID);
        builder.setTimeoutAfter(6000);

    }


    public Notifier setIntent(String ACTION,Intent intent,@Nullable int DRAWABLE){
        if (DRAWABLE < 0){
            DRAWABLE = android.R.drawable.sym_action_email;
        }
        pendingIntent = PendingIntent.getActivity(context, 0, intent, 0);
        builder.addAction(DRAWABLE,ACTION,pendingIntent);
        return this;
    }

    public Notifier setIntent(String ACTION,PendingIntent intent,@Nullable int DRAWABLE){
        if (DRAWABLE < 0){
            DRAWABLE = android.R.drawable.sym_action_email;
        }
//        builder.addAction(DRAWABLE,ACTION,intent);
        builder.setContentIntent(intent);
        return this;
    }

    public Notifier setMessage(String TITLE,String SUBJECT,String BODY){
        Text = TITLE;
        builder.setContentTitle(TITLE);
        builder.setContentText(SUBJECT);
        builder.setStyle(new NotificationCompat.BigTextStyle()
                .bigText(BODY));
//        builder.setStyle(new NotificationCompat.InboxStyle());

        return this;
    }

    public void show(){
        Notification notification = builder.build();
        notifManager.notify(0, notification);
        Log.e("Notification",Text);
    }

    public void show(String TITLE,String MESSAGE) {

        builder.setContentTitle(TITLE);
        builder.setContentText(MESSAGE);
        builder.setSmallIcon(R.mipmap.ic_launcher);
        builder.setLargeIcon(BitmapFactory.decodeResource(context.getResources(),
                R.mipmap.ic_launcher));
        Notification notification = builder.build();
        notifManager.notify(0, notification);
        Log.e("Notification",TITLE);
    }
}
