package com.app.ecobba.Utils;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.RecyclerView;

import com.app.ecobba.Fragments.Shop.Adapters.ShopDashboardAdapter;
import com.app.ecobba.Fragments.Shop.ConfirmDelete;
import com.app.ecobba.R;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class SwipeDetectCallback  extends ItemTouchHelper.SimpleCallback {
    private ShopDashboardAdapter adapter;
    private Drawable icon;
   // private final ColorDrawable colorDrawable;
    public  SwipeDetectCallback(ShopDashboardAdapter adapter2){
        super(0,ItemTouchHelper.LEFT|ItemTouchHelper.RIGHT);
        adapter = adapter2;
    }
    @Override
    public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
        return false;
    }

    @Override
    public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {

        int position = viewHolder.getAdapterPosition();

        ConfirmDelete confirmDelete = new ConfirmDelete(adapter,position);
        confirmDelete.show(adapter.fragment.getParentFragmentManager(),"TAG");



//        new SweetAlertDialog(adapter.context, SweetAlertDialog.WARNING_TYPE)
//                .setTitleText("Are you sure?")
//                .setContentText("Won't be able to recover this file!")
//                .setConfirmText("Yes,delete it!")
//                .setCancelText("Cancel")
//
//
//                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
//                    @Override
//                    public void onClick(SweetAlertDialog sDialog) {
//                        adapter.deleteItem(position);
//                        sDialog.dismissWithAnimation();
//                    }
//                })
//                .setCancelClickListener( new SweetAlertDialog.OnSweetClickListener() {
//                    @Override
//                    public void onClick(SweetAlertDialog sDialog) {
//                        adapter.undodelete(position,adapter.dataList.get(position));
//                        sDialog.dismissWithAnimation();
//                    }
//                })
//                .show();

    }
//    public  SwipeDetectCallback(ShopDashboardAdapter shopDashboardAdapter){
//        super(0,ItemTouchHelper.LEFT|ItemTouchHelper.RIGHT);
//        adapter = shopDashboardAdapter;
//        icon = ContextCompat.getDrawable(adapter.context,R.drawable.account_avatar);
//        colorDrawable = new ColorDrawable(Color.RED);
//    }
}
