package com.app.ecobba.Utils

import android.content.Context
import android.util.Log
import androidx.work.*
import com.app.ecobba.Application.ChatWorkManager
import com.app.ecobba.Application.MainApplication
import java.io.InputStream
import java.io.OutputStream
import java.util.concurrent.TimeUnit


const val CHAT_WORK_TAG = "chat_worker"


public fun InputStream.copyTo(out: OutputStream, bufferSize: Int = DEFAULT_BUFFER_SIZE): Long {
    var bytesCopied: Long = 0
    val buffer = ByteArray(bufferSize)
    var bytes = read(buffer)
    while (bytes >= 0) {
        out.write(buffer, 0, bytes)
        bytesCopied += bytes
        bytes = read(buffer)
    }
    return bytesCopied
}

fun Context.startWorker() {

    Log.d("WorkerChat", "Worker Called")

    val worker =
            PeriodicWorkRequestBuilder<ChatWorkManager>(
                    15, TimeUnit.MINUTES
            ).addTag(CHAT_WORK_TAG)
                    .build()
    WorkManager
            .getInstance(this)
            .enqueueUniquePeriodicWork(
                    CHAT_WORK_TAG,
                    ExistingPeriodicWorkPolicy.KEEP,
                    worker
            )

}

fun Context.startInitialWorker() {
    Log.d("WorkerChat", "Initial Called")
    val asSoonAsPossibleWorkRequest = OneTimeWorkRequestBuilder<InitialWorker>()
            .addTag("InitialWorker")
            .build()

    WorkManager.getInstance(this).enqueue(asSoonAsPossibleWorkRequest)
}

class InitialWorker(val context: Context, params: WorkerParameters) : CoroutineWorker(context, params) {
    override suspend fun doWork(): Result {

        context.startWorker()
        Log.d("WorkerChat", "Initial Started")

        MainApplication.chatViewModel!!.getMessage()

        return Result.success()
    }


}

