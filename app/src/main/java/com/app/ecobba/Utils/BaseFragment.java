package com.app.ecobba.Utils;

import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.app.ecobba.R;
import com.google.android.material.appbar.AppBarLayout;

import java.util.Locale;

import butterknife.BindView;
import butterknife.Unbinder;

public class BaseFragment extends Fragment implements AppBarLayout.OnOffsetChangedListener{

    @Nullable
    @BindView(R.id.ablProfile)
    public AppBarLayout ablProfile;


    public Unbinder unbinder;

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        Locale local = LocalesUtils.getCurrentLocale(getContext());
        Toast.makeText(getContext(),local.getDisplayLanguage(),Toast.LENGTH_SHORT).show();
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {

        if (ablProfile!=null) {
            ablProfile.addOnOffsetChangedListener(this);
        }else{
            Log.e("BUTTER_NULL","APPBAR NOT FOUND");
        }
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (unbinder!=null) {
            unbinder.unbind();
        }
    }

    int val = 0;
    boolean transparent = true;

    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int i) {

        if (i<val){
            if (transparent) {
                ((View) getActivity().findViewById(R.id.toolbar)).setBackgroundResource(R.color.colorSurface);
                transparent = false;
            }
        }else if(i > val){

            if (!transparent) {
                ((View) getActivity().findViewById(R.id.toolbar)).setBackgroundResource(R.color.colorSurfaceAlpha);
                transparent = true;
            }
        }

        val = i;
    }


}
