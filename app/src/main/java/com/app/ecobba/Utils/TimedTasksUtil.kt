package com.app.ecobba.Utils

import android.app.AlarmManager
import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import com.app.ecobba.Application.MainApplication
import com.app.ecobba.Fragments.chat.ChatViewModel

/**
 * This class will handle initiation of periodic tasks with Alarm manager
 */

class TimedTasksUtil {

    /**
     * setup alarm utils
     * intent on boot
     * intent on wake lock
     * setup Alarm manager Period 1 minute ( too frequent )
     */

    val context : Context by lazy { MainApplication.context }

    val PERIOD = 1000;
    val CHAT_REQUEST_CODE = 1111

    val alarmManager : AlarmManager by lazy {
        context.getSystemService(Context.ALARM_SERVICE) as AlarmManager
    }

    val timedPendingIntent : PendingIntent by lazy {
        PendingIntent.getBroadcast(context,CHAT_REQUEST_CODE, Intent(context, TimedBroadCastReciever::class.java),0)
    }


    init {
        setupAlarmManager()
    }

    fun setupAlarmManager(){
        alarmManager.setInexactRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP,0,PERIOD.toLong(),timedPendingIntent)
    }

}

class TimedBroadCastReciever : BroadcastReceiver(){

    val chatViewModel : ChatViewModel by lazy {
        MainApplication.chatViewModel!!
    }

    override fun onReceive(p0: Context?, p1: Intent?) {
        /**
         *  Do network call here
         */

        chatViewModel.getMessage()

    }

}
