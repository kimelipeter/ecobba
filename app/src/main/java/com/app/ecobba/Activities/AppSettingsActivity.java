package com.app.ecobba.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import com.app.ecobba.R;
import com.app.ecobba.common.ApisKtKt;

public class AppSettingsActivity extends AppCompatActivity {

LinearLayout privacyPolicy,lLTerms,lLDisclaimer;
    String fagsUrl = (String) ApisKtKt.getFags;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_app_settings);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_baseline_arrow_back_24);
        toolbar.setTitle(R.string.help);
        toolbar.setTitleTextColor(getResources().getColor(R.color.primaryColor));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        privacyPolicy=(LinearLayout) findViewById(R.id.privacyPolicy);
        lLTerms=(LinearLayout) findViewById(R.id.lLTerms);
        lLDisclaimer=(LinearLayout) findViewById(R.id.lLDisclaimer);

        privacyPolicy.setOnClickListener((view ->{
            Intent intent=new Intent(this, PrivacyPolicyActivity.class);
            startActivity(intent);
        }));
        lLTerms.setOnClickListener((v -> {
            startActivity(new Intent(this,TermsAndConditionsActivity.class));
        }));
        lLDisclaimer.setOnClickListener((v -> {
            startActivity(new Intent(this,DisclaimerActivity.class));
        }));


    }
}