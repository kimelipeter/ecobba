package com.app.ecobba.Activities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.app.ecobba.R;
import com.app.ecobba.common.SharedMethods;
import com.google.android.material.card.MaterialCardView;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;
import com.jaeger.library.StatusBarUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;

import static com.app.ecobba.common.ApisKtKt.login;
import static com.app.ecobba.common.ApisKtKt.recover;
import static com.app.ecobba.common.AppConstantsKt.INITIATOR_LOGIN;
import static com.app.ecobba.common.AppConstantsKt.INITIATOR_RECOVER;
import static com.app.ecobba.common.SharedMethods.HTTP_POST;
import static com.app.ecobba.common.SharedMethods.getDefaults;
import static com.app.ecobba.common.SharedMethods.global_error_message;
import static com.app.ecobba.common.SharedMethods.hideDialog;
import static com.app.ecobba.common.SharedMethods.parseMessage;
import static com.app.ecobba.common.SharedMethods.postToServer;
import static com.app.ecobba.common.SharedMethods.runOnUI;
import static com.app.ecobba.common.SharedMethods.showSweetAlertDialogError;
import static com.app.ecobba.common.SharedMethods.showSweetAlertDialogSuccess;
import static com.app.ecobba.common.SharedMethods.storePrefs;

public class LoginActivity extends AppCompatActivity
        implements View.OnClickListener , NavigationView.OnNavigationItemSelectedListener,
        SharedMethods.NetworkObjectListener
        {

    @BindView(R.id.btnLogin)    Button btnLogin;
    @BindView(R.id.btnRegister)    TextView btnRegister;
    @BindView(R.id.btnForgotPassword)  TextView btnForgotPassword;
    @BindView(R.id.etPassword)    EditText etPassword;
    @BindView(R.id.etEmailPhoneNumber)    EditText etEmailPhoneNumber;
    @BindView(R.id.btnRecover) Button btnRecover;
    @BindViews({R.id.login_card,R.id.recover_card}) List<MaterialCardView> mode;
    @BindView(R.id.cbRemeber) CheckBox rememberMe;

    @BindView(R.id.etEmailPhoneNumberRec)
            TextInputEditText tiEmailRecov;


    String passwordToApi,emailPhoneToApi;
    Context ctx;
    private int MODE = 0;
    String email="" ,msdisn="";

    @Override
    public void onCreate(Bundle savedInstanceState) {
//        LocaleChanger.setLocale(new Locale("sw"));
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        //start of back arrow
        androidx.appcompat.widget.Toolbar toolbar = (androidx.appcompat.widget.Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_baseline_arrow_back_24);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        //end of back arrow
        if (!isConnected(LoginActivity.this)) builderDialog(LoginActivity.this).show();

        else {
            Toast toast = Toast.makeText(getApplicationContext(),
                    "Welcome",
                    Toast.LENGTH_SHORT);

            //toast.show();
        }
        StatusBarUtil.setTransparent(this);
        ButterKnife.bind(this);

        ctx = this;

        //local code//

        String token = getDefaults("token",ctx);
        String remember = getDefaults("remember",ctx);

        if (token !=null && remember !=null){
            startActivity(new Intent(ctx,MainActivity.class));
            finish();
        }

        //***********//
        btnRecover.setOnClickListener(this);
        btnLogin.setOnClickListener(this);
        btnRegister.setOnClickListener(this);
        btnForgotPassword.setOnClickListener(this);
        rememberMe.setOnCheckedChangeListener((compoundButton, b) -> {
            if (b){
                SharedMethods.storePrefs("remember","yes",ctx);
            }else{
                SharedMethods.deletePrefs("remember",ctx);
            }
        });

        loginMode(MODE);
    }


    private void loginMode(int n){
        MODE = n;
        if (n == 0){
            mode.get(0).setVisibility(View.VISIBLE);
            mode.get(1).setVisibility(View.GONE);
        }else if (n == 1){
            mode.get(1).setVisibility(View.VISIBLE);
            mode.get(0).setVisibility(View.GONE);
            tiEmailRecov.setText(etEmailPhoneNumber.getText());
        }
    }

    @Override
    public void onBackPressed() {
        if (MODE == 1){
            loginMode(0);
        }else {
            super.onBackPressed();
        }
    }

            @Override
            public boolean onCreateOptionsMenu(Menu menu) {
                MenuInflater inflater = getMenuInflater();
                inflater.inflate(R.menu.menu_features, menu);
                return true;
            }

        //    @Override
            public boolean showMsgDirectMenuXml(MenuItem item) {
                // Handle item selection
                switch (item.getItemId()) {
                    case R.id.action_forgot_password:
                       startActivity(new Intent(this,ForgotPasswordActivity.class));
                        String password_link="Forgot password  item";
                        Log.e("item_cliked",password_link);
                        return true;

                    default:
                        return super.onOptionsItemSelected(item);
                }
            }
                @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btnLogin:
                if (validateFormFields()) {
                    if (isConnected(ctx)) {
                        //form is valid and there is internet connection, so proceed to login
                        loginToServer();
                    } else {
                        Snackbar.make(view,getString(R.string.no_internet),Snackbar.LENGTH_SHORT).show();
//                        Toast.makeText(this, "INTERNET NOT AVAILABLE", Toast.LENGTH_SHORT).show();
                    }
                }
                break;
            case R.id.btnRegister:
                startActivity(new Intent(this,RegistrationActivity.class));
                break;
            case R.id.btnForgotPassword:
                loginMode(1);
                break;
            case R.id.btnRecover:
                    recoverPassword();
                break;
        }
    }

    private boolean validateFormFields(){
        boolean valid = true;
        passwordToApi = etPassword.getText().toString();
        emailPhoneToApi = etEmailPhoneNumber.getText().toString();

        if (emailPhoneToApi.contains("@"))
        {
            email=emailPhoneToApi;
        }
        else {
           msdisn=emailPhoneToApi;
        }

        // Password validation
        if (passwordToApi.isEmpty()) {
            etPassword.setError(getString(R.string.emptyPassword));
            valid = false;
        }else if ( passwordToApi.length() <  4 || passwordToApi.length() >20  ) {
            etPassword.setError(getString(R.string.passwordInvalidLength));
            valid = false;
        }else{
//            etPassword.setError(getString(R.string.emptyPassword));
        }
//R.string.phoneEmailInvalidLength
        // Phone number validation
        if (emailPhoneToApi.isEmpty() || emailPhoneToApi.length() < 10 || emailPhoneToApi.length() > 100) {
            etEmailPhoneNumber.setError(getString(R.string.phoneEmailInvalidLength));
            valid = false;
        } else {
//            etEmailPhoneNumber.setError(getString(R.string.emptyPhoneEmail));
        }

        return valid;
    }

    private void loginToServer(){
//        showProgressBarDialog(ctx);

        String params = "{ \"email\":" + '"' + email + '"' +
                ", \"msisdn\": " + '"' + msdisn + '"' +
                ", \"password\": " + '"' + passwordToApi + '"' + "}\n" + "\n";
        Log.e("LOGIN_POST_PARAMS",params);

        postToServer( login ,"", params , INITIATOR_LOGIN , HTTP_POST , ctx ,this);
    }

    private void recoverPassword(){
        String email = tiEmailRecov.getText().toString();
            if (email.isEmpty()){
                tiEmailRecov.setError(getResources().getString(R.string.emptyField));
                return;
        }else{
            String params = "{ \"email\":" + '"' + email + '"' +
                    "}\n" + "\n";

            postToServer( recover ,"", params , INITIATOR_RECOVER , HTTP_POST , ctx ,this);

        }

    }

    @Override
    public void onDataReady(String initiator, final String response) {
        switch (initiator) {
            case INITIATOR_LOGIN:
                try {
                    runOnUI(new Runnable() {
                        @Override
                        public void run() {
                            Log.e("LOGIN RESPONSE", "Response is " + response);
                            hideDialog(ctx);
                            JSONObject jObj = null;
                            try {
                                jObj = new JSONObject(response);
                                if (jObj.has("errors")) {
                                    String errors = parseMessage(jObj.getString("errors"));
                                    showSweetAlertDialogError(errors, ctx);
                                } else { // if no error message was found in the response,
                                    boolean success = jObj.getBoolean("success");
                                    if (success) { // if the request was successfuls, got forward, else show error message
                                        JSONObject jObjUser = jObj.getJSONObject("user");
                                        JSONArray jArrayUserRoles = jObjUser.getJSONArray("roles");
                                        JSONObject jObjUserRole = jArrayUserRoles.getJSONObject(0);
                                        String userRole = jObjUserRole.getString("id");
                                        String msisdn = jObjUser.getString("msisdn");
                                        storePrefs("msisdn", msisdn, ctx);
                                        storePrefs("role", userRole, ctx);
                                        String token = jObj.getString("token");
                                        storePrefs("token", token, ctx);
                                        startActivity(new Intent(ctx,MainActivity.class));
                                        finish();
                                    } else {
                                        String api_error_message = jObj.getString("message");
                                        showSweetAlertDialogError(api_error_message, ctx);
                                    }
                                }

                            } catch (JSONException e) {
                                Log.e("ExceptionLOGIN-DATA", e.toString());
                                showSweetAlertDialogError(global_error_message, ctx);
                                e.printStackTrace();
                            }
                        }
                    });
                } catch (Exception e) {
//                            hideDialog(ctx);
                    Log.e("Exception- LOGIN", e.toString());
                    showSweetAlertDialogError(global_error_message, ctx);
//                    Toast.makeText(ctx, "Login Data Response Exception" + e.toString(), Toast.LENGTH_LONG).show();
                }
                break;
            case INITIATOR_RECOVER:
                try {
                    runOnUI(new Runnable() {
                        @Override
                        public void run() {
                            Log.e("INITIATOR_RECOVER", "Response is " + response);
                            hideDialog(ctx);
                            JSONObject jObj = null;
                            try {
                                jObj = new JSONObject(response);
                                if (jObj.has("errors")) {
                                    String errors = jObj.getString("errors");
                                    showSweetAlertDialogError(errors, ctx);
                                } else { // if no error message was found in the response,
                                    boolean success = jObj.getBoolean("success");
                                    if (success) { // if the request was successfuls, got forward, else show error message
                                        String api_message = jObj.getString("message");
                                        showSweetAlertDialogSuccess(api_message,ctx);
                                    } else {
                                        String api_error_message = jObj.getString("message");
                                        showSweetAlertDialogError(api_error_message, ctx);
                                    }
                                }

                            } catch (JSONException e) {
                                Log.e("ExceptionLOGIN-DATA", e.toString());
                                showSweetAlertDialogError(global_error_message, ctx);
                                e.printStackTrace();
                            }
                        }
                    });
                } catch (Exception e) {
//                            hideDialog(ctx);
                    Log.e("Exception- LOGIN", e.toString());
                    showSweetAlertDialogError(global_error_message, ctx);
//                    Toast.makeText(ctx, "Login Data Response Exception" + e.toString(), Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

    @Override
    public void onDataLoaded(String initiator, JSONArray data) {

    }

    @Override
    public void onNetworkError(String initiator, final String errorCode, final String errorMessage) {
        switch (initiator) {
            case INITIATOR_RECOVER:
                try{
                    runOnUI(new Runnable() {
                        @Override
                        public void run() {
                            hideDialog(ctx);
                            showSweetAlertDialogError(errorMessage, ctx);
                        }
                    });
                }catch (Exception e){
                    hideDialog(ctx);
                    showSweetAlertDialogError(global_error_message, ctx);
                    Log.e("Exception- LOGIN", e.toString());

//                    Toast.makeText(ctx, "Login Error Response Exception" + e.toString(), Toast.LENGTH_LONG).show();
                }
                break;
            case INITIATOR_LOGIN:
                try{
                    runOnUI(new Runnable() {
                        @Override
                        public void run() {
                            hideDialog(ctx);
                            showSweetAlertDialogError(errorMessage, ctx);
                        }
                    });
                }catch (Exception e){
                    hideDialog(ctx);
                    showSweetAlertDialogError(global_error_message, ctx);
                    Log.e("Exception- LOGIN", e.toString());

//                    Toast.makeText(ctx, "Login Error Response Exception" + e.toString(), Toast.LENGTH_LONG).show();
                }

                break;
        }

    }
            public boolean isConnected(Context context){
                ConnectivityManager cm=(ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo networkInfo=cm.getActiveNetworkInfo();
                if (networkInfo !=null && networkInfo.isConnectedOrConnecting()){
                    android.net.NetworkInfo wifi=cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
                    android.net.NetworkInfo mobile=cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

                    if ((mobile !=null && mobile.isConnectedOrConnecting()) ||
                            (wifi !=null && wifi.isConnectedOrConnecting() )) return true;
                    else return false;
                }

                else return false;
            }


            public AlertDialog.Builder builderDialog(Context c){
                AlertDialog.Builder builder = new AlertDialog.Builder(c);
                builder.setTitle("No Internet Connection!");
                builder.setMessage("Check and try Again!!!");
                builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        finish();
                    }
                });
                return builder;
            }


            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                return false;
            }

            @Override
            public void onPointerCaptureChanged(boolean hasCapture) {

            }
        }
