package com.app.ecobba.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.app.ecobba.R;

import static com.app.ecobba.common.SharedMethods.getDefaults;

public class WelcomeActivity extends AppCompatActivity {
    private Button btnLogin,btnCreateAccount;
    Context ctx;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);

        btnLogin =(Button) findViewById(R.id.btnLogin);
        btnCreateAccount=(Button) findViewById(R.id.btnCreateAccount);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent login=new Intent(WelcomeActivity.this,LoginActivity.class);
                startActivity(login);
            }
        });
        btnCreateAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent createAccount=new Intent(WelcomeActivity.this,RegistrationActivity.class);
                startActivity(createAccount);
            }
        });

        //local code//

        String token = getDefaults("token",ctx);
        String remember = getDefaults("remember",ctx);

        if (token !=null && remember !=null ){
            startActivity(new Intent(this,MainActivity.class));
            finish();
//            Intent intent=new Intent(WelcomeActivity.this,MainActivity.class);
//            startActivity(intent);
        }
    }


}