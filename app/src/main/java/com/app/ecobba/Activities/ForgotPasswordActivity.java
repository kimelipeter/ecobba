package com.app.ecobba.Activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.app.ecobba.R;
import com.app.ecobba.common.SharedMethods;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.textfield.TextInputEditText;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.app.ecobba.common.ApisKtKt.recover;
import static com.app.ecobba.common.AppConstantsKt.INITIATOR_RECOVER;
import static com.app.ecobba.common.SharedMethods.HTTP_POST;
import static com.app.ecobba.common.SharedMethods.getDefaults;
import static com.app.ecobba.common.SharedMethods.global_error_message;
import static com.app.ecobba.common.SharedMethods.hideDialog;
import static com.app.ecobba.common.SharedMethods.postToServer;
import static com.app.ecobba.common.SharedMethods.runOnUI;
import static com.app.ecobba.common.SharedMethods.showSweetAlertDialogError;
import static com.app.ecobba.common.SharedMethods.showSweetAlertDialogSuccess;

public class ForgotPasswordActivity extends AppCompatActivity implements View.OnClickListener , NavigationView.OnNavigationItemSelectedListener,
        SharedMethods.NetworkObjectListener{
    @BindView(R.id.etEmailPhoneNumberRec)

    TextInputEditText tiEmailRecov;
    @BindView(R.id.btnRecover) Button btnRecover;
    Context ctx;
    String emailPhoneToApi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_baseline_arrow_back_24);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        ButterKnife.bind(this);
        btnRecover.setOnClickListener(this);
        String token = getDefaults("token",ctx);
        String remember = getDefaults("remember",ctx);

        if (token !=null && remember !=null){
            startActivity(new Intent(ctx,MainActivity.class));
            finish();
        }
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btnRecover:
                recoverPassword();
                break;
        }

    }
    private void recoverPassword(){
        String email = tiEmailRecov.getText().toString();
        if (email.isEmpty()){
            tiEmailRecov.setError(getResources().getString(R.string.emptyField));
            return;
        }else{
            String params = "{ \"email\":" + '"' + email + '"' +
                    "}\n" + "\n";

            postToServer( recover ,"", params , INITIATOR_RECOVER , HTTP_POST , ctx ,this);

        }

    }
    private boolean validateFormFields(){
        boolean valid = true;
        emailPhoneToApi = tiEmailRecov.getText().toString();

//R.string.phoneEmailInvalidLength
        // Phone number validation
        if (emailPhoneToApi.isEmpty() || emailPhoneToApi.length() < 10 || emailPhoneToApi.length() > 100) {
            tiEmailRecov.setError(getString(R.string.phoneEmailInvalidLength));
            valid = false;
        } else {
//            etEmailPhoneNumber.setError(getString(R.string.emptyPhoneEmail));
        }

        return valid;
    }

    @Override
    public void onDataReady(String initiator, String response) {
        switch (initiator){
            case INITIATOR_RECOVER:
                try {
                    runOnUI(new Runnable() {
                        @Override
                        public void run() {
                            Log.e("INITIATOR_RECOVER", "Response is " + response);
                            hideDialog(ctx);
                            JSONObject jObj = null;
                            try {
                                jObj = new JSONObject(response);
                                if (jObj.has("errors")) {
                                    String errors = jObj.getString("errors");
                                    showSweetAlertDialogError(errors, ctx);
                                } else { // if no error message was found in the response,
                                    boolean success = jObj.getBoolean("success");
                                    if (success) { // if the request was successfuls, got forward, else show error message
                                        String api_message = jObj.getString("message");
                                        showSweetAlertDialogSuccess(api_message,ctx);
                                    } else {
                                        String api_error_message = jObj.getString("message");
                                        showSweetAlertDialogError(api_error_message, ctx);
                                    }
                                }

                            } catch (JSONException e) {
                                Log.e("ExceptionLOGIN-DATA", e.toString());
                                showSweetAlertDialogError(global_error_message, ctx);
                                e.printStackTrace();
                            }
                        }
                    });
                } catch (Exception e) {
//                            hideDialog(ctx);
                    Log.e("Exception- LOGIN", e.toString());
                    showSweetAlertDialogError(global_error_message, ctx);
//                    Toast.makeText(ctx, "Login Data Response Exception" + e.toString(), Toast.LENGTH_LONG).show();
                }
                break;
        }

    }

    @Override
    public void onDataLoaded(String initiator, JSONArray data) {

    }

    @Override
    public void onNetworkError(String initiator, String errorCode, String errorMessage) {
        switch (initiator){
            case INITIATOR_RECOVER:
                try{
                    runOnUI(new Runnable() {
                        @Override
                        public void run() {
                            hideDialog(ctx);
                            showSweetAlertDialogError(errorMessage, ctx);
                        }
                    });
                }catch (Exception e){
                    hideDialog(ctx);
                    showSweetAlertDialogError(global_error_message, ctx);
                    Log.e("Exception- LOGIN", e.toString());

//                    Toast.makeText(ctx, "Login Error Response Exception" + e.toString(), Toast.LENGTH_LONG).show();
                }
                break;
        }

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        return false;
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }
}