package com.app.ecobba.Activities;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.util.Patterns;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.app.ecobba.R;
import com.google.android.material.textfield.TextInputLayout;

import java.io.File;
import java.util.List;

public class SupportActivity extends AppCompatActivity {
    /* access modifiers changed from: private */
    public ProgressDialog register_progress;
    private Button support_button;
    /* access modifiers changed from: private */
    public EditText support_input_email;
    private TextInputLayout support_input_layout_email;
    private TextInputLayout support_input_layout_message;
    private TextInputLayout support_input_layout_name;
    private TextInputLayout support_input_layout_subject;
    /* access modifiers changed from: private */
    public EditText support_input_message;
    /* access modifiers changed from: private */
    public EditText support_input_name;
    public EditText support_input_subject;
    public String emailToSendTo;
    public String emailToSendTo2;

    private class SupportTextWatcher implements TextWatcher {
        private View view;

        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        private SupportTextWatcher(View view2) {
            this.view = view2;
        }

        public void afterTextChanged(Editable editable) {
            int id = this.view.getId();
            if (id != R.id.support_input_email) {
                switch (id) {
                    case R.id.support_input_message /*2131296819*/:
                        SupportActivity.this.validatMessage();
                        return;
                    case R.id.support_input_name /*2131296820*/:
                        SupportActivity.this.validatName();
                        return;
                    case R.id.support_input_subject /*2131296820*/:
                        SupportActivity.this.validatSubject();
                        return;
                    default:
                        return;
                }
            } else {
                SupportActivity.this.validateEmail();
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_support);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle((CharSequence) getResources().getString(R.string.help_center));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        initView();
        initAction();
    }

    public void onBackPressed() {
        super.onBackPressed();
        /* changed to intent from overridePendingTransition below */
        Intent intent=new Intent(SupportActivity.this,MainActivity.class);
        startActivity(intent);
        //overridePendingTransition(R.anim.back_enter, R.anim.back_exit);
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() != 16908332) {
            return super.onOptionsItemSelected(menuItem);
        }
        super.onBackPressed();
        /* changed to intent from overridePendingTransition below */
        Intent intent=new Intent(SupportActivity.this,MainActivity.class);
        startActivity(intent);
       // overridePendingTransition(R.anim.back_enter, R.anim.back_exit);
        return true;
    }

    public void initView() {
        this.support_input_email = (EditText) findViewById(R.id.support_input_email);
        this.support_input_message = (EditText) findViewById(R.id.support_input_message);
        this.support_input_name = (EditText) findViewById(R.id.support_input_name);
        this.support_input_subject=(EditText)findViewById(R.id.support_input_subject);
        this.support_input_layout_email = (TextInputLayout) findViewById(R.id.support_input_layout_email);
        this.support_input_layout_message = (TextInputLayout) findViewById(R.id.support_input_layout_message);
        this.support_input_layout_name = (TextInputLayout) findViewById(R.id.support_input_layout_name);
        this.support_input_layout_subject=(TextInputLayout) findViewById(R.id.support_input_layout_subject);
        this.support_button = (Button) findViewById(R.id.support_button);
    }

    public void initAction() {
        this.support_input_email.addTextChangedListener(new SupportTextWatcher(this.support_input_email));
        this.support_input_name.addTextChangedListener(new SupportTextWatcher(this.support_input_name));
        this.support_input_subject.addTextChangedListener(new SupportTextWatcher(this.support_input_subject));
        this.support_input_message.addTextChangedListener(new SupportTextWatcher(this.support_input_message));
        this.support_button.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                SupportActivity.this.submit();
            }
        });
    }

    @SuppressLint("IntentReset")
    public void submit() {

        //emailToSendTo2="info@ecobba.com";
        if (validatName() && validateEmail() &&validatSubject() && validatMessage() ) {

            String fullname = this.support_input_name.getText().toString().trim();
            String user_email = this.support_input_email.getText().toString().trim();
            String mail_subject = this.support_input_subject.getText().toString().trim();
            String mail_body = this.support_input_message.getText().toString().trim();
            String fullmessage =getString(R.string.hi)+getString(R.string.my_name)+" "+ fullname +" \n " + mail_body;
            emailToSendTo="info@ecobba.com";

            Log.e("Full Name is :",fullname);
            Log.e("Email is :",user_email);
            Log.e("Email To is",emailToSendTo);
            Log.e("Subject is :",mail_subject);
            Log.e("Message is :",fullmessage);
            if (emailToSendTo !=null){

                final Intent Email = new Intent(
                        android.content.Intent.ACTION_SEND);
                Email.setType("*/*");
                Email.putExtra(android.content.Intent.EXTRA_SUBJECT,
                        mail_subject);
                Email.putExtra(Intent.EXTRA_EMAIL, new String[] { "info@ecobba.com" });
                Email.putExtra(Intent.EXTRA_SUBJECT, mail_subject);
                Email.putExtra(android.content.Intent.EXTRA_TEXT,fullmessage);




                final PackageManager pm = getPackageManager();
                final List<ResolveInfo> matches = pm.queryIntentActivities(
                        Email, 0);
                ResolveInfo best = null;
                for (final ResolveInfo info : matches)
                    if (info.activityInfo.packageName.endsWith(".gm")
                            || info.activityInfo.name.toLowerCase()
                            .contains("gmail"))
                        best = info;
                if (best != null)
                    Email.setClassName(best.activityInfo.packageName,
                            best.activityInfo.name);
                startActivity(Email);

            }


        }
    }

    /* access modifiers changed from: private */
    public boolean validatName() {
        if (this.support_input_name.getText().toString().trim().isEmpty() || this.support_input_name.getText().length() < 3) {
            this.support_input_layout_name.setError(getString(R.string.error_short_value));
            requestFocus(this.support_input_name);
            return false;
        }
        this.support_input_layout_name.setErrorEnabled(false);
        return true;
    }
    /* access modifiers changed from: private */
    public boolean validatSubject() {
        if (this.support_input_subject.getText().toString().trim().isEmpty() || this.support_input_subject.getText().length() < 3) {
            this.support_input_layout_subject.setError(getString(R.string.error_short_value));
            requestFocus(this.support_input_subject);
            return false;
        }
        this.support_input_layout_subject.setErrorEnabled(false);
        return true;
    }

    /* access modifiers changed from: private */
    public boolean validatMessage() {
        if (this.support_input_message.getText().toString().trim().isEmpty() || this.support_input_message.getText().length() < 3) {
            this.support_input_layout_message.setError(getString(R.string.error_short_value));
            requestFocus(this.support_input_message);
            return false;
        }
        this.support_input_layout_message.setErrorEnabled(false);
        return true;
    }

    private static boolean isValidEmail(String str) {
        return !TextUtils.isEmpty(str) && Patterns.EMAIL_ADDRESS.matcher(str).matches();
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(5);
        }
    }

    /* access modifiers changed from: private */
    public boolean validateEmail() {
        String trim = this.support_input_email.getText().toString().trim();
        if (trim.isEmpty() || !isValidEmail(trim)) {
            this.support_input_layout_email.setError(getString(R.string.error_mail_valide));
            requestFocus(this.support_input_email);
            return false;
        }
        this.support_input_layout_email.setErrorEnabled(false);
        return true;
    }
}
