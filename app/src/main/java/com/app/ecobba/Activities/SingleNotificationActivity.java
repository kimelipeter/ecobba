package com.app.ecobba.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.app.ecobba.R;

public class SingleNotificationActivity extends AppCompatActivity {
  public   String test;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_notification);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_baseline_arrow_back_24);
        toolbar.setTitle(R.string.notification_details);
        toolbar.setTitleTextColor(getResources().getColor(R.color.primaryColor));

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        //Recieve Intent Data date_borrowed
        String loan_name = getIntent().getExtras().getString("loan_name_title");
        String loan_amount = getIntent().getExtras().getString("loan_amount");
        String date_borrowed = getIntent().getExtras().getString("date_borrowed");
        Log.e("DATE_BORROW",date_borrowed);
//        String loan_char = getIntent().getExtras().getString("loan_char");
        Log.e("loan_char",loan_name);
        Log.e("date_borrowed_is",date_borrowed);
        Log.e("LOANDETAILS",loan_name +" "+ loan_amount+" "+date_borrowed);
        String name=loan_name;
        char ch=name.charAt(1);//returns the char value at the 1st index
        Log.e("First_letter_Name_is", String.valueOf(ch));


        //ini views
        TextView tv_loan_name = findViewById(R.id.tvLoanName);
        TextView tv_loan_amount= findViewById(R.id.tv_loan_amount);
        TextView tv_date_borrowed = findViewById(R.id.tv_date_borrowed);
        TextView tv_loan_char = findViewById(R.id.loan_char);


        // setting values to each view
        tv_loan_name.setText(loan_name);
        tv_loan_amount.setText(loan_amount);
        tv_date_borrowed.setText(date_borrowed);
        tv_loan_char.setText(String.valueOf(ch));
    }
}