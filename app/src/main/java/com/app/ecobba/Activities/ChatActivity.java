package com.app.ecobba.Activities;

import android.os.Bundle;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;
import androidx.navigation.Navigation;

import com.app.ecobba.Application.MainApplication;
import com.app.ecobba.Fragments.chat.ChatViewModel;
import com.app.ecobba.R;

public class ChatActivity extends AppCompatActivity {

    ChatViewModel chatViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        chatViewModel = MainApplication.Companion.getChatViewModel();

        String idStr = getIntent().getStringExtra("OTHER_PARTY");
        int otherParty = (idStr == null || idStr.isEmpty()) ? -1 : Integer.parseInt(idStr);
        Log.e("other_party_id",String.valueOf(otherParty));
        if (otherParty != -1){
            Bundle args = new Bundle();
            args.putInt("OTHER_PARTY",otherParty);
            Navigation.findNavController(this,R.id.fragment).navigate(R.id.chatMessagesFragment,args);
        }


        /**
         * .navigate(R.id.chatMessagesFragment,Bundle().let {
         *                             it.putInt("OTHER_PARTY",datum?.id ?: 0)
         *                             it
         *                         })
         */


//        setupToolbar();
    }

//    public void setupToolbar() {
//        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);
//        final ActionBar actionBar = getSupportActionBar();
//        if (actionBar != null) {
//            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//            getSupportActionBar().setHomeButtonEnabled(true);
//            getSupportActionBar().setTitle("Peter");
//
//
//        }
//
//        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                finish();
//            }
//        });
//    }
}

