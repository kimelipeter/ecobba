package com.app.ecobba.Activities;

import android.Manifest;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.FileUtils;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.app.ecobba.Dialogs.ChangeLanguageDialog;
import com.app.ecobba.Models.LevelOne;
import com.app.ecobba.Models.RegionDetail;
import com.app.ecobba.R;
import com.app.ecobba.common.SharedMethods;
import com.google.gson.Gson;
import com.jaeger.library.StatusBarUtil;
import com.jaredrummler.materialspinner.MaterialSpinner;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import in.mayanknagwanshi.imagepicker.ImageSelectActivity;

import static com.app.ecobba.common.ApisKtKt.getLevelThreeData;
import static com.app.ecobba.common.ApisKtKt.getLevelTwoData;
import static com.app.ecobba.common.ApisKtKt.getRegisterData;
import static com.app.ecobba.common.ApisKtKt.getRegistrationData;
import static com.app.ecobba.common.ApisKtKt.register;
import static com.app.ecobba.common.AppConstantsKt.INITIATOR_CREATE_GROUP_MEMBER;
import static com.app.ecobba.common.AppConstantsKt.INITIATOR_GETERGISTER_DATA_REG;
import static com.app.ecobba.common.AppConstantsKt.INITIATOR_GET_GROUP_REGISTRATION_DATA;
import static com.app.ecobba.common.AppConstantsKt.INITIATOR_GET_LEVEL_ONE_DATA;
import static com.app.ecobba.common.AppConstantsKt.INITIATOR_GET_LEVEL_THREE_DATA;
import static com.app.ecobba.common.AppConstantsKt.INITIATOR_GET_LEVEL_TWO_DATA;
import static com.app.ecobba.common.AppConstantsKt.INITIATOR_LOGIN;
import static com.app.ecobba.common.SharedMethods.HTTP_GET;
import static com.app.ecobba.common.SharedMethods.HTTP_POST;
import static com.app.ecobba.common.SharedMethods.getFromServer;
import static com.app.ecobba.common.SharedMethods.global_error_message;
import static com.app.ecobba.common.SharedMethods.hideDialog;
import static com.app.ecobba.common.SharedMethods.postToServer;
import static com.app.ecobba.common.SharedMethods.runOnUI;
import static com.app.ecobba.common.SharedMethods.showSweetAlertDialogError;


public class RegistrationActivity extends AppCompatActivity
        implements SharedMethods.NetworkObjectListener,
        View.OnClickListener,
        MaterialSpinner.OnItemSelectedListener {

    @BindView(R.id.regVf) ViewFlipper viewFlipper;
    @BindViews({R.id.back,
            R.id.next,
            R.id.etConfirm,
            R.id.btnFullReg,
//            R.id.btCalender,
            R.id.btFinish,
     //       R.id.btUpload,
            R.id.btnEasy})
    List<Button> nav;

    @BindView(R.id.progress) SeekBar progress;
    @BindView(R.id.nav_container) FrameLayout navContainer;
    @BindView(R.id.msGender) MaterialSpinner gender;
    @BindView(R.id.msMaritalStatus) MaterialSpinner maritalStatus;
    @BindView(R.id.msIdentification) MaterialSpinner identification;
    @BindView(R.id.msIncomeClass) MaterialSpinner incomClass;
    @BindView(R.id.msResidentType) MaterialSpinner residentType;
    @BindView(R.id.msCountry)   MaterialSpinner country;
    //@BindView(R.id.etFirstName) TextInputEditText firstName;
    @BindView(R.id.etFirstName) EditText firstName;
//    @BindView(R.id.etOtherName) TextInputEditText otherName;
    @BindView(R.id.etOtherName) EditText otherName;
//    @BindView(R.id.etDateOfBirth) TextInputEditText dateOfBirth;
    @BindView(R.id.etDateOfBirth) EditText dateOfBirth;
    @BindView(R.id.etUploadDoc) EditText etUploadDoc;
    @BindView(R.id.etOccupation) EditText etOccupation;
    @BindView(R.id.etAddress) EditText etAddress;
    @BindView(R.id.etPhone) EditText etPhone;
    @BindView(R.id.etEmail) EditText etEmail;
    @BindView(R.id.etPassword) EditText etPassword;
    @BindView(R.id.etConfirmPassword) EditText etConfirmPassword;

//    @BindView(R.id.mkoa) MaterialSpinner mkoa;
//    @BindView(R.id.wilaya) MaterialSpinner wilaya;
//    @BindView(R.id.kata) MaterialSpinner kata;
    @BindView(R.id.mtaa) EditText mtaa;

    @BindViews({R.id.etFirstName,
            R.id.etOtherName,
            R.id.etEmail,
            R.id.etPhone,
            R.id.etCity,
            R.id.etState,
            R.id.etAddress,
            R.id.etOccupation,
            R.id.etDocumentNumber,
            R.id.etPostalCode,
            R.id.etDateOfBirth,
            R.id.etPassword,
            R.id.etConfirmPassword,
            R.id.mtaa,
            R.id.etUploadDoc
    })
    List<TextView> input;

    @BindView(R.id.imDocument)
    ImageView ivDocument;
    int MY_PERMISSIONS_REQUEST_CAMERA = 0;
    File identification_document;
    String document_name;



    //    LanguageSwitcher ls;
    String annual_income_id_toapi,doc_type_id_toapi,gender_id_toapi,marital_id_toapi,country_id_toapi,resident_type_id_toapi;
    List<String> annual_income_ids,doc_type_ids,gender_ids,marital_ids,country_ids,resident_type_ids;
    private List<String> level_ones_ids,level_two_ids,level_three_ids;
    private String selectedAssociationValue = "0",selectedMkoaValue ="0",selectedWilayaValue="0",selectedKataValue="0";

    RegionDetail region;

    String token;

    Context ctx;
    Uri path;

    private static int num = 6;
    private static final String PDF_DIRECTORY = "/ecobba";
    private static final int BUFFER_SIZE = 1024 * 2;


    public void createUserToAPI(){
        String name = input.get(0).getText().toString();
        Log.e("post_name_is",name);
        String other_names =  input.get(1).getText().toString();
        Log.e("post_name_is",other_names);
        String email =  input.get(2).getText().toString();
        Log.e("post_email_is",email);
        String msisdn =  input.get(3).getText().toString();
        String city =  input.get(4).getText().toString();
        String state =  input.get(5).getText().toString();
        String address =  input.get(6).getText().toString();
        String occupation =  input.get(7).getText().toString();
        String doc_no =  input.get(8).getText().toString();
        String postal_code =  input.get(9).getText().toString();
        String dob =  input.get(10).getText().toString();
        String password = input.get(11).getText().toString();
        String cPassword = input.get(12).getText().toString();
        String mtaa = input.get(13).getText().toString();


        if (name.isEmpty()){
            input.get(0).setError(null);
        }
        if (other_names.isEmpty()){
            input.get(1).setError(null);
        }
        if (dob.isEmpty()){
            input.get(10).setError(null);
        }
        if (msisdn.isEmpty()){
            input.get(3).setError(null);
        }

        if (!password.equals(cPassword)){
            input.get(12).setError(null);
            showSweetAlertDialogError("Password does not match ",ctx);
            return;
        }
        /* dropdown fields below
         *
         * annual_income_id_toapi,doc_type_id_toapi,gender_id_toapi,marital_id_toapi,country_id_toapi,resident_type_id_toapi
         *
         */
        gender_id_toapi = gender_ids.get(gender.getSelectedIndex());
        annual_income_id_toapi = annual_income_ids.get(incomClass.getSelectedIndex());
        doc_type_id_toapi = doc_type_ids.get(identification.getSelectedIndex());
        marital_id_toapi = marital_ids.get(maritalStatus.getSelectedIndex());
        country_id_toapi = country_ids.get(country.getSelectedIndex());
        resident_type_id_toapi = resident_type_ids.get(residentType.getSelectedIndex());

        String params = "{ \"name\":" + '"' + name + '"' + //
                ", \"other_names\": " + '"' + other_names + '"' + //
                ", \"email\": " + '"' + email + '"' + //
                ", \"msisdn\": " + '"' + msisdn + '"' + //
                ", \"city\": " + '"' + city + '"' + //
                ", \"state\": " + '"' + state + '"' + //
                ", \"address\": " + '"' + address + '"' + //
                ", \"occupation\": " + '"' + occupation + '"' + //
                ", \"doc_no\": " + '"' + doc_no + '"' + //
                ", \"postal_code\": " + '"' + postal_code + '"' + //
//                ", \"level_one\": " + '"' + selectedMkoaValue + '"' +
//                ", \"level_two\": " + '"' + selectedWilayaValue + '"' +
//                ", \"level_three\": " + '"' + selectedKataValue + '"' +
//                ", \"mtaa\": " + '"' + mtaa + '"' +
                ", \"dob\": " + '"' + dob + '"' + //
                ", \"income\": " + '"' + annual_income_id_toapi + '"' + //
                ", \"doc_type\": " + '"' + doc_type_id_toapi + '"' + //
                ", \"gender\": " + '"' + gender_id_toapi + '"' + //
                ", \"marital_status\": " + '"' + marital_id_toapi + '"' + //
                ", \"country\": " + '"' + country_id_toapi + '"' + //
                ", \"residence\": " + '"' + resident_type_id_toapi + '"' + //
                ", \"identification_document\": " + '"' + identification_document + '"' + //
                ", \"password\": " + '"' + password + '"' + //
                ", \"password_confirmation\": " + '"' + cPassword + '"' + //
                "}\n" + "\n";
        Log.e("POST_PARAMS", params);
        postToServer( register ,token, params , INITIATOR_CREATE_GROUP_MEMBER , HTTP_POST , ctx ,this);

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        StatusBarUtil.setTransparent(this);
        ButterKnife.bind(this);
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_baseline_arrow_back_24);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               Intent intent= new Intent(RegistrationActivity.this,LoginActivity.class);
               startActivity(intent);
            }
        });

        ////start of camera access persmission
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                == PackageManager.PERMISSION_DENIED)


            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, MY_PERMISSIONS_REQUEST_CAMERA);

        ////end of camera access persmission

        etUploadDoc =findViewById(R.id.etUploadDoc);
//        etUploadDoc.setOnClickListener(this);
        etUploadDoc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setType("application/pdf");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select PDF"), 1);

            }
        });
        dateOfBirth = findViewById(R.id.etDateOfBirth);
        dateOfBirth.setOnClickListener(this);
        ctx = this;
        country.setOnItemSelectedListener(this);
        setProg(0);
        setPage(0);
        navContainer.setVisibility(View.GONE);
        for (Button b : nav){
            b.setOnClickListener(this);
        }
//        mkoa.setOnItemSelectedListener(this);
//        wilaya.setOnItemSelectedListener(this);
//        kata.setOnItemSelectedListener(this);
        getRegisterData(token);
//        getGroupRegistrationData(token);
        gender.setItems(getResources().getStringArray(R.array.gender));
        maritalStatus.setItems(getResources().getStringArray(R.array.maritalStatus));
        identification.setItems(getResources().getStringArray(R.array.idDoc));
        incomClass.setItems(getResources().getStringArray(R.array.income_class));
        residentType.setItems(getResources().getStringArray(R.array.residentType));
        Locale _default = new Locale("en");
//        Locale baseLocal = _default;
//        ls = new LanguageSwitcher(this,_default,baseLocal);
//        ls.setSupportedLocales(R.string.help);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.

        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public void onBackPressed() {
        if (viewFlipper.getDisplayedChild() <= 0){
            super.onBackPressed();
        }else{
            back();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        int id = item.getItemId();
        item.setChecked(true);
//        Log.e("Supported languages",ls.fetchAvailableLocales(R.string.help).toString());
        switch (id){
            case R.id.switchLanguage:
//                if (getCurrentLanguage().getDisplayLanguage().equals("Kiswahili")) {
//                    SharedMethods.storePrefs("locale","en",getApplicationContext());
//                    setLanguage("en");
//                }else{
//                    SharedMethods.storePrefs("locale","sw",getApplicationContext());
//                    setLanguage("sw");
//                }
//                ls.showChangeLanguageDialog(this);
                new ChangeLanguageDialog().show(getSupportFragmentManager(),"LANGUAGE");
                break;

        }

        return super.onOptionsItemSelected(item);
    }

    private void getRegisterData(String token){
        Map<String, String> params = new HashMap<>();
        params.put("token", token);
        getFromServer( getRegisterData ,token, params , INITIATOR_GETERGISTER_DATA_REG , HTTP_GET , ctx ,this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.back:
                back();
                break;
            case R.id.next:
                next();
                break;
            case R.id.etConfirm:
                //put registration code here
                createUserToAPI();
//                startActivity(new Intent(this,MainActivity.class));
//                finish();
                break;
            case R.id.btnFullReg:
                navContainer.setVisibility(View.VISIBLE);
                setPage(1);
                setProg(1);
                break;
            case R.id.btFinish:
               // startActivity(new Intent(ctx,MainActivity.class));
                startActivity(new Intent(ctx,LoginActivity.class));
                finish();
                break;
            case R.id.etDateOfBirth:
                Log.e("PICK","DATE");
                pickDate(dateOfBirth);
                break;
//            case R.id.btCalender:
//                Log.e("PICK","DATE");
//                pickDate(dateOfBirth);
//                break;
//            case R.id.etUploadDoc:
//
//                Intent intent = new Intent(this, ImageSelectActivity.class);
//                intent.putExtra(ImageSelectActivity.FLAG_COMPRESS, false);//default is true
//                intent.putExtra(ImageSelectActivity.FLAG_CAMERA, true);//default is true
//                intent.putExtra(ImageSelectActivity.FLAG_GALLERY, true);//default is true
//                startActivityForResult(intent, 1213);
//
//                break;
            case R.id.btnEasy:
                startActivity(new Intent(ctx, EasyRegistration.class));
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == 1) {
                path = data.getData();



                String paths = getFilePathFromURI(RegistrationActivity.this,path);
                identification_document = new File(paths);
                 document_name=identification_document.getName();
                Log.e("PDF_NAME_IS",document_name);
                etUploadDoc.setText(document_name);
                Log.e("ioooo", String.valueOf(identification_document));
            }
        }
    }

    private String getFilePathFromURI(Context context, Uri contentUri) {
        //copy file and send new file path
        String fileName = getFileName(contentUri);
        File wallpaperDirectory = new File(
                Environment.getExternalStorageDirectory() + PDF_DIRECTORY);
        // have the object build the directory structure, if needed.
        if (!wallpaperDirectory.exists()) {
            wallpaperDirectory.mkdirs();
        }
        if (!TextUtils.isEmpty(fileName)) {
            File copyFile = new File(wallpaperDirectory + File.separator + fileName);
            // create folder if not exists

            copy(context, contentUri, copyFile);
            return copyFile.getAbsolutePath();
        }
        return null;

    }
    public static String getFileName(Uri uri) {
        if (uri == null) return null;
        String fileName = null;
        String path = uri.getPath();
        int cut = path.lastIndexOf('/');
        if (cut != -1) {
            fileName = path.substring(cut + 1);
        }
        return fileName;
    }

    public static void copy(Context context, Uri srcUri, File dstFile) {
        try {
            InputStream inputStream = context.getContentResolver().openInputStream(srcUri);
            if (inputStream == null) return;
            OutputStream outputStream = new FileOutputStream(dstFile);
            copystream(inputStream, outputStream);
            inputStream.close();
            outputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public static int copystream(InputStream input, OutputStream output) throws Exception, IOException {
        byte[] buffer = new byte[BUFFER_SIZE];

        BufferedInputStream in = new BufferedInputStream(input, BUFFER_SIZE);
        BufferedOutputStream out = new BufferedOutputStream(output, BUFFER_SIZE);
        int count = 0, n = 0;
        try {
            while ((n = in.read(buffer, 0, BUFFER_SIZE)) != -1) {
                out.write(buffer, 0, n);
                count += n;
            }
            out.flush();
        } finally {
            try {
                out.close();
            } catch (IOException e) {
                Log.e(e.getMessage(), String.valueOf(e));
            }
            try {
                in.close();
            } catch (IOException e) {
                Log.e(e.getMessage(), String.valueOf(e));
            }
        }
        return count;
    }




    public void back(){

        int n = viewFlipper.getDisplayedChild()-1;

        setProg(n);
        setPage(n);
    }
    private Date pickDate(EditText view){
        final Date[] date = new Date[1];
        Calendar c = Calendar.getInstance();
        int mYear = 2000;
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);
        new DatePickerDialog(view.getContext(),
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker,
                                          int year,
                                          int monthOfYear,
                                          int dayOfMonth) {
                        String string = String.valueOf(year)+"/"+String.valueOf(monthOfYear + 1)+"/"+String.valueOf(dayOfMonth);
                        view.setText(string);
                        try {
                            date[0] = new SimpleDateFormat("dd/MM/yyyy").parse(string);
                            final Calendar datePickerCal = Calendar.getInstance();
                            datePickerCal.set(year, monthOfYear + 1, dayOfMonth);
//                            dob = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).format(datePickerCal.getTime());
//                            dob = date[0];
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                    }
                },mYear,mMonth,mDay)
                .show();
        return date[0];
    }

    public void next(){

        if(validate(viewFlipper.getDisplayedChild())) {
            int n = viewFlipper.getDisplayedChild() + 1;

            setProg(n);
            setPage(n);
        }
    }

    public void setProg(int n){
        if (n>=0 && n <=num) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                progress.setProgress(n, true);
            } else {
                progress.setProgress(n);
            }
        }
        if (n>=num){
            nav.get(1).setVisibility(View.GONE);
            nav.get(0).setVisibility(View.VISIBLE);
        }else {
            nav.get(1).setVisibility(View.VISIBLE);

        }
        if (n<=0){
            nav.get(0).setVisibility(View.GONE);
            nav.get(1).setVisibility(View.VISIBLE);
            navContainer.setVisibility(View.GONE);
        }else {
            nav.get(0).setVisibility(View.VISIBLE);

        }

    }

    public void setPage(int n){
        if (n>=0 && n <=num) {
            viewFlipper.setDisplayedChild(n);

        }
    }

    public boolean validate(int n){
        TextView ti;
        MaterialSpinner ms;
        boolean valid = true;
        switch(n){
            case 1:
                ti = input.get(0);
                if (ti.getText().toString().isEmpty()){
                    ti.setError(getResources().getString(R.string.emptyField));
                    valid = false;
                }
                ti = input.get(1);
                if (ti.getText().toString().isEmpty()){
                    ti.setError(getResources().getString(R.string.emptyField));
                    valid = false;
                }
                ti = input.get(10);
                if (ti.getText().toString().isEmpty()){
                    ti.setError(getResources().getString(R.string.emptyField));
                    valid = false;
                }
                break;
            case 2:
                ti = input.get(8);
                if (ti.getText().toString().isEmpty()){
                    ti.setError(getResources().getString(R.string.emptyField));
                    valid = false;
                }
                break;
            case 3:

                break;
            case 4:
                //TODO
                ti = input.get(13);
                if (ti.getText().toString().isEmpty()){
                    ti.setError(getResources().getString(R.string.emptyField));
                    valid = false;
                }

                ti = input.get(9);
                if (ti.getText().toString().isEmpty()){
                    ti.setError(getResources().getString(R.string.emptyField));
                    valid = false;
                }
                ti = input.get(6);
                if (ti.getText().toString().isEmpty()){
                    ti.setError(getResources().getString(R.string.emptyField));
                    valid = false;
                }
                break;
            case 5:
                ti = input.get(3);
                if (ti.getText().toString().isEmpty()){
                    ti.setError(getResources().getString(R.string.emptyField));
                    valid = false;
                }
                break;
            case 6:
                ti = input.get(11);
                if (ti.getText().toString().isEmpty()){
                    ti.setError(getResources().getString(R.string.emptyField));
                    valid = false;
                }
                ti = input.get(12);
                if (ti.getText().toString().isEmpty()){
                    ti.setError(getResources().getString(R.string.emptyField));
                    valid = false;
                }
                break;
            default:
                return valid;

        }

        return valid;
    }

    @Override
    public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
        switch (view.getId()){
            case R.id.msCountry:
                country_id_toapi = country_ids.get(position);
                getMkoaData(country_id_toapi);
                break;
//            case R.id.mkoa:
////                selectedMkoaValue  = mkoa.getSelectedIndex();
//                selectedMkoaValue= level_ones_ids.get(position);
//                getWilayaData(level_ones_ids.get(position));
//                Log.d("MKOA Status id:",""+level_ones_ids.get(position) );
//                break;
//            case R.id.wilaya:
////                selectedWilayaValue  = wilaya.getSelectedIndex();
//                selectedWilayaValue= level_two_ids.get(position);
//                getKataData(level_two_ids.get(position));
//                Log.d("WILAYA Status id:",""+ level_two_ids.get(position) );
//                break;
//            case R.id.kata:
////                selectedKataValue  = kata.getSelectedIndex();
//                selectedKataValue= level_three_ids.get(position);
//                Log.d("KATA Status id:",""+ level_three_ids.get(position) );
//                break;
        }
    }

    private void getMkoaData(String countr_id){
        Map<String, String> params = new HashMap<String, String>();
        params.put("token", token);
        getFromServer( getRegistrationData+"/"+countr_id ,token, params , INITIATOR_GET_LEVEL_ONE_DATA , HTTP_GET , ctx ,this);

    }

    private void getWilayaData(String mkoa_id){
        Map<String, String> params = new HashMap<String, String>();
        params.put("token", token);
        getFromServer( getLevelTwoData+"/"+mkoa_id ,token, params , INITIATOR_GET_LEVEL_TWO_DATA , HTTP_GET , ctx ,this);
    }

    private void getKataData(String id){
        Map<String, String> params = new HashMap<String, String>();
        params.put("token", token);
        getFromServer( getLevelThreeData+"/"+id ,token, params , INITIATOR_GET_LEVEL_THREE_DATA , HTTP_GET , ctx ,this);
    }

    @Override
    public void onDataReady(String initiator, String response) {
        switch (initiator) {
            case INITIATOR_GETERGISTER_DATA_REG:
                try {
                    runOnUI(new Runnable() {
                        @Override
                        public void run() {
                            Log.e("REG_DATA RESPONSE", "Response is " + response);
//                                    hideDialog(ctx);
                            JSONObject jObj = null;
                            annual_income_ids  = new ArrayList<>();
                            doc_type_ids  = new ArrayList<>();
                            gender_ids  = new ArrayList<>();
                            marital_ids  = new ArrayList<>();
                            country_ids  = new ArrayList<>();
                            resident_type_ids  = new ArrayList<>();

                            try {
                                jObj = new JSONObject(response);
                                if (jObj.has("errors")) {
                                    String errors = jObj.getString("errors");
                                    showSweetAlertDialogError(errors, ctx);
                                } else { // if no error message was found in the response,
                                    boolean success = jObj.getBoolean("success");
                                    if (success) { // if the request was successfuls, got forward, else show error message
                                        JSONObject data = jObj.getJSONObject("message");
//                                        JSONObject jObjWallet = jObjUser.getJSONObject("wallet");
//                                        String balance = jObjWallet.getString("balance");
//                                        tvUserbalance.setText(balance);
                                        JSONArray docs = data.getJSONArray("docs");
//                                        JSONObject region_detail = data.getJSONObject("region_detail");

                                        ArrayList<String> docs_drop_down = new ArrayList<String>();
                                        docs_drop_down.add(


                                                getResources().getString(R.string.please_select_identification_type));
                                        doc_type_ids.add("0");
                                        if (docs.length() > 0) {
                                            for (int i = 0; i < docs.length(); i++) {
                                                JSONObject jsonObject = docs.getJSONObject(i);
                                                String id = jsonObject.getString("id");
                                                String name = jsonObject.getString("name");
                                                docs_drop_down.add(name);
                                                doc_type_ids.add(id);
                                            }
                                            identification.setItems(docs_drop_down);
                                            identification.setSelectedIndex(0);
                                        }

                                        JSONArray countries = data.getJSONArray("countries");
                                        ArrayList<String> countries_drop_down = new ArrayList<String>();
                                        countries_drop_down.add(getResources().getString(R.string.please_select_country));
                                        country_ids.add("0");
                                        if (countries.length() > 0) {
                                            for (int i = 0; i < countries.length(); i++) {
                                                JSONObject jsonObject = countries.getJSONObject(i);
                                                String id = jsonObject.getString("id");
                                                String name = jsonObject.getString("name");
                                                countries_drop_down.add(name);
                                                country_ids.add(id);
                                            }
                                            country.setItems(countries_drop_down);
                                            country.setSelectedIndex(0);
                                        }

                                        JSONArray genders = data.getJSONArray("genders");
                                        ArrayList<String> gender_drop_down = new ArrayList<String>();
                                        gender_drop_down.add(getResources().getString(R.string.please_select_gender));
                                        gender_ids.add("0");
                                        if (genders.length() > 0) {
                                            for (int i = 0; i < genders.length(); i++) {
                                                JSONObject jsonObject = genders.getJSONObject(i);
                                                String id = jsonObject.getString("id");
                                                String name = jsonObject.getString("name");
                                                gender_drop_down.add(name);
                                                gender_ids.add(id);
                                            }
                                            gender.setItems(gender_drop_down);
                                            gender.setSelectedIndex(0);
                                        }

                                        JSONArray marital_statuses = data.getJSONArray("marital_statuses");
                                        ArrayList<String> marital_statuses_drop_down = new ArrayList<String>();
                                        marital_statuses_drop_down.add(getResources().getString(R.string.please_select_marital_status));
                                        marital_ids.add("0");
                                        if (marital_statuses.length() > 0) {
                                            for (int i = 0; i < marital_statuses.length(); i++) {
                                                JSONObject jsonObject = marital_statuses.getJSONObject(i);
                                                String id = jsonObject.getString("id");
                                                String name = jsonObject.getString("name");
                                                marital_statuses_drop_down.add(name);
                                                marital_ids.add(id);
                                            }
                                            maritalStatus.setItems(marital_statuses_drop_down);
                                            maritalStatus.setSelectedIndex(0);
                                        }

                                        JSONArray resident_types = data.getJSONArray("resident_types");
                                        ArrayList<String> resident_types_statuses_drop_down = new ArrayList<String>();
                                        resident_types_statuses_drop_down.add(getResources().getString(R.string.please_select_typeof_residency));
                                        resident_type_ids.add("0");
                                        if (resident_types.length() > 0) {
                                            for (int i = 0; i < resident_types.length(); i++) {
                                                JSONObject jsonObject = resident_types.getJSONObject(i);
                                                String id = jsonObject.getString("id");
                                                String name = jsonObject.getString("name");
                                                resident_types_statuses_drop_down.add(name);
                                                resident_type_ids.add(id);
                                            }
                                            residentType.setItems(resident_types_statuses_drop_down);
                                            residentType.setSelectedIndex(0);
                                        }

                                        JSONArray income_classes = data.getJSONArray("classes");
                                        ArrayList<String> income_classes_drop_down = new ArrayList<String>();
                                        income_classes_drop_down.add(getResources().getString(R.string.please_select_income_class));
                                        annual_income_ids.add("0");
                                        if (income_classes.length() > 0) {
                                            for (int i = 0; i < income_classes.length(); i++) {
                                                JSONObject jsonObject = income_classes.getJSONObject(i);
                                                String id = jsonObject.getString("id");
                                                String name = jsonObject.getString("name");
                                                income_classes_drop_down.add(name);
                                                annual_income_ids.add(id);
                                            }
                                            incomClass.setItems(income_classes_drop_down);
                                            incomClass.setSelectedIndex(0);
                                        }

                                    } else {
                                        String api_error_message = jObj.getString("message");
                                        showSweetAlertDialogError(api_error_message, ctx);
                                    }
                                }

                            } catch (JSONException e) {
                                Log.e("ExceptionSIGNUP-DATA", e.toString());
                                showSweetAlertDialogError(global_error_message, ctx);
                                e.printStackTrace();
                            }
                        }
                    });
                } catch (Exception e) {
//                            hideDialog(ctx);
                    Log.e("Exception- SIGNUP", e.toString());
                    showSweetAlertDialogError(global_error_message, ctx);
//                    Toast.makeText(ctx, "Login Data Response Exception" + e.toString(), Toast.LENGTH_LONG).show();
                }
                break;
            case INITIATOR_CREATE_GROUP_MEMBER:
                try {
                    runOnUI(new Runnable() {
                        @Override
                        public void run() {
                            Log.e("SIGNUP RESPONSE", "Response is " + response);
                            hideDialog(ctx);
                            JSONObject jObj = null;
                            try {
                                jObj = new JSONObject(response);
                                if (jObj.has("errors")) {
                                    String errors = jObj.getString("errors");
                                    showSweetAlertDialogError(errors, ctx);
                                } else { // if no error message was found in the response,
                                    boolean success = jObj.getBoolean("success");
                                    if (success) { // if the request was successfuls, got forward, else show error message
                                        String user = jObj.getString("user");
                                        String token = jObj.getString("token");
                                       // storePrefs("token", token, ctx);

                                        navContainer.setVisibility(View.GONE);
                                        setProg(viewFlipper.getChildCount()-1);
                                        setPage(viewFlipper.getChildCount()-1);



                                      //  startActivity(new Intent(ctx,MainActivity.class));
                                        Toast toast = Toast.makeText(RegistrationActivity.this,"Email verification has been sent to you", Toast.LENGTH_LONG);
                                        toast.setGravity(Gravity.CENTER, 0, 0);
                                        toast.show();
                                        startActivity(new Intent(ctx,LoginActivity.class));
                                        finish();


                                    } else {
                                        String api_error_message = jObj.getString("message");
                                        showSweetAlertDialogError(api_error_message, ctx);
                                    }
                                }

                            } catch (JSONException e) {
                                Log.e("ExceptionSIGNUP-DATA", e.toString());
                                showSweetAlertDialogError(global_error_message, ctx);
                                e.printStackTrace();
                            }
                        }
                    });
                } catch (Exception e) {
                    hideDialog(ctx);
                    Log.e("Exception- SIGNUP", e.toString());
                    showSweetAlertDialogError(global_error_message, ctx);
//                    Toast.makeText(ctx, "Login Data Response Exception" + e.toString(), Toast.LENGTH_LONG).show();
                }
                break;
            case INITIATOR_GET_LEVEL_ONE_DATA:
                try {
                    runOnUI(()->{
                        Log.e(initiator,"Response is "+response);
                        JSONObject jObj = null;
                        try {
                            jObj = new JSONObject(response);
                            if (jObj.has("errors")) {
                                String errors = jObj.getString("errors");
                                showSweetAlertDialogError(errors, ctx);
                            } else {
                                boolean success = jObj.getBoolean("success");
                                if (success) {

                                    if (jObj.has("data") && !jObj.isNull("data")){
                                        level_ones_ids = new ArrayList<>();
                                        level_ones_ids.add("0");
                                        List<String> mkoaString = new ArrayList<>();
                                        mkoaString.add(getString(R.string.please_select_mkoa));
                                        region = new Gson().fromJson(jObj.getJSONObject("data").toString(),RegionDetail.class);
                                        for (LevelOne levelOne:region.getLevel_ones()){
                                            level_ones_ids.add(levelOne.getId());
                                            mkoaString.add(levelOne.getName());
                                        }
                                       // mkoa.setItems(mkoaString);
                                    }

                                }else {
                                    String api_error_message = jObj.getString("message");
                                    showSweetAlertDialogError(api_error_message, ctx);
                                }
                            }
                        }catch (JSONException e){
                            Log.e("Exception-DATA", e.toString());
                            showSweetAlertDialogError(global_error_message, ctx);
                            e.printStackTrace();
                        }
                    });
                }catch (Exception e){
                    Log.e(initiator,e.toString());
                    showSweetAlertDialogError(global_error_message,ctx);
                }
                break;
            case INITIATOR_GET_LEVEL_TWO_DATA:
                try {
                    runOnUI(new Runnable() {
                        @Override
                        public void run() {
                            Log.e("CREATE_GROUP_MEMBER", "Response is " + response);
                            JSONObject jObj = null;
                            try {
                                jObj = new JSONObject(response);
                                if (jObj.has("errors")) {
                                    String errors = jObj.getString("errors");
                                    showSweetAlertDialogError(errors, ctx);
                                } else { // if no error message was found in the response,
                                    boolean success = jObj.getBoolean("success");
                                    if (success) {
                                        level_two_ids  = new ArrayList<>();
//                                        JSONObject level_two = jObj.getJSONObject("data");
                                        JSONArray level_two = jObj.getJSONArray("data");
                                        ArrayList<String> level_two_drop_down = new ArrayList<String>();
                                        level_two_drop_down.add(getResources().getString(R.string.please_select_wilaya));
                                        level_two_ids.add("0");
                                        if (level_two.length() > 0) {
                                            for (int i = 0; i < level_two.length(); i++) {
                                                Log.e("FINAL DATA IS : ", level_two.toString());
                                                JSONObject jsonObject = level_two.getJSONObject(i);
                                                String id = jsonObject.getString("id");
                                                String name = jsonObject.getString("name");
                                                level_two_drop_down.add(name);
                                                level_two_ids.add(id);
                                            }

//                                            wilaya.setItems(level_two_drop_down);
//                                            wilaya.setSelectedIndex(0);
                                        }
                                    } else {
                                        String api_error_message = jObj.getString("message");
                                        showSweetAlertDialogError(api_error_message, ctx);
                                    }
                                }

                            } catch (JSONException e) {
                                Log.e("Exception-DATA", e.toString());
                                showSweetAlertDialogError(global_error_message, ctx);
                                e.printStackTrace();
                            }
                        }
                    });
                } catch (Exception e) {
                    Log.e("Exception_+GROUP_MEMBER", e.toString());
                    showSweetAlertDialogError(global_error_message, ctx);
                }
                break;
            case INITIATOR_GET_LEVEL_THREE_DATA:
                try {
                    runOnUI(new Runnable() {
                        @Override
                        public void run() {
                            Log.e("CREATE_GROUP_MEMBER", "Response is " + response);
//                            hideDialog(ctx);
                            JSONObject jObj = null;
                            try {
                                jObj = new JSONObject(response);
                                if (jObj.has("errors")) {
                                    String errors = jObj.getString("errors");
                                    showSweetAlertDialogError(errors, ctx);
                                } else { // if no error message was found in the response,
                                    boolean success = jObj.getBoolean("success");
                                    if (success) {
                                        level_three_ids  = new ArrayList<>();
//                                        JSONObject level_two = jObj.getJSONObject("data");
                                        JSONArray level_two = jObj.getJSONArray("data");
                                        ArrayList<String> level_three_drop_down = new ArrayList<String>();
                                        level_three_drop_down.add(getResources().getString(R.string.please_select_kata));
                                        level_three_ids.add("0");
                                        if (level_two.length() > 0) {
                                            for (int i = 0; i < level_two.length(); i++) {
                                                Log.e("FINAL DATA IS : ", level_two.toString());
                                                JSONObject jsonObject = level_two.getJSONObject(i);
                                                String id = jsonObject.getString("id");
                                                String name = jsonObject.getString("name");
                                                level_three_drop_down.add(name);
                                                level_three_ids.add(id);
                                            }
//                                            kata.setItems(level_three_drop_down);
//                                            kata.setSelectedIndex(0);
                                        }
                                    } else {
                                        String api_error_message = jObj.getString("message");
                                        showSweetAlertDialogError(api_error_message, ctx);
                                    }
                                }

                            } catch (JSONException e) {
                                Log.e("Exception-DATA", e.toString());
                                showSweetAlertDialogError(global_error_message, ctx);
                                e.printStackTrace();
                            }
                        }
                    });
                } catch (Exception e) {
//                    hideDialog(ctx);
                    Log.e("Exception_+GROUP_MEMBER", e.toString());
                    showSweetAlertDialogError(global_error_message, ctx);
//                    Toast.makeText(ctx, "Login Data Response Exception" + e.toString(), Toast.LENGTH_LONG).show();
                }
                break;
            case INITIATOR_GET_GROUP_REGISTRATION_DATA:
                try {
                    runOnUI(new Runnable() {
                        @Override
                        public void run() {
                            Log.e("REG DATA RESPONSE", "Response is " + response);
//                            hideDialog(ctx);

                            JSONObject jObj = null;
                            try {
                                jObj = new JSONObject(response);
                                if (jObj.has("errors")) {
                                    String errors = jObj.getString("errors");
                                    showSweetAlertDialogError(errors, ctx);
                                } else { // if no error message was found in the response,
                                    boolean success = jObj.getBoolean("success");
                                    if (success) { // if the request was successfuls, got forward, else show error message
                                        JSONObject data = jObj.getJSONObject("data");
                                        //  loop over the tranactions and display in the recyclerview
                                        level_ones_ids  = new ArrayList<>();
                                        JSONArray level_ones = data.getJSONArray("level_ones");
                                        ArrayList<String> level_ones_drop_down = new ArrayList<String>();
                                        level_ones_drop_down.add(getResources().getString(R.string.please_select_mkoa));
                                        level_ones_ids.add("0");
                                        if (level_ones.length() > 0) {
                                            for (int i = 0; i < level_ones.length(); i++) {
                                                JSONObject jsonObject = level_ones.getJSONObject(i);
                                                String id = jsonObject.getString("id");
                                                String name = jsonObject.getString("name");
                                                level_ones_drop_down.add(name);
                                                level_ones_ids.add(id);
                                            }
//
//                                            mkoa.setItems(level_ones_drop_down);
//                                            mkoa.setSelectedIndex(0);
                                        }
                                    } else {
                                        String api_error_message = jObj.getString("message");
                                        showSweetAlertDialogError(api_error_message, ctx);
                                    }
                                }

                            } catch (JSONException e) {
                                Log.e("ExceptionLOGIN-DATA", e.toString());
                                showSweetAlertDialogError(global_error_message, ctx);
                                e.printStackTrace();
                            }
                        }
                    });
                } catch (Exception e) {
//                    hideDialog(ctx);
                    Log.e("Exception- HOME", e.toString());
                    showSweetAlertDialogError(global_error_message, ctx);
//                    Toast.makeText(ctx, "Login Data Response Exception" + e.toString(), Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

    @Override
    public void onDataLoaded(String initiator, JSONArray data) {

    }

    @Override
    public void onNetworkError(String initiator, String errorCode, String errorMessage) {
        switch (initiator) {
            case INITIATOR_LOGIN:
                try{
                    runOnUI(new Runnable() {
                        @Override
                        public void run() {
                            hideDialog(ctx);
                            showSweetAlertDialogError(errorMessage, ctx);
                        }
                    });
                }catch (Exception e){
                    hideDialog(ctx);
                    showSweetAlertDialogError(global_error_message, ctx);
                    Log.e("Exception- SIGNUP", e.toString());
//                    Toast.makeText(ctx, "Login Error Response Exception" + e.toString(), Toast.LENGTH_LONG).show();
                }

                break;

            case INITIATOR_GETERGISTER_DATA_REG:
                try{
                    runOnUI(new Runnable() {
                        @Override
                        public void run() {
//                                    hideDialog(ctx);
                            showSweetAlertDialogError(errorMessage, ctx);
                        }
                    });
                }catch (Exception e){
//                            hideDialog(ctx);
                    showSweetAlertDialogError(global_error_message, ctx);
                    Log.e("Exception- SIGNUP", e.toString());
//                    Toast.makeText(ctx, "Login Error Response Exception" + e.toString(), Toast.LENGTH_LONG).show();
                }

                break;
            case INITIATOR_CREATE_GROUP_MEMBER:
                try{
                    runOnUI(new Runnable() {
                        @Override
                        public void run() {
//                                    hideDialog(ctx);
                            showSweetAlertDialogError(errorMessage, ctx);
                        }
                    });
                }catch (Exception e){
//                            hideDialog(ctx);
                    showSweetAlertDialogError(global_error_message, ctx);
                    Log.e("Exception- SIGNUP", e.toString());
//                    Toast.makeText(ctx, "Login Error Response Exception" + e.toString(), Toast.LENGTH_LONG).show();
                }
                break;
            case INITIATOR_GET_LEVEL_TWO_DATA:
                try{
                    runOnUI(new Runnable() {
                        @Override
                        public void run() {
                            showSweetAlertDialogError(errorMessage, ctx);
                        }
                    });
                }catch (Exception e){
//                            hideDialog(ctx);
                    showSweetAlertDialogError(global_error_message, ctx);
                    Log.e("Exception- HOME", e.toString());
//                    Toast.makeText(ctx, "Login Error Response Exception" + e.toString(), Toast.LENGTH_LONG).show();
                }
                break;
            case INITIATOR_GET_LEVEL_THREE_DATA:
                try{
                    runOnUI(new Runnable() {
                        @Override
                        public void run() {
                            showSweetAlertDialogError(errorMessage, ctx);
                        }
                    });
                }catch (Exception e){
//                            hideDialog(ctx);
                    showSweetAlertDialogError(global_error_message, ctx);
                    Log.e("Exception- HOME", e.toString());
//                    Toast.makeText(ctx, "Login Error Response Exception" + e.toString(), Toast.LENGTH_LONG).show();
                }
                break;
            case INITIATOR_GET_LEVEL_ONE_DATA:
                try{
                    runOnUI(new Runnable() {
                        @Override
                        public void run() {
                            showSweetAlertDialogError(errorMessage, ctx);
                        }
                    });
                }catch (Exception e){
//                            hideDialog(ctx);
                    showSweetAlertDialogError(global_error_message, ctx);
                    Log.e(initiator, e.toString());
//                    Toast.makeText(ctx, "Login Error Response Exception" + e.toString(), Toast.LENGTH_LONG).show();
                }
                break;

        }
    }
}