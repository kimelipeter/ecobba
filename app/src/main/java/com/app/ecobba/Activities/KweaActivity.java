package com.app.ecobba.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.ecobba.Models.KweaItemsShop.Data;
import com.app.ecobba.Models.KweaItemsShop.Image;
import com.app.ecobba.R;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import ru.tinkoff.scrollingpagerindicator.ScrollingPagerIndicator;

public class KweaActivity extends AppCompatActivity {
    ImageView toolbar;
    Button contact_seller;
    Data shop;
    Bundle bundle;
    KweaActivityAdapter adapter;
RecyclerView recyclerView;
    TextView toolbarName;

    ScrollingPagerIndicator recyclerIndicator;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_kwea);
        //setupToolbar();
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_baseline_arrow_back_24);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              finish();
            }
        });
        contact_seller=findViewById(R.id.contact_seller);

        //Recieve Data
        String name = getIntent().getExtras().getString("productname");
        String item_adress = getIntent().getExtras().getString("shop_address");
        String description = getIntent().getExtras().getString("description");
        String owner = getIntent().getExtras().getString("owner");
        String profile = getIntent().getExtras().getString("profile");
        String price = getIntent().getExtras().getString("price");
        assert price != null;
        Log.e("item_price_is",price);
        String seller = getIntent().getExtras().getString("name");
        Log.e("seller_name",seller);
        List<Image> images  = getIntent().getExtras().getParcelableArrayList("img");
        Log.e("prod_images", String.valueOf(images));
        String item_currency=getIntent().getExtras().getString("item_currency");
        String item_owner_id=getIntent().getExtras().getString("user_id");
        String posted_time=getIntent().getExtras().getString("result");
        assert item_owner_id != null;
        Log.e("user_owner_id_is",item_owner_id);



        contact_seller.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(KweaActivity.this,ChatActivity.class);
                intent.putExtra("OTHER_PARTY",item_owner_id);
                startActivity(intent);
            }
        });
        //ini views
        TextView tv_name = findViewById(R.id.aa_anime_name);
        TextView tv_price = findViewById(R.id.aa_price);
        TextView tv_description = findViewById(R.id.aa_description);
//        ImageView img = findViewById(R.id.aa_thumbnail);
        TextView tv_currency=findViewById(R.id.item_currency);
        TextView tv_shop_address=findViewById(R.id.item_shop_adress);
        TextView tv_shopname=findViewById(R.id.shopname);
        ImageView imageView=findViewById(R.id.profile_image);
        recyclerView = findViewById(R.id.recyclerView);
        adapter = new KweaActivityAdapter(new ArrayList<>(),this);
        RecyclerView.LayoutManager layoutManager =new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerIndicator = findViewById(R.id.indicator);
        recyclerView.setAdapter(adapter);
        recyclerIndicator.setDotColor(getResources().getColor(R.color.primaryColor));
        recyclerIndicator.attachToRecyclerView(recyclerView);

        adapter.images = images;
        adapter.notifyDataSetChanged();

        // setting values to each view
        tv_name.setText(name);
        tv_description.setText(description);
        tv_price.setText(price);
        tv_currency.setText(item_currency);
        tv_shop_address.setText(item_adress);
        tv_shopname.setText("Posted "+" "+posted_time+"  by "+owner);
//        Glide.with(this)
//                .load(profile)
//                .error(R.drawable.ic_shop).override(40, 40)
//                .fitCenter().into(imageView);
        Picasso.get().load(profile).fit().centerCrop().into(imageView);


        //set Image Using Glide
       // Glide.with(this).load(image_url).fitCenter().into(img);

       // Picasso.get().load(image_url).fit().centerCrop().into(img);

    }

}