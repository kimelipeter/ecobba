package com.app.ecobba.Activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;

import com.app.ecobba.Models.Credentials;
import com.app.ecobba.R;
import com.app.ecobba.common.SharedMethods;
import com.google.android.material.textfield.TextInputEditText;
import com.google.gson.Gson;
import com.jaeger.library.StatusBarUtil;
import com.jaredrummler.materialspinner.MaterialSpinner;
import com.rilixtech.widget.countrycodepicker.Country;
import com.rilixtech.widget.countrycodepicker.CountryCodePicker;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.LinkedList;
import java.util.List;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;

import static com.app.ecobba.common.ApisKtKt.register;
import static com.app.ecobba.common.AppConstantsKt.INITIATOR_CREATE_GROUP_MEMBER;
import static com.app.ecobba.common.SharedMethods.HTTP_POST;
import static com.app.ecobba.common.SharedMethods.global_error_message;
import static com.app.ecobba.common.SharedMethods.hideDialog;
import static com.app.ecobba.common.SharedMethods.parseMessage;
import static com.app.ecobba.common.SharedMethods.postToServer;
import static com.app.ecobba.common.SharedMethods.runOnUI;
import static com.app.ecobba.common.SharedMethods.showAlertAndMoveToPage;
import static com.app.ecobba.common.SharedMethods.showSweetAlertDialogError;

public class EasyRegistration extends AppCompatActivity implements View.OnClickListener,SharedMethods.NetworkObjectListener{

    @BindViews({
//            R.id.btnGoogle,
//            R.id.btnLinkedIn,
//            R.id.btnFb,
            R.id.btnSignUp
    })
    List<Button> buttons;

    @BindViews({
            R.id.tiPhone,
            R.id.tiEmail,
            R.id.tiPassword,
            R.id.tiConfirm
    })
//    @BindView(R.id.progressBar) ProgressBar progressBar;
    List<EditText> inputs;

//    @BindView(R.id.msPhoneFormat)
//    MaterialSpinner msPhoneFormat;

    String[] phoneFormatList;

    String phone,email,password,confirm,token="",name,other_names;
    Context ctx;
    CountryCodePicker ccp;
    String code;
    @BindView(R.id.tvName)
    EditText tvName;
    @BindView(R.id.tvOtherName)
    EditText tvOtherName;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration_v2);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_baseline_arrow_back_24);
        toolbar.setTitleTextColor(getResources().getColor(R.color.primaryColor));

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        ctx = this;
        StatusBarUtil.setTransparent(this);
        ButterKnife.bind(this);
//        phoneFormatList = getResources().getStringArray(R.array.phone_format);
//        msPhoneFormat.setItems(phoneFormatList);
        for (Button button:buttons){
            button.setOnClickListener(this);
        }

        ccp = (CountryCodePicker) findViewById(R.id.ccp);


        ccp.setOnCountryChangeListener(new CountryCodePicker.OnCountryChangeListener() {
            @Override
            public void onCountrySelected(Country selectedCountry) {
                Toast.makeText(EasyRegistration.this, "Updated country to " + ccp.getSelectedCountryName(), Toast.LENGTH_SHORT).show();
                 code=ccp.getSelectedCountryCode();

                Log.e("Code_country",code);
            }
        });

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){

            case R.id.btnSignUp:
//                signUpProcess();
                if(isFormValid()){
                    signUpProcess();
                }
                break;
        }
    }

    public void signUpProcess(){
        String countryPhone=code +" " + phone;




//        progressBar.setVisibility(View.VISIBLE);

        String params = "{ \"email\":" + '"' + email+  '"' +
                ", \"name\": " + '"' + name + '"' + //
                ", \"other_names\": " + '"' + other_names + '"' + //
                ", \"msisdn\": " + '"' + countryPhone + '"' +
                ", \"password\": " + '"' + password + '"' +
                ", \"password_confirmation\": " + '"' + confirm + '"' +

                "}\n" + "\n";
        Log.e("POST_PARAMS_DATA", params);;
        postToServer( register ,token, params , INITIATOR_CREATE_GROUP_MEMBER , HTTP_POST , ctx ,this);
    }

    public boolean isFormValid(){
        boolean val = true;
        name=tvName.getText().toString();
        other_names=tvOtherName.getText().toString();
        phone = inputs.get(0).getText().toString();
        email = inputs.get(1).getText().toString();
        password = inputs.get(2).getText().toString();
        confirm = inputs.get(3).getText().toString();

        String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";


        if (phone.startsWith("0")){
            phone = phone.substring(1);
        }
        if (phone.isEmpty()){
            inputs.get(0).setError(getString(R.string.emptyField));
            val =false;

        }
        if (name.isEmpty()){
            tvName.setError(getString(R.string.emptyField));
            val =false;

        } if (other_names.isEmpty()){
            tvOtherName.setError(getString(R.string.emptyField));
            val =false;

        }
        if (ccp.getSelectedCountryCode().toString().trim()==null) {
             val = false;

             Toast.makeText(ctx, "Select Country code",
                    Toast.LENGTH_SHORT).show();
        }

        if (phone.length()<8) {
            val = false;

            Toast.makeText(ctx, "Phone number can not be less than 8",
                    Toast.LENGTH_SHORT).show();
        }
        if (email.isEmpty()){
            inputs.get(1).setError(getString(R.string.emptyField));
            inputs.get(1).requestFocus();
            val =false;

        }
        if (password.isEmpty()){
            inputs.get(2).setError(getString(R.string.emptyField));
            inputs.get(2).requestFocus();
            val =false;

        }
        if (confirm.isEmpty()){
            inputs.get(3).setError(getString(R.string.emptyField));
            inputs.get(3).requestFocus();
            val =false;

        }


        // String formatedstring = phoneFormatList[msPhoneFormat.getSelectedIndex()]+phone;
        //  Credentials credentials = new Credentials(formatedstring,email,password,confirm);
//            if (credentials.getEmail().isEmpty() && email.matches(emailPattern)){
//                credentials.setEmail(null);
//            }
        if (email.matches(emailPattern) &&  inputs.get(1).getText().length() > 0)
        {
            //Toast.makeText(getApplicationContext(),"valid email address",Toast.LENGTH_SHORT).show();

        }

        else
        {
            Toast.makeText(getApplicationContext(),"Invalid email address",Toast.LENGTH_SHORT).show();
            //or

        }


        if (!password.equals(confirm)){

            showSweetAlertDialogError("Password does not match ",ctx);
            val =false;

        }
        return val;
    }

    @Override
    public void onDataReady(String initiator, String response) {
        switch (initiator) {
            case INITIATOR_CREATE_GROUP_MEMBER:
                try {
                    runOnUI(new Runnable() {
                        @Override
                        public void run() {
                            Log.e("SIGNUP_RESPONSE", "Response is " + response);
                            hideDialog(ctx);

                            JSONObject jObj = null;
                            try {
                                jObj = new JSONObject(response);
                                if (jObj.has("errors")) {
                                    String errors = parseMessage(jObj.getString("errors"));
                                    showSweetAlertDialogError(errors, ctx);
                                } else { // if no error message was found in the response,
                                    boolean success = jObj.getBoolean("success");
                                    if (success) { // if the request was successfuls, got forward, else show error message
                                        String user = jObj.getString("user");
                                        String token = jObj.getString("token");
                                      //  storePrefs("token", token, ctx);
//                                        startActivity(new Intent(ctx,MainActivity.class));

//                                        String message = jObj.getString("message");;
                                        showAlertAndMoveToPage("Email verification has been sent to you", LoginActivity.class,ctx);

                                    } else {
                                        String api_error_message = jObj.getString("message");
                                        showSweetAlertDialogError(api_error_message, ctx);
                                    }
                                }

                            } catch (JSONException e) {
                                Log.e("ExceptionSIGNUP-DATA", e.toString());
                                showSweetAlertDialogError(global_error_message, ctx);
                                e.printStackTrace();
                            }
                        }
                    });
                } catch (Exception e) {
                    hideDialog(ctx);
                    Log.e("Exception- SIGNUP", e.toString());
                    showSweetAlertDialogError(global_error_message, ctx);
//                    Toast.makeText(ctx, "Login Data Response Exception" + e.toString(), Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

    @Override
    public void onDataLoaded(String initiator, JSONArray data) {

    }

    @Override
    public void onNetworkError(String initiator, String errorCode, String errorMessage) {
        try{
            runOnUI(new Runnable() {
                @Override
                public void run() {
                    hideDialog(ctx);
                    showSweetAlertDialogError(errorMessage, ctx);
                }
            });
        }catch (Exception e){
            hideDialog(ctx);
            showSweetAlertDialogError(global_error_message, ctx);
            Log.e("Exception- SIGNUP", e.toString());
//                    Toast.makeText(ctx, "Login Error Response Exception" + e.toString(), Toast.LENGTH_LONG).show();
        }
    }


}
