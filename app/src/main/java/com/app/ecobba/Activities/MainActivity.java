package com.app.ecobba.Activities;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.content.ServiceConnection;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.Settings;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.NavigationUI;

import com.allenliu.badgeview.BadgeFactory;
import com.allenliu.badgeview.BadgeView;
import com.app.ecobba.BuildConfig;
import com.app.ecobba.Dialogs.ChangeLanguageDialog;
import com.app.ecobba.Fragments.Profile.viewmodel.ProfileViewModel;
import com.app.ecobba.R;
import com.app.ecobba.Services.BackgroundService;
import com.app.ecobba.common.ApisKtKt;
import com.app.ecobba.common.SharedMethods;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.play.core.appupdate.AppUpdateInfo;
import com.google.android.play.core.appupdate.AppUpdateManager;
import com.google.android.play.core.appupdate.AppUpdateManagerFactory;
import com.google.android.play.core.install.InstallStateUpdatedListener;
import com.google.android.play.core.install.model.AppUpdateType;
import com.google.android.play.core.install.model.InstallStatus;
import com.google.android.play.core.install.model.UpdateAvailability;
import com.google.android.play.core.tasks.OnSuccessListener;
import com.google.android.play.core.tasks.Task;
import com.jaeger.library.StatusBarUtil;

import org.json.JSONArray;

import butterknife.BindView;
import butterknife.ButterKnife;
import lombok.val;

import static com.app.ecobba.common.AppConstantsKt.NETBROADCAST;
import static com.app.ecobba.common.SharedMethods.deletePrefs;

//import com.app.ecobba.Fragments.Log.e.KweaFragment;

//import static com.akexorcist.localizationactivity.core.LanguageSetting.getLanguage;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,
        View.OnClickListener, SharedMethods.NetworkObjectListener {
    String fagsUrl = (String) ApisKtKt.getFags;
    String ServicesUrl = (String) ApisKtKt.getServices;
    BackgroundService notificationService;
    Boolean serviceBound = false;
    NavController navHostFragment;
    Context ctx;
    ImageView profile_image;
    NavigationView navigationView;

    //    @BindView(R.id.tvBalance)  TextView tvBalance;
    @BindView(R.id.help_center)
    Button help_center;
    @BindView(R.id.chats_contacts)
    Button chats_contacts;
    @BindView(R.id.notification)
    Button notification;
    @BindView(R.id.flagIcon)
    Button flagIcon;
    @BindView(R.id.btnCloseCard)
    Button btnClose;
    @BindView(R.id.btnOpenProfile)
    Button btnProfile;
    @BindView(R.id.cvProfile)
    CardView profile;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawer;

    ActionBarDrawerToggle toggle;

    ProfileViewModel profileViewModel;
    private AppUpdateManager appUpdateManager;
    private AppUpdateInfo appUpdateInfo;
    int UPDATE_REQUEST_CODE = 1;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        StatusBarUtil.setTransparent(this);
        ButterKnife.bind(this);
        ctx = this;
//        appUpdateManager = AppUpdateManagerFactory.create(ctx);
        checkUpdate();


        profileViewModel = ViewModelProviders.of(this).get(ProfileViewModel.class);

        navigationView = findViewById(R.id.nav_view);

        profile_image = toolbar.findViewById(R.id.profile_image);
        profile_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawer.openDrawer(GravityCompat.START);

            }
        });

        help_center.setOnClickListener(this);
        chats_contacts.setOnClickListener(this);
        notification.setOnClickListener(this);
        flagIcon.setOnClickListener(this);
        btnClose.setOnClickListener(this);
        btnProfile.setOnClickListener(this);
        try {
            String number = SharedMethods.getDefaults("notifications", ctx);
            if (number != null) {
                int num = Integer.parseInt(number);
                BadgeFactory.create(ctx)
                        .setShape(BadgeView.SHAPE_CIRCLE)
                        .setBadgeCount(num)
                        .setWidthAndHeight(20, 20)
                        .setBadgeGravity(Gravity.RIGHT)
                        .setTextSize(10)
                        .bind(notification);
                Log.e("BADGE", "BADGE SET");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        navHostFragment = Navigation.findNavController(this, R.id.content);
        navHostFragment.addOnDestinationChangedListener((controller, destination, arguments) -> {
            if (destination.getId() == R.id.dashboardFragment) {
                toolbar.setNavigationIcon(null);
            } else {
                profile_image.setVisibility(View.INVISIBLE);
                toolbar.setNavigationIcon(R.drawable.ic_back);
            }
        });
        toolbar.setNavigationOnClickListener(v -> {
            onBackPressed();
        });
        //BottomNavigationView bottomNavigationView = findViewById(R.id.bottom_nav);
        //  NavigationUI.setupWithNavController(bottomNavigationView,navHostFragment);
        NavigationUI.setupWithNavController(navigationView, navHostFragment);
//        NavigationUI.setupWithNavController(toolbar,navHostFragment,drawer);

        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == UPDATE_REQUEST_CODE) {
            if (resultCode != RESULT_OK) {
                Log.e("Update flow failed!", String.valueOf(resultCode));
                // If the update is cancelled or fails,
                // you can request to start the update again.
            }
        }
    }


    private void checkUpdate() {
        appUpdateManager = AppUpdateManagerFactory.create(this);
        // Creates instance of the manager.


        // Returns an intent object that you use to check for an update.
        Task<AppUpdateInfo> appUpdateInfoTask = appUpdateManager.getAppUpdateInfo();

        // Checks that the platform will allow the specified type of update.
        appUpdateInfoTask.addOnSuccessListener(new OnSuccessListener<AppUpdateInfo>() {
            @Override
            public void onSuccess(AppUpdateInfo appUpdateInfo) {

                Log.e("AVAILABLE_VERSION_CODE", appUpdateInfo.availableVersionCode() + "");
                if (appUpdateInfo.updateAvailability() == UpdateAvailability.UPDATE_AVAILABLE
                        // For a flexible update, use AppUpdateType.FLEXIBLE
                        && appUpdateInfo.updatePriority() >= 4 /* high priority */
                        && appUpdateInfo.clientVersionStalenessDays() != null
                        && appUpdateInfo.isUpdateTypeAllowed(AppUpdateType.IMMEDIATE)) {
                    // Request the update.

                    try {
                        appUpdateManager.startUpdateFlowForResult(
                                // Pass the intent that is returned by 'getAppUpdateInfo()'.
                                appUpdateInfo,
                                // Or 'AppUpdateType.FLEXIBLE' for flexible updates.
                                AppUpdateType.IMMEDIATE,
                                // The current activity making the update request.
                                MainActivity.this,
                                // Include a request code to later monitor this update request.
                                UPDATE_REQUEST_CODE);
                    } catch (IntentSender.SendIntentException ignored) {

                    }
                }
            }
        });

        appUpdateManager.registerListener(installStateUpdatedListener);
        appUpdateManager.unregisterListener(installStateUpdatedListener);

    }

    //lambda operation used for below listener
    InstallStateUpdatedListener installStateUpdatedListener = installState -> {
        if (installState.installStatus() == InstallStatus.DOWNLOADED) {
            popupSnackbarForCompleteUpdate();
        }
//            else  if (installState.installStatus() == InstallStatus.DOWNLOADING){
//                long bytesDownloaded = installState.bytesDownloaded();
//                long totalBytesToDownload = installState.totalBytesToDownload();
//                // Implement progress bar.
//            }
        else
            Log.e("UPDATE", "Not downloaded yet");
    };


    private void popupSnackbarForCompleteUpdate() {

        Snackbar snackbar =
                Snackbar.make(
                        findViewById(R.id.content),
                        "Update almost finished!",
                        Snackbar.LENGTH_INDEFINITE);
        //lambda operation used for below action
        snackbar.setAction(this.getString(R.string.restart), view ->
                appUpdateManager.completeUpdate());
        snackbar.setActionTextColor(getResources().getColor(R.color.primaryColor));
        snackbar.show();
    }


    public boolean isConnected(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnectedOrConnecting()) {
            android.net.NetworkInfo wifi = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            android.net.NetworkInfo mobile = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

            if ((mobile != null && mobile.isConnectedOrConnecting()) ||
                    (wifi != null && wifi.isConnectedOrConnecting())) return true;
            else return false;
        } else return false;
    }




    private ServiceConnection mConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            BackgroundService.LocalBinder binder = (BackgroundService.LocalBinder) iBinder;
            notificationService = binder.getService();
            serviceBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            serviceBound = false;
        }
    };

    BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            Bundle b = intent.getExtras();

            String message = b.getString("message");

            Log.e("BROADCAST", "" + message);

            notificationService.postData();

        }
    };


    @Override
    protected void onStart() {
        super.onStart();
        Intent intent = new Intent(this, BackgroundService.class);
        bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
        registerReceiver(broadcastReceiver, new IntentFilter(NETBROADCAST));

    }

    @Override
    protected void onDestroy() {
        unbindService(mConnection);
        unregisterReceiver(broadcastReceiver);
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        navHostFragment.addOnDestinationChangedListener((controller, destination, arguments) -> {
            if (destination.getId() == R.id.dashboardFragment) {
                toolbar.setNavigationIcon(null);
                profile_image.setVisibility(View.VISIBLE);
            } else {

                toolbar.setNavigationIcon(R.drawable.ic_back);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.

        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    MenuItem prevItem;

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        item.setChecked(true);

        if (id == R.id.switchLanguage) {
            new ChangeLanguageDialog().show(getSupportFragmentManager(), "Lang");
        }
        return super.onOptionsItemSelected(item);
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        Log.e("MENU", "CLICKED");
        switch (item.getItemId()) {
            case R.id.services:
                Intent services = new Intent(Intent.ACTION_VIEW, Uri.parse(ServicesUrl));
                startActivity(services);

                break;
            case R.id.resources:

                navHostFragment.navigate(R.id.resourcesDasboard);

                break;
            case R.id.web_faqs:
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(fagsUrl));
                startActivity(browserIntent);
                break;
            case R.id.groupsFragment:
                navHostFragment.navigate(R.id.groupsFragment);
                break;
            case R.id.packagesFragment:
                navHostFragment.navigate(R.id.packagesFragment);
                break;
            case R.id.settingsFragment:
                navHostFragment.navigate(R.id.settingsFragment);
                break;
            case R.id.borrowFragment:
                navHostFragment.navigate(R.id.borrowFragment);
                break;
            case R.id.kwea_market:
                navHostFragment.navigate(R.id.kweaServicesDashboard);
                break;
            case R.id.Utilities:
                navHostFragment.navigate(R.id.buy_Airtime);
                break;
            case R.id.my_profile:
                navHostFragment.navigate(R.id.accountFragment);
                break;
            case R.id.help_center:
                Intent intent = new Intent(MainActivity.this, SupportActivity.class);
                startActivity(intent);
                break;
            case R.id.logOut:
                Log.d("LOGOUT", "LOGOUT TAKEN");
                deletePrefs("role", ctx);
                deletePrefs("token", ctx);
                startActivity(new Intent(MainActivity.this, LoginActivity.class));
                finish();
                break;
            case R.id.share_ecobba:
                try {
                    Intent shareIntent = new Intent(Intent.ACTION_SEND);
                    shareIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    shareIntent.setType("text/plain");
                    shareIntent.putExtra(Intent.EXTRA_SUBJECT, "Ecobba");
                    String shareMessage = "\nI Would like to share Ecobba with you. Here You Can Download This Application from PlayStore\n\n";
                    shareMessage = shareMessage + "https://play.google.com/store/apps/details?id=" + BuildConfig.APPLICATION_ID + "\n\n";
                    shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage);
                    startActivity(Intent.createChooser(shareIntent, "choose one"));
                } catch (Exception e) {
                    //e.toString();
                }
                break;


        }
        return true;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.help_center:
                Intent intent = new Intent(MainActivity.this, SupportActivity.class);
                startActivity(intent);
                break;
            case R.id.notification:
                navHostFragment.navigate(R.id.notificationFragment);
                break;
            case R.id.chats_contacts:
                //  navHostFragment.navigate(R.id.chatContactsFragment2);
                Intent intent1 = new Intent(this, ChatActivity.class);
                startActivity(intent1);
                break;
            case R.id.btnOpenProfile:
                navHostFragment.navigate(R.id.accountFragment);
                new Thread(() -> {
                    try {
                        Thread.sleep(500);
                        runOnUiThread(() -> {
                            profile.setVisibility(View.GONE);
                        });
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                }).start();
                break;
            case R.id.btnCloseCard:
                profile.setVisibility(View.GONE);
                break;
            case R.id.flagIcon:
                new ChangeLanguageDialog().show(getSupportFragmentManager(), "Lang");
                break;
        }
    }

    @Override
    public void onDataReady(String initiator, String response) {

    }

    @Override
    public void onDataLoaded(String initiator, JSONArray data) {

    }

    @Override
    public void onNetworkError(String initiator, String errorCode, String errorMessage) {

    }




}
