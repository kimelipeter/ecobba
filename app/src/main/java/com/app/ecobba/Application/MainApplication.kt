package com.app.ecobba.Application

import android.app.Notification
import android.content.Context
import androidx.appcompat.app.AppCompatDelegate
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import androidx.multidex.MultiDexApplication
import com.ahmedjazzar.rosetta.LanguageSwitcher
import com.app.ecobba.Fragments.chat.ChatViewModel
import java.util.*
import kotlin.collections.ArrayList

const val GROUP_EMAIL = "GROUP_EMAILS"

final class MainApplication : MultiDexApplication(), LifecycleObserver {
    var ls: LanguageSwitcher? = null
    val notifList = ArrayList<Notification>()
    lateinit var chatWorkManager: ChatWorkManager

    companion object {

        lateinit var context: Context
        lateinit var instance: MainApplication
        var APP_IN_FORGROUND: Boolean = true

        @JvmField
        var locales = Arrays.asList(
                Locale("fr", "FR"),
                Locale("en", "US"),
                Locale("sw", "TZ"))
        var chatViewModel: ChatViewModel? = null
            private set

    }

    init {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)
    }

    override fun onCreate() {
        super.onCreate()
        context = this
        instance = this
        if (chatViewModel == null) chatViewModel = ChatViewModel(this)
        val base = Locale.ENGLISH
        val _default = Locale("en")
        val swahili = Locale("sw")
        val french = Locale("fr")
        ls = LanguageSwitcher(baseContext, base, _default)
        ls!!.setSupportedLocales(HashSet(locales))
    }


    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    fun appInForeground() {
        APP_IN_FORGROUND = false
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    fun appInBackground() {
        APP_IN_FORGROUND = true
    }


}