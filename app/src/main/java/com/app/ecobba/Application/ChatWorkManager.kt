package com.app.ecobba.Application

import android.content.Context
import android.util.Log
import androidx.work.*

class ChatWorkManager(val context: Context, params: WorkerParameters) : CoroutineWorker(context, params) {
    override suspend fun doWork(): Result {

        Log.d("WorkerChat", "Main Started")
        MainApplication.chatViewModel!!.getMessage()
        return Result.success()

    }


}
