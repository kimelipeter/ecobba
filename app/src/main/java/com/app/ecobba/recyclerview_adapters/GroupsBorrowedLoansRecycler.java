package com.app.ecobba.recyclerview_adapters;


import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.app.ecobba.Models.Approve;
import com.app.ecobba.R;
import com.app.ecobba.common.SharedMethods;
import com.app.ecobba.recyclerview_models.GroupsBorrowedLoansAdapter;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import static com.app.ecobba.common.ApisKtKt.ProcessLoan;
import static com.app.ecobba.common.AppConstantsKt.INITIATOR_PROCESS_LOAN;
import static com.app.ecobba.common.SharedMethods.HTTP_POST;
import static com.app.ecobba.common.SharedMethods.global_error_message;
import static com.app.ecobba.common.SharedMethods.postToServer;
import static com.app.ecobba.common.SharedMethods.runOnUI;
import static com.app.ecobba.common.SharedMethods.showSweetAlertDialogError;
import static com.app.ecobba.common.SharedMethods.showSweetAlertDialogSuccess;

/**
 * Adapter to display recycler view.
 */
public class GroupsBorrowedLoansRecycler extends RecyclerView.Adapter<GroupsBorrowedLoansRecycler.ViewHolder> implements SharedMethods.NetworkObjectListener {
    Context context;
    List<GroupsBorrowedLoansAdapter> DataAdpter;
    Fragment fragment;
    String token;
    String loanLimit;

    public GroupsBorrowedLoansRecycler(List<GroupsBorrowedLoansAdapter> DataAapter, Context context, Fragment fragment,String loanLimit) {
        this.DataAdpter = DataAapter;
        this.context = context;
        this.fragment = fragment;
        token = SharedMethods.getDefaults("token",context);
        this.loanLimit = loanLimit;
    }



    @Override
    public GroupsBorrowedLoansRecycler.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new GroupsBorrowedLoansRecycler.ViewHolder(LayoutInflater.from(parent.getContext()), parent);
    }

    @Override
    public void onBindViewHolder(final GroupsBorrowedLoansRecycler.ViewHolder holder, final int position) {

        final String id = DataAdpter.get(position).getLoan_id();
        final String tvLoanBorrowedDate = DataAdpter.get(position).getBorrowed_date();
        final String tvLoanAmount = DataAdpter.get(position).getLoan_amount();
        final String tvLoanTitle = DataAdpter.get(position).getLoan_name();
        final String tvLoanBorrowerName = DataAdpter.get(position).getBorrower_name();
        final String tvLoanStatus = DataAdpter.get(position).getLoan_status();
        //  1 - approved, 2-denied, 0 -pending
        String loan_status_name = "";
        holder.setIsRecyclable(false);
        switch (tvLoanStatus) {
            case "1":
                loan_status_name = "Approved";
                holder.btnApproveLoan.setVisibility(View.GONE);
                holder.tvDeclineLoan.setVisibility(View.GONE);
                holder.tvLoanStatus.setBackgroundColor(context.getResources().getColor(R.color.green));
                break;
            case "2":
                holder.btnApproveLoan.setVisibility(View.GONE);
                holder.tvDeclineLoan.setVisibility(View.GONE);
                loan_status_name = "Denied";
                holder.tvLoanStatus.setBackgroundColor(context.getResources().getColor(R.color.red));
                break;
            case "0":
                loan_status_name = "Pending";
                break;
        }

        holder.tvLoanStatus.setText(loan_status_name);
        holder.tvLoanBorrowerName.setText(tvLoanBorrowerName);
        holder.tvLoanTitle.setText(tvLoanTitle);
        holder.tvLoanAmount.setText(tvLoanAmount);
        holder.tvLoanBorrowedDate.setText(tvLoanBorrowedDate);



        holder.btnApproveLoan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("GOTO APPROVE","YES "+position);
                // API call to approve group loans
                final String params = new Gson().toJson(new Approve(1));
               postToServer(ProcessLoan+"/"+id,token,params,INITIATOR_PROCESS_LOAN,HTTP_POST,context,GroupsBorrowedLoansRecycler.this);
            }
        });

        holder.tvDeclineLoan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("GOTO DECLINE","YES "+position);
                // API call to decline group loans
                final String params = new Gson().toJson(new Approve(2));
                postToServer(ProcessLoan+"/"+id,token,params,INITIATOR_PROCESS_LOAN,HTTP_POST,context,GroupsBorrowedLoansRecycler.this);
            }
        });
    }

    public void removeItem(int position){
        DataAdpter.remove(position);
        notifyItemRemoved(position);
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return DataAdpter.size();
    }

    @Override
    public void onDataReady(String initiator, String response) {
        switch (initiator){
            case INITIATOR_PROCESS_LOAN:
                try {
                    runOnUI(() -> {
                        Log.e("DEPOSIT RESPONSE", "Response is " + response);
//                                    hideDialog(ctx);
                        JSONObject jObj = null;
                        try {
                            jObj = new JSONObject(response);
                            if (jObj.has("errors")) {
                                String errors = jObj.getString("errors");
                                showSweetAlertDialogError(errors, context);
                            } else { // if no error message was found in the response,
                                boolean success = jObj.getBoolean("success");
                                if (success){ // if the request was successfuls, got forward, else show error message
                                    String message = jObj.getString("message");
                                    showSweetAlertDialogSuccess(message,context);
                                    if (callback!=null){
                                        callback.update();
                                    }
                                } else {
                                    String api_error_message = jObj.getString("message");
                                    showSweetAlertDialogError(api_error_message, context);
                                }
                            }
                        } catch (JSONException e) {
                            Log.e("ExceptionLOGIN-DATA", e.toString());
                            showSweetAlertDialogError(global_error_message, context);
                            e.printStackTrace();
                        }
                    });
                } catch (Exception e) {
//                            hideDialog(ctx);
                    Log.e("Exception- HOME", e.toString());
                    showSweetAlertDialogError(global_error_message, context);
//                    Toast.makeText(ctx, "Login Data Response Exception" + e.toString(), Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

    @Override
    public void onDataLoaded(String initiator, JSONArray data) {

    }

    @Override
    public void onNetworkError(String initiator, String errorCode, String errorMessage) {
        switch (initiator){
            case INITIATOR_PROCESS_LOAN:
                try{
                    runOnUI(new Runnable() {
                        @Override
                        public void run() {
//                                    hideDialog(ctx);
                            showSweetAlertDialogError(errorMessage, context);
                        }
                    });
                }catch (Exception e){
//                            hideDialog(ctx);
                    showSweetAlertDialogError(global_error_message, context);
                    Log.e("Exception- HOME", e.toString());
//                    Toast.makeText(ctx, "Login Error Response Exception" + e.toString(), Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

    /**
     * Class containing all the views used here, textviews and all.
     */
    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tvLoanStatus,tvLoanBorrowerName,tvLoanTitle,tvLoanAmount,tvLoanBorrowedDate;
        CardView civ;
        ImageView imageViewIsured;
        Button btnApproveLoan,tvDeclineLoan;

        public ViewHolder(LayoutInflater inflater, ViewGroup parent) {
            super(inflater.inflate(R.layout.list_item_group_loan_borrowed, parent, false));
            tvLoanStatus =  itemView.findViewById(R.id.tvLoanStatus);
            tvLoanBorrowedDate = itemView.findViewById(R.id.tvLoanBorrowedDate);
            tvLoanBorrowerName = itemView.findViewById(R.id.tvLoanBorrowerName);
            tvLoanTitle = itemView.findViewById(R.id.tvLoanTitle);
            tvLoanAmount = itemView.findViewById(R.id.tvLoanAmount);
            btnApproveLoan = itemView.findViewById(R.id.btnApproveLoan);
            tvDeclineLoan = itemView.findViewById(R.id.tvDeclineLoan);
            civ = itemView.findViewById(R.id.cardHeader);
        }
    }

    public interface onDataSetChanged{
        void update();
    }

    private onDataSetChanged callback;
    public GroupsBorrowedLoansRecycler SetDataChangedCallback(onDataSetChanged c){

        callback =c;

        return this;
    }

}


