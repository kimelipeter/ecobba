package com.app.ecobba.recyclerview_adapters;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.ecobba.Fragments.Shop.MyKweaProducts.EditKweaShopItem;
import com.app.ecobba.Models.SingleShopItems.Item;
import com.app.ecobba.R;

import java.util.ArrayList;
import java.util.List;

public class SingleShopAdapter extends RecyclerView.Adapter<SingleShopAdapter.ViewHolder> {
    private static final String TAG = "SingleShopAdapter";
    public List<Item> datalist;
    Fragment fragment;
    Context context;
    EditKweaShopItem editKweaShopItem;
    String item_name, item_amount, item_description, shop_name,item_id,item_currency;

    public SingleShopAdapter(List<Item> datalist, Fragment fragment, Context context) {
        this.datalist = datalist;
        this.fragment = fragment;
        this.context = context;
    }

    @NonNull
    @Override
    public SingleShopAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.single_kwea_shop_items, parent, false);


        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SingleShopAdapter.ViewHolder holder, int position) {
        Item item = datalist.get(position);
         item_currency = item.getItem_currency();
        item_name =item.getItem_name();
        item_amount = String.valueOf(item.getItem_price());
        item_description = item.getItem_description();
        shop_name = item.getShop().getShop_name();
        holder.tv_item_name.setText(item_name);
        holder.tv_item_currency.setText(item_currency);
        holder.tv_item_price.setText("" + item_amount);
        holder.tv_item_category.setText(item.getItem_category());
        holder.tv_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "onClick: Name: " +item_name);
                EditShopItem(holder.tv_item_name.getText().toString(), holder.tv_item_price.getText().toString(),
                        item.getItem_description(), shop_name, String.valueOf(item.getId()));

            }
        });

    }

    private void EditShopItem(String item_name, String item_amount, String item_description, String shop_name,String item_id) {

        FragmentManager manager = ((AppCompatActivity) context).getSupportFragmentManager();
        editKweaShopItem = new EditKweaShopItem(item_name, item_amount, item_description, shop_name,item_id);
        Log.d("TAG", "EditShopItem: " + editKweaShopItem);
        editKweaShopItem.show(manager, "TaG");

    }

    @Override
    public int getItemCount() {
        return datalist.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tv_item_name;
        TextView tv_item_currency;
        TextView tv_item_price;
        TextView tv_item_category;
        TextView tv_edit;
        LinearLayout action_shops_items;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            // action_shops_items=itemView.findViewById(R.id.action_shops_items);
            //   dashhboard_container=itemView.findViewById(R.id.dashhboard_container);
            tv_item_name = itemView.findViewById(R.id.shop_item_name);
            tv_item_currency = itemView.findViewById(R.id.shop_item_currency);
            tv_item_price = itemView.findViewById(R.id.shop_item_price);
            tv_item_category = itemView.findViewById(R.id.shop_item_category);
            tv_edit = itemView.findViewById(R.id.tvEdit);

        }
    }
}
