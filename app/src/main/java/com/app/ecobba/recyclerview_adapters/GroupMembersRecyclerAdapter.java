package com.app.ecobba.recyclerview_adapters;


import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.RecyclerView;

import com.app.ecobba.Models.GroupMmembers.Data;
import com.app.ecobba.R;

import com.google.gson.Gson;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.squareup.picasso.Picasso;

import java.util.List;

public class GroupMembersRecyclerAdapter extends RecyclerView.Adapter<GroupMembersRecyclerAdapter.ViewHolder> {

   public List<Data> datalist;
    public Context context;
    public Fragment fragment;

    private final int limit = 3;
    int user_id;


    public GroupMembersRecyclerAdapter( List<Data> datalist, Context context, Fragment fragment) {
        this.datalist = datalist;
        this.context = context;
        this.fragment = fragment;
    }

    @Override
    public GroupMembersRecyclerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.list_item_user, parent, false);
        return new GroupMembersRecyclerAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final GroupMembersRecyclerAdapter.ViewHolder holder, final int position) {
        final String member_avatar = datalist.get(position).getAvatar();
        final String tv_group_member_name = datalist.get(position).getName()+" "+datalist.get(position).getOther_names();
        final String tv_group_member_phone_number = datalist.get(position).getMsisdn();
        Log.e("MEMBER_PICTURE",member_avatar);
        final  String member_id= String.valueOf(datalist.get(position).getMember_id());
        final  String role_name=datalist.get(position).getMember_role().getDisplay_name();
        holder.role.setText(role_name);

        holder.tv_group_member_name.setText(tv_group_member_name);
//        holder.tv_group_member_phone_number.setText(tv_group_member_phone_number);
//        holder.createdAt.setText(datalist.get(position).getMember_since());
//
//        List<Role> roles = datalist.get(position).getRoles();
//
//
//
//
////        for (int i = 0;i<roles.size();i++){
////            String name = roles.get(i).getName();
////            Log.e("all_Name",name);
////            if (name.equals("group-admin")) {
////                holder.role.setText(R.string.coordinator);
////            } else if (name.equals("group-member") && !name.contains("group-admin")) {
////                holder.role.setText(R.string.member);
////            } else if (name.equals("group-secretary")) {
////
////                holder.role.setText(R.string.secretary);
////            } else if (name.equals("normal-user")) {
////
////                holder.role.setText(R.string.normal_user);
////            } else if (name.equals("group-treasurer")) {
////
////                holder.role.setText(R.string.treasurer);
////            } else if (name.equals("service-provider")) {
////
////                holder.role.setText(R.string.sevice_provider);
////            } else if (name.equals("treasurer")) {
////
////                holder.role.setText(R.string.treasurer);
////            }
////        }

        Picasso.get().load(member_avatar).fit().centerCrop().into(holder.circularImageView);



        holder.civ.setOnClickListener((view) -> {
            Bundle arg = new Bundle();
            arg.putString("MEMBER",member_id);
            arg.putString("avatar",member_avatar);
            NavHostFragment.findNavController(fragment).navigate(R.id.editGroupMemberFragment, arg);
        });

    }


    @Override
    public int getItemCount() {
        if(datalist.size() > limit){
            return limit;
        }
        else
        {
            return datalist.size();
        }
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_group_member_name, tv_group_member_phone_number, role, createdAt;
        CardView civ;
        public CircularImageView circularImageView;

        public ViewHolder(View itemView) {
            super(itemView);
            tv_group_member_name = itemView.findViewById(R.id.firstname);
//            tv_group_member_phone_number = itemView.findViewById(R.id.tvGroupMemberMsisdn);
            circularImageView = itemView.findViewById(R.id.circularImageView);
            role = itemView.findViewById(R.id.role);
            civ = itemView.findViewById(R.id.cardHeader);
        }
    }


}


