package com.app.ecobba.recyclerview_adapters;


import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.RecyclerView;


import com.app.ecobba.Models.UserLoans.Loan;
import com.app.ecobba.R;
import com.google.gson.Gson;
import com.snov.timeagolibrary.PrettyTimeAgo;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import static com.app.ecobba.common.SharedMethods.GroupDigits;

/**
 * Adapter to display recycler view.
 */
public class MyLoansRecycler extends
        RecyclerView.Adapter<MyLoansRecycler.ViewHolder> {

    public List<Loan> datalist;
    public Context context;
    public Fragment fragment;
    public String user_currency;
    long datepower;
    String date;

    public MyLoansRecycler(List<Loan> datalist, Context context, Fragment fragment,String user_currency) {
        this.datalist = datalist;
        this.context = context;
        this.fragment = fragment;
        this.user_currency=user_currency;
    }

    @Override
    public MyLoansRecycler.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.list_item_borrowed_loan, parent, false);
        return new MyLoansRecycler.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final MyLoansRecycler.ViewHolder holder, final int position) {
        final String loan_status = datalist.get(position).getStatus();
        final String loan_title = datalist.get(position).getLoan_title();
        final int amount = (int) datalist.get(position).getAmount();
        final String installment =  datalist.get(position).getDetail().getNo_of_installments();
        final double payable = datalist.get(position).getDetail().getPrincipal();
        final double balance =  datalist.get(position).getAmount();
        date = datalist.get(position).getDetail().getCreated_at();
        Log.e("PAY_BACK_DATE_IS", date);

        // loan statuses
        /**
         * 0 pending
         * 1 approved
         * 2 declined
         * 3 paid
         * 4 Defaulted
         */
        String loan_status_name = "";
        if (loan_status.equals("pending")) {
            loan_status_name = "Pending";
            holder.tvStatus.setBackgroundResource(R.drawable.pending_status);
            holder.tvStatus.setTextColor(context.getResources().getColor(R.color.white));
        } else if (loan_status .equals("approved")) {
            loan_status_name = "Approved";
            holder.tvStatus.setBackgroundResource(R.drawable.rounded_corner);
            holder.tvStatus.setTextColor(context.getResources().getColor(R.color.white));
            //holder.tvStatus.setBackground(ContextCompat.getDrawable(context, R.drawable.circle_approved));
        } else if (loan_status.equals("declined")) {
            loan_status_name = "Declined";
            holder.tvStatus.setBackgroundResource(R.drawable.declined_status);
            holder.tvStatus.setTextColor(context.getResources().getColor(R.color.white));
        } else if (loan_status.equals("paid")) {
            loan_status_name = "Paid";
            holder.tvStatus.setBackgroundResource(R.drawable.rounded_corner);
            holder.tvStatus.setTextColor(context.getResources().getColor(R.color.white));
        } else if (loan_status.equals("defaulted")) {
            loan_status_name = "Defaulted";
            holder.tvStatus.setBackgroundColor(context.getResources().getColor(R.color.red));
            holder.tvStatus.setTextColor(context.getResources().getColor(R.color.white));
        }



        holder.tvStatus.setText(loan_status_name);
//        holder.tvBalance.setText(GroupDigits(balance));
        holder.tvLoanName.setText(loan_title);
        holder.tvAmount.setText(user_currency+" "+GroupDigits(amount));


        holder.loansLl.setOnClickListener(v -> {
            Bundle arg = new Bundle();
            arg.putString("loan", new Gson().toJson(datalist.get(position)));
            NavHostFragment.findNavController(fragment).navigate(R.id.loanFragment, arg);
        });
    }


    @Override
    public int getItemCount() {
        return datalist.size();

    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tvLoanName, tvAmount, tvStatus;
        LinearLayout loansLl;

        public ViewHolder(View itemView) {
            super(itemView);
            tvLoanName = itemView.findViewById(R.id.tvLoanName);
            tvAmount = itemView.findViewById(R.id.tvLoanAmount);
            tvStatus = itemView.findViewById(R.id.tvLoanStatus);
            loansLl = itemView.findViewById(R.id.loansLl);

        }
    }

}


