package com.app.ecobba.recyclerview_adapters;

import android.content.Context;
import android.util.Log;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class MyShopListAdapter <T,V extends RecyclerView.ViewHolder> extends RecyclerView.Adapter<V> {
    private static String TAG = "Gen_Adapter";
    List<T> data = new ArrayList<T>();
    private Context mContext;

    public interface AdapterInterface<T,V>{
        V createViewHolder(ViewGroup parent,int viewType);
        void bindViewHolder(V viewholder,T datum,int position);
    }

    private AdapterInterface<T,V> adapterInterface;
    private RecyclerView recyclerView;

    public MyShopListAdapter(AdapterInterface<T,V> adapterInterface,RecyclerView recyclerView){
        Log.e(TAG,"adapter created");
        this.adapterInterface = adapterInterface;
        this.recyclerView = recyclerView;
        if (this.recyclerView.getLayoutManager() == null) {
            this.recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext(), RecyclerView.VERTICAL, false));
        }
        if (this.recyclerView.getItemAnimator() == null) {
            this.recyclerView.setItemAnimator(new DefaultItemAnimator());
        }
        this.recyclerView.setAdapter(this);
    }

    @NonNull
    @Override
    public V onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
//        View view ;
//        LayoutInflater mInflater = LayoutInflater.from(mContext);
//        view = mInflater.inflate(R.layout.shop_list_items,parent,false);
        //final MyViewHolder viewHolder=new MyViewHolder(view);

        V viewHolder = adapterInterface.createViewHolder(parent,viewType);

        Log.e(TAG,viewHolder==null? "view null":"view created");
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull V holder, int position) {
        Log.e(TAG,"view bind called");
        adapterInterface.bindViewHolder(holder,data.get(position),position);
    }

    @Override
    public int getItemCount() {
        Log.e(TAG,"item count "+data.size());
        return data.size();
    }

    public void addData(List<T> data){
        Log.e(TAG,"try to add data size :"+data.size());
        recyclerView.post(()->{
            int oldSize = this.data.size();
            this.data.clear();
            this.data.addAll(data);
            int newSize = this.data.size();
            MyShopListAdapter.this.notifyDataSetChanged();
            Log.e(TAG,"data added");

            Log.e(TAG,"old : "+oldSize+" new : "+newSize);

        });
    }

}
