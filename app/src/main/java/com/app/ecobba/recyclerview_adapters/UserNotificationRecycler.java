package com.app.ecobba.recyclerview_adapters;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.ecobba.Activities.SingleNotificationActivity;
import com.app.ecobba.R;
import com.app.ecobba.recyclerview_models.UserNotifications;
import com.snov.timeagolibrary.PrettyTimeAgo;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

public class UserNotificationRecycler extends RecyclerView.Adapter<UserNotificationRecycler.ViewHolder>{

    Context context;
    List<UserNotifications> data;
    long datepower;

    public UserNotificationRecycler(List<UserNotifications> data, Context context){
        this.context = context;
        this.data = data;
    }

    public UserNotificationRecycler.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        return new UserNotificationRecycler.ViewHolder(LayoutInflater.from(parent.getContext()),parent);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final String message = data.get(position).getMessage(),
                date = data.get(position).getDate();
        Log.e("date",date);
        long datepowerd;

        String amount = message.substring(0, message.indexOf(" has")).replace(",", "");
        String t = message.substring(message.indexOf(" titled") + 7);

        Log.e("Loan_Amount_is", "Amount " + amount + " Title " + t);
        Log.e("Amount_Value_is",amount);
        Log.e("Loan_Title_is",t);

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
        sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
        Date testDate = null;
        try {
            testDate = sdf.parse(date);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        Log.e("BEg", String.valueOf(datepower));
        datepower = testDate.getTime();
        Log.e("Datw", String.valueOf(+datepower));
        PrettyTimeAgo timeAgo = new PrettyTimeAgo();
        String result = timeAgo.getTimeAgo(datepower);
        Log.e("Time", result);

        holder.message.setText(message);
        holder.date.setText(result);
        holder.rvNotificationsItem.setOnClickListener((view ->{
            Toast toast = Toast.makeText(context,"Loan Details", Toast.LENGTH_LONG);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
            Intent intent=new Intent(context, SingleNotificationActivity.class);
            //set values to intent
            intent.putExtra("loan_name_title",t);
            intent.putExtra("loan_amount",amount);
            intent.putExtra("date_borrowed",date);
            context.startActivity(intent);
        }));

    }

    @Override
    public int getItemCount() {
        return data.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        LinearLayout rvNotificationsItem;
        TextView message,date;
        public ViewHolder(LayoutInflater inflater, ViewGroup parent) {
            super(inflater.inflate(R.layout.list_item_notification, parent, false));
            message = itemView.findViewById(R.id.tvMessage);
            date = itemView.findViewById(R.id.tvDate);
            rvNotificationsItem=itemView.findViewById(R.id.rvNotificationsItem);
        }
    }

}
