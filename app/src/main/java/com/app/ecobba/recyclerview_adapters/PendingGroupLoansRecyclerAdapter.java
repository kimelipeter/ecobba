package com.app.ecobba.recyclerview_adapters;

import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.ecobba.R;

public class PendingGroupLoansRecyclerAdapter extends
        RecyclerView.Adapter<PendingGroupLoansRecyclerAdapter.ViewHolder> {

    public PendingGroupLoansRecyclerAdapter(){

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()),parent);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 0;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{

        public TextView loanName,repaymentPlan, minAmount, maxAmount,createdBy,interest;

        public ViewHolder(LayoutInflater inflater, ViewGroup parent) {
            super(inflater.inflate(R.layout.list_item_pending_loan, parent, false));
            loanName = itemView.findViewById(R.id.tvLoanName);
            repaymentPlan = itemView.findViewById(R.id.tvRepaymentPlan);
            minAmount = itemView.findViewById(R.id.tvMinAmount);
            maxAmount = itemView.findViewById(R.id.tvMaxAmount);
        }
    }
}
