package com.app.ecobba.recyclerview_adapters;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;


import androidx.annotation.NonNull;
;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;

import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.RecyclerView;


import com.app.ecobba.Models.GroupMeetings.Meeting;
import com.app.ecobba.R;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

public class GroupMeetingsAdapter extends RecyclerView.Adapter<GroupMeetingsAdapter.ViewHolder> {
    public List<Meeting> datalist;
    public Context context;
    public Fragment fragment;

    public GroupMeetingsAdapter(List<Meeting> datalist, Context context, Fragment fragment) {
        this.datalist = datalist;
        this.context = context;
        this.fragment = fragment;
    }

    @NonNull
@Override
public GroupMeetingsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

    LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
    View view = layoutInflater.inflate(R.layout.list_item_meeting,parent,false);
    return new GroupMeetingsAdapter.ViewHolder(view);
}



    @Override
    public void onBindViewHolder(final GroupMeetingsAdapter.ViewHolder holder, final int position) {

        Meeting meeting=datalist.get(position);
         String id = String.valueOf(meeting.getId());
         String status = meeting.getStatus();
         String venue = meeting.getVenue();
      //   String timeDifference = DataAdpter.get(position).getEnd_date_time();
         String attendance_array = String.valueOf(meeting.getAttendance());
         String dateString = meeting.getMeeting_date_time();
         String timeString = meeting.getMeeting_date_time();
         String meetingNameString = meeting.getMeeting_name();
         String group_id= String.valueOf(meeting.getGroup_id());
        String ready_for_collection= String.valueOf(meeting.getReady_for_collection());
        Log.e("Meeting_STATUSES",status);

        holder.tvMeetingName.setText(meetingNameString);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
       sdf.setTimeZone(TimeZone.getTimeZone("GMT"));

        try {
            Date date1 = sdf.parse(dateString);
            sdf = new SimpleDateFormat("dd MMMM yyyy");
            String date = sdf.format(date1);

            sdf =  new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");

            Date time = sdf.parse(timeString);
            sdf = new SimpleDateFormat("h:mm a");

            String time1 = sdf.format(time);

            Log.e("DateAndTime", "Date " + date + " Time " + time1);
            holder.tvTimeDifference.setText(time1);
            holder.tvDate.setText(date);
        } catch (ParseException e) {
            Log.e("DateAndTime", e.getLocalizedMessage());
            e.printStackTrace();
        }

        String group_status_name = "";
        holder.tvVenue.setText(venue);
        if (status.equals("upcoming")) {
            group_status_name = "Upcoming";
        } else if (status.equals("active")) {
            group_status_name = "Ended";
        }

//        if (ready_for_collection.equals("true")) {
//            group_status_name = "In Progress";
//            holder.tvStatus.setBackgroundColor(context.getResources().getColor(R.color.green));
//        } else {
//            holder.tvStatus.setBackgroundColor(context.getResources().getColor(R.color.primaryColor));
//        }
        holder.tvStatus.setText(group_status_name);


        holder.meetingLl.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)

            @Override
            public void onClick(View v) {
               if (status.equals("upcoming")){
                   Toast toast = Toast.makeText(context,"Wait for the meeting to start", Toast.LENGTH_LONG);
                   toast.setGravity(Gravity.CENTER, 0, 0);
                   toast.show();
               }
               else {

                   Bundle args = new Bundle();
                   args.putString("meeting_id", id);
                   args.putString("group_id", group_id);
                   args.putString("attendance_array", String.valueOf(attendance_array));
                   args.putString("status", status);
                   args.putString("ready_for_collection", ready_for_collection);

//                Pass data in bundle object as follows
//                pass fragment object as from parent fragment to use when calling the navcontroller
                   NavHostFragment.findNavController(fragment).navigate(R.id.meetingFragment, args);
               }

            }
        });


    }

    @Override
    public int getItemCount() {
        return datalist.size();
    }

    public  class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tvMeetingName,tvStatus, tvVenue, tvTimeDifference, tvDate, tvGroupBalance, tvGroupCreatedAt, tvGroupStatus;
        LinearLayout meetingLl;

        public ViewHolder(View itemView) {
            super(itemView);
            tvStatus = itemView.findViewById(R.id.tvStatus);
            tvVenue = itemView.findViewById(R.id.tvVenue);
            tvDate = itemView.findViewById(R.id.tvDate);
            tvMeetingName=itemView.findViewById(R.id.tvMeetingName);
            tvTimeDifference = itemView.findViewById(R.id.tvTimeDifference);
            meetingLl = itemView.findViewById(R.id.meetingLl);
        }
    }

}


