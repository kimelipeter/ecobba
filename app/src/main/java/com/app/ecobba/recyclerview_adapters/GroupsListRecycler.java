package com.app.ecobba.recyclerview_adapters;


import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.RecyclerView;

import com.app.ecobba.Models.Groups.Group;
import com.app.ecobba.R;
import com.app.ecobba.recyclerview_models.GroupsListAdapter;
import com.google.android.material.card.MaterialCardView;

import java.util.List;
import java.util.Random;

import static com.app.ecobba.common.SharedMethods.CheckIfEmptyOrNull;
import static com.app.ecobba.common.SharedMethods.GroupDigits;
import static com.app.ecobba.common.SharedMethods.showSweetAlertDialogError;

/**
 * Adapter to display recycler view.
 */
public class GroupsListRecycler extends RecyclerView.Adapter<GroupsListRecycler.ViewHolder> {

    public List<Group> datalist;
    public Context context;
    public Fragment fragment;

    public GroupsListRecycler(List<Group> datalist, Context context, Fragment fragment) {
        this.datalist = datalist;
        this.context = context;
        this.fragment = fragment;
    }

    @Override
    public GroupsListRecycler.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.list_item_group, parent, false);
        return new GroupsListRecycler.ViewHolder(view);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onBindViewHolder(final GroupsListRecycler.ViewHolder holder, final int position) {

        final String group_id = String.valueOf(datalist.get(position).getGroup().getId());
        final String group_name = datalist.get(position).getGroup().getName();
        final String group_comment = datalist.get(position).getGroup().getComment();
        final String group_status = String.valueOf(datalist.get(position).getGroup().getStatus());
        final String groupMembersCount = String.valueOf(datalist.get(position).getMembers());
        final String totalFines = "0";
        final String totalFees = String.valueOf(datalist.get(position).getTotalFees());
        final String totalHisa = String.valueOf(datalist.get(position).getTotalHisa());
        final String totalJamii = String.valueOf(datalist.get(position).getTotalJamii());
        final String totalGroupBalance = String.valueOf(datalist.get(position).getWallet());
        final String groupCreatedAt = datalist.get(position).getGroup().getCreated_at();

        String group_status_name = "";
        holder.tvGroupMembersCount.setText(GroupDigits(CheckIfEmptyOrNull(groupMembersCount)));
        holder.tv_group_name.setText(group_name);
        String groupNameChar = group_name;
        String sCharGroupName = groupNameChar.substring(0, 1);
        Log.e("First_letter_Name_is", sCharGroupName);
        holder.tv_group_name_char.setText(sCharGroupName.toUpperCase());
        Random r = new Random();
        int red = r.nextInt(255 - 0 + 1) + 0;
        int green = r.nextInt(255 - 0 + 1) + 0;
        int blue = r.nextInt(255 - 0 + 1) + 0;

        GradientDrawable draw = new GradientDrawable();
        draw.setShape(GradientDrawable.OVAL);
        draw.setColor(Color.rgb(red, green, blue));
        holder.tv_group_name_char.setBackground(draw);

        if (group_status.equals("1")) {
            /* you can uncomment the line below to see the status*/
            //group_status_name = "Active";
            holder.tvGroupStatus.setBackground(ContextCompat.getDrawable(context, R.drawable.circle_approved));
        } else {
            /* you can uncomment the line below to see the status*/
            //group_status_name = "Inactive";
            // holder.tvGroupStatus.setBackgroundColor(context.getResources().getColor(R.color.side_color));
            holder.tvGroupStatus.setBackground(ContextCompat.getDrawable(context, R.drawable.ic_not_activated));

        }
        holder.tvGroupStatus.setText(group_status_name);


        holder.groups_list_container.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)

            @Override
            public void onClick(View v) {
                if (group_status.equals("1")) {
                    Bundle args = new Bundle();
                    args.putString("id", group_id);
                    args.putString("group_name", group_name);
//                args.putString("totalFines", totalFines);
                    args.putString("totalFees", totalFees);
                    args.putString("totalGroupBalance", totalGroupBalance);
                    args.putString("groupMembersCount", groupMembersCount);
                    args.putString("totalHisa", totalHisa);
                    args.putString("totalJamii", totalJamii);
                    args.putString("groupMembersCount",groupMembersCount);

//                Pass data in bundle object as follows
//                pass fragment object as from parent fragment to use when calling the navcontroller
                    NavHostFragment.findNavController(fragment).navigate(R.id.groupFragment, args);
                } else {
                    showSweetAlertDialogError("You Can't Perform any operation till the group is activated",context);

                }


            }
        });

    }

    private void openFragment(Fragment fragment) {
        FragmentManager fragmentManager = ((FragmentActivity) context).getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.container, fragment).commit();
    }

    @Override
    public int getItemCount() {
        return datalist.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_group_name_char, tvGroupMembersCount, tv_group_name, tvTotalFines, tvTotalFees, tvGroupBalance, tvGroupCreatedAt, tvGroupStatus;
        CardView groups_list_container;

        public ViewHolder(View itemView) {
            super(itemView);
            tvGroupMembersCount = itemView.findViewById(R.id.tvGroupMembersCount);
            tv_group_name_char = itemView.findViewById(R.id.tv_group_name_char);
            tv_group_name = itemView.findViewById(R.id.tv_group_name);
            groups_list_container = itemView.findViewById(R.id.groups_list_container);
            // tvTotalFines = itemView.findViewById(R.id.tvTotalFines);
            // tvTotalFees = itemView.findViewById(R.id.tvTotalFees);
//            tvGroupBalance =  itemView.findViewById(R.id.tvGroupBalance);
//            tvGroupCreatedAt =  itemView.findViewById(R.id.tvGroupCreatedAt);
            tvGroupStatus = itemView.findViewById(R.id.tvGroupStatus);
//            civ = itemView.findViewById(R.id.card);
        }
    }

}


