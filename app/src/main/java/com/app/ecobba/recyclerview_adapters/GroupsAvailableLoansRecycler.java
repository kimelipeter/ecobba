package com.app.ecobba.recyclerview_adapters;


import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.RecyclerView;


import com.app.ecobba.Fragments.Group.Loans.ApplyGroupLoanBottomSheetDialog;
import com.app.ecobba.R;

import java.util.List;
import java.util.Random;

import com.app.ecobba.Models.GroupLoans.Package;

/**
 * Adapter to display recyclerview loans for the group.
 */
public class
GroupsAvailableLoansRecycler extends RecyclerView.Adapter<GroupsAvailableLoansRecycler.ViewHolder> {

    public List<Package> datalist;
    public Context context;
    public Fragment fragment;
    String loanLimit = "0";
    public String user_currency;

    public GroupsAvailableLoansRecycler(List<Package> datalist, Context context, Fragment fragment, String user_currency) {
        this.datalist = datalist;
        this.context = context;
        this.fragment = fragment;
        this.user_currency = user_currency;
    }

    @Override
    public GroupsAvailableLoansRecycler.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.list_item_loans, parent, false);
        return new GroupsAvailableLoansRecycler.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final GroupsAvailableLoansRecycler.ViewHolder holder, final int position) {
        String id = String.valueOf(datalist.get(position).getId());
        String group_id = String.valueOf(datalist.get(position).getGroup_id());
        String interest_rate = datalist.get(position).getInterest_rate() + "%";
        String loanName = datalist.get(position).getName();
        String fineChargeAmount = datalist.get(position).getFine_rate();
        String repayment_period = datalist.get(position).getRepayment_period();
        String repayment_plan = datalist.get(position).getRepayment_plan();
        String min = user_currency + " " + datalist.get(position).getMin_amount();
        String max = user_currency + " " + datalist.get(position).getMax_amount();

        holder.tvLoanName.setText(loanName);
        String loanNameChar = loanName;
        String sCharloanName = loanNameChar.substring(0, 1);
        Log.e("LOAN_NAME_IS :", sCharloanName);
        holder.tvFirstName.setText(sCharloanName.toUpperCase());
        Random r = new Random();
        int red = r.nextInt(255 - 0 + 1) + 0;
        int green = r.nextInt(255 - 0 + 1) + 0;
        int blue = r.nextInt(255 - 0 + 1) + 0;

        GradientDrawable draw = new GradientDrawable();
        draw.setShape(GradientDrawable.OVAL);
        draw.setColor(Color.rgb(red, green, blue));
        holder.tvFirstName.setBackground(draw);
        if (repayment_plan.equals("1")) {
            holder.tvRepayPlan.setText(context.getResources().getString(R.string.repay_plan) + ": " + context.getResources().getString(R.string.weekly));
        } else if (repayment_plan.equals("3")) {
            holder.tvRepayPlan.setText(context.getResources().getString(R.string.repay_plan) + ": " + context.getResources().getString(R.string.monthly));
        } else if (repayment_plan.equals("4")) {
            holder.tvRepayPlan.setText(context.getResources().getString(R.string.repay_plan) + ": " + context.getResources().getString(R.string.annually));
        }


        holder.LLloan.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onClick(View v) {

                FragmentManager manager = ((AppCompatActivity) context).getSupportFragmentManager();
                ApplyGroupLoanBottomSheetDialog applyGroupLoanBottomSheetDialog
                        = new ApplyGroupLoanBottomSheetDialog(id, loanName, min, max, group_id, fineChargeAmount, interest_rate, repayment_plan, repayment_period);
                applyGroupLoanBottomSheetDialog.show(manager, "TAG");

            }
        });


    }


    @Override
    public int getItemCount() {
        return datalist.size();
    }

    /**
     * Class containing all the views used here, textviews and all.
     */
    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tvRepayPlan, tvLoanName,tvFirstName;
        public LinearLayout LLloan;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvRepayPlan = itemView.findViewById(R.id.tvRepayPlan);
            tvLoanName = itemView.findViewById(R.id.tvLoanName);
            LLloan = itemView.findViewById(R.id.Lllayout);
            tvFirstName = itemView.findViewById(R.id.tvFirstName);


        }
    }

}


