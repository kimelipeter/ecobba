package com.app.ecobba.recyclerview_adapters;


import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.RecyclerView;

import com.app.ecobba.R;
import com.app.ecobba.recyclerview_models.AvailableLoansAdapter;

import java.util.List;

import javax.security.auth.callback.Callback;

import static com.app.ecobba.common.SharedMethods.GroupDigits;

/**
 * Adapter to display recycler view.
 */
public class AvailableLoansRecycler extends RecyclerView.Adapter<AvailableLoansRecycler.ViewHolder> {
    Context context;
    List<AvailableLoansAdapter> DataAdpter;
    Fragment fragment;
    public static String id;
    public static String loan_name;
    public static String min_amount;
    public static String max_amount;
    public static String interest_pa;
    public static String insured_status;
    public static String repayment_plan;

    public AvailableLoansRecycler(List<AvailableLoansAdapter> DataAapter, Context context, Fragment fragment) {
        this.DataAdpter = DataAapter;
        this.context = context;
        this.fragment = fragment;
    }

    @Override
    public AvailableLoansRecycler.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new AvailableLoansRecycler.ViewHolder(LayoutInflater.from(parent.getContext()), parent);
    }

    @Override
    public void onBindViewHolder(AvailableLoansRecycler.ViewHolder holder, final int position) {
        id = DataAdpter.get(position).getId();
        loan_name = DataAdpter.get(position).getLoan_name();
        min_amount = DataAdpter.get(position).getMin_amount();
        max_amount = DataAdpter.get(position).getMax_amount();
        interest_pa = DataAdpter.get(position).getInterest_per_annum();
        insured_status = DataAdpter.get(position).getInsured_status();
        repayment_plan = DataAdpter.get(position).getRepayment_plan();
        Log.e("LOAN_NAME_APPLY",loan_name);

        holder.tv_loan_name.setText(loan_name);
        String loanNameChar = loan_name;
        String sCharloanName = loanNameChar.substring(0, 1);
        Log.e("LOAN_NAME_IS :", sCharloanName);
        holder.tv_max_amount.setText(GroupDigits(max_amount));
        holder.tv_interest_pa.setText(interest_pa);

        if (repayment_plan.equals("3")){
            holder.repaymentPlan.setText("Monthly");
        }
        else if (repayment_plan.equals("2")){
            holder.repaymentPlan.setText("Bi-Weekly");
        }
        else if (repayment_plan.equals("4")){
            holder.repaymentPlan.setText("Anually");
        }
        else if (repayment_plan.equals("1")){
            holder.repaymentPlan.setText("Weekly");
        }
        holder.tv_min_amount.setText(min_amount);

        holder.btnApply.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onClick(View v) {
                Log.e("GOTO_APLLY", loan_name);

                Bundle args = new Bundle();
                args.putString("id", id);
                args.putString("loan_name", loan_name);
                args.putString("min_amount", min_amount);
                args.putString("max_amount", max_amount);
                args.putString("interest_pa", interest_pa);
                args.putString("insured_status", insured_status);
                args.putString("repayment_plan", repayment_plan);

                NavHostFragment.findNavController(fragment).navigate(R.id.applyLoanFragment, args);
            }
        });
    }


    @Override
    public int getItemCount() {
        return DataAdpter.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_loan_name, loan_name_fisrtletter, tv_min_amount, tv_max_amount, tv_interest_pa, tv_loans_action, repaymentPlan;

        CardView btnApplyLoan;
        Button btnApply;

        public ViewHolder(LayoutInflater inflater, ViewGroup parent) {
            super(inflater.inflate(R.layout.list_item_loanpackage_view, parent, false));
            tv_loan_name = itemView.findViewById(R.id.tv_loan_name);
            repaymentPlan = itemView.findViewById(R.id.repaymentPlan);
            tv_max_amount = itemView.findViewById(R.id.tv_max_amount);
            tv_min_amount = itemView.findViewById(R.id.tvMinAmount);
            tv_interest_pa = itemView.findViewById(R.id.tv_interest_pa);
           btnApplyLoan = itemView.findViewById(R.id.card);
           btnApply= itemView.findViewById(R.id.btnApplyLoan);




        }
    }

}


