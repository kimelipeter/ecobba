package com.app.ecobba.recyclerview_adapters;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.app.ecobba.R;
import com.app.ecobba.recyclerview_models.GroupsFinesAdapter;

import java.util.List;

/**
 * Adapter to display recycler view.
 */
public class GroupsFinesRecycler extends RecyclerView.Adapter<GroupsFinesRecycler.ViewHolder> {
    Context context;
    List<GroupsFinesAdapter> DataAdpter;

    public GroupsFinesRecycler(List<GroupsFinesAdapter> DataAapter, Context context) {
        this.DataAdpter = DataAapter;
        this.context = context;
    }

    @Override
    public GroupsFinesRecycler.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new GroupsFinesRecycler.ViewHolder(LayoutInflater.from(parent.getContext()), parent);
    }

    @Override
    public void onBindViewHolder(final GroupsFinesRecycler.ViewHolder holder, final int position) {

        final String fine_name = DataAdpter.get(position).getName();
        final String fine_amount = DataAdpter.get(position).getAmount();

        holder.name.setText(fine_name);
        holder.amount.setText(fine_amount);

    }

    @Override
    public int getItemCount() {
        return DataAdpter.size();
    }

    /**
     * Class containing all the views used here, textviews and all.
     */
    public static class ViewHolder extends RecyclerView.ViewHolder {
        CardView civ;
        TextView name,amount;
        public ViewHolder(LayoutInflater inflater, ViewGroup parent) {
            super(inflater.inflate(R.layout.list_item_fine, parent, false));
            name = itemView.findViewById(R.id.tvFineName);
            amount = itemView.findViewById(R.id.tvFineAmount);
        }
    }

}


