package com.app.ecobba.recyclerview_adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.app.ecobba.Models.LendingPackageDetails.Package;
import com.app.ecobba.R;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

public class MyLoansBorrowersListRecycler extends RecyclerView.Adapter<MyLoansBorrowersListRecycler.ViewHolder> {

    public List<Package> datalist;
    public Context context;
    public Fragment fragment;

    public MyLoansBorrowersListRecycler( List<Package> datalist, Context context, Fragment fragment) {
        this.datalist = datalist;
        this.context = context;
        this.fragment = fragment;
    }

    @Override
    public MyLoansBorrowersListRecycler.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.list_item_borrower, parent, false);
        return new MyLoansBorrowersListRecycler.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final MyLoansBorrowersListRecycler.ViewHolder holder, final int position) {

        String user_names = datalist.get(position).getUser().getName() + " " + datalist.get(position).getUser().getOther_names();
//        String amount = String.valueOf(datalist.get(position).getAmount());
//        String status = datalist.get(position).getStatus();
//        String currency = datalist.get(position).getLoan_package().getPackage_currency().getPrefix();
        String dateString = datalist.get(position).getCreated_at();

        String avatar=datalist.get(position).getUser().getAvatar();
        Log.e("ATARARAR", "onBindViewHolder: "+avatar);


        holder.firstname.setText(user_names);
//        holder.tvBorrowedAmount.setText(currency + " " + amount);
//        holder.tvBorrowedLoanStatus.setText(status);


        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        sdf.setTimeZone(TimeZone.getTimeZone("GMT"));

        try {
            Date date1 = sdf.parse(dateString);
            sdf = new SimpleDateFormat("dd MMMM yyyy");
            String date = sdf.format(date1);

            sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");

            Date time = sdf.parse(dateString);
            sdf = new SimpleDateFormat("h:mm a");

            String time1 = sdf.format(time);

            Log.e("DateAndTime", "Date " + date + " Time " + time1);

            holder.tvLoanBorrowedDate.setText(date + " " + time1);

        } catch (ParseException e) {
            Log.e("DateAndTime", e.getLocalizedMessage());
            e.printStackTrace();
        }

    }


    @Override
    public int getItemCount() {
        return datalist.size();
    }

    /**
     * Class containing all the views used here, textviews and all.
     */
    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView firstname, tvBorrowedAmount, tvBorrowedLoanStatus, tvLoanBorrowedDate;

        LinearLayout linearLayout;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            firstname = itemView.findViewById(R.id.firstname);
            tvBorrowedAmount = itemView.findViewById(R.id.tvBorrowedAmount);
            tvBorrowedLoanStatus = itemView.findViewById(R.id.tvBorrowedLoanStatus);
            tvLoanBorrowedDate = itemView.findViewById(R.id.tvTransactionDate);
            linearLayout = itemView.findViewById(R.id.layout_trxn);
        }
    }

}



