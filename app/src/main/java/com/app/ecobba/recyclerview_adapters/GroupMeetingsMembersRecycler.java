package com.app.ecobba.recyclerview_adapters;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.RecyclerView;

import com.app.ecobba.Models.Role;
import com.app.ecobba.Models.User;
import com.app.ecobba.R;
import com.app.ecobba.common.SharedMethods;
import com.app.ecobba.recyclerview_models.GroupMeetingsMembersAdapter;
import com.google.gson.Gson;

import org.json.JSONArray;

import java.util.List;

import static com.app.ecobba.common.SharedMethods.GroupDigits;

public class GroupMeetingsMembersRecycler extends RecyclerView.Adapter<GroupMeetingsMembersRecycler.ViewHolder> {
        Context context;
        List<GroupMeetingsMembersAdapter> DataAdpter;
        Fragment fragment;



    public GroupMeetingsMembersRecycler(List<GroupMeetingsMembersAdapter> DataAapter, Context context, Fragment fragment) {
            this.DataAdpter = DataAapter;
            this.context = context;
            this.fragment = fragment;
        }

        @Override
        public GroupMeetingsMembersRecycler.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new GroupMeetingsMembersRecycler.ViewHolder(LayoutInflater.from(parent.getContext()), parent);
        }




        @Override
        public void onBindViewHolder(final GroupMeetingsMembersRecycler.ViewHolder holder, final int position) {
            String role_id= SharedMethods.getDefaults("role",context);

            boolean isCoordinator = false;

            try{
                User loggedInUser = new Gson().fromJson(SharedMethods.getDefaults("USER-LOGIN",context),User.class);
                for (Role role:loggedInUser.getRoles()){
                    if (role.getId().equals("7") || role.getId().equals("11")){
                        isCoordinator = true;
                        Log.e("USER_ROLE_GROUP","is coordinator");
                    }
                }

            }catch (Exception e){
                Log.e("USER_ROLE_GROUP",e.toString());
            }

            if(!isCoordinator) {//not group coordinator
                holder.collect.setVisibility(View.GONE);
            }


            final String id = DataAdpter.get(position).getMemberId();
            final String group_id = DataAdpter.get(position).getGroup_id();
            final String attendance = DataAdpter.get(position).getAttendance_status();
            final String memberName = DataAdpter.get(position).getMemberName();
            final String meeting_id = DataAdpter.get(position).getMeeting_id();
            final int totalFines = DataAdpter.get(position).getTotalFines();
            final String totalHisa = DataAdpter.get(position).getTotalHisa();
            final String totalJamii = DataAdpter.get(position).getTotalJamii();
            final String memberShipfee = DataAdpter.get(position).getTotalMemberShipfee();
            final String meetingStatus = DataAdpter.get(position).getMeeting_status();
            final JSONArray meetings_array = DataAdpter.get(position).getMeeting_fines();
            final String ready_for_collection = DataAdpter.get(position).getReady_for_collection();
            final boolean status = DataAdpter.get(position).getStatus();

            if(ready_for_collection.equals("1")){ // Meeting is ended, hide collect field

                Log.e("MEETING STATUS",meetingStatus);
                holder.collect.setVisibility(View.VISIBLE);

            }else {
                Log.e("MEETING STATUS COLLECT",meetingStatus);
            }

            if (status){
                holder.collect.setVisibility(View.GONE);

            }

            holder.tvMembershipFee.setText(GroupDigits(memberShipfee));
            holder.tvAttendance.setText(attendance);
            holder.tvTotalFines.setText(GroupDigits(String.valueOf(totalFines)));
            holder.tvMemberName.setText(memberName);
            holder.tvJamii.setText(GroupDigits(totalJamii));
            holder.tvHisa.setText(GroupDigits(totalHisa));

            holder.collect.setOnClickListener(v -> {
                Log.d("MEMBER NAME ","----------------     -----------------------");
//              Fragment homeFragment = GroupSettingsFragment.newInstance();
                Bundle args = new Bundle();
                args.putString("member_id", id );
                args.putString("groupFinesArray",meetings_array.toString());
                args.putString("meeting_id",meeting_id);
                args.putString("group_id",group_id);
//              Pass data in bundle object as follows
//              pass fragment object as from parent fragment to use when calling the navcontroller
                NavHostFragment.findNavController(fragment).navigate(R.id.collectionFragment,args);
            });


        }

        @Override
        public int getItemCount() {
            return DataAdpter.size();
        }

        /**
         * Class containing all the views used here, textviews and all.
         */
        public static class ViewHolder extends RecyclerView.ViewHolder {
            public TextView tvMembershipFee, tvAttendance,tvTotalFines,tvMemberName,tvJamii,tvHisa;
            CardView civ; Button collect;

            public ViewHolder(LayoutInflater inflater, ViewGroup parent) {
                super(inflater.inflate(R.layout.list_item_user_meeting, parent, false));
                tvMembershipFee =  itemView.findViewById(R.id.tvMembershipFee);
                tvAttendance = itemView.findViewById(R.id.tvAttendance);
                tvTotalFines = itemView.findViewById(R.id.tvTotalFines);
                tvMemberName =  itemView.findViewById(R.id.tvMemberName);
                tvJamii = itemView.findViewById(R.id.tvJamii);
                tvHisa = itemView.findViewById(R.id.tvHisa);
                collect = itemView.findViewById(R.id.collect);
                civ = itemView.findViewById(R.id.cvCardHeader);
            }
        }

    }



