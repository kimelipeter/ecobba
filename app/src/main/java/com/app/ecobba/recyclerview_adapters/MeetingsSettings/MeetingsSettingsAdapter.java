package com.app.ecobba.recyclerview_adapters.MeetingsSettings;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.ecobba.Fragments.Group.Settings.MeetingSettingFragment;

import com.app.ecobba.R;
import com.app.ecobba.recyclerview_models.resources.Data;
import com.app.ecobba.recyclerview_models.resources.articles.ArticlesRecyclerViewAdapter;

import java.util.ArrayList;
import java.util.List;

public class MeetingsSettingsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {



     Context context;
   /// private List<CurrentSettings> list =new ArrayList<>();
    int imageView = 1;
private View.OnClickListener onClickListener;
    public MeetingsSettingsAdapter(View.OnClickListener onClickListener, Context context) {
        this.onClickListener = onClickListener;
        this.context=context;
    }
//    public void setList(List<CurrentSettings> newList) {
//        list.clear();
//        list.addAll(newList);
//        // list.get(0).setAttachmenttype("video");  //was testing videos
//        notifyDataSetChanged();
//    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new RecyclerView.ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.list_meeting_settings, parent, false)) {
        };
    }



    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        //CurrentSettings currentSetting = null;
//
//        if (holder.getItemViewType() == imageView) {
//          ImageViewHolder imageViewHolder = (ImageViewHolder) holder;
//            imageViewHolder.bind(currentSetting);
//        }

    }

    @Override
    public int getItemCount() {
       // return list.size();
        return 0;
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    public class ImageViewHolder extends RecyclerView.ViewHolder{
        TextView tvMeetingName,tvVenue,tvFrequency,tvDay,tvTime,tvPeriods;

        public ImageViewHolder(@NonNull View itemView) {
            super(itemView);
            tvMeetingName = itemView.findViewById(R.id.tvMeetingName);
            tvVenue = itemView.findViewById(R.id.tvVenue);
            tvFrequency = itemView.findViewById(R.id.tvFrequency);
            tvDay = itemView.findViewById(R.id.tvDay);
            tvTime = itemView.findViewById(R.id.tvTime);
            tvPeriods = itemView.findViewById(R.id.tvPeriods);
        }

//        public void bind(CurrentSettings currentSettings){
//            tvMeetingName.setText(currentSettings.getData().component1().getName());
//
//        }
    }


}
