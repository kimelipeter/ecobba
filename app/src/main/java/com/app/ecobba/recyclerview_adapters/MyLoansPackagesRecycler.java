package com.app.ecobba.recyclerview_adapters;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.RecyclerView;

import com.app.ecobba.Fragments.Borrowing.Adapters.AvailableLoansAdapter;
import com.app.ecobba.Models.Lending.LoanPackage;
import com.app.ecobba.R;
import com.app.ecobba.recyclerview_models.MyLoansPackagesAdapter;

import java.util.List;

public class MyLoansPackagesRecycler extends RecyclerView.Adapter<MyLoansPackagesRecycler.ViewHolder> {

  public   List<LoanPackage> datalist;
  public   Context context;
  public   Fragment fragment;

    public MyLoansPackagesRecycler(List<LoanPackage> datalist, Context context, Fragment fragment) {
        this.datalist = datalist;
        this.context = context;
        this.fragment = fragment;
    }

    @Override
    public MyLoansPackagesRecycler.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.list_item_loanpackage,parent,false);
        return new MyLoansPackagesRecycler.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final MyLoansPackagesRecycler.ViewHolder holder, final int position) {
        final String loan_id = String.valueOf(datalist.get(position).getId());
        final String loan_name = datalist.get(position).getName();
        final String repayment_plan = datalist.get(position).getRepayment_plan();
        Log.e("REPAY_PLAN", "onBindViewHolder: "+repayment_plan );
        final String interest_rate = datalist.get(position).getInterest_rate()+"%";
        final String created_on = datalist.get(position).getCreated_at();
        final String status = String.valueOf(datalist.get(position).getStatus());
        final String insured_status = String.valueOf(datalist.get(position).getInsured());
        final String currency = datalist.get(position).getPackage_currency().getPrefix();
        final String min_credit = String.valueOf(datalist.get(position).getMin_credit_score());

        String loanNameChar=loan_name;
        String sCharloanName=loanNameChar.substring(0,1);
        Log.e("First letter Name is :",sCharloanName);
        holder.tv_loan_char.setText(sCharloanName.toUpperCase());
        holder.tv_loan_name.setText(loan_name);

        holder.tv_interest_rate.setText(interest_rate);
//        holder.tv_created_on.setText(created_on);
        holder.tv_currency.setText(currency);
        holder.tv_min_credit.setText(min_credit);
        if (repayment_plan.equals("1")){
            holder.tv_repayment_plan.setText("Weekly");
        }
        else if (repayment_plan.equals("3")){
            holder.tv_repayment_plan.setText("Monthly");
        }
        else if (repayment_plan.equals("4")){
            holder.tv_repayment_plan.setText("Annually");
        }


        holder.civ.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onClick(View v) {
                Bundle args = new Bundle();
                args.putString("id", loan_id );
                args.putString("package_name", loan_name );
                args.putString("interest_rate", interest_rate );
                args.putString("repayment_plan", repayment_plan );
                args.putString("insured_status", insured_status );
                args.putString("package_status", status );
                args.putString("currency",currency);
                args.putString("min_credit_score", String.valueOf(datalist.get(position).getMin_credit_score()));
                args.putString("borrowers_number", datalist.get(position).getFine_rate());
                args.putString("loanRequests", String.valueOf(datalist.get(position).getRepaymentplan()));

//                Pass data in bundle object as follows
//                pass fragment object as from parent fragment to use when calling the navcontroller
                NavHostFragment.findNavController(fragment).navigate(R.id.packageFragment,args);


            }
        });
    }
    private void openFragment(Fragment fragment) {

        FragmentManager fragmentManager = ((FragmentActivity)context).getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.container, fragment).commit();
    }

    @Override
    public int getItemCount() {

        return datalist.size();
    }

    /**
     * Class containing all the views used here, textviews and all.
     */
    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_loan_char, tv_loan_name, tv_repayment_plan,tv_interest_rate,tv_created_on,tv_loan_action,tv_currency,tv_min_credit;
        CardView civ;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tv_loan_char=itemView.findViewById(R.id.loan_char);
            tv_loan_name =  itemView.findViewById(R.id.tvLoanName);
            tv_repayment_plan = itemView.findViewById(R.id.tv_repayment_plan);
            tv_interest_rate = itemView.findViewById(R.id.tv_interest_rate);
//            tv_created_on = itemView.findViewById(R.id.tv_created_at);
            tv_currency = itemView.findViewById(R.id.tv_currency);
            tv_min_credit = itemView.findViewById(R.id.tvMinCredit);
            civ = itemView.findViewById(R.id.card);
        }
    }

}



