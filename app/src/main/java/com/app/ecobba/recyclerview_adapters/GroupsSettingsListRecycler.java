package com.app.ecobba.recyclerview_adapters;


import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.RecyclerView;

import com.app.ecobba.R;
import com.app.ecobba.common.SharedMethods;
import com.app.ecobba.recyclerview_models.GroupsSettingsListAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.app.ecobba.common.ApisKtKt.disableSetting;
import static com.app.ecobba.common.ApisKtKt.enableSetting;
import static com.app.ecobba.common.AppConstantsKt.INITIATOR_DISABLE_SETTING;
import static com.app.ecobba.common.AppConstantsKt.INITIATOR_ENABLE_SETTING;
import static com.app.ecobba.common.SharedMethods.HTTP_POST;
import static com.app.ecobba.common.SharedMethods.global_error_message;
import static com.app.ecobba.common.SharedMethods.postToServer;
import static com.app.ecobba.common.SharedMethods.runOnUI;
import static com.app.ecobba.common.SharedMethods.showSweetAlertDialogError;
import static com.app.ecobba.common.SharedMethods.showSweetAlertDialogSuccess;

/**
 * Adapter to display recycler view.
 */
public class GroupsSettingsListRecycler extends RecyclerView.Adapter<GroupsSettingsListRecycler.ViewHolder> implements SharedMethods.NetworkObjectListener {
    Context context;
    List<GroupsSettingsListAdapter> DataAdpter;
    String groupId,token;
    NavController navigate;

    public GroupsSettingsListRecycler(List<GroupsSettingsListAdapter> DataAapter, String groupId, String token, Context context, Fragment fragment) {
        this.DataAdpter = DataAapter;
        this.context = context;
        this.groupId = groupId;
        this.token = token;
        navigate = NavHostFragment.findNavController(fragment);
    }

    @Override
    public GroupsSettingsListRecycler.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new GroupsSettingsListRecycler.ViewHolder(LayoutInflater.from(parent.getContext()), parent);
    }

    @Override
    public void onBindViewHolder(final GroupsSettingsListRecycler.ViewHolder holder, final int position) {
        final String id = DataAdpter.get(position).getId();
        final String group_settings_name = DataAdpter.get(position).getSettings_name();
        final String group_settings_description = DataAdpter.get(position).getSettings_description();

        holder.tvName.setText(group_settings_name);
        holder.tvDescription.setText(group_settings_description);
        holder.swEnable.setChecked(DataAdpter.get(position).getIs_enabled());
        holder.swEnable.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                String params = "{" +
                        "\"setting_id\":" + '"' + id + '"'+
                        ",\"group_id\":" + '"' + groupId + '"'+
                        "}";
                Log.e("CHANGE_SETTING",params);
                if (b){

                    postToServer(enableSetting,token,params,INITIATOR_ENABLE_SETTING,HTTP_POST,context,GroupsSettingsListRecycler.this);
                }else{
                    Map<String, String> _params = new HashMap<String, String>();
                    _params.put("token", token);
                    _params.put("param",params);
                    Log.e("CHANGE_SETTING",_params.toString());
                    postToServer(disableSetting,token,params,INITIATOR_DISABLE_SETTING,HTTP_POST,context,GroupsSettingsListRecycler.this);
                }
            }
        });

        holder.civ.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle args = new Bundle();
                args.putString("args",id+"/"+groupId);
                args.putString("id",id);
                args.putString("group_id",groupId);
                Log.e("args",args.getString("args"));
                switch(id){
                    case "1":
                        navigate.navigate(R.id.meetingSettingFragment,args);
                        break;
                    case "2":
                        navigate.navigate(R.id.contributionsSettingFragment,args);
                        navigate.navigate(R.id.contributionsListFragment2,args);
                        break;
                    case "3":
                        navigate.navigate(R.id.loansSettingFragment,args);
                        break;
                    case "4":
                        navigate.navigate(R.id.finesSettingFragment,args);
                        break;
                    case "5":
                        navigate.navigate(R.id.feesSettingFragment,args);
                        break;
                    case "6":
                        navigate.navigate(R.id.loansSettingFragment,args);
                        break;
//                        showSweetAlertDialogError("Not Available",context);
//                        break;
                    case "7":
                        showSweetAlertDialogError("Not Available",context);
                        break;
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return DataAdpter.size();
    }

    @Override
    public void onDataReady(String initiator, String response) {
        if (callback!=null){
            callback.settingChanged();
        }
        switch (initiator){
            case INITIATOR_ENABLE_SETTING:
                try{
                    runOnUI(new Runnable() {
                        @Override
                        public void run() {
                            Log.e(initiator,response);
                            try {
                                JSONObject jObj = new JSONObject(response);
                                if (jObj.has("errors")) {
                                    String errors = jObj.getString("errors");
                                    showSweetAlertDialogError(errors, context);
                                } else { // if no error message was found in the response,
                                    boolean success = jObj.getBoolean("success");
                                    String api_error_message = jObj.getString("message");
                                    showSweetAlertDialogSuccess(api_error_message, context);
                                }
                            }catch (JSONException e){
                                e.printStackTrace();
                                Log.e(initiator,e.toString());
                                showSweetAlertDialogError(global_error_message, context);
                            }
                        }
                    });
                }catch (Exception e){
                    e.printStackTrace();
                    Log.e(initiator,e.toString());
                    showSweetAlertDialogError(global_error_message, context);
                }
                break;
            case INITIATOR_DISABLE_SETTING:
                try{
                    runOnUI(new Runnable() {
                        @Override
                        public void run() {
                            Log.e("initiator_res",response);
                            try {
                                JSONObject jObj = new JSONObject(response);
                                if (jObj.has("errors")) {
                                    String errors = jObj.getString("errors");
                                    showSweetAlertDialogError(errors, context);
                                } else { // if no error message was found in the response,
                                    boolean success = jObj.getBoolean("success");
                                    String api_error_message = jObj.getString("message");
                                    showSweetAlertDialogSuccess(api_error_message, context);
                                }
                            }catch (JSONException e){
                                e.printStackTrace();
                                Log.e(initiator,e.toString());
                                showSweetAlertDialogError(global_error_message, context);
                            }
                        }
                    });
                }catch (Exception e){
                    e.printStackTrace();
                    Log.e(initiator,e.toString());
                    showSweetAlertDialogError(global_error_message, context);
                }
                break;
        }

    }

    public interface OnSettingChanged{
        void settingChanged();
    }

    private OnSettingChanged callback;

    public GroupsSettingsListRecycler setOnSettingChanged(OnSettingChanged onSettingChanged){
        callback=onSettingChanged;
        return this;
    }

    @Override
    public void onDataLoaded(String initiator, JSONArray data) {

    }

    @Override
    public void onNetworkError(String initiator, String errorCode, String errorMessage) {
        try{
            runOnUI(new Runnable() {
                @Override
                public void run() {
//                                    hideDialog(ctx);
                    showSweetAlertDialogError(errorMessage, context);
                }
            });
        }catch (Exception e){
//                            hideDialog(ctx);
            showSweetAlertDialogError(global_error_message, context);
            Log.e(initiator, e.toString());
//                    Toast.makeText(ctx, "Login Error Response Exception" + e.toString(), Toast.LENGTH_LONG).show();
        }
    }

    /**
     * Class containing all the views used here, textviews and all.
     */
    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tvName,tvDescription;
        Switch swEnable;
        LinearLayout civ;

        public ViewHolder(LayoutInflater inflater, ViewGroup parent) {
            super(inflater.inflate(R.layout.list_item_group_setting, parent, false));
            tvName = itemView.findViewById(R.id.tvSettingName);
            tvDescription = itemView.findViewById(R.id.tvSettingDescription);
            swEnable = itemView.findViewById(R.id.swEnable);
            civ = itemView.findViewById(R.id.llSetting);
        }
    }

}


