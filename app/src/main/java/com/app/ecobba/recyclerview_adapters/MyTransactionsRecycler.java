package com.app.ecobba.recyclerview_adapters;


import android.content.Context;
import android.os.Build;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.app.ecobba.Fragments.Wallet.WalletFragment;
import com.app.ecobba.Models.User;
import com.app.ecobba.Models.UserWallet.Transaction;
import com.app.ecobba.R;
import com.app.ecobba.recyclerview_models.UserTransactionsAdapter;
import com.snov.timeagolibrary.PrettyTimeAgo;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import static com.app.ecobba.common.SharedMethods.GroupDigits;


public class MyTransactionsRecycler extends RecyclerView.Adapter<MyTransactionsRecycler.ViewHolder> {

    public List<Transaction> datalist;
    public Context context;
    public Fragment fragment;
    public String user_currency;
    long datepower;


    public MyTransactionsRecycler(List<Transaction> datalist, Context context, Fragment fragment,String user_currency) {
        this.datalist = datalist;
        this.context = context;
        this.fragment = fragment;
        this.user_currency=user_currency;


    }

    @Override
    public MyTransactionsRecycler.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MyTransactionsRecycler.ViewHolder(LayoutInflater.from(parent.getContext()), parent);
    }

    @Override
    public void onBindViewHolder(final MyTransactionsRecycler.ViewHolder holder, final int position) {
        String trx_code = datalist.get(position).getTxn_code();
        String amount = String.valueOf(datalist.get(position).getAmount());
        String date = datalist.get(position).getCreated_at();

        String txn_type = String.valueOf(datalist.get(position).getTxn_type());
        String status = String.valueOf(datalist.get(position).getStatus());
        Log.e("Tranx", txn_type);

        // String postdate = date;
        Log.e("date", date);
        long datepowerd;

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
        sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
        Date testDate = null;
        try {
            testDate = sdf.parse(date);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        Log.e("BEg", String.valueOf(datepower));
        datepower = testDate.getTime();
        Log.e("Datw", String.valueOf(+datepower));
        PrettyTimeAgo timeAgo = new PrettyTimeAgo();
        String result = timeAgo.getTimeAgo(datepower);
//            Log.e("Time", result);
        // holder.txttime.setText(result);

//
//        if (datalist.get(position).getType().equals("credit")) {
//            Log.e("Tranx", txn_type);
//            holder.deposit.setVisibility(View.GONE);
//            holder.withdraw.setVisibility(View.VISIBLE);
//
//        } else {
//            holder.deposit.setVisibility(View.VISIBLE);
//            holder.withdraw.setVisibility(View.GONE);
//
//        }

        holder.tvTrxCode.setText(trx_code);
        holder.tvTrxAmount.setText(user_currency+" "+amount);

        String transaction_type = "";
        if (txn_type.equals("1")) {// credit
            transaction_type = "Credit";
            holder.tvTransType.setText("Credit");

        } else if (txn_type.equals("2")) { //debit
            holder.tvTransType.setText("Debit");

        }
        holder.trxStatus.setText(status);
//        holder.balanceCurrency.setText(WalletFragment.currencyValue);

        if (result == null) {
            holder.tvTransactionDate.setText("just now");
        } else {
            holder.tvTransactionDate.setText(result);
        }



        holder.linearLayout.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onClick(View v) {
//                    Intent intent = new Intent(context, JobDetailsPage.class);
//                    intent.putExtra("id_request", DataAdpter.get(position).getId_request());
//                    intent.putExtra("INITIATOR_PAGE", initiator_page);
//                    context.startActivity(intent);

            }
        });
    }


    @Override
    public int getItemCount() {
        return datalist.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tvTrxAmount, trxStatus, tvTrxCode, tvTransType, tvTransactionDate, balanceCurrency;
        // CardView civ;
        LinearLayout linearLayout;
        ImageView deposit, withdraw;

        public ViewHolder(LayoutInflater inflater, ViewGroup parent) {
            super(inflater.inflate(R.layout.item_list_messages, parent, false));
            tvTrxAmount = itemView.findViewById(R.id.tvTrxAmount);
            balanceCurrency = itemView.findViewById(R.id.balanceCurrency);
            trxStatus = itemView.findViewById(R.id.trxStatus);
            tvTrxCode = itemView.findViewById(R.id.tvTrxCode);
            tvTransactionDate = itemView.findViewById(R.id.tvTransactionDate);
            tvTransType = itemView.findViewById(R.id.tvTransType);
            deposit = itemView.findViewById(R.id.trxn_deposit_image);
            withdraw = itemView.findViewById(R.id.trxn_withdraw_image);
            // civ = itemView.findViewById(R.id.viewcard);
            linearLayout = itemView.findViewById(R.id.layout_trxn);
        }
    }

}


