package com.app.ecobba.ViewModel;

import android.annotation.SuppressLint;
import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.app.ecobba.Models.UserSummary.UserSummaryResponse;
import com.app.ecobba.common.rxApi.Api;
import com.app.ecobba.common.rxApi.ApiService;
import com.app.ecobba.common.rxApi.RxApi;

import io.reactivex.Observable;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

import static com.app.ecobba.common.rxApi.RxUtility.compose;

public class DashBoardViewModel extends ViewModel {

    private static final String TAG = "DashBoardViewModel";
    CompositeDisposable disposable = new CompositeDisposable();
    public MutableLiveData<UserSummaryResponse> userSummaryResponseMutableLiveData;

    public DashBoardViewModel(){
        userSummaryResponseMutableLiveData = new MutableLiveData<>();
    }

    public LiveData<UserSummaryResponse> getUserSummaryResponseMutableLiveData(){
        return userSummaryResponseMutableLiveData;
    }

    @SuppressLint("CheckResults")
    public void getDashBoard(){
        disposable.add(compose(new RxApi().getApi().getUserSummary(),
                userSummaryResponse -> {
            userSummaryResponseMutableLiveData.postValue((UserSummaryResponse) userSummaryResponse);
                },
                error -> {
                    Log.d(TAG, "getDashBoard: " + error.getLocalizedMessage());
                }));

    }

    @Override
    protected void onCleared() {
        disposable.clear();
        super.onCleared();
    }
}
