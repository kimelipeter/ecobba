package com.app.ecobba.ViewModel;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.app.ecobba.Models.RegistrationData.Classe;
import com.app.ecobba.Models.RegistrationData.Country;
import com.app.ecobba.Models.RegistrationData.Doc;
import com.app.ecobba.Models.RegistrationData.Gender;
import com.app.ecobba.Models.RegistrationData.MaritalStatuse;
import com.app.ecobba.Models.RegistrationData.RegistrationDataResponse;
import com.app.ecobba.Models.RegistrationData.ResidentType;
import com.app.ecobba.Models.UserProfile.UserProfileResponse;
import com.app.ecobba.Utils.Resource;
import com.app.ecobba.common.rxApi.Api;
import com.app.ecobba.common.rxApi.RxApi;
import com.app.ecobba.common.rxApi.RxUtility;

import java.util.List;

import io.reactivex.disposables.CompositeDisposable;
import retrofit2.HttpException;

import static com.app.ecobba.common.rxApi.RxUtility.*;

public class UserProfileViewModel extends ViewModel {

    private static final String TAG = "ProfileViewModel";
    public CompositeDisposable compositeDisposable = new CompositeDisposable();
    public MutableLiveData<UserProfileResponse> userProfileResponseMutableLiveData;
    public MutableLiveData<List<MaritalStatuse>> userMaritalStatusLiveData;
    public MutableLiveData<List<Gender>> userGenderMutableLiveData;
    public MutableLiveData<List<Doc>> userDocsMutableLiveData;
    public MutableLiveData<List<ResidentType>> userResidentMutableLiveData;
    public MutableLiveData<List<Country>> userCountryMutableLiveData;
    public MutableLiveData<List<Classe>> userIncomeClasses;

    public UserProfileViewModel() {
        userProfileResponseMutableLiveData = new MutableLiveData<>();
        userGenderMutableLiveData = new MutableLiveData<>();
        userMaritalStatusLiveData = new MutableLiveData<>();
        userDocsMutableLiveData = new MutableLiveData<>();
        userCountryMutableLiveData = new MutableLiveData<>();
        userResidentMutableLiveData = new MutableLiveData<>();
        userIncomeClasses = new MutableLiveData<>();
    }

    public LiveData<UserProfileResponse> userProfileResponseLiveData() {
        return userProfileResponseMutableLiveData;
    }

    public LiveData<List<MaritalStatuse>> userMaritalResponseLiveData() {
        return userMaritalStatusLiveData;
    }

    public LiveData<List<Gender>> userGenderResponseLiveData() {
        return userGenderMutableLiveData;
    }

    public LiveData<List<Doc>> userDocResponseLiveData() {
        return userDocsMutableLiveData;
    }

    public LiveData<List<Country>> userCountryResponseLiveData() {
        return userCountryMutableLiveData;
    }

    public LiveData<List<ResidentType>> userResidentResponseLiveData() {
        return userResidentMutableLiveData;
    }

    public LiveData<List<Classe>> userIncome() {
        return userIncomeClasses;
    }

    public void getUserProfile() {
        compositeDisposable.add(compose(new RxApi().getApi().userProfile(),
                response -> {
                    userProfileResponseMutableLiveData.postValue(response);
                },
                error -> {
                    userProfileResponseMutableLiveData.postValue(null);
                }));
    }

    public void getUserMaritalStatus() {
        compositeDisposable.add(compose(new RxApi().getApi().getRegistrationData(),
                response -> {

                    List<MaritalStatuse> maritalStatuses = response.getMessage().getMarital_statuses();

                    userMaritalStatusLiveData.postValue(maritalStatuses);
                },
                error -> {
                    Log.d(TAG, "getUserMaritalStatus: " + error.getLocalizedMessage());

                }));
    }

    public void getUserGender() {
        compositeDisposable.add(compose(new RxApi().getApi().getRegistrationData(),
                response -> {

                    List<Gender> genders = response.getMessage().getGenders();

                    userGenderMutableLiveData.postValue(genders);
                },
                error -> {
                    Log.d(TAG, "getUserGender: " + error.getLocalizedMessage());

                }));
    }

    public void getUserIdentificationDocs() {
        compositeDisposable.add(compose(new RxApi().getApi().getRegistrationData(),
                response -> {

                    List<Doc> docs = response.getMessage().getDocs();

                    userDocsMutableLiveData.postValue(docs);
                },
                error -> {
                    Log.d(TAG, "getUserGender: " + error.getLocalizedMessage());

                }));
    }

    public void getUserCountry() {
        compositeDisposable.add(compose(new RxApi().getApi().getRegistrationData(),
                response -> {
                    List<Country> countries = response.getMessage().getCountries();

                    userCountryMutableLiveData.postValue(countries);


                },
                error -> {
                    Log.d(TAG, "getUserGender: " + error.getLocalizedMessage());

                }));
    }

    public void getUserResidentType() {
        compositeDisposable.add(compose(new RxApi().getApi().getRegistrationData(),
                response -> {
                    List<ResidentType> residentTypes = response.getMessage().getResident_types();

                    userResidentMutableLiveData.postValue(residentTypes);

                },
                error -> {
                    Log.d(TAG, "getUserGender: " + error.getLocalizedMessage());

                }));
    }

    public void getUserIncome() {
        compositeDisposable.add(compose(new RxApi().getApi().getRegistrationData(),
                response -> {

                    List<Classe> income = response.getMessage().getClasses();
                    userIncomeClasses.postValue(income);

                },
                error -> {
                    Log.d(TAG, "getUserGender: " + error.getLocalizedMessage());

                }));
    }

    @Override
    protected void onCleared() {
        compositeDisposable.clear();
        super.onCleared();
    }
}
