package com.app.ecobba.Dialogs;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.app.ecobba.R;
import com.app.ecobba.common.SharedMethods;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import java.util.List;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;

public class PinDialogFragment extends DialogFragment implements View.OnClickListener {

    @BindViews({
            R.id.tiOld,
            R.id.tiNew,
            R.id.tiConfirm,
    })
    List<TextInputEditText> inputs;

    @BindView(R.id.btnConfirm)
    Button confirm;
    @BindView(R.id.btCancel)
    Button cancel;

    @BindView(R.id.tilOld)
    TextInputLayout tilOld;

    String pin;
    boolean mode;

    public PinDialogFragment(boolean mode){
        this.mode = mode;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View dialog = inflater.inflate(R.layout.dialog_create_change_pin,container,false);
        ButterKnife.bind(this,dialog);
        return dialog;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        confirm.setOnClickListener(this);
        cancel.setOnClickListener(this);
        if (!mode){
            tilOld.setVisibility(View.GONE);
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btnConfirm:
                if (validate()) {
                    SharedMethods.storePrefs("PIN", pin, getContext());
                    if (callback!=null){
                        callback.onConfirm();
                    }
                    dismiss();
                }
                break;
            case R.id.btCancel:
                dismiss();
                break;
        }
    }

    private boolean validate(){
        boolean val = true;
        String old = inputs.get(0).getText().toString(),
                _new = inputs.get(1).getText().toString(),
                confirm = inputs.get(2).getText().toString();

        pin = _new;

        if (mode) {
            if (old.isEmpty()) {
                inputs.get(0).setError(getString(R.string.emptyField));
                val = false;
            } else {
                String _old = SharedMethods.getDefaults("PIN", getContext());
                if (!_old.equals(old)) {
                    inputs.get(0).setError(getString(R.string.wrong_pin));
                    val = false;
                }
            }
        }

        if (_new.isEmpty()){
            inputs.get(1).setError(getString(R.string.emptyField));
            val = false;
        }
        if (confirm.isEmpty()){
            inputs.get(2).setError(getString(R.string.emptyField));
            val = false;
        }

        if (!_new.equals(confirm)){
            inputs.get(2).setError(getString(R.string.confirm_password));
            val = false;
        }

        return val;
    }

    public interface OnConfirm{
        void onConfirm();
    }

    private OnConfirm callback;

    public PinDialogFragment SetOnConfirm(OnConfirm c){
        callback = c;
        return this;
    }

}
