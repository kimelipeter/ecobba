package com.app.ecobba.Dialogs.Money;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.app.ecobba.Models.NetworksResponse;
import com.app.ecobba.Models.User;
import com.app.ecobba.R;
import com.app.ecobba.common.SharedMethods;
import com.google.android.material.textfield.TextInputEditText;
import com.google.gson.Gson;
import com.jaredrummler.materialspinner.MaterialSpinner;

import java.util.List;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;

public class WithdrawDialogFragment extends DialogFragment implements View.OnClickListener , MaterialSpinner.OnItemSelectedListener {

    @BindViews({R.id.btTransfer,R.id.btCancel})
    List<Button> buttons;
    @BindViews({R.id.tiAmount,R.id.tiPin})
    List<TextInputEditText> inputs;
    @BindView(R.id.msTransferMode)
    MaterialSpinner mode;
    @BindView(R.id.msOptions)
    MaterialSpinner options;
    @BindView(R.id.tvTransferTitle)
    TextView title;
    @BindView(R.id.tiMsisdn)
    TextInputEditText msisdn;
    String TITLE,amount,pin,MODE;
    Context context;
    User user;

    @BindView(R.id.ivFrom)
    ImageView ivFrom;

    @BindView(R.id.ivTo)
    ImageView ivTo;

    int wallet = R.drawable.ic_wallet,
        phone = R.drawable.ic_phone,
        card = R.drawable.ic_credit_card;

    NetworksResponse networksResponse;
    List<String > networks;

    public WithdrawDialogFragment(NetworksResponse networksResponse){
        this.networksResponse = networksResponse;
    }
    public WithdrawDialogFragment(onTransferClosed n,NetworksResponse networksResponse){
        this.returnFun = n;
        this.networksResponse = networksResponse;
    }

    protected void init(){
        networks = networksResponse.getNetworkList();
        //set Transfer modes here i.e mobile-money, bank, etc
//        mode.setItems(getResources().getStringArray(R.array.transfermode));
        mode.setItems(networks);
        context = getActivity();
        user = new Gson().fromJson(SharedMethods.getDefaults("USER-LOGIN",context),User.class);

    }

    private boolean validate(){
        boolean valid = true;
        amount= inputs.get(0).getText().toString();
        pin = inputs.get(1).getText().toString();
//        MODE = getResources().getStringArray(R.array.transfermode)[mode.getSelectedIndex()];
        String _pin = SharedMethods.getDefaults("PIN",context);



        if (amount.isEmpty()|| Double.parseDouble(amount)<=0){
            inputs.get(0).setError(getResources().getString(R.string.emptyField));
            valid = false;
        }
        if (pin.isEmpty()){
            inputs.get(1).setError(getResources().getString(R.string.emptyField));
//            valid = false;
        }else if (!pin.equals(_pin)){
            inputs.get(1).setError(getResources().getString(R.string.wrong_pin));
//            valid = false;
        }
        return valid;
    }

    @Override
    public void onClick(View view) {
        for (TextView tv : inputs){
            if (tv.getText().toString().isEmpty()){
                tv.setText("0");
            }
        }
        switch (view.getId()){
            case R.id.btTransfer:

                if (returnFun!=null){
                    if (validate()) {
                        returnFun.onTransferClicked(false,MODE,
                                Double.parseDouble(amount),
                                networksResponse.getId(networks.get(mode.getSelectedIndex())));
                        dismiss();
                    }
                }else {
                    Log.e("TRANSFER DIALOG","RETUN FUNCTION NOT SET");
                }


                break;
            case R.id.btCancel:


                dismiss();
                break;
        }
    }

    @Override
    public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
//        switch (position){
//            case 0:
//                ivFrom.setImageResource(card);
//                break;
//            case 1:
//                ivFrom.setImageResource(phone);
//                break;
//
//        }
    }

    public interface onTransferClosed{
        void onTransferClicked(Boolean cancelled ,String mode ,@Nullable Double amount, @Nullable String PIN);
    }

    onTransferClosed returnFun;

    public DialogFragment setOnTransferClosedListerner(onTransferClosed n){
        this.returnFun = n;
        return this;
    }

    public DialogFragment setTitle(String s){
        TITLE = s;
        return this;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View dialog = inflater.inflate(R.layout.dialog_money_transfer,container,false);
        ButterKnife.bind(this,dialog);
        init();
        mode.setOnItemSelectedListener(this);
        for (Button b : buttons){
            b.setOnClickListener(this);
        }
        if(TITLE!=null){
            title.setText(TITLE);
        }

        msisdn.setText(user.getMsisdn());

        return dialog;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ivFrom.setImageResource(wallet);
        ivTo.setImageResource(phone);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    }

}

