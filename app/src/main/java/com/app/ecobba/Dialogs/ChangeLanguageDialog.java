package com.app.ecobba.Dialogs;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import androidx.appcompat.app.AlertDialog;

import com.ahmedjazzar.rosetta.LanguagesListDialogFragment;
import com.app.ecobba.Application.MainApplication;
import com.app.ecobba.R;
import com.app.ecobba.Utils.LocalesUtils;

import butterknife.BindView;

public class ChangeLanguageDialog extends LanguagesListDialogFragment {

    @BindView(R.id.rgLangs)
    RadioGroup languages;

    @BindView(R.id.rbEnglish)
    RadioButton english;

    @BindView(R.id.rbFrench)
    RadioButton french;

    @BindView(R.id.rbSwahili)
    RadioButton swahili;

//    Locale[] locales = {new Locale("en"),
//                        new Locale("sw")};
    String[] langs = {"English","Kiswahili","French"};



    private final int DIALOG_TITLE_ID = R.string.language_switcher_dialog_title;
    private final int DIALOG_POSITIVE_ID = R.string.language_switcher_positive_button;
    private final int DIALOG_NEGATIVE_ID = R.string.language_switcher_negative_button;

    public ChangeLanguageDialog(){

    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
//        onLanguageSelected(getLanguages().length-1);
        builder.setTitle(getString(DIALOG_TITLE_ID))
                .setItems(
                        getLanguages(),
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int which) {
                                LocalesUtils.setAppLocale(getActivity(), MainApplication.locales.get(which));
                                LocalesUtils.refreshApplication(getActivity());
//                                onLanguageSelected(which);
//                                onPositiveClick();
                            }
                        });


        return builder.create();

    }

}
