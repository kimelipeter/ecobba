package com.app.ecobba.Dialogs.Money;

public class TransferRequest{
    public String network_id;
    public Double amount;
    public String phone;

    public TransferRequest(String network_id, Double amount, String phone) {
        this.network_id = network_id;
        this.amount = amount;
        this.phone = phone;
    }

    public TransferRequest() {
    }
}
