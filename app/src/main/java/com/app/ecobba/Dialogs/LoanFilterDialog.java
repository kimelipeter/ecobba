package com.app.ecobba.Dialogs;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.app.ecobba.R;
import com.jaredrummler.materialspinner.MaterialSpinner;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LoanFilterDialog extends DialogFragment implements View.OnClickListener {

    @BindView(R.id.btnFilter)
    Button btnFilter;

    @BindView(R.id.btnCancel)
    Button btnCancel;

    @BindView(R.id.msInsured)
    MaterialSpinner msInsured;

    @BindView(R.id.msRepaymentPlan)
    MaterialSpinner msRepaymentPlan;

    public  LoanFilterDialog(){}

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View dialog = inflater.inflate(R.layout.dialog_filter,container,false);
        ButterKnife.bind(this,dialog);
        return dialog;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        btnCancel.setOnClickListener(this);
        btnFilter.setOnClickListener(this);
        msInsured.setItems(getResources().getStringArray(R.array.insured));
        msRepaymentPlan.setItems(getResources().getStringArray(R.array.repayment));

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btnFilter:

                break;
            case R.id.btnCancel:
                dismiss();
                break;
        }
    }
}
