package com.app.ecobba.Dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;


import com.app.ecobba.Fragments.Wallet.TransactionReceiptActivity;
import com.app.ecobba.Models.DepositReceipt.DepositReceiptResponse;

import com.app.ecobba.Models.walletTransactionReceipt.Master_Visa_Card.MasterVisaCardResponse;

import com.app.ecobba.R;
import com.app.ecobba.common.SharedMethods;

import com.google.gson.Gson;

import com.jaredrummler.materialspinner.MaterialSpinner;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

import static com.app.ecobba.common.ApisKtKt.adaPayDeposit;
import static com.app.ecobba.common.ApisKtKt.adaPaySourcesTypes;
import static com.app.ecobba.common.ApisKtKt.adaPayTelcos;

import static com.app.ecobba.common.ApisKtKt.currencies;
import static com.app.ecobba.common.AppConstantsKt.INITIATOR_ADA_PAY_ADAPAY_SOURCE_TYPES;
import static com.app.ecobba.common.AppConstantsKt.INITIATOR_ADA_PAY_DEPOSIT;
import static com.app.ecobba.common.AppConstantsKt.INITIATOR_ADA_PAY_TELCOS;

import static com.app.ecobba.common.AppConstantsKt.INITIATOR_GET_USER_CURRENCY_DATA;
import static com.app.ecobba.common.SharedMethods.HTTP_GET;
import static com.app.ecobba.common.SharedMethods.HTTP_POST;
import static com.app.ecobba.common.SharedMethods.getFromServer;
import static com.app.ecobba.common.SharedMethods.global_error_message;
import static com.app.ecobba.common.SharedMethods.postToServer;
import static com.app.ecobba.common.SharedMethods.runOnUI;
import static com.app.ecobba.common.SharedMethods.showSweetAlertDialogError;
import static com.app.ecobba.common.SharedMethods.showSweetAlertDialogSuccess;

public class DepositRequestDialog extends DialogFragment implements SharedMethods.NetworkObjectListener,
        View.OnClickListener, MaterialSpinner.OnItemSelectedListener<String> {
    String phonrnumber = " ";
    @BindView(R.id.close)
    Button close;
    @BindView(R.id.editTextAmount)
    EditText editTextAmount;
    @BindView(R.id.editTextMsdn)
    EditText editTextMsdn;
    @BindView(R.id.currency)
    MaterialSpinner currency;
    @BindView(R.id.deposit)
    Button deposit;
    List<String> currencies_ids, ada_pay_ids, currency_prefix,telcos_adapay;
    String currency_id_toapi = "", ada_pay_id_toapi = "0", currency_prefix_toapi,selected_type,selected_provider;
    String _amount,_msdn;

    Context ctx;
    String token;
    private Unbinder unbinder;
    ColorDrawable popupColor;

    Spinner spinner,provider;

    LinearLayout linearLayoutBank ;
    LinearLayout linearLayoutMobile ,llLayoutMobileMpesa;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    int depositAmount,fees;
    String sumTotal, linkDatahosted_payment,reference,receipt_phone,invoice_number,description;

    public DepositRequestDialog(String msdin) {
        phonrnumber = msdin;
        Log.e("msdin_bundle ", phonrnumber);
    }


    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.deposit_menu_dialog, null);
        ctx = getActivity();
        token = SharedMethods.getDefaults("token", ctx);
        fetchFormDropDownData(token);
        fetchAdaPaySourceType(token);
        fetchAdaPayTelcos(token);
        close = view.findViewById(R.id.close);
        unbinder = ButterKnife.bind(this, view);

        editTextMsdn = view.findViewById(R.id.editTextMsdn);
        editTextMsdn.setText(phonrnumber);
        spinner = view.findViewById(R.id.payMethod);
        provider=view.findViewById(R.id.provider);
        deposit.setOnClickListener(this);
        currency.setOnItemSelectedListener(this);
        popupColor = new ColorDrawable(getResources().getColor(R.color.colorSurface));
        currency.getPopupWindow().setBackgroundDrawable(popupColor);

        close.setOnClickListener(v -> dismiss());
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(view);
         linearLayoutBank = view.findViewById(R.id.linearLayoutBank);
         linearLayoutMobile = view.findViewById(R.id.linearLayoutMobile);
        llLayoutMobileMpesa=view.findViewById(R.id.llLayoutMobileMpesa);

        return builder.create();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ctx = getActivity();
        token = SharedMethods.getDefaults("token", ctx);
        fetchFormDropDownData(token);
        fetchAdaPaySourceType(token);
        fetchAdaPayTelcos(token);
        currency.setOnItemSelectedListener(this);
        popupColor = new ColorDrawable(getResources().getColor(R.color.colorSurface));
        currency.getPopupWindow().setBackgroundDrawable(popupColor);
        deposit.setOnClickListener(this);

    }



    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.deposit:
                if(validateFormFields()) {
                    depositfromMpesa();

                }
                break;
        }

    }

    private void depositfromMpesa() {
        progressBar.setVisibility(View.VISIBLE);

        String params =

                "{"
                        +"\"source\": {"
//                        +"\"telco\": \""+selected_provider+"\","
                        +"\"telco\": \"safaricom_ke\","
                        +"\"msisdn\": "+_msdn+""
                        +"},"
                        +"\"currency\": {"
                        +"\"id\": "+currency_id_toapi+""
                        +"},"
                        +"\"amount\": "+_amount+","
                        +"\"method\": \""+selected_type+"\""
                        +"}";



        Log.e("POST_PARAMS", params);

        //depositfromMpesa
        postToServer(adaPayDeposit, token, params, INITIATOR_ADA_PAY_DEPOSIT, HTTP_POST, ctx, this);

    }

    private boolean validateFormFields() {
        boolean valid = true;
        _amount=editTextAmount.getText().toString();
        Log.e("deposit_amount",_amount);
        if (_amount.isEmpty()) {
            editTextAmount.setError(getString(R.string.emptyField));
            valid = false;
        }
        _msdn=editTextMsdn.getText().toString();
        Log.e("deposit_msdn",_msdn);
        if (_msdn.isEmpty()) {
            editTextMsdn.setError(getString(R.string.emptyField));
            valid = false;
        }

        if(  currency_id_toapi.equals("")){
            valid = false;
            Toast.makeText(ctx,getResources().getString(R.string.fill_in_all_fields),Toast.LENGTH_LONG).show();
        }

//         if (provider.getSelectedItem().toString().trim().equalsIgnoreCase("Select Option")) {
//             valid = false;
//
//             Toast.makeText(ctx, "Select Provider",
//                    Toast.LENGTH_SHORT).show();
//        }


        return  valid;

    }

    public void fetchFormDropDownData(String token) {
        Map<String, String> params = new HashMap<>();
        params.put("token", token);
        getFromServer(currencies, token, params, INITIATOR_GET_USER_CURRENCY_DATA, HTTP_GET, ctx, this);
    }

    public void fetchAdaPaySourceType(String token) {
        Map<String, String> params = new HashMap<>();
        params.put("token", token);
        getFromServer(adaPaySourcesTypes, token, params, INITIATOR_ADA_PAY_ADAPAY_SOURCE_TYPES, HTTP_GET, ctx, this);
    }
    public void fetchAdaPayTelcos(String token) {
        Map<String, String> params = new HashMap<>();
        params.put("token", token);
        getFromServer(adaPayTelcos, token, params, INITIATOR_ADA_PAY_TELCOS, HTTP_GET, ctx, this);
    }


    @Override
    public void onDataReady(String initiator, String response) {
        switch (initiator) {
            case INITIATOR_GET_USER_CURRENCY_DATA:
                try {
                    runOnUI(new Runnable() {
                        @Override
                        public void run() {
                            Log.e("REG_DATA RESPONSE", "Response is " + response);

                            JSONObject jObj = null;

                            currencies_ids = new ArrayList<>();
                            currency_prefix = new ArrayList<>();

                            try {

                                jObj = new JSONObject(response);
                                if (jObj.has("errors")) {
                                    String errors = jObj.getString("errors");
                                    showSweetAlertDialogError(errors, ctx);
                                } else { // if no error message was found in the response,
                                    boolean success = jObj.getBoolean("success");
                                    if (success) {
                                        JSONArray currencies = jObj.getJSONArray("data");
                                        ArrayList<String> currencies_drop_down = new ArrayList<String>();
                                        currencies_drop_down.add(getResources().getString(R.string.please_select_currency));
                                        currencies_ids.add("0");
                                        currency_prefix.add("0");
                                        if (currencies.length() > 0) {
                                            for (int i = 0; i < currencies.length(); i++) {
                                                JSONObject jsonObject = currencies.getJSONObject(i);
                                                String id = jsonObject.getString("id");
                                                Log.e("currency_Id_data", id);
                                                String name = jsonObject.getString("name");
                                                String prefix = jsonObject.getString("prefix");
                                                Log.e("cur_prefix", prefix);
                                                currencies_drop_down.add(name);
                                                currencies_ids.add(id);
                                                currency_prefix.add(prefix);
                                            }

                                            currency.setItems(currencies_drop_down);
                                            currency.setSelectedIndex(0);
                                        }


                                    } else {
                                        String api_error_message = jObj.getString("message");
                                        showSweetAlertDialogError(api_error_message, ctx);
                                    }
                                }

                            } catch (Exception e) {
                                Log.e("ExceptionLOGIN-DATA", e.toString());
                                showSweetAlertDialogError(global_error_message, ctx);
                                e.printStackTrace();
                            }
                        }
                    });
                } catch (Exception e) {
//                            hideDialog(ctx);
                    Log.e("Exception- HOME", e.toString());
                    showSweetAlertDialogError(global_error_message, ctx);
//                    Toast.makeText(ctx, "Login Data Response Exception" + e.toString(), Toast.LENGTH_LONG).show();
                }
                break;

            case INITIATOR_ADA_PAY_ADAPAY_SOURCE_TYPES:
                try {
                    runOnUI(new Runnable() {
                        @Override
                        public void run() {
                            Log.e("ADA_PAY_SOURCES_RES", "Response is " + response);
                            try {
                                JSONObject jObj = new JSONObject(response);

                                JSONObject type = jObj.getJSONObject("adapay_source_types");

                                String mobile = type.getString("mobile_money");
                                String card = type.getString("card_hosted");
                                String bank_transfer=type.getString("bank_transfer");

                                ada_pay_ids = new ArrayList<>();
                                ada_pay_ids.add(0,getResources().getString(R.string.please_select_type));
                                ada_pay_ids.add(1, mobile);
                                ada_pay_ids.add(2, card);
                                ada_pay_ids.add(3,bank_transfer);

                                ArrayAdapter<String> adapter =
                                        new ArrayAdapter<String>(ctx, android.R.layout.simple_spinner_dropdown_item, ada_pay_ids);
                                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                                spinner.setAdapter(adapter);

                                spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                    @Override
                                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                                        String type = (String) parent.getItemAtPosition(position);
//
//                                        String selected;
//                                        if (type.equals("Mobile Money")) {
//                                            selected = "mobile_money";
//                                        } else {
//                                            selected = "card_hosted";
//                                        }
//
//                                        Log.e("Selected_type", String.valueOf(type));
//                                        Log.e("Selected_type2", selected);
                                         selected_type = "";
                                        if (type.equals("Mobile Money")){
                                            selected_type = "mobile_money";
                                            linearLayoutMobile.setVisibility(View.VISIBLE);
                                            linearLayoutBank.setVisibility(View.GONE);

                                        } else if (type.equals("Master/Visa Card")){
                                            selected_type = "card_hosted";
                                            linearLayoutBank.setVisibility(View.GONE);
                                            linearLayoutMobile.setVisibility(View.GONE);
                                        }
                                        else if (type.equals("Bank Transfer")){
                                            selected_type = "bank_transfer";
                                            linearLayoutBank.setVisibility(View.GONE);
                                            linearLayoutMobile.setVisibility(View.GONE);
                                        }
                                        else {

                                            linearLayoutBank.setVisibility(View.GONE);
                                            linearLayoutMobile.setVisibility(View.GONE);
                                        }
                                        Log.e("Selected_type", String.valueOf(type));
                                        Log.e("Selected_type2", selected_type);
                                    }

                                    @Override
                                    public void onNothingSelected(AdapterView<?> parent) {

                                    }
                                });


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                        }
                    });
                } catch (Exception e) {
//                            hideDialog(ctx);
                    Log.e("Exception- HOME", e.toString());
                    showSweetAlertDialogError(global_error_message, ctx);
//                    Toast.makeText(ctx, "Login Data Response Exception" + e.toString(), Toast.LENGTH_LONG).show();
                }
                break;
            case INITIATOR_ADA_PAY_TELCOS:
                try {
                    runOnUI(new Runnable() {
                        @Override
                        public void run() {
                            Log.e("ADA_PAY_TELCOS_RES", "Response is " + response);
                            try {
                                JSONObject jObj = new JSONObject(response);

                                JSONObject type = jObj.getJSONObject("adapay_telcos");

                                String telcos = type.getString("safaricom_ke");

                                telcos_adapay = new ArrayList<>();
                                telcos_adapay.add(0,getResources().getString(R.string.please_select_type));
                                telcos_adapay.add(1, telcos);


                                ArrayAdapter<String> adapter =
                                        new ArrayAdapter<String>(ctx, android.R.layout.simple_spinner_dropdown_item, telcos_adapay);
                                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                                provider.setAdapter(adapter);


                                provider.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                    @Override
                                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                                        String type = (String) parent.getItemAtPosition(position);

                                         selected_provider = "";

                                        if (type.equals("Mpesa")) {
                                            llLayoutMobileMpesa.setVisibility(View.VISIBLE);
                                            selected_provider = "safaricom_ke";
                                        }
                                        else {
                                            llLayoutMobileMpesa.setVisibility(View.GONE);
                                        }
                                        Log.e("Selected_telcos", String.valueOf(type));
                                        Log.e("Selected_telcos2", selected_provider);
                                    }

                                    @Override
                                    public void onNothingSelected(AdapterView<?> parent) {

                                    }
                                });


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                        }
                    });
                } catch (Exception e) {
//                            hideDialog(ctx);
                    Log.e("Exception- HOME", e.toString());
                    showSweetAlertDialogError(global_error_message, ctx);
//                    Toast.makeText(ctx, "Login Data Response Exception" + e.toString(), Toast.LENGTH_LONG).show();
                }
                break;

            case INITIATOR_ADA_PAY_DEPOSIT:
                try {
                    runOnUI(new Runnable() {
                        @Override
                        public void run() {
                            Log.e("ADA_PAY_WITHDRAW", "Response is " + response);
                            progressBar.setVisibility(View.GONE);
                            JSONObject jObj = null;
                            try {
                                jObj = new JSONObject(response);
                                if (jObj.has("errors")) {
                                    String errors = jObj.getString("errors");
                                    showSweetAlertDialogError(errors, ctx);
                                } else { // if no error message was found in the response,
                                    boolean success = jObj.getBoolean("success");
                                    if (success) {
                                        String api_message = jObj.getString("message");
                                        showSweetAlertDialogSuccess(api_message,ctx);
//
                                        if (jObj.getJSONObject("transaction").isNull("transaction") && (selected_type.equals("card_hosted")||selected_type.equals("bank_transfer"))) {
                                           // TransactionReceiptResponse transactionReceiptResponse = new Gson().fromJson(response, TransactionReceiptResponse.class);
                                          MasterVisaCardResponse masterVisaCardResponse=new Gson().fromJson(response,MasterVisaCardResponse.class);
                                            linkDatahosted_payment=  masterVisaCardResponse.getTransaction()
                                                 .getTransaction_data()
                                                 .getServer_response_create()
                                                 .getCharge()
                                                 .getAttributes()
                                                 .getHosted_payment()
                                                 .getLink();
                                         Log.e("link_Datahosted_payment",linkDatahosted_payment);

                                          description=masterVisaCardResponse.getTransaction().getTransaction_data()
                                                 .getServer_response_create().getCharge().getAttributes().getStatement_descriptor();
                                          sumTotal=masterVisaCardResponse.getTransaction().getTransaction_data()
                                                 .getServer_response_create().getCharge().getAttributes().getAmount().getAmount();
                                          reference=masterVisaCardResponse.getTransaction().getReference();
                                          invoice_number=masterVisaCardResponse.getTransaction().getTransaction_data()
                                                 .getServer_response_create().getCharge().getAttributes().getInvoice_number();
                                          receipt_phone=masterVisaCardResponse.getTransaction().getTransaction_data()
                                                 .getServer_response_create().getCharge().getAttributes().getReceipt_phone();
                                          fees=masterVisaCardResponse.getTransaction().getTransaction_fees();
                                          depositAmount=masterVisaCardResponse.getTransaction().getAmount();
                                          Log.e("fees_charge", String.valueOf(fees));

                                            Intent intent = new Intent(ctx, TransactionReceiptActivity.class);
                                            intent.putExtra("amount", depositAmount);
                                            intent.putExtra("fees", fees);
                                            intent.putExtra("sum_total", sumTotal);
                                            intent.putExtra("link", linkDatahosted_payment);
                                            intent.putExtra("reference", reference);
                                            startActivity(intent);
                                            dismiss();



                                        }


                                        else if (jObj.getJSONObject("transaction").isNull("transaction") && selected_type.equals("mobile_money")){

                                           // Intent  selected_type =new Intent(ctx,MainActivity.class);
                                           // MobileMoneyReceiptResponse mobileMoneyReceiptResponse=new Gson().fromJson(response,MobileMoneyReceiptResponse.class);
                                            DepositReceiptResponse depositReceiptResponse=new  Gson().fromJson(response,DepositReceiptResponse.class);
                                          int depositedAmount=  depositReceiptResponse.getTransaction().getAmount();
                                          String txn_code=depositReceiptResponse.getTransaction().getTxn_code();
                                            Log.e("amount_deposited", String.valueOf(depositedAmount));
                                            Log.e("txn_code_is",txn_code);
                                           // startActivity(selected_type);
                                            dismiss();

                                        }

                                    } else {
                                        String api_error_message = jObj.getString("message");
                                        showSweetAlertDialogError(api_error_message, ctx);
                                    }
                                }

                            } catch (JSONException e) {
                                Log.e("Exception-DATA", e.toString());
                                showSweetAlertDialogError(global_error_message, ctx);
                                e.printStackTrace();
                            }
                        }
                    });
                } catch (Exception e) {
                    progressBar.setVisibility(View.GONE);
                    Log.e("Exception_+GROUP_MEMBER", e.toString());
                    showSweetAlertDialogError(global_error_message, ctx);
//                    Toast.makeText(ctx, "Login Data Response Exception" + e.toString(), Toast.LENGTH_LONG).show();
                }
                break;
        }

    }

    @Override
    public void onDataLoaded(String initiator, JSONArray data) {

    }

    @Override
    public void onNetworkError(String initiator, String errorCode, String errorMessage) {
        switch (initiator) {

            case INITIATOR_GET_USER_CURRENCY_DATA:
                try {
                    runOnUI(new Runnable() {
                        @Override
                        public void run() {
//                                    hideDialog(ctx);
                            showSweetAlertDialogError(errorMessage, ctx);
                        }
                    });
                } catch (Exception e) {
//                            hideDialog(ctx);
                    showSweetAlertDialogError(global_error_message, ctx);
                    Log.e("Exception- HOME", e.toString());
//                    Toast.makeText(ctx, "Login Error Response Exception" + e.toString(), Toast.LENGTH_LONG).show();
                }

                break;
        }

    }

    @Override
    public void onItemSelected(MaterialSpinner view, int position, long id, String item) {
        switch (view.getId()) {
            case R.id.currency:
                Log.d("Currency_id_is ", currencies_ids.get(position));
                currency_id_toapi = currencies_ids.get(position);
                currency_prefix_toapi = currency_prefix.get(position);
                break;


        }
    }


}
