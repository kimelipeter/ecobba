package com.app.ecobba.Dialogs;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.archit.calendardaterangepicker.customviews.DateRangeCalendarView;
import com.app.ecobba.R;
import com.google.android.material.snackbar.Snackbar;

import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;

public class DateRangePickerDialogFragment extends DialogFragment implements View.OnClickListener, DateRangeCalendarView.CalendarListener {

    @BindViews({R.id.btFilter,R.id.btCancel})
    List<Button> buttons;

    @BindView(R.id.drcvCalender)
    DateRangeCalendarView calender;

    Calendar start,end;

    public DateRangePickerDialogFragment(onFilterPressed retunFun){
        this.retunFun = retunFun;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View dialog = inflater.inflate(R.layout.dialog_date_range_picker,container,false);
        return dialog;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this,view);
        calender.setCalendarListener(this);
        for (Button b:buttons){
            b.setOnClickListener(this);
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btFilter:
                if (start!=null && end!=null){
                    retunFun.onFilterPressed(false, start, end);
                    dismiss();
                }else {
                    Snackbar.make(view,"Range not picked",Snackbar.LENGTH_SHORT).show();
                }
                break;
            case R.id.btCancel:
                    dismiss();
                break;
        }
    }

    @Override
    public void onFirstDateSelected(Calendar startDate) {
        Toast.makeText(getContext(),"Range selected",Toast.LENGTH_SHORT).show();
        start = startDate;
    }

    @Override
    public void onDateRangeSelected(Calendar startDate, Calendar endDate) {
        Toast.makeText(getContext(),"Range selected",Toast.LENGTH_SHORT).show();
        start = startDate;
        end = endDate;
    }

    public interface onFilterPressed{
        void onFilterPressed(Boolean cancelled,Calendar START, Calendar END);
    }

    private onFilterPressed retunFun;

}
