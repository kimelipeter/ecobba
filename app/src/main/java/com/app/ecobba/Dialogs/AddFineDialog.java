package com.app.ecobba.Dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.app.ecobba.Models.GroupWallet.AdapaySource;
import com.app.ecobba.Models.GroupWallet.groupWalletResponse;
import com.app.ecobba.Models.PaymentSources.paymentSourcesResponse;
import com.app.ecobba.Models.adapay.adapaySourcesResponse;
import com.app.ecobba.Models.adapay.telcos.telcosResponse;
import com.app.ecobba.R;
import com.app.ecobba.common.SharedMethods;
import com.app.ecobba.common.rxApi.Api;
import com.app.ecobba.common.rxApi.RxApi;
import com.google.android.material.textfield.TextInputEditText;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.app.ecobba.common.ApisKtKt.setPaymentMethod;
import static com.app.ecobba.common.ApisKtKt.storeFinesSettings;
import static com.app.ecobba.common.AppConstantsKt.INITIATOR_SET_PAYMENT_METHOD;
import static com.app.ecobba.common.AppConstantsKt.INITIATOR_STORE_FINE_SETTING;
import static com.app.ecobba.common.SharedMethods.HTTP_POST;
import static com.app.ecobba.common.SharedMethods.global_error_message;
import static com.app.ecobba.common.SharedMethods.postToServer;
import static com.app.ecobba.common.SharedMethods.runOnUI;
import static com.app.ecobba.common.SharedMethods.showSweetAlertDialogError;
import static com.app.ecobba.common.SharedMethods.showSweetAlertDialogSuccess;

public class AddFineDialog extends DialogFragment implements
        SharedMethods.NetworkObjectListener, View.OnClickListener {


    @BindView(R.id.btApply)
    Button btApply;
    Context ctx;
    private Api api;
    String token;
    private Unbinder unbinder;
    ArrayAdapter arrayAdapter;
    LinearLayout linearLayoutMobile, llLayoutMobileMpesa;
    ColorDrawable popupColor;
    List<String> ada_pay_ids, telcos_adapay;
    String _msisdn, type, telco, selected_type, selected_provider;
    Spinner msisdn;

    Spinner payMethod, provider;
    String _group_id, _type, _telco, _setting_id, id = "", _name, _amount, _user_currency;


    @BindView(R.id.tiType)
    TextInputEditText name;
    @BindView(R.id.tiAmount)
    TextInputEditText amount;
    @BindView(R.id.user_currency)
    TextView currency;


    public AddFineDialog(String setting_id, String group_id, String user_currency) {

        _setting_id = setting_id;
        _group_id = group_id;
        _user_currency = user_currency;
    }


    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_add_fine, null);
        ctx = getActivity();
        api = new RxApi().getApi();
        token = SharedMethods.getDefaults("token", ctx);
        fetchAdaPaySourceType();
        fetchAdaPayTelcos();
        unbinder = ButterKnife.bind(this, view);
        payMethod = view.findViewById(R.id.payMethod);
        provider = view.findViewById(R.id.provider);
        fetchUserPaymentSoucers(_group_id);

        _type = selected_type;
        _telco = selected_provider;
        btApply.setOnClickListener(this);
        api = new RxApi().getApi();
        linearLayoutMobile = view.findViewById(R.id.linearLayoutMobile);
        msisdn = view.findViewById(R.id.msisdn);
        llLayoutMobileMpesa = view.findViewById(R.id.llLayoutMobileMpesa);
        popupColor = new ColorDrawable(getResources().getColor(R.color.colorSurface));
        currency.setText("(" + _user_currency + ")");

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(view);
        return builder.create();
    }

    private void postFine() {

        String params = "{ \"name\":" + '"' + _name + '"' +
                ", \"amount\": " + '"' + _amount + '"' +
                ", \"group_id\": " + '"' + _group_id + '"' +
                ", \"setting_id\": " + '"' + _setting_id + "\"," +
                "\"payout_source\": {"
                + "\"id\": \"" + id + "\","
                + "\"method\": \"" + selected_type + "\","
                + "\"telco\": \"" + selected_provider + "\","
                + "\"msisdn\": \"" + _msisdn + "\"" +
                "}"
                + "}\n" + "\n";
        Log.e("POST_PARAMS_DATA", params);
        postToServer(storeFinesSettings, token, params, INITIATOR_STORE_FINE_SETTING, HTTP_POST, ctx, this);
    }

    public void fetchAdaPaySourceType() {

        Call<adapaySourcesResponse> call = api.fetchAdapaySources();
        call.enqueue(new Callback<adapaySourcesResponse>() {
            @Override
            public void onResponse(Call<adapaySourcesResponse> call, Response<adapaySourcesResponse> response) {
                try {
                    if (response.body().getSuccess()) {

                        String mobile = response.body().getAdapay_source_types().getMobile_money();
                        String card = response.body().getAdapay_source_types().getCard_hosted();
                        String bank_transfer = response.body().getAdapay_source_types().getBank_transfer();

                        ada_pay_ids = new ArrayList<>();
                        ada_pay_ids.add(0, getResources().getString(R.string.please_select_type));
                        ada_pay_ids.add(1, mobile);
                        ada_pay_ids.add(2, card);
                        ada_pay_ids.add(3, bank_transfer);

                        ArrayAdapter<String> adapter =
                                new ArrayAdapter<String>(ctx, android.R.layout.simple_spinner_dropdown_item, ada_pay_ids);
                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                        payMethod.setAdapter(adapter);

                        payMethod.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                                String type = (String) parent.getItemAtPosition(position);

                                selected_type = "";
                                if (type.equals("Mobile Money")) {
                                    selected_type = "mobile_money";
                                    linearLayoutMobile.setVisibility(View.VISIBLE);
//                                            linearLayoutBank.setVisibility(View.GONE);

                                }
//                                        else if (type.equals("Master/Visa Card")){
//                                            selected_type = "card_hosted";
//                                            linearLayoutBank.setVisibility(View.GONE);
//                                            linearLayoutMobile.setVisibility(View.GONE);
//                                        }
//                                        else if (type.equals("Bank Transfer")){
//                                            selected_type = "bank_transfer";
//                                            linearLayoutBank.setVisibility(View.GONE);
//                                            linearLayoutMobile.setVisibility(View.GONE);
//                                        }
                                else {
//
                                    //    linearLayoutBank.setVisibility(View.GONE);
                                    linearLayoutMobile.setVisibility(View.GONE);
                                }
                                Log.e("Selected_type", String.valueOf(type));
                                Log.e("Selected_type2", selected_type);
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {

                            }

                        });
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }


            }

            @Override
            public void onFailure(Call<adapaySourcesResponse> call, Throwable t) {

            }
        });

    }

    private void fetchUserPaymentSoucers(String group_id) {
        Call<groupWalletResponse> call = api.getGroupWalletPaymentSources(group_id);
        call.enqueue(new Callback<groupWalletResponse>() {
            @Override
            public void onResponse(Call<groupWalletResponse> call, Response<groupWalletResponse> response) {
                Log.e("TAG_payment_sources", "onResponse: " + response);
                try {
                    if (response.body().getSuccess()) {
                        if (response.body().getAdapay_sources().size() > 0) {

                            List<AdapaySource> sources = response.body().getAdapay_sources();
                            ArrayList<String> availables = new ArrayList<>();
                            // availables.add(0, getResources().getString(R.string.please_select_contribution));
                            for (int item = 0; item < sources.size(); item++) {
                                availables.add(sources.get(item).getMsisdn());
                            }
                            arrayAdapter = new ArrayAdapter(ctx, android.R.layout.simple_spinner_dropdown_item, availables);
                            arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            msisdn.setAdapter(arrayAdapter);
                            msisdn.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                    String item_resource_type = (String) parent.getItemAtPosition(position);

                                    _msisdn = sources.get(position).getMsisdn();
                                    Log.e("PHONE_NUMBER", _msisdn);

                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> parent) {

                                }
                            });
                        }

                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<groupWalletResponse> call, Throwable t) {

            }
        });
    }

    public void fetchAdaPayTelcos() {
        Call<telcosResponse> call = api.fetchAdapayTelcos();
        call.enqueue(new Callback<telcosResponse>() {
            @Override
            public void onResponse(Call<telcosResponse> call, Response<telcosResponse> response) {
                try {
                    if (response.body().getSuccess() == true) {
                        String telcos = response.body().getAdapay_telcos().getSafaricom_ke();
                        telcos_adapay = new ArrayList<>();
                        telcos_adapay.add(0, getResources().getString(R.string.please_select_type));
                        telcos_adapay.add(1, telcos);
                        ArrayAdapter<String> adapter =
                                new ArrayAdapter<String>(ctx, android.R.layout.simple_spinner_dropdown_item, telcos_adapay);
                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        provider.setAdapter(adapter);
                        provider.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                String type = (String) parent.getItemAtPosition(position);
                                selected_provider = "";
                                if (type.equals("Mpesa")) {
                                    llLayoutMobileMpesa.setVisibility(View.VISIBLE);
                                    selected_provider = "safaricom_ke";
                                } else {
                                    llLayoutMobileMpesa.setVisibility(View.GONE);
                                }
                                Log.e("Selected_telcos", String.valueOf(type));
                                Log.e("Selected_telcos2", selected_provider);
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {

                            }
                        });


                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<telcosResponse> call, Throwable t) {

            }
        });
    }

    private boolean validateFormFields() {
        boolean valid = true;
        _name = name.getText().toString();
        _amount = amount.getText().toString();
        Log.e("_name", _name);
        if (_name.isEmpty()) {
            name.setError(getString(R.string.emptyField));
            valid = false;
        }
        if (_amount.isEmpty()) {
            amount.setError(getString(R.string.emptyField));
            valid = false;
        }


        return valid;

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btApply:
                if (validateFormFields()) {
                    postFine();
                }
                break;
        }
    }


    @Override
    public void onDataReady(String initiator, String response) {
        switch (initiator) {
            case INITIATOR_STORE_FINE_SETTING:
                try {
                    runOnUI(new Runnable() {
                        @Override
                        public void run() {
                            Log.e("SET_PAYMENT_SOURCE", "Response is " + response);
//                            progressBar.setVisibility(View.GONE);
                            JSONObject jObj = null;
                            try {
                                jObj = new JSONObject(response);
                                if (jObj.has("errors")) {
                                    String errors = jObj.getString("errors");
                                    showSweetAlertDialogError(errors, ctx);
                                } else { // if no error message was found in the response,
                                    boolean success = jObj.getBoolean("success");
                                    if (success) {
                                        dismiss();
                                        String api_message = _name + " " + "Setting has been successfully applied";
                                        showSweetAlertDialogSuccess(api_message, ctx);

                                    } else {
                                        dismiss();
                                        String api_error_message = jObj.getString("message");
                                        showSweetAlertDialogError(api_error_message, ctx);
                                    }
                                }

                            } catch (JSONException e) {
                                Log.e("Exception-DATA", e.toString());
                                showSweetAlertDialogError(global_error_message, ctx);
                                e.printStackTrace();
                            }
                        }
                    });
                } catch (Exception e) {
//                    progressBar.setVisibility(View.GONE);
                    Log.e("Exception_PAY_SOURCE", e.toString());
                    showSweetAlertDialogError(global_error_message, ctx);
//                    Toast.makeText(ctx, "Login Data Response Exception" + e.toString(), Toast.LENGTH_LONG).show();
                }
                break;
        }

    }

    @Override
    public void onDataLoaded(String initiator, JSONArray data) {

    }

    @Override
    public void onNetworkError(String initiator, String errorCode, String errorMessage) {

        switch (initiator) {
            case INITIATOR_STORE_FINE_SETTING:
                try {
                    runOnUI(new Runnable() {
                        @Override
                        public void run() {
//                                    hideDialog(ctx);
                            showSweetAlertDialogError(errorMessage, ctx);
                        }
                    });
                } catch (Exception e) {
//                            hideDialog(ctx);
                    showSweetAlertDialogError(global_error_message, ctx);
                    Log.e("Exception- HOME", e.toString());
//                    Toast.makeText(ctx, "Login Error Response Exception" + e.toString(), Toast.LENGTH_LONG).show();
                }

                break;

        }

    }
}
