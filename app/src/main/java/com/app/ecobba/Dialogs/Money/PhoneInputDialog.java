package com.app.ecobba.Dialogs.Money;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.app.ecobba.R;
import com.google.android.material.textfield.TextInputLayout;


import butterknife.BindView;
import butterknife.ButterKnife;

public class PhoneInputDialog extends DialogFragment {

   // @BindView(R.id.pilPhoneInput)
//    PhoneInputLayout phoneInputLayout;

    public PhoneInputDialog(){}

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View dialog = inflater.inflate(R.layout.fragment_money_phone,container,false);
        ButterKnife.bind(this,dialog);
      //  phoneInputLayout.getTextInputLayout().setBoxBackgroundMode(TextInputLayout.BOX_BACKGROUND_OUTLINE);
//        phoneInputLayout.getTextInputLayout().setPadding(5,5,5,5);
//        phoneInputLayout.getTextInputLayout().setBoxCornerRadii(15,15,15,15);
     //   phoneInputLayout.getTextInputLayout().setBoxBackgroundColor(getResources().getColor(R.color.white));
     //   phoneInputLayout.getTextInputLayout().setBoxStrokeColor(getResources().getColor(R.color.white));
     //   phoneInputLayout.setDefaultCountry("+254");

        return dialog;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    }

}
