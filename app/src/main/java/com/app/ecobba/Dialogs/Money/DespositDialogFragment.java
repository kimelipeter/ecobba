package com.app.ecobba.Dialogs.Money;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.app.ecobba.Models.NetworksResponse;
import com.jaredrummler.materialspinner.MaterialSpinner;

public class DespositDialogFragment extends WithdrawDialogFragment {

    public DespositDialogFragment(onTransferClosed n, NetworksResponse networksResponse){
        super(n,networksResponse);
    }

    @Override
    public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
        super.onItemSelected(view, position, id, item);

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mode.setVisibility(View.VISIBLE);
        ivTo.setImageResource(wallet);
        ivFrom.setImageResource(phone);
    }
}
