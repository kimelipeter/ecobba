package com.app.ecobba.Dialogs;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.app.ecobba.Models.User;
import com.app.ecobba.R;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import java.util.Date;

public class NextLoanPayment extends BottomSheetDialogFragment {

    ImageView close;
    TextView loanName,loanPayamount,loanPaybackDate;
    String date,namePackage, loannameTitle;
    int loanNextAmount;

    public NextLoanPayment(String date, String namePackage, int loanNextAmount, String loannameTitle) {
        this.date=date;
        this.namePackage=namePackage;
        this.loanNextAmount=loanNextAmount;
        this.loannameTitle = loannameTitle;



        Log.e("Loan_data_payable ", date+" "+ namePackage+""+loanNextAmount+" "+loannameTitle);
    }

    @Override
    public int getTheme() {
        return R.style.AppBottomSheetDialogTheme;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.next_loan_payment_details_bottom_sheet,container,false);
        close=view.findViewById(R.id.close);
        loanName=view.findViewById(R.id.loanName);
        loanPayamount=view.findViewById(R.id.loanPayamount);
        loanPaybackDate=view.findViewById(R.id.loanPaybackDate);
        setStyle(STYLE_NORMAL, R.style.AppBottomSheetDialogTheme);

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {

        super.onViewCreated(view, savedInstanceState);
       loanName.setText(namePackage);
        loanPayamount.setText(String.valueOf(loanNextAmount));
        loanPaybackDate.setText(date);
        close.setOnClickListener(v -> {
            dismiss();
        });
    }
}
