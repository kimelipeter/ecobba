package com.app.ecobba.Dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import com.app.ecobba.R;
import com.app.ecobba.common.SharedMethods;
import com.jaredrummler.materialspinner.MaterialSpinner;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

import static com.app.ecobba.common.ApisKtKt.adaPaySources;
import static com.app.ecobba.common.ApisKtKt.adaPayWithdraw;
import static com.app.ecobba.common.ApisKtKt.currencies;
import static com.app.ecobba.common.AppConstantsKt.INITIATOR_ADA_PAY_SOURCES;
import static com.app.ecobba.common.AppConstantsKt.INITIATOR_ADA_PAY_WITHDRAW;
import static com.app.ecobba.common.AppConstantsKt.INITIATOR_GET_USER_CURRENCY_DATA;
import static com.app.ecobba.common.SharedMethods.HTTP_GET;
import static com.app.ecobba.common.SharedMethods.HTTP_POST;
import static com.app.ecobba.common.SharedMethods.getFromServer;
import static com.app.ecobba.common.SharedMethods.global_error_message;
import static com.app.ecobba.common.SharedMethods.postToServer;
import static com.app.ecobba.common.SharedMethods.runOnUI;
import static com.app.ecobba.common.SharedMethods.showSweetAlertDialogError;
import static com.app.ecobba.common.SharedMethods.showSweetAlertDialogSuccess;

public class WalletTransfer extends DialogFragment implements SharedMethods.NetworkObjectListener,
        View.OnClickListener, MaterialSpinner.OnItemSelectedListener<String> {
    @BindView(R.id.currency)
    MaterialSpinner currency;
    @BindView(R.id.payMethod)

    MaterialSpinner payMethod;
    @BindView(R.id.close)
    Button close;
    @BindView(R.id.withdraw)
    Button withdraw;
    @BindView(R.id.editTextAmount)
    EditText editTextAmount;
    @BindView(R.id.editTextMsdn)
    EditText editTextMsdn;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    List<String> currencies_ids,ada_pay_ids,currency_prefix;
    String currency_id_toapi = "0",ada_pay_id_toapi="0",currency_prefix_toapi;
    Context ctx;
    ColorDrawable popupColor;
    String token;
    String phonrnumber = " ";
    String _amount,_msdn;


    private Unbinder unbinder;
    public WalletTransfer(String bundle){
        phonrnumber = bundle;
        Log.e("bundle ",phonrnumber);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.withdraw_menu_dialog, null);
        ctx = getActivity();
        token = SharedMethods.getDefaults("token", ctx);
        fetchFormDropDownData(token);
        fetchPayMethod(token);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        close = view.findViewById(R.id.close);
        unbinder = ButterKnife.bind(this, view);

        close.setOnClickListener(v -> dismiss());
        withdraw.setOnClickListener(this);
        currency.setOnItemSelectedListener(this);
        popupColor = new ColorDrawable(getResources().getColor(R.color.colorSurface));
        currency.getPopupWindow().setBackgroundDrawable(popupColor);
        payMethod.setOnItemSelectedListener(this);
        popupColor = new ColorDrawable(getResources().getColor(R.color.colorSurface));
        payMethod.getPopupWindow().setBackgroundDrawable(popupColor);
        editTextMsdn=view.findViewById(R.id.editTextMsdn);
        editTextMsdn.setText(phonrnumber);

        builder.setView(view);


        return builder.create();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ctx=getActivity();
        token= SharedMethods.getDefaults("token",ctx);
        fetchFormDropDownData(token);
        fetchPayMethod(token);
        currency.setOnItemSelectedListener(this);
        withdraw.setOnClickListener(this);
        popupColor = new ColorDrawable(getResources().getColor(R.color.colorSurface));
        currency.getPopupWindow().setBackgroundDrawable(popupColor);
        payMethod.setOnItemSelectedListener(this);
        popupColor = new ColorDrawable(getResources().getColor(R.color.colorSurface));
        payMethod.getPopupWindow().setBackgroundDrawable(popupColor);

    }

    private boolean validateFormFields() {
        boolean valid = true;
        _amount=editTextAmount.getText().toString();
        Log.e("withdraw_amount",_amount);
        if (_amount.isEmpty()) {
            editTextAmount.setError(getString(R.string.emptyField));
            valid = false;
        }
        _msdn=editTextMsdn.getText().toString();
        Log.e("withdraw_msdn",_msdn);
        if (_msdn.isEmpty()) {
            editTextMsdn.setError(getString(R.string.emptyField));
            valid = false;
        }

        if(  currency_id_toapi.equals("0")){
            valid = false;
            Toast.makeText(ctx,getResources().getString(R.string.fill_in_all_fields),Toast.LENGTH_LONG).show();
        }
//        if(  ada_pay_id_toapi.equals("0")){
//            valid = false;
//            Toast.makeText(ctx,getResources().getString(R.string.fill_in_all_fields),Toast.LENGTH_LONG).show();
//        }
        return valid;
    }

    private void withdrawfromMpesa() {
        dismiss();
        progressBar.setVisibility(View.VISIBLE);

        String params =
        "{"
            +"\"source\": {"
                +"\"telco\": \"safaricom_ke\","
                +"\"msisdn\": "+_msdn+""
            +"},"
            +"\"currency\": {"
                +"\"id\": "+currency_id_toapi+""
            +"},"
            +"\"amount\": "+_amount+","
            +"\"method\": \"mobile_money\""
        +"}";

        Log.e("POST_PARAMS", params);

        //withdrawfromWallet
        postToServer(adaPayWithdraw, token, params, INITIATOR_ADA_PAY_WITHDRAW, HTTP_POST, ctx, this);

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.withdraw:
                if(validateFormFields()) {
                    withdrawfromMpesa();
                }
                break;
        }

    }

    public void fetchFormDropDownData(String token) {
        Map<String, String> params = new HashMap<>();
        params.put("token", token);
        getFromServer(currencies, token, params, INITIATOR_GET_USER_CURRENCY_DATA, HTTP_GET, ctx, this);
    }
    public void fetchPayMethod(String token) {
        Map<String, String> params = new HashMap<>();
        params.put("token", token);
        getFromServer(adaPaySources, token, params, INITIATOR_ADA_PAY_SOURCES, HTTP_GET, ctx, this);
    }






    @Override
    public void onDataReady(String initiator, String response) {
        switch (initiator) {


            case INITIATOR_GET_USER_CURRENCY_DATA:
                try {
                    runOnUI(new Runnable() {
                        @Override
                        public void run() {
                            Log.e("REG_DATA RESPONSE", "Response is " + response);

                            JSONObject jObj = null;

                            currencies_ids = new ArrayList<>();
                            currency_prefix=new ArrayList<>();

                            try {

                                jObj = new JSONObject(response);
                                if (jObj.has("errors")) {
                                    String errors = jObj.getString("errors");
                                    showSweetAlertDialogError(errors, ctx);
                                } else { // if no error message was found in the response,
                                    boolean success = jObj.getBoolean("success");
                                    if (success) {
                                        JSONArray currencies = jObj.getJSONArray("data");
                                        ArrayList<String> currencies_drop_down = new ArrayList<String>();
                                        currencies_drop_down.add(getResources().getString(R.string.please_select_currency));
                                        currencies_ids.add("0");
                                        currency_prefix.add("0");
                                        if (currencies.length() > 0) {
                                            for (int i = 0; i < currencies.length(); i++) {
                                                JSONObject jsonObject = currencies.getJSONObject(i);
                                                String id = jsonObject.getString("id");
                                                Log.e("currency_Id_data",id);
                                                String name = jsonObject.getString("name");
                                                String prefix = jsonObject.getString("prefix");
                                                Log.e("cur_prefix",prefix);
                                                currencies_drop_down.add(name);
                                                currencies_ids.add(id);
                                                currency_prefix.add(prefix);
                                            }

                                            currency.setItems(currencies_drop_down);
                                            currency.setSelectedIndex(0);
                                        }


                                    } else {
                                        String api_error_message = jObj.getString("message");
                                        showSweetAlertDialogError(api_error_message, ctx);
                                    }
                                }

                            } catch (Exception e) {
                                Log.e("ExceptionLOGIN-DATA", e.toString());
                                showSweetAlertDialogError(global_error_message, ctx);
                                e.printStackTrace();
                            }
                        }
                    });
                } catch (Exception e) {
//                            hideDialog(ctx);
                    Log.e("Exception- HOME", e.toString());
                    showSweetAlertDialogError(global_error_message, ctx);
//                    Toast.makeText(ctx, "Login Data Response Exception" + e.toString(), Toast.LENGTH_LONG).show();
                }
                break;
            case INITIATOR_ADA_PAY_SOURCES:
                try {
                    runOnUI(new Runnable() {
                        @Override
                        public void run() {
                            Log.e("ADA_PAY_SOURCES_RES", "Response is " + response);

                            JSONObject jObj = null;

                            ada_pay_ids = new ArrayList<>();

                            try {

                                jObj = new JSONObject(response);
                                if (jObj.has("errors")) {
                                    String errors = jObj.getString("errors");
                                    showSweetAlertDialogError(errors, ctx);
                                } else { // if no error message was found in the response,
                                    boolean success = jObj.getBoolean("success");
                                    if (success) {
//                                        JSONObject data = jObj.getJSONObject("message");
                                        JSONArray currencies = jObj.getJSONArray("adapay_sources");
                                        ArrayList<String> currencies_drop_down = new ArrayList<String>();
                                        currencies_drop_down.add(getResources().getString(R.string.please_select_pay_method));
                                        ada_pay_ids.add("0");
                                        if (currencies.length() > 0) {
                                            for (int i = 0; i < currencies.length(); i++) {
                                                JSONObject jsonObject = currencies.getJSONObject(i);
                                                String id = jsonObject.getString("id");
                                                Log.e("Sources_Id_data",id);
                                                String name = jsonObject.getString("type");

                                                String mpesa = jsonObject.getString("msisdn");
                                                String telco = jsonObject.getString("telco");
                                                String toSpinner = "";
                                                if (!TextUtils.isEmpty(mpesa)){
                                                    if (telco.contains("safaricom_ke")){
                                                        String nameData="Mobile Money";
                                                     telco = "Mpesa: " ;
                                                     //  toSpinner  = nameData + "(" +telco +mpesa +")";
                                                     toSpinner  = nameData ;
                                                 }

                                                }else if (telco.contains("safaricom_ke")){
                                                //    name = "Master/Visa Card";

                                                    toSpinner = name;
                                                }
                                                currencies_drop_down.add(toSpinner);
                                                ada_pay_ids.add(name);
                                            }

                                            payMethod.setItems(currencies_drop_down);
                                            payMethod.setSelectedIndex(0);
                                        }


                                    } else {
                                        String api_error_message = jObj.getString("message");
                                        showSweetAlertDialogError(api_error_message, ctx);
                                    }
                                }

                            } catch (Exception e) {
                                Log.e("ExceptionLOGIN-DATA", e.toString());
                                showSweetAlertDialogError(global_error_message, ctx);
                                e.printStackTrace();
                            }
                        }
                    });
                } catch (Exception e) {
//                            hideDialog(ctx);
                    Log.e("Exception- HOME", e.toString());
                    showSweetAlertDialogError(global_error_message, ctx);
//                    Toast.makeText(ctx, "Login Data Response Exception" + e.toString(), Toast.LENGTH_LONG).show();
                }
                break;
            case INITIATOR_ADA_PAY_WITHDRAW:
                try {
                    runOnUI(new Runnable() {
                        @Override
                        public void run() {
                            Log.e("ADA_PAY_WITHDRAW", "Response is " + response);
                            progressBar.setVisibility(View.GONE);
                            JSONObject jObj = null;
                            try {
                                jObj = new JSONObject(response);
                                if (jObj.has("errors")) {
                                    String errors = jObj.getString("errors");
                                    showSweetAlertDialogError(errors, ctx);
                                } else { // if no error message was found in the response,
                                    boolean success = jObj.getBoolean("success");
                                    if (success) {
                                        String api_message = jObj.getString("message");
                                        showSweetAlertDialogSuccess(api_message,ctx);

                                    } else {
                                        String api_error_message = jObj.getString("message");
                                        showSweetAlertDialogError(api_error_message, ctx);
                                    }
                                }

                            } catch (JSONException e) {
                                Log.e("Exception-DATA", e.toString());
                                showSweetAlertDialogError(global_error_message, ctx);
                                e.printStackTrace();
                            }
                        }
                    });
                } catch (Exception e) {
                    progressBar.setVisibility(View.GONE);
                    Log.e("Exception_+GROUP_MEMBER", e.toString());
                    showSweetAlertDialogError(global_error_message, ctx);
//                    Toast.makeText(ctx, "Login Data Response Exception" + e.toString(), Toast.LENGTH_LONG).show();
                }
                break;
        }

    }

    @Override
    public void onDataLoaded(String initiator, JSONArray data) {

    }

    @Override
    public void onNetworkError(String initiator, String errorCode, String errorMessage) {
        switch (initiator) {

            case INITIATOR_GET_USER_CURRENCY_DATA:
                try {
                    runOnUI(new Runnable() {
                        @Override
                        public void run() {
//                                    hideDialog(ctx);
                            showSweetAlertDialogError(errorMessage, ctx);
                        }
                    });
                } catch (Exception e) {
//                            hideDialog(ctx);
                    showSweetAlertDialogError(global_error_message, ctx);
                    Log.e("Exception- HOME", e.toString());
//                    Toast.makeText(ctx, "Login Error Response Exception" + e.toString(), Toast.LENGTH_LONG).show();
                }

                break;
            case INITIATOR_ADA_PAY_SOURCES:
                try {
                    runOnUI(new Runnable() {
                        @Override
                        public void run() {

                            showSweetAlertDialogError("error", ctx);
                        }
                    });
                } catch (Exception e) {

                    showSweetAlertDialogError(global_error_message, ctx);
                    Log.e("Exception- HOME", e.toString());

                }

                break;
            case INITIATOR_ADA_PAY_WITHDRAW:
                try{
                    runOnUI(new Runnable() {
                        @Override
                        public void run() {
                            progressBar.setVisibility(View.GONE);
                            showSweetAlertDialogError(errorMessage, ctx);
                        }
                    });
                }catch (Exception e){
                    progressBar.setVisibility(View.GONE);
                    showSweetAlertDialogError(global_error_message, ctx);
                    Log.e("Exception- HOME", e.toString());
//                    Toast.makeText(ctx, "Login Error Response Exception" + e.toString(), Toast.LENGTH_LONG).show();
                }

                break;

        }

    }

    @Override
    public void onItemSelected(MaterialSpinner view, int position, long id, String item) {
    switch (view.getId()){
        case R.id.currency:
            Log.d("Currency_id_is ", currencies_ids.get(position));
            currency_id_toapi = currencies_ids.get(position);
            currency_prefix_toapi=currency_prefix.get(position);
            break;
        case R.id.payMethod:
            Log.d("ada_pay_ids_is ", ada_pay_ids.get(position));
            ada_pay_id_toapi = ada_pay_ids.get(position);
            break;
       }
    }
}
