package com.app.ecobba.Dialogs

import android.annotation.SuppressLint
import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import com.app.ecobba.Models.Wallet.DepositRequest
import com.app.ecobba.Models.Wallet.DepositResponse
import com.app.ecobba.R
import com.app.ecobba.Utils.RxUtils
import com.app.ecobba.common.rxApi.ApiService
import com.app.ecobba.common.rxApi.RxApi
import com.app.ecobba.recyclerview_models.AdaPay.AdapaySource
import com.app.ecobba.recyclerview_models.AdaPay.Sources
import com.app.ecobba.recyclerview_models.AdaPay.types.SourceTypes
import com.app.ecobba.recyclerview_models.CountryCurrencies
import com.app.ecobba.recyclerview_models.Data
import io.reactivex.Observable
import kotlinx.android.synthetic.main.deposit_dialog.view.*

class DepositFragment(val callback: (DepositFragment, DepositResponse?) -> Unit) : DialogFragment() {
    val api: ApiService by lazy { RxApi().apiService }
    var adapaySource: AdapaySource? = null
    var currency: Data? = null

    @SuppressLint("CheckResult")
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val view = LayoutInflater.from(context).inflate(R.layout.deposit_dialog, null)
        Observable.merge(listOf(api.getCurrencies(), api.getPaymentSources(),api.getSourceTypes()))
                .compose(RxUtils.applyObservableIoSchedulers())
                .doOnSubscribe { view.progressBar.visibility = View.VISIBLE }
                .doFinally { view.progressBar.visibility = View.INVISIBLE }
                .subscribe({
                    try {
                        when (it) {
                            is CountryCurrencies -> {
                                val currencies = it.data.sortedBy { it.prefix }.also {
                                    currency = it.firstOrNull()
                                }
                                view.currency.setItems(currencies.mapIndexedNotNull { index, data -> "${data.prefix} ${data.name}" })
                                view.currency.setOnItemSelectedListener { view, position, id, item ->
                                    currency = currencies[position]
                                }
                            }
                            is Sources -> {
                                val sources = it.adapay_sources.filter { !it.msisdn.isNullOrEmpty() && !it.telco.isNullOrEmpty() }.also {
                                    adapaySource = it.firstOrNull()
                                }
                                view.payMethod.setItems(sources.mapNotNull { "${it.telco?.toLowerCase()?.capitalize()} ${it.msisdn}" })
                                view.payMethod.setOnItemSelectedListener { view, position, id, item ->
                                    adapaySource = sources[position]
                                }
                            }
                            is SourceTypes -> {

                                val sourceTypeList = it.adapay_source_types.toList()



                            }

                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }, {
                    Toast.makeText(requireContext(), "Could not obtain supported currencies", Toast.LENGTH_SHORT).show()
                })
        view.close.setOnClickListener {
            dismiss()
        }
        view.deposit.setOnClickListener {
            val amount = view.editTextAmount.text.toString()
            if (amount.isNullOrEmpty()) {
                Toast.makeText(it.context, "Amount must be greater than 0", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            if (adapaySource == null) {
                Toast.makeText(it.context, "Please select a valid source", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            if (currency == null) {
                Toast.makeText(it.context, "Please select a valid currency", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            val request = DepositRequest(
                    amount = amount.toInt(),
                    source = adapaySource!!,
                    currency = currency!!,
                    method = adapaySource!!.type!!
            )
            api.postDeposit(request).compose(RxUtils.applyObservableIoSchedulers())
                    .doOnSubscribe { view?.progressBar?.visibility = View.VISIBLE }
                    .doFinally { view?.progressBar?.visibility = View.INVISIBLE }
                    .subscribe({
                        callback(this, it)
                    }, {
                        callback(this, null)
                    })
        }
        val dialogBuilder = AlertDialog.Builder(requireContext())
                .setView(view)
        return dialogBuilder.create()
    }
}