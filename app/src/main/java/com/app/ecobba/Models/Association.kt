package com.app.ecobba.Models

data class Association(
        val admin_id: String?,
        val id: String?,
        val name: String?
)