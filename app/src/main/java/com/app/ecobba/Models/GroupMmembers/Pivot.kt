package com.app.ecobba.Models.GroupMmembers

data class Pivot(
    val group_id: Int?,
    val user_id: Int?
)