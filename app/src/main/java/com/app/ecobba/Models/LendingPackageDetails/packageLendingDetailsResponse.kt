package com.app.ecobba.Models.LendingPackageDetails

data class packageLendingDetailsResponse(
    val message: String?,
    val `package`: List<Package>?,
    val success: Boolean?
)