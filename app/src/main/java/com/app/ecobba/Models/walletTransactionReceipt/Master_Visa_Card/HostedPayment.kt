package com.app.ecobba.Models.walletTransactionReceipt.Master_Visa_Card

data class HostedPayment(
    val link: String
)