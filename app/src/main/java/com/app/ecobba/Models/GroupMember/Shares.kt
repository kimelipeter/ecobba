package com.app.ecobba.Models.GroupMember

data class Shares(
    val loan_limit: Int?,
    val total_shares: Int?,
    val total_shares_value: Int?,
    val transactions: List<Any>?
)