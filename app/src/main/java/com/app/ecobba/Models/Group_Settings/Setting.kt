package com.app.ecobba.Models.Group_Settings



data class Setting(
    val activated_groups: List<ActivatedGroup>,
    val created_at: String,
    val description: String,
    val id: Int,
    val name: String,
    val status: Boolean,
    val updated_at: String
)

{
    fun isActivated(groupid: Int = id): Boolean {
        return activated_groups.filter { it.id.toInt() == groupid }.isNotEmpty()
    }

}