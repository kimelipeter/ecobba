package com.app.ecobba.Models.MyResponseShops

import lombok.Data

@Data
class MyShopLostResponse {
    var data: List<Datum>? = null
    var links: Links? = null
    var meta: Meta? = null
    var success: Boolean? = null
}