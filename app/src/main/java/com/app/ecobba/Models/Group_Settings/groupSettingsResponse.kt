package com.app.ecobba.Models.Group_Settings

data class groupSettingsResponse(
    val group: Group,
    val settings: List<Setting>,
    val success: Boolean
)