package com.app.ecobba.Models.walletTransactionReceipt.MobileMoney

data class Wallet(
    val account_number: String,
    val balance: Int,
    val created_at: String,
    val id: Int,
    val status: Int,
    val updated_at: String,
    val user: UserX,
    val user_id: Int
)