package com.app.ecobba.Models.Groups

data class groupsResponse(
    val groups: List<Group>?,
    val success: Boolean?
)