package com.app.ecobba.Models.DepositReceipt

data class TransactionData(
    val server_response_create: ServerResponseCreate,
    val wallet_details: WalletDetails
)