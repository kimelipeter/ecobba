package com.app.ecobba.Models.Wallet

import java.io.Serializable

data class Amount(
    val amount: String? =null,
    val currency: String? =null
) : Serializable