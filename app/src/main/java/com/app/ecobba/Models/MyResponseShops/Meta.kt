package com.app.ecobba.Models.MyResponseShops

import lombok.Data

@Data
class Meta {
    var currentPage: Long = 0
    var from: Long = 0
    var lastPage: Long = 0
    var path: String? = null
    var perPage: Long = 0
    var to: Long = 0
    var total: Long = 0
}