package com.app.ecobba.Models.ChatsResponse

import lombok.Data

@Data
class Inbox {
    var createdAt: String? = null
    var id: Long = 0
    var message: String? = null
    var receiver: Receiver? = null
    var receiverId: Long = 0
    var sender: Sender? = null
    var senderId: Long = 0
    var updatedAt: String? = null
}