package com.app.ecobba.Models.RegistrationData

data class Message(
    val classes: List<Classe>?,
    val countries: List<Country>?,
    val docs: List<Doc>?,
    val genders: List<Gender>?,
    val marital_statuses: List<MaritalStatuse>?,
    val resident_types: List<ResidentType>?
)