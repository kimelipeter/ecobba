package com.app.ecobba.Models.UserLoans

data class Detail(
    val created_at: String?,
    val currency_id: Int?,
    val id: Int?,
    val loan_id: Int?,
    val next_payment_date: Any?,
    val no_of_installments: String?,
    val payback_date: String?,
    val principal: Int?,
    val repayment_plan: String?,
    val repaymentplan: Repaymentplan?,
    val updated_at: String?
)