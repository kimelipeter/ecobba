package com.app.ecobba.Models.payment

data class AttendanceXX(
    val account_no: Any?,
    val account_verified: Int?,
    val avatar: String?,
    val created_at: String?,
    val customer_username: Any?,
    val deleted_at: Any?,
    val email: String?,
    val email_verified_at: Any?,
    val id: Int?,
    val identification_document: String?,
    val msisdn: String?,
    val name: String?,
    val other_names: String?,
    val status: Int?,
    val token: Any?,
    val updated_at: String?,
    val verified: Int?
)