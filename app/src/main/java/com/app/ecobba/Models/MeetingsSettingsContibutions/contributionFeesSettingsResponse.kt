package com.app.ecobba.Models.MeetingsSettingsContibutions

data class contributionFeesSettingsResponse(
    val current_settings: CurrentSettings?,
    val group: Group?,
    val message: String?,
    val setting: Setting?,
    val success: Boolean?
)