package com.app.ecobba.Models.LendingData

data class Repaymentplan(
    val id: Int?,
    val name: String?
)