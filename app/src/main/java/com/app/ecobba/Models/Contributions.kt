package com.app.ecobba.Models

data class Contributions(
        val jamii_fund: JamiiFund?,
        val shares: Shares?
)