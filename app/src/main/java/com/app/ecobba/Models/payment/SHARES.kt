package com.app.ecobba.Models.payment

data class SHARES(
    val contributions: List<ContributionXX>?,
    val total: Int?
)