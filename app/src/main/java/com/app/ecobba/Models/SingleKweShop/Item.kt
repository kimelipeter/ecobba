package com.app.ecobba.Models.SingleKweShop

import com.google.gson.annotations.SerializedName
import lombok.Data

@Data
class Item {
    @SerializedName(value = "created_at", alternate = ["createdAt"])
    var createdAt: String? = null
    var id: Long = 0

    @SerializedName(value = "item_category", alternate = ["itemCategory"])
    var itemCategory: String? = null

    @SerializedName(value = "item_currency", alternate = ["itemCurrency"])
    var itemCurrency: String? = null

    @SerializedName(value = "item_description", alternate = ["itemDescription"])
    var itemDescription: String? = null

    @SerializedName(value = "item_name", alternate = ["itemName"])
    var itemName: String? = null

    @SerializedName(value = "item_price", alternate = ["itemPrice"])
    var itemPrice: Long = 0
    var shop: Shop? = null

    @SerializedName(value = "updated_at", alternate = ["updatedAt"])
    var updatedAt: String? = null

    constructor() {}
    constructor(createdAt: String?, id: Long, itemCategory: String?, itemCurrency: String?, itemDescription: String?, itemName: String?, itemPrice: Long, shop: Shop?, updatedAt: String?) {
        this.createdAt = createdAt
        this.id = id
        this.itemCategory = itemCategory
        this.itemCurrency = itemCurrency
        this.itemDescription = itemDescription
        this.itemName = itemName
        this.itemPrice = itemPrice
        this.shop = shop
        this.updatedAt = updatedAt
    }
}