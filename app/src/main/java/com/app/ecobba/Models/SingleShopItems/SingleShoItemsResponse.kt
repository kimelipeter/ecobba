package com.app.ecobba.Models.SingleShopItems

data class SingleShoItemsResponse(
    val `data`: Data?,
    val success: Boolean?
)