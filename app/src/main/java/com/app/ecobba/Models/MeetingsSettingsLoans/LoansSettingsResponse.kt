package com.app.ecobba.Models.MeetingsSettingsLoans

data class LoansSettingsResponse(
    val current_settings: CurrentSettings?,
    val group: Group?,
    val message: String?,
    val setting: Setting?,
    val success: Boolean?
)