package com.app.ecobba.Models.GroupMember

data class Fees(
    val total: Int?,
    val transactions: List<Any>?
)