package com.app.ecobba.Models.RegisterData

data class Doc(
    val created_at: String?,
    val id: Int?,
    val name: String?,
    val path: Any?,
    val updated_at: String?
)