package com.app.ecobba.Models

data class Pivot(
        val role_id: String,
        val user_id: String,
        val user_type: String
)