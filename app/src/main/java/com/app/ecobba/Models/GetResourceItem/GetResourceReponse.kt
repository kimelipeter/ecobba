package com.app.ecobba.Models.GetResourceItem

data class GetResourceReponse(
    val `data`: Data,
    val message: String,
    val success: Boolean
)