package com.app.ecobba.Models.DepositReceipt

data class Amount(
    val amount: String,
    val currency: String
)