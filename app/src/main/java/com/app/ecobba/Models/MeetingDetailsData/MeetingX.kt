package com.app.ecobba.Models.MeetingDetailsData

data class MeetingX(
    val attendance: List<AttendanceX>?,
    val contributions: List<ContributionXXXX>?,
    val created_at: String?,
    val end_date_time: String?,
    val group_id: Int?,
    val id: Int?,
    val meeting_date_time: String?,
    val meeting_minutes: Any?,
    val meeting_name: String?,
    val settings_index: Int?,
    val status: String?,
    val updated_at: String?,
    val venue: String?
)