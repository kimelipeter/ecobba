package com.app.ecobba.Models.GroupMember

data class groupMmemberResponse(
    val `data`: Data?,
    val message: String?,
    val success: Boolean?
)