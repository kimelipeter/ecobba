package com.app.ecobba.Models.Wallet

import java.io.Serializable

data class TransactionData(
    val attributes: Attributes,
    val id: String? =null,
    val type: String? =null
) : Serializable