package com.app.ecobba.Models.Roles

data class getRolesResponse(
    val message: List<Message>?,
    val success: Boolean?
)