package com.app.ecobba.Models.payment

data class ContributionXXXXXX(
    val amount: Int?,
    val contribution_date: String?,
    val contribution_name: String?,
    val contribution_source: ContributionSourceXXXX?,
    val created_at: String?,
    val end_date_time: String?,
    val fine: String?,
    val group: GroupXXXXX?,
    val group_id: Int?,
    val id: Int?,
    val max_bought: Int?,
    val meeting: MeetingXXX?,
    val meeting_id: Int?,
    val min_bought: Int?,
    val settings_index: Int?,
    val source_id: Int?,
    val status: String?,
    val type: String?,
    val updated_at: String?
)