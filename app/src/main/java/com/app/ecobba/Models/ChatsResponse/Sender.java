
package com.app.ecobba.Models.ChatsResponse;

import lombok.Data;

@Data
@SuppressWarnings("unused")
public class Sender {

    private long id;
    private String name;

}
