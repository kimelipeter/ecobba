package com.app.ecobba.Models

data class HisaContributionSetting(
        val fine_shares: String?,
        val group_id: String?,
        val maximum: String?,
        val minimum: String?,
        val period: String?,
        val setting_id: String?,
        val setting_name: String?,
        val value: String?
)