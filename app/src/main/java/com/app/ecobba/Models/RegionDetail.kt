package com.app.ecobba.Models

data class RegionDetail(
        val associations: List<Association>,
        val level_ones: List<LevelOne>,
        val region_detail: RegionDetailX,
        val regions_arr: List<String>,
        val user: String
)