package com.app.ecobba.Models.GroupMeetingDetails

data class ContributionXX(
    val amount: Int?,
    val contribution_date: String?,
    val contribution_name: String?,
    val created_at: String?,
    val end_date_time: String?,
    val fine: String?,
    val group_id: Int?,
    val id: Int?,
    val max_bought: Int?,
    val meeting_id: Int?,
    val min_bought: Int?,
    val settings_index: Int?,
    val type: String?,
    val updated_at: String?
)