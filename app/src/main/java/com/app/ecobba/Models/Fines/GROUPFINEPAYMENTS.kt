package com.app.ecobba.Models.Fines

data class GROUPFINEPAYMENTS(
    val amount: Int?,
    val contribution_id: Any?,
    val created_at: String?,
    val fine_name: String?,
    val fine_type: String?,
    val group: GroupXX?,
    val group_id: Int?,
    val id: Int?,
    val source_id: Any?,
    val updated_at: String?,
    val user: UserXX?,
    val user_id: Int?
)