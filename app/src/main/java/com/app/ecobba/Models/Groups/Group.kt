package com.app.ecobba.Models.Groups

data class Group(
    val group: GroupX?,
    val members: Int?,
    val totalFees: Int?,
    val totalHisa: Int?,
    val totalJamii: Int?,
    val totalLoans: Int?,
    val wallet: Int?
)