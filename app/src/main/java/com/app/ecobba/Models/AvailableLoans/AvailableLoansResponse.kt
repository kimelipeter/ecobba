package com.app.ecobba.Models.AvailableLoans

data class AvailableLoansResponse(
    val message: String?,
    val packages: List<Package>?,
    val success: Boolean?
)