package com.app.ecobba.Models.GetResourceItem

data class User(
    val account_no: Any,
    val account_verified: Boolean,
    val avatar: String,
    val created_at: String,
    val customer_username: String,
    val deleted_at: String,
    val email: String,
    val email_verified_at: String,
    val id: Int,
    val identification_document: String,
    val msisdn: String,
    val name: String,
    val other_names: String,
    val status: Boolean,
    val token: String,
    val updated_at: String,
    val verified: Boolean
)