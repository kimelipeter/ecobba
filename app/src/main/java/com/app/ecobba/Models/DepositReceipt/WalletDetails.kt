package com.app.ecobba.Models.DepositReceipt

data class WalletDetails(
    val user: User,
    val user_email: String,
    val user_name: String,
    val user_phone: String,
    val wallet: Wallet
)