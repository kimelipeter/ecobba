package com.app.ecobba.Models.UserProfile

data class Currency(
    val name: String,
    val prefix: String
)