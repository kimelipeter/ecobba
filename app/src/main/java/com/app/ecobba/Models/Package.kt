package com.app.ecobba.Models

data class Package(
        val created_at: String,
        val group_id: Int,
        val id: Int,
        val interest_rate: Int,
        val package_name: String,
        val repayment_period: String,
        val repayment_plan: Int,
        val status: Int,
        val updated_at: String
)