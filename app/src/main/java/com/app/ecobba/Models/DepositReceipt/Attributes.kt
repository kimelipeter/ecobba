package com.app.ecobba.Models.DepositReceipt

data class Attributes(
    val amount: Amount,
    val billing: String,
    val created_at: String,
    val currency: String,
    val identifier: String,
    val invoice_number: String,
    val receipt_email: String,
    val receipt_phone: String,
    val reference: String,
    val source: String,
    val statement_descriptor: String,
    val updated_at: String
)