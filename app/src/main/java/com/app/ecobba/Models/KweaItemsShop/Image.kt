package com.app.ecobba.Models.KweaItemsShop

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Image (
    val created_at: String?,
    val id: Int?,
    val image: String?,
    val updated_at: String?
): Parcelable