package com.app.ecobba.Models.Responses

import com.google.gson.annotations.SerializedName
import lombok.Data

@Data
class ShopListResponse {
    @SerializedName("data")
    var shops: List<Shop>? = null
}