package com.app.ecobba.Models.UserProfile

data class Transactions(
    val credit: Int,
    val debit: Int
)