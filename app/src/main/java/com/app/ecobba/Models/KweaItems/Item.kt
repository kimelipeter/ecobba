package com.app.ecobba.Models.KweaItems

import lombok.Data

@Data
class Item {
    var createdAt: String? = null
    var id: Long = 0
    var itemCategory: String? = null
    var itemCurrency: String? = null
    var itemDescription: String? = null
    var itemName: String? = null
    var itemPrice: Long = 0
    var shop: Shop? = null
    var updatedAt: String? = null
}