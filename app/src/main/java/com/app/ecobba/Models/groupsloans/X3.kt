package com.app.ecobba.Models.groupsloans

data class X3(
        val fine: String?,
        val interest: String?,
        val name: String?,
        val payout_source: PayoutSourceXX?,
        val repayment_period: String?,
        val repayment_plan: String?,
        val visibility: String?
)