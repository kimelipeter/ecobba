package com.app.ecobba.Models.GroupMember

data class Group(
    val id: Int?,
    val name: String?,
    val status: Int?
)