package com.app.ecobba.Models.UserProfile

data class User(
    val account_no: String,
    val account_verified: Boolean,
    val avatar: String,
    val created_at: String,
    val credit_score: Int,
    val customer_username: String,
    val deleted_at: String,
    val detail: Detail,
    val email: String,
    val email_verified_at: String,
    val id: Int,
    val identification_document: String,
    val msisdn: String,
    val name: String,
    val next_loan_repayment: NextLoanRepayment,
    val organization: String,
    val other_names: String,
    val roles: List<Role>,
    val status: Boolean,
    val token: String,
    val transactions: Transactions,
    val updated_at: String,
    val verified: Boolean,
    val wallet: Wallet
)