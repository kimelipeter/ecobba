package com.app.ecobba.Models

data class Message(
        var message:String?,
        var date:String?
)