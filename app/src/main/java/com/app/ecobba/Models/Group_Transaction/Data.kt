package com.app.ecobba.Models.Group_Transaction

data class Data(
    val all_transactions: List<AllTransaction>?,
    val member_transactions: List<MemberTransaction>?
)