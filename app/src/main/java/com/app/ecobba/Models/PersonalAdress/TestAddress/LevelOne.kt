package com.app.ecobba.Models.PersonalAdress.TestAddress

data class LevelOne(
    val country_id: Int,
    val created_at: String,
    val id: Int,
    val name: String,
    val updated_at: String
)