package com.app.ecobba.Models.MeetingDetailsData

data class MeetingDetailsDataResponse(
    val `data`: Data?,
    val message: String?,
    val success: Boolean?
)