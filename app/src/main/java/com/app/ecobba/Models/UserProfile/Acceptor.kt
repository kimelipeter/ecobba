package com.app.ecobba.Models.UserProfile

data class Acceptor(
    val email: String,
    val id: Int,
    val msisdn: String,
    val name: String,
    val other_names: String
)