package com.app.ecobba.Models.GroupMmembers

data class Data(
    val account_no: String?,
    val account_verified: Boolean?,
    val avatar: String?,
    val created_at: String?,
    val customer_username: Any?,
    val deleted_at: Any?,
    val email: String?,
    val email_verified_at: Any?,
    val id: Int?,
    val identification_document: String?,
    val member_id: Int?,
    val member_role: MemberRole?,
    val member_status: Boolean?,
    val msisdn: String?,
    val name: String?,
    val other_names: String?,
    val pivot: Pivot?,
    val status: Boolean?,
    val token: Any?,
    val updated_at: String?,
    val verified: Boolean?
)