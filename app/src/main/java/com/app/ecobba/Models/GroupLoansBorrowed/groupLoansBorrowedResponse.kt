package com.app.ecobba.Models.GroupLoansBorrowed

data class groupLoansBorrowedResponse(
    val current_loans: List<CurrentLoan>?,
    val message: String?,
    val success: Boolean?
)