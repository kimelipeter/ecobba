package com.app.ecobba.Models.SingleKweShop

import com.google.gson.annotations.SerializedName
import lombok.Data

@Data
class Shop {
    @SerializedName(value = "created_at", alternate = ["createdAt"])
    var createdAt: String? = null
    var id: Long = 0

    @SerializedName(value = "shop_address", alternate = ["shopAddress"])
    var shopAddress: String? = null

    @SerializedName(value = "shop_email", alternate = ["shopEmail"])
    var shopEmail: String? = null

    @SerializedName(value = "shop_msisdn", alternate = ["shopMsisdn"])
    var shopMsisdn: String? = null

    @SerializedName(value = "shop_name", alternate = ["shopName"])
    var shopName: String? = null

    @SerializedName(value = "updated_at", alternate = ["updatedAt"])
    var updatedAt: String? = null

    @SerializedName(value = "user_id", alternate = ["userId"])
    var userId: Long = 0
}