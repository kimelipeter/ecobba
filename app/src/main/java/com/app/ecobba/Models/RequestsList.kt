package com.app.ecobba.Models

data class RequestsList(
        var requests : List<Pair<String,GenericRequest>>?
)