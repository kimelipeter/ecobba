package com.app.ecobba.Models

data class UserRoleResponse(
        val message: List<UserRole>,
        val success: Boolean
)