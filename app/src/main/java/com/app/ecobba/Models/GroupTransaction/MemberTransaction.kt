package com.app.ecobba.Models.GroupTransaction

data class MemberTransaction(
    val amount: Int?,
    val created_at: String?,
    val currency_id: Int?,
    val group_id: Int?,
    val id: Int?,
    val name: String?,
    val org_id: Any?,
    val other_names: String?,
    val reference: String?,
    val status: Int?,
    val transaction_data: String?,
    val transaction_fees: Int?,
    val transaction_type_id: Int?,
    val txn_code: String?,
    val txn_type: Int?,
    val type: Int?,
    val updated_at: String?,
    val user_id: Int?
)