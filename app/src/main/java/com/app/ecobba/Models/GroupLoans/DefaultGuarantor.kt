package com.app.ecobba.Models.GroupLoans

data class DefaultGuarantor(
    val email: String?,
    val id: Int?,
    val msisdn: String?,
    val name: String?,
    val other_names: String?
)