package com.app.ecobba.Models.adapay

data class AdapaySourceTypes(
    val bank_transfer: String?,
    val card_hosted: String?,
    val mobile_money: String?
)