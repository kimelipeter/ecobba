package com.app.ecobba.Models.walletTransactionReceipt.Master_Visa_Card

data class ServerResponseCreate(
    val charge: Charge,
    val code: Int,
    val message: String
)