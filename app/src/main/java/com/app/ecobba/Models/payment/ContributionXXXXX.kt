package com.app.ecobba.Models.payment

data class ContributionXXXXX(
    val amount: Int?,
    val contribution_date: String?,
    val contribution_name: String?,
    val contribution_source: ContributionSourceXXX?,
    val created_at: String?,
    val end_date_time: String?,
    val fine: String?,
    val group: GroupXXX?,
    val group_id: Int?,
    val id: Int?,
    val max_bought: Any?,
    val meeting: Any?,
    val meeting_id: Any?,
    val min_bought: Any?,
    val settings_index: Int?,
    val source_id: Int?,
    val status: String?,
    val type: String?,
    val updated_at: String?
)