package com.app.ecobba.Models.MeetingDetailsData

data class Data(
    val group: Group?,
    val meeting: Meeting?,
    val meeting_data: MeetingData?,
    val member_contributions: List<MemberContribution>?
)