package com.app.ecobba.Models.groupsloans

data class X1(
        val fine: String?,
        val interest: String?,
        val name: String?,
        val payout_source: PayoutSourceX?,
        val repayment_period: String?,
        val repayment_plan: String?,
        val visibility: String?
)