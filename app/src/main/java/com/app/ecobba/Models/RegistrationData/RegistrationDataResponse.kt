package com.app.ecobba.Models.RegistrationData

data class RegistrationDataResponse(
    val message: Message?,
    val success: Boolean?
)