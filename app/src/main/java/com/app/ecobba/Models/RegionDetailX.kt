package com.app.ecobba.Models

data class RegionDetailX(
        val country_id: String,
        val created_at: String,
        val id: String,
        val level_four: String,
        val level_one: String,
        val level_three: String,
        val level_two: String,
        val updated_at: String
)