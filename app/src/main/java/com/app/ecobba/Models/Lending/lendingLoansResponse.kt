package com.app.ecobba.Models.Lending

data class lendingLoansResponse(
    val loan_packages: List<LoanPackage>?,
    val message: String?,
    val success: Boolean?
)