package com.app.ecobba.Models

data class FeeSetting(
        val amount_contributed: String?,
        val fee_frequency: String?,
        val fee_name: String?,
        val group_id: String?,
        val membership_fee_fine: String?,
        val period: String?,
        val setting_id: String?,
        val setting_name: String?
)