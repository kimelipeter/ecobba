package com.app.ecobba.Models.RegisterData

data class Classe(
    val created_at: String?,
    val id: Int?,
    val name: String?,
    val updated_at: String?
)