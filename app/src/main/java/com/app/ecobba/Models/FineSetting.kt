package com.app.ecobba.Models

data class FineSetting(
        val amount: String,
        val group_id: String,
        val name: String,
        val setting_id: String,
        val setting_name: String? = "Fine"
)