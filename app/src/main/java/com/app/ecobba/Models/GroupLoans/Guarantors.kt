package com.app.ecobba.Models.GroupLoans

data class Guarantors(
    val default_guarantors: List<DefaultGuarantor>?,
    val min_guarantors: Int?
)