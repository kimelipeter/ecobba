package com.app.ecobba.Models.UserProfile

data class Pivot(
    val role_id: Int,
    val user_id: Int,
    val user_type: String
)