package com.app.ecobba.Models.UserLoans

data class Loan(
    val amount: Int?,
    val approvers: Any?,
    val borrower_credit_score: Int?,
    val created_at: String?,
    val detail: Detail?,
    val group: Group?,
    val group_id: Int?,
    val guarantors: List<Guarantor>?,
    val id: Int?,
    val loan_deductions: List<Any>?,
    val loan_description: String?,
    val loan_package: LoanPackage?,
    val loan_package_id: Int?,
    val loan_schedule: List<LoanSchedule>?,
    val loan_source: LoanSource?,
    val loan_source_id: Int?,
    val loan_term: LoanTerm?,
    val loan_title: String?,
    val manual_disburse: Any?,
    val org_id: Any?,
    val status: String?,
    val updated_at: String?,
    val user: User?,
    val user_id: Int?
)