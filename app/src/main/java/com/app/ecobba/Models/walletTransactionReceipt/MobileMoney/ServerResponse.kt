package com.app.ecobba.Models.walletTransactionReceipt.MobileMoney

data class ServerResponse(
    val charge: Charge,
    val code: Int,
    val message: String
)