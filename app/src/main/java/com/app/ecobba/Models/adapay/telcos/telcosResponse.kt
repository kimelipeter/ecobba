package com.app.ecobba.Models.adapay.telcos

data class telcosResponse(
    val adapay_telcos: AdapayTelcos?,
    val message: String?,
    val success: Boolean?
)