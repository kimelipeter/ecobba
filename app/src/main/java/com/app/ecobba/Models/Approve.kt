package com.app.ecobba.Models

data class Approve(
        val action: Int
)

const val APPROVE = 1
const val DENY = 2