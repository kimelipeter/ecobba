package com.app.ecobba.Models.GroupMember

data class Underpayments(
    val total: Int?,
    val transactions: List<Any>?
)