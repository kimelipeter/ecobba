package com.app.ecobba.Models.MeetingsSettingsFines

data class finesSettingsResponse(
    val current_settings: CurrentSettings?,
    val group: Group?,
    val message: String?,
    val setting: Setting?,
    val success: Boolean?
)