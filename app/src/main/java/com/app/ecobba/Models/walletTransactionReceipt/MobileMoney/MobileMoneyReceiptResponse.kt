package com.app.ecobba.Models.walletTransactionReceipt.MobileMoney

data class MobileMoneyReceiptResponse(
    val message: String,
    val success: Boolean,
    val transaction: Transaction
)