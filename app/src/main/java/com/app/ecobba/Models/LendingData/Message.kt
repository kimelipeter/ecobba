package com.app.ecobba.Models.LendingData

data class Message(
    val currencies: List<Currency>?,
    val repaymentplans: List<Repaymentplan>?
)