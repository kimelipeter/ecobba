package com.app.ecobba.Models.GroupMeetingDetails

data class Data(
    val group: Group?,
    val meeting: Meeting?,
    val meeting_data: MeetingData?,
    val member_contributions: List<MemberContribution>?
)