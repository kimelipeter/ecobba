package com.app.ecobba.Models.Kwea

import lombok.Data

@Data
class KweaMarket {
    var shops: List<Shop>? = null
    var success: Boolean? = null
}