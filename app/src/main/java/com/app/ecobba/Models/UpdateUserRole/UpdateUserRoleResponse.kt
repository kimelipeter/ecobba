package com.app.ecobba.Models.UpdateUserRole

data class UpdateUserRoleResponse(
    val `data`: Data?,
    val message: String?,
    val success: Boolean?
)