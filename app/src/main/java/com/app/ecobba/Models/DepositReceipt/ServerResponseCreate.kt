package com.app.ecobba.Models.DepositReceipt

data class ServerResponseCreate(
    val charge: Charge,
    val code: Int,
    val message: String
)