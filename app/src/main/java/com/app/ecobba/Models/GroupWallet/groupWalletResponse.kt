package com.app.ecobba.Models.GroupWallet

data class groupWalletResponse(
    val adapay_sources: List<AdapaySource>?,
    val message: String?,
    val success: Boolean?
)