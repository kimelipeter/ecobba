package com.app.ecobba.Models.LatestTransaction

data class Transaction(
    val amount: Int,
    val created_at: String,
    val group_id: Any,
    val id: Int,
    val org_id: Any,
    val reference: Any,
    val status: Int,
//    val transaction_data: Any,
    val transaction_fees: Int,
    val transaction_type_id: Int,
    val txn_code: String,
    val txn_type: Int,
    val type: Any,
    val updated_at: String,
    val user_id: Int
)