package com.app.ecobba.Models.UserRegion

import lombok.Data

@Data
class UserRegionResponse {
    var associations: List<Association>? = null
    var levelOnes: List<LevelOne>? = null
    var regionDetail: RegionDetail? = null
    var regionsArr: List<String>? = null
    var user: String? = null
}