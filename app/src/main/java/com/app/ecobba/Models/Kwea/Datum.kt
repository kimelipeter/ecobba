package com.app.ecobba.Models.Kwea

import lombok.Data

@Data
class Datum {
    var createdAt: String? = null
    var id: Long = 0
    var images: List<Image>? = null
    var itemCategory: String? = null
    var itemCurrency: String? = null
    var itemDescription: String? = null
    var itemName: String? = null
    var itemPrice: Long = 0
    var shop: Shop? = null
    var updatedAt: String? = null

    constructor() {}
    constructor(createdAt: String?, id: Long, images: List<Image>?, itemCategory: String?, itemCurrency: String?, itemDescription: String?, itemName: String?, itemPrice: Long, shop: Shop?, updatedAt: String?) {
        this.createdAt = createdAt
        this.id = id
        this.images = images
        this.itemCategory = itemCategory
        this.itemCurrency = itemCurrency
        this.itemDescription = itemDescription
        this.itemName = itemName
        this.itemPrice = itemPrice
        this.shop = shop
        this.updatedAt = updatedAt
    }
}