package com.app.ecobba.Models.KweaItems

import lombok.Data

@Data
class Data {
    var createdAt: String? = null
    var id: Long = 0
    var items: List<Item>? = null
    var shopAddress: String? = null
    var shopEmail: String? = null
    var shopMsisdn: String? = null
    var shopName: String? = null
    var updatedAt: String? = null
    var userId: Long = 0
}