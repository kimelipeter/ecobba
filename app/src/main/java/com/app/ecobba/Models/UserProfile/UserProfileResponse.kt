package com.app.ecobba.Models.UserProfile

data class UserProfileResponse(
    val message: String,
    val success: Boolean,
    val user: User
)