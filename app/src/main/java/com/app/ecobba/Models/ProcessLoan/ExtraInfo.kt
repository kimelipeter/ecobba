package com.app.ecobba.Models.ProcessLoan

data class ExtraInfo(
    val acceptors: List<Acceptor>?,
    val min_guarantors: String?
)