package com.app.ecobba.Models.MeetingsSettingsData

data class meetingsSettingsResponse(
    val current_settings: CurrentSettings?,
    val group: Group?,
    val message: String?,
    val setting: Setting?,
    val success: Boolean?
)