package com.app.ecobba.Models.GroupDetails

data class GroupDetailsResponse(
    val `data`: Data?,
    val message: String?,
    val success: Boolean?
)