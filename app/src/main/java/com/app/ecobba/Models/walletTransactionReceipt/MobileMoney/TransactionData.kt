package com.app.ecobba.Models.walletTransactionReceipt.MobileMoney

data class TransactionData(
    val server_response: ServerResponse,
    val wallet_details: WalletDetails
)