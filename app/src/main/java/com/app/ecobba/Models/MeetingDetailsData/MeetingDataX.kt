package com.app.ecobba.Models.MeetingDetailsData

data class MeetingDataX(
    val fees: List<Any>?,
    val shares: List<ShareX>?,
    val society: List<Any>?
)