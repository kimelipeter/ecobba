package com.app.ecobba.Models.GroupMeetings

data class Meeting(
    val attendance: Any?,
    val created_at: String?,
    val end_date_time: Any?,
    val group_id: Int?,
    val id: Int?,
    val meeting_date_time: String?,
    val meeting_minutes: Any?,
    val meeting_name: String?,
    val ready_for_collection: Boolean?,
    val settings_index: Any?,
    val status: String?,
    val updated_at: String?,
    val venue: String?
)