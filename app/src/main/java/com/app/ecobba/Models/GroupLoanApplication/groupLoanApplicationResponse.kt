package com.app.ecobba.Models.GroupLoanApplication

data class groupLoanApplicationResponse(
    val loan_details: LoanDetails?,
    val message: String?,
    val success: Boolean?
)