package com.app.ecobba.Models.GroupWallet

data class AdapaySource(
    val created_at: String?,
    val group: Group?,
    val group_id: Int?,
    val id: Int?,
    val msisdn: String?,
    val source_default: Any?,
    val telco: String?,
    val token: String?,
    val type: String?,
    val updated_at: String?,
    val usage: String?,
    val user_id: Any?
)