package com.app.ecobba.Models.walletTransactionReceipt.Master_Visa_Card

data class Charge(
    val attributes: Attributes,
    val id: String,
    val type: String
)