package com.app.ecobba.Models.UserProfile

data class Income(
    val created_at: String,
    val id: Int,
    val name: String,
    val updated_at: String
)