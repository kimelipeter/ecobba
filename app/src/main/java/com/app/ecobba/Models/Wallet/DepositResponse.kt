package com.app.ecobba.Models.Wallet

import java.io.Serializable

data class DepositResponse(
    val message: String,
    val success: Boolean,
    val transaction: Transaction
) : Serializable