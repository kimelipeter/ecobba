package com.app.ecobba.Models.Fines

data class GROUPFINES(
    val amount: Int?,
    val contribution_id: Any?,
    val created_at: String?,
    val fine_name: String?,
    val fine_type: String?,
    val group: GroupX?,
    val group_id: Int?,
    val id: Int?,
    val source_id: Any?,
    val updated_at: String?,
    val user: UserX?,
    val user_id: Int?
)