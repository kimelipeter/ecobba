package com.app.ecobba.Models

data class GroupX(
        val account_no: String?,
        val account_verified: Int?,
        val association_id: Int?,
        val bank: Int?,
        val bank_branch: Any?,
        val bank_name: String?,
        val comment: String?,
        val created_at: String?,
        val group_certificate: Any?,
        val group_data: Any?,
        val group_number: Any,
        val group_settings: Any,
        val id: Int,
        val level_four: Any,
        val level_one: String,
        val level_three: String,
        val level_two: String,
        val name: String,
        val org_id: Any,
        val status: Int,
        val trainer_id: Any,
        val updated_at: String,
        val user_id: Int
)