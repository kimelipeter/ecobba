package com.app.ecobba.Models

data class LoanToApi(
        var interest:String,
        var fine:String,
        var loan_name:String,
        val setting_id: String,
        var group_id:String,
        var repayment_plan:String,
        var repayment_period:String,
        var setting_name:String
)