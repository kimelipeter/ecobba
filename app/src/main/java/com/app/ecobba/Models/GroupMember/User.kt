package com.app.ecobba.Models.GroupMember

data class User(
    val account_no: Any?,
    val account_verified: Int?,
    val avatar: String?,
    val balance: Int?,
    val created_at: String?,
    val customer_username: Any?,
    val deleted_at: Any?,
    val email: String?,
    val email_verified_at: Any?,
    val id: Int?,
    val identification_document: String?,
    val memberid: Int?,
    val memberstatus: Int?,
    val msisdn: String?,
    val name: String?,
    val other_names: String?,
    val password: String?,
    val remember_token: String?,
    val status: Int?,
    val token: Any?,
    val updated_at: String?,
    val verified: Int?
)