package com.app.ecobba.Models.GroupLoanApplication

data class CurrencyUnit(
    val country_id: Int?,
    val created_at: String?,
    val exchange_rate: String?,
    val id: Int?,
    val name: String?,
    val prefix: String?,
    val status: Int?,
    val updated_at: String?
)