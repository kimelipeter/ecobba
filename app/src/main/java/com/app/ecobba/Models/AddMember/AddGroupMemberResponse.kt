package com.app.ecobba.Models.AddMember

data class AddGroupMemberResponse(
    val message: String?,
    val success: Boolean?
)