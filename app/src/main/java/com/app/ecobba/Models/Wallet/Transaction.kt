package com.app.ecobba.Models.Wallet

import java.io.Serializable

data class Transaction(
    val amount: Int? =null,
    val created_at: String? =null,
    val group_id: Int? =null,
    val id: Int? =null,
    val reference: String ? =null,
    val status: Int? =null,
    val transaction_data: TransactionData? =null,
    val transaction_fees: Int? =null,
    val transaction_type_id: Int? =null,
    val txn_code: String? =null,
    val txn_type: Int? =null,
    val type: Int? =null,
    val updated_at: String? =null,
    val user_id: Int? =null
) : Serializable