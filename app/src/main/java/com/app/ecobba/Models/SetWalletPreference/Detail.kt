package com.app.ecobba.Models.SetWalletPreference

data class Detail(
    val address: String?,
    val branch_id: Any?,
    val city: String?,
    val country: Int?,
    val created_at: String?,
    val dob: String?,
    val doc_no: String?,
    val doc_type: Int?,
    val gender: Int?,
    val group_id: Any?,
    val id: Int?,
    val income: String?,
    val marital_status: Int?,
    val occupation: String?,
    val org_id: Any?,
    val postal_code: String?,
    val residence: Int?,
    val state: String?,
    val updated_at: String?,
    val user_id: Int?
)