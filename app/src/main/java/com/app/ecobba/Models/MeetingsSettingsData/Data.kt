package com.app.ecobba.Models.MeetingsSettingsData

data class Data(
    val day: String?,
    val financial_period: String?,
    val frequency: String?,
    val index: Int?,
    val name: String?,
    val time: String?,
    val venue: String?
)