package com.app.ecobba.Models.payment

data class Contributions(
    val AVAILABLE_CONTRIBUTIONS: List<AVAILABLECONTRIBUTIONS>?,
    val FEES: FEES?,
    val SHARES: SHARES?,
    val SOCIETY: SOCIETY?,
    val TOTAL_CONTRIBUTED: Int?
)