package com.app.ecobba.Models.UserRegion

import com.google.gson.annotations.SerializedName
import lombok.Data

@Data
class LevelOne {
    @SerializedName(value = "country_id", alternate = ["countryId"])
    var countryId: Long = 0

    @SerializedName(value = "created_at", alternate = ["createdAt"])
    var createdAt: String? = null
    var id: Long = 0
    var name: String? = null

    @SerializedName(value = "updated_at", alternate = ["updatedAt"])
    var updatedAt: String? = null
}