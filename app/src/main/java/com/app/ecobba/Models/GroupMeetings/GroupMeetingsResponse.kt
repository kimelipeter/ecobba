package com.app.ecobba.Models.GroupMeetings

data class GroupMeetingsResponse(
        val group: Group?,
        val meetings: List<Meeting>?,
        val message: String?,
        val success: Boolean?
)