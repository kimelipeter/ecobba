package com.app.ecobba.Models.UpdateUserRole

data class Data(
    val created_at: String?,
    val group: Group?,
    val group_id: Int?,
    val group_wallet: Int?,
    val id: Int?,
    val is_admin: Boolean?,
    val member_data: Any?,
    val role: Role?,
    val role_id: Int?,
    val status: Boolean?,
    val updated_at: String?,
    val user: User?,
    val user_id: Int?
)