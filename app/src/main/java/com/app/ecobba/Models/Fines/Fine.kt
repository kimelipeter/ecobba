package com.app.ecobba.Models.Fines

data class Fine(
    val amount: Int?,
    val contribution_id: Any?,
    val created_at: String?,
    val fine_name: String?,
    val fine_type: String?,
    val group: GroupXXXX?,
    val group_id: Int?,
    val id: Int?,
    val source_id: Any?,
    val updated_at: String?,
    val user: UserXXX?,
    val user_id: Int?
)