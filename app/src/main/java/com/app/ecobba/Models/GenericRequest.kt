package com.app.ecobba.Models

data class GenericRequest( var url : String,
                           var token : String,
                           var params : String,
                           var initiator :String,
                           var method: String
)