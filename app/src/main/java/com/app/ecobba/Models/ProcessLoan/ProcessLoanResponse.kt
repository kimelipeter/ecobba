package com.app.ecobba.Models.ProcessLoan

data class ProcessLoanResponse(
    val loan: Loan?,
    val message: String?,
    val success: Boolean?
)