package com.app.ecobba.Models.PersonalAdress.TestAddress

data class ResponseAddress(
    val associations: List<Association>,
    val level_ones: List<LevelOne>,
    val region_detail: RegionDetail,
    val regions_arr: List<String>,
    val user: String
)