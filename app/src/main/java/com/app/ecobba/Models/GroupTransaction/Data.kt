package com.app.ecobba.Models.GroupTransaction

data class Data(
    val all_transactions: List<AllTransaction>?,
    val member_transactions: List<MemberTransaction>?
)