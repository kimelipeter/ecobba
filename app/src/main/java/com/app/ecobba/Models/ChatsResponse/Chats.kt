package com.app.ecobba.Models.ChatsResponse

import lombok.Data

@Data
class Chats {
    var inbox: List<Inbox>? = null
    var outbox: List<Outbox>? = null
}