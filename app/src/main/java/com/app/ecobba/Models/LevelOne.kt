package com.app.ecobba.Models

data class LevelOne(
        val country_id: String?,
        val created_at: String?,
        val id: String?,
        val name: String?,
        val updated_at: String?
)