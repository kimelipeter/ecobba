package com.app.ecobba.Models.GroupLoansBorrowed

data class PackageCurrency(
    val country_id: Int?,
    val created_at: String?,
    val exchange_rate: Any?,
    val id: Int?,
    val name: String?,
    val prefix: String?,
    val status: Int?,
    val updated_at: String?
)