package com.app.ecobba.Models

data class FederationSettings(
        val group: Group?,
        val loans: Loans?,
        val shares: SharesX?
)