package com.app.ecobba.Models.MeetingsSettingsFees

data class Data(
    val amount: String?,
    val day: String?,
    val fine: String?,
    val frequency: String?,
    val meeting: Any?,
    val name: String?,
    val source: Source?,
    val time: String?
)