package com.app.ecobba.Models

data class FeeToApi(
        val amount_contributed: String,
        val membership_fee_fine:String,
        val group_id: String,
        val fee_name: String,
        val setting_id: String,
        val setting_name: String,
        val fee_frequency:String="1"
)