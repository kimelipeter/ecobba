package com.app.ecobba.Models.DepositReceipt

data class DepositReceiptResponse(
    val message: String,
    val success: Boolean,
    val transaction: Transaction
)