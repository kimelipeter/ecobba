package com.app.ecobba.Models.RegisterData

data class GetRegistrationDataResponse(
    val message: Message?,
    val success: Boolean?
)