package com.app.ecobba.Models.walletTransactionReceipt.Master_Visa_Card

data class Amount(
    val amount: String,
    val currency: String
)