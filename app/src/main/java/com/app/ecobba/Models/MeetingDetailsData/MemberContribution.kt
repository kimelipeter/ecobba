package com.app.ecobba.Models.MeetingDetailsData

data class MemberContribution(
    val contribution: ContributionXXX?,
    val contribution_id: Int?,
    val created_at: String?,
    val id: Int?,
    val shares_bought: Int?,
    val status: String?,
    val total_amount: String?,
    val updated_at: String?,
    val user_id: Int?
)