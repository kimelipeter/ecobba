package com.app.ecobba.Models.GroupLoanApplication

data class LoanDetails(
    val currencyUnit: CurrencyUnit?,
    val descriptionOfLoan: String?,
    val expectedScore: Int?,
    val fineRate: String?,
    val guarantors: List<Guarantor>?,
    val installmentAmount: Double?,
    val interestRate: Double?,
    val lengthOfLoan: String?,
    val loan_source: LoanSource?,
    val paymentFrequency: String?,
    val periodFine: Int?,
    val periodInterest: Int?,
    val principal: Int?,
    val score: Int?,
    val serviceFee: Double?,
    val subTotal: Double?,
    val titleOfLoan: String?,
    val totalAmount: Double?,
    val totalFines: Int?,
    val totalInterest: Int?
)