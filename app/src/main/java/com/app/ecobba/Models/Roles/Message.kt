package com.app.ecobba.Models.Roles

data class Message(
    val created_at: String?,
    val description: String?,
    val display_name: String?,
    val id: Int?,
    val name: String?,
    val updated_at: String?
)