package com.app.ecobba.Models

data class Meetings(
        val financial_period: Any?,
        val meeting_day: Any?,
        val meeting_frequency: Any?,
        val meeting_name: Any?,
        val meeting_time: Any?,
        val meeting_venue: Any?
)