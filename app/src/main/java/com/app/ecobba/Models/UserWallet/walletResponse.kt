package com.app.ecobba.Models.UserWallet

data class walletResponse(
    val message: String?,
    val success: Boolean?,
    val transactions: List<Transaction>?
)