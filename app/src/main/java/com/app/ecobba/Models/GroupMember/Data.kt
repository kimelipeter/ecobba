package com.app.ecobba.Models.GroupMember

data class Data(
    val country: String?,
    val `data`: Any?,
    val detail: Detail?,
    val document: String?,
    val fees: Fees?,
    val fines: Fines?,
    val gender: String?,
    val group: Group?,
    val income: String?,
    val jamii: Jamii?,
    val loans: Loans?,
    val member: Member?,
    val overpayments: Overpayments?,
    val residence: String?,
    val roles: List<Role>?,
    val shares: Shares?,
    val underpayments: Underpayments?,
    val user: User?,
    val user_wallet: UserWallet?
)