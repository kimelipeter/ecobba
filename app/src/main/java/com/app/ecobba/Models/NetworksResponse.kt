package com.app.ecobba.Models

data class NetworksResponse(
    val `data`: List<Data>,
    val message: String,
    val success: Boolean
){

    fun getNetworkList():List<String>{
        if (data == null) return emptyList();
        val networks = ArrayList<String>();
        for (datum in data ){
            networks.add(datum.name)
        }
        return networks;
    }

    fun getId(name: String): String? {
        val networkMap = HashMap<String,Int>();
        for (datum in data ){
            networkMap.put(datum.name,datum.id)
        }

        if (networkMap.containsKey(name)) return networkMap.get(name)!!.toString();
        return "-1";

    }

}