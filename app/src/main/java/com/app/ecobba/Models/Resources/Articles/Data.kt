package com.app.ecobba.Models.Resources.Articles

data class Data(
    val attachments: String,
    val attachmenttype: String,
    val category: String,
    val content: String,
    val created_at: String,
    val datesingle: String,
    val id: Int,
    val identification_document: String,
    val imageurl: String,
    val link: String,
    val name: String,
    val type: String,
    val updated_at: String,
    val user: User,
    val user_id: Int,
    val videourl: String
)