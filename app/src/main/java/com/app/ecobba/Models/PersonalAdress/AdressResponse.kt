package com.app.ecobba.Models.PersonalAdress

data class AdressResponse(
    val associations: List<Association>,
    val level_ones: List<LevelOne>,
    val region_detail: RegionDetail,
    val regions_arr: List<String>,
    val user: String
)