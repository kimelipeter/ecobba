package com.app.ecobba.Models.MeetingsSettingsFines

data class Setting(
    val activated_groups: List<ActivatedGroup>?,
    val created_at: String?,
    val description: String?,
    val id: Int?,
    val name: String?,
    val status: Boolean?,
    val updated_at: String?
)