package com.app.ecobba.Models.PaymentSources

data class paymentSourcesResponse(
    val adapay_sources: List<AdapaySource>?,
    val message: String?,
    val success: Boolean?
)