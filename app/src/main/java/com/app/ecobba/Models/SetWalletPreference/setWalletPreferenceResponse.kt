package com.app.ecobba.Models.SetWalletPreference

data class setWalletPreferenceResponse(
    val `data`: Data?,
    val message: String?,
    val success: Boolean?
)