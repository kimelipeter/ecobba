package com.app.ecobba.Models

data class User(
        val account_no: String?,
        val account_verified: String?,
        val avatar: String?,
        val created_at: String?,
        val customer_username: String?,
        val deleted_at: String?,
        val email: String?,
        val id: String?,
        val identification_document: String?,
        val msisdn: String?,
        val name: String?,
        val other_names: String?,
        val roles: List<Role>?,
        val status: String?,
        val token: String?,
        val updated_at: String?,
        val verified: String?,
        val detail : UserDetails?
)