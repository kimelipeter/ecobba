package com.app.ecobba.Models.walletTransactionReceipt.MobileMoney

data class Charge(
    val attributes: Attributes,
    val id: String,
    val type: String
)