package com.app.ecobba.Models.walletTransactionReceipt.MobileMoney

data class Transaction(
    val amount: Int,
    val created_at: String,
    val group_id: Any,
    val id: Int,
    val reference: String,
    val status: Int,
    val transaction_data: TransactionData,
    val transaction_fees: Int,
    val transaction_type_id: Int,
    val txn_code: String,
    val txn_type: Int,
    val type: Int,
    val updated_at: String,
    val user_id: Int
)