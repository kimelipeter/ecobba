package com.app.ecobba.Models.Borrow

data class BoorowResponse(
    val message: String?,
    val packages: List<Package>?,
    val success: Boolean?
)