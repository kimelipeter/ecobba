package com.app.ecobba.Models.GroupMember

data class Approved(
    val amount: Int?,
    val approvers: Any?,
    val borrower_credit_score: Int?,
    val created_at: String?,
    val group_id: Int?,
    val guarantors: String?,
    val id: Int?,
    val loan_description: String?,
    val loan_package_id: Int?,
    val loan_source_id: Int?,
    val loan_title: String?,
    val org_id: Any?,
    val status: String?,
    val updated_at: String?,
    val user_id: Int?
)