package com.app.ecobba.Models.MeetingsSettingsLoans

data class CurrentSettings(
    val created_at: String?,
    val current_index: Int?,
    val `data`: List<Data>?,
    val group_id: Int?,
    val id: Int?,
    val setting_id: Int?,
    val setting_name: String?,
    val updated_at: String?
)