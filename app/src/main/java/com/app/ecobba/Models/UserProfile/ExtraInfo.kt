package com.app.ecobba.Models.UserProfile

data class ExtraInfo(
    val acceptors: List<Acceptor>,
    val min_guarantors: String
)