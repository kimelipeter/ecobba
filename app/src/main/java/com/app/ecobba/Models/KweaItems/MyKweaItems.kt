package com.app.ecobba.Models.KweaItems

import com.app.ecobba.Models.Responses.Shop
import com.google.gson.annotations.SerializedName
import lombok.Data

@Data
class MyKweaItems {
    @SerializedName("data")
    var shops: List<Shop>? = null
    var items: List<Item>? = null
}