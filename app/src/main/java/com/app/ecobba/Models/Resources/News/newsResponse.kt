package com.app.ecobba.Models.Resources.News

data class newsResponse(
    val `data`: List<Data>,
    val message: String,
    val success: Boolean
)