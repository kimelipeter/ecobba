package com.app.ecobba.Models.GroupMember

data class Loans(
    val approved: List<Approved>?,
    val declined: List<Any>?,
    val defaulted: List<Any>?,
    val paid: List<Any>?,
    val pending: List<Any>?
)