package com.app.ecobba.Models.KweaItemsShop

data class KweaItemsShopResponse(
    val `data`: List<Data>?,
    val success: Boolean?
)