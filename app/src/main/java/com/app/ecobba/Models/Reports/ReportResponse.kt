package com.app.ecobba.Models.Reports

data class ReportResponse(
    val data: Data,
    val message: String,
    val success: Boolean
)