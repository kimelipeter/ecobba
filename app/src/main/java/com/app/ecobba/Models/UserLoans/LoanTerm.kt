package com.app.ecobba.Models.UserLoans

data class LoanTerm(
    val created_at: String?,
    val fine_rate: Double?,
    val id: Int?,
    val interest_rate: Double?,
    val loan_id: Int?,
    val repayment_period: String?,
    val repayment_plan: String?,
    val repaymentplan: RepaymentplanXX?,
    val updated_at: String?
)