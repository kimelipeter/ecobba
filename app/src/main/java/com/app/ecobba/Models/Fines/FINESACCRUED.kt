package com.app.ecobba.Models.Fines

data class FINESACCRUED(
    val amount: Int?,
    val contribution_id: Any?,
    val created_at: String?,
    val fine_name: String?,
    val fine_type: String?,
    val group: Group?,
    val group_id: Int?,
    val id: Int?,
    val source_id: Int?,
    val updated_at: String?,
    val user: User?,
    val user_id: Int?
)