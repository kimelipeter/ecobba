package com.app.ecobba.Models.MeetingsSettingsFees

data class MeetingsSettingsFeesResponse(
    val current_settings: CurrentSettings?,
    val group: Group?,
    val message: String?,
    val setting: Setting?,
    val success: Boolean?
)