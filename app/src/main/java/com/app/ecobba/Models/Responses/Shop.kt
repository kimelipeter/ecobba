package com.app.ecobba.Models.Responses

import com.app.ecobba.Models.Kwea.Image
import com.google.gson.annotations.SerializedName
import lombok.Data

@Data
class Shop {
    var category: String? = null

    @SerializedName(value = "created_at", alternate = ["createdAt"])
    var createdAt: String? = null

    @SerializedName(value = "item_description", alternate = ["description"])
    var description: String? = null

    @SerializedName(value = "shop_email", alternate = ["email"])
    var email: String? = null
    var id: Long = 0

    @SerializedName(value = "image", alternate = ["img"])
    var img: String? = null

    @SerializedName(value = "shop_address", alternate = ["location"])
    var location: String? = null
    var name: String? = null

    @SerializedName(value = "user_id", alternate = ["ownerId"])
    var ownerId: Long = 0

    @SerializedName(value = "shop_msisdn", alternate = ["phonenumber"])
    var phonenumber: String? = null
    var picture: String? = null

    @SerializedName(value = "item_price", alternate = ["price"])
    var price: String? = null

    @SerializedName(value = "item_name", alternate = ["productname"])
    var productname: String? = null

    @SerializedName(value = "shop_name", alternate = ["shopname"])
    var shopname: String? = null

    @SerializedName(value = "updated_at", alternate = ["updatedAt"])
    var updatedAt: String? = null
    var item_currency: String? = null
    var images: List<Image>? = null

    constructor() {}
    constructor(images: List<Image>?, category: String?, createdAt: String?, description: String?, email: String?, id: Long, img: String?, location: String?, name: String?, ownerId: Long, phonenumber: String?, picture: String?, price: String?, productname: String?, shopname: String?, updatedAt: String?, item_currency: String?) {
        this.category = category
        this.createdAt = createdAt
        this.description = description
        this.email = email
        this.id = id
        this.img = img
        this.location = location
        this.name = name
        this.ownerId = ownerId
        this.phonenumber = phonenumber
        this.picture = picture
        this.price = price
        this.productname = productname
        this.shopname = shopname
        this.updatedAt = updatedAt
        this.item_currency = item_currency
        this.images = images
    }
}