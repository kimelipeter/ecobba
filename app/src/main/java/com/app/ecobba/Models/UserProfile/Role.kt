package com.app.ecobba.Models.UserProfile

data class Role(
    val created_at: String,
    val description: String,
    val display_name: String,
    val id: Int,
    val name: String,
    val pivot: Pivot,
    val updated_at: String
)