package com.app.ecobba.Models.walletTransactionReceipt.Master_Visa_Card

data class TransactionData(
    val server_response_create: ServerResponseCreate,
    val wallet_details: WalletDetails
)