package com.app.ecobba.Models

data class Credentials(
        var msisdn: String,
        var email: String,
        var password: String,
        var confirm: String
)