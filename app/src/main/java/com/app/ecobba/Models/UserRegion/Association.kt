package com.app.ecobba.Models.UserRegion

import com.google.gson.annotations.SerializedName
import lombok.Data

@Data
class Association {
    @SerializedName(value = "admin_id", alternate = ["adminId"])
    var adminId: Long = 0
    var id: Long = 0
    var name: String? = null
}