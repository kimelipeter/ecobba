package com.app.ecobba.Models.MeetingsSettingsLoans

data class Data(
    val approvers: List<Approver>?,
    val fine: String?,
    val interest: String?,
    val max_amount: String?,
    val min_amount: String?,
    val min_guarantors: String?,
    val name: String?,
    val repayment_period: String?,
    val repayment_plan: String?,
    val settings_index: Int?,
    val settings_type: String?,
    val source: Source?,
    val visibility: String?
)