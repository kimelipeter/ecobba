package com.app.ecobba.Models.DepositReceipt

data class Detail(
    val address: String,
    val branch_id: Any,
    val city: String,
    val country: Int,
    val created_at: String,
    val dob: String,
    val doc_no: String,
    val doc_type: Any,
    val gender: Int,
    val group_id: Any,
    val id: Int,
    val income: String,
    val level_one: Any,
    val marital_status: Int,
    val occupation: String,
    val org_id: Any,
    val postal_code: String,
    val residence: Any,
    val state: String,
    val updated_at: String,
    val user_id: Int
)