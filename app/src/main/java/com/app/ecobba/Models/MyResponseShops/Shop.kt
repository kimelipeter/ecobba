package com.app.ecobba.Models.MyResponseShops

import lombok.Data

@Data
class Shop {
    var createdAt: String? = null
    var id: Long = 0
    var shopAddress: String? = null
    var shopEmail: String? = null
    var shopMsisdn: String? = null
    var shopName: String? = null
    var updatedAt: String? = null
    var userId: Long = 0

    constructor() {}
    constructor(createdAt: String?, id: Long, shopAddress: String?, shopEmail: String?, shopMsisdn: String?, shopName: String?, updatedAt: String?, userId: Long) {
        this.createdAt = createdAt
        this.id = id
        this.shopAddress = shopAddress
        this.shopEmail = shopEmail
        this.shopMsisdn = shopMsisdn
        this.shopName = shopName
        this.updatedAt = updatedAt
        this.userId = userId
    }
}