package com.app.ecobba.Models.Fines

data class Fines(
    val FINES_ACCRUED: List<FINESACCRUED>?,
    val FINE_PAYMENTS: List<Any>?,
    val GROUP_FINES: List<GROUPFINES>?,
    val GROUP_FINE_PAYMENTS: List<GROUPFINEPAYMENTS>?,
    val TOTAL_FINES_ACCRUED: Int?,
    val TOTAL_FINES_PAID: Int?,
    val TOTAL_GROUP_FINES: Int?,
    val TOYAL_GROUP_FINE_PAYMENTS: List<Any>?
)