package com.app.ecobba.Models.UserSummary

data class UserSummaryResponse(
        val loans: Loans?,
        val message: String?,
        val success: Boolean?
)