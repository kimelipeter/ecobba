package com.app.ecobba.Models

data class Role(
        val created_at: String,
        val description: String,
        val id: String,
        val name: String,
        val pivot: Pivot,
        val updated_at: String
)