package com.app.ecobba.Models.DepositReceipt

data class Charge(
    val attributes: Attributes,
    val id: String,
    val type: String
)