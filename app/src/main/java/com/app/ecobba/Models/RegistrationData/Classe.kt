package com.app.ecobba.Models.RegistrationData

data class Classe(
    val created_at: String?,
    val id: Int?,
    val name: String?,
    val updated_at: String?
)