package com.app.ecobba.Models.GroupMember

data class Pivot(
    val role_id: Int?,
    val user_id: Int?,
    val user_type: String?
)