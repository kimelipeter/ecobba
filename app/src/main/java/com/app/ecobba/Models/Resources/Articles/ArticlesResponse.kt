package com.app.ecobba.Models.Resources.Articles

data class ArticlesResponse(
    val `data`: List<Data>,
    val message: String,
    val success: Boolean
)