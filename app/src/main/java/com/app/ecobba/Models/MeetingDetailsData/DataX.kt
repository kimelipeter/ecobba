package com.app.ecobba.Models.MeetingDetailsData

data class DataX(
    val group: GroupX?,
    val meeting: MeetingX?,
    val meeting_data: MeetingDataX?,
    val member_contributions: List<MemberContributionX>?
)