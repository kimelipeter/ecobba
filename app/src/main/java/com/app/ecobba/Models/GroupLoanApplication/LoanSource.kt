package com.app.ecobba.Models.GroupLoanApplication

data class LoanSource(
    val created_at: String?,
    val group_id: String?,
    val id: Int?,
    val msisdn: String?,
    val source_default: String?,
    val telco: String?,
    val token: String?,
    val type: String?,
    val updated_at: String?,
    val usage: String?,
    val user_id: Int?
)