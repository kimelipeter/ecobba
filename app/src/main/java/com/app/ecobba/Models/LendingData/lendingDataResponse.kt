package com.app.ecobba.Models.LendingData

data class lendingDataResponse(
    val message: Message?,
    val success: Boolean?
)