package com.app.ecobba.Models.UserLoans

data class userLoansResponse(
    val loans: List<Loan>?,
    val message: String?,
    val success: Boolean?
)