package com.app.ecobba.Models.Wallet

import java.io.Serializable

data class Attributes(
    val amount: Amount? =null,
    val billing: String? =null,
    val created_at: String? =null,
    val currency: String? =null,
    val identifier: String? =null,
    val invoice_number: String? =null,
    val receipt_email: String? =null,
    val receipt_phone: String? =null,
    val reference: String? =null,
    val source: String? =null,
    val statement_descriptor: String? =null,
    val updated_at: String? =null
) : Serializable