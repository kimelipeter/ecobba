package com.app.ecobba.Models.Resources.Trainings

data class TrainingResponse(
    val `data`: List<Data>,
    val message: String,
    val success: Boolean
)