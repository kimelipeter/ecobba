package com.app.ecobba.Models.GroupMember

data class Overpayments(
    val total: Int?,
    val transactions: List<Any>?
)