package com.app.ecobba.Models.GroupMember

data class Member(
    val created_at: String?,
    val group_id: Int?,
    val group_wallet: Int?,
    val id: Int?,
    val is_admin: Boolean?,
    val member_data: Any?,
    val role_id: Int?,
    val status: Boolean?,
    val updated_at: String?,
    val user_id: Int?
)