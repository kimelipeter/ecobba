package com.app.ecobba.Models.MeetingDetailsData

data class MeetingData(
    val fees: List<Any>?,
    val shares: List<Share>?,
    val society: List<Any>?
)