package com.app.ecobba.Models.SingleGroupDetails

data class SingleGroupDetailsResponse(
    val `data`: Data?,
    val message: String?,
    val success: Boolean?
)