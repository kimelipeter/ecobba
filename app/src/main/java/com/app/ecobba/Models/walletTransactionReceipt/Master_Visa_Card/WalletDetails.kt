package com.app.ecobba.Models.walletTransactionReceipt.Master_Visa_Card

data class WalletDetails(
    val user: User,
    val user_email: String,
    val user_name: String,
    val user_phone: String,
    val wallet: Wallet
)