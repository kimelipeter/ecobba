package com.app.ecobba.Models

data class UserRole(
    val created_at: String,
    val description: String,
    val id: Int,
    val name: String,
    val updated_at: String
)