package com.app.ecobba.Models.GroupMember

data class Fines(
    val paid_fines: List<Any>?,
    val paid_fines_amount: Int?,
    val pending_fines: List<Any>?,
    val pending_fines_amount: Int?
)