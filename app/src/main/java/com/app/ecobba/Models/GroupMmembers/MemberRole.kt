package com.app.ecobba.Models.GroupMmembers

data class MemberRole(
    val created_at: String?,
    val description: String?,
    val display_name: String?,
    val id: Int?,
    val name: String?,
    val updated_at: String?
)