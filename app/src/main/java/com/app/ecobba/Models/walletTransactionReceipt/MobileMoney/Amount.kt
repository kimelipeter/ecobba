package com.app.ecobba.Models.walletTransactionReceipt.MobileMoney

data class Amount(
    val amount: String,
    val currency: String
)