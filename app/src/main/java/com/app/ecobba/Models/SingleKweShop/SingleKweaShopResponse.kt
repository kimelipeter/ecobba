package com.app.ecobba.Models.SingleKweShop

import com.google.gson.annotations.SerializedName

@lombok.Data
class SingleKweaShopResponse {
    @SerializedName("data")
    var data: Data? = null
    var success: Boolean? = null
}