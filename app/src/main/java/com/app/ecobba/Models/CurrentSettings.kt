package com.app.ecobba.Models

data class CurrentSettings(
        val contributions: Contributions?,
        val fees: List<Any>?,
        val fines: Any?,
        val loans: List<Any>?,
        val meetings: Meetings?
)