package com.app.ecobba.Models.walletTransactionReceipt.Master_Visa_Card

data class MasterVisaCardResponse(
    val message: String,
    val success: Boolean,
    val transaction: Transaction
)