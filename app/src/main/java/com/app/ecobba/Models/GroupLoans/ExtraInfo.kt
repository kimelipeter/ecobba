package com.app.ecobba.Models.GroupLoans

data class ExtraInfo(
    val guarantors: Guarantors?
)