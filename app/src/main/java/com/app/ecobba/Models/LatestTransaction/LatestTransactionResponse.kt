package com.app.ecobba.Models.LatestTransaction

data class LatestTransactionResponse(
    val message: String,
    val success: Boolean,
    val transactions: List<Transaction>
)