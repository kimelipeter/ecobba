package com.app.ecobba.Models.walletTransactionReceipt.Master_Visa_Card

data class UserX(
    val account_no: String,
    val account_verified: Int,
    val avatar: String,
    val created_at: String,
    val customer_username: String,
    val deleted_at: String,
    val email: String,
    val email_verified_at: String,
    val id: Int,
    val identification_document: String,
    val msisdn: String,
    val name: String,
    val other_names: String,
    val status: Int,
    val token: String,
    val updated_at: String,
    val verified: Int
)