package com.app.ecobba.Models.GroupMember

data class UserWallet(
    val account_number: String?,
    val balance: Int?,
    val created_at: String?,
    val id: Int?,
    val status: Int?,
    val updated_at: String?,
    val user_id: Int?
)