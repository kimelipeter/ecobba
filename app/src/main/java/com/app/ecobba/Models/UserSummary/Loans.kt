package com.app.ecobba.Models.UserSummary

data class Loans(
    val TotalGroups: Int?,
    val groupsjoined: Int?,
    val mygroups: Int?,
    val myloans: Int?,
    val mypackages: Int?
)