package com.app.ecobba.Models.PersonalAdress
data class RegionDetail(
    val country_id: Int,
        val created_at: String,
        val id: Int,
        val level_four: String,
        val level_one: String,
        val level_three: String,
        val level_two: String,
        val updated_at: String
)