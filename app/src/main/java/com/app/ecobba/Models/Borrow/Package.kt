package com.app.ecobba.Models.Borrow

data class Package(
    val created_at: String?,
    val currency: String?,
    val description: String?,
    val extra_info: Any?,
    val fine_rate: String?,
    val group_id: Any?,
    val group_loan_type: Any?,
    val group_settings_index: Any?,
    val id: Int?,
    val insured: Int?,
    val interest_rate: Double?,
    val max_amount: Int?,
    val min_amount: Int?,
    val min_credit_score: Int?,
    val name: String?,
    val org_id: Any?,
    val package_type: String?,
    val package_visibilty: String?,
    val repayment_period: Any?,
    val repayment_plan: String?,
    val status: Int?,
    val updated_at: String?,
    val user_id: Int?
)