package com.app.ecobba.Models

data class MeetingSetting(
        val day: String,
        val financial_period: String,
        val frequency: String,
        val group_id: String,
        val setting_id: String,
        val setting_name: String = "Meeting",
        val time: String,
        val venue: String
)