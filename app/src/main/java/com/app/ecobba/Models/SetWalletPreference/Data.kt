package com.app.ecobba.Models.SetWalletPreference

data class Data(
    val created_at: String?,
    val id: Int?,
    val msisdn: String?,
    val telco: String?,
    val token: String?,
    val type: String?,
    val updated_at: String?,
    val usage: String?,
    val user: User?,
    val user_id: Int?
)