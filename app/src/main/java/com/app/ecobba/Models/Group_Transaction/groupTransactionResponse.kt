package com.app.ecobba.Models.Group_Transaction

data class groupTransactionResponse(
    val `data`: Data?,
    val message: String?,
    val success: Boolean?
)