package com.app.ecobba.Models.SingleShopItems

data class Item(
    val created_at: String?,
    val id: Int?,
    val item_category: String?,
    val item_currency: String?,
    val item_description: String?,
    val item_name: String?,
    val item_price: Int?,
    val shop: Shop?,
    val updated_at: String?
)