package com.app.ecobba.Models.GroupTransaction

data class GroupTransactionResponse(
    val `data`: Data?,
    val message: String?,
    val success: Boolean?
)