package com.app.ecobba.Models.GroupLoans

data class PackageSource(
    val created_at: String?,
    val group_id: Any?,
    val id: Int?,
    val msisdn: String?,
    val source_default: Any?,
    val telco: String?,
    val token: String?,
    val type: String?,
    val updated_at: String?,
    val usage: String?,
    val user_id: Int?
)