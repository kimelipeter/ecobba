package com.app.ecobba.Models.Fines

data class FinesResponse(
    val `data`: Data?,
    val message: String?,
    val success: Boolean?
)