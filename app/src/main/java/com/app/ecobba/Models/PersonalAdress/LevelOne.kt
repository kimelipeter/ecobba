package com.app.ecobba.Models.PersonalAdress

data class LevelOne(
//    val country_id: Int,
//    val created_at: String,
//    val id: Int,
//    val name: String,
//    val updated_at: String

    val country_id: String?,
    val created_at: String?,
    val id: String?,
    val name: String?,
    val updated_at: String?
)