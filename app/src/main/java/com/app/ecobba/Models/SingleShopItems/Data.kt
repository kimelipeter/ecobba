package com.app.ecobba.Models.SingleShopItems

data class Data(
    val created_at: String?,
    val id: Int?,
    val items: List<Item>?,
    val shop_address: String?,
    val shop_email: String?,
    val shop_msisdn: String?,
    val shop_name: String?,
    val updated_at: String?,
    val user: UserX?,
    val user_id: Int?
)