package com.app.ecobba.Models

data class SharesX(
        val max_shares: Any?,
        val min_shares: Any?
)