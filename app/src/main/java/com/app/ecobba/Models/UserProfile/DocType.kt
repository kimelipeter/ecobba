package com.app.ecobba.Models.UserProfile

data class DocType(
    val created_at: String,
    val id: Int,
    val name: String,
    val path: Any,
    val updated_at: String
)