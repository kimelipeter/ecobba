package com.app.ecobba.Models.GroupLoans

data class getGroupLoansResponse(
    val packages: List<Package>?,
    val success: Boolean?
)