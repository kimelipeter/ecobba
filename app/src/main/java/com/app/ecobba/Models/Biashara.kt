package com.app.ecobba.Models

data class Biashara(
        val max_interest_biashara: String?,
        val max_period_biashara: String?,
        val min_interest_biashara: String?,
        val min_period_biashara: String?,
        val repayment_plan_biashara: String?
)