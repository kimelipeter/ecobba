package com.app.ecobba.Models.shopsdashboard

data class shopsDataResponse(
    val data: List<Data>,
    val success: Boolean
)