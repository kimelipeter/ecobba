package com.app.ecobba.Models.RegistrationData

data class Country(
    val created_at: String?,
    val id: Int?,
    val name: String?,
    val phonecode: String?,
    val prefix: String?,
    val status: Int?,
    val updated_at: String?
)