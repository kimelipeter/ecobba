package com.app.ecobba.Models

data class Jamii(
        val max_interest_jamii: String,
        val max_period_jamii: String,
        val min_interest_jamii: String,
        val min_period_jamii: String,
        val repayment_plan_jamii: String
)