package com.app.ecobba.Models.UserProfile

data class LoanDetail(
    val created_at: String,
    val currency_id: Int,
    val id: Int,
    val loan_id: Int,
    val next_payment_date: String,
    val no_of_installments: String,
    val payback_date: String,
    val principal: Int,
    val repayment_plan: String,
    val repaymentplan: RepaymentplanXXX,
    val updated_at: String
)