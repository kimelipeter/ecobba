package com.app.ecobba.Models.PersonalAdress

data class Association(
    val admin_id: Int,
    val id: Int,
    val name: String
)