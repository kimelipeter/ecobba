package com.app.ecobba.Models.ProcessLoan

data class LoanTerm(
    val created_at: String?,
    val fine_rate: Int?,
    val id: Int?,
    val interest_rate: Int?,
    val loan_id: Int?,
    val repayment_period: String?,
    val repayment_plan: String?,
    val repaymentplan: RepaymentplanXX?,
    val updated_at: String?
)