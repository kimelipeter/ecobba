package com.app.ecobba.Models.MeetingsSettingsContibutions

data class Data(
    val day: String?,
    val fine: String?,
    val frequency: String?,
    val maximum: String?,
    val meeting: Any?,
    val minimum: String?,
    val name: String?,
    val settings_index: Int?,
    val settings_type: String?,
    val source: Source?,
    val time: String?,
    val value: String?
)