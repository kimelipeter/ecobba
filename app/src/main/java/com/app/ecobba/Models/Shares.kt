package com.app.ecobba.Models

data class Shares(
        val fines: Any,
        val frequency: Any,
        val max_shares: Any,
        val min_shares: Any,
        val value_shares: Any
)