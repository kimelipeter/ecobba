package com.app.ecobba.Models.GroupLoansBorrowed

data class LoanSchedule(
    val created_at: String?,
    val fine_accrued: Int?,
    val id: Int?,
    val interest_accrued: Float?,
    val loan_id: Int?,
    val payment_amount: Double?,
    val payment_date: String?,
    val status: String?,
    val updated_at: String?
)