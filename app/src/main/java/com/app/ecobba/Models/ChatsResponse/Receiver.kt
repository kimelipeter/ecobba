package com.app.ecobba.Models.ChatsResponse

import lombok.Data

@Data
class Receiver {
    var id: Long = 0
    var name: String? = null
}