package com.app.ecobba.Models.GroupMeetingDetails

data class MeetingData(
    val fees: List<Any>?,
    val shares: List<Share>?,
    val society: List<Any>?
)