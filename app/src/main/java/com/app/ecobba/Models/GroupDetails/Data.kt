package com.app.ecobba.Models.GroupDetails

data class Data(
    val balance: Int?,
    val group: Group?,
    val total_members: Int?
)