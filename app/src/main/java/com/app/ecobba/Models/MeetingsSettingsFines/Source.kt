package com.app.ecobba.Models.MeetingsSettingsFines

data class Source(
    val created_at: String?,
    val group_id: Int?,
    val id: Int?,
    val msisdn: String?,
    val source_default: Any?,
    val telco: String?,
    val token: String?,
    val type: String?,
    val updated_at: String?,
    val usage: String?,
    val user: User?,
    val user_id: Int?
)