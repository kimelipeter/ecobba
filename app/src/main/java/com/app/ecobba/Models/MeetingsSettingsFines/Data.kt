package com.app.ecobba.Models.MeetingsSettingsFines

data class Data(
    val amount: String?,
    val index: Int?,
    val name: String?,
    val source: Source?
)