package com.app.ecobba.Models

data class JamiiFund(
        val contributions: Any?,
        val fines: Any?,
        val frequency: Any?
)