package com.app.ecobba.Models.GroupMember

data class Jamii(
    val total: Int?,
    val transactions: List<Any>?
)