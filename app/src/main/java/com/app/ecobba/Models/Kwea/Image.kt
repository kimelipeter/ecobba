package com.app.ecobba.Models.Kwea

import lombok.Data

@Data
class Image {
    var createdAt: String? = null
    var id: Long = 0
    var image: String? = null
    var updatedAt: String? = null
}