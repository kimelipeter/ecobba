package com.app.ecobba.Models.MyResponseShops

import lombok.Data

@Data
class Links {
    var first: String? = null
    var last: String? = null
    var next: Any? = null
    var prev: Any? = null
}