package com.app.ecobba.Models

data class GroupSettings(
        val current_settings: CurrentSettings?,
        val federation_settings: FederationSettings?,
        val group: GroupX,
        val message: String,
        val packages: List<Package>,
        val plans: List<Plan>,
        val success: Boolean
)