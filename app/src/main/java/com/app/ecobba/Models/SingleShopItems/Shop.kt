package com.app.ecobba.Models.SingleShopItems

data class Shop(
    val created_at: String?,
    val id: Int?,
    val shop_address: String?,
    val shop_email: String?,
    val shop_msisdn: String?,
    val shop_name: String?,
    val updated_at: String?,
    val user: User?,
    val user_id: Int?
)