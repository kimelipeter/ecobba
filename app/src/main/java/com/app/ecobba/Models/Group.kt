package com.app.ecobba.Models

data class Group(
        val max_members: String?,
        val min_members: String?
)