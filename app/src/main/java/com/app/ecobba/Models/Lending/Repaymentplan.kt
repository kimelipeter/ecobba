package com.app.ecobba.Models.Lending

data class Repaymentplan(
    val created_at: String?,
    val id: Int?,
    val name: String?,
    val updated_at: String?
)