package com.app.ecobba.Models.UserRegion

import com.google.gson.annotations.SerializedName
import lombok.Data

@Data
class RegionDetail {
    @SerializedName(value = "country_id", alternate = ["countryId"])
    var countryId: Long = 0

    @SerializedName(value = "created_at", alternate = ["createdAt"])
    var createdAt: String? = null
    var id: Long = 0

    @SerializedName(value = "level_four", alternate = ["levelFour"])
    var levelFour: String? = null

    @SerializedName(value = "level_one", alternate = ["levelOne"])
    var levelOne: String? = null

    @SerializedName(value = "level_three", alternate = ["levelThree"])
    var levelThree: String? = null

    @SerializedName(value = "level_two", alternate = ["levelTwo"])
    var levelTwo: String? = null

    @SerializedName(value = "updated_at", alternate = ["updatedAt"])
    var updatedAt: String? = null
}