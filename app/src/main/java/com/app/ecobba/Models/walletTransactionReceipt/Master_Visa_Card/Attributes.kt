package com.app.ecobba.Models.walletTransactionReceipt.Master_Visa_Card

data class Attributes(
    val amount: Amount,
    val billing: String,
    val created_at: String,
    val currency: String,
    val hosted_payment: HostedPayment,
    val identifier: String,
    val invoice_number: String,
    val receipt_email: String,
    val receipt_phone: String,
    val reference: String,
    val source: String,
    val statement_descriptor: String,
    val updated_at: String
)