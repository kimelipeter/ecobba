package com.app.ecobba.Models.adapay

data class adapaySourcesResponse(
    val adapay_source_types: AdapaySourceTypes?,
    val message: String?,
    val success: Boolean?
)