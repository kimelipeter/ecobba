package com.app.ecobba.Models.Wallet


import com.app.ecobba.recyclerview_models.AdaPay.AdapaySource
import com.app.ecobba.recyclerview_models.Data

data class DepositRequest(
        val amount: Int = 0,
        val currency: Data,
        val method: String,
        val source: AdapaySource
)