package com.app.ecobba.Models.payment

data class FEES(
    val contributions: List<Contribution>?,
    val total: Int?
)