package com.app.ecobba.Models.UserProfile

data class NextLoanRepayment(
    val countdown: Int,
    val created_at: String,
    val fine_accrued: Int,
    val id: Int,
    val interest_accrued: Double,
    val loan: Loan,
    val loan_detail: LoanDetail,
    val loan_id: Int,
    val payment_amount: Double,
    val payment_date: String,
    val status: String,
    val updated_at: String
)