package com.app.ecobba.Models.GroupMeetingDetails

data class GroupMeetingDetailsResponse(
    val `data`: Data?,
    val message: String?,
    val success: Boolean?
)