package com.app.ecobba.Models

data class JamiiContributionSetting(
        val fine_jamii: String,
        val group_id: String,
        val period_jamii: String,
        val setting_id: String,
        val setting_name: String,
        val value_jamii: String
)