package com.app.ecobba.Models.UserProfile

data class Detail(
    val address: String,
    val branch_id: Any,
    val city: String,
    val country: Country,
    val created_at: String,
    val dob: String,
    val doc_no: String,
    val doc_type: DocType,
    val gender: Gender,
    val group_id: Int,
    val id: Int,
    val income: Income,
    val marital_status: MaritalStatus,
    val occupation: String,
    val org_id: Any,
    val postal_code: String,
    val residence: Residence,
    val state: String,
    val updated_at: String,
    val user_id: Int
)