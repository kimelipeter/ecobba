package com.app.ecobba.Fragments.Group.Member;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.RecyclerView;

import com.app.ecobba.Models.GroupMember.Data;
import com.app.ecobba.Models.GroupMember.groupMmemberResponse;
import com.app.ecobba.Models.UserProfile.Detail;
import com.app.ecobba.Models.UserProfile.Role;
import com.app.ecobba.Models.UserRole;
import com.app.ecobba.R;
import com.app.ecobba.Utils.BaseFragment;
import com.app.ecobba.ViewModel.UserProfileViewModel;
import com.app.ecobba.common.SharedMethods;
import com.app.ecobba.common.rxApi.Api;
import com.app.ecobba.common.rxApi.RxApi;
import com.bumptech.glide.Glide;
import com.flipboard.bottomsheet.BottomSheetLayout;
import com.google.android.material.chip.ChipGroup;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.app.ecobba.common.SharedMethods.GroupDigits;
import static com.app.ecobba.common.SharedMethods.HTTP_GET;
import static com.app.ecobba.common.SharedMethods.HTTP_POST;
import static com.app.ecobba.common.SharedMethods.getFromServer;
import static com.app.ecobba.common.SharedMethods.postToServer;

public class GroupMember extends BaseFragment implements View.OnClickListener {

    @BindView(R.id.tvFirstName)
    TextView tvFirstName;

    @BindView(R.id.tvRole)
    TextView tvRole;

    @BindView(R.id.tvNumber)
    TextView tvNumber;

    @BindView(R.id.tvEmail)
    TextView tvEmail;

    @BindView(R.id.tvHisaNumber)
    TextView tvHisaNumber;

    @BindView(R.id.tvHisaValue)
    TextView tvHisaValue;

    @BindView(R.id.tvLoanLimit)
    TextView tvLoanLimit;

    @BindView(R.id.tvLoanApproved)
    TextView tvLoanApproved;

    @BindView(R.id.tvLoanPending)
    TextView tvLoanPending;

    @BindView(R.id.tvLoanDeclined)
    TextView tvLoanDeclined;

    @BindView(R.id.tvLoanPaid)
    TextView tvLoanPaid;

    @BindView(R.id.tvLoanDefaulted)
    TextView tvLoanDefaulted;

    @BindView(R.id.tvFinesPaid)
    TextView tvFinesPaid;

    @BindView(R.id.tvFinesPending)
    TextView tvFinesPending;

    @BindView(R.id.tvJamii)
    TextView tvJamii;

    @BindView(R.id.tvFees)
    TextView tvFees;
    @BindView(R.id.profile_image)
    CircleImageView profile_image;

    @BindView(R.id.fabRoleEdit)
    FloatingActionButton fabRoleEdit;

    Data member;

    BottomSheetLayout bottomSheetLayout;

    View bottomView;

    ChipGroup cgRoles;

    RecyclerView rvRoleDescription;

    List<UserRole> userRoles;

    String selectedId = "";
    String member_id, avatar;
    int user_id,group_id;

    String token;

    Context ctx;
    UserProfileViewModel userProfileViewModel;



    private Api api;
    MemberRolesBottomSheetDialog memberRolesBottomSheetDialog;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View frag = inflater.inflate(R.layout.fragment_group_member_edit, container, false);
        ButterKnife.bind(this, frag);
        ctx = getActivity();
        userProfileViewModel = new ViewModelProvider(getActivity()).get(UserProfileViewModel.class);
        api = new RxApi().getApi();
        userRoles = new ArrayList<>();
        Bundle args = getArguments();
        token = SharedMethods.getDefaults("token", ctx);
        fabRoleEdit.setOnClickListener(this);

        bottomSheetLayout = getActivity().findViewById(R.id.bottomSheetLayout);
        bottomSheetLayout.setPeekOnDismiss(true);
        bottomSheetLayout.setPeekSheetTranslation(900);
        bottomSheetLayout.setUseHardwareLayerWhileAnimating(true);
        bottomSheetLayout.setShouldDimContentView(true);
        bottomView = getLayoutInflater().inflate(R.layout.fragment_group_member_roles, bottomSheetLayout, false);
        Button close = bottomView.findViewById(R.id.btnCloseSheet);
        Button apply = bottomView.findViewById(R.id.btnApply);
        cgRoles = bottomView.findViewById(R.id.cgRoles);
        cgRoles.setSingleSelection(true);
        rvRoleDescription = bottomView.findViewById(R.id.rvRoleDescription);
        close.setOnClickListener(this);
        apply.setOnClickListener(this);


        args = getArguments();
        assert args != null;
        member_id = args.getString("MEMBER");
        avatar = args.getString("avatar");

        return frag;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        fetchMemberDetails(member_id);
        fetchUsersData();

    }


    @SuppressLint("LongLogTag")
    private void fetchMemberDetails(String member_id) {
        Call<groupMmemberResponse> call = api.getGroupMeber(member_id);
        call.enqueue(new Callback<groupMmemberResponse>() {
            @Override
            public void onResponse(Call<groupMmemberResponse> call, Response<groupMmemberResponse> response) {
                Log.e("adadadada", "onResponse: " + response);
                try {
                    if (response.body().getSuccess()) {
                        Glide.with(ctx).load(avatar).centerCrop().into(profile_image);
                        tvFirstName.setText(response.body().getData().getUser().getName() + " " + response.body().getData().getUser().getOther_names());
                        tvNumber.setText(response.body().getData().getUser().getMsisdn());
                        tvEmail.setText(response.body().getData().getUser().getEmail());
                         user_id=response.body().getData().getUser().getId();
                         group_id=response.body().getData().getGroup().getId();
                        List<com.app.ecobba.Models.GroupMember.Role> roles = response.body().getData().getRoles();
                        for (int i = 0; i < roles.size(); i++) {
                            String name = roles.get(i).getName();

                            if (name.equals("group-admin")) {
                                tvRole.setText(R.string.coordinator);
                            } else if (name.equals("group-member") && !name.contains("group-admin")) {
                                tvRole.setText(R.string.member);
                            } else if (name.equals("group-secretary")) {

                                tvRole.setText(R.string.secretary);
                            } else if (name.equals("normal-user")) {

                                tvRole.setText(R.string.normal_user);
                            } else if (name.equals("group-treasurer")) {

                                tvRole.setText(R.string.treasurer);
                            } else if (name.equals("service-provider")) {

                                tvRole.setText(R.string.sevice_provider);
                            } else if (name.equals("treasurer")) {

                                tvRole.setText(R.string.treasurer);
                            }
                        }

                        String hisaAmount = String.valueOf(response.body().getData().getShares().getTotal_shares());
                        String hisaValue = String.valueOf(response.body().getData().getShares().getTotal_shares_value());
                        String loanLimit = String.valueOf(response.body().getData().getShares().getLoan_limit());

                        tvHisaNumber.setText(hisaAmount);
                        tvHisaValue.setText(GroupDigits(hisaValue));
                        tvLoanLimit.setText(GroupDigits(loanLimit));

                        int pending = response.body().getData().getLoans().getPending().size(),
                                approved = response.body().getData().getLoans().getApproved().size(),
                                declined = response.body().getData().getLoans().getDeclined().size(),
                                paid = response.body().getData().getLoans().getPaid().size(),
                                defaulted = response.body().getData().getLoans().getDefaulted().size();

                        tvLoanApproved.setText(String.valueOf(approved));
                        tvLoanPending.setText(String.valueOf(pending));
                        tvLoanDeclined.setText(String.valueOf(declined));
                        tvLoanPaid.setText(String.valueOf(paid));
                        tvLoanDefaulted.setText(String.valueOf(defaulted));

                        String jamiiTotal = String.valueOf(response.body().getData().getJamii().getTotal());

                        tvJamii.setText(GroupDigits(jamiiTotal));

                        String feeTotal = String.valueOf(response.body().getData().getFees().getTotal());

                        tvFees.setText(GroupDigits(feeTotal));


                        String pendingFines = String.valueOf(response.body().getData().getFines().getPending_fines_amount());
                        String paidFines = String.valueOf(response.body().getData().getFines().getPaid_fines_amount());

                        tvFinesPending.setText(GroupDigits(pendingFines));
                        tvFinesPaid.setText(GroupDigits(paidFines));

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<groupMmemberResponse> call, Throwable t) {
                Log.e("yyyyyyyyyyy", "onFailure: " + t.getLocalizedMessage());
            }
        });

    }


    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.fabRoleEdit:
                memberRoles();
                break;
            case R.id.btnCloseSheet:
                bottomView.getLayoutParams().height = (int) getResources().getDimension(R.dimen.height);
                bottomSheetLayout.dismissSheet();
                break;
        }
    }

    private void memberRoles() {
        memberRolesBottomSheetDialog = new MemberRolesBottomSheetDialog(user_id,group_id);
        memberRolesBottomSheetDialog.show(getChildFragmentManager(), "TaG");

    }

    private void fetchUsersData() {
        userProfileViewModel.getUserProfile();
        userProfileViewModel.userProfileResponseLiveData().observe(getViewLifecycleOwner(), userProfileResponse -> {
            try {
                if (userProfileResponse.getSuccess()) {
                    Detail userDetails = userProfileResponse.getUser().getDetail();
                    List<Role> roles = userProfileResponse.getUser().getRoles();
                    for (com.app.ecobba.Models.UserProfile.Role role1 : roles) {
                        String member_login_role = role1.getName();
                        Log.e("MEMBER_LOGIN_ROLE", member_login_role);
                        if (member_login_role.equals("group-admin")) {
                            fabRoleEdit.setVisibility(View.VISIBLE);

                        }
                    }




                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

}
