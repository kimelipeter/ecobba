
package com.app.ecobba.Fragments.Profile.UserProfile;

import lombok.Data;

@Data
@SuppressWarnings("unused")
public class Detail {

    private String address;
    private String branchId;
    private String city;
    private long country;
    private String createdAt;
    private String dob;
    private String docNo;
    private long docType;
    private long gender;
    private String groupId;
    private long id;
    private long income;
    private long maritalStatus;
    private String occupation;
    private long orgId;
    private String postalCode;
    private long residence;
    private String state;
    private String updatedAt;
    private long userId;

}
