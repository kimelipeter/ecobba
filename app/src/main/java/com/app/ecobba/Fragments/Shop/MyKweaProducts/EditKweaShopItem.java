package com.app.ecobba.Fragments.Shop.MyKweaProducts;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatButton;

import com.app.ecobba.Models.GroupMeetings.GroupMeetingsResponse;
import com.app.ecobba.R;
import com.app.ecobba.common.rxApi.Api;
import com.app.ecobba.common.rxApi.RxApi;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

//import cn.pedant.SweetAlert.SweetAlertDialog;
import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.app.ecobba.common.SharedMethods.showSweetAlertDialogSuccess;

public class EditKweaShopItem  extends BottomSheetDialogFragment {
    String item_name,item_amount,item_description,shop_name,item_id;
    ImageView close;
    EditText itemName,itemPrice,itemDescription;
    TextView shopName;
    Spinner item_category,item_currency;
    String selected_item_category,selected_item_currency,_item_name,_item_price,_item_description;
    AppCompatButton update;
    private Api api;
    public ProgressDialog progressDialog;
    public EditKweaShopItem(String item_name, String item_amount, String item_description, String shop_name, String item_id) {
        this.item_name=item_name;
        this.item_amount=item_amount;
        this.item_description=item_description;
        this.shop_name=shop_name;
        this.item_id=item_id;
        Log.e("WEDFGHJKWERTYU",item_name+item_amount+"ID IS"+item_id);
    }


    @Override
    public int getTheme() {
        return R.style.AppBottomSheetDialogTheme;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.edit_kwea_shop_item_bottom_sheet,container,false);
      close=view.findViewById(R.id.close);
       itemName=view.findViewById(R.id.item_name);
        itemPrice=view.findViewById(R.id.item_price);
        itemDescription=view.findViewById(R.id.item_description);
        shopName=view.findViewById(R.id.shop_name);
        item_category=view.findViewById(R.id.item_category);
        item_currency=view.findViewById(R.id.item_currency);
        update=view.findViewById(R.id.btUpdate);
        setStyle(STYLE_NORMAL, R.style.AppBottomSheetDialogTheme);
        api = new RxApi().getApi();

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        itemName.setText(item_name);
        _item_name=itemName.getText().toString();
        Log.e("ertyrewrtyrewrtre",_item_name);
        itemPrice.setText(String.valueOf(item_amount));
        _item_price=itemPrice.getText().toString();
        itemDescription.setText(item_description);
        _item_description=itemDescription.getText().toString();
        shopName.setText(shop_name);
        close.setOnClickListener(v -> {
            dismiss();
        });
        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ;
                _item_name=itemName.getText().toString();
                Log.e("ertyrewrtyrewrtre",_item_name);
                _item_price=itemPrice.getText().toString();
                _item_description=itemDescription.getText().toString();
                editShopItem(item_id);
            }
        });

        item_category.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

               selected_item_category= adapterView.getItemAtPosition(i).toString();

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        item_currency.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

               selected_item_currency= adapterView.getItemAtPosition(i).toString();

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void editShopItem(String item_id) {
        this.progressDialog = ProgressDialog.show(getContext(), null, getString(R.string.progress_login));
        PatchKweaItem patchKweaItem = new PatchKweaItem(_item_name,selected_item_category,_item_price,selected_item_currency,_item_description);
        Call<GroupMeetingsResponse> call=api.editShopitem(item_id,patchKweaItem);
        call.enqueue(new Callback<GroupMeetingsResponse>() {
            @Override
            public void onResponse(Call<GroupMeetingsResponse> call, Response<GroupMeetingsResponse> response) {
               Log.e("Success", String.valueOf(response.body().getSuccess()));
               dismiss();
                showSweetAlertDialogSuccess("Updated Successfully!.You can Swipe to Refresh changes", getContext());
                   progressDialog.dismiss();

            }

            @Override
            public void onFailure(Call<GroupMeetingsResponse> call, Throwable t) {

                Log.e("Failure",t.getLocalizedMessage());


            }
        });
    }
}
