package com.app.ecobba.Fragments.Group;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.app.ecobba.Activities.MainActivity;
import com.app.ecobba.R;
import com.app.ecobba.common.SharedMethods;
import com.google.android.material.textfield.TextInputEditText;
import com.jaredrummler.materialspinner.MaterialSpinner;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.app.ecobba.common.ApisKtKt.createGroup;
import static com.app.ecobba.common.ApisKtKt.getGroupRegistrationData;
import static com.app.ecobba.common.ApisKtKt.getLevelThreeData;
import static com.app.ecobba.common.ApisKtKt.getLevelTwoData;
import static com.app.ecobba.common.ApisKtKt.getProfile;
import static com.app.ecobba.common.AppConstantsKt.INITIATOR_CREATE_GROUP;
import static com.app.ecobba.common.AppConstantsKt.INITIATOR_GETPROFILE;
import static com.app.ecobba.common.AppConstantsKt.INITIATOR_GET_GROUP_REGISTRATION_DATA;
import static com.app.ecobba.common.AppConstantsKt.INITIATOR_GET_LEVEL_THREE_DATA;
import static com.app.ecobba.common.AppConstantsKt.INITIATOR_GET_LEVEL_TWO_DATA;
import static com.app.ecobba.common.SharedMethods.GroupDigits;
import static com.app.ecobba.common.SharedMethods.HTTP_GET;
import static com.app.ecobba.common.SharedMethods.HTTP_POST;
import static com.app.ecobba.common.SharedMethods.getFromServer;
import static com.app.ecobba.common.SharedMethods.global_error_message;
import static com.app.ecobba.common.SharedMethods.postToServer;
import static com.app.ecobba.common.SharedMethods.runOnUI;
import static com.app.ecobba.common.SharedMethods.showAlertAndMoveToPage;
import static com.app.ecobba.common.SharedMethods.showSweetAlertDialogError;

public class GroupCreateFragment extends Fragment
        implements SharedMethods.NetworkObjectListener,
        MaterialSpinner.OnItemSelectedListener,
        View.OnClickListener,
        CompoundButton.OnCheckedChangeListener {

    @BindView(R.id.create) Button create;
    @BindView(R.id.bankdetails)    LinearLayout bankdetails;
    @BindView(R.id.progressBar)    ProgressBar progressBar;
    @BindView(R.id.cbNewGroup) CheckBox cbNewGroup;
    @BindView(R.id.llCarryOver) LinearLayout llCarryOver;
    @BindView(R.id.tiBalance) TextInputEditText tiBalance;
    @BindView(R.id.tiHisa) TextInputEditText tiHisa;
    @BindView(R.id.tiJamii) TextInputEditText tiJamii;
    @BindView(R.id.tiFines) TextInputEditText tiFines;


    private MaterialSpinner association,bank,mkoa,wilaya,kata;
    private ColorDrawable popupColor;
    private TextInputEditText mtaa;
    private EditText name,branch,bankname,accountNumber,comments;
    private int selectedBankValue = 0;
    public String selectedAssociationValue ="0",selectedMkoaValue ="0",selectedWilayaValue="0",selectedKataValue="0",
            _level_two,_level_three,_level_four;
    private Context ctx;
    private List<String> association_ids,level_ones_ids,level_two_ids,level_three_ids,level_four_ids;
    private String token;
    String bank_name,bank_branch,account_no;

    private String _name,_kata,_bank_name,_bank_branch,_comments,_account_no;

    private String balance,hisa,jamii,fines,new_group="true";

    TextView textView_county,textView_subcounty,textView_location;
    LinearLayout linearlayoulocation;
    String association_value="";

    public GroupCreateFragment(){

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View frag = inflater.inflate(R.layout.fragment_create_group,container,false);
        popupColor = new ColorDrawable(getResources().getColor(R.color.colorSurface));
        return frag;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        ctx=getActivity();
        ButterKnife.bind(this, view);
        token= SharedMethods.getDefaults("token",ctx);
        getGroupRegistrationData(token);
        fetchUsersData(token);

        association = view.findViewById(R.id.association);
        association.setItems(getResources().getStringArray(R.array.associations_list));
        association.setOnItemSelectedListener(this);
//        association.getPopupWindow().setBackgroundDrawable(popupColor);
//        association.setItems(getResources().getStringArray(R.array.association));

        bank = view.findViewById(R.id.bankaccount);
        linearlayoulocation = view.findViewById(R.id.linearlayoulocation);
        bank.getPopupWindow().setBackgroundDrawable(popupColor);
        bank.setItems(getResources().getStringArray(R.array.bank_account));
        bank.setOnItemSelectedListener(this);

        mkoa = view.findViewById(R.id.mkoa);
        textView_county=view.findViewById(R.id.textView_county);
        textView_subcounty=view.findViewById(R.id.textView_subcounty);
        textView_location=view.findViewById(R.id.textView_location);
//        mkoa.getPopupWindow().setBackgroundDrawable(popupColor);
//        mkoa.setItems(getResources().getStringArray(R.array.mkoa));
        mkoa.setOnItemSelectedListener(this);

        wilaya = view.findViewById(R.id.wilaya);
//        wilaya.getPopupWindow().setBackgroundDrawable(popupColor);
//        wilaya.setItems(getResources().getStringArray(R.array.wilaya));
        wilaya.setOnItemSelectedListener(this);

        kata = view.findViewById(R.id.kata);
//        kata.getPopupWindow().setBackgroundDrawable(popupColor);
//        kata.setItems(getResources().getStringArray(R.array.kata));
        kata.setOnItemSelectedListener(this);

        name = view.findViewById(R.id.name);

        //mtaa = view.findViewById(R.id.mtaa);

        comments = view.findViewById(R.id.comment);
        bankname = view.findViewById(R.id.bankname);
        branch = view.findViewById(R.id.branch);
        accountNumber = view.findViewById(R.id.accountNumber);
        cbNewGroup.setOnCheckedChangeListener(this);
        create.setOnClickListener(this);

    }


    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.create:
                if(validateFormFields()){
                    createGroupToAPI();
                }
                break;
        }
    }

    private boolean validateFormFields(){
        boolean valid = true;

        _name = name.getText().toString();
       _kata = kata.getText().toString();
        _bank_name = bankname.getText().toString();
        _bank_branch =  branch.getText().toString();
        _comments = comments.getText().toString();
        _account_no = accountNumber.getText().toString();
        balance = tiBalance.getText().toString();
        hisa = tiHisa.getText().toString();
        jamii = tiJamii.getText().toString();
        fines = tiFines.getText().toString();
        if (_name.isEmpty()) {
            name.setError(getString(R.string.emptyField));
            valid = false;
        }

//        if (_kata.isEmpty()) {
//            kata.setError(getString(R.string.emptyField));
//            valid = false;
//        }

        if (_comments.isEmpty()) {
            comments.setError(getString(R.string.emptyField));
            valid = false;
        }


        if(selectedBankValue == 1){ // if there is branches
            if (_account_no.isEmpty()) {
                accountNumber.setError(getString(R.string.emptyField));
                valid = false;
            }

            if (_bank_name.isEmpty()) {
                bankname.setError(getString(R.string.emptyField));
                valid = false;
            }

            if (_bank_branch.isEmpty()) {
                branch.setError(getString(R.string.emptyField));
                valid = false;
            }

        }


        if(   selectedMkoaValue.equals("0")
                || selectedWilayaValue.equals("0")
                 ){
            valid = true;
            Toast.makeText(ctx,getResources().getString(R.string.fill_in_all_fields),Toast.LENGTH_LONG).show();
        }

//

//        if(    selectedAssociationValue.equals("0") || selectedMkoaValue.equals("0")
//                || selectedWilayaValue.equals("0") //|| selectedKataValue.equals("0")
//         ){
//            valid = false;
//            Toast.makeText(ctx,getResources().getString(R.string.fill_in_all_fields),Toast.LENGTH_LONG).show();
//        }

        if (!cbNewGroup.isChecked()){
            if (balance.isEmpty()){
                tiBalance.setError(getString(R.string.emptyField));
            }
            if (hisa.isEmpty()){
                tiHisa.setError(getString(R.string.emptyField));
            }
            if (jamii.isEmpty()){
                tiJamii.setError(getString(R.string.emptyField));
            }
            if (fines.isEmpty()){
                tiFines.setError(getResources().getString(R.string.emptyField));
            }
        }


        return valid;
    }


    private void createGroupToAPI(){

        if(   selectedAssociationValue.equals("0")){
            //String associationValue=association_value;
            Log.e("association_Value_is", "VALUE IS" +association_value);
            //Toast.makeText(ctx,getResources().getString(R.string.fill_in_all_fields),Toast.LENGTH_LONG).show();
        }
        else {
           association_value=selectedAssociationValue;
            Log.e("association_Value_DATA", selectedAssociationValue);
        }
        progressBar.setVisibility(View.VISIBLE);

        String params = "{ \"name\":" + '"' + _name + '"' +
                ", \"association\": " + '"' + association_value + '"' +
//                ", \"group_certificate\": " + '"' + _email + '"' +
                ", \"bank\": " + '"' + selectedBankValue + '"' +
                ", \"bank_name\": " + '"' + _bank_name + '"' +
                ", \"bank_branch\": " + '"' + _bank_branch + '"' +
                ", \"account_no\": " + '"' + _account_no + '"' +
                ", \"level_one\": " + '"' + selectedMkoaValue + '"' +
                ", \"level_two\": " + '"' + selectedWilayaValue + '"' +
                ", \"level_three\": " + '"' + selectedKataValue + '"' +
                ", \"comment\": " + '"' + _comments + '"' +
                ", \"new_group\": " + '"' + new_group + '"' +
                ", \"balance\": " + '"' + balance + '"' +
                ", \"hisa\": " + '"' + hisa + '"' +
                ", \"jamii\": " + '"' + jamii + '"' +
                ", \"fines\": " + '"' + fines + '"' +
                "}\n";
        Log.e("POST_CREATE_GROUP", params);

       postToServer( createGroup ,token, params , INITIATOR_CREATE_GROUP , HTTP_POST , ctx ,this);
    }
    private void fetchUsersData(String token) {
        Map<String, String> params = new HashMap<>();
        params.put("token", token);
        getFromServer(getProfile, token, params, INITIATOR_GETPROFILE, HTTP_GET, ctx, this, true);
    }

    private void getWilayaData(String mkoa_id){
        Map<String, String> params = new HashMap<String, String>();
        params.put("token", token);
        getFromServer( getLevelTwoData+"/"+mkoa_id ,token, params , INITIATOR_GET_LEVEL_TWO_DATA , HTTP_GET , ctx ,this);
    }

    private void getKataData(String kata_id){
        Map<String, String> params = new HashMap<String, String>();
        params.put("token", token);
        getFromServer( getLevelThreeData+"/"+kata_id ,token, params , INITIATOR_GET_LEVEL_THREE_DATA , HTTP_GET , ctx ,this);
    }
    public void getGroupRegistrationData(String token){
        Map<String, String> params = new HashMap<String, String>();
        params.put("token", token);
        getFromServer( getGroupRegistrationData ,token, params , INITIATOR_GET_GROUP_REGISTRATION_DATA , HTTP_GET , ctx ,this);
    }


    @Override
    public void onDataReady(String initiator, String response) {
        switch (initiator) {
            case INITIATOR_GETPROFILE:
                try {
                    runOnUI(new Runnable() {
                        @Override
                        public void run() {
                            Log.e("PROFILE RESPONSE", "Response is " + response);
//                                    hideDialog(ctx);
                            JSONObject jObj = null;
                            try {
                                jObj = new JSONObject(response);
                                if (jObj.has("errors")) {
                                    String errors = jObj.getString("errors");
                                    showSweetAlertDialogError(errors, ctx);
                                } else { // if no error message was found in the response,
                                    boolean success = jObj.getBoolean("success");
                                    if (success) { // if the request was successfuls, got forward, else show error message
                                        JSONObject jObjUser = jObj.getJSONObject("user");
                                        String name = jObjUser.getString("name");
                                        String other_names = jObjUser.getString("other_names");
                                        if (jObjUser.has("detail") && !jObjUser.isNull("detail")) {

                                            JSONObject currencyObject = jObjUser.getJSONObject( "detail");
                                            Log.e("country_name_code", currencyObject.getString("country"));
                                           String currencyValue=currencyObject.getString("country");
                                           Log.e("country_code",currencyValue);

                                           if (currencyValue .equals("131") ){
                                               linearlayoulocation.setVisibility(View.GONE);
                                           }else {
                                               linearlayoulocation.setVisibility(View.VISIBLE);
                                           }

                                        }

                                        String created_at = jObjUser.getString("created_at");
                                        JSONObject jObjWallet = jObjUser.getJSONObject("wallet");
                                        String balance = jObjWallet.getString("balance");
                                        String MyCurrency = jObjWallet.getString("currency");
                                        Log.e("my_vurrency_is", MyCurrency);
                                        JSONObject currencyObject = jObjWallet.getJSONObject( "currency");
                                        Log.e("currency_name", currencyObject.getString("name") + " " + currencyObject.getString("prefix"));

//                                        String email_address = jObjUser.getString("email");
//                                        String account_number = jObjUser.getString("account_no");
//                                        int gender_api = jObjUserDetails.getInt("gender");
//                                        String income_class = jObjUserDetails.getString("income");
//                                        String occupation = jObjUserDetails.getString("occupation");
//                                        int residence_api = jObjUserDetails.getInt("residence");
                                        JSONObject jObjUserTransactionsCount = jObjUser.getJSONObject("transactions");
                                        String credits = jObjUserTransactionsCount.getString("credit");
                                        String debits = jObjUserTransactionsCount.getString("debit");

                                    } else {
                                        String api_error_message = jObj.getString("message");
                                        showSweetAlertDialogError(api_error_message, ctx);
                                    }
                                }

                            } catch (JSONException e) {
                                Log.e("ExceptionLOGIN-DATA", e.toString());
                                showSweetAlertDialogError(global_error_message, ctx);
                                e.printStackTrace();
                            }
                        }
                    });
                } catch (Exception e) {
//                            hideDialog(ctx);
                    Log.e("Exception- HOME", e.toString());
                    showSweetAlertDialogError(global_error_message, ctx);
//                    Toast.makeText(ctx, "Login Data Response Exception" + e.toString(), Toast.LENGTH_LONG).show();
                }
                break;

            case INITIATOR_GET_GROUP_REGISTRATION_DATA:
                try {
                    runOnUI(new Runnable() {
                        @Override
                        public void run() {
                            Log.e("REG_DATA_RES", "Response is " + response);
//                            hideDialog(ctx);

                            JSONObject jObj = null;
                            try {
                                jObj = new JSONObject(response);
                                if (jObj.has("errors")) {
                                    String errors = jObj.getString("errors");
                                    showSweetAlertDialogError(errors, ctx);
                                } else { // if no error message was found in the response,
                                    boolean success = jObj.getBoolean("success");
                                    if (success) { // if the request was successfuls, got forward, else show error message
                                        JSONObject data = jObj.getJSONObject("data");
                                        //  loop over the tranactions and display in the recyclerview
                                        JSONArray associations = data.getJSONArray("associations");
                                        JSONObject region_detail = data.getJSONObject("region_detail");
                                        String _level_one = region_detail.getString("level_one");
                                        _level_two = region_detail.getString("level_two");
                                         _level_three = region_detail.getString("level_three");
                                        Log.e("_level_three_is",_level_three);
                                        String _level_four = region_detail.getString("level_four");
                                        JSONArray regions_arr = data.getJSONArray("regions_arr");
                                        Log.e("UseRegions", String.valueOf(regions_arr));

                                        association_ids  = new ArrayList<>();
                                        ArrayList<String> associations_drop_down = new ArrayList<String>();
                                        associations_drop_down.add(getResources().getString(R.string.please_select_associations) +" "+ associations);
                                        association_ids.add("0");
                                        if (associations.length() > 0) {
                                            for (int i = 0; i < associations.length(); i++) {
                                                JSONObject jsonObject = associations.getJSONObject(i);
                                                String id = jsonObject.getString("id");
                                                String admin_id = jsonObject.getString("admin_id");
                                              String name = jsonObject.getString("name");
                                                associations_drop_down.add(name);
                                                association_ids.add(id);
                                            }

                                            association.setItems(associations_drop_down);
                                            association.setSelectedIndex(0);
                                        }
                                        level_ones_ids  = new ArrayList<>();
                                        JSONArray level_ones = data.getJSONArray("level_ones");
                                        Log.e("Level_One_is", String.valueOf(level_ones));
                                        ArrayList<String> level_ones_drop_down = new ArrayList<String>();
                                        level_ones_drop_down.add(getResources().getString(R.string.please_select)+" "+_level_one);
//                                        level_ones_drop_down.add(_level_one);
                                        level_ones_ids.add("0");
                                        if (level_ones.length() > 0) {
                                            for (int i = 0; i < level_ones.length(); i++) {
                                                JSONObject jsonObject = level_ones.getJSONObject(i);
                                                String id = jsonObject.getString("id");
                                                String name = jsonObject.getString("name");
                                                level_ones_drop_down.add(name);
                                                level_ones_ids.add(id);
                                                Log.e("State_Name_is_this",_level_one);
                                                textView_county.setText(_level_one);
                                            }

                                            mkoa.setItems(level_ones_drop_down);
                                            mkoa.setSelectedIndex(0);

                                        }



                                    } else {
                                        String api_error_message = jObj.getString("message");
                                        showSweetAlertDialogError(api_error_message, ctx);
                                    }
                                }

                            } catch (JSONException e) {
                                Log.e("ExceptionLOGIN-DATA", e.toString());
                                showSweetAlertDialogError(global_error_message, ctx);
                                e.printStackTrace();
                            }
                        }
                    });
                } catch (Exception e) {
//                    hideDialog(ctx);
                    Log.e("Exception- HOME", e.toString());
                    showSweetAlertDialogError(global_error_message, ctx);
//                    Toast.makeText(ctx, "Login Data Response Exception" + e.toString(), Toast.LENGTH_LONG).show();
                }
                break;
            case INITIATOR_CREATE_GROUP:
                try {
                    runOnUI(new Runnable() {
                        @Override
                        public void run() {
                            Log.e("CREATE_GROUP_MEMBER", "Response is " + response);
                            progressBar.setVisibility(View.GONE);
                            JSONObject jObj = null;
                            try {
                                jObj = new JSONObject(response);
                                if (jObj.has("errors")) {
                                    String errors = jObj.getString("errors");
                                    showSweetAlertDialogError(errors, ctx);
                                }
                                else { // if no error message was found in the response,
                                    boolean success = jObj.getBoolean("success");
                                    if (success)
                                    {
                                        showAlertAndMoveToPage("Success, Group has been created", MainActivity.class,ctx);
                                    }
                                    else {
                                        String api_error_message = jObj.getString("message");
                                        showSweetAlertDialogError(api_error_message, ctx);
                                    }
                                }

                            } catch (JSONException e) {
                                Log.e("Exception-DATA", e.toString());

                                showSweetAlertDialogError(global_error_message, ctx);
                                e.printStackTrace();
                            }
                        }
                    });
                } catch (Exception e) {
                    Log.e("Exception_+GROUP_MEMBER", e.toString());
                    showSweetAlertDialogError(global_error_message, ctx);
//                    Toast.makeText(ctx, "Login Data Response Exception" + e.toString(), Toast.LENGTH_LONG).show();
                }
                break;
            case INITIATOR_GET_LEVEL_TWO_DATA:
                try {
                    runOnUI(new Runnable() {
                        @Override
                        public void run() {
                            Log.e("CREATE_GROUP_MEMBER", "Response is " + response);
                            JSONObject jObj = null;
                            try {
                                jObj = new JSONObject(response);
                                if (jObj.has("errors")) {
                                    String errors = jObj.getString("errors");
                                    showSweetAlertDialogError(errors, ctx);
                                } else { // if no error message was found in the response,
                                    boolean success = jObj.getBoolean("success");
                                    if (success) {
                                        level_two_ids  = new ArrayList<>();
//                                        JSONObject level_two = jObj.getJSONObject("data");
                                        JSONArray level_two = jObj.getJSONArray("data");
                                        ArrayList<String> level_two_drop_down = new ArrayList<String>();
                                        level_two_drop_down.add(getResources().getString(R.string.please_select)+" "+_level_two);
                                        level_two_ids.add("0");
                                        if (level_two.length() > 0) {
                                            for (int i = 0; i < level_two.length(); i++) {
                                                Log.e("FINAL DATA IS : ", level_two.toString());
                                                JSONObject jsonObject = level_two.getJSONObject(i);
                                                String id = jsonObject.getString("id");
                                                String name = jsonObject.getString("name");
                                                level_two_drop_down.add(name);
                                                level_two_ids.add(id);
                                                Log.e("level_two_name_is",_level_two);
                                                textView_subcounty.setText(_level_two);
                                            }

                                            wilaya.setItems(level_two_drop_down);
                                            wilaya.setSelectedIndex(0);
                                        }
                                    } else {
                                        String api_error_message = jObj.getString("message");
                                        showSweetAlertDialogError(api_error_message, ctx);
                                    }
                                }

                            } catch (JSONException e) {
                                Log.e("Exception-DATA", e.toString());
                                showSweetAlertDialogError(global_error_message, ctx);
                                e.printStackTrace();
                            }
                        }
                    });
                } catch (Exception e) {
                    Log.e("Exception_+GROUP_MEMBER", e.toString());
                    showSweetAlertDialogError(global_error_message, ctx);
                }
                break;
            case INITIATOR_GET_LEVEL_THREE_DATA:
                try {
                    runOnUI(new Runnable() {
                        @Override
                        public void run() {
                            Log.e("CREATE_GROUP_MEMBER", "Response is " + response);
//                            hideDialog(ctx);
                            JSONObject jObj = null;
                            try {
                                jObj = new JSONObject(response);
                                if (jObj.has("errors")) {
                                    String errors = jObj.getString("errors");
                                    showSweetAlertDialogError(errors, ctx);
                                } else { // if no error message was found in the response,
                                    boolean success = jObj.getBoolean("success");
                                    if (success) {
                                        level_three_ids  = new ArrayList<>();
//                                        JSONObject level_two = jObj.getJSONObject("data");
                                        JSONArray level_three = jObj.getJSONArray("data");
                                        ArrayList<String> level_three_drop_down = new ArrayList<String>();
                                        level_three_drop_down.add(getResources().getString(R.string.please_select)+" "+_level_three);
                                        level_three_ids.add("0");
                                        if (level_three.length() > 0) {
                                            for (int i = 0; i < level_three.length(); i++) {
                                                Log.e("FINAL DATA IS : ", level_three.toString());
                                                JSONObject jsonObject = level_three.getJSONObject(i);
                                                String id = jsonObject.getString("id");
                                                String name = jsonObject.getString("name");
                                                level_three_drop_down.add(name);
                                                level_three_ids.add(id);
                                                Log.e("level_three_is",_level_three);
                                                textView_location.setText(_level_three);
                                            }
                                            kata.setItems(level_three_drop_down);
                                            kata.setSelectedIndex(0);
                                        }
                                    } else {
                                        String api_error_message = jObj.getString("message");
                                        showSweetAlertDialogError(api_error_message, ctx);
                                    }
                                }

                            } catch (JSONException e) {
                                Log.e("Exception-DATA", e.toString());
                                showSweetAlertDialogError(global_error_message, ctx);
                                e.printStackTrace();
                            }
                        }
                    });
                } catch (Exception e) {
//                    hideDialog(ctx);
                    Log.e("Exception_+GROUP_MEMBER", e.toString());
                    showSweetAlertDialogError(global_error_message, ctx);
//                    Toast.makeText(ctx, "Login Data Response Exception" + e.toString(), Toast.LENGTH_LONG).show();
                }
                break;

        }
    }

    @Override
    public void onDataLoaded(String initiator, JSONArray data) {

    }

    @Override
    public void onNetworkError(String initiator, String errorCode, String errorMessage) {
        switch (initiator) {
            case INITIATOR_GET_GROUP_REGISTRATION_DATA:
                try{
                    runOnUI(new Runnable() {
                        @Override
                        public void run() {
//                                    hideDialog(ctx);
                            showSweetAlertDialogError(errorMessage, ctx);
                        }
                    });
                }catch (Exception e){
//                            hideDialog(ctx);
                    showSweetAlertDialogError(global_error_message, ctx);
                    Log.e("Exception- HOME", e.toString());
//                    Toast.makeText(ctx, "Login Error Response Exception" + e.toString(), Toast.LENGTH_LONG).show();
                }
                break;
            case INITIATOR_GET_LEVEL_TWO_DATA:
                try{
                    runOnUI(new Runnable() {
                        @Override
                        public void run() {
                            showSweetAlertDialogError(errorMessage, ctx);
                        }
                    });
                }catch (Exception e){
//                            hideDialog(ctx);
                    showSweetAlertDialogError(global_error_message, ctx);
                    Log.e("Exception- HOME", e.toString());
//                    Toast.makeText(ctx, "Login Error Response Exception" + e.toString(), Toast.LENGTH_LONG).show();
                }
                break;
            case INITIATOR_GET_LEVEL_THREE_DATA:
                try{
                    runOnUI(new Runnable() {
                        @Override
                        public void run() {
                            showSweetAlertDialogError(errorMessage, ctx);
                        }
                    });
                }catch (Exception e){
//                            hideDialog(ctx);
                    showSweetAlertDialogError(global_error_message, ctx);
                    Log.e("Exception- HOME", e.toString());
//                    Toast.makeText(ctx, "Login Error Response Exception" + e.toString(), Toast.LENGTH_LONG).show();
                }
                break;
            case INITIATOR_CREATE_GROUP:
                try{
                    runOnUI(new Runnable() {
                        @Override
                        public void run() {
                            progressBar.setVisibility(View.GONE);
                            showSweetAlertDialogError(errorMessage, ctx);
                        }
                    });
                }catch (Exception e){
                    progressBar.setVisibility(View.GONE);
                    showSweetAlertDialogError(global_error_message, ctx);
                    Log.e("Exception- HOME", e.toString());
//                    Toast.makeText(ctx, "Login Error Response Exception" + e.toString(), Toast.LENGTH_LONG).show();
                }
                break;

        }
    }

    @Override
    public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
        switch(view.getId()) {
            case R.id.bankaccount:
                selectedBankValue  = bank.getSelectedIndex();
                if(selectedBankValue == 1){
                    bankdetails.setVisibility(View.VISIBLE);
                    Log.d("Bank Status id:",""+ "ITS EQUAL TO 2");
                }else{
                    bankdetails.setVisibility(View.GONE);
                }
                Log.d("Bank Status id:",""+ bank.getSelectedIndex());

                break;
            case R.id.association:
//                selectedAssociationValue = association.getSelectedIndex();
                selectedAssociationValue= association_ids.get(position);
                Log.d("ASSOCIATION Status id:",""+ association.getSelectedIndex());
//                gender_id_toapi= gender_ids.get(position);
//                Log.d("Gender id is : ",""+ gender_id_toapi);
                break;
            case R.id.mkoa:
//                selectedMkoaValue  = mkoa.getSelectedIndex();
                selectedMkoaValue= level_ones_ids.get(position);
                getWilayaData(level_ones_ids.get(position));
                Log.d("MKOA Status id:",""+level_ones_ids.get(position) );
                break;
            case R.id.wilaya:
//                selectedWilayaValue  = wilaya.getSelectedIndex();
                selectedWilayaValue= level_two_ids.get(position);
                getKataData(level_two_ids.get(position));
                Log.d("WILAYA Status id:",""+ level_two_ids.get(position) );
                break;
            case R.id.kata:
              //  selectedKataValue  = kata.getSelectedIndex();
                selectedKataValue= level_three_ids.get(position);
                Log.d("KATA Status id:",""+ level_three_ids.get(position) );
                break;
        }
    }


    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
        switch (compoundButton.getId()){
            case R.id.cbNewGroup:
                if (b){
                    llCarryOver.setVisibility(View.GONE);
                    new_group = "true";
                }else {
                    llCarryOver.setVisibility(View.VISIBLE);
                    new_group = "false";
                }
                break;
        }
    }
}
