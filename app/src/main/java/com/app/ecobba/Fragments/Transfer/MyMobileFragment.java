package com.app.ecobba.Fragments.Transfer;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.app.ecobba.R;

import butterknife.ButterKnife;

public class MyMobileFragment extends Fragment {

    Context ctx;

    public MyMobileFragment(Context ctx){this.ctx = ctx;}

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View frag = inflater.inflate(R.layout.fragment_money_mobile,container,false);
        ButterKnife.bind(this,frag);
        return frag;
    }

}
