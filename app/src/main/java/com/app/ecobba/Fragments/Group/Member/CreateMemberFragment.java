package com.app.ecobba.Fragments.Group.Member;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;

import com.app.ecobba.Activities.EasyRegistration;
import com.app.ecobba.Activities.MainActivity;
import com.app.ecobba.Models.AddMember.AddGroupMemberResponse;
import com.app.ecobba.Models.RegisterData.Classe;
import com.app.ecobba.Models.RegisterData.Gender;
import com.app.ecobba.Models.RegisterData.GetRegistrationDataResponse;
import com.app.ecobba.Models.UserSummary.UserSummaryResponse;
import com.app.ecobba.R;
import com.app.ecobba.common.SharedMethods;
import com.app.ecobba.common.rxApi.Api;
import com.app.ecobba.common.rxApi.RxApi;
import com.google.android.material.textfield.TextInputEditText;
import com.jaredrummler.materialspinner.MaterialSpinner;
import com.rilixtech.widget.countrycodepicker.Country;
import com.rilixtech.widget.countrycodepicker.CountryCodePicker;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.app.ecobba.common.ApisKtKt.addGroupMember;
import static com.app.ecobba.common.ApisKtKt.getRegisterData;
import static com.app.ecobba.common.ApisKtKt.makeContribution;
import static com.app.ecobba.common.AppConstantsKt.INITIATOR_CREATE_GROUP_MEMBER;
import static com.app.ecobba.common.AppConstantsKt.INITIATOR_GETERGISTER_DATA;
import static com.app.ecobba.common.AppConstantsKt.INITIATOR_MAKE_CONTRIBUTION;
import static com.app.ecobba.common.SharedMethods.HTTP_GET;
import static com.app.ecobba.common.SharedMethods.HTTP_POST;
import static com.app.ecobba.common.SharedMethods.getFromServer;
import static com.app.ecobba.common.SharedMethods.global_error_message;
import static com.app.ecobba.common.SharedMethods.hideDialog;
import static com.app.ecobba.common.SharedMethods.postToServer;
import static com.app.ecobba.common.SharedMethods.runOnUI;
import static com.app.ecobba.common.SharedMethods.showAlertAndMoveToPage;
import static com.app.ecobba.common.SharedMethods.showSweetAlertDialogError;
import static com.app.ecobba.common.SharedMethods.showSweetAlertDialogSuccess;

public class CreateMemberFragment extends Fragment implements
        MaterialSpinner.OnItemSelectedListener, SharedMethods.NetworkObjectListener,

        View.OnClickListener,
        CompoundButton.OnCheckedChangeListener {

    EditText firstName, otherName, phoneNumber, occupation, email;
    MaterialSpinner gender, income;
    ColorDrawable popupColor;

    String annual_income_id_toapi = "", gender_id_toapi = "0";
    List<String> annual_income_ids, gender_ids;
    String group_id;
    Context ctx;
    int role_id = 7;


    @BindView(R.id.create)
    Button create;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.cbExistingMember)
    CheckBox cbExistingMember;
    @BindView(R.id.llCarryOver)
    LinearLayout llCarryOver;
    @BindView(R.id.tiBalance)
    TextInputEditText tiBalance;
    @BindView(R.id.tiHisa)
    TextInputEditText tiHisa;
    @BindView(R.id.tiJamii)
    TextInputEditText tiJamii;
    @BindView(R.id.tiFines)
    TextInputEditText tiFines;

    String name, other_names, _email, msisdn, _occupation;
    String balance, hisa, jamii, fines;
    CountryCodePicker ccp;
    String code, token;
    public static EditText dateOfBirth;
    public static String dob = "0";
    Api api;

    public CreateMemberFragment() {
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View frag = inflater.inflate(R.layout.fragment_addmember, container, false);
        popupColor = new ColorDrawable(getResources().getColor(R.color.colorSurface));
        api = new RxApi().getApi();
        return frag;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        ctx = getActivity();
        assert getArguments() != null;
        group_id = getArguments().getString("group_id");
        token = SharedMethods.getDefaults("token", ctx);
        getRegisterData();

        firstName = view.findViewById(R.id.firstname);
        otherName = view.findViewById(R.id.othername);
        phoneNumber = view.findViewById(R.id.phoneNumber);
        dateOfBirth = view.findViewById(R.id.etDateOfBirth);
        occupation = view.findViewById(R.id.occupation);
        email = view.findViewById(R.id.etEmail);
        gender = view.findViewById(R.id.msGender);

        gender.getPopupWindow().setBackgroundDrawable(popupColor);

        income = view.findViewById(R.id.income);
        income.getPopupWindow().setBackgroundDrawable(popupColor);
        dateOfBirth = view.findViewById(R.id.etDateOfBirth);
        dateOfBirth.setOnClickListener(this);

        income.setOnItemSelectedListener(this);
        gender.setOnItemSelectedListener(this);
        create.setOnClickListener(this);

        cbExistingMember.setOnCheckedChangeListener(this);


        ccp = view.findViewById(R.id.ccp);


        ccp.setOnCountryChangeListener(new CountryCodePicker.OnCountryChangeListener() {
            @Override
            public void onCountrySelected(Country selectedCountry) {
                Toast.makeText(ctx, "Updated country to " + ccp.getSelectedCountryName(), Toast.LENGTH_SHORT).show();
                code = ccp.getSelectedCountryCode();

                Log.e("Code_country", code);
            }
        });


    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.etDateOfBirth:
                DialogFragment dialogfragment = new DatePickerDialogTheme4();
                dialogfragment.show(getParentFragmentManager(), "Theme 4");
                break;
            case R.id.create:
                if (validateFormFields()) {
                    createMemberToAPI();
                }
                break;
        }
    }

    @Override
    public void onDataReady(String initiator, String response) {
        switch (initiator) {
            case INITIATOR_CREATE_GROUP_MEMBER:
                try {
                    runOnUI(new Runnable() {
                        @Override
                        public void run() {
                            Log.e("CREATE_GROUP_MEMBER", "Response is " + response);
                            progressBar.setVisibility(View.GONE);
                            JSONObject jObj = null;
                            try {
                                jObj = new JSONObject(response);
                                if (jObj.has("errors")) {
                                    String errors = jObj.getString("errors");
                                    showSweetAlertDialogError(errors, ctx);
                                } else { // if no error message was found in the response,
                                    boolean success = jObj.getBoolean("success");
                                    if (success) {
                                        String message = jObj.getString("message");
                                        showSweetAlertDialogSuccess(message,ctx);
                                        requireActivity().onBackPressed();
                                    } else {
                                        String api_error_message = jObj.getString("message");
                                        showSweetAlertDialogError(api_error_message, ctx);
                                        requireActivity().onBackPressed();
                                    }
                                }

                            } catch (JSONException e) {
                                Log.e("Exception-DATA", e.toString());
                                showSweetAlertDialogError(global_error_message, ctx);
                                e.printStackTrace();
                            }
                        }
                    });
                } catch (Exception e) {
                    progressBar.setVisibility(View.GONE);
                    Log.e("Exception_+GROUP_MEMBER", e.toString());
                    showSweetAlertDialogError(global_error_message, ctx);
//                    Toast.makeText(ctx, "Login Data Response Exception" + e.toString(), Toast.LENGTH_LONG).show();
                }
                break;
        }

    }

    @Override
    public void onDataLoaded(String initiator, JSONArray data) {

    }

    @Override
    public void onNetworkError(String initiator, String errorCode, String errorMessage) {
        switch (initiator) {
            case INITIATOR_CREATE_GROUP_MEMBER:
                try{
                    runOnUI(new Runnable() {
                        @Override
                        public void run() {
                            progressBar.setVisibility(View.GONE);
                            showSweetAlertDialogError(errorMessage, ctx);
                        }
                    });
                }catch (Exception e){
                    progressBar.setVisibility(View.GONE);
                    showSweetAlertDialogError(global_error_message, ctx);
                    Log.e("Exception- HOME", e.toString());
//                    Toast.makeText(ctx, "Login Error Response Exception" + e.toString(), Toast.LENGTH_LONG).show();
                }
                break;
        }

    }


    public static class DatePickerDialogTheme4 extends DialogFragment implements DatePickerDialog.OnDateSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final Calendar calendar = Calendar.getInstance();
            int year = calendar.get(Calendar.YEAR);
            int month = calendar.get(Calendar.MONTH);
            int day = calendar.get(Calendar.DAY_OF_MONTH);

            DatePickerDialog datepickerdialog = new DatePickerDialog(getActivity(),
                    AlertDialog.THEME_HOLO_LIGHT, this, year, month, day);

            return datepickerdialog;
        }

        public void onDateSet(DatePicker view, int year, int month, int day) {
            String string = String.valueOf(year) + "-" + String.valueOf(month + 1) + "-" + String.valueOf(day);
            dateOfBirth.setText(string);
            try {
                Date date = new SimpleDateFormat("yyyy-MM-dd").parse(string);
                final Calendar datePickerCal = Calendar.getInstance();
                datePickerCal.set(year, month, day);
                dob = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).format(datePickerCal.getTime());
            } catch (ParseException e) {
                e.printStackTrace();
            }

        }
    }


    private boolean validateFormFields() {
        boolean valid = true;
        name = firstName.getText().toString();
        other_names = otherName.getText().toString();
        _email = email.getText().toString();
        msisdn = phoneNumber.getText().toString();
        _occupation = occupation.getText().toString();
        dob = dateOfBirth.getText().toString();
        balance = tiBalance.getText().toString();
        hisa = tiHisa.getText().toString();
        jamii = tiJamii.getText().toString();
        fines = tiFines.getText().toString();

        if (name.isEmpty()) {
            firstName.setError(getString(R.string.emptyField));
            valid = false;
        }
        if (other_names.isEmpty()) {
            otherName.setError(getString(R.string.emptyField));
            valid = false;
        }
        if (_email.isEmpty()) {
            valid = true;
        }
        if (msisdn.isEmpty()) {
            phoneNumber.setError(getString(R.string.emptyField));
            valid = false;
        }
//        if (_occupation.isEmpty()) {
//            valid = true;
//        }
        if (dob.isEmpty()) {
            valid = true;
        }


        if (gender_id_toapi.equals("0")) {
            valid = false;
            Toast.makeText(ctx, "Please Select Gender", Toast.LENGTH_LONG).show();
        }
        if (annual_income_id_toapi.equals("0")) {
            annual_income_id_toapi.equals("");
            valid = true;
        }

        if (!cbExistingMember.isChecked()) {
            if (balance.isEmpty()) {
                tiBalance.setError(getString(R.string.emptyField));
            }
            if (hisa.isEmpty()) {
                tiHisa.setError(getString(R.string.emptyField));
            }
            if (jamii.isEmpty()) {
                tiJamii.setError(getString(R.string.emptyField));
            }
            if (fines.isEmpty()) {
                tiFines.setError(getString(R.string.emptyField));
            }
        }


        return valid;

    }


    private void createMemberToAPI() {
        progressBar.setVisibility(View.VISIBLE);
        String phone_number = code + msisdn;
//
//        Log.e("POST_PARAM", "createMemberToAPI: "+group_id+" and  name"+name +" and other name is"+other_names+" and email is "+_email+" and date od birth is "+ dob+" and gender is "+gender_id_toapi+"and role id is"+role_id);
//
//        Call<AddGroupMemberResponse> call = api.addgroupmember(group_id, name, other_names, _email, phone_number, _occupation, dob, annual_income_id_toapi, gender_id_toapi, role_id);
//
//        call.enqueue(new Callback<AddGroupMemberResponse>() {
//            @Override
//            public void onResponse(Call<AddGroupMemberResponse> call, Response<AddGroupMemberResponse> response) {
//
//                try {
//                    if (response.body().getSuccess()) {
//                        showSweetAlertDialogSuccess(response.body().getMessage(), ctx);
//                        requireActivity().onBackPressed();
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//
//            }
//
//            @Override
//            public void onFailure(Call<AddGroupMemberResponse> call, Throwable t) {
//
//            }
//        });

        String params = "{ \"name\":" + '"' + name + '"' +
                ", \"other_names\": " + '"' + other_names + '"' +
                ", \"email\": " + '"' + _email + '"' +
                ", \"msisdn\": " + '"' + phone_number + '"' +
                ", \"occupation\": " + '"' + _occupation + '"' +
                ", \"dob\": " + '"' + dob + '"' +
                ", \"income\": " + '"' + annual_income_id_toapi + '"' +
                ", \"gender\": " + '"' + gender_id_toapi + '"' +
                ", \"role\": " + '"' + role_id + '"' +
                "}\n" + "\n";
        Log.e("POST - CREATE_GROUP", params);

        //add member
        postToServer(addGroupMember + "/" + group_id, token, params, INITIATOR_CREATE_GROUP_MEMBER, HTTP_POST, ctx, this);

    }

    private void getRegisterData() {
        Call<GetRegistrationDataResponse> call = api.getUserRegisterData();
        call.enqueue(new Callback<GetRegistrationDataResponse>() {
            @Override
            public void onResponse(Call<GetRegistrationDataResponse> call, Response<GetRegistrationDataResponse> response) {
                try {
                    if (response.body().getSuccess()) {
                        annual_income_ids = new ArrayList<>();
                        gender_ids = new ArrayList<>();
                        if (response.body().getMessage().getGenders().size() > 0) {
                            List<Gender> genders = response.body().getMessage().getGenders();
                            ArrayList<String> gender_drop_down = new ArrayList<String>();
                            gender_drop_down.add(getResources().getString(R.string.please_select_gender));
                            gender_ids.add("0");

                            for (int i = 0; i < genders.size(); i++) {
                                String id = String.valueOf(genders.get(i).getId());
                                Log.e("Id_data", id);
                                String name = genders.get(i).getName();
                                gender_drop_down.add(name);
                                gender_ids.add(id);
                            }
                            gender.setItems(gender_drop_down);
                            gender.setSelectedIndex(0);

                        }
                        if (response.body().getMessage().getClasses().size() > 0) {
                            List<Classe> income_classes = response.body().getMessage().getClasses();
                            ArrayList<String> income_classes_drop_down = new ArrayList<String>();
                            income_classes_drop_down.add(getResources().getString(R.string.please_select_income_class));
                            annual_income_ids.add("0");
                            for (int i = 0; i < income_classes.size(); i++) {
                                String id = String.valueOf(income_classes.get(i).getId());
                                String name = income_classes.get(i).getName();
                                income_classes_drop_down.add(name);
                                annual_income_ids.add(id);
                            }
                            income.setItems(income_classes_drop_down);
                            income.setSelectedIndex(0);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<GetRegistrationDataResponse> call, Throwable t) {
                t.getLocalizedMessage();
            }
        });

    }


    @Override
    public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
        switch (view.getId()) {
            case R.id.income:
                annual_income_id_toapi = annual_income_ids.get(position);
                break;
            case R.id.msGender:
                gender_id_toapi = gender_ids.get(position);

                break;
        }
    }


    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
        switch (compoundButton.getId()) {
            case R.id.cbExistingMember:
                if (b) {
                    llCarryOver.setVisibility(View.GONE);
                } else {
                    llCarryOver.setVisibility(View.VISIBLE);
                }
                break;
        }
    }
}
