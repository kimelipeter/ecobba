package com.app.ecobba.Fragments.Borrowing.Borrowed;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.app.ecobba.Dialogs.DateRangePickerDialogFragment;
import com.app.ecobba.Models.UserLoans.Loan;
import com.app.ecobba.R;
import com.app.ecobba.Utils.BaseFragment;
import com.google.gson.Gson;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.app.ecobba.common.SharedMethods.GroupDigits;
import static com.app.ecobba.common.SharedMethods.storeReminder;
import static com.app.ecobba.common.SharedMethods.stringToCalenderParser;

public class LoanFragment extends BaseFragment implements View.OnClickListener {

    @BindView(R.id.tvLoanName)
    TextView tvLoanName;

    @BindView(R.id.tvNextPayment)
    TextView tvNextPayment;

    @BindView(R.id.tvAmount)
    TextView tvAmount;

    @BindView(R.id.tvInstallemt)
    TextView tvInstallment;

    @BindView(R.id.tvLoanNameDetail)
    TextView tvLoanNameDetail;

    @BindView(R.id.tvPurpose)
    TextView tvPurpose;

    @BindView(R.id.tvFrequency)
    TextView tvFrequency;

    @BindView(R.id.tvInstallments)
    TextView tvInstallments;

    @BindView(R.id.btnRemindMe)
    Button btnRemindMe;

    @BindView(R.id.btFilter)
    Button btFilter;

    com.app.ecobba.Models.UserLoans.Loan loan;

    String loanName,nextDate;
    int amount;
    String installment;

    public LoanFragment(){}

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View frag = inflater.inflate(R.layout.fragment_borrowed_loan,container,false);
        unbinder = ButterKnife.bind(this,frag);
        return frag;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Bundle arg = getArguments();
        if (arg.getString("loan") != null){
            loan = new Gson().fromJson(arg.getString("loan"), Loan.class);
        }

        loanName = loan.getLoan_title();
        nextDate=loan.getCreated_at();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        sdf.setTimeZone(TimeZone.getTimeZone("GMT"));

        try {
            Date date1 = sdf.parse(nextDate);
            sdf = new SimpleDateFormat("dd MMMM yyyy");
            String dates = sdf.format(date1);

            sdf =  new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");

            Date time = sdf.parse(nextDate);
            sdf = new SimpleDateFormat("h:mm a");

            String time1 = sdf.format(time);

            Log.e("DateAndTime", "Date " + dates + " Time " + time1);
//            holder.tvTimeDifference.setText(time1);
            tvNextPayment.setText(dates);
        } catch (ParseException e) {
            Log.e("DateAndTime", e.getLocalizedMessage());
            e.printStackTrace();
        }

        installment= loan.getDetail().getNo_of_installments();
        amount = (int) loan.getAmount();
        String purpose=loan.getLoan_title();

        tvLoanName.setText(loanName);
//        tvNextPayment.setText(nextDate);
        tvInstallment.setText(String.valueOf(installment));
        tvAmount.setText(GroupDigits(amount));
        tvLoanNameDetail.setText(loan.getLoan_title());
        tvPurpose.setText(purpose);
        tvFrequency.setText(loan.getDetail().getRepayment_plan());
        tvInstallments.setText(loan.getDetail().getNo_of_installments());

        btFilter.setOnClickListener(this);
        btnRemindMe.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch(view.getId()){
            case R.id.btnRemindMe:

                try {
                    Calendar start = stringToCalenderParser(nextDate);
                    Calendar end = start;
                    end.add(Calendar.HOUR,1);
                    storeReminder(getActivity(),
                            start,
                            end,
                            "Loan Payment for '"+ loanName+"' loan",
                            "This is to remind me to make a payment of "+installment+"/= for '" +loanName+"' loan" );
                } catch (ParseException e) {
                    e.printStackTrace();
                }

//                storeReminder(this);

                break;
            case R.id.btFilter:

                new DateRangePickerDialogFragment(new DateRangePickerDialogFragment.onFilterPressed() {
                    @Override
                    public void onFilterPressed(Boolean cancelled, Calendar START, Calendar END) {

                    }
                }).show(getChildFragmentManager(),"FILTER");

                break;
        }
    }
}
