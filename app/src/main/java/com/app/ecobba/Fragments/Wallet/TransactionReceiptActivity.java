package com.app.ecobba.Fragments.Wallet;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.ecobba.R;

public class TransactionReceiptActivity extends AppCompatActivity {
    String link;
    Toolbar toolbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transaction_receipt);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_baseline_arrow_back_24);
        toolbar.setBackgroundColor(ContextCompat.getColor(this, R.color.white));
        toolbar.setTitle("Transaction Receipt");
        toolbar.setTitleTextColor(ContextCompat.getColor(this,R.color.primaryColor));


        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        Intent intent = getIntent();
        int depositAmount = intent.getIntExtra("amount", 0); // here 0 is the default value


        //Recieve Data
        //int depositAmount = getIntent().getIntExtra("amount", 0);
       // int depositAmount = getIntent().getExtras().getInt("amount");
        int fees = getIntent().getIntExtra("fees",0);

        String sum_total = getIntent().getExtras().getString("sum_total");
         link = getIntent().getExtras().getString("link");
        String reference = getIntent().getExtras().getString("reference");
        Log.e("Receipt_data",depositAmount+" "+fees+" "+ sum_total+" "+reference);
        Log.e("link_payment",link);

        //ini views
        TextView tv_reference = findViewById(R.id.transaction_code);
        TextView tv_depositAmount = findViewById(R.id.amount);
        TextView tv_fee = findViewById(R.id.fee);
        TextView tv_total = findViewById(R.id.total);
        Button confirmPayment=findViewById(R.id.confirmPayment);
        // setting values to each view
        tv_depositAmount.setText(String.valueOf(depositAmount));
        tv_fee.setText(String.valueOf(fees));
        tv_total.setText(sum_total);
        tv_reference.setText(reference);


        confirmPayment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1= new Intent(TransactionReceiptActivity.this,ConfirmPaymentActivity.class);
                intent1.putExtra("link",link);
                startActivity(intent1);
            }
        });
    }
}