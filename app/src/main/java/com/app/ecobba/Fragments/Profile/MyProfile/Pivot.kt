package com.app.ecobba.Fragments.Profile.MyProfile

data class Pivot(
    val role_id: Int,
    val user_id: Int,
    val user_type: String
)