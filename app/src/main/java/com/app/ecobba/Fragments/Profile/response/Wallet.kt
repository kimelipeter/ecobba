package com.app.ecobba.Fragments.Profile.response

import com.google.gson.annotations.SerializedName
import lombok.Data

@Data
class Wallet {
    var balance = 0

    @SerializedName(value = "created_at", alternate = ["createdAt"])
    var createdAt: String? = null
    var id = 0
    var status = 0

    @SerializedName(value = "updated_at", alternate = ["updatedAt"])
    var updatedAt: String? = null

    @SerializedName(value = "user_id", alternate = ["userId"])
    var userId = 0
    var currency: Currency? = null
}