package com.app.ecobba.Fragments.Group.Meeting;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

import com.app.ecobba.R;
import com.app.ecobba.common.SharedMethods;
import com.google.android.material.chip.Chip;
import com.google.android.material.chip.ChipGroup;
import com.google.android.material.textfield.TextInputLayout;
import com.jaredrummler.materialspinner.MaterialSpinner;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.app.ecobba.common.ApisKtKt.postmeetingcontributions;
import static com.app.ecobba.common.AppConstantsKt.INITIATOR_PUSH_COLLECTIONS;
import static com.app.ecobba.common.SharedMethods.HTTP_POST;
import static com.app.ecobba.common.SharedMethods.global_error_message;
import static com.app.ecobba.common.SharedMethods.postToServer;
import static com.app.ecobba.common.SharedMethods.runOnUI;
import static com.app.ecobba.common.SharedMethods.showSweetAlertDialogError;
import static com.app.ecobba.common.SharedMethods.showSweetAlertDialogSuccess;

public class CollectionFragment extends Fragment implements
        SharedMethods.NetworkObjectListener ,
        View.OnClickListener,
        MaterialSpinner.OnItemSelectedListener {

    @BindView(R.id.msAttendance)
    MaterialSpinner attendance;
    @BindView(R.id.etHisa)
    EditText etHisa;
    @BindView(R.id.etContributions)
    EditText etContributions;
    @BindView(R.id.collect)
    Button collect;
    @BindView(R.id.btCancel)
    Button btCancel;
    @BindView(R.id.etReason)
    EditText etReason;
    @BindView(R.id.tilReason)
    TextInputLayout tilReason;



    @BindView(R.id.userGroup)
    ChipGroup userGroup;

    Bundle args;
    Context ctx;
    JSONArray group_fines_array;

    String token,group_id,meeting_id,reason,hisa,contributions,attention_to_api="present",member_id;
    List<String> groups_fines_list = new ArrayList<>();
    public CollectionFragment() {

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View frag = inflater.inflate(R.layout.fragment_collections, container, false);
        ButterKnife.bind(this, frag);
        ctx = getActivity();

        collect.setOnClickListener(this);
        attendance.setItems(getResources().getStringArray(R.array.attendance));
        attendance.setOnItemSelectedListener(this);
        args = getArguments();
        assert args != null;
        meeting_id = args.getString("meeting_id");
       // member_id = args.getString("member_id");
        group_id = args.getString("group_id");
        token= SharedMethods.getDefaults("token",ctx);
//        String jsonArray = args.getString("groupFinesArray");
//        try {
//            group_fines_array = new JSONArray(jsonArray);
//            if( group_fines_array.length() > 0 ) {
//                for (int i = 0; i <= group_fines_array.length(); i++) {
//                    JSONObject jsonObject = group_fines_array.getJSONObject(i);
//                    String name = jsonObject.getString("name");
//
//                    final Chip chip = new Chip(ctx);
//                    int paddingDp = (int) TypedValue.applyDimension(
//                            TypedValue.COMPLEX_UNIT_DIP, 10,
//                            getResources().getDisplayMetrics()
//                    );
//                    chip.setPadding(paddingDp, paddingDp, paddingDp, paddingDp);
//                    chip.setText(name);
//
//                    chip.setOnClickListener(view -> {
//                        // Handle the click.
//                        Log.d("CHIP IS", chip.getText() + " ID " + chip.getId());
//                    });
//                    chip.setOnCheckedChangeListener((view, isChecked) -> {
//                        // Handle the toggle.
//                        Log.d("CHIP STATUS IS", chip.getText() + " ID " + chip.getId() + " STATUS " + isChecked);
//                        if (isChecked) {
//                            groups_fines_list.add(chip.getText() + "");
//                        }
//                    });
//                    chip.setCheckable(true);
//                    userGroup.addView(chip);
//                }
//            }
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
        return frag;
    }



    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.collect:
                if(validateFormFields()){
                    pushCollectionsToAPI();
                }
                break;
        }

    }

    private boolean validateFormFields(){
        boolean valid = true;

        reason = etReason.getText().toString();
        contributions = etContributions.getText().toString();
        hisa = etHisa.getText().toString();

        if ( attention_to_api.equals("absent with reason" ) && reason.isEmpty()) {
            etReason.setError(getString(R.string.emptyField));
            valid = false;
        }

        if (contributions.isEmpty()) {
//            etContributions.setError(getString(R.string.emptyField));
//            valid = false;
            contributions = "0";
        }

        if (hisa.isEmpty()) {
//            etHisa.setError(getString(R.string.emptyField));
//            valid = false;
            hisa = "1";
        }

        return valid;
    }

    // 0725453124
    private void pushCollectionsToAPI() {
        String mem_id="187";
        String res="I will come";
        String params = "{ \"amount\":" + '"' + contributions + '"' +
                ", \"hisavalue\": " + '"' + hisa + '"' +
                ", \"group_id\": " + '"' + group_id + '"' +
                ", \"meeting\": " + '"' + meeting_id + '"' +
                ", \"memberid\": " + '"' + mem_id + '"' +
                ", \"attendance\": " + '"' + attention_to_api + '"' +
                ", \"reason\": " + '"' + res + '"' +
                ", \"finelist\": " + '"' + groups_fines_list + '"' +
                "}\n" + "\n";

        Log.e("POST_PARAMS", params);
        postToServer(postmeetingcontributions, token, params, INITIATOR_PUSH_COLLECTIONS, HTTP_POST, ctx, this,true,group_id+member_id+meeting_id);
    }

    @Override
    public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
        Log.e("SELECTED", "" + position);
        switch (position) {
            case 0:
                tilReason.setVisibility(View.GONE);
                attention_to_api="present";
                Log.e("VISIBILITY", "GONE");
                break;
            case 1:
                tilReason.setVisibility(View.GONE);
                attention_to_api="absent";
                Log.e("VISIBILITY", "GONE");
                break;
            case 2:
                tilReason.setVisibility(View.VISIBLE);
                attention_to_api="absent with reason";
                Log.e("VISIBILITY", "VISIBLE");
                break;
        }
    }

    @Override
    public void onDataReady(String initiator, final String response) {
        switch (initiator) {
            case INITIATOR_PUSH_COLLECTIONS:
                try {
                    runOnUI(() -> {
                        Log.e("GROUP_PUSH_COLLECTIONS", "Response is " + response);

                        JSONObject jObj = null;
                        try {
                            jObj = new JSONObject(response);
                            if (jObj.has("errors")) {
                                String errors = jObj.getString("errors");
                                showSweetAlertDialogError(errors, ctx);
                            } else { // if no error message was found in the response,
                                boolean success = jObj.getBoolean("success");
                                if (success) {
                                    String message = jObj.getString("message");
                                    showSweetAlertDialogSuccess(message,ctx);
                                    NavHostFragment.findNavController(this).navigateUp();
                                } else {
                                    String api_error_message = jObj.getString("message");
                                    showSweetAlertDialogError(api_error_message, ctx);
//                                    NavHostFragment.findNavController(frag).navigateUp();
                                }
                            }

                        } catch (JSONException e) {
                            Log.e("Exception-DATA", e.toString());
                            showSweetAlertDialogError(global_error_message, ctx);
                            e.printStackTrace();
                        }
                    });
                } catch (Exception e) {
//                    hideDialog(ctx);
                    Log.e("Exception- HOME", e.toString());
                    showSweetAlertDialogError(global_error_message, ctx);
//                    Toast.makeText(ctx, "Login Data Response Exception" + e.toString(), Toast.LENGTH_LONG).show();
                }
                break;
        }
    }


    @Override
    public void onDataLoaded(String initiator, JSONArray data) {

    }

    @Override
    public void onNetworkError(String initiator, String errorCode, final String errorMessage) {
        switch (initiator) {
            case INITIATOR_PUSH_COLLECTIONS:
                try{
                    runOnUI(new Runnable() {
                        @Override
                        public void run() {
//                                    hideDialog(ctx);
                            showSweetAlertDialogError(errorMessage, ctx);
                        }
                    });
                }catch (Exception e){
//                            hideDialog(ctx);
                    showSweetAlertDialogError(global_error_message, ctx);
                    Log.e("Exception- HOME", e.toString());
//                    Toast.makeText(ctx, "Login Error Response Exception" + e.toString(), Toast.LENGTH_LONG).show();
                }
                break;
        }
    }


}