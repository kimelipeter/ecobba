package com.app.ecobba.Fragments.Shop;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.app.ecobba.Fragments.Shop.Adapters.ShopDashboardAdapter;
import com.app.ecobba.R;

public class ConfirmDelete extends DialogFragment {
    private ShopDashboardAdapter adapter;
    int positions;
    public ConfirmDelete(ShopDashboardAdapter shopDashboardAdapter, int position) {
        adapter =shopDashboardAdapter;
        positions =position;
    }

    Button read_btn,delete;

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.confirm_delete, null);
        read_btn = view.findViewById(R.id.buttonCancel);
        delete = view.findViewById(R.id.buttonOk);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        read_btn.setOnClickListener(v -> {
            adapter.undodelete(positions,adapter.dataList.get(positions));
            dismiss();
        });
        delete.setOnClickListener(v -> {
            adapter.deleteItem(positions);
            dismiss();
        });
        builder.setView(view);
        return builder.create();

    }
}
