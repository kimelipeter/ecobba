package com.app.ecobba.Fragments.Profile.response

import com.app.ecobba.Models.User
import lombok.Data

@Data
class UserProfileResponse {
    var message: String? = null
    var success: Boolean? = null
    var user: User? = null
}