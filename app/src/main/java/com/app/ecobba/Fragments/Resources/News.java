package com.app.ecobba.Fragments.Resources;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.app.ecobba.Fragments.Resources.Adapters.NewsAdapter;
import com.app.ecobba.Models.Resources.News.newsResponse;
import com.app.ecobba.R;
import com.app.ecobba.common.SharedMethods;
import com.app.ecobba.common.rxApi.Api;
import com.app.ecobba.common.rxApi.RxApi;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;


import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class News extends Fragment implements View.OnClickListener {
    @BindView(R.id.create)
    FloatingActionButton create;
    Context context;
    Fragment fragment;
    String token;
    NewsAdapter adapter;
    private static final String TAG = "services_data";
    private RecyclerView rvServices;
    SwipeRefreshLayout mSwipeRefreshLayout;
    long datepower;
    private Api api;
    ProgressDialog progressDialog;
    @BindView(R.id.tvNoAvailableData)
    TextView tvNoAvailableData;

    public News() {
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_news, container, false);

        rvServices = (RecyclerView) view.findViewById(R.id.recyclerviewid);
        mSwipeRefreshLayout = view.findViewById(R.id.swipe_container);
        adapter = new NewsAdapter(new ArrayList<>(), requireContext(), fragment);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(requireContext());
        rvServices.setLayoutManager(layoutManager);
        rvServices.setAdapter(adapter);

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        context = getActivity();
        token = SharedMethods.getDefaults("token", context);
        ButterKnife.bind(this, view);
        api = new RxApi().getApi();
        progressDialog = new ProgressDialog(context);
        loadRecyclerViewData();
        create.setOnClickListener(this);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                Toast.makeText(context, "Refreshing", Toast.LENGTH_LONG).show();
                loadRecyclerViewData();
                mSwipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary,
                        android.R.color.holo_green_dark,
                        android.R.color.holo_orange_dark,
                        android.R.color.holo_blue_dark);

                mSwipeRefreshLayout.setRefreshing(false);
            }
        });


    }

    private void loadRecyclerViewData() {
        progressDialog.setMessage("Loading.Please wait....");
        progressDialog.show();
        Call<newsResponse> call = api.getNewsResources();
        call.enqueue(new Callback<newsResponse>() {
            @Override
            public void onResponse(Call<newsResponse> call, Response<newsResponse> response) {
                try {
                    if (response.body().getSuccess()) {
                        progressDialog.dismiss();
                        if (response.body().getData().size() > 0) {
                            adapter.dataList = response.body().getData();
                            adapter.notifyDataSetChanged();
                        } else {
                            Log.e("kkkkkkkkk", "onResponse: "+ response.body().getData().size());
                            tvNoAvailableData.setVisibility(View.VISIBLE);
                            tvNoAvailableData.setText(getResources().getString(R.string.no_available_data));
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<newsResponse> call, Throwable t) {

            }
        });

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.create:
                NavHostFragment.findNavController(this).navigate(R.id.addResources);
        }

    }


    @Override
    public void onResume() {
        super.onResume();
        // make api call to get list of shop items
        //  loadRecyclerViewData();
    }

}

