package com.app.ecobba.Fragments.Group.ViewMoreFragments;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.app.ecobba.Models.Group_Transaction.AllTransaction;
import com.app.ecobba.R;
import com.bumptech.glide.Glide;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import de.hdodenhof.circleimageview.CircleImageView;

public class ViewMoreTransactionAdapter extends RecyclerView.Adapter<ViewMoreTransactionAdapter.ViewHolder> {

    public List<AllTransaction> datalist;
    public Context context;
    public Fragment fragment;
    public int numOfItems;
    long datepower;
    private final int limit = 3;

    public ViewMoreTransactionAdapter(List<AllTransaction> datalist, Context context, Fragment fragment, int numOfItems) {
        this.datalist = datalist;
        this.context = context;
        this.fragment = fragment;
        this.numOfItems = numOfItems;
    }

    @Override
    public ViewMoreTransactionAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.list_item_group_transaction, parent, false);
        return new ViewMoreTransactionAdapter.ViewHolder(view);

    }

    @Override
    public void onBindViewHolder(final ViewMoreTransactionAdapter.ViewHolder holder, final int position) {
        final String id = String.valueOf(datalist.get(position).getId());
        final String trx_code = datalist.get(position).getTxn_code();
        final String trx_type = String.valueOf(datalist.get(position).getType());
        final int trx_amount = datalist.get(position).getAmount();
        final String trx_date_time = datalist.get(position).getCreated_at();
        String member = datalist.get(position).getName() + " " + datalist.get(position).getOther_names();
        String currency = String.valueOf(datalist.get(position).getCurrency_id());
        String UserProfile = datalist.get(position).getAvatar();

//  load image from the internet using Glide
        Glide.with(context)
                .load(UserProfile)
                .error(R.drawable.ic_shop).override(40, 40)
                .fitCenter().into(holder.profile_image);
//        holder.trx_code.setText(trx_code);
//        holder.tvTransType.setText(trx_type);
        holder.trx_member_name.setText(member);
//        holder.trx_amount.setText(GroupDigits(trx_amount));
        holder.trx_amount.setText(new DecimalFormat("0.00").format(trx_amount));
        Log.e("TRX_DATE_IS", trx_date_time);
        String date = trx_date_time;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));

        try {
            Date date1 = simpleDateFormat.parse(date);
            DateFormat dateFormat = new SimpleDateFormat("EEE dd MMMM yyyy");
            DateFormat dateFormatTime = new SimpleDateFormat("HH:mm");
            Log.e("TRX_DATE_IS_MO", dateFormat.format(date1));
            Log.e("TRX_DATE_IS_M_TM", dateFormatTime.format(date1));
            holder.trx_date.setText(dateFormat.format(date1) + " " + dateFormatTime.format(date1));

        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (currency.equals("109")) {
            holder.trx_currency.setText("KES");
        }
    }

    @Override
    public int getItemCount() {

        return datalist.size();

    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView trx_code, trx_member_name, trx_amount, trx_date, tvTransType, trx_currency;
        // CardView civ;
        LinearLayout linearLayout;
        CircleImageView profile_image;

        public ViewHolder(View itemView) {
            super(itemView);
            // trx_code =  itemView.findViewById(R.id.tvTrxCode);
            trx_member_name = itemView.findViewById(R.id.trx_member_name);
            // tvTransType = itemView.findViewById(R.id.tvTransType);
            trx_amount = itemView.findViewById(R.id.tvTrxAmount);
            trx_date = itemView.findViewById(R.id.tvTransactionDate);
            trx_currency = itemView.findViewById(R.id.trx_currency);
            profile_image = itemView.findViewById(R.id.profile_image);
            // civ = itemView.findViewById(R.id.viewcard);
            //linearLayout=itemView.findViewById(R.id.layout_trxn);
        }
    }

}
