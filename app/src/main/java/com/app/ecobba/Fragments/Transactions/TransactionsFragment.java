package com.app.ecobba.Fragments.Transactions;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.ecobba.Dialogs.DateRangePickerDialogFragment;
import com.app.ecobba.R;
import com.app.ecobba.common.SharedMethods;
import com.app.ecobba.recyclerview_adapters.MyTransactionsRecycler;
import com.app.ecobba.recyclerview_models.UserTransactionsAdapter;
import com.google.android.material.snackbar.Snackbar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.app.ecobba.common.ApisKtKt.getTransactions;
import static com.app.ecobba.common.AppConstantsKt.INITIATOR_GET_TRANSACTIONS;
import static com.app.ecobba.common.SharedMethods.HTTP_GET;
import static com.app.ecobba.common.SharedMethods.getFromServer;
import static com.app.ecobba.common.SharedMethods.global_error_message;
import static com.app.ecobba.common.SharedMethods.runOnUI;
import static com.app.ecobba.common.SharedMethods.showSweetAlertDialogError;

public class TransactionsFragment extends Fragment   implements
        SharedMethods.NetworkObjectListener,
        View.OnClickListener{

    Context ctx;

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.btFilter)
    Button filter;
    public TransactionsFragment(){

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View frag = inflater.inflate(R.layout.fragment_transactions,container,false);
        ButterKnife.bind(this,frag);
        return frag;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ctx = getActivity();
        filter.setOnClickListener(this);
//        recyclerView = view.findViewById(R.id.recyclerView);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(ctx);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());


        String token= SharedMethods.getDefaults("token",ctx);
        fetchLoans(token);

    }

    private void fetchLoans(String token){
        Map<String, String> params = new HashMap<String, String>();
        params.put("token", token);
        getFromServer( getTransactions ,token, params , INITIATOR_GET_TRANSACTIONS , HTTP_GET , ctx ,this);
    }

    @Override
    public void onDataReady(String initiator,final String response) {
        switch (initiator) {

//            case INITIATOR_GET_TRANSACTIONS:
//                try {
//                    runOnUI(new Runnable() {
//                        @Override
//                        public void run() {
//                            Log.e("RANSACTIONS RESPONSE", "Response is " + response);
////                            hideDialog(ctx);
//                            final List<UserTransactionsAdapter> transactionsAdapter = new ArrayList<>();
//                            JSONObject jObj = null;
//                            try {
//                                jObj = new JSONObject(response);
//                                if (jObj.has("errors")) {
//                                    String errors = jObj.getString("errors");
//                                    showSweetAlertDialogError(errors, ctx);
//                                } else { // if no error message was found in the response,
//                                    boolean success = jObj.getBoolean("success");
//                                    if (success) { // if the request was successfuls, got forward, else show error message
//                                        JSONArray jObjUserTransactions = jObj.getJSONArray("transactions");
//                                        //  loop over the tranactions and display in the recyclerview
//                                        if (jObjUserTransactions.length() > 0) {
//                                            for (int i = 0; i < jObjUserTransactions.length(); i++) {
//                                                JSONObject jsonObject = jObjUserTransactions.getJSONObject(i);
//                                                String trx_code = jsonObject.getString("txn_code");
//                                                String amount = jsonObject.getString("amount");
//                                                String date = jsonObject.getString("created_at");
//                                                int txn_type = jsonObject.getInt("txn_type");
//                                                int trx_status = jsonObject.getInt("status");
//                                                String status  = "Failed";
//                                                if(trx_status == 0){
//                                                    status = "Successful";
//                                                }
//                                                String transaction_type = "";
//                                                if(txn_type == 1 ) {// credit
//                                                    transaction_type = "Credit";
//
//                                                } else if(txn_type == 2) { //debit
//                                                    transaction_type = "Debit";
//
//                                                }
//                                                //
//                                                UserTransactionsAdapter transactionsAdapter_ = new UserTransactionsAdapter(trx_code,amount,transaction_type,date,status);
//                                                transactionsAdapter.add(transactionsAdapter_);
//                                            }
//                                            MyTransactionsRecycler cAdapter = new MyTransactionsRecycler(transactionsAdapter, ctx);
//
//                                            recyclerView.setAdapter(cAdapter);
//                                        }
//
//                                    } else {
//                                        String api_error_message = jObj.getString("message");
//                                        showSweetAlertDialogError(api_error_message, ctx);
//                                    }
//                                }
//
//                            } catch (JSONException e) {
//                                Log.e("ExceptionLOGIN-DATA", e.toString());
//                                showSweetAlertDialogError(global_error_message, ctx);
//                                e.printStackTrace();
//                            }
//                        }
//                    });
//                } catch (Exception e) {
////                    hideDialog(ctx);
//                    Log.e("Exception- HOME", e.toString());
//                    showSweetAlertDialogError(global_error_message, ctx);
////                    Toast.makeText(ctx, "Login Data Response Exception" + e.toString(), Toast.LENGTH_LONG).show();
//                }
//                break;
        }
    }

    @Override
    public void onDataLoaded(String initiator, JSONArray data) {

    }

    @Override
    public void onNetworkError(String initiator, String errorCode, final String errorMessage) {
        switch (initiator) {
            case INITIATOR_GET_TRANSACTIONS:
                try{
                    runOnUI(new Runnable() {
                        @Override
                        public void run() {
//                                    hideDialog(ctx);
                            showSweetAlertDialogError(errorMessage, ctx);
                        }
                    });
                }catch (Exception e){
//                            hideDialog(ctx);
                    showSweetAlertDialogError(global_error_message, ctx);
                    Log.e("Exception- HOME", e.toString());
//                    Toast.makeText(ctx, "Login Error Response Exception" + e.toString(), Toast.LENGTH_LONG).show();
                }
                break;
        }
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btFilter:
                new DateRangePickerDialogFragment(new DateRangePickerDialogFragment.onFilterPressed() {
                    @Override
                    public void onFilterPressed(Boolean cancelled, Calendar START, Calendar END) {
                        if (cancelled){
                            Snackbar.make(view,"Filter Cancelled",Snackbar.LENGTH_SHORT).show();
                        }else {
                            Snackbar.make(view,"Filtering "+START.toString()+"-"+END.toString(),Snackbar.LENGTH_SHORT);
                        }
                    }
                }).show(getFragmentManager(),"DATEPICKER");

                break;
        }
    }
}
