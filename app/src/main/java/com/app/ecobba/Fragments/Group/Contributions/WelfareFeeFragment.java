package com.app.ecobba.Fragments.Group.Contributions;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.ecobba.Fragments.Group.Contributions.Adapters.WelfareFeeAdapter;
import com.app.ecobba.Models.payment.contributionResponse;
import com.app.ecobba.R;
import com.app.ecobba.Utils.BaseFragment;
import com.app.ecobba.ViewModel.UserProfileViewModel;
import com.app.ecobba.common.rxApi.Api;
import com.app.ecobba.common.rxApi.RxApi;
import com.github.ybq.android.spinkit.SpinKitView;
import com.github.ybq.android.spinkit.style.Circle;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WelfareFeeFragment extends BaseFragment {
    Context context;
    private RecyclerView recyclerView;
    private Api api;
    Fragment fragment = this;

    String group_id, user_currency;
    @BindView(R.id.tvNoAvailableData)
    TextView tvNoAvailableData;
    SpinKitView spinKitView;
    UserProfileViewModel userProfileViewModel;
    WelfareFeeAdapter adapter;

    public WelfareFeeFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_welfare_fee, container, false);
        ButterKnife.bind(this, view);
        context = getActivity();
        userProfileViewModel = new ViewModelProvider(getActivity()).get(UserProfileViewModel.class);
        spinKitView = (SpinKitView) view.findViewById(R.id.spin_kit);
        Circle circle = new Circle();
        spinKitView.setIndeterminateDrawable(circle);
        recyclerView = view.findViewById(R.id.recyclerView);
        recyclerView.addItemDecoration(new DividerItemDecoration(getContext(),
                DividerItemDecoration.VERTICAL));
        adapter = new WelfareFeeAdapter(new ArrayList<>(), requireContext(), fragment,"");
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(requireContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
        api = new RxApi().getApi();
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        context = getActivity();
        SharedPreferences sh = getContext().getSharedPreferences("PREFERENCE", Context.MODE_PRIVATE);
        group_id = sh.getString("group_id", "");
        Log.e("GROUP_ID_VAL", group_id);
        WelfareFeeContributions(group_id);
        fetchUsersData();
    }

    private void WelfareFeeContributions(String group_id) {
        spinKitView.setVisibility(View.VISIBLE);
        Call<contributionResponse> call = api.getContributions(group_id);
        call.enqueue(new Callback<contributionResponse>() {
            @Override
            public void onResponse(Call<contributionResponse> call, Response<contributionResponse> response) {
               try {
                   if (response.body().getSuccess()) {
                       spinKitView.setVisibility(View.GONE);
                       if (response.body().getData().getContributions().getSOCIETY().getContributions().size() > 0) {
                           String  total_contribution= String.valueOf(response.body().getData().getContributions().getSOCIETY().getTotal());
                           Log.e("TOTAL_WELFARE",total_contribution);
                           adapter.datalist = response.body().getData().getContributions().getSOCIETY().getContributions();
                           adapter.notifyDataSetChanged();
                       } else {
                           spinKitView.setVisibility(View.GONE);
                           tvNoAvailableData.setText(getResources().getString(R.string.no_contribution_available));
                           tvNoAvailableData.setVisibility(View.VISIBLE);
                       }
                   }
               } catch (Exception e) {
                   e.printStackTrace();
               }
            }

            @Override
            public void onFailure(Call<contributionResponse> call, Throwable t) {

            }
        });
    }

    private void fetchUsersData() {
        userProfileViewModel.getUserProfile();
        userProfileViewModel.userProfileResponseLiveData().observe(getViewLifecycleOwner(), userProfileResponse -> {
            try {
                if (userProfileResponse.getSuccess()) {
                    user_currency = userProfileResponse.getUser().getWallet().getCurrency().getPrefix();
                    Log.e("user_currency", user_currency);
                    adapter.user_currency=user_currency;
                    adapter.notifyDataSetChanged();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }
}
