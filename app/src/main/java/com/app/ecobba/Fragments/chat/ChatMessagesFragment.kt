package com.app.ecobba.Fragments.chat

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.app.ecobba.Application.MainApplication
import com.app.ecobba.Fragments.chat.data.Message
import com.app.ecobba.Fragments.chat.data.Participant
import com.app.ecobba.Fragments.chat.data.SendMessage
import com.app.ecobba.R
import com.app.ecobba.Utils.BaseFragment
import com.app.ecobba.Utils.RxUtils
import com.app.ecobba.recyclerview_adapters.GenericAdapter
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.chat_item_rcv.view.*
import kotlinx.android.synthetic.main.chat_item_sent.view.*
import kotlinx.android.synthetic.main.fragment_chat_messages.*
import java.text.SimpleDateFormat
import java.util.*

class ChatMessagesFragment : BaseFragment(){


    val chatViewModel = MainApplication.chatViewModel
    val other : Int by lazy { arguments?.getInt("OTHER_PARTY",0) ?: 0 }
//    Log.e("other_owner_id_is", other.toString()) }



    inner class ViewHolder(override val containerView: View): RecyclerView.ViewHolder(containerView), LayoutContainer

    lateinit var adapter : ChatAdapter<Message,ViewHolder>

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_chat_messages,container,false)
    }

    @SuppressLint("CheckResult")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        toolbar.setNavigationOnClickListener {
            requireActivity().onBackPressed()

        }
//        lateinit var intent: Intent
//        var bundle :Bundle ?=intent.extras
//        var message = bundle!!.getString("owner_name") // 1
//        Log.e("the_owner_is ",message)

        adapter = ChatAdapter(object : GenericAdapter.AdapterInterface<Message,ViewHolder> {
            override fun createViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
                val inflator = LayoutInflater.from(parent.context)
                return when(viewType){
                            0 -> ViewHolder(inflator.inflate(R.layout.chat_item_sent,parent,false))
                            1 -> ViewHolder(inflator.inflate(R.layout.chat_item_rcv,parent,false))
                            else -> ViewHolder(inflator.inflate(R.layout.chat_item_rcv,parent,false))
                        }
            }

            override fun bindViewHolder(viewholder: ViewHolder?, datum: Message?, position: Int) {
                var owner_name: Participant? = datum?.receiver
                Log.e("Particant_name_is", owner_name.toString())
                viewholder?.run {
                    when(itemViewType){
                        0 ->{
                            containerView.chat_item_sent.text  = datum?.message
                            var created_at: String? = datum?.created_at
//                            // The Z is now quoted as a literal.
                            val sdf = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
                            sdf.timeZone = TimeZone.getTimeZone("GMT")

                            var testDate: Date?
                            try {
                                testDate = sdf.parse(created_at)
                                val sdf2 = SimpleDateFormat("HH:mm | MMM dd")
//                                val sdf2 = SimpleDateFormat("HH:mm")
                                var setdate = sdf2.format(testDate)
                                containerView.timestamp_text_view.text = setdate
                                Log.e("chat_item_sent_date",setdate)
                            }catch (e:Exception){
                                e.printStackTrace()
                            }




                        }
                        1 ->{
                            containerView.chat_item_rcv.text = datum?.message
                            var created_at: String? = datum?.created_at
                             // The Z is now quoted as a literal.
                            val sdf = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
                            sdf.timeZone = TimeZone.getTimeZone("Etc/UTC")
                            var testDate: Date? = null
                           // testDate = sdf.parse(created_at)
                            try {
                                testDate = sdf.parse(created_at)
                                val sdf2 = SimpleDateFormat("HH:mm | MMM dd")
                               // val sdf2 = SimpleDateFormat("HH:mm")
                                var setdate = sdf2.format(testDate)
                                containerView.timestamp_text_view2.text = setdate
                                Log.e("chat_item_rcv_date",setdate)
                            }catch (e:Exception){
                                e.printStackTrace()
                            }

                        }
                        else -> {

                        }
                    }
                }
            }

        },rvChat)





        chatViewModel?.chatDb?.getChatMessages()?.compose(RxUtils.applyObservableIoSchedulers())?.subscribe({
            it.filter { it.receiver.id == other || it.sender.id == other  }.run {
                adapter.setData(this)
                rvChat.post {
                    if (size > 0)
                        rvChat.scrollToPosition(size -1)
                }
            }
        },{

        })

        btnSend.setOnClickListener {
            if (!tiMessage.text.toString().isNullOrEmpty()) {
                chatViewModel?.sendMessage(SendMessage(tiMessage.text.toString(),other.toString()))
               tiMessage.setText("")
            }
        }

    }

    /**
     * Other Party 0
     * Me Party 1
     */

    inner class ChatAdapter<T : Message,V : RecyclerView.ViewHolder>(a:AdapterInterface<T,V>,recyclerView: RecyclerView):GenericAdapter<T,V>(a,recyclerView){
        override fun getItemViewType(position: Int): Int {
            return if (data[position].receiver.id == other )
                0
            else
                1
        }
    }

}