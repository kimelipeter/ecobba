package com.app.ecobba.Fragments.Wallet;

import android.annotation.SuppressLint;
import android.content.Context;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.app.ecobba.Models.PaymentSources.AdapaySource;
import com.app.ecobba.R;
import com.mikhaellopez.circularimageview.CircularImageView;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;


public class PaymentSourcesAdapter extends RecyclerView.Adapter<PaymentSourcesAdapter.ViewHolder> {
    public List<AdapaySource> datalist;
    public Context context;
    public Fragment fragment;
    public String avatar;

    public PaymentSourcesAdapter(List<AdapaySource> datalist, Context context, Fragment fragment, String avatar) {
        this.datalist = datalist;
        this.fragment = fragment;
        this.context = context;
        this.avatar = avatar;

        Log.e("TAG_USER_AVATAR", "PaymentSourcesAdapter: " + avatar);

    }

    @NonNull
    @Override
    public PaymentSourcesAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.payment_sources_list_item, parent, false);
        return new PaymentSourcesAdapter.ViewHolder(view);
    }

    @SuppressLint("ResourceType")
    @Override
    public void onBindViewHolder(@NonNull PaymentSourcesAdapter.ViewHolder holder, int position) {

        String telco = datalist.get(position).getTelco();
        String type = datalist.get(position).getType();
        if (telco.equals("safaricom_ke")) {
            holder.tvTelco.setText("Mpesa");
            holder.mpesa.setBackgroundResource(R.drawable.mpesa);
        }
        if (type.equals("mobile_money")) {
            holder.tvType.setText("Mobile Money");
        }
        holder.tvMsisdn.setText(datalist.get(position).getMsisdn());
    }

    @Override
    public int getItemCount() {
        return datalist.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvTelco, tvType, tvMsisdn;
        CircularImageView mpesa;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvTelco = itemView.findViewById(R.id.tvTelco);
            tvType = itemView.findViewById(R.id.tvType);
            tvMsisdn = itemView.findViewById(R.id.tvMsisdn);
            mpesa=itemView.findViewById(R.id.mpesa);
        }
    }
}
