package com.app.ecobba.Fragments.Group.Loans;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

import com.app.ecobba.R;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.android.material.chip.Chip;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ApplyGroupLoanBottomSheetDialog extends BottomSheetDialogFragment {
    Context context;
    @BindView(R.id.close)
    ImageView close;
    @BindView(R.id.tvLoanName)
    TextView tvLoanName;
    @BindView(R.id.tvName)
    TextView tvName;
    @BindView(R.id.tvMinAmount)
    TextView tvMinAmount;
    @BindView(R.id.tvMaxAmount)
    TextView tvMaxAmount;
    @BindView(R.id.tvInterest)
    TextView tvInterest;
    @BindView(R.id.tvRepaymentPlan)
    TextView tvRepayPlan;
    @BindView(R.id.tvRepaymentPeriod)
    TextView tvRepaymentPeriod;
    @BindView(R.id.tvFine)
    TextView tvFine;
    @BindView(R.id.btnApply)
    Chip btnApply;
    public  Fragment fragment;

    String id,loanName,min,max,group_id,fineChargeAmount,interest_rate,repayment_plan,repayment_period;

    public ApplyGroupLoanBottomSheetDialog(String id, String loanName, String min, String max, String group_id, String fineChargeAmount, String interest_rate, String repayment_plan, String repayment_period) {
        this.id=id;
        this.loanName=loanName;
        this.min=min;
        this.max=max;
        this.group_id=group_id;
        this.fineChargeAmount=fineChargeAmount;
        this.interest_rate=interest_rate;
        this.repayment_plan=repayment_plan;
        this.repayment_period=repayment_period;
    }
    @Override
    public int getTheme() {
        return R.style.AppBottomSheetDialogTheme;
    }
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.apply_group_bottom_sheet,container,false);
        setStyle(STYLE_NORMAL, R.style.AppBottomSheetDialogTheme);
        ButterKnife.bind(this, view);
        context = requireActivity();
        fragment=this;
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        context = requireActivity();
        fragment=this;
        close.setOnClickListener(v -> {
            dismiss();
        });
        tvFine.setText(fineChargeAmount);
        tvLoanName.setText(loanName);
        tvName.setText(loanName);
        tvMinAmount.setText(min);
        tvMaxAmount.setText(max);
        tvInterest.setText(interest_rate);
        tvRepaymentPeriod.setText(repayment_period);
        if (repayment_plan.equals("1")) {
            tvRepayPlan.setText("Weekly");
        } else if (repayment_plan.equals("3")) {
           tvRepayPlan.setText("Monthly");
        } else if (repayment_plan.equals("4")) {
           tvRepayPlan.setText("Annually");
        }


        btnApply.setOnClickListener(v -> {
            dismiss();
            Bundle args = new Bundle();
            args.putString("id", id);
            args.putString("loan_name", loanName);
            args.putString("min", min);
            args.putString("max", max);
            args.putString("group_id", group_id);
            args.putString("fine_rate",fineChargeAmount);
            args.putString("loan_limit", "0");
            args.putString("interest", interest_rate);
            args.putString("payment_frequency", repayment_plan);
            args.putString("payment_period", repayment_period);
            args.putString("loanLimit", "0");
                NavHostFragment.findNavController(fragment).navigate(R.id.applyGroupLoanFragment, args);
        });
    }
}
