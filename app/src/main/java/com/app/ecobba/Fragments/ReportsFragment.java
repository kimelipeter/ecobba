package com.app.ecobba.Fragments;

import android.Manifest;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DownloadManager;
import android.content.Context;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.DialogFragment;

import com.app.ecobba.Activities.MainActivity;
import com.app.ecobba.Models.Reports.ReportResponse;
import com.app.ecobba.R;
import com.app.ecobba.Utils.BaseFragment;
import com.app.ecobba.Utils.ExtensionsKt;
import com.app.ecobba.common.SharedMethods;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;
import com.jaredrummler.materialspinner.MaterialSpinner;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.app.ecobba.common.ApisKtKt.reports;
import static com.app.ecobba.common.AppConstantsKt.INITIATOR_USER_REPORTS;
import static com.app.ecobba.common.SharedMethods.HTTP_POST;
import static com.app.ecobba.common.SharedMethods.global_error_message;
import static com.app.ecobba.common.SharedMethods.postToServer;
import static com.app.ecobba.common.SharedMethods.runOnUI;
import static com.app.ecobba.common.SharedMethods.showAlertAndMoveToPage;
import static com.app.ecobba.common.SharedMethods.showSweetAlertDialogError;
import static kotlin.io.ConstantsKt.DEFAULT_BUFFER_SIZE;

public class ReportsFragment extends BaseFragment implements SharedMethods.NetworkObjectListener,
        View.OnClickListener,
        MaterialSpinner.OnItemSelectedListener,
        AppBarLayout.OnOffsetChangedListener {

    //    @BindView(R.id.ablProfile)
//    AppBarLayout ablProfile;
    @BindView(R.id.generateReport)
    Button generateReport;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    MaterialSpinner type_of_resource, sort_criteria, type_of_loan, type_of_report;
    TextView type_of_transaction, DocTitle;
    EditText etStartDate, etEndDate, nameOfSavedDocument;
    Context ctx;
    String token, sort;
    String strTypeOfResource, strTitle, strSortCreteria, strNameOfSavedDoc, strTransValue, strReportTypeValue;
    public static String  strStartDate, strEndDate;

    public ReportsFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View frag = inflater.inflate(R.layout.fragment_report_referal, container, false);
        //  unbinder = ButterKnife.bind(this,frag);
        ButterKnife.bind(this, frag);
//        ablProfile.addOnOffsetChangedListener(this);

        ctx = getActivity();
        token = SharedMethods.getDefaults("token", ctx);

        return frag;
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        etStartDate = view.findViewById(R.id.etStartDate);
        etEndDate = view.findViewById(R.id.etEndDate);
        nameOfSavedDocument = view.findViewById(R.id.nameOfSavedDocument);
        DocTitle = view.findViewById(R.id.DocTitle);
        type_of_resource = (MaterialSpinner) view.findViewById(R.id.type_of_resource);
        sort_criteria = (MaterialSpinner) view.findViewById(R.id.sort_criteria);
        type_of_transaction = view.findViewById(R.id.type_of_transaction);
        type_of_loan = (MaterialSpinner) view.findViewById(R.id.type_of_loan);
        type_of_report = (MaterialSpinner) view.findViewById(R.id.type_of_report);
        type_of_resource.setItems("Loan", "Transaction");
        sort_criteria.setItems("Created At");
        type_of_loan.setItems("All", "Paid", "Unpaid", "Defaulted", "Pending");
        type_of_report.setItems("Excel");
        type_of_resource.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener<String>() {

            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, String item) {
                Snackbar.make(view, "Clicked " + item, Snackbar.LENGTH_LONG).show();

                Log.e("Selected Item", item);

                if (position == 0) {
                    type_of_loan.setItems("Select the Type of Loan", "All", "Paid", "Unpaid", "Defaulted", "Pending");
                    type_of_transaction.setText("Type of Loan");
                } else if (position == 1) {
                    type_of_loan.setItems("Select the Type of Transaction", "Credits", "Debits", "Complete", "Incomplete");
                    type_of_transaction.setText("Type of Transaction");
                }
            }
        });
        sort_criteria.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
                Snackbar.make(view, "Clicked " + item, Snackbar.LENGTH_LONG).show();
                Log.e("Type of Creteria is", item.toString());
            }
        });
        type_of_loan.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
                Snackbar.make(view, "Clicked " + item, Snackbar.LENGTH_LONG).show();

                Log.e("Type of loan", item.toString());
            }
        });
        type_of_report.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
                Snackbar.make(view, "Clicked " + item, Snackbar.LENGTH_LONG).show();

                Log.e("Type of Report", item.toString());
            }
        });


        etStartDate = view.findViewById(R.id.etStartDate);
        etEndDate = view.findViewById(R.id.etEndDate);
        etStartDate.setOnClickListener(this);
        etEndDate.setOnClickListener(this);
        generateReport.setOnClickListener(this);
    }

    private boolean validateFormFields() {
        boolean valid = true;
        strTypeOfResource = type_of_resource.getText().toString().toLowerCase();
        Log.e("Resource_type_is", strTypeOfResource);
        strStartDate = etStartDate.getText().toString();
        strEndDate = etEndDate.getText().toString();
        strTitle = DocTitle.getText().toString();
        strSortCreteria = sort_criteria.getText().toString().toLowerCase();
        sort = strSortCreteria.replace(" ", "_");
        Log.e("sort_criteria_is", sort);
        strNameOfSavedDoc = nameOfSavedDocument.getText().toString();
        strTransValue = type_of_loan.getText().toString().toLowerCase();
        Log.e("type_criteria_is", strTransValue);
        strReportTypeValue = type_of_report.getText().toString().toLowerCase();
        Log.e("type_of_report_is", strReportTypeValue);

        if (strTypeOfResource.isEmpty()) {
            type_of_resource.setError(getString(R.string.emptyField));
            type_of_resource.setFocusableInTouchMode(true);
            type_of_resource.requestFocus();
            valid = false;
        }
        if (strStartDate.isEmpty()) {
            etStartDate.setError(getString(R.string.emptyField));
            etStartDate.setFocusableInTouchMode(true);
            etStartDate.requestFocus();
            valid = false;
        }
        if (strEndDate.isEmpty()) {
            etEndDate.setError(getString(R.string.emptyField));
            valid = false;
        }
        if (strTitle.isEmpty()) {
            DocTitle.setError(getString(R.string.emptyField));
            DocTitle.setFocusableInTouchMode(true);
            DocTitle.requestFocus();
            valid = false;
        }
        if (strSortCreteria.isEmpty()) {
            sort_criteria.setError(getString(R.string.emptyField));
            valid = false;
        }
        if (strNameOfSavedDoc.isEmpty()) {
            nameOfSavedDocument.setError(getString(R.string.emptyField));
            nameOfSavedDocument.setFocusableInTouchMode(true);
            nameOfSavedDocument.requestFocus();
            valid = false;
        }
        if (strTransValue.isEmpty()) {
            type_of_loan.setError(getString(R.string.emptyField));
            type_of_loan.setFocusableInTouchMode(true);
            type_of_loan.requestFocus();
            valid = false;
        }
        if (strReportTypeValue.isEmpty()) {
            type_of_report.setError(getString(R.string.emptyField));
            type_of_report.setFocusableInTouchMode(true);
            type_of_report.requestFocus();
            valid = false;
        }

        return valid;

    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.etStartDate:
                DialogFragment dialogfragment = new startDatePickerDialogTheme();

                dialogfragment.show(getFragmentManager(), "Theme start");
                break;
            case R.id.etEndDate:
                DialogFragment enddialogfragment = new endDatePickerDialogTheme();

                enddialogfragment.show(getFragmentManager(), "Theme end");
                break;
            case R.id.generateReport:
                checkWritePermission();
                // Toast.makeText(requireContext(), "Clicked", Toast.LENGTH_LONG).show();
                break;
        }


    }


    private void checkWritePermission() {

        if (ContextCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.WRITE_EXTERNAL_STORAGE
        ) == PackageManager.PERMISSION_GRANTED) {
            if (validateFormFields()) {
                createReportToAPI();
            }
        } else {
            Toast.makeText(requireContext(), "Please allow write permission", Toast.LENGTH_LONG).show();
            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 23);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (grantResults.length > 0) {
            if (validateFormFields()) {
                createReportToAPI();
            }
        }
    }

    private void createReportToAPI() {
        progressBar.setVisibility(View.VISIBLE);
        String params = "{ \"resource_type\":" + '"' + strTypeOfResource + '"' +
                ", \"from_date\": " + '"' + strStartDate + '"' +
                ", \"to_date\": " + '"' + strEndDate + '"' +
                ", \"title\": " + '"' + strTitle + '"' +
                ", \"created_at\": " + '"' + sort + '"' +
                ", \"file-name\": " + '"' + strNameOfSavedDoc + '"' +
                ", \"type_criteria\": " + '"' + strTransValue + '"' +
                ", \"type\": " + '"' + strReportTypeValue + '"' +
                "}\n" + "\n";
        Log.e("POST_CREATE_REPORT", params);

        postToServer(reports, token, params, INITIATOR_USER_REPORTS, HTTP_POST, ctx, this);

    }

    @Override
    public void onDataReady(String initiator, String response) {
        switch (initiator) {
            case INITIATOR_USER_REPORTS:
                try {
                    runOnUI(new Runnable() {
                        @Override
                        public void run() {
                            Log.e("CREATE_USER_REPORTS", "Response is " + response);
                            progressBar.setVisibility(View.GONE);
                            JSONObject jObj = null;
                            ReportResponse reportResponse = new Gson().fromJson(response, ReportResponse.class);


                            try {
                                jObj = new JSONObject(response);
                                if (jObj.has("errors")) {
                                    String errors = jObj.getString("errors");
                                    showSweetAlertDialogError(errors, ctx);
                                } else { // if no error message was found in the response,
                                    // boolean success = jObj.getBoolean(reportResponse);
                                    if (reportResponse.getSuccess()) {
                                        String message = reportResponse.getMessage();
                                        String file = reportResponse.getData().getReport_file();
                                        DownloadManager downloadmanager = (DownloadManager) ctx.getSystemService(Context.DOWNLOAD_SERVICE);
                                        Uri uri = Uri.parse(file);
                                        DownloadManager.Request request = new DownloadManager.Request(uri);
                                        request.setTitle(strTitle);
                                        request.setDescription("Downloading....");
                                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                                            request.allowScanningByMediaScanner();
                                            request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
                                        }
                                        //  request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
                                        request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, file);
                                        request.setVisibleInDownloadsUi(true);
                                        downloadmanager.enqueue(request);

                                        Log.e("file_path", file);
                                        Log.e("file_uri", String.valueOf(uri));


                                        showAlertAndMoveToPage(message, MainActivity.class, ctx);
                                    } else {
                                        String api_error_message = reportResponse.getMessage();
                                        showSweetAlertDialogError(api_error_message, ctx);
                                    }
                                }

                            } catch (JSONException e) {
                                Log.e("Exception-DATA", e.toString());
                                showSweetAlertDialogError(global_error_message, ctx);
                                e.printStackTrace();
                            }
                        }
                    });
                } catch (Exception e) {
                    progressBar.setVisibility(View.GONE);
                    Log.e("Exception_+USER_REPORTS", e.toString());
                    showSweetAlertDialogError(global_error_message, ctx);
//                    Toast.makeText(ctx, "Login Data Response Exception" + e.toString(), Toast.LENGTH_LONG).show();
                }
                break;
        }

    }


    @Override
    public void onDataLoaded(String initiator, JSONArray data) {

    }

    @Override
    public void onNetworkError(String initiator, String errorCode, String errorMessage) {
        switch (initiator) {
            case INITIATOR_USER_REPORTS:
                try {
                    runOnUI(new Runnable() {
                        @Override
                        public void run() {
                            progressBar.setVisibility(View.GONE);
                            showSweetAlertDialogError(errorMessage, ctx);
                        }
                    });
                } catch (Exception e) {
                    progressBar.setVisibility(View.GONE);
                    showSweetAlertDialogError(global_error_message, ctx);
                    Log.e("Exception- HOME", e.toString());
//                    Toast.makeText(ctx, "Login Error Response Exception" + e.toString(), Toast.LENGTH_LONG).show();
                }
                break;

        }

    }

    @Override
    public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {

    }

    int val = 0;
    boolean transparent = true;

    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int i) {

        if (i < val) {
            if (transparent) {
                ((View) getActivity().findViewById(R.id.toolbar)).setBackgroundResource(R.color.colorSurface);
                transparent = false;
            }
        } else if (i > val) {

            if (!transparent) {
                ((View) getActivity().findViewById(R.id.toolbar)).setBackgroundResource(R.color.colorSurfaceAlpha);
                transparent = true;
            }
        }

        val = i;

    }

    public static class startDatePickerDialogTheme extends DialogFragment implements DatePickerDialog.OnDateSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final Calendar calendar = Calendar.getInstance();
            int year = calendar.get(Calendar.YEAR);
            int month = calendar.get(Calendar.MONTH);
            int day = calendar.get(Calendar.DAY_OF_MONTH);

            DatePickerDialog datepickerdialog = new DatePickerDialog(getActivity(),
                    AlertDialog.THEME_HOLO_LIGHT, this, year, month, day-1);
            //following line to restrict future date selection
            datepickerdialog.getDatePicker().setMaxDate(System.currentTimeMillis());
            datepickerdialog.show();

            return datepickerdialog;
        }

        public void onDateSet(DatePicker view, int year, int month, int day) {

            EditText editText = getActivity().findViewById(R.id.etStartDate);
            String string = String.valueOf(day) + "-" + String.valueOf(month + 1) + "-" + String.valueOf(year);
            editText.setText(string);
            Log.e("edrfghjkuytrdfgh", string);
            try {
                Date date = new SimpleDateFormat("dd-MM-yyyy").parse(string);
                final Calendar datePickerCal = Calendar.getInstance();
                datePickerCal.set(year, month, day);
                strStartDate = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).format(datePickerCal.getTime());
                Log.e("REWARDZ_AD", strStartDate);
//                            dob = date[0];
            } catch (ParseException e) {
                e.printStackTrace();
            }

        }
    }
 public static class endDatePickerDialogTheme extends DialogFragment implements DatePickerDialog.OnDateSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final Calendar calendar = Calendar.getInstance();
            int year = calendar.get(Calendar.YEAR);
            int month = calendar.get(Calendar.MONTH);
            int day = calendar.get(Calendar.DAY_OF_MONTH);

            DatePickerDialog datepickerdialog = new DatePickerDialog(getActivity(),
                    AlertDialog.THEME_HOLO_LIGHT, this, year, month, day);
            //following line to restrict future date selection
            datepickerdialog.getDatePicker().setMaxDate(System.currentTimeMillis());
            datepickerdialog.show();

            return datepickerdialog;
        }



        public void onDateSet(DatePicker view, int year, int month, int day) {

            EditText editText1 = getActivity().findViewById(R.id.etEndDate);
            String string = String.valueOf(day) + "-" + String.valueOf(month + 1) + "-" + String.valueOf(year);
            editText1.setText(string);
            Log.e("edrfghjkuytrdfgh", string);
            try {
                Date date = new SimpleDateFormat("dd-MM-yyyy").parse(string);
                final Calendar datePickerCal = Calendar.getInstance();
                datePickerCal.set(year, month, day);
                strEndDate = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).format(datePickerCal.getTime());
                Log.e("REWARDZ_AD", strEndDate);
//                            dob = date[0];
            } catch (ParseException e) {
                e.printStackTrace();
            }

        }
    }

}
