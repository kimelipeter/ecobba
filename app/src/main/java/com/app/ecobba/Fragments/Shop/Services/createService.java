package com.app.ecobba.Fragments.Shop.Services;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.navigation.fragment.NavHostFragment;

import com.app.ecobba.R;
import com.app.ecobba.common.ApisKtKt;
import com.app.ecobba.common.SharedMethods;
import com.app.ecobba.recyclerview_models.ServiceCategories.servicesCategoriesResponse;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.ProgressCallback;

import org.json.JSONArray;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import in.mayanknagwanshi.imagepicker.ImageSelectActivity;

import static com.app.ecobba.common.SharedMethods.showSweetAlertDialogSuccess;

public class createService extends Fragment implements SharedMethods.NetworkObjectListener, View.OnClickListener {
    @BindView(R.id.create) Button create;
    Spinner item_category;
    List<servicesCategoriesResponse> servicesCategoriesResponse;
    String url= ApisKtKt.createService;
    private String token = "";
    Context ctx;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    private String _item_name, _offer_duration,_item_price, _item_description, _item_category, _item_currency,item_cat;
    EditText item_name,offer_duration, item_price, item_description;
    private Unbinder unbinder;
    int MY_PERMISSIONS_REQUEST_CAMERA=0;

    @BindView(R.id.imDocument)
    ImageView ivDocument;

    ImageView imageViewUpload;
    private ArrayList<String> arrayList = new ArrayList<String>();
    private String filePath1;
    File fileToUpload;
    ImageView uploadImage;
    TextView choseImage;
    Bitmap selectedImage;



    public createService() {
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View frag = inflater.inflate(R.layout.fragment_create_services,container,false);
        ButterKnife.bind(this,frag);
        token = SharedMethods.getDefaults("token", requireActivity());
        item_name=frag.findViewById(R.id.item_name);
        offer_duration=frag.findViewById(R.id.offer_duration);
        item_price=frag.findViewById(R.id.item_price);
        item_price=frag.findViewById(R.id.item_price);
        item_description=frag.findViewById(R.id.item_description);
        item_category=frag.findViewById(R.id.item_category);
        item_category.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                Toast.makeText(getActivity(), "This is " +
                        adapterView.getItemAtPosition(i).toString(), Toast.LENGTH_LONG).show();
                item_cat= adapterView.getItemAtPosition(i).toString();

                try {
                    //Your task here
                }catch (Exception e)
                {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        ////start of camera access persmission
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA)
                == PackageManager.PERMISSION_DENIED)


            ActivityCompat.requestPermissions(getActivity(), new String[] {Manifest.permission.CAMERA}, MY_PERMISSIONS_REQUEST_CAMERA);

        ////end of camera access persmission
        uploadImage =frag. findViewById(R.id.post_image);
        choseImage =frag. findViewById(R.id.choose_image_text);
        imageViewUpload=frag.findViewById(R.id.imDocument);
        choseImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getActivity(), ImageSelectActivity.class);
                intent.putExtra(ImageSelectActivity.FLAG_COMPRESS, false);//default is true
                intent.putExtra(ImageSelectActivity.FLAG_CAMERA, true);//default is true
                intent.putExtra(ImageSelectActivity.FLAG_GALLERY, true);//default is true
                startActivityForResult(intent, 1213);


            }
        });
        imageViewUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getActivity(), ImageSelectActivity.class);
                intent.putExtra(ImageSelectActivity.FLAG_COMPRESS, false);//default is true
                intent.putExtra(ImageSelectActivity.FLAG_CAMERA, true);//default is true
                intent.putExtra(ImageSelectActivity.FLAG_GALLERY, true);//default is true
                startActivityForResult(intent, 1213);


            }
        });

        return frag;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1213 && resultCode == Activity.RESULT_OK) {
            filePath1 = data.getStringExtra(ImageSelectActivity.RESULT_FILE_PATH);
            selectedImage = BitmapFactory.decodeFile(filePath1);
            Log.e("image path is",filePath1);
            fileToUpload = new File(filePath1);
            arrayList.add(filePath1);
            ivDocument.setImageBitmap(selectedImage);

        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        token = SharedMethods.getDefaults("token", requireActivity());
        ctx=getActivity();
        create.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.create:
                if(validateFormFields()){
                    createService();
                }
                break;
        }

    }

    private boolean validateFormFields() {
        boolean valid = true;

        _item_name = item_name.getText().toString().trim();

        _offer_duration = offer_duration.getText().toString().trim();
        _item_price = item_price.getText().toString().trim();
        _item_description = item_description.getText().toString().trim();
        Log.e("POST_DATA",_item_name+_offer_duration+_item_price+_item_description + item_cat+fileToUpload);



        if (_item_name.isEmpty()) {
            item_name.setError(getString(R.string.emptyField));
            valid = false;
        }
        if (_offer_duration.isEmpty()) {
            offer_duration.setError(getString(R.string.emptyField));
            valid = false;
        }

        if (_item_price.isEmpty()) {
            item_price.setError(getString(R.string.emptyField));
            valid = false;
        }
        if (_item_description.isEmpty()) {
            item_description.setError(getString(R.string.emptyField));
            valid = false;
        }


        return valid;
    }

    public void createService(){


            ProgressDialog mProgressDialog = new ProgressDialog(getActivity());
            mProgressDialog.setMessage("Please Wait....");
            mProgressDialog.setIndeterminate(true);
            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            mProgressDialog.setCancelable(true);

            mProgressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {
                    // downloadTask.cancel(true);
                }
            });
            mProgressDialog.show();

//==============ION part
            Ion.with(getActivity())
                    .load("POST",url)
                    .uploadProgressDialog(mProgressDialog)
                    .uploadProgressHandler(new ProgressCallback() {
                        @Override
                        public void onProgress(long uploaded, long total) {
                            mProgressDialog.setIndeterminate(true);
                            mProgressDialog.setMax((int) total);
                            mProgressDialog.setProgress((int) uploaded);

                        }
                    })
                    .addHeader("Accept","application/json")
                    .addHeader("Authorization","Bearer " +token)
                    .setMultipartParameter("description",_item_description)
                    .setMultipartParameter("title", _item_name)
                    .setMultipartParameter("category",item_cat)
                    .setMultipartParameter("duration",_offer_duration)
                    .setMultipartParameter("price",_item_price)
                    .setMultipartFile("service_image", "image/jpeg", new File(arrayList.get(0)))
                    .setMultipartFile("image", "image/jpeg",  new File(arrayList.get(0)))
                    .asJsonObject()
                    .setCallback(new FutureCallback<JsonObject>() {
                        @Override
                        public void onCompleted(Exception e, JsonObject result) {
                            // do stuff with the result or error
                            if (result != null) {
                                //Upload Success
                                Log.e("Upload_data_is", String.valueOf(result));
                                showSweetAlertDialogSuccess("Saved Successfully", ctx);
                                NavHostFragment.findNavController(getParentFragment()).navigateUp();
                                mProgressDialog.dismiss();

//                                Intent intent= new Intent(CreateNewItemActivity.this, KweaItemsActivity.class);
//                                intent.putExtra("shop_id_number",shop_id_number);
//                                startActivity(intent);
                                return;
                            } else {
                                //Upload Failed
                                progressBar.setVisibility(View.GONE);
                                Log.e("ERROR_UPLOAD " + e.getMessage(), String.valueOf(e));
                                Toast.makeText(ctx, "Error while adding Service item ", Toast.LENGTH_LONG).show();
                                mProgressDialog.dismiss();
                                return;

                            }
                        }
                    });


    }

    @Override
    public void onDataReady(String initiator, String response) {

    }

    @Override
    public void onDataLoaded(String initiator, JSONArray data) {

    }

    @Override
    public void onNetworkError(String initiator, String errorCode, String errorMessage) {

    }
}
