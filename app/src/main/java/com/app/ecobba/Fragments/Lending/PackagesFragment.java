
package com.app.ecobba.Fragments.Lending;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.ecobba.Dialogs.DateRangePickerDialogFragment;
import com.app.ecobba.Fragments.Borrowing.Adapters.AvailableLoansAdapter;
import com.app.ecobba.Models.Lending.lendingLoansResponse;
import com.app.ecobba.R;
import com.app.ecobba.common.SharedMethods;
import com.app.ecobba.common.rxApi.Api;
import com.app.ecobba.common.rxApi.RxApi;
import com.app.ecobba.recyclerview_adapters.MyLoansPackagesRecycler;

import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.card.MaterialCardView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.skyfishjy.library.RippleBackground;


import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;



public class PackagesFragment extends Fragment implements
        View.OnClickListener,
        AppBarLayout.OnOffsetChangedListener {
    Button search;
    MaterialCardView card;
    //    TextInputLayout searchContainer;
    @BindView(R.id.tvNoAvailableLoans)
    TextView tvNoAvailableLoans;
    // @BindView(R.id.tvActive) TextView tvActive;
    //@BindView(R.id.tvTotal) TextView tvTotal;
    RippleBackground rippleBackground;
    ImageView imageView;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.ablProfile)
    AppBarLayout ablProfile;
    Context context;
    RecyclerView recyclerView;
    //    @BindView(R.id.recyclerView)   RecyclerView create_loan_package;
    FloatingActionButton create;
    Fragment fragment;
    MyLoansPackagesRecycler adapter;
    private Api api;

    public PackagesFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_lending, container, false);
        ButterKnife.bind(this, view);
        context = getActivity();
        fragment = this;
        api = new RxApi().getApi();
        recyclerView = view.findViewById(R.id.recyclerViewLoans);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
        adapter = new MyLoansPackagesRecycler(new ArrayList(), context, fragment);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
        ablProfile.addOnOffsetChangedListener(this);
         rippleBackground=(RippleBackground) view.findViewById(R.id.content);
         imageView=(ImageView) view.findViewById(R.id.centerImage);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        context = getActivity();
        fragment = this;
        recyclerView = view.findViewById(R.id.recyclerViewLoans);
        create = view.findViewById(R.id.create);
        create.setOnClickListener(this);
        fetchLoansPackages();


        rippleBackground.startRippleAnimation();
//        imageView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                rippleBackground.startRippleAnimation();
//            }
//        });



        ablProfile.addOnOffsetChangedListener(this);

//        search = view.findViewById(R.id.search);
//        search.setOnClickListener(this);

    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.search:
                new DateRangePickerDialogFragment(new DateRangePickerDialogFragment.onFilterPressed() {
                    @Override
                    public void onFilterPressed(Boolean cancelled, Calendar START, Calendar END) {
                        if (cancelled) {
                            Snackbar.make(view, "Filter Cancelled", Snackbar.LENGTH_SHORT).show();
                        } else {
                            Snackbar.make(view, "Filtering " + START.toString() + "-" + END.toString(), Snackbar.LENGTH_SHORT);
                        }
                    }
                }).show(getFragmentManager(), "DATEPICKER");
                break;
            case R.id.card:
                NavHostFragment.findNavController(this).navigate(R.id.packageFragment);
                break;
            case R.id.create:
                NavHostFragment.findNavController(this).navigate(R.id.createPackageFragment);
                break;
        }
    }

    private void viewVisibilityToggle(View view) {
        if (view.getVisibility() == View.VISIBLE) {
            view.setVisibility(View.GONE);
        } else {
            view.setVisibility(View.VISIBLE);
        }
    }

    private void fetchLoansPackages() {
        Call<lendingLoansResponse> call = api.getUserLendingPackages();
        call.enqueue(new Callback<lendingLoansResponse>() {
            @Override
            public void onResponse(Call<lendingLoansResponse> call, Response<lendingLoansResponse> response) {
                if (response.body().getSuccess()) {
                    Log.e("PPPPP", "onResponse: "+response );
                   try {
                       if (response.body().getLoan_packages().size() > 0) {
                           progressBar.setVisibility(View.GONE);
                           adapter.datalist = response.body().getLoan_packages();
                           adapter.notifyDataSetChanged();
                       } else {
                           progressBar.setVisibility(View.GONE);
                           rippleBackground.setVisibility(View.VISIBLE);
                           tvNoAvailableLoans.setText(getResources().getString(R.string.no_available_data));
                           tvNoAvailableLoans.setVisibility(View.VISIBLE);
                       }
                   } catch (Exception e) {
                       e.printStackTrace();
                       Log.e("TAG", "onResponse: "+e.getLocalizedMessage());
                   }
                }
            }

            @Override
            public void onFailure(Call<lendingLoansResponse> call, Throwable t) {
                Log.e("TAG_ERROR_DATA", "onFailure: "+t.getLocalizedMessage());

            }
        });
    }



    int val = 0;
    boolean transparent = true;

    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int i) {
        if (i < val) {
            if (transparent) {
                ((View) getActivity().findViewById(R.id.toolbar)).setBackgroundResource(R.color.colorSurface);
                transparent = false;
                create.hide();
            }
        } else if (i > val) {
            if (!transparent) {
                ((View) getActivity().findViewById(R.id.toolbar)).setBackgroundResource(R.color.colorSurfaceAlpha);
                transparent = true;
                create.show();
            }
        }
        val = i;
    }
}