package com.app.ecobba.Fragments.Group.Settings;

import android.app.TimePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

import com.app.ecobba.Activities.MainActivity;
import com.app.ecobba.R;
import com.app.ecobba.common.SharedMethods;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.app.ecobba.common.ApisKtKt.postGroupMeetings;
import static com.app.ecobba.common.ApisKtKt.storeMeetingsSettings;
import static com.app.ecobba.common.AppConstantsKt.INITIATOR_SCHEDULE_MEETING;
import static com.app.ecobba.common.AppConstantsKt.INITIATOR_STORE_MEETING_SETTING;
import static com.app.ecobba.common.SharedMethods.HTTP_POST;
import static com.app.ecobba.common.SharedMethods.global_error_message;
import static com.app.ecobba.common.SharedMethods.postToServer;
import static com.app.ecobba.common.SharedMethods.runOnUI;
import static com.app.ecobba.common.SharedMethods.showAlertAndMoveToPage;
import static com.app.ecobba.common.SharedMethods.showSweetAlertDialogError;
import static com.app.ecobba.common.SharedMethods.showSweetAlertDialogSuccess;

public class CreateMeetingSetting  extends Fragment implements View.OnClickListener, SharedMethods.NetworkObjectListener {
    @BindView(R.id.etMeetingName)
    EditText etMeetingName;
    @BindView(R.id.day)
    Spinner day;
    @BindView(R.id.period)
    Spinner period;
    @BindView(R.id.frequency)
    Spinner frequency;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    EditText etMeetingTime;
    @BindView(R.id.etMeetingVenue)
    EditText etMeetingVenue;
    Context ctx;
    String token, _day,_financial_period,_frequency, meeting_time,group_id,setting_id;
    String _meeting_name,_venue;
    @BindView(R.id.create)
    Button create;
    Bundle args;

    public CreateMeetingSetting() {
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_create_meeting_setting,container,false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        ctx = getActivity();
        args = getArguments();
        assert args != null;
        group_id = args.getString("group_id");
        setting_id = args.getString("id");
        Log.e("settings_ID_VAL","group id is " +group_id +" and setting id is "+setting_id);

        token = SharedMethods.getDefaults("token", ctx);

        etMeetingTime = view.findViewById(R.id.etMeetingTime);

        etMeetingTime.setOnClickListener(this);
        create.setOnClickListener(this);
        day.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                _day =(String) parent.getItemAtPosition(position);


            }


            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        period.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                _financial_period =(String) parent.getItemAtPosition(position);

            }


            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        frequency.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                _frequency =(String) parent.getItemAtPosition(position);


            }


            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.etMeetingTime:
                pickTime(etMeetingTime);
                break;
            case R.id.create:
                if(validateFormFields()) {
                    createMeetingToAPI();
                }
                break;
        }

    }

    private void createMeetingToAPI() {
        progressBar.setVisibility(View.VISIBLE);

        String params = "{ \"setting_name\":" + '"' + _meeting_name + '"' +
                ", \"venue\": " + '"' + _venue + '"' +
                ", \"day\": " + '"' + _day + '"' +
                ", \"time\": " + '"' + meeting_time + '"' +
                ", \"frequency\": " + '"' + _frequency + '"' +
                ", \"financial_period\": " + '"' + _financial_period + '"' +
                ", \"group_id\": " + '"' + group_id + '"' +
                ", \"setting_id\": " + '"' + setting_id + '"' +

                "}\n" + "\n";
        Log.e("POST_PARAMS", params);

        //scheduleMeeting
        postToServer(storeMeetingsSettings, token, params, INITIATOR_STORE_MEETING_SETTING, HTTP_POST, ctx, this);

    }

    private boolean validateFormFields() {
        boolean valid= true;
        _meeting_name=etMeetingName.getText().toString();
        _venue=etMeetingVenue.getText().toString();

        if (_meeting_name.isEmpty()) {
            etMeetingName.setError(getString(R.string.emptyField));
            valid = false;
        }
        if (_venue.isEmpty()) {
            etMeetingVenue.setError(getString(R.string.emptyField));
            valid = false;
        }

        return valid;
    }

    private void pickTime(EditText view) {

        new TimePickerDialog(ctx, new TimePickerDialog.OnTimeSetListener() {

            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {

                String time = hourOfDay + ":" + minute;

                try {
                    final SimpleDateFormat sdf = new SimpleDateFormat("H:mm");
                    final Date dateObj = sdf.parse(time);
                    //meeting_time = new SimpleDateFormat("hh:mm aa").format(dateObj);

                    meeting_time = new SimpleDateFormat("hh:mm").format(dateObj);
                    etMeetingTime.setText(meeting_time);

                    Toast.makeText(ctx, meeting_time, Toast.LENGTH_LONG).show();

                } catch (final ParseException e) {
                    e.printStackTrace();
                }

            }
        }, 0, 0, true).show();


    }

    @Override
    public void onDataReady(String initiator, String response) {
        switch (initiator){
            case INITIATOR_STORE_MEETING_SETTING:
                try {
                    runOnUI(new Runnable() {
                        @Override
                        public void run() {
                            Log.e("SCHEDULE_GROUP_MEETING", "Response is " + response);
                            progressBar.setVisibility(View.GONE);
                            JSONObject jObj = null;
                            try {
                                jObj = new JSONObject(response);
                                if (jObj.has("errors")) {
                                    String errors = jObj.getString("errors");
                                    showSweetAlertDialogError(errors, ctx);
                                } else { // if no error message was found in the response,
                                    boolean success = jObj.getBoolean("success");
                                    if (success) {
                                        String api_success_message = jObj.getString("message");
//
                                        showAlertAndMoveToPage(api_success_message, MainActivity.class, ctx);


                                    } else {
                                        String api_error_message = jObj.getString("message");
                                        showSweetAlertDialogError(api_error_message, ctx);
                                    }
                                }

                            } catch (JSONException e) {
                                Log.e("Exception-DATA", e.toString());
                                showSweetAlertDialogError(global_error_message, ctx);
                                e.printStackTrace();
                            }
                        }
                    });
                } catch (Exception e) {
                    progressBar.setVisibility(View.GONE);
                    Log.e("Exception_+GROUP_MEMBER", e.toString());
                    showSweetAlertDialogError(global_error_message, ctx);
//                    Toast.makeText(ctx, "Login Data Response Exception" + e.toString(), Toast.LENGTH_LONG).show();
                }
                break;
        }

    }

    @Override
    public void onDataLoaded(String initiator, JSONArray data) {

    }

    @Override
    public void onNetworkError(String initiator, String errorCode, String errorMessage) {
        switch (initiator){
            case INITIATOR_STORE_MEETING_SETTING:
                try{
                    runOnUI(new Runnable() {
                        @Override
                        public void run() {
                            progressBar.setVisibility(View.GONE);
                            showSweetAlertDialogError(errorMessage, ctx);
                        }
                    });
                }catch (Exception e){
                    progressBar.setVisibility(View.GONE);
                    showSweetAlertDialogError(global_error_message, ctx);
                    Log.e("Exception- HOME", e.toString());
//                    Toast.makeText(ctx, "Login Error Response Exception" + e.toString(), Toast.LENGTH_LONG).show();
                }

                break;
        }

    }
}
