package com.app.ecobba.Fragments.chat.data

import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "chat_messages")
data class Message(
        @PrimaryKey
        @NonNull
        @ColumnInfo(name = "id")
        var id: Int = 0,
        var created_at: String = "",
        var message: String = "",
        var `receiver`: Participant = Participant(),
        var receiver_id: Int? = 0,
        var sender: Participant = Participant(),
        var sender_id: Int? = 0,
        var updated_at: String? = ""
){
    override fun equals(other: Any?): Boolean {
        return other != null && (other is Message) && other.id == id
    }

    override fun hashCode(): Int {
        return id
    }

}