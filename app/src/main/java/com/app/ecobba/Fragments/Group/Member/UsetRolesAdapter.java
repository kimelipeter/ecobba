package com.app.ecobba.Fragments.Group.Member;

import android.content.Context;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.ecobba.Models.Roles.Message;
import com.app.ecobba.R;


import java.util.List;


public class UsetRolesAdapter extends RecyclerView.Adapter<UsetRolesAdapter.ViewHolder> {
    public List<Message> datalist;
    public Context context;

    private RadioButton lastChecked = null;
    private int lastCheckedPos = 0;
    private int selectedStarPosition = -1;
    public CallBack callBack;

    private static RadioButton lastCheckedItem = null;
    private static int lastCheckedPosition = 0;

    public UsetRolesAdapter(List<Message> datalist, Context context, CallBack callBack) {
        this.datalist = datalist;
        this.context = context;
        this.callBack = callBack;
    }

    @NonNull
    @Override
    public UsetRolesAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.list_roles_item, parent, false);
        return new UsetRolesAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull UsetRolesAdapter.ViewHolder holder, int position) {
        try {
            String name = datalist.get(position).getDisplay_name();
            int role_id = datalist.get(position).getId();

            for (int i = 0; i < holder.selectionState.getChildCount(); i++) {
                ((RadioButton) holder.selectionState.getChildAt(i)).setText(name);
                ((RadioButton) holder.selectionState.getChildAt(i)).setChecked(position == selectedStarPosition);

                holder.selected.setTag(new Integer(position));
                if (position == 0 && holder.selected.isChecked()) {
                    lastChecked = holder.selected;
                    lastCheckedPos = 0;
                }
                holder.selected.setOnClickListener(v -> {
                    RadioButton cb = (RadioButton) v;
                    int clickedPos = ((Integer) cb.getTag()).intValue();
                    if (cb.isChecked()) {
                        if (lastChecked != null) {
                            lastChecked.setChecked(false);
                        }
                        lastChecked = cb;
                        lastCheckedPos = clickedPos;
                        Toast toast = Toast.makeText(context, "Role id is " + role_id + " and role name is " + name, Toast.LENGTH_LONG);
                        toast.setGravity(Gravity.CENTER, 0, 0);
                        callBack.getRoleId(role_id);
                        toast.show();
                    } else
                        lastChecked = null;
                });

            }


        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return datalist.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public RadioGroup selectionState;
        public RadioButton selected;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            selectionState = itemView.findViewById(R.id.selectedRole);
            selected = itemView.findViewById(R.id.selected);

        }
    }


    public interface CallBack {
        void getRoleId(int role_id);
    }
}
