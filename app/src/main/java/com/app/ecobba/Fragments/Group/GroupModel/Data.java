
package com.app.ecobba.Fragments.Group.GroupModel;

import java.util.List;

@lombok.Data
@SuppressWarnings("unused")
public class Data {

    private List<Object> associations;
    private List<LevelOne> levelOnes;
    private RegionDetail regionDetail;
    private List<String> regionsArr;
    private long user;

}
