package com.app.ecobba.Fragments.Shop;

public class CreateShopKwea {

    private String shop_name;
    private String shop_email;
    private  String shop_msisdn;
    private  String shop_address;

    public CreateShopKwea(String shop_name, String shop_email, String shop_msisdn, String shop_address) {
        this.shop_name = shop_name;
        this.shop_email = shop_email;
        this.shop_msisdn = shop_msisdn;
        this.shop_address = shop_address;
    }

    public String getShop_name() {
        return shop_name;
    }

    public void setShop_name(String shop_name) {
        this.shop_name = shop_name;
    }

    public String getShop_email() {
        return shop_email;
    }

    public void setShop_email(String shop_email) {
        this.shop_email = shop_email;
    }

    public String getShop_msisdn() {
        return shop_msisdn;
    }

    public void setShop_msisdn(String shop_msisdn) {
        this.shop_msisdn = shop_msisdn;
    }

    public String getShop_address() {
        return shop_address;
    }

    public void setShop_address(String shop_address) {
        this.shop_address = shop_address;
    }
}
