package com.app.ecobba.Fragments.Profile;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.util.Pair;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.PermissionRequest;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.fragment.NavHostFragment;
import androidx.viewpager.widget.ViewPager;

import com.app.ecobba.BuildConfig;
import com.app.ecobba.Fragments.Profile.Avatar.UpdateAvatarDialog;
import com.app.ecobba.Models.UserProfile.Detail;
import com.app.ecobba.ViewModel.UserProfileViewModel;
import com.budiyev.android.circularprogressbar.CircularProgressBar;
import com.app.ecobba.Adapter.ProfileViewPagerAdapter;
import com.app.ecobba.Fragments.Profile.viewmodel.ProfileViewModel;
import com.app.ecobba.R;
import com.app.ecobba.common.SharedMethods;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.tabs.TabLayout;
import com.google.gson.Gson;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.mikhaellopez.circularimageview.CircularImageView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.app.Activity.RESULT_OK;
import static com.app.ecobba.common.ApisKtKt.updateUser;
import static com.app.ecobba.common.AppConstantsKt.INITIATOR_GETERGISTER_DATA_REG;
import static com.app.ecobba.common.AppConstantsKt.INITIATOR_GETPROFILE;
import static com.app.ecobba.common.AppConstantsKt.INITIATOR_UPDATE_USER;
import static com.app.ecobba.common.SharedMethods.HTTP_GET;
import static com.app.ecobba.common.SharedMethods.HTTP_POST;
import static com.app.ecobba.common.SharedMethods.getFromServer;
import static com.app.ecobba.common.SharedMethods.global_error_message;
import static com.app.ecobba.common.SharedMethods.postToServer;
import static com.app.ecobba.common.SharedMethods.runOnUI;
import static com.app.ecobba.common.SharedMethods.showSweetAlertDialogError;
import static com.app.ecobba.common.SharedMethods.showSweetAlertDialogSuccess;

public class EditProfileFragment extends Fragment implements View.OnClickListener, AppBarLayout.OnOffsetChangedListener, SharedMethods.NetworkObjectListener {


    private static final String TAG = "EditProfileFragment";
    @BindView(R.id.vpProfile)
    ViewPager vpProfile;

    @BindView(R.id.tlProfile)
    TabLayout tlProfile;

    @BindView(R.id.btUpdate)
    Button update;

    @BindView(R.id.ablProfile)
    AppBarLayout ablProfile;

    @BindView(R.id.tvProfileProgress)
    TextView tvProfileProgress;

    @BindView(R.id.cpbProfile)
    CircularProgressBar cpbProfile;

    @BindView(R.id.tvRemark)
    TextView tvRemark;
    @BindView(R.id.llProgress)
    LinearLayout llProgress;

    ProfileViewPagerAdapter adapter;
    Context ctx;
    String token;

    ProfileViewModel profileViewModel;
    UserProfileViewModel userProfileViewModel;

    CircularImageView user_prof;
    static final int REQUEST_TAKE_PHOTO = 101;
    static final int REQUEST_GALLERY_PHOTO = 102;
    File mPhotoFile;


    UpdateAvatarDialog updateAvatarDialog;

    public EditProfileFragment() {

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View frag = inflater.inflate(R.layout.fragment_profile_edit, container, false);
        ButterKnife.bind(this, frag);
        ablProfile.addOnOffsetChangedListener(this);
        profileViewModel = ViewModelProviders.of(getActivity()).get(ProfileViewModel.class);

        userProfileViewModel = new ViewModelProvider(getActivity()).get(UserProfileViewModel.class);
        user_prof=frag.findViewById(R.id.user_prof);
        return frag;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //pass bundle args for user to populate views
        adapter = new ProfileViewPagerAdapter(getChildFragmentManager(), getContext());
        vpProfile.setAdapter(adapter);
        vpProfile.setOffscreenPageLimit(6);
        tlProfile.setupWithViewPager(vpProfile);

        update.setOnClickListener(this);
        vpProfile.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (position <= (adapter.getCount() + 1)) {
                    update.setVisibility(View.VISIBLE);
                } else {
                    update.setVisibility(View.GONE);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        ctx = getActivity();
        token = SharedMethods.getDefaults("token", ctx);

        userData();
        user_prof.setOnClickListener(this);

    }


   private void updateAvatar(){
        updateAvatarDialog=new UpdateAvatarDialog();
       updateAvatarDialog.show(getChildFragmentManager(), "TaG");
   }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btUpdate:
                //Todo get data params from adapter.getData()
                //Call update Endpoint
                Log.e("DATA", adapter.getData());
                postUpdateData(token);
                break;
                case R.id.user_prof:
                    updateAvatar();
                break;

        }
    }



        public void postUpdateData(String token) {
        Log.e("POST_DATA", adapter.getData());
        postToServer(updateUser, token, adapter.getData(), INITIATOR_UPDATE_USER, HTTP_POST, ctx, this);
    }


    int val = 0;
    boolean transparent = true;


    @Override
    public void onResume() {
        super.onResume();
        new Thread(new Runnable() {
            @Override
            public void run() {
            }
        });


    }

    public void userData() {
        userProfileViewModel.getUserProfile();
        userProfileViewModel.userProfileResponseLiveData().observe(getViewLifecycleOwner(), userProfileResponse -> {

           try {
               if (userProfileResponse.getSuccess()) {
                   Detail userDetails = userProfileResponse.getUser().getDetail();
                   String imageUri=userProfileResponse.getUser().getAvatar();
                   Log.e("USER_AVATAR",imageUri);
                   Glide.with(requireContext()).load(imageUri).centerCrop().into(user_prof);
                   Pair<Integer, Integer> profile = showProfileProgress(userDetails);

                   int max = profile.second;
                   int progress = profile.first;
                   float perc = ((float) progress / (float) max) * 100;
                   int percentage = (int) perc;
                   Log.e("PROGRESS_profile", percentage + "-" + progress + "/" + max);
                   if (percentage > 80) {
                       tvRemark.setText(getString(R.string.all_done));
                       llProgress.setVisibility(View.GONE);
                   }

                   tvProfileProgress.setText(String.valueOf(percentage) + "%");
                   cpbProfile.setMaximum(max);
                   cpbProfile.setProgress(progress);
                   if (progress > 90) {
                       getActivity().findViewById(R.id.cvProfile).setVisibility(View.GONE);
                   } else {

                   }
//                JSONObject jObj = new JSONObject(String.valueOf(userProfileResponse));
//                adapter.setInitialData(userProfileResponse.getUser().getDetail());

               } else {

                   showSweetAlertDialogError("Something went Wrong", ctx);
               }
           } catch (Exception e) {
               e.printStackTrace();
           }
        });
    }

    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int i) {

        if (i < val) {
            if (transparent) {
                ((View) getActivity().findViewById(R.id.toolbar)).setBackgroundResource(R.color.colorSurface);
                transparent = false;
            }
        } else if (i > val) {

            if (!transparent) {
                ((View) getActivity().findViewById(R.id.toolbar)).setBackgroundResource(R.color.colorSurfaceAlpha);
                transparent = true;
            }
        }

        val = i;
    }

    @Override
    public void onDataReady(String initiator, String response) {
        switch (initiator) {
            case INITIATOR_UPDATE_USER:
                try {
                    try {
                        JSONObject jObj = new JSONObject(response);
                        if (jObj.has("errors")) {
                            String errors = jObj.getString("errors");
                            showSweetAlertDialogError(errors, ctx);
                        } else { // if no error message was found in the response,
                            boolean success = jObj.getBoolean("success");
                            if (success) { // if the request was successfuls, got forward, else show error message
                                //pass data to the viewpager fragments
                                String api_message = jObj.getString("message");
                                showSweetAlertDialogSuccess(api_message, ctx);
                                NavHostFragment.findNavController(this).navigateUp();
                            } else {
                                String api_error_message = jObj.getString("message");
                                showSweetAlertDialogError(api_error_message, ctx);
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.e(initiator, e.toString());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e(initiator, e.toString());
                }
                break;
        }
    }

    @Override
    public void onDataLoaded(String initiator, JSONArray data) {

    }

    @Override
    public void onNetworkError(String initiator, String errorCode, String errorMessage) {
        switch (initiator) {
            case INITIATOR_GETERGISTER_DATA_REG:
                try {
                    runOnUI(new Runnable() {
                        @Override
                        public void run() {
//                                    hideDialog(ctx);
                            showSweetAlertDialogError(errorMessage, ctx);
                        }
                    });
                } catch (Exception e) {
//                            hideDialog(ctx);
                    showSweetAlertDialogError(global_error_message, ctx);
                    Log.e(initiator, e.toString());
//                    Toast.makeText(ctx, "Login Error Response Exception" + e.toString(), Toast.LENGTH_LONG).show();
                }
                break;
            case INITIATOR_GETPROFILE:
                try {
                    runOnUI(new Runnable() {
                        @Override
                        public void run() {
//                                    hideDialog(ctx);
                            showSweetAlertDialogError(errorMessage, ctx);
                        }
                    });
                } catch (Exception e) {
//                            hideDialog(ctx);
                    showSweetAlertDialogError(global_error_message, ctx);
                    Log.e(initiator, e.toString());
//                    Toast.makeText(ctx, "Login Error Response Exception" + e.toString(), Toast.LENGTH_LONG).show();
                }
                break;
            case INITIATOR_UPDATE_USER:
                try {
                    runOnUI(new Runnable() {
                        @Override
                        public void run() {
//                                    hideDialog(ctx);
                            showSweetAlertDialogError(errorMessage, ctx);
                        }
                    });
                } catch (Exception e) {
//                            hideDialog(ctx);
                    showSweetAlertDialogError(global_error_message, ctx);
                    Log.e(initiator, e.toString());
//                    Toast.makeText(ctx, "Login Error Response Exception" + e.toString(), Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

    @SuppressLint("LongLogTag")
    public Pair<Integer, Integer> showProfileProgress(Detail userDetails) {
        Log.e("PROFILE_PROGRESS", new Gson().toJson(userDetails));
        Field[] fields = userDetails.getClass().getDeclaredFields();
        int total = fields.length - 1;
        int filled = 0;
        for (Field field : fields) {
            field.setAccessible(true);
            try {
                String val = String.valueOf(field.get(userDetails));
                if (val.equals("0")) {
                    Log.e("PROFILE_PROGRESS->0", val);
                } else if (val.equals("null")) {
                    Log.e("PROFILE_PROGRESS->null", val);
                } else if (!val.isEmpty()) {
                    Log.e("PROFILE_PROGRESS->notempty", val);
                    filled++;
                }
            } catch (IllegalAccessException e) {
                e.printStackTrace();
                Log.e("PROFILE_PROGRESS", e.toString());
            }
        }

        float val = filled / total;
        Log.e("PROFILE_PROGRESS", val + "-" + filled + "/" + total);
        return new Pair<Integer, Integer>(filled, total);
    }
}