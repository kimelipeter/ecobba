package com.app.ecobba.Fragments.Profile.userProfResponse

data class Transactions(
    val credit: Int,
    val debit: Int
)