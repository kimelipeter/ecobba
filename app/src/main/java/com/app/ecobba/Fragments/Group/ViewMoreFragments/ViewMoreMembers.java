package com.app.ecobba.Fragments.Group.ViewMoreFragments;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.ecobba.Models.GroupMmembers.groupMembersResponse;
import com.app.ecobba.R;
import com.app.ecobba.common.SharedMethods;
import com.app.ecobba.common.rxApi.Api;
import com.app.ecobba.common.rxApi.RxApi;
import com.app.ecobba.recyclerview_adapters.GroupMembersRecyclerAdapter;
import com.app.ecobba.recyclerview_models.GroupMembersListAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.app.ecobba.common.SharedMethods.showSweetAlertDialogError;

public class ViewMoreMembers extends Fragment implements View.OnClickListener{

    @BindView(R.id.recyclerViewMembers)    RecyclerView recyclerViewMembers;
    @BindView(R.id.tvGroupName)    TextView tvGroupName;
    @BindView(R.id.tvNoAvailableDataMembers)    TextView tvNoAvailableDataMembers;
    @BindView(R.id.progress_dialog)
    ProgressBar progress_dialog;
    String group_id,group_name;
    Context ctx;
    Bundle args;
    Fragment fragment;
    Context context;
    ViewMoreMembersAdapter adapter_member;
    private Api api;


    public ViewMoreMembers(){

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.view_more_groupmembers,container,false);
        api = new RxApi().getApi();
        //view members
        recyclerViewMembers = view.findViewById(R.id.recyclerViewMembers);
        fragment = this;
        adapter_member = new ViewMoreMembersAdapter(new ArrayList<>(), requireContext(), fragment);
        RecyclerView.LayoutManager layoutManager1 = new GridLayoutManager(requireContext(),3);
        recyclerViewMembers.setLayoutManager(layoutManager1);
        recyclerViewMembers.setAdapter(adapter_member);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ctx = getActivity();

        ButterKnife.bind(this, view);

        args = getArguments();
        assert args != null;
        group_id = args.getString("group_id");
        group_name  =args.getString("group_name");
        tvGroupName.setText(group_name);
        fetchGroupMembers(group_id);
    }

    @Override
    public void onClick(View view) {
        switch(view.getId()){

        }
    }


    private void fetchGroupMembers(String group_id) {
        progress_dialog.setVisibility(View.VISIBLE);
        Call<groupMembersResponse> call = api.getGroupMmebers(group_id);
        call.enqueue(new Callback<groupMembersResponse>() {
            @Override
            public void onResponse(Call<groupMembersResponse> call, Response<groupMembersResponse> response) {
                Log.e("_DATA_MEMBERS", "onResponse: " + response.body());
                try {
                    if (response.body().getSuccess()) {
                        progress_dialog.setVisibility(View.GONE);
                        if (response.body().getData().size() > 0) {
                            adapter_member.datalist = response.body().getData();
                            adapter_member.notifyDataSetChanged();
                        } else {
                            tvNoAvailableDataMembers.setText(getResources().getString(R.string.no_available_data));
                            tvNoAvailableDataMembers.setVisibility(View.VISIBLE);

                        }
                    } else if (!response.body().getSuccess()) {
//                        String api_error_message = "Sorry";
//                        showSweetAlertDialogError(api_error_message, context);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<groupMembersResponse> call, Throwable t) {

            }
        });
    }



}
