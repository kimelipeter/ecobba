package com.app.ecobba.Fragments.Group.Member;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.ecobba.Models.Roles.getRolesResponse;
import com.app.ecobba.Models.UpdateUserRole.UpdateUserRoleResponse;
import com.app.ecobba.Models.UserRole;
import com.app.ecobba.Models.UserRoleResponse;
import com.app.ecobba.R;
import com.app.ecobba.common.SharedMethods;
import com.app.ecobba.common.rxApi.Api;
import com.app.ecobba.common.rxApi.RxApi;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.android.material.chip.Chip;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.app.ecobba.common.SharedMethods.showSweetAlertDialogError;
import static com.app.ecobba.common.SharedMethods.showSweetAlertDialogSuccess;

public class MemberRolesBottomSheetDialog extends BottomSheetDialogFragment implements UsetRolesAdapter.CallBack, View.OnClickListener {
    UsetRolesAdapter adapter;
    RecyclerView rvRoleDescription;
    Context context;
    private Api api;
    ImageView close;
    @BindView(R.id.btUpdate)
    Button btUpdate;
    int role, user_id, group_id;


    public MemberRolesBottomSheetDialog(int user_id,int group_id) {
        this.user_id = user_id;
        this.group_id=group_id;
    }

    public int getTheme() {
        return R.style.AppBottomSheetDialogTheme;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.roles_bottom_sheet, container, false);
        ButterKnife.bind(this, view);
        context = getActivity();
        close = view.findViewById(R.id.close);
        rvRoleDescription = view.findViewById(R.id.rvRoleDescription);
        setStyle(STYLE_NORMAL, R.style.AppBottomSheetDialogTheme);
        api = new RxApi().getApi();
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        close.setOnClickListener(v -> {
            dismiss();
        });
        btUpdate.setOnClickListener(this);
        getUserRoles();
        rvRoleDescription.addItemDecoration(new DividerItemDecoration(getContext(),
                DividerItemDecoration.VERTICAL));
        adapter = new UsetRolesAdapter(new ArrayList<>(), requireContext(), this);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(requireContext());
        rvRoleDescription.setLayoutManager(layoutManager);
        rvRoleDescription.setAdapter(adapter);

    }

    private void getUserRoles() {
        Call<getRolesResponse> call = api.getRoles();
        call.enqueue(new Callback<getRolesResponse>() {
            @Override
            public void onResponse(Call<getRolesResponse> call, Response<getRolesResponse> response) {
                try {
                    if (response.body().getSuccess()) {
                        adapter.datalist = response.body().getMessage();
                        adapter.notifyDataSetChanged();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<getRolesResponse> call, Throwable t) {

            }
        });
    }

    @Override
    public void getRoleId(int role_id) {
        role = role_id;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btUpdate:
                updateRole();
                break;

        }
    }

    private void updateRole() {
        Call<UpdateUserRoleResponse> call = api.updateUserRole(role, user_id, group_id);
        call.enqueue(new Callback<UpdateUserRoleResponse>() {
            @Override
            public void onResponse(Call<UpdateUserRoleResponse> call, Response<UpdateUserRoleResponse> response) {
                try {
                    if (response.isSuccessful()) {
                        showSweetAlertDialogSuccess(response.body().getMessage(), context);
                    } else {
                        showSweetAlertDialogError("Something went wrong", context);
                    }
                    dismiss();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<UpdateUserRoleResponse> call, Throwable t) {
                showSweetAlertDialogError(t.getLocalizedMessage(), context);
                dismiss();
            }
        });
    }

}
