package com.app.ecobba.Fragments.Group.Meeting;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.ecobba.Models.GroupMeetings.GroupMeetingsResponse;
import com.app.ecobba.R;
import com.app.ecobba.common.rxApi.Api;
import com.app.ecobba.common.rxApi.RxApi;
import com.app.ecobba.recyclerview_adapters.GroupMeetingsAdapter;

import com.google.android.material.floatingactionbutton.FloatingActionButton;


import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MeetingsFragment extends Fragment implements View.OnClickListener {

    Context context;
    private RecyclerView recyclerView;
    Fragment frag;
    Bundle args;
    private Api api;
    Fragment fragment = this;


    @BindView(R.id.tvNoAvailableData)
    TextView tvNoAvailableData;

    @BindView(R.id.create)
    FloatingActionButton create;

    GroupMeetingsAdapter adapter;
    String group_id;

    public MeetingsFragment() {
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_meetings, container, false);
        ButterKnife.bind(this, view);
        context = getActivity();
        create.setOnClickListener(this);
        recyclerView = view.findViewById(R.id.recyclerView);
        recyclerView.addItemDecoration(new DividerItemDecoration(getContext(),
                DividerItemDecoration.VERTICAL));
        adapter = new GroupMeetingsAdapter(new ArrayList<>(), requireContext(), fragment);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(requireContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);

        api = new RxApi().getApi();
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        context = getActivity();
        SharedPreferences sh = getContext().getSharedPreferences("PREFERENCE", Context.MODE_PRIVATE);
        group_id = sh.getString("group_id", "");
        Log.e("GROUP_ID_VAL", group_id);
        fetchGroupSettings(group_id);

    }

    private void fetchGroupSettings(String group_id) {
        Call<GroupMeetingsResponse> call = api.fetchGroupMeetings(group_id);
        call.enqueue(new Callback<GroupMeetingsResponse>() {
            @Override
            public void onResponse(Call<GroupMeetingsResponse> call, Response<GroupMeetingsResponse> response) {
                try {
                    if (response.body().getSuccess()) {
                        if (response.body().getMeetings().size() > 0) {
                            adapter.datalist = response.body().getMeetings();
                            adapter.notifyDataSetChanged();
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<GroupMeetingsResponse> call, Throwable t) {

            }
        });

    }


    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.create:
                Bundle args = new Bundle();
                args.putString("group_id", group_id);
                NavHostFragment.findNavController(this).navigate(R.id.fragmentCreateMeeting, args);
                break;
        }
    }


}
