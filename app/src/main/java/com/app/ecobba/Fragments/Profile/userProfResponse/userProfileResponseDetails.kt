package com.app.ecobba.Fragments.Profile.userProfResponse

data class userProfileResponseDetails(
    val message: String,
    val success: Boolean,
    val user: User
)