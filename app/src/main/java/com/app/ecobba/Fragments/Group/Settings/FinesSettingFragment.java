package com.app.ecobba.Fragments.Group.Settings;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.app.ecobba.Dialogs.AddFineDialog;

import com.app.ecobba.Fragments.Group.Settings.Adapters.FinesSettingsAdapter;

import com.app.ecobba.Models.MeetingsSettingsFines.Data;

import com.app.ecobba.Models.MeetingsSettingsFines.finesSettingsResponse;

import com.app.ecobba.Models.UserProfile.Detail;
import com.app.ecobba.R;
import com.app.ecobba.ViewModel.UserProfileViewModel;
import com.app.ecobba.common.SharedMethods;
import com.app.ecobba.common.rxApi.Api;
import com.app.ecobba.common.rxApi.RxApi;
import com.app.ecobba.recyclerview_adapters.GenericAdapter;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


import static com.app.ecobba.common.AppConstantsKt.INITIATOR_STORE_FINE_SETTING;;

import static com.app.ecobba.common.SharedMethods.global_error_message;

import static com.app.ecobba.common.SharedMethods.runOnUI;
import static com.app.ecobba.common.SharedMethods.showSweetAlertDialogError;
import static com.app.ecobba.common.SharedMethods.showSweetAlertDialogSuccess;

public class FinesSettingFragment extends Fragment implements View.OnClickListener, SharedMethods.NetworkObjectListener, GenericAdapter.AdapterInterface<Data, FinesSettingFragment.FinesViewHolder> {

    @BindView(R.id.fabAddFine)
    FloatingActionButton addFine;

    @BindView(R.id.rvFines)
    RecyclerView rvFines;

    String token, api_tail;
    String group_id, setting_id, setting_name = "Fines";
    Context context;
    Bundle args;
    Fragment fragment;

    FinesSettingsAdapter adapter;
    private Api api;
    @BindView(R.id.tvNoAvailableData)
    TextView tvNoAvailableData;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    SwipeRefreshLayout mSwipeRefreshLayout;
    UserProfileViewModel userProfileViewModel;
    String user_currency;

    public FinesSettingFragment() {

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_group_setting_fines, container, false);
        ButterKnife.bind(this, view);
        mSwipeRefreshLayout = view.findViewById(R.id.swipe_container);
        context = getContext();
        userProfileViewModel = new ViewModelProvider(getActivity()).get(UserProfileViewModel.class);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        addFine.setOnClickListener(this);
        token = SharedMethods.getDefaults("token", context);
        args = getArguments();
        assert args != null;
        api_tail = args.getString("args");
        group_id = args.getString("group_id");
        setting_id = args.getString("id");
        fragment = this;
        api = new RxApi().getApi();
        rvFines.addItemDecoration(new DividerItemDecoration(getContext(),
                DividerItemDecoration.VERTICAL));
        adapter = new FinesSettingsAdapter(new ArrayList<>(), requireContext(), fragment, "");
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(requireContext());
        rvFines.setLayoutManager(layoutManager);
        rvFines.setAdapter(adapter);
        getData(setting_id, group_id);
        fetchUserProfile();
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // Toast.makeText(ctx,"Refreshing", Toast.LENGTH_LONG).show();
                getData(setting_id, group_id);
                mSwipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary,
                        android.R.color.holo_green_dark,
                        android.R.color.holo_orange_dark,
                        android.R.color.holo_blue_dark);

                mSwipeRefreshLayout.setRefreshing(false);
            }
        });
    }

    private void fetchUserProfile() {
        userProfileViewModel.getUserProfile();
        userProfileViewModel.userProfileResponseLiveData().observe(getViewLifecycleOwner(), userProfileResponse -> {
            try {
                if (userProfileResponse.getSuccess()) {
                    Detail userDetails = userProfileResponse.getUser().getDetail();
                    String name = userProfileResponse.getUser().getName();

                    user_currency = userProfileResponse.getUser().getWallet().getCurrency().getPrefix();
                    Log.e("MUDHDGDGD", user_currency);

                    user_currency = userProfileResponse.getUser().getWallet().getCurrency().getPrefix();
                    adapter.user_currency = user_currency;
                    adapter.notifyDataSetChanged();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    private void getData(String setting_id, String group_id) {
        Call<finesSettingsResponse> call = api.getFinesSettings(setting_id, group_id);
        call.enqueue(new Callback<finesSettingsResponse>() {
            @Override
            public void onResponse(Call<finesSettingsResponse> call, Response<finesSettingsResponse> response) {
                progressBar.setVisibility(View.GONE);
                if (response.body().getSuccess()) {
                    if (response.body().getCurrent_settings().getData().size() > 0) {
                        adapter.datalist = response.body().getCurrent_settings().getData();
                        adapter.notifyDataSetChanged();
                    } else {
                        progressBar.setVisibility(View.GONE);
                        tvNoAvailableData.setText(getResources().getString(R.string.no_available_data));
                        tvNoAvailableData.setVisibility(View.VISIBLE);
                    }
                }
            }

            @Override
            public void onFailure(Call<finesSettingsResponse> call, Throwable t) {

            }
        });
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.fabAddFine:
                AddFineDialog addFineDialog = new AddFineDialog(setting_id, group_id,user_currency);
                addFineDialog.show(getActivity().getSupportFragmentManager(), "FINE_DIALOG");
                break;
        }
    }

    @Override
    public void onDataReady(String initiator, String response) {
        switch (initiator) {

            case INITIATOR_STORE_FINE_SETTING:
                try {
                    runOnUI(() -> {
                        Log.e("DEPOSIT RESPONSE", "Response is " + response);
//                                    hideDialog(ctx);
                        JSONObject jObj = null;
                        try {
                            jObj = new JSONObject(response);
                            if (jObj.has("errors")) {
                                String errors = jObj.getString("errors");
                                showSweetAlertDialogError(errors, context);
                            } else { // if no error message was found in the response,
                                boolean success = jObj.getBoolean("success");
                                if (success) { // if the request was successfuls, got forward, else show error message
                                    String message = jObj.getString("message");
                                    showSweetAlertDialogSuccess(message, context);
//                                    getData();
                                } else {
                                    String api_error_message = jObj.getString("message");
                                    showSweetAlertDialogError(api_error_message, context);
                                }
                            }
                        } catch (JSONException e) {
                            Log.e("ExceptionLOGIN-DATA", e.toString());
                            showSweetAlertDialogError(global_error_message, context);
                            e.printStackTrace();
                        }
                    });
                } catch (Exception e) {
//                            hideDialog(ctx);
                    Log.e("Exception- HOME", e.toString());
                    showSweetAlertDialogError(global_error_message, context);
//                    Toast.makeText(ctx, "Login Data Response Exception" + e.toString(), Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

    @Override
    public void onDataLoaded(String initiator, JSONArray data) {

    }

    @Override
    public void onNetworkError(String initiator, String errorCode, String errorMessage) {
        switch (initiator) {
            case INITIATOR_STORE_FINE_SETTING:
                try {
                    runOnUI(new Runnable() {
                        @Override
                        public void run() {
//                                    hideDialog(ctx);
                            showSweetAlertDialogError(errorMessage, context);
                            NavHostFragment.findNavController(fragment).navigateUp();
                        }
                    });
                } catch (Exception e) {
//                            hideDialog(ctx);
                    showSweetAlertDialogError(global_error_message, context);
                    Log.e("initiator", e.toString());
                    NavHostFragment.findNavController(fragment).navigateUp();
//                    Toast.makeText(ctx, "Login Error Response Exception" + e.toString(), Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

    @Override
    public FinesViewHolder createViewHolder(ViewGroup parent, int viewType) {
        return new FinesViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_fine, parent, false));
    }

    @Override
    public void bindViewHolder(FinesViewHolder viewholder, Data datum, int position) {

        FinesViewHolder holder = (FinesViewHolder) viewholder;

        holder.name.setText(datum.getName());
        holder.amount.setText(datum.getAmount());

    }

    class FinesViewHolder extends RecyclerView.ViewHolder {
        TextView name, amount;

        public FinesViewHolder(@NonNull View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.tvFineName);
            amount = itemView.findViewById(R.id.tvFineAmount);
        }
    }
}
