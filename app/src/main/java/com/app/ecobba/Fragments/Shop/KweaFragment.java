package com.app.ecobba.Fragments.Shop;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.app.ecobba.Models.KweaItemsShop.Data;
import com.app.ecobba.Models.KweaItemsShop.Image;
import com.app.ecobba.Models.KweaItemsShop.KweaItemsShopResponse;
import com.bumptech.glide.Glide;
import com.app.ecobba.Activities.KweaActivity;
import com.app.ecobba.Models.Responses.Shop;
import com.app.ecobba.R;
import com.app.ecobba.Utils.BaseFragment;
import com.app.ecobba.Utils.RxUtils;
import com.app.ecobba.common.SharedMethods;
import com.app.ecobba.common.rxApi.ApiService;
import com.app.ecobba.common.rxApi.RxApi;
import com.app.ecobba.recyclerview_adapters.GenericAdapter;
import com.app.ecobba.recyclerview_models.KweaRes.Datum;
import com.app.ecobba.recyclerview_models.KweaRes.KweaShopResponse;
import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.google.gson.Gson;
import com.snov.timeagolibrary.PrettyTimeAgo;

import org.json.JSONArray;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;

import static com.app.ecobba.common.ApisKtKt.kwea_items;
import static com.app.ecobba.common.AppConstantsKt.INITIATOR_FETCH_KWEA_ITEMS;
import static com.app.ecobba.common.SharedMethods.HTTP_GET;
import static com.app.ecobba.common.SharedMethods.getDefaults;
import static com.app.ecobba.common.SharedMethods.getFromServer;
import static com.app.ecobba.common.SharedMethods.global_error_message;
import static com.app.ecobba.common.SharedMethods.runOnUI;
import static com.app.ecobba.common.SharedMethods.showSweetAlertDialogError;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link KweaFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class KweaFragment extends BaseFragment implements View.OnClickListener,
        SharedMethods.NetworkObjectListener, GenericAdapter.AdapterInterface<Data, KweaFragment.ShopVieHolder> {
    private static final String TAG = "KWEA_FRAGMENT";
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private String token = "";
    private RecyclerView rvShops;
    private SearchView searchView;
    public TextView owner_id;
    private ProgressDialog loadingBar;
    long datepower;


    private RecyclerView.ViewHolder viewHolder;
    private GenericAdapter adapter;
    private List<Shop> mShops;
    List<Shop> filteredShopList;
    //    RequestOptions options ;
    private Context mContext;
    @BindViews({R.id.my_shop,R.id.create_shop})
    List<FloatingActionButton> options;
    ApiService apiService = new RxApi().getApiService();
    com.google.android.material.floatingactionbutton.FloatingActionButton fab1, fab2;
    @BindView(R.id.tvNoAvailableData)
    TextView tvNoAvailableData;
    SwipeRefreshLayout mSwipeRefreshLayout;

    public KweaFragment() {
        // Required empty public constructor
    }


    public static KweaFragment newInstance(String param1, String param2) {
        KweaFragment fragment = new KweaFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public static Fragment newInstance() {
        return null;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mContext = requireContext();
        View view = inflater.inflate(R.layout.fragment_kwea, container, false);
        ButterKnife.bind(this, view);
        mSwipeRefreshLayout=view.findViewById(R.id.swipe_container);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                Toast.makeText(requireActivity(),"Refreshing", Toast.LENGTH_LONG).show();
                loadRecyclerViewData();
                mSwipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary,
                        android.R.color.holo_green_dark,
                        android.R.color.holo_orange_dark,
                        android.R.color.holo_blue_dark);

                mSwipeRefreshLayout.setRefreshing(false);
            }
        });
        options.get(0).setImageResource(R.drawable.ic_package3);
        options.get(1).setImageResource(R.drawable.ic_meeting);

        for (FloatingActionButton floatingActionButton:options){
            floatingActionButton.setOnClickListener(this);
        }
        loadingBar = new ProgressDialog(getContext());
        /*search  kwea items*/
        /*  Associate searchable configuration with the SearchView*/
       searchView = (SearchView) view.findViewById(R.id.simpleSearchView);
        searchView.setMaxWidth(Integer.MAX_VALUE);
        /* listening to search query text change*/
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                /*  filter recycler view when query submitted*/
                loadingBar.setTitle("");
                loadingBar.setMessage("searching.....");
                loadingBar.show();
                apiService.kweaItemSearch(query).compose(RxUtils.INSTANCE.applyObservableIoSchedulers())
                        .doOnNext(kweaShopResponse -> {
                            // handle response here
                            int count_results = kweaShopResponse.getData().size();
                            if (count_results > 0) {
                                Log.e(TAG, "search_results_is:" + kweaShopResponse);
                                kweaShopResponse.getData();
                                adapter.setData(kweaShopResponse.getData());
                                loadingBar.dismiss();
                            } else {
                                // No data found
                                Toast toast = Toast.makeText(getContext(), R.string.not_available, Toast.LENGTH_LONG);
                                toast.setGravity(Gravity.CENTER, 0, 0);
                                toast.show();
//                                tvNoAvailableData.setText(getResources().getString(R.string.no_available_data));
//                                tvNoAvailableData.setVisibility(View.VISIBLE);
                                loadingBar.dismiss();
                            }


                        })
                        .doOnError(error -> {
                            //get an error here
                            showSweetAlertDialogError(global_error_message, mContext);
                        })
                        .doOnComplete(() -> {

                        })
                        .subscribe();

                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                // filter recycler view when text is changed
                getFilter().filter(query);
                return false;
            }
        });

        rvShops = (RecyclerView) view.findViewById(R.id.recyclerviewid);
        adapter = new GenericAdapter<Data, ShopVieHolder>(this, rvShops);
        return view;

    }

    private void loadRecyclerViewData() {
        token = getDefaults("token", requireContext());
        Map<String, String> params = new HashMap();
        getFromServer(kwea_items, token, params, INITIATOR_FETCH_KWEA_ITEMS, HTTP_GET, requireContext(), this);

    }

    private void hideKeyboard() {

        View view = requireActivity().getCurrentFocus();

        if (view != null) {
            InputMethodManager inputManager = (InputMethodManager) requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    //filter method
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    filteredShopList = mShops;
                } else {
                    List filteredList = new ArrayList<>();
                    for (Shop row : mShops) {
                        //filter data
                        if (row.getName().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        }
                    }

                    filteredShopList = filteredList;
                }
                adapter.notifyDataSetChanged();

                FilterResults filterResults = new FilterResults();
                filterResults.values = filteredShopList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                filteredShopList = (ArrayList) filterResults.values;
                adapter.notifyDataSetChanged();

            }
        };

    }


    @Override
    public void onResume() {
        super.onResume();
        hideKeyboard();
        // make api call to get list of shop items
        token = getDefaults("token", requireContext());
        Map<String, String> params = new HashMap();
        getFromServer(kwea_items, token, params, INITIATOR_FETCH_KWEA_ITEMS, HTTP_GET, requireContext(), this);
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mContext = getActivity();
        ButterKnife.bind(this, view);


    }

    @Override
    public void onDataReady(String initiator, String response) {

        switch (initiator) {
            case INITIATOR_FETCH_KWEA_ITEMS: {
                // this is where the response is handled
                // Log.e(TAG,response);
                Log.e("RESPONSE_KWEA", "Response is " + response);
                try {


                    KweaItemsShopResponse kweaShopResponse=new Gson().fromJson(response,KweaItemsShopResponse.class);
                    Log.e(TAG, "converted :" + kweaShopResponse.toString());
                    kweaShopResponse.getData();
                    adapter.addData(kweaShopResponse.getData());

                } catch (Exception e) {
                    // Log.e(TAG,"Shop list conversion failed",e);
                    Log.e("ExceptionLOGIN-DATA", e.toString());
                    showSweetAlertDialogError(global_error_message, mContext);
                    e.printStackTrace();
                }
            }
            default: {
                // Not supported
            }
        }
    }

    @Override
    public void onDataLoaded(String initiator, JSONArray data) {
        Log.e(TAG, "loaded :" + data.toString());
    }

    @Override
    public void onNetworkError(String initiator, String errorCode, final String errorMessage) {
        switch (initiator) {
            case INITIATOR_FETCH_KWEA_ITEMS:
                try {
                    runOnUI(new Runnable() {
                        @Override
                        public void run() {
//                                    hideDialog(ctx);
                            showSweetAlertDialogError(errorMessage, mContext);
                        }
                    });
                } catch (Exception e) {
//                            hideDialog(mContext);
                    showSweetAlertDialogError(global_error_message, mContext);
                    Log.e("Exception- HOME", e.toString());
//                    Toast.makeText(ctx, "Login Error Response Exception" + e.toString(), Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

    @Override
    public ShopVieHolder createViewHolder(ViewGroup parent, int viewType) {


        return new ShopVieHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.shop_list_items, parent, false));

    }

    @Override
    public void bindViewHolder(ShopVieHolder viewholder, Data shop, int position) {


        ShopVieHolder holder = (ShopVieHolder) viewholder;

        holder.action_shops.setAnimation(AnimationUtils.loadAnimation(mContext, R.anim.fade_transition_animation));
        holder.tv_productname.setText(shop.getItem_name());
        holder.item_shop_address.setText(shop.getShop().getShop_address());
        holder.tv_price.setText(String.valueOf(shop.getItem_price()));
        String item_price;
        item_price = String.valueOf(shop.getItem_price());
        Log.e("item_price_is", item_price);
        String owner=shop.getShop().getUser().getName()+" \n"+shop.getShop().getUser().getOther_names();
        holder.tv_shopname.setText(owner);
        holder.item_currency.setText(shop.getItem_currency());
//        holder.tv_owner_id.setText(String.valueOf(shop.getShop().getUser_id()));
        String owner_id;
        owner_id = String.valueOf(shop.getShop().getUser_id());
        Log.e("Owner_id_is", owner_id);

        String UserProfile=shop.getShop().getUser().getAvatar();
        Log.e("yyyuvofciuuuvuvuv",UserProfile);
        String date_posted=shop.getCreated_at();
        Log.e("DATE_POSTED_IS",date_posted);

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.ENGLISH);
        sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
        Date testDate = null;
        try {
            testDate = sdf.parse(date_posted);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        Log.e("BEg", String.valueOf(datepower));
        datepower = testDate.getTime();
        Log.e("Datw", String.valueOf(+datepower));
        PrettyTimeAgo timeAgo = new PrettyTimeAgo();
        String result = timeAgo.getTimeAgo(datepower);
       // Log.e("Time", result);
//  load image from the internet using Glide
        Glide.with(requireActivity())
                .load(UserProfile)
                .error(R.drawable.ic_shop).override(40, 40)
                .fitCenter().into(holder.profile_image);

        //  load image from the internet using Glide
        Glide.with(requireActivity())
                .load(shop.getImages().get(0).getImage())
                .error(R.drawable.ic_shop).override(200, 200)
                .fitCenter().into(holder.product_img);
        holder.view_container.setOnClickListener((view -> {
            Intent intent = new Intent(mContext, KweaActivity.class);
            //Shop shop=mShops.get(position);
            intent.putParcelableArrayListExtra("img",(ArrayList<Image>) shop.getImages());
            intent.putExtra("productname", shop.getItem_name());
            intent.putExtra("price", item_price);
            intent.putExtra("description", shop.getItem_description());
            intent.putExtra("name", shop.getShop().getShop_name());
            intent.putExtra("item_currency", shop.getItem_currency());
            intent.putExtra("shop_address", shop.getShop().getShop_address());
            intent.putExtra("user_id", owner_id);
            intent.putExtra("owner", shop.getShop().getUser().getName()+" "+shop.getShop().getUser().getOther_names());
            intent.putExtra("profile", UserProfile);
            intent.putExtra("result", result);
            mContext.startActivity(intent);
        }));


    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.my_shop:
                NavHostFragment.findNavController(this).navigate(R.id.shopDashboardFragment);
                break;
            case R.id.create_shop:
                NavHostFragment.findNavController(this).navigate(R.id.addShopFragment);
                break;
        }

    }

    class ShopVieHolder extends RecyclerView.ViewHolder {
        TextView tv_productname;
        TextView tv_price;
        TextView tv_shopname;
        TextView tv_owner_id;
        TextView item_currency;
        TextView item_shop_address;
        ImageView product_img,profile_image;
        androidx.constraintlayout.widget.ConstraintLayout view_container;
        LinearLayout action_shops;

        public ShopVieHolder(@NonNull View itemView) {
            super(itemView);
            action_shops = itemView.findViewById(R.id.action_shops);
            view_container = itemView.findViewById(R.id.shop_container);
            tv_productname = itemView.findViewById(R.id.productname);
            tv_price = itemView.findViewById(R.id.price);
            tv_shopname = itemView.findViewById(R.id.shopname);
            product_img = itemView.findViewById(R.id.thumbnail_image);
            item_currency = itemView.findViewById(R.id.item_currency);
            item_shop_address=itemView.findViewById(R.id.shop_adress);
//            tv_owner_id = itemView.findViewById(R.id.owner_id);
            profile_image= itemView.findViewById(R.id.profile_image);

        }
    }

    @Override
    public void onPause() {
        super.onPause();
        searchView.clearFocus();
    }
}