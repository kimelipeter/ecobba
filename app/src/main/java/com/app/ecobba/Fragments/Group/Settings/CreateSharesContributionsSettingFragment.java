package com.app.ecobba.Fragments.Group.Settings;

import android.app.TimePickerDialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;


import com.app.ecobba.Models.GroupWallet.AdapaySource;
import com.app.ecobba.Models.GroupWallet.groupWalletResponse;

import com.app.ecobba.Models.LendingData.Repaymentplan;
import com.app.ecobba.Models.LendingData.lendingDataResponse;
import com.app.ecobba.Models.MeetingsSettingsData.Data;
import com.app.ecobba.Models.MeetingsSettingsData.meetingsSettingsResponse;

import com.app.ecobba.Models.adapay.adapaySourcesResponse;
import com.app.ecobba.Models.adapay.telcos.telcosResponse;
import com.app.ecobba.R;
import com.app.ecobba.common.SharedMethods;
import com.app.ecobba.common.rxApi.Api;
import com.app.ecobba.common.rxApi.RxApi;
import com.google.android.material.chip.Chip;
import com.jaredrummler.materialspinner.MaterialSpinner;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.app.ecobba.common.ApisKtKt.storeHisaContributionSettings;
import static com.app.ecobba.common.AppConstantsKt.INITIATOR_STORE_HISA_SETTING;
import static com.app.ecobba.common.AppConstantsKt.INITIATOR_STORE_JAMII_SETTING;

import static com.app.ecobba.common.SharedMethods.HTTP_POST;
import static com.app.ecobba.common.SharedMethods.global_error_message;

import static com.app.ecobba.common.SharedMethods.postToServer;
import static com.app.ecobba.common.SharedMethods.runOnUI;
import static com.app.ecobba.common.SharedMethods.showSweetAlertDialogError;
import static com.app.ecobba.common.SharedMethods.showSweetAlertDialogSuccess;

public class CreateSharesContributionsSettingFragment extends Fragment implements View.OnClickListener, SharedMethods.NetworkObjectListener, MaterialSpinner.OnItemSelectedListener {

    @BindView(R.id.btnSharesApply)
    Chip btnSharesApply;

    @BindView(R.id.tiName)
    EditText tiName;
    @BindView(R.id.tvMinimumBought)
    EditText tvMinimumBought;
    @BindView(R.id.tvMaxBought)
    EditText tvMaxBought;
    @BindView(R.id.tvValuePerShare)
    EditText tvValuePerShare;
    @BindView(R.id.tvFine)
    EditText tvFine;
    @BindView(R.id.tvTime)
    EditText tvTime;
    Spinner spinner;
    @BindView(R.id.tiContributionStartTime)
    TextView tiContributionStartTime;
    @BindView(R.id.tvYes)
    Chip tvYes;
    @BindView(R.id.tvNo)
    Chip tvNo;

    @BindView(R.id.tiContributionDay)
    Spinner tiContributionDay;
    @BindView(R.id.llAttachMeeting)
    LinearLayout llAttachMeeting;

    @BindView(R.id.tiContributionFrequency)
    Spinner tiContributionFrequency;
    @BindView(R.id.repaymentPlan)
    MaterialSpinner repaymentPlan;

    private Api api;

    @BindView(R.id.mainFrequencyLayout)
    ConstraintLayout mainFrequencyLayout;

    ColorDrawable popupColor;
    Fragment fragment;
    Context ctx;
    Bundle args;
    String token, api_tail;
    String fine_shares, group_id, maximum, minimum, period, setting_id, setting_name, value;
    String fine_jamii, period_jamii, jamii_setting_name = "contributions", jamii_setting_id, value_jamii;

    String meeting_index = "", meeting_name, meeting_time, time;
    ArrayAdapter arrayAdapter;
    List<String> repayment_plans;
    String con_frequency = "0", repayment_plan = "0";
    LinearLayout linearLayoutMobile, llLayoutMobileMpesa, linearLayoutBank;
    ;

    List<String> ada_pay_ids, telcos_adapay;
    String _msisdn, type, telco, selected_type, selected_provider, day;
    Spinner msisdn;

    Spinner payMethod, provider, contributionDay;
    String _group_id, _type, _telco, _setting_id, ada_id = "", _name, _amount, _user_currency;

    public CreateSharesContributionsSettingFragment() {

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View frag = inflater.inflate(R.layout.fragment_group_setting_contributions, container, false);
        ButterKnife.bind(this, frag);
        ctx = getContext();
        fragment = this;
        spinner = frag.findViewById(R.id.meetings);
        payMethod = frag.findViewById(R.id.payMethod);
        provider = frag.findViewById(R.id.provider);
        contributionDay = frag.findViewById(R.id.contributionDay);

        msisdn = frag.findViewById(R.id.msisdn);
        linearLayoutMobile = frag.findViewById(R.id.linearLayoutMobile);
        llLayoutMobileMpesa = frag.findViewById(R.id.llLayoutMobileMpesa);
        linearLayoutBank = frag.findViewById(R.id.linearLayoutBank);
        api = new RxApi().getApi();
        return frag;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        token = SharedMethods.getDefaults("token", ctx);

        args = getArguments();
        assert args != null;
        api_tail = args.getString("args");
        group_id = args.getString("group_id");
        Log.e("GROUP_ID_IS", group_id);
        setting_id = args.getString("id");
//        setting_id = "1";
        Log.e("SETTING_ID_IS", setting_id);
        btnSharesApply.setOnClickListener(this);
        tvYes.setOnClickListener(this);
        tvNo.setOnClickListener(this);
        tvTime.setOnClickListener(this);
        ctx = getContext();
        getMeetingsData(setting_id, group_id);
        fetchFormDropDownData();
        fetchAdaPaySourceType();
        fetchAdaPayTelcos();
        fetchUserPaymentSoucers(group_id);
        mainFrequencyLayout.setVisibility(View.VISIBLE);
        repaymentPlan.setOnItemSelectedListener(this);
        repaymentPlan.getPopupWindow().setBackgroundDrawable(popupColor);
        contributionDay.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                day = (String) parent.getItemAtPosition(position);

//                _msisdn = sources.get(position).getMsisdn();
//                Log.e("PHONE_NUMBER", _msisdn);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


    }


    private boolean validateFormFields() {

        boolean valid = true;
        setting_name = tiName.getText().toString();
        minimum = tvMinimumBought.getText().toString();
        maximum = tvMaxBought.getText().toString();
        value = tvValuePerShare.getText().toString();
        fine_shares = tvFine.getText().toString();
        if (setting_name.isEmpty()) {
            tiName.setError(getResources().getString(R.string.emptyField));
            valid = false;
        }
        if (minimum.isEmpty()) {
            tvMinimumBought.setError(getResources().getString(R.string.emptyField));
            valid = false;
        }
        if (maximum.isEmpty()) {
            tvMaxBought.setError(getResources().getString(R.string.emptyField));
            valid = false;
        }
        if (value.isEmpty()) {
            tvValuePerShare.setError(getResources().getString(R.string.emptyField));
            valid = false;
        }
        if (fine_shares.isEmpty()) {
            tvFine.setError(getResources().getString(R.string.emptyField));
            valid = false;
        }

        if (repayment_plan.equals("0")) {
            valid = false;
            Toast.makeText(ctx, getResources().getString(R.string.fill_in_all_fields), Toast.LENGTH_LONG).show();
        }

        return valid;
    }


    private void postShares() {

        String params = "{ \"setting_name\":" + '"' + setting_name + '"' +
                ", \"minimum\": " + '"' + minimum + '"' +
                ", \"maximum\": " + '"' + maximum + '"' +
//                ", \"value\": " + '"' + value + '"' +
//                " \"period\": " + '"' + repayment_plan + '"' +
                ", \"value\": " + '"' + value + '"' +
                ", \"period\": " + '"' + repayment_plan + '"' +
                ", \"day\": " + '"' + day + '"' +
                ", \"time\": " + '"' + time + '"' +
                ", \"fine_shares\": " + '"' + fine_shares + '"' +
                ", \"group_id\": " + '"' + group_id + '"' +
                ", \"setting_id\": " + '"' + setting_id + '"' +
                ", \"meeting_index\": " + '"' + meeting_index + "\"," +
                "\"payout_source\": {"
                + "\"id\": \"" + ada_id + "\","
                + "\"method\": \"" + selected_type + "\","
                + "\"telco\": \"" + selected_provider + "\","
                + "\"msisdn\": \"" + _msisdn + "\"" + "}" +
                "}\n";

        Log.e("POST_DATA_PARAMS", params);
        postToServer(storeHisaContributionSettings, token, params, INITIATOR_STORE_HISA_SETTING, HTTP_POST, ctx, this);

    }

    public void fetchAdaPaySourceType() {

        Call<adapaySourcesResponse> call = api.fetchAdapaySources();
        call.enqueue(new Callback<adapaySourcesResponse>() {
            @Override
            public void onResponse(Call<adapaySourcesResponse> call, Response<adapaySourcesResponse> response) {

                try {
                    if (response.body().getSuccess()) {
                        String mobile = response.body().getAdapay_source_types().getMobile_money();
                        String card = response.body().getAdapay_source_types().getCard_hosted();
                        String bank_transfer = response.body().getAdapay_source_types().getBank_transfer();

                        ada_pay_ids = new ArrayList<>();
                        ada_pay_ids.add(0, getResources().getString(R.string.please_select_type));
                        ada_pay_ids.add(1, mobile);
                        ada_pay_ids.add(2, card);
                        ada_pay_ids.add(3, bank_transfer);

                        ArrayAdapter<String> adapter =
                                new ArrayAdapter<String>(ctx, android.R.layout.simple_spinner_dropdown_item, ada_pay_ids);
                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                        payMethod.setAdapter(adapter);

                        payMethod.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                                String type = (String) parent.getItemAtPosition(position);

                                selected_type = "";
                                if (type.equals("Mobile Money")) {
                                    selected_type = "mobile_money";
                                    linearLayoutMobile.setVisibility(View.VISIBLE);
//                                            linearLayoutBank.setVisibility(View.GONE);

                                }
//                                        else if (type.equals("Master/Visa Card")){
//                                            selected_type = "card_hosted";
//                                            linearLayoutBank.setVisibility(View.GONE);
//                                            linearLayoutMobile.setVisibility(View.GONE);
//                                        }
//                                        else if (type.equals("Bank Transfer")){
//                                            selected_type = "bank_transfer";
//                                            linearLayoutBank.setVisibility(View.GONE);
//                                            linearLayoutMobile.setVisibility(View.GONE);
//                                        }
                                else {
//
                                    //    linearLayoutBank.setVisibility(View.GONE);
                                    linearLayoutMobile.setVisibility(View.GONE);
                                }
                                Log.e("Selected_type", String.valueOf(type));
                                Log.e("Selected_type2", selected_type);
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {

                            }

                        });
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }


            }

            @Override
            public void onFailure(Call<adapaySourcesResponse> call, Throwable t) {

            }
        });

    }

    private void fetchUserPaymentSoucers(String group_id) {
        Call<groupWalletResponse> call = api.getGroupWalletPaymentSources(group_id);
        call.enqueue(new Callback<groupWalletResponse>() {
            @Override
            public void onResponse(Call<groupWalletResponse> call, Response<groupWalletResponse> response) {
                try {
                    if (response.body().getSuccess()) {
                        if (response.body().getAdapay_sources().size() > 0) {

                            List<AdapaySource> sources = response.body().getAdapay_sources();
                            ArrayList<String> availables = new ArrayList<>();
                            // availables.add(0, getResources().getString(R.string.please_select_contribution));
                            for (int item = 0; item < sources.size(); item++) {
                                availables.add(sources.get(item).getMsisdn());
                            }
                            arrayAdapter = new ArrayAdapter(ctx, android.R.layout.simple_spinner_dropdown_item, availables);
                            arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            msisdn.setAdapter(arrayAdapter);
                            msisdn.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                    String item_resource_type = (String) parent.getItemAtPosition(position);

                                    _msisdn = sources.get(position).getMsisdn();
                                    Log.e("PHONE_NUMBER", _msisdn);

                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> parent) {

                                }
                            });
                        }

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }


            }

            @Override
            public void onFailure(Call<groupWalletResponse> call, Throwable t) {

            }
        });
    }

    public void fetchAdaPayTelcos() {
        Call<telcosResponse> call = api.fetchAdapayTelcos();
        call.enqueue(new Callback<telcosResponse>() {
            @Override
            public void onResponse(Call<telcosResponse> call, Response<telcosResponse> response) {
                try {
                    if (response.body().getSuccess()) {
                        String telcos = response.body().getAdapay_telcos().getSafaricom_ke();
                        telcos_adapay = new ArrayList<>();
                        telcos_adapay.add(0, getResources().getString(R.string.please_select_type));
                        telcos_adapay.add(1, telcos);
                        ArrayAdapter<String> adapter =
                                new ArrayAdapter<String>(ctx, android.R.layout.simple_spinner_dropdown_item, telcos_adapay);
                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        provider.setAdapter(adapter);
                        provider.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                String type = (String) parent.getItemAtPosition(position);
                                selected_provider = "";
                                if (type.equals("Mpesa")) {
                                    llLayoutMobileMpesa.setVisibility(View.VISIBLE);
                                    selected_provider = "safaricom_ke";
                                } else {
                                    llLayoutMobileMpesa.setVisibility(View.GONE);
                                }
                                Log.e("Selected_telcos", String.valueOf(type));
                                Log.e("Selected_telcos2", selected_provider);
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {

                            }
                        });


                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<telcosResponse> call, Throwable t) {
                t.getLocalizedMessage();

            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnSharesApply:
                if (validateFormFields()) {
                    postShares();
                }
                break;

            case R.id.tvYes:
                Toast toast = Toast.makeText(ctx, "Clicked Yes", Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
                llAttachMeeting.setVisibility(View.VISIBLE);
                break;
            case R.id.tvNo:
                Toast toasts = Toast.makeText(ctx, "Clicked No", Toast.LENGTH_LONG);
                toasts.setGravity(Gravity.CENTER, 0, 0);
                toasts.show();
                llAttachMeeting.setVisibility(View.GONE);
                break;
            case R.id.tvTime:
                showHourPicker();
                break;

        }
    }

    public void showHourPicker() {
        final Calendar myCalender = Calendar.getInstance();
        int hour = myCalender.get(Calendar.HOUR_OF_DAY);
        int minute = myCalender.get(Calendar.MINUTE);


        TimePickerDialog.OnTimeSetListener myTimeListener = new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                if (view.isShown()) {
                    myCalender.set(Calendar.HOUR_OF_DAY, hourOfDay);
                    myCalender.set(Calendar.MINUTE, minute);

                    time = hourOfDay + ":" + minute;

                    try {
                        final SimpleDateFormat sdf = new SimpleDateFormat("H:mm");
                        final Date dateObj = sdf.parse(time);
                        //meeting_time = new SimpleDateFormat("hh:mm aa").format(dateObj);

                        meeting_time = new SimpleDateFormat("hh:mm").format(dateObj);
                        tvTime.setText(time);

                        Toast.makeText(ctx, meeting_time, Toast.LENGTH_LONG).show();

                    } catch (final ParseException e) {
                        e.printStackTrace();
                    }

                }
            }
        };
        TimePickerDialog timePickerDialog = new TimePickerDialog(getActivity(), android.R.style.Theme_Holo_Light_Dialog_NoActionBar, myTimeListener, hour, minute, true);
        timePickerDialog.setTitle(getResources().getString(R.string.contribution_start_time));
        timePickerDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        timePickerDialog.show();
    }

    public void fetchFormDropDownData() {

        Call<lendingDataResponse> call = api.getLendingData();
        call.enqueue(new Callback<lendingDataResponse>() {
            @Override
            public void onResponse(Call<lendingDataResponse> call, Response<lendingDataResponse> response) {
                try {
                    if (response.body().getSuccess()) {
                        if (response.body().getMessage().getRepaymentplans().size() > 0) {
                            repayment_plans = new ArrayList<>();
                            List<Repaymentplan> repayment_classes = response.body().getMessage().getRepaymentplans();
                            ArrayList<String> repaymentplans_drop_down = new ArrayList<String>();
                            repaymentplans_drop_down.add(getResources().getString(R.string.select_plan));
                            repayment_plans.add("0");
                            for (int i = 0; i < repayment_classes.size(); i++) {
                                String id = String.valueOf(repayment_classes.get(i).getId());
                                Log.e("Id_data", id);
                                String name = repayment_classes.get(i).getName();
                                repaymentplans_drop_down.add(name);
                                repayment_plans.add(name);

                            }
                            repaymentPlan.setItems(repaymentplans_drop_down);
                            repaymentPlan.setSelectedIndex(0);
                        }

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }


            }

            @Override
            public void onFailure(Call<lendingDataResponse> call, Throwable t) {

            }
        });
    }

    @Override
    public void onDataReady(String initiator, String response) {
        switch (initiator) {
            case INITIATOR_STORE_HISA_SETTING:
                try {
                    runOnUI(() -> {
                        Log.e("STORE_JAMII_RESPONSE", "Response is " + response);
//                                    hideDialog(ctx);
                        JSONObject jObj = null;
                        try {
                            jObj = new JSONObject(response);
                            if (jObj.has("errors")) {
                                String errors = jObj.getString("errors");
                                showSweetAlertDialogError(errors, ctx);
                            } else { // if no error message was found in the response,
                                boolean success = jObj.getBoolean("success");
                                if (success) { // if the request was successfuls, got forward, else show error message
                                    String message = jObj.getString("message");
                                    Log.e("MESSAGE_TAG_S", "onDataReady: " + message);
                                    showSweetAlertDialogSuccess("Contribution settings saved Successfully", ctx);
                                    NavHostFragment.findNavController(getParentFragment()).navigateUp();
                                    Toast toast = Toast.makeText(ctx, "settings saved Successfully", Toast.LENGTH_LONG);
                                    toast.setGravity(Gravity.CENTER, 0, 0);
                                    toast.show();
                                } else {
                                    String api_error_message = jObj.getString("message");
                                    Log.e("ERROR_TAG_TAG", "onDataReady: " + api_error_message);
                                    showSweetAlertDialogError(api_error_message, ctx);
                                }
                            }
                        } catch (JSONException e) {
                            Log.e("ExceptionLOGIN-DATA", e.toString());
                            showSweetAlertDialogError(global_error_message, ctx);
                            e.printStackTrace();
                        }
                    });
                } catch (Exception e) {
//                            hideDialog(ctx);
                    Log.e("Exception- HOME", e.toString());
                    showSweetAlertDialogError(global_error_message, ctx);
//                    Toast.makeText(ctx, "Login Data Response Exception" + e.toString(), Toast.LENGTH_LONG).show();
                }
                break;


        }
    }

    @Override
    public void onDataLoaded(String initiator, JSONArray data) {

    }

    @Override
    public void onNetworkError(String initiator, String errorCode, String errorMessage) {
        switch (initiator) {

            case INITIATOR_STORE_JAMII_SETTING:
                try {
                    runOnUI(new Runnable() {
                        @Override
                        public void run() {
//                                    hideDialog(ctx);
                            showSweetAlertDialogError(errorMessage, ctx);
                            NavHostFragment.findNavController(fragment).navigateUp();
                        }
                    });
                } catch (Exception e) {

                    showSweetAlertDialogError(global_error_message, ctx);
                    Log.e("initiator", e.toString());
                    NavHostFragment.findNavController(fragment).navigateUp();

                }
                break;
        }
    }

    private void getMeetingsData(String setting_id, String group_id) {
        Call<meetingsSettingsResponse> call = api.getMeetingsSettings(setting_id, group_id);
        call.enqueue(new Callback<meetingsSettingsResponse>() {
            @Override
            public void onResponse(Call<meetingsSettingsResponse> call, Response<meetingsSettingsResponse> response) {
                try {
                    if (response.body().getSuccess()) {
                        Log.e("MEETINGS_RESPONSE", "onResponse: " + response.body().getCurrent_settings().getData());
                        if (response.body().getCurrent_settings().getData().size() > 0) {
                            List<Data> Meetings = response.body().getCurrent_settings().getData();
                            ArrayList<String> available_meetings = new ArrayList<>();
                            //   available_meetings.add(0, getResources().getString(R.string.attach_meeting  ));
                            for (int item = 0; item < Meetings.size(); item++) {
                                available_meetings.add(Meetings.get(item).getName());
                            }
                            arrayAdapter = new ArrayAdapter(ctx, android.R.layout.simple_spinner_dropdown_item, available_meetings);
                            arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            spinner.setAdapter(arrayAdapter);
                            spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                    // String item_resource_type = (String) parent.getItemAtPosition(position);


                                    meeting_index = String.valueOf(Meetings.get(position).getIndex());
                                    meeting_name = Meetings.get(position).getFrequency();
                                    Log.e("MEETING_INDEX", meeting_index);

                                    String time = Meetings.get(position).getTime();
                                    String frequency = Meetings.get(position).getFrequency();
                                    String day = Meetings.get(position).getDay();

                                    ArrayList<String> freqList = new ArrayList();
                                    freqList.add("Weekly");
                                    freqList.add("Monthly");
                                    freqList.add("Annually");
                                    int freqs = freqList.indexOf(frequency);

                                    ArrayList<String> daysList = new ArrayList();
                                    daysList.add("Sunday");
                                    daysList.add("Monday");
                                    daysList.add("Tuesday");
                                    daysList.add("Wednesday");
                                    daysList.add("Thursday");
                                    daysList.add("Friday");
                                    daysList.add("Saturday");
                                    int days = daysList.indexOf(day);

                                    tiContributionStartTime.setText(time);
                                    tiContributionDay.setSelection(days);
                                    tiContributionFrequency.setSelection(freqs);

                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> parent) {

                                }
                            });
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<meetingsSettingsResponse> call, Throwable t) {
                Log.e("MEETINGS_FAILED", "onFailure: " + t.getLocalizedMessage());

            }
        });

    }

    @Override
    public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
        switch (view.getId()) {
            case R.id.repaymentPlan:
                Log.d("Repayment plan id is ", repayment_plans.get(position));
                repayment_plan = repayment_plans.get(position);
                break;


        }
    }

}
