package com.app.ecobba.Fragments.Profile.MyProfile

data class Wallet(
    val account_number: String,
    val balance: Int,
    val created_at: String,
    val currency: Currency,
    val id: Int,
    val order_ref: String,
    val status: Int,
    val updated_at: String,
    val user_id: Int
)