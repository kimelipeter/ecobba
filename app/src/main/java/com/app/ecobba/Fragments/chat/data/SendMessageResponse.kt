package com.app.ecobba.Fragments.chat.data

data class SendMessageResponse(
    val message: Message,
    val response_text: String
)