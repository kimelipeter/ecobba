package com.app.ecobba.Fragments.chat.data

import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "chat_participants")
data class Participant(
        @PrimaryKey
        @NonNull
        @ColumnInfo(name = "participant_id")
        var id: Int = 0,
        var name: String = "",
        var lastMessage : String = ""
){
    override fun hashCode(): Int {
        return id.hashCode()+name.hashCode()
    }
}