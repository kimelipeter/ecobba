package com.app.ecobba.Fragments.Group.Settings.Adapters;

import android.content.Context;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.ecobba.Fragments.Group.Settings.ContributionSettingDetailsBotomSheetDialog;
import com.app.ecobba.Fragments.Group.Settings.LoanSettingDetailsBotomSheetDialog;
import com.app.ecobba.Models.MeetingsSettingsContibutions.Data;


import com.app.ecobba.R;

import java.util.List;
import java.util.stream.Stream;

public class ContributionsFeesAdapter extends RecyclerView.Adapter<ContributionsFeesAdapter.ViewHolder> {

    public List<Data> dataList;
    public Context context;
    public Fragment fragment;
    public  String user_currency;

    public ContributionsFeesAdapter(List<Data> dataList, Context context, Fragment fragment,String user_currency) {
        this.dataList = dataList;
        this.context = context;
        this.fragment = fragment;
        this.user_currency=user_currency;
    }

    @NonNull
    @Override
    public ContributionsFeesAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.contributions_list_item, parent, false);
        return new ContributionsFeesAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ContributionsFeesAdapter.ViewHolder holder, int position) {


        try {
            String frequency = dataList.get(position).getFrequency();
            String name=dataList.get(position).getName();
            String min=dataList.get(position).getMinimum();
            String max=dataList.get(position).getMaximum();
            String value=dataList.get(position).getValue();
            String day=dataList.get(position).getDay();
            String fine=dataList.get(position).getFine();
            String time=dataList.get(position).getTime();
            String type=dataList.get(position).getSettings_type();
            String source_type=dataList.get(position).getSource().getType();
            String telco=dataList.get(position).getSource().getTelco();
            String msisdn=dataList.get(position).getSource().getMsisdn();



            holder.contributionsLl.setAnimation(AnimationUtils.loadAnimation(context, R.anim.fade_transition_animation));
            holder.tvContributionName.setText(name);


            holder.tvContributionFrequency.setText(context.getResources().getString(R.string.frequency) + ": " + frequency);

            holder.contributionsLl.setOnClickListener(v -> {
                FragmentManager manager = ((AppCompatActivity) context).getSupportFragmentManager();
                ContributionSettingDetailsBotomSheetDialog contributionSettingDetailsBotomSheetDialog = new
                        ContributionSettingDetailsBotomSheetDialog(name,min,max,frequency,value,day,fine,time,type,user_currency,source_type,telco,msisdn);
                contributionSettingDetailsBotomSheetDialog.show(manager, "TaG");
            });


        } catch (Exception e) {
            e.printStackTrace();

        }

    }

    @Override
    public int getItemCount() {
        Log.e("Size2", "size2 " + dataList.size());
        return dataList.size();

    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView tvContributionName;

        TextView tvContributionFrequency;
        LinearLayout contributionsLl;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            tvContributionName = itemView.findViewById(R.id.tvLoanName);

            tvContributionFrequency = itemView.findViewById(R.id.tvContributionFrequency);
            contributionsLl = itemView.findViewById(R.id.contributionsLl);

        }
    }
}
