package com.app.ecobba.Fragments.Transfer;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.app.ecobba.R;
import com.tenbis.library.consts.CardType;
import com.tenbis.library.listeners.OnCreditCardStateChanged;
import com.tenbis.library.models.CreditCard;
import com.tenbis.library.views.CompactCreditCardInput;

import org.jetbrains.annotations.NotNull;

import butterknife.BindView;
import butterknife.ButterKnife;
//import cards.payment.paycardsrecognizer.sdk.Card;
//import cards.payment.paycardsrecognizer.sdk.ScanCardIntent;

public class MyCardsFragment extends Fragment implements View.OnClickListener ,OnCreditCardStateChanged{

    private static final int SCAN_CARD = 10001;

    @BindView(R.id.btnScanCard)
    Button btnScanCard;

    @BindView(R.id.btnAddCard)
    Button btnAddCard;

    @BindView(R.id.ciCard)
    CompactCreditCardInput ciCard;

    @BindView(R.id.ivStatus)
    ImageView ivStatus;



    Context ctx;

    public MyCardsFragment(Context ctx){this.ctx = ctx;}

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View frag = inflater.inflate(R.layout.fragment_money_cards,container,false);
        ButterKnife.bind(this,frag);
        return frag;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        btnScanCard.setOnClickListener(this);
        btnAddCard.setOnClickListener(this);
        ciCard.setCloseKeyboardOnValidCard(true);
        ciCard.addOnCreditCardStateChangedListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btnScanCard:
                scanCard();
                break;
            case R.id.btnAddCard:

                break;
        }
    }

    private void scanCard() {
//        Intent intent = new ScanCardIntent.Builder(getContext()).build();
//        startActivityForResult(intent, SCAN_CARD);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == SCAN_CARD) {
//            if (resultCode == Activity.RESULT_OK) {
//                Card card = data.getParcelableExtra(ScanCardIntent.RESULT_PAYCARDS_CARD);
//                String cardData = "Card number: " + card.getCardNumberRedacted() + "\n"
//                        + "Card holder: " + card.getCardHolderName() + "\n"
//                        + "Card expiration date: " + card.getExpirationDate();
//                Log.e("SCAN_CARD", "Card info: " + cardData);
//            } else if (resultCode == Activity.RESULT_CANCELED) {
//                Log.e("SCAN_CARD", "Scan canceled");
//            } else {
//                Log.e("SCAN_CARD", "Scan failed");
//            }
        }
    }

    @Override
    public void onCreditCardValid(@NotNull CreditCard creditCard) {
        ivStatus.setImageResource(R.drawable.ic_appintro_done_white);
        ivStatus.setColorFilter(getResources().getColor(R.color.green));

        btnAddCard.setEnabled(true);
    }

    @Override
    public void onCreditCardNumberValid(@NotNull String s) {

    }

    @Override
    public void onCreditCardExpirationDateValid(int i, int i1) {

    }

    @Override
    public void onCreditCardCvvValid(@NotNull String s) {

    }

    @Override
    public void onCreditCardTypeFound(@NotNull CardType cardType) {
//        ((ImageView)getActivity().findViewById(R.id.ivCard)).setImageResource(ciCard.getCardTypeImageId(cardType));
//        ((ImageView)getActivity().findViewById(R.id.ivCard)).setColorFilter(getResources().getColor(R.color.transparent));
    }

    @Override
    public void onInvalidCardTyped() {
        ivStatus.setImageResource(android.R.drawable.ic_menu_close_clear_cancel);
        ivStatus.setColorFilter(getResources().getColor(R.color.red));
        btnAddCard.setEnabled(false);

    }


}
