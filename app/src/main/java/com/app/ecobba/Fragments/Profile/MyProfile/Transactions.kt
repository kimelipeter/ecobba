package com.app.ecobba.Fragments.Profile.MyProfile

data class Transactions(
    val credit: Int,
    val debit: Int
)