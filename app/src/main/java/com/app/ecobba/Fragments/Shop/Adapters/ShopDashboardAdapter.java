package com.app.ecobba.Fragments.Shop.Adapters;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.RecyclerView;

import com.app.ecobba.Models.shopsdashboard.Data;
import com.app.ecobba.R;
import com.app.ecobba.common.rxApi.Api;
import com.app.ecobba.common.rxApi.RxApi;
import com.google.android.material.snackbar.Snackbar;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ShopDashboardAdapter extends RecyclerView.Adapter<ShopDashboardAdapter.ViewHolder> {
   public List<Data> dataList;
   public Fragment fragment;
   public Context context;
   Boolean clicked = true;
   Handler handler;
   Runnable runnable;


    public ShopDashboardAdapter(List<Data> dataList, Fragment fragment, Context context) {
        this.dataList = dataList;
        this.fragment = fragment;
        this.context = context;
    }

    @NonNull
    @Override
    public ShopDashboardAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.dashbord_myshops_list_items,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ShopDashboardAdapter.ViewHolder holder, int position) {

        Data shop = dataList.get(position);

        holder.action_shops.setAnimation(AnimationUtils.loadAnimation(context,R.anim.fade_transition_animation));
        //holder.tv_name.setText(shop.getName());
        String email=shop.getShop_email();
        String name=shop.getShop_name();
        Log.e("SHOP_EMAIL_NAME",email+name);
        holder.tv_email.setText(shop.getShop_email());

        holder.tv_pnumber.setText(shop.getShop_msisdn());
        holder.tv_loc.setText(shop.getShop_address());
        // holder.tv_cat.setText(shop.getCategory());
        String idshop = String.valueOf(shop.getId());
        Log.d("Shop_ID:",idshop);
        //next here
        holder.tv_shopname.setText(shop.getShop_name());
        String shopNameChar=shop.getShop_name();
        String sCharshopName=shopNameChar.substring(0,1);
        Log.e("First letter Name is :",sCharshopName);
        holder.tv_shop_name_char.setText(sCharshopName.toUpperCase());
        holder.my_shops_container.setOnClickListener((view ->{


            Bundle args = new Bundle();
            args.putString("name",name);
            args.putString("shop_id_number",idshop );
            NavHostFragment.findNavController(fragment).navigate(R.id.single_shop_list_item,args);
        }));


    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    public void deleteItem(int position) {
       Data deleteditem = dataList.get(position);

        clicked = true;

                if (clicked=true){
                    Api api = new RxApi().getApi();
                    Call<ResponseBody> call = api.deletesingleshop(dataList.get(position).getId());
                    call.enqueue(new Callback<ResponseBody>() {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                            if (response.isSuccessful()){
                                Toast.makeText(context," Deleted ",Toast.LENGTH_LONG).show();
                                dataList.remove(position);
                                notifyItemRemoved(position);
                               notifyItemRangeChanged(position,dataList.size());
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {

                        }
                    });
                }else {
                    Toast.makeText(context," False ",Toast.LENGTH_LONG).show();
                }




    }

    private void showUndoSnackbar(int position1,Data data) {
        Snackbar snackbar = Snackbar.make(fragment.requireParentFragment().requireView(),"",Snackbar.LENGTH_LONG);

        snackbar.setAction("Undo",v -> undodelete(position1,data));
        snackbar.show();
    }

    public void undodelete(int position,Data data) {
        clicked = false;
        dataList.add(position,data);

      //  handler.removeCallbacks(runnable);
        notifyItemInserted(position);


    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tv_email;
        TextView tv_pnumber;
        TextView tv_loc;
        TextView tv_shop_name_char;
        TextView tv_shopname;
        LinearLayout action_shops;
        LinearLayout my_shops_container;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            action_shops=itemView.findViewById(R.id.action_shops);
            my_shops_container=itemView.findViewById(R.id.my_shops_container);
            //cardHeaderShops=itemView.findViewById(R.id.cardHeaderShops);
            //tv_name=itemView.findViewById(R.id.name);
            tv_email=itemView.findViewById(R.id.email);
            tv_pnumber=itemView.findViewById(R.id.pnumber);
            tv_loc=itemView.findViewById(R.id.loc);
//            tv_cat=itemView.findViewById(R.id.cat);
            tv_shopname=itemView.findViewById(R.id.shopname);
            tv_shop_name_char=itemView.findViewById(R.id.shop_name_char);
        }
    }
}
