package com.app.ecobba.Fragments.Group.Settings;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import com.app.ecobba.Fragments.Group.Settings.Adapters.MeetingSettingAdapter;
import com.app.ecobba.Models.MeetingsSettingsData.meetingsSettingsResponse;
import com.app.ecobba.R;

import com.app.ecobba.common.rxApi.Api;
import com.app.ecobba.common.rxApi.RxApi;


import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;

import butterknife.BindView;

import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MeetingSettingFragment extends Fragment implements View.OnClickListener {


    Context context;
    Fragment fragment;
    Bundle args;
    String api_tail;
    String group_id, setting_id;
    MeetingSettingAdapter adapter;
    private RecyclerView rvMeetings;
    @BindView(R.id.create)
    FloatingActionButton create;
    @BindView(R.id.tvNoAvailableData)
    TextView tvNoAvailableData;
    private Api api;
    ProgressDialog progressDialog;


    public MeetingSettingFragment() {

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_group_setting_meeting, container, false);
        ButterKnife.bind(this, view);
        context = getActivity();
        fragment = this;
        rvMeetings = (RecyclerView) view.findViewById(R.id.recyclerView);
        rvMeetings.addItemDecoration(new DividerItemDecoration(getContext(),
                DividerItemDecoration.VERTICAL));
        adapter = new MeetingSettingAdapter(new ArrayList<>(), requireContext(), fragment);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(requireContext());
        rvMeetings.setLayoutManager(layoutManager);
        rvMeetings.setAdapter(adapter);
        api = new RxApi().getApi();
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        progressDialog = new ProgressDialog(context);
        args = getArguments();
        assert args != null;
        api_tail = args.getString("args");
        Log.e("tail_data", api_tail);
        group_id = args.getString("group_id");
        setting_id = args.getString("id");
        create.setOnClickListener(this);
        getData(setting_id, group_id);
    }


    private void getData(String setting_id, String group_id) {
        progressDialog.setMessage("Loading.Please wait....");
        progressDialog.show();
        Call<meetingsSettingsResponse> call = api.getMeetingsSettings(setting_id, group_id);
        call.enqueue(new Callback<meetingsSettingsResponse>() {
            @Override
            public void onResponse(Call<meetingsSettingsResponse> call, Response<meetingsSettingsResponse> response) {
                try {
                    if (response.body().getSuccess()) {
                        progressDialog.dismiss();
                        Log.e("DATA_COMING", "onResponse: " + response);
                        if (response.body().getCurrent_settings().getData().size() > 0) {
                            adapter.dataList = response.body().getCurrent_settings().getData();
                            adapter.notifyDataSetChanged();
                        } else {

                            tvNoAvailableData.setText(getResources().getString(R.string.no_available_data));
                            tvNoAvailableData.setVisibility(View.VISIBLE);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<meetingsSettingsResponse> call, Throwable t) {

            }
        });

    }


    @Override
    public void onClick(View view) {
        Bundle arg = new Bundle();
        arg.putString("id", setting_id);
        arg.putString("group_id", group_id);
        switch (view.getId()) {

            case R.id.create:
                NavHostFragment.findNavController(this).navigate(R.id.createMeetingSetting, arg);
                break;

        }

    }

}
