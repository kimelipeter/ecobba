package com.app.ecobba.Fragments.Lending;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.ecobba.Fragments.Borrowing.Adapters.AvailableLoansAdapter;
import com.app.ecobba.Models.LendingPackageDetails.packageLendingDetailsResponse;

import com.app.ecobba.R;
import com.app.ecobba.common.SharedMethods;
import com.app.ecobba.common.rxApi.Api;
import com.app.ecobba.common.rxApi.RxApi;
import com.app.ecobba.recyclerview_adapters.MyLoansBorrowersListRecycler;

import com.google.android.material.textfield.TextInputLayout;

import java.util.ArrayList;


import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class PackageFragment extends Fragment implements View.OnClickListener {

    @BindView(R.id.search)
    Button search;
    @BindView(R.id.info)
    Button info;
    @BindView(R.id.searchContainer)
    TextInputLayout searchContainer;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.currency)
    TextView currency;
    @BindView(R.id.repayment)
    TextView repayment;

    @BindView(R.id.interest)
    TextView interest;
    @BindView(R.id.insuredStatus)
    TextView insuredStatus;
    @BindView(R.id.minCreditScore)
    TextView minCreditScore;
    @BindView(R.id.status)
    TextView status;
    @BindView(R.id.packageName)
    TextView packageName;
    @BindView(R.id.tvNoAvailableLoans)
    TextView tvNoAvailableLoans;

    private Unbinder unbinder;
    Context context;
    Fragment fragment;
    String id;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    public PackageFragment() {
    }

    private Api api;
    MyLoansBorrowersListRecycler adapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_loanpackage, container, false);
        ButterKnife.bind(this, view);
        api = new RxApi().getApi();
        recyclerView = view.findViewById(R.id.recyclerView);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
        adapter = new MyLoansBorrowersListRecycler(new ArrayList(), context, fragment);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        searchContainer.setVisibility(View.GONE);
        search.setOnClickListener(this);
        ButterKnife.bind(this, view);
        context = getActivity();

        assert getArguments() != null;
        id = getArguments().getString("id");
        String package_name = getArguments().getString("package_name");
        String interest_rate = getArguments().getString("interest_rate");
        String repayment_plan = getArguments().getString("repayment_plan");
        String insured_status = getArguments().getString("insured_status");
        String package_status = getArguments().getString("package_status");
        String _currency = getArguments().getString("currency");
        String min_credit_score = getArguments().getString("min_credit_score");
        String borrowers_number = getArguments().getString("borrowers_number");
        String _loanRequests = getArguments().getString("loanRequests");

        Log.e("Number of Borrowers", borrowers_number);
        assert borrowers_number != null;
        if (borrowers_number.equals("null") || borrowers_number.isEmpty()) {
            borrowers_number = "0";
        }
        assert _loanRequests != null;
        if (_loanRequests.equals("null") || _loanRequests.isEmpty()) {
            _loanRequests = "0";
        }

        //        0-pending\n1-approved\n2-declined\n3-paid\n4-defaulted
        String package_status_ui = "";
        assert package_status != null;
        if (package_status.equals("0")) {
            package_status_ui = "Inactive";
        } else if (package_status.equals("1")) {
            package_status_ui = "Active";
        }
        assert insured_status != null;
        String insured_status_iu = "No";
        if (insured_status.equals("1")) {
            insured_status_iu = "Yes";
        }

        packageName.setText(package_name);

        status.setText(package_status_ui);
        interest.setText(interest_rate);
        insuredStatus.setText(insured_status_iu);
        currency.setText(_currency);

        minCreditScore.setText(min_credit_score);

        if (repayment_plan.equals("1")){
            repayment.setText("Weekly");
        }
        else if (repayment_plan.equals("3")){
            repayment.setText("Monthly");
        }
        else if (repayment_plan.equals("4")){
            repayment.setText("Annually");
        }

        fetchPackageDetails(id);

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.search:
                viewVisibilityToggle(searchContainer);
                if (search.getText().toString().equals(getResources().getString(R.string.search))) {
                    search.setText(R.string.close);
                } else {
                    search.setText(R.string.search);
                }
                break;
            case R.id.card:

                break;
        }
    }

    private void viewVisibilityToggle(View view) {
        if (view.getVisibility() == View.VISIBLE) {
            view.setVisibility(View.GONE);
        } else {
            view.setVisibility(View.VISIBLE);
        }
    }


    private void fetchPackageDetails(String id) {

        Call<packageLendingDetailsResponse> call = api.getPackageDetails(id);
        call.enqueue(new Callback<packageLendingDetailsResponse>() {
            @Override
            public void onResponse(Call<packageLendingDetailsResponse> call, Response<packageLendingDetailsResponse> response) {
                try {
                    if (response.body().getSuccess()) {
                        Log.e("gsfsffs", "onResponse: "+response );
                        if (response.body().getPackage().size() > 0) {
                            progressBar.setVisibility(View.GONE);
                            adapter.datalist = response.body().getPackage();
                            adapter.notifyDataSetChanged();

                        } else {
                            progressBar.setVisibility(View.GONE);
                            tvNoAvailableLoans.setText(getResources().getString(R.string.no_available_data));
                            tvNoAvailableLoans.setVisibility(View.VISIBLE);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<packageLendingDetailsResponse> call, Throwable t) {
                Log.e("error_data", "onFailure: "+t.getLocalizedMessage());

            }
        });
    }


}
