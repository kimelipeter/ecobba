package com.app.ecobba.Fragments.Utility;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;

import com.app.ecobba.R;
import com.app.ecobba.Utils.BaseFragment;
import com.app.ecobba.common.SharedMethods;
import com.google.gson.Gson;
import com.rilixtech.widget.countrycodepicker.Country;
import com.rilixtech.widget.countrycodepicker.CountryCodePicker;

import org.json.JSONArray;

import java.util.HashMap;
import java.util.Map;

//import static com.app.ecobba.common.ApisKtKt.utility;
import static com.app.ecobba.common.ApisKtKt.utility;
import static com.app.ecobba.common.AppConstantsKt.INITIATOR_BUY_AIRTIME;
import static com.app.ecobba.common.SharedMethods.HTTP_POST;
import static com.app.ecobba.common.SharedMethods.getDefaults;
import static com.app.ecobba.common.SharedMethods.postToServer;

public class Buy_Airtime extends BaseFragment implements SharedMethods.NetworkObjectListener {
    private EditText phone;
    private EditText amount;
    private Button button,water_bills,electricity;
    CountryCodePicker ccp;
    ProgressDialog pd;
    TextView tv;
    CardView cardViewElectricity;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_utility,container,false);

        phone =(EditText) view.findViewById(R.id.edtphone);
        amount = (EditText) view.findViewById(R.id.edt_amount);
        button = (Button) view.findViewById(R.id.button7);
        water_bills=(Button) view.findViewById(R.id.wate_bills);
        electricity =(Button) view.findViewById(R.id.electricity);
        tv=(TextView) view.findViewById(R.id.tv_coming_soon);
        ccp = (CountryCodePicker) view.findViewById(R.id.ccp);
        cardViewElectricity=(CardView) view.findViewById(R.id.cvElectricity);

        ccp.setOnCountryChangeListener(new CountryCodePicker.OnCountryChangeListener() {
            @Override
            public void onCountrySelected(Country selectedCountry) {
                Toast.makeText(getContext(), "Updated country to " + ccp.getSelectedCountryName(), Toast.LENGTH_SHORT).show();
                String code=ccp.getSelectedCountryCode();

                Log.e("Code_country",code);
            }
        });


        water_bills.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Build an AlertDialog
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());

                // Set a title for alert dialog
                builder.setTitle(R.string.water_bills);

                // Ask the final question
                builder.setMessage(getString(R.string.coming_soon));

                // Set click listener for alert dialog buttons
                DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch(which){
                            case DialogInterface.BUTTON_POSITIVE:
                                // User clicked the Yes button
                                tv.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 35);
                                break;

                            case DialogInterface.BUTTON_NEGATIVE:
                                // User clicked the No button
                                break;
                        }
                    }
                };

                // Set the alert dialog yes button click listener
                builder.setPositiveButton("Ok", dialogClickListener);

                // Set the alert dialog no button click listener
              //  builder.setNegativeButton("No",dialogClickListener);

                AlertDialog dialog = builder.create();
                // Display the alert dialog on interface
                dialog.show();
            }

        });
        cardViewElectricity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Build an AlertDialog
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());

                // Set a title for alert dialog
                builder.setTitle(R.string.electricity);

                // Ask the final question
                builder.setMessage("Coming Soon!");

                // Set click listener for alert dialog buttons
                DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch(which){
                            case DialogInterface.BUTTON_POSITIVE:
                                // User clicked the Yes button
                                tv.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 35);
                                break;

                            case DialogInterface.BUTTON_NEGATIVE:
                                // User clicked the No button
                                break;
                        }
                    }
                };

                // Set the alert dialog yes button click listener
                builder.setPositiveButton("Ok", dialogClickListener);

                // Set the alert dialog no button click listener
                //  builder.setNegativeButton("No",dialogClickListener);

                AlertDialog dialog = builder.create();
                // Display the alert dialog on interface
                dialog.show();
            }

        });

        electricity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Build an AlertDialog
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());

                // Set a title for alert dialog
                builder.setTitle(R.string.electricity);

                // Ask the final question
                builder.setMessage("Coming Soon!");

                // Set click listener for alert dialog buttons
                DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch(which){
                            case DialogInterface.BUTTON_POSITIVE:
                                // User clicked the Yes button
                                tv.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 35);
                                break;

                            case DialogInterface.BUTTON_NEGATIVE:
                                // User clicked the No button
                                break;
                        }
                    }
                };

                // Set the alert dialog yes button click listener
                builder.setPositiveButton("Ok", dialogClickListener);

                // Set the alert dialog no button click listener
                //  builder.setNegativeButton("No",dialogClickListener);

                AlertDialog dialog = builder.create();
                // Display the alert dialog on interface
                dialog.show();
            }

        });

        water_bills.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Build an AlertDialog
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());

                // Set a title for alert dialog
                builder.setTitle(R.string.water_bills);

                // Ask the final question
                builder.setMessage("Coming Soon!");

                // Set click listener for alert dialog buttons
                DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch(which){
                            case DialogInterface.BUTTON_POSITIVE:
                                // User clicked the Yes button
                                tv.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 35);
                                break;

                            case DialogInterface.BUTTON_NEGATIVE:
                                // User clicked the No button
                                break;
                        }
                    }
                };

                // Set the alert dialog yes button click listener
                builder.setPositiveButton("Ok", dialogClickListener);

                // Set the alert dialog no button click listener
                //  builder.setNegativeButton("No",dialogClickListener);

                AlertDialog dialog = builder.create();
                // Display the alert dialog on interface
                dialog.show();
            }

        });

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String token = getDefaults("TOKEN",requireContext());
                String phonenumber = phone.getText().toString();
                String phoneamount = amount.getText().toString();
                if (phonenumber.isEmpty()){
                    phone.setError(getResources().getString(R.string.emptyField));
                    return;
                }
                if (phoneamount.isEmpty()){
                    amount.setError(getResources().getString(R.string.emptyField));
                    return;
                }

                 /**
                 * {
                 *     phone:07000000,
                 *     amount:10
                 * }
                 */


                 Map<String,String> params = new HashMap();
                 params.put("phone",phonenumber);
                params.put("amount",phoneamount);
                 String params_ = new Gson().toJson(params);
                 postToServer(utility,token,params_,INITIATOR_BUY_AIRTIME,HTTP_POST,requireContext(),Buy_Airtime.this);

            }
        });


        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);



    }

    @Override
    public void onDataReady(String initiator, String response) {

        String buyairtime = response;

        Long converted = new Gson().fromJson(response,Long.class);
        String message = String.valueOf(converted);
        Log.e("Tag",""+message);
        //
       // switch (initiator){
            //case INITIATOR_BUY_AIRTIME:
            //    Long converted = new Gson().fromJson(response,Long.class);
            //    break;
        }
   // }

    @Override
    public void onDataLoaded(String initiator, JSONArray data) {



    }

    @Override
    public void onNetworkError(String initiator, String errorCode, String errorMessage) {
        /*
        switch (initiator){
            case INITIATOR_BUY_AIRTIME:

                break;
        }

         */
    }
}
