package com.app.ecobba.Fragments.Group.Settings.Adapters;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.RecyclerView;

import com.app.ecobba.Fragments.Group.Settings.LoanSettingDetailsBotomSheetDialog;
import com.app.ecobba.Models.MeetingsSettingsLoans.Data;
import com.app.ecobba.R;

import java.util.List;

public class LoansSettingsAdapter extends RecyclerView.Adapter<LoansSettingsAdapter.ViewHolder> {
    public List<Data> dataList;
    public Context context;
    public Fragment fragment;
    public String user_currency;

    public LoansSettingsAdapter(List<Data> dataList, Context context, Fragment fragment, String user_currency) {
        this.dataList = dataList;
        this.context = context;
        this.fragment = fragment;
        this.user_currency = user_currency;
    }

    @NonNull
    @Override
    public LoansSettingsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.list_item_loan_settings, parent, false);
        return new LoansSettingsAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull LoansSettingsAdapter.ViewHolder holder, int position) {
        try {

            String name = dataList.get(position).getName();
            String type = dataList.get(position).getSettings_type();
            String min = dataList.get(position).getMin_amount();
            String max = dataList.get(position).getMax_amount();
            String repayment_plan = dataList.get(position).getRepayment_plan();
            String repayment_period = dataList.get(position).getRepayment_period();
            String interest = dataList.get(position).getInterest();
            String fine = dataList.get(position).getFine();

            holder.meetingLl.setAnimation(AnimationUtils.loadAnimation(context, R.anim.fade_transition_animation));
            holder.loan_name.setText(name);
            holder.meetingLl.setOnClickListener(v -> {
                FragmentManager manager = ((AppCompatActivity) context).getSupportFragmentManager();
                LoanSettingDetailsBotomSheetDialog loanSettingDetailsBotomSheetDialog = new
                        LoanSettingDetailsBotomSheetDialog(name, type, min, max, user_currency, repayment_plan, repayment_period, interest, fine);
                loanSettingDetailsBotomSheetDialog.show(manager, "TaG");


            });

            if (type.equals("jamii")) {

                holder.loan_type.setText(R.string.welfare_loan);
            } else {
                holder.loan_type.setText(R.string.businness_loan);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView loan_name, loan_type, tvDateCreated;
        LinearLayout meetingLl;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            loan_name = itemView.findViewById(R.id.tvLoanName);
            loan_type = itemView.findViewById(R.id.tvLoanType);
            meetingLl = itemView.findViewById(R.id.meetingLl);
        }
    }
}
