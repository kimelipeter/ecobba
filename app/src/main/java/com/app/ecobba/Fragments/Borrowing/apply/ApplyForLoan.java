package com.app.ecobba.Fragments.Borrowing.apply;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.ViewFlipper;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

import com.app.ecobba.R;
import com.app.ecobba.common.SharedMethods;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.app.ecobba.common.ApisKtKt.confirmLoanApplication;
import static com.app.ecobba.common.ApisKtKt.getLendingData;
import static com.app.ecobba.common.ApisKtKt.loanApplicationRequest;
import static com.app.ecobba.common.AppConstantsKt.INITIATOR_APPLY_LOAN;
import static com.app.ecobba.common.AppConstantsKt.INITIATOR_CONFIRM_APPLY_LOAN;
import static com.app.ecobba.common.AppConstantsKt.INITIATOR_GETLENDING_DATA;
import static com.app.ecobba.common.SharedMethods.GroupDigits;
import static com.app.ecobba.common.SharedMethods.HTTP_GET;
import static com.app.ecobba.common.SharedMethods.HTTP_POST;
import static com.app.ecobba.common.SharedMethods.getFromServer;
import static com.app.ecobba.common.SharedMethods.global_error_message;
import static com.app.ecobba.common.SharedMethods.postToServer;
import static com.app.ecobba.common.SharedMethods.runOnUI;
import static com.app.ecobba.common.SharedMethods.showSweetAlertDialogError;

public class ApplyForLoan extends Fragment implements  SharedMethods.NetworkObjectListener,View.OnClickListener {
    Context ctx;
    String id,token;
    //        @BindView(R.id.tv_min_and_max_placeholder)
//        TextView tv_min_and_max_placeholder;
    @BindView(R.id.package_name)
    TextView package_name;
    @BindView(R.id.min_amount)
    TextView tv_min_amount;
    @BindView(R.id.max_amount)
    TextView tv_max_amount;
    @BindView(R.id.interest_rate)
    TextView tv_interest_rate;
    @BindView(R.id.insured_status)
    TextView tv_insured_status;
    @BindView(R.id.btApplyLoan)
    Button apply_loan;
    @BindView(R.id.etConfirmPassword)
    FrameLayout confirm;
//    @BindView(R.id.input_loan_name)
    EditText et_input_loan_name;
    @BindView(R.id.input_loan_amount)
    EditText et_input_loan_amount;
    @BindView(R.id.input_loan_length)
    EditText et_input_loan_length;
    @BindView(R.id.input_payment_frequency)
    EditText et_input_payment_frequency;
    @BindView(R.id.input_loan_description)
    EditText et_input_input_loan_description;
    @BindView(R.id.repayment_plan)
    TextView tv_repayment_plan;
    @BindView(R.id.tvPeriod)
    TextView tvPeriod;

    //    @BindView(R.id.cancel_apply_loan)
//    LinearLayout cancel_apply_loan;
//    @BindView(R.id.apply_loan)
    String loan_name;
    @BindView(R.id.tv_loan_title_name)
    TextView tv_loan_title_name;
    @BindView(R.id.tv_no_of_installments)
    TextView tv_no_of_installments;
    @BindView(R.id.tv_payment_frequency)
    TextView tv_payment_frequency;
    @BindView(R.id.tv_currency)
    TextView tv_currency;
    @BindView(R.id.principle_amount)
    TextView principle_amount;
    @BindView(R.id.total_interest_amount)
    TextView total_interest_amount;
    @BindView(R.id.monthly_amount_deducted)
    TextView monthly_amount_deducted;
    @BindView(R.id.annual_interest_rate)
    TextView annual_interest_rate;
    @BindView(R.id.sub_totals_value)
    TextView sub_totals_value;
    @BindView(R.id.processing_fee)
    TextView processing_fee;
    @BindView(R.id.grand_total)
    TextView grand_total;
    @BindView(R.id.vfLoanApplication)
    ViewFlipper vfLoanApplication;
    @BindView(R.id.btContinue)
    Button btContinue;
    @BindView(R.id.btConfirm)
    Button btConfirm;
    @BindView(R.id.cancel_apply_loan)
    Button cancel;
    @BindView(R.id.sbApplyLoan)
    SeekBar sbApplyLoan;
    @BindView(R.id.pbApplyLoan)
    ProgressBar load;



    String _input_loan_name,_input_loan_amount,_input_payment_frequency,_input_loan_length,_input_loan_description;
    String repayment_plan_id,repayment_plan;

    public String subTotalAmount,monthlyDeduction,strtotalInterest,strPrincipal,strgrand_total_loan;

    String principal,subTotal,totalAmount,totalInterest,installmentAmount,titleOfLoan,lengthOfLoan,expectedScore,score,walletBalance,serviceFee,paymentFrequency,annualInterest,currencyUnit;

    Fragment fragment=this;
    String regex = "(?<=[\\d])(,)(?=[\\d])";
    Pattern p = Pattern.compile(regex);

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.apply_for_loan, container, false);
        ButterKnife.bind(this, v);
        ctx=getActivity();

        assert getArguments() != null;
        id = getArguments().getString("id");
         loan_name = getArguments().getString("loan_name");
        String min_amount = getArguments().getString("min_amount");
        String max_amount = getArguments().getString("max_amount");
        String currency = getArguments().getString("currency");
        Log.e("Maximum Loan Amount",max_amount);
        String interest_pa = getArguments().getString("interest_pa");
        String insured_status = getArguments().getString("insured_status");
        repayment_plan = getArguments().getString("repayment_plan");
        Log.e("repayment_plan_request",repayment_plan);
        repayment_plan_id = getArguments().getString("repayment_plan_id");


        if (repayment_plan.equals("1")){
            tvPeriod.setText("Weeks");
            tv_repayment_plan.setText("Weekly");
        }
        else  if (repayment_plan.equals("2")){
            tvPeriod.setText("Bi-Weekly");
            tv_repayment_plan.setText("Bi-Weekly");
        }
        else  if (repayment_plan.equals("3")){
            tvPeriod.setText("Months");
            tv_repayment_plan.setText("Monthly");
        }
        else  if (repayment_plan.equals("4")){
            tvPeriod.setText("Annually");
            tv_repayment_plan.setText("Annually");
        }
        else if (repayment_plan.equals("weekly")){
            tvPeriod.setText("Weeks");
            tv_repayment_plan.setText("Weekly");
        }
        else if (repayment_plan.equals("bi-weekly")){
            tvPeriod.setText("Bi-Weekly");
            tv_repayment_plan.setText("Bi-Weekly");
        }
        else if (repayment_plan.equals("monthly")){
            tvPeriod.setText("Months");
            tv_repayment_plan.setText("Monthly");
        }

        package_name.setText(loan_name.trim());
        assert min_amount != null;
        tv_min_amount.setText(GroupDigits(min_amount).trim());
        assert max_amount != null;
        tv_max_amount.setText(GroupDigits(max_amount).trim());
        tv_interest_rate.setText(interest_pa.trim());



        String insured_status_to_ui = "No";
        assert insured_status != null;
        if( insured_status.equals("1") ){
            insured_status_to_ui = "Yes";
        }
        tv_insured_status.setText(insured_status_to_ui);
        btConfirm.setOnClickListener(this);
        cancel.setOnClickListener(this);
        btContinue.setOnClickListener(this);

        token= SharedMethods.getDefaults("token",ctx);
        getLendingData(token);
        View.OnClickListener apply_loan_ClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //your listener code here

            }
        };

        apply_loan.setOnClickListener(this);

        return v;
    }
    private void getLendingData(String token){
        Map<String, String> params = new HashMap<>();
        params.put("token", token);
        getFromServer( getLendingData,token, params , INITIATOR_GETLENDING_DATA , HTTP_GET , ctx ,this);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        et_input_loan_name=view.findViewById(R.id.input_loan_name);
        et_input_loan_name.setText(loan_name);


    }

    private boolean validateFormFields(){
        boolean valid = true;


        _input_loan_name = et_input_loan_name.getText().toString();
        _input_loan_amount = et_input_loan_amount.getText().toString();
        _input_loan_length    = et_input_loan_length.getText().toString();
        _input_payment_frequency=et_input_payment_frequency.getText().toString();
        _input_loan_description=et_input_input_loan_description.getText().toString();

        if (_input_loan_name.isEmpty()) {
            et_input_loan_name.setError(getString(R.string.emptyField));
            valid = false;
        }

        if (_input_loan_amount.isEmpty()) {
            et_input_loan_amount.setError(getString(R.string.emptyField));
            valid = false;
        }

        if (_input_loan_length.isEmpty()) {
            et_input_loan_length.setError(getString(R.string.emptyField));
            valid = false;
        }
        if (_input_payment_frequency.isEmpty()) {
            et_input_payment_frequency.setError(getString(R.string.emptyField));
            valid = false;
        } if (_input_loan_description.isEmpty()) {
            et_input_input_loan_description.setError(getString(R.string.emptyField));
            valid = false;
        }

        return valid;
    }

    public void loanApplicationToAPI(){


        String params = "{ \"amount\":" + '"' + _input_loan_amount + '"' +
                ", \"length_of_loan\": " + '"' + _input_loan_length + '"' +
                ", \"payment_frequency\": " + '"' + _input_payment_frequency + '"' +
                ", \"loan_description\": " + '"' +_input_loan_description + '"' +
                ", \"loan_title \": " + '"' + _input_loan_name + '"' +
                "}\n";
        Log.e("POST_PARAMS", params);

        postToServer( loanApplicationRequest +"/"+id ,token, params , INITIATOR_APPLY_LOAN , HTTP_POST , ctx ,this);
    }




    @Override
    public void onDataReady(String initiator, final String response) {
        switch (initiator) {
            case INITIATOR_APPLY_LOAN:
                try {
                    runOnUI(new Runnable() {
                        @Override
                        public void run() {
                            Log.e("APPLY_LOAN RESPONSE", "Response is " + response);
//                            hideDialog(ctx);
                            JSONObject jObj = null;
                            try {
                                jObj = new JSONObject(response);
                                if (jObj.has("errors")) {
                                    String errors = jObj.getString("errors");
                                    showSweetAlertDialogError(errors, ctx);
                                } else { // if no error message was found in the response,
                                    boolean success = jObj.getBoolean("success");
                                    if (success) { // if the request was successfuls, got forward, else show error message
//                                        showAlertAndMoveToPage("Success, You application has been submitted",HomePageBottomNav.class,ctx);

                                        JSONObject jOBj_loanrequest = jObj.getJSONObject("loanrequest");
                                        principal = jOBj_loanrequest.getString("principal");
                                        subTotal = jOBj_loanrequest.getString("subTotal");
                                        totalAmount = jOBj_loanrequest.getString("totalAmount");
                                        Log.e("loantotal",totalAmount);
                                        totalInterest = jOBj_loanrequest.getString("totalInterest");
                                        installmentAmount = jOBj_loanrequest.getString("installmentAmount");
                                        titleOfLoan = jOBj_loanrequest.getString("titleOfLoan");
                                        lengthOfLoan = jOBj_loanrequest.getString("lengthOfLoan");
                                        expectedScore = jOBj_loanrequest.getString("expectedScore");
                                        score = jOBj_loanrequest.getString("score");
                                        walletBalance = jOBj_loanrequest.getString("walletBalance");
                                        serviceFee = jOBj_loanrequest.getString("serviceFee");
                                        Log.e("service_fee_amount",serviceFee);
                                        paymentFrequency = jOBj_loanrequest.getString("paymentFrequency");
                                        annualInterest = jOBj_loanrequest.getString("annualInterest");
                                        currencyUnit = jOBj_loanrequest.getString("currencyUnit");

                                        tv_loan_title_name.setText(titleOfLoan);
                                        tv_no_of_installments.setText(lengthOfLoan);
                                        tv_payment_frequency.setText( paymentFrequency==null?
                                                    "":paymentFrequency.isEmpty()? "":paymentFrequency
                                                );
                                        tv_currency.setText(currencyUnit);
                                         subTotalAmount=subTotal;
                                        Matcher msubTotal = p.matcher(subTotalAmount);
                                        subTotalAmount = msubTotal.replaceAll("");
                                        Log.e("subTotalAmount Amount:",subTotalAmount);
                                        sub_totals_value.setText(subTotalAmount);

                                        processing_fee.setText(serviceFee);
                                        annual_interest_rate.setText(annualInterest);
                                         monthlyDeduction=installmentAmount;
                                        Matcher mMonthlyDeduction = p.matcher(monthlyDeduction);
                                        monthlyDeduction = mMonthlyDeduction.replaceAll("");
                                        Log.e("installmentAmount :",monthlyDeduction);
                                        monthly_amount_deducted.setText(monthlyDeduction);

                                         strtotalInterest = totalInterest;
                                        Matcher mtotalInterest = p.matcher(strtotalInterest);
                                        strtotalInterest = mtotalInterest.replaceAll("");
                                        Log.e("My STR totalInterest:",strtotalInterest);
                                        total_interest_amount.setText(strtotalInterest);

                                         strPrincipal = principal;
                                        Matcher m = p.matcher(strPrincipal);
                                        strPrincipal = m.replaceAll("");
                                        Log.e("My STR Amount is:",strPrincipal);
                                        principle_amount.setText(strPrincipal);
                                         strgrand_total_loan = totalAmount;
                                        Matcher matcher = p.matcher(strgrand_total_loan);
                                        strgrand_total_loan = matcher.replaceAll("");
                                        Log.e("grand_total_loan:",strgrand_total_loan);
                                        grand_total.setText(strgrand_total_loan);

                                        setPage(1);
                                        apply_loan.setVisibility(View.GONE);
                                        confirm.setVisibility(View.VISIBLE);
                                    } else {
                                        String api_error_message = jObj.getString("message");
                                        showSweetAlertDialogError(api_error_message, ctx);
                                    }
                                }

                            } catch (JSONException e) {
                                Log.e("ExceptionLOGIN-DATA", e.toString());
                                showSweetAlertDialogError(global_error_message, ctx);
                                e.printStackTrace();
                                apply_loan.setVisibility(View.VISIBLE);
                            }
                        }
                    });
                } catch (Exception e) {
//                    hideDialog(ctx);
                    Log.e("Exception- LOGIN", e.toString());
                    showSweetAlertDialogError(global_error_message, ctx);
//                    Toast.makeText(ctx, "Login Data Response Exception" + e.toString(), Toast.LENGTH_LONG).show();
                }
                break;
            case INITIATOR_GETLENDING_DATA:
                try {
                    runOnUI(() -> {
                        Log.e("REG_DATA RESPONSE", "Response is " + response);
//                                    hideDialog(ctx);
                        JSONObject jObj = null;
                        try {
                            jObj = new JSONObject(response);
                            if (jObj.has("errors")) {
                                String errors = jObj.getString("errors");
                                showSweetAlertDialogError(errors, ctx);
                            } else { // if no error message was found in the response,
                                boolean success = jObj.getBoolean("success");
                                if (success) { // if the request was successfuls, got forward, else show error message
                                    JSONObject data = jObj.getJSONObject("message");
                                    JSONArray repaymentplans = data.getJSONArray("repaymentplans");
                                    ArrayList<String> repaymentplans_drop_down = new ArrayList<String>();
                                    repaymentplans_drop_down.add(getResources().getString(R.string.please_select));
                                    if (repaymentplans.length() > 0) {
                                        for (int i = 0; i < repaymentplans.length(); i++) {
                                            JSONObject jsonObject = repaymentplans.getJSONObject(i);
                                            String id = jsonObject.getString("id");
                                            String name = jsonObject.getString("name");
                                            repaymentplans_drop_down.add(name);
                                        }

//                                            spinner_country.setItems(repaymentplans_drop_down);
//                                            spinner_country.setSelectedIndex(0);
                                    }

                                } else {
                                    String api_error_message = jObj.getString("message");
                                    showSweetAlertDialogError(api_error_message, ctx);
                                }
                            }

                        } catch (JSONException e) {
                            Log.e("ExceptionLOGIN-DATA", e.toString());
                            showSweetAlertDialogError(global_error_message, ctx);
                            e.printStackTrace();
                        }
                    });
                } catch (Exception e) {
//                            hideDialog(ctx);
                    Log.e("Exception- HOME", e.toString());
                    showSweetAlertDialogError(global_error_message, ctx);
//                    Toast.makeText(ctx, "Login Data Response Exception" + e.toString(), Toast.LENGTH_LONG).show();
                }
                break;
            case INITIATOR_CONFIRM_APPLY_LOAN:
                try {
                    runOnUI(new Runnable() {
                        @Override
                        public void run() {
                            Log.e("APPLY_LOAN RESPONSE", "Response is " + response);
//                            hideDialog(ctx);
                            JSONObject jObj = null;
                            try {
                                jObj = new JSONObject(response);
                                if (jObj.has("errors")) {
                                    String errors = jObj.getString("errors");
                                    showSweetAlertDialogError(errors, ctx);
                                } else { // if no error message was found in the response,
                                    boolean success = jObj.getBoolean("success");
                                    if (success) { // if the request was successfuls, got forward, else show error message
//                                        showAlertAndMoveToPage("Success, You application has been submitted", MainActivity.class,ctx);
                                        setPage(2);
                                    } else {
                                        String api_error_message = jObj.getString("message");
                                        showSweetAlertDialogError(api_error_message, ctx);
                                        toggleProg();
                                    }
                                }

                            } catch (JSONException e) {
                                Log.e("ExceptionLOGIN-DATA", e.toString());
                                showSweetAlertDialogError(global_error_message, ctx);
                                e.printStackTrace();
                            }
                        }
                    });
                } catch (Exception e) {
//                    hideDialog(ctx);
                    Log.e("Exception- LOGIN", e.toString());
                    showSweetAlertDialogError(global_error_message, ctx);
//                    Toast.makeText(ctx, "Login Data Response Exception" + e.toString(), Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

    @Override
    public void onDataLoaded(String initiator, JSONArray data) {

    }

    @Override
    public void onNetworkError(String initiator, String errorCode, final String errorMessage) {
        switch (initiator) {
            case INITIATOR_APPLY_LOAN:
                try{
                    runOnUI(new Runnable() {
                        @Override
                        public void run() {
//                            hideDialog(ctx);
                            showSweetAlertDialogError(errorMessage, ctx);
                        }
                    });
                }catch (Exception e){
//                    hideDialog(ctx);
                    showSweetAlertDialogError(global_error_message, ctx);
                    Log.e("Exception- LOGIN", e.toString());
//                    Toast.makeText(ctx, "Login Error Response Exception" + e.toString(), Toast.LENGTH_LONG).show();
                }

                break;
            case INITIATOR_GETLENDING_DATA:
                try{
                    runOnUI(new Runnable() {
                        @Override
                        public void run() {
//                            hideDialog(ctx);
                            showSweetAlertDialogError(errorMessage, ctx);
                        }
                    });
                }catch (Exception e){
//                    hideDialog(ctx);
                    showSweetAlertDialogError(global_error_message, ctx);
                    Log.e("Exception", e.toString());
//                    Toast.makeText(ctx, "Login Error Response Exception" + e.toString(), Toast.LENGTH_LONG).show();
                }
                break;
            case INITIATOR_CONFIRM_APPLY_LOAN:
                try {
                    runOnUI(new Runnable() {
                        @Override
                        public void run() {
//                            hideDialog(ctx);
                            showSweetAlertDialogError(errorMessage, ctx);
                        }
                    });
                } catch (Exception e) {
//                    hideDialog(ctx);
                    showSweetAlertDialogError(global_error_message, ctx);
                    Log.e("Exception-LOGIN", e.toString());
//                    Toast.makeText(ctx, "Login Error Response Exception" + e.toString(), Toast.LENGTH_LONG).show();
                }

                break;
        }
    }

    public void confirmLoanPackageApplication(String token){
        String params = "{ \"principal\":" + '"' + strPrincipal + '"' +
                ", \"subTotal\": " + '"' + subTotalAmount + '"' +
                ", \"totalAmount\": " + '"' + strgrand_total_loan + '"' +
                ", \"totalInterest\": " + '"' + strtotalInterest + '"' +
                ", \"installmentAmount\": " + '"' + monthlyDeduction + '"' +
                ", \"titleOfLoan\": " + '"' + titleOfLoan + '"' +
                ", \"lengthOfLoan\": " + '"' + lengthOfLoan + '"' +
                ", \"expectedScore\": " + '"' + expectedScore + '"' +
                ", \"score\": " + '"' + score + '"' +
                ", \"walletBalance\": " + '"' + walletBalance + '"' +
                ", \"serviceFee\": " + '"' + serviceFee + '"' +
                ", \"paymentFrequency\": " + '"' + paymentFrequency + '"' +
                ", \"annualInterest\": " + '"' + annualInterest + '"' +
                ", \"currencyUnit\": " + '"' + currencyUnit + '"' +
                "}\n";
        Log.e("POST PARAMS", params);
        postToServer( confirmLoanApplication +"/"+id ,token, params , INITIATOR_CONFIRM_APPLY_LOAN , HTTP_POST , ctx ,this);
    }




    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btApplyLoan:
//                view.setVisibility(View.GONE);
                if(validateFormFields()){
                    loanApplicationToAPI();
                }
                break;
            case R.id.btConfirm:
                view.setEnabled(false);
                toggleProg();
                Log.e("Loan Confirm","PRESSED");
                confirmLoanPackageApplication(token);
                break;
            case R.id.cancel_apply_loan:
                NavHostFragment.findNavController(this).navigateUp();
                break;
            case R.id.btContinue:
                NavHostFragment.findNavController(this).navigateUp();
                break;
        }

    }

    private void setPage(int n){
        int m = vfLoanApplication.getChildCount()-1;
        sbApplyLoan.setMax(m);
        if (n <= m && n>=0){
            vfLoanApplication.setDisplayedChild(n);
            sbApplyLoan.setProgress(n);
            if (n==m){
                confirm.setVisibility(View.GONE);
            }
        }
    }

    private void toggleProg(){
        if (load.getVisibility() == View.GONE){
            load.setVisibility(View.VISIBLE);
        }else {
            load.setVisibility(View.VISIBLE);
        }
    }
}
