package com.app.ecobba.Fragments.Profile.MyProfile

data class Currency(
    val name: String,
    val prefix: String
)