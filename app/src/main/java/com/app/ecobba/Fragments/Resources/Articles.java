package com.app.ecobba.Fragments.Resources;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.app.ecobba.Fragments.Resources.Adapters.ArticlesAdapter;
import com.app.ecobba.Models.Resources.Articles.ArticlesResponse;
import com.app.ecobba.common.rxApi.Api;
import com.app.ecobba.common.rxApi.RxApi;

import com.app.ecobba.R;
import com.app.ecobba.common.SharedMethods;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;


import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;



public class Articles extends Fragment implements View.OnClickListener, SwipeRefreshLayout.OnRefreshListener {
    @BindView(R.id.create)
    FloatingActionButton create;
    Context context;
    String token;
    ArticlesAdapter adapter;
    Fragment fragment;
    private static final String TAG = "articles_data";
    private RecyclerView rvServices;
    SwipeRefreshLayout mSwipeRefreshLayout;
    long datepower;
    private Api api;
    ProgressDialog progressDialog;
    @BindView(R.id.tvNoAvailableData)
    TextView tvNoAvailableData;

    public Articles() {
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_articles, container, false);
        rvServices = (RecyclerView) view.findViewById(R.id.recyclerviewid);
        mSwipeRefreshLayout = view.findViewById(R.id.swipe_container);
        adapter = new ArticlesAdapter(new ArrayList<>(), requireContext(), fragment);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(requireContext());
        rvServices.setLayoutManager(layoutManager);
        rvServices.setAdapter(adapter);

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                Toast.makeText(context, "Refreshing", Toast.LENGTH_LONG).show();
                loadRecyclerViewData();
                mSwipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary,
                        android.R.color.holo_green_dark,
                        android.R.color.holo_orange_dark,
                        android.R.color.holo_blue_dark);

                mSwipeRefreshLayout.setRefreshing(false);
            }
        });


        return view;
    }


    private void loadRecyclerViewData() {
        progressDialog.setMessage("Loading.Please wait....");
        progressDialog.show();
        Call<ArticlesResponse> call=api.getArticleResources();
        call.enqueue(new Callback<ArticlesResponse>() {
            @Override
            public void onResponse(Call<ArticlesResponse> call, Response<ArticlesResponse> response) {
                try {
                    if (response.body().getSuccess()){
                        progressDialog.dismiss();
                        if (response.body().getData().size()>0){
                            adapter.dataList=response.body().getData();
                            adapter.notifyDataSetChanged();
                        }
                        else {
                            tvNoAvailableData.setText(getResources().getString(R.string.no_available_data));
                            tvNoAvailableData.setVisibility(View.VISIBLE);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ArticlesResponse> call, Throwable t) {

            }
        });

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        context = getActivity();
        token = SharedMethods.getDefaults("token", context);
        api = new RxApi().getApi();
        progressDialog = new ProgressDialog(context);
        ButterKnife.bind(this, view);
        create.setOnClickListener(this);
        loadRecyclerViewData();

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.create:
                NavHostFragment.findNavController(this).navigate(R.id.addResources);
        }

    }


    @Override
    public void onResume() {
        super.onResume();
        // make api call to get list of articles items
        loadRecyclerViewData();
    }


    @Override
    public void onRefresh() {
        // Fetching data from server
        loadRecyclerViewData();
    }
}
