package com.app.ecobba.Fragments.Group.Contributions;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.app.ecobba.Models.Currencies.Data;
import com.app.ecobba.Models.Currencies.currenciesResponse;

import com.app.ecobba.Models.PaymentSources.AdapaySource;
import com.app.ecobba.Models.PaymentSources.paymentSourcesResponse;
import com.app.ecobba.Models.adapay.adapaySourcesResponse;
import com.app.ecobba.Models.adapay.telcos.telcosResponse;
import com.app.ecobba.Models.payment.AVAILABLECONTRIBUTIONS;

import com.app.ecobba.Models.payment.contributionResponse;

import com.app.ecobba.R;
import com.app.ecobba.common.SharedMethods;
import com.app.ecobba.common.rxApi.Api;
import com.app.ecobba.common.rxApi.RxApi;

import com.jaredrummler.materialspinner.MaterialSpinner;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


import static com.app.ecobba.common.ApisKtKt.makeContribution;

import static com.app.ecobba.common.AppConstantsKt.INITIATOR_ADA_PAY_TELCOS;

import static com.app.ecobba.common.AppConstantsKt.INITIATOR_MAKE_CONTRIBUTION;

import static com.app.ecobba.common.SharedMethods.HTTP_POST;

import static com.app.ecobba.common.SharedMethods.global_error_message;
import static com.app.ecobba.common.SharedMethods.postToServer;
import static com.app.ecobba.common.SharedMethods.runOnUI;
import static com.app.ecobba.common.SharedMethods.showSweetAlertDialogError;
import static com.app.ecobba.common.SharedMethods.showSweetAlertDialogSuccess;

public class MakeContributionDialog extends DialogFragment implements SharedMethods.NetworkObjectListener,
        View.OnClickListener, MaterialSpinner.OnItemSelectedListener<String> {
    Context ctx;
    String token;
    ColorDrawable popupColor;
    String fullname = "", groupid, phone_number;

    Spinner payMethod, provider, meeting_contribution;
    @BindView(R.id.currency)
    MaterialSpinner currency;

//    @BindView(R.id.editTextMsdn)
//    EditText editTextMsdn;
    @BindView(R.id.tvSharesBought)
    EditText tvSharesBought;
    @BindView(R.id.etCashCollected)
    EditText etCashCollected;

    LinearLayout linearLayoutBank;
    LinearLayout linearLayoutMobile, llLayoutMobileMpesa;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.close)
    Button close;
    @BindView(R.id.deposit)
    Button deposit;
    @BindView(R.id.textView7)
    TextView textView7;
    private Unbinder unbinder;
    ArrayAdapter arrayAdapter;

    Spinner SpinnerMsdn;

    List<String> currencies_ids, ada_pay_ids, currency_prefix, telcos_adapay;
    String currency_id_toapi = "", ada_pay_id_toapi = "0", currency_prefix_toapi, selected_type, selected_provider;
    String _amount_shares, _amount_collected, _msdn, _contribution_id;
    private Api api;
    int _contribution_value, total;
    String ada_id="";

    int userInput = 0;


    public MakeContributionDialog(String full_name, String group_id, String msisdn) {
        fullname = full_name;
        groupid = group_id;
        phone_number = msisdn;
        Log.e("user_bundle ", full_name + " and " + groupid + " and phone number" + phone_number);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.make_contribution_dialog, null);
        ctx = getActivity();
        api = new RxApi().getApi();
        token = SharedMethods.getDefaults("token", ctx);
        fetchFormDropDownData(token);
        fetchAdaPaySourceType();
        fetchAdaPayTelcos();
        fetchUserPaymentSoucers();
        close = view.findViewById(R.id.close);
        unbinder = ButterKnife.bind(this, view);
        SpinnerMsdn = view.findViewById(R.id.SpinnerMsdn);
        payMethod = view.findViewById(R.id.payMethod);
        provider = view.findViewById(R.id.provider);
        meeting_contribution = view.findViewById(R.id.meeting_contribution);
        deposit.setOnClickListener(this);
        currency.setOnItemSelectedListener(this);
//        payMethod.setOnItemSelectedListener(this);
        api = new RxApi().getApi();
        linearLayoutMobile = view.findViewById(R.id.linearLayoutMobile);
        llLayoutMobileMpesa = view.findViewById(R.id.llLayoutMobileMpesa);
        popupColor = new ColorDrawable(getResources().getColor(R.color.colorSurface));
        currency.getPopupWindow().setBackgroundDrawable(popupColor);
//        payMethod.getPopupWindow().setBackgroundDrawable(popupColor);
        close.setOnClickListener(v -> dismiss());
        textView7.setText(fullname);
//        editTextMsdn.setText(phone_number);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(view);
        fetchAvailableContributions(groupid);

        tvSharesBought.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (s.length() != 0) {
                    userInput = Integer.parseInt(s.toString());
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

                if (s.length() != 0) {
                    userInput = Integer.parseInt(s.toString());
                    tvSharesBought.removeTextChangedListener(this);
//                    tvSharesBought.setText(userInput);
                    tvSharesBought.addTextChangedListener(this);

                }
            }
        });


        return builder.create();
    }

    private void fetchAvailableContributions(String group_id) {
        Call<contributionResponse> call = api.getAvailableContributions(group_id);
        call.enqueue(new Callback<contributionResponse>() {
            @Override
            public void onResponse(Call<contributionResponse> call, Response<contributionResponse> response) {
                Log.e("TAG_DATA", "onResponse: " + response);
                if (response.body().getSuccess()) {
                    if (response.body().getData().getContributions().getAVAILABLE_CONTRIBUTIONS().size() > 0) {
                        List<AVAILABLECONTRIBUTIONS> Contributions = response.body().getData().getContributions().getAVAILABLE_CONTRIBUTIONS();
                        ArrayList<String> availables = new ArrayList<>();
                        // availables.add(0, getResources().getString(R.string.please_select_contribution));
                        for (int item = 0; item < Contributions.size(); item++) {
                            availables.add(Contributions.get(item).getContribution_name());
                        }
                        arrayAdapter = new ArrayAdapter(ctx, android.R.layout.simple_spinner_dropdown_item, availables);
                        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        meeting_contribution.setAdapter(arrayAdapter);
                        meeting_contribution.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                String item_resource_type = (String) parent.getItemAtPosition(position);
                                _contribution_id = String.valueOf(Contributions.get(position).getId());
                                _contribution_value = Contributions.get(position).getAmount();
                                Log.e("CONTRIBUTION_ID", _contribution_id);
                                total = userInput * _contribution_value;
                                Log.e("CONTRIBUTION_VALU", String.valueOf(total));
                                etCashCollected.setText(String.valueOf(total));
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {

                            }
                        });
                    }

                }
            }

            @Override
            public void onFailure(Call<contributionResponse> call, Throwable t) {
                Log.e("TAG", "onFailure: " + t.getLocalizedMessage());
            }
        });


    }



    public void fetchFormDropDownData(String token) {
        Call<currenciesResponse> call = api.fetchCurrencies();
        call.enqueue(new Callback<currenciesResponse>() {
            @Override
            public void onResponse(Call<currenciesResponse> call, Response<currenciesResponse> response) {
                if (response.body().getSuccess()) {
                    currencies_ids = new ArrayList<>();
                    currency_prefix = new ArrayList<>();
                    List<Data> currencies = response.body().getData();
                    ArrayList<String> currencies_drop_down = new ArrayList<String>();
                    currencies_drop_down.add(getResources().getString(R.string.please_select_currency));
                    currencies_ids.add("0");
                    currency_prefix.add("0");
                    if (currencies.size() > 0) {
                        for (int i = 0; i < currencies.size(); i++) {
                            String id = String.valueOf(currencies.get(i).getId());
                            Log.e("currency_Id_data", id);
                            String name = currencies.get(i).getName();
                            String prefix = currencies.get(i).getPrefix();
                            Log.e("cur_prefix", prefix);
                            currencies_drop_down.add(name);
                            currencies_ids.add(id);
                            currency_prefix.add(prefix);
                        }

                        currency.setItems(currencies_drop_down);
                        currency.setSelectedIndex(0);
                    }


                } else {
                    String api_error_message = String.valueOf(response.body().getSuccess().booleanValue());
                    showSweetAlertDialogError(api_error_message, ctx);
                }

            }

            @Override
            public void onFailure(Call<currenciesResponse> call, Throwable t) {

            }
        });
    }
    private void fetchUserPaymentSoucers() {
        Call<paymentSourcesResponse> call = api.getUserPayPrefences();
        call.enqueue(new Callback<paymentSourcesResponse>() {
            @Override
            public void onResponse(Call<paymentSourcesResponse> call, Response<paymentSourcesResponse> response) {
                Log.e("TAG_payment_sources", "onResponse: " + response);
                if (response.body().getSuccess()) {
                    if (response.body().getAdapay_sources().size() > 0) {
                        List<AdapaySource> sources = response.body().getAdapay_sources();
                        ArrayList<String> availables = new ArrayList<>();
                        // availables.add(0, getResources().getString(R.string.please_select_contribution));
                        for (int item = 0; item < sources.size(); item++) {
                            availables.add(sources.get(item).getMsisdn());
                        }
                        arrayAdapter = new ArrayAdapter(ctx, android.R.layout.simple_spinner_dropdown_item, availables);
                        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        SpinnerMsdn.setAdapter(arrayAdapter);
                        SpinnerMsdn.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                String item_resource_type = (String) parent.getItemAtPosition(position);

                                _msdn = sources.get(position).getMsisdn();
                                Log.e("PHONE_NUMBER", _msdn);

                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {

                            }
                        });
                    } else {

                    }
                }


            }

            @Override
            public void onFailure(Call<paymentSourcesResponse> call, Throwable t) {

            }
        });
    }
    public void fetchAdaPaySourceType() {

        Call<adapaySourcesResponse> call = api.fetchAdapaySources();
        call.enqueue(new Callback<adapaySourcesResponse>() {
            @Override
            public void onResponse(Call<adapaySourcesResponse> call, Response<adapaySourcesResponse> response) {

                Log.e("SOLANI_DATA", "onResponse: " + response.body().getAdapay_source_types());
                String mobile = response.body().getAdapay_source_types().getMobile_money();
                String card = response.body().getAdapay_source_types().getCard_hosted();
                String bank_transfer = response.body().getAdapay_source_types().getBank_transfer();

                ada_pay_ids = new ArrayList<>();
                ada_pay_ids.add(0, getResources().getString(R.string.please_select_type));
                ada_pay_ids.add(1, mobile);
//                                ada_pay_ids.add(2, card);
//                                ada_pay_ids.add(3,bank_transfer);

                ArrayAdapter<String> adapter =
                        new ArrayAdapter<String>(ctx, android.R.layout.simple_spinner_dropdown_item, ada_pay_ids);
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                payMethod.setAdapter(adapter);

                payMethod.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                        String type = (String) parent.getItemAtPosition(position);

                        selected_type = "";
                        if (type.equals("Mobile Money")) {
                            selected_type = "mobile_money";
                            linearLayoutMobile.setVisibility(View.VISIBLE);
//                                            linearLayoutBank.setVisibility(View.GONE);

                        }
//                                        else if (type.equals("Master/Visa Card")){
//                                            selected_type = "card_hosted";
//                                            linearLayoutBank.setVisibility(View.GONE);
//                                            linearLayoutMobile.setVisibility(View.GONE);
//                                        }
//                                        else if (type.equals("Bank Transfer")){
//                                            selected_type = "bank_transfer";
//                                            linearLayoutBank.setVisibility(View.GONE);
//                                            linearLayoutMobile.setVisibility(View.GONE);
//                                        }
                        else {
//
                            //    linearLayoutBank.setVisibility(View.GONE);
                            linearLayoutMobile.setVisibility(View.GONE);
                        }
                        Log.e("Selected_type", String.valueOf(type));
                        Log.e("Selected_type2", selected_type);
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }

                });


            }

            @Override
            public void onFailure(Call<adapaySourcesResponse> call, Throwable t) {

            }
        });

    }

    public void fetchAdaPayTelcos() {
        Call<telcosResponse> call = api.fetchAdapayTelcos();
        call.enqueue(new Callback<telcosResponse>() {
            @Override
            public void onResponse(Call<telcosResponse> call, Response<telcosResponse> response) {
                if (response.body().getSuccess() == true) {
                    String telcos = response.body().getAdapay_telcos().getSafaricom_ke();
                    telcos_adapay = new ArrayList<>();
                    telcos_adapay.add(0, getResources().getString(R.string.please_select_type));
                    telcos_adapay.add(1, telcos);
                    ArrayAdapter<String> adapter =
                            new ArrayAdapter<String>(ctx, android.R.layout.simple_spinner_dropdown_item, telcos_adapay);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    provider.setAdapter(adapter);
                    provider.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            String type = (String) parent.getItemAtPosition(position);
                            selected_provider = "";
                            if (type.equals("Mpesa")) {
                                llLayoutMobileMpesa.setVisibility(View.VISIBLE);
                                selected_provider = "safaricom_ke";
                            } else {
                                llLayoutMobileMpesa.setVisibility(View.GONE);
                            }
                            Log.e("Selected_telcos", String.valueOf(type));
                            Log.e("Selected_telcos2", selected_provider);
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });


                }
            }

            @Override
            public void onFailure(Call<telcosResponse> call, Throwable t) {

            }
        });
    }


    @Override
    public void onDataReady(String initiator, String response) {
        switch (initiator) {

            case INITIATOR_ADA_PAY_TELCOS:
                try {
                    runOnUI(new Runnable() {
                        @Override
                        public void run() {
                            Log.e("ADA_PAY_TELCOS_RES", "Response is " + response);
                            try {
                                JSONObject jObj = new JSONObject(response);

                                JSONObject type = jObj.getJSONObject("adapay_telcos");

                                String telcos = type.getString("safaricom_ke");

                                telcos_adapay = new ArrayList<>();
                                telcos_adapay.add(0, getResources().getString(R.string.please_select_type));
                                telcos_adapay.add(1, telcos);


                                ArrayAdapter<String> adapter =
                                        new ArrayAdapter<String>(ctx, android.R.layout.simple_spinner_dropdown_item, telcos_adapay);
                                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                                provider.setAdapter(adapter);


                                provider.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                    @Override
                                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                                        String type = (String) parent.getItemAtPosition(position);

                                        selected_provider = "";

                                        if (type.equals("Mpesa")) {
                                            llLayoutMobileMpesa.setVisibility(View.VISIBLE);
                                            selected_provider = "safaricom_ke";
                                        } else {
                                            llLayoutMobileMpesa.setVisibility(View.GONE);
                                        }
                                        Log.e("Selected_telcos", String.valueOf(type));
                                        Log.e("Selected_telcos2", selected_provider);
                                    }

                                    @Override
                                    public void onNothingSelected(AdapterView<?> parent) {

                                    }
                                });


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                        }
                    });
                } catch (Exception e) {
//                            hideDialog(ctx);
                    Log.e("Exception- HOME", e.toString());
                    showSweetAlertDialogError(global_error_message, ctx);
//                    Toast.makeText(ctx, "Login Data Response Exception" + e.toString(), Toast.LENGTH_LONG).show();
                }
                break;

            case INITIATOR_MAKE_CONTRIBUTION:
                try {
                    runOnUI(new Runnable() {
                        @Override
                        public void run() {
                            Log.e("MAKE_CONTRIBUTION", "Response is " + response);
                            progressBar.setVisibility(View.GONE);
                            JSONObject jObj = null;
                            try {
                                jObj = new JSONObject(response);
                                if (jObj.has("errors")) {
                                    String errors = jObj.getString("errors");
                                    showSweetAlertDialogError(errors, ctx);
                                } else { // if no error message was found in the response,
                                    boolean success = jObj.getBoolean("success");
                                    if (success) {
                                        String api_message = jObj.getString("message");
                                        showSweetAlertDialogSuccess(api_message, ctx);
                                        dismiss();

                                    } else {
                                        String api_error_message = jObj.getString("message");
                                        showSweetAlertDialogError(api_error_message, ctx);
                                        dismiss();
                                    }
                                }

                            } catch (JSONException e) {
                                Log.e("Exception-DATA", e.toString());
                                showSweetAlertDialogError(global_error_message, ctx);
                                e.printStackTrace();
                            }
                        }
                    });
                } catch (Exception e) {
                    progressBar.setVisibility(View.GONE);
                    Log.e("Exception_+GROUP_MEMBER", e.toString());
                    showSweetAlertDialogError(global_error_message, ctx);
//                    Toast.makeText(ctx, "Login Data Response Exception" + e.toString(), Toast.LENGTH_LONG).show();
                }
                break;
        }

    }

    @Override
    public void onDataLoaded(String initiator, JSONArray data) {

    }

    @Override
    public void onNetworkError(String initiator, String errorCode, String errorMessage) {
        switch (initiator) {


        }

    }

    @Override
    public void onItemSelected(MaterialSpinner view, int position, long id, String item) {
        switch (view.getId()) {
            case R.id.currency:
                Log.d("Currency_id_is ", currencies_ids.get(position));
                currency_id_toapi = currencies_ids.get(position);
                currency_prefix_toapi = currency_prefix.get(position);
                break;


        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.deposit:
                if (validateFormFields()) {
                    makeContribution();

                }
                break;
        }
    }

    private boolean validateFormFields() {
        boolean valid = true;
//        _msdn = editTextMsdn.getText().toString();
//        Log.e("deposit_msdn", _msdn);
//        if (_msdn.isEmpty()) {
//            editTextMsdn.setError(getString(R.string.emptyField));
//            valid = false;
//        }
        _amount_shares = tvSharesBought.getText().toString();
        Log.e("amount", _amount_shares);
        if (_amount_shares.isEmpty()) {
            tvSharesBought.setError(getString(R.string.emptyField));
            valid = false;
        }
        _amount_collected = etCashCollected.getText().toString();
        Log.e("amount", _amount_collected);
        if (_amount_collected.isEmpty()) {
            etCashCollected.setError(getString(R.string.emptyField));
            valid = false;
        }

        if (currency_id_toapi.equals("")) {
            valid = false;
            Toast.makeText(ctx, getResources().getString(R.string.fill_in_all_fields), Toast.LENGTH_LONG).show();
        }

        return valid;
    }

    private void makeContribution() {
        progressBar.setVisibility(View.VISIBLE);

        String params =


                "{"
                        + "\"source\": {"
                        + "\"id\": \"" + ada_id + "\","
                        + "\"method\": \"" + selected_type + "\","
                        + "\"telco\": \"" + selected_provider + "\","
                        + "\"msisdn\": \"" + _msdn + "\""
                        + "},"
                        + "\"contribution_id\": " + _contribution_id + ","
                        + "\"shares_bought\": " + _amount_shares + ","
                        + "\"currency_id\": " + currency_id_toapi + ","
                        + "\"group_id\": " + groupid + ","
                        + "\"total_amount\": \"" + total + "\""
                        + "}";


        Log.e("POST_PARAMS", params);

        //makecontibution
        postToServer(makeContribution, token, params, INITIATOR_MAKE_CONTRIBUTION, HTTP_POST, ctx, this);
    }


}
