
package com.app.ecobba.Fragments.Profile.UserProfile;

import lombok.Data;

@Data
@SuppressWarnings("unused")
public class Role {

    private String createdAt;
    private String description;
    private String displayName;
    private long id;
    private String name;
    private Pivot pivot;
    private String updatedAt;

}
