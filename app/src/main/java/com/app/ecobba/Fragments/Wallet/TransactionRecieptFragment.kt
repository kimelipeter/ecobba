package com.app.ecobba.Fragments.Wallet

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.navArgs
import com.app.ecobba.R
import kotlinx.android.synthetic.main.frag_deposit_transaction_reciept.*

class TransactionRecieptFragment : Fragment(R.layout.frag_deposit_transaction_reciept) {
    val arguments : TransactionRecieptFragmentArgs by navArgs()
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        with(arguments.transactionResponse){
            transaction_code.text = transaction.txn_code
            amount.text = transaction.amount.toString()
            fee.text = transaction.transaction_fees.toString()

            title.text = transaction.transaction_data?.attributes?.statement_descriptor

            val first: Int? = transaction.amount
            val second: Int? = transaction.transaction_fees

            val sum = second?.let { first?.plus(it) }

            Log.d("SUM_IS", sum.toString())
            total.text = sum.toString()
        }


        close.setOnClickListener {
            NavHostFragment.findNavController(this).navigateUp()
        }
    }
}