
package com.app.ecobba.Fragments.Profile.UserProfile;

import lombok.Data;

@Data
@SuppressWarnings("unused")
public class Organization {

    private String name;

}
