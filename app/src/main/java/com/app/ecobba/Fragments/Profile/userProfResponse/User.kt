package com.app.ecobba.Fragments.Profile.userProfResponse

data class User(
    val account_no: String,
    val account_verified: Int,
    val avatar: String,
    val created_at: String,
    val credit_score: Int,
    val customer_username: String,
    val deleted_at: String,
    val detail: Detail,
    val email: String,
    val email_verified_at: String,
    val id: Int,
    val identification_document: String,
    val msisdn: String,
    val name: String,
    val organization: String,
    val other_names: String,
    val roles: List<Role>,
    val status: Int,
    val token: String,
    val transactions: Transactions,
    val updated_at: String,
    val verified: Int,
    val wallet: Wallet
)