package com.app.ecobba.Fragments.Group.Settings;

import android.app.TimePickerDialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.app.ecobba.Models.GroupWallet.AdapaySource;
import com.app.ecobba.Models.GroupWallet.groupWalletResponse;
import com.app.ecobba.Models.LendingData.Repaymentplan;
import com.app.ecobba.Models.LendingData.lendingDataResponse;
import com.app.ecobba.Models.MeetingsSettingsData.meetingsSettingsResponse;
import com.app.ecobba.Models.MeetingsSettingsFees.Data;
import com.app.ecobba.Models.MeetingsSettingsFees.MeetingsSettingsFeesResponse;
import com.app.ecobba.Models.UserProfile.Detail;
import com.app.ecobba.Models.adapay.adapaySourcesResponse;
import com.app.ecobba.Models.adapay.telcos.telcosResponse;
import com.app.ecobba.R;
import com.app.ecobba.ViewModel.UserProfileViewModel;
import com.app.ecobba.common.SharedMethods;
import com.app.ecobba.common.rxApi.Api;
import com.app.ecobba.common.rxApi.RxApi;
import com.google.android.material.chip.Chip;
import com.jaredrummler.materialspinner.MaterialSpinner;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.app.ecobba.common.ApisKtKt.storeFeesSettings;

import static com.app.ecobba.common.AppConstantsKt.INITIATOR_STORE_FINE_SETTING;

import static com.app.ecobba.common.SharedMethods.HTTP_POST;
import static com.app.ecobba.common.SharedMethods.global_error_message;
import static com.app.ecobba.common.SharedMethods.postToServer;
import static com.app.ecobba.common.SharedMethods.runOnUI;
import static com.app.ecobba.common.SharedMethods.showSweetAlertDialogError;
import static com.app.ecobba.common.SharedMethods.showSweetAlertDialogSuccess;

public class FeesSettingFragment extends Fragment implements View.OnClickListener, SharedMethods.NetworkObjectListener, MaterialSpinner.OnItemSelectedListener {

    @BindView(R.id.tiName)
    EditText tiName;


    @BindView(R.id.tiAmountPerWeek)
    EditText tiAmountPerWeek;


    @BindView(R.id.mainFrequencyLayout)
    ConstraintLayout mainFrequencyLayout;

    @BindView(R.id.tiFine)
    EditText tiFine;

    @BindView(R.id.btFeesApply)
    Button btFeesApply;

    @BindView(R.id.tvTime)
    EditText tvTime;
    Spinner spinner, msisdn;
    @BindView(R.id.tiContributionStartTime)
    TextView tiContributionStartTime;
    @BindView(R.id.yes)
    Chip tvYes;
    @BindView(R.id.no)
    Chip tvNo;

    @BindView(R.id.tiContributionDay)
    Spinner tiContributionDay;
    @BindView(R.id.llAttachMeeting)
    LinearLayout llAttachMeeting;
    @BindView(R.id.llNoAttachMeeting)
    LinearLayout llNoAttachMeeting;

    @BindView(R.id.tiContributionFrequency)
    Spinner tiContributionFrequency;
    @BindView(R.id.repaymentPlan)
    MaterialSpinner repaymentPlan;
    @BindView(R.id.currency)
    TextView currency;
    @BindView(R.id.fine_currency)
    TextView fine_currency;

    Context ctx;
    Fragment fragment;
    Bundle args;
    String token, api_tail;
    String amount_contributed;
    String fee_frequency;
    String fee_name;
    String group_id;
    String membership_fee_fine;
    String period;
    String setting_id;
    String setting_name = "Membership Fee", feesName;
    String contribution_interval_text;
    String meeting_index = "";
    String federation_amount = "";

    int x = 1;

    Spinner payMethod, provider, contributionDay;
    LinearLayout linearLayoutMobile, llLayoutMobileMpesa, linearLayoutBank;
    String meeting_name, meeting_time, time, frequency;
    ArrayAdapter arrayAdapter;
    List<String> repayment_plans;
    String con_frequency = "0", repayment_plan = "0";
    List<String> ada_pay_ids, telcos_adapay;
    String _msisdn, type, telco, selected_type, selected_provider, day;
    private Api api;
    String _group_id, _type, _telco, _setting_id, ada_id = "", _name, _amount, _user_currency;
    ColorDrawable popupColor;
    UserProfileViewModel userProfileViewModel;
    String user_currency;

    public FeesSettingFragment() {

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View frag = inflater.inflate(R.layout.fragment_group_setting_fees, container, false);
        ButterKnife.bind(this, frag);
        btFeesApply.setOnClickListener(this);
        ctx = getContext();
        fragment = this;
        spinner = frag.findViewById(R.id.meetings);
        payMethod = frag.findViewById(R.id.payMethod);
        provider = frag.findViewById(R.id.provider);
        contributionDay = frag.findViewById(R.id.contributionDay);

        msisdn = frag.findViewById(R.id.msisdn);
        linearLayoutMobile = frag.findViewById(R.id.linearLayoutMobile);
        llLayoutMobileMpesa = frag.findViewById(R.id.llLayoutMobileMpesa);
        linearLayoutBank = frag.findViewById(R.id.linearLayoutBank);
        api = new RxApi().getApi();
        userProfileViewModel = new ViewModelProvider(getActivity()).get(UserProfileViewModel.class);
        return frag;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        token = SharedMethods.getDefaults("token", ctx);
        args = getArguments();
        Log.e("args_data", String.valueOf(args));
        assert args != null;
        api_tail = args.getString("args");
        Log.e("api_detail_is", api_tail);
        group_id = args.getString("group_id");
        Log.e("GROUP_TEST_ID", group_id);
        _setting_id = args.getString("id");
        setting_id = "1";
        tvYes.setOnClickListener(this);
        tvNo.setOnClickListener(this);
        tvTime.setOnClickListener(this);
        getData(_setting_id, group_id);
        getMeetingsData(setting_id, group_id);
        fetchAdaPaySourceType();
        fetchAdaPayTelcos();
        fetchFormDropDownData();
        fetchUserPaymentSoucers(group_id);
        fetchUserProfile();

        mainFrequencyLayout.setVisibility(View.GONE);
        repaymentPlan.setOnItemSelectedListener(this);
        repaymentPlan.getPopupWindow().setBackgroundDrawable(popupColor);

        contributionDay.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                day = (String) parent.getItemAtPosition(position);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void getData(String setting_id, String group_id) {

        Call<MeetingsSettingsFeesResponse> call = api.getFeesSettings(setting_id, group_id);
        call.enqueue(new Callback<MeetingsSettingsFeesResponse>() {
            @Override
            public void onResponse(Call<MeetingsSettingsFeesResponse> call, Response<MeetingsSettingsFeesResponse> response) {
                try {
                    if (response.body().getSuccess()) {
                        Log.e("RESPONSE_TAG", "onResponse: " + response);
                        if (response.body().getCurrent_settings().getData().size() > 0) {
                            List<Data> data = response.body().getCurrent_settings().getData();
                            for (Data data1 : data) {
                                tiFine.setText(data1.getFine());
                                tiAmountPerWeek.setText(data1.getAmount());
                                tiName.setText(data1.getName());

                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<MeetingsSettingsFeesResponse> call, Throwable t) {

            }
        });
    }

    private void getMeetingsData(String setting_id, String group_id) {
        Call<meetingsSettingsResponse> call = api.getMeetingsSettings(setting_id, group_id);
        call.enqueue(new Callback<meetingsSettingsResponse>() {
            @Override
            public void onResponse(Call<meetingsSettingsResponse> call, Response<meetingsSettingsResponse> response) {
                try {
                    if (response.body().getSuccess()) {
                        Log.e("MEETINGS_RESPONSE", "onResponse: " + response.body().getCurrent_settings().getData());
                        if (response.body().getCurrent_settings().getData().size() > 0) {
                            List<com.app.ecobba.Models.MeetingsSettingsData.Data> Meetings = response.body().getCurrent_settings().getData();
                            ArrayList<String> available_meetings = new ArrayList<>();
                            //   available_meetings.add(0, getResources().getString(R.string.attach_meeting  ));
                            for (int item = 0; item < Meetings.size(); item++) {
                                available_meetings.add(Meetings.get(item).getName());
                            }
                            arrayAdapter = new ArrayAdapter(ctx, android.R.layout.simple_spinner_dropdown_item, available_meetings);
                            arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            spinner.setAdapter(arrayAdapter);
                            spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                    // String item_resource_type = (String) parent.getItemAtPosition(position);


                                    meeting_index = String.valueOf(Meetings.get(position).getIndex());
                                    meeting_name = Meetings.get(position).getFrequency();
                                    Log.e("MEETING_INDEX", meeting_index);

                                    time = Meetings.get(position).getTime();
                                    frequency = Meetings.get(position).getFrequency();
                                    day = Meetings.get(position).getDay();

                                    ArrayList<String> freqList = new ArrayList();
                                    freqList.add("Weekly");
                                    freqList.add("Monthly");
                                    freqList.add("Annually");
                                    int freqs = freqList.indexOf(frequency);

                                    ArrayList<String> daysList = new ArrayList();
                                    daysList.add("Sunday");
                                    daysList.add("Monday");
                                    daysList.add("Tuesday");
                                    daysList.add("Wednesday");
                                    daysList.add("Thursday");
                                    daysList.add("Friday");
                                    daysList.add("Saturday");
                                    int days = daysList.indexOf(day);

                                    tiContributionStartTime.setText(time);
                                    tiContributionDay.setSelection(days);
                                    tiContributionFrequency.setSelection(freqs);

                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> parent) {

                                }
                            });
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<meetingsSettingsResponse> call, Throwable t) {
                Log.e("MEETINGS_FAILED", "onFailure: " + t.getLocalizedMessage());

            }
        });

    }

    private void fetchUserProfile() {
        userProfileViewModel.getUserProfile();
        userProfileViewModel.userProfileResponseLiveData().observe(getViewLifecycleOwner(), userProfileResponse -> {
            try {
                if (userProfileResponse.getSuccess()) {
                    Detail userDetails = userProfileResponse.getUser().getDetail();
                    String name = userProfileResponse.getUser().getName();

                    user_currency = userProfileResponse.getUser().getWallet().getCurrency().getPrefix();
                    Log.e("MUDHDGDGD", user_currency);
                    currency.setText("(" + user_currency + ")");
                    fine_currency.setText("(" + user_currency + ")");


                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    private void postData() {

        String params = "{ \"fee_name\":" + '"' + setting_name + '"' +
                ", \"amount_contributed\": " + '"' + amount_contributed + '"' +
                ", \"membership_fee_fine\": " + '"' + membership_fee_fine + '"' +
                ", \"fee_frequency\": " + '"' + repayment_plan + '"' +
                ", \"fee_day\": " + '"' + day + '"' +
                ", \"fee_time\": " + '"' + time + '"' +
                ", \"group_id\": " + '"' + group_id + '"' +
                ", \"setting_id\": " + '"' + _setting_id + '"' +
                ", \"meeting_index\": " + '"' + meeting_index + "\"," +
                "\"payout_source\": {"
                + "\"id\": \"" + ada_id + "\","
                + "\"method\": \"" + selected_type + "\","
                + "\"telco\": \"" + selected_provider + "\","
                + "\"msisdn\": \"" + _msisdn + "\"" + "}" +
                "}\n";

        Log.e("post_params", params);
        postToServer(storeFeesSettings, token, params, INITIATOR_STORE_FINE_SETTING, HTTP_POST, ctx, this);
    }

    private boolean validateFormFields() {
        boolean valid = true;

        setting_name = tiName.getText().toString();
        amount_contributed = tiAmountPerWeek.getText().toString();
        membership_fee_fine = tiFine.getText().toString();

        if (membership_fee_fine.isEmpty()) {
            tiFine.setError(getResources().getString(R.string.emptyField));
            valid = false;
        }

        return valid;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btFeesApply:

                if (validateFormFields()) {
                    postData();
                }
                break;

            case R.id.yes:
                Toast toast = Toast.makeText(ctx, "Clicked Yes", Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
                llAttachMeeting.setVisibility(View.VISIBLE);
                llNoAttachMeeting.setVisibility(View.GONE);

                break;
            case R.id.no:
                Toast toasts = Toast.makeText(ctx, "Clicked No", Toast.LENGTH_LONG);
                toasts.setGravity(Gravity.CENTER, 0, 0);
                toasts.show();
                llAttachMeeting.setVisibility(View.GONE);
                llNoAttachMeeting.setVisibility(View.VISIBLE);
                break;
            case R.id.tvTime:
                showHourPicker();
                break;
        }
    }


    public void showHourPicker() {
        final Calendar myCalender = Calendar.getInstance();
        int hour = myCalender.get(Calendar.HOUR_OF_DAY);
        int minute = myCalender.get(Calendar.MINUTE);


        TimePickerDialog.OnTimeSetListener myTimeListener = new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                if (view.isShown()) {
                    myCalender.set(Calendar.HOUR_OF_DAY, hourOfDay);
                    myCalender.set(Calendar.MINUTE, minute);

                    time = hourOfDay + ":" + minute;

                    try {
                        final SimpleDateFormat sdf = new SimpleDateFormat("H:mm");
                        final Date dateObj = sdf.parse(time);
                        //meeting_time = new SimpleDateFormat("hh:mm aa").format(dateObj);

                        meeting_time = new SimpleDateFormat("hh:mm").format(dateObj);
                        tvTime.setText(time);

                        Toast.makeText(ctx, meeting_time, Toast.LENGTH_LONG).show();

                    } catch (final ParseException e) {
                        e.printStackTrace();
                    }

                }
            }
        };
        TimePickerDialog timePickerDialog = new TimePickerDialog(getActivity(), android.R.style.Theme_Holo_Light_Dialog_NoActionBar, myTimeListener, hour, minute, true);
        timePickerDialog.setTitle(getResources().getString(R.string.contribution_start_time));
        timePickerDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        timePickerDialog.show();
    }

    public void fetchFormDropDownData() {

        Call<lendingDataResponse> call = api.getLendingData();
        call.enqueue(new Callback<lendingDataResponse>() {
            @Override
            public void onResponse(Call<lendingDataResponse> call, Response<lendingDataResponse> response) {
                try {
                    if (response.body().getSuccess()) {
                        if (response.body().getMessage().getRepaymentplans().size() > 0) {
                            repayment_plans = new ArrayList<>();
                            List<Repaymentplan> repayment_classes = response.body().getMessage().getRepaymentplans();
                            ArrayList<String> repaymentplans_drop_down = new ArrayList<String>();
                            repaymentplans_drop_down.add(getResources().getString(R.string.select_plan));
                            repayment_plans.add("0");
                            for (int i = 0; i < repayment_classes.size(); i++) {
                                String id = String.valueOf(repayment_classes.get(i).getId());
                                Log.e("Id_data", id);
                                String name = repayment_classes.get(i).getName();
                                repaymentplans_drop_down.add(name);
                                repayment_plans.add(name);

                            }
                            repaymentPlan.setItems(repaymentplans_drop_down);
                            repaymentPlan.setSelectedIndex(0);
                        }

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }


            }

            @Override
            public void onFailure(Call<lendingDataResponse> call, Throwable t) {

            }
        });
    }

    public void fetchAdaPaySourceType() {

        Call<adapaySourcesResponse> call = api.fetchAdapaySources();
        call.enqueue(new Callback<adapaySourcesResponse>() {
            @Override
            public void onResponse(Call<adapaySourcesResponse> call, Response<adapaySourcesResponse> response) {
                try {
                    if (response.body().getSuccess()) {

                        String mobile = response.body().getAdapay_source_types().getMobile_money();
                        String card = response.body().getAdapay_source_types().getCard_hosted();
                        String bank_transfer = response.body().getAdapay_source_types().getBank_transfer();

                        ada_pay_ids = new ArrayList<>();
                        ada_pay_ids.add(0, getResources().getString(R.string.please_select_type));
                        ada_pay_ids.add(1, mobile);
                        ada_pay_ids.add(2, card);
                        ada_pay_ids.add(3, bank_transfer);

                        ArrayAdapter<String> adapter =
                                new ArrayAdapter<String>(ctx, android.R.layout.simple_spinner_dropdown_item, ada_pay_ids);
                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                        payMethod.setAdapter(adapter);

                        payMethod.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                                String type = (String) parent.getItemAtPosition(position);

                                selected_type = "";
                                if (type.equals("Mobile Money")) {
                                    selected_type = "mobile_money";
                                    linearLayoutMobile.setVisibility(View.VISIBLE);
//                                            linearLayoutBank.setVisibility(View.GONE);

                                }
//                                        else if (type.equals("Master/Visa Card")){
//                                            selected_type = "card_hosted";
//                                            linearLayoutBank.setVisibility(View.GONE);
//                                            linearLayoutMobile.setVisibility(View.GONE);
//                                        }
//                                        else if (type.equals("Bank Transfer")){
//                                            selected_type = "bank_transfer";
//                                            linearLayoutBank.setVisibility(View.GONE);
//                                            linearLayoutMobile.setVisibility(View.GONE);
//                                        }
                                else {
//
                                    //    linearLayoutBank.setVisibility(View.GONE);
                                    linearLayoutMobile.setVisibility(View.GONE);
//                            llLayoutMobileMpesa.setVisibility(View.GONE);
                                }
                                Log.e("Selected_type", String.valueOf(type));
                                Log.e("Selected_type2", selected_type);
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {

                            }

                        });

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<adapaySourcesResponse> call, Throwable t) {

            }
        });

    }

    private void fetchUserPaymentSoucers(String group_id) {
        Call<groupWalletResponse> call = api.getGroupWalletPaymentSources(group_id);
        call.enqueue(new Callback<groupWalletResponse>() {
            @Override
            public void onResponse(Call<groupWalletResponse> call, Response<groupWalletResponse> response) {
                try {
                    Log.e("TAG_payment_sources", "onResponse: " + response);
                    if (response.body().getSuccess()) {
                        if (response.body().getAdapay_sources().size() > 0) {

                            List<AdapaySource> sources = response.body().getAdapay_sources();
                            ArrayList<String> availables = new ArrayList<>();
                            // availables.add(0, getResources().getString(R.string.please_select_contribution));
                            for (int item = 0; item < sources.size(); item++) {
                                availables.add(sources.get(item).getMsisdn());
                            }
                            arrayAdapter = new ArrayAdapter(ctx, android.R.layout.simple_spinner_dropdown_item, availables);
                            arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            msisdn.setAdapter(arrayAdapter);
                            msisdn.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                    String item_resource_type = (String) parent.getItemAtPosition(position);

                                    _msisdn = sources.get(position).getMsisdn();
                                    Log.e("PHONE_NUMBER", _msisdn);

                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> parent) {

                                }
                            });
                        }

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }


            }

            @Override
            public void onFailure(Call<groupWalletResponse> call, Throwable t) {

            }
        });
    }

    public void fetchAdaPayTelcos() {
        Call<telcosResponse> call = api.fetchAdapayTelcos();
        call.enqueue(new Callback<telcosResponse>() {
            @Override
            public void onResponse(Call<telcosResponse> call, Response<telcosResponse> response) {
                try {
                    if (response.body().getSuccess() == true) {
                        String telcos = response.body().getAdapay_telcos().getSafaricom_ke();
                        telcos_adapay = new ArrayList<>();
                        telcos_adapay.add(0, getResources().getString(R.string.please_select_type));
                        telcos_adapay.add(1, telcos);
                        ArrayAdapter<String> adapter =
                                new ArrayAdapter<String>(ctx, android.R.layout.simple_spinner_dropdown_item, telcos_adapay);
                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        provider.setAdapter(adapter);
                        provider.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                String type = (String) parent.getItemAtPosition(position);
                                selected_provider = "";
                                if (type.equals("Mpesa")) {
                                    llLayoutMobileMpesa.setVisibility(View.VISIBLE);
                                    selected_provider = "safaricom_ke";
                                } else {
                                    llLayoutMobileMpesa.setVisibility(View.GONE);
                                }
                                Log.e("Selected_telcos", String.valueOf(type));
                                Log.e("Selected_telcos2", selected_provider);
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {

                            }
                        });


                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<telcosResponse> call, Throwable t) {

            }
        });
    }

    @Override
    public void onDataReady(String initiator, String response) {
        switch (initiator) {

            case INITIATOR_STORE_FINE_SETTING:
                try {
                    runOnUI(() -> {
                        Log.e("DEPOSIT RESPONSE", "Response is " + response);
//                                    hideDialog(ctx);
                        JSONObject jObj = null;
                        try {
                            jObj = new JSONObject(response);
                            if (jObj.has("errors")) {
                                String errors = jObj.getString("errors");
                                showSweetAlertDialogError(errors, ctx);
                            } else { // if no error message was found in the response,
                                boolean success = jObj.getBoolean("success");
                                if (success) { // if the request was successfuls, got forward, else show error message
                                    String message = jObj.getString("message");
                                    showSweetAlertDialogSuccess(message, ctx);
                                } else {
                                    String api_error_message = jObj.getString("message");
                                    showSweetAlertDialogError(api_error_message, ctx);
                                }
                            }
                        } catch (JSONException e) {
                            Log.e("ExceptionLOGIN-DATA", e.toString());
                            showSweetAlertDialogError(global_error_message, ctx);
                            e.printStackTrace();
                        }
                    });
                } catch (Exception e) {
//                            hideDialog(ctx);
                    Log.e("Exception- HOME", e.toString());
                    showSweetAlertDialogError(global_error_message, ctx);
//                    Toast.makeText(ctx, "Login Data Response Exception" + e.toString(), Toast.LENGTH_LONG).show();
                }
                break;


        }
    }

    @Override
    public void onDataLoaded(String initiator, JSONArray data) {

    }

    @Override
    public void onNetworkError(String initiator, String errorCode, String errorMessage) {
        switch (initiator) {
            case INITIATOR_STORE_FINE_SETTING:
                try {
                    runOnUI(new Runnable() {
                        @Override
                        public void run() {
//                                    hideDialog(ctx);
                            showSweetAlertDialogError(errorMessage, ctx);
                        }
                    });
                } catch (Exception e) {
//                            hideDialog(ctx);
                    showSweetAlertDialogError(global_error_message, ctx);
                    Log.e("initiator", e.toString());
//                    Toast.makeText(ctx, "Login Error Response Exception" + e.toString(), Toast.LENGTH_LONG).show();
                }
                break;


        }
    }

    @Override
    public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
        switch (view.getId()) {
            case R.id.repaymentPlan:
                Log.d("Repayment plan id is ", repayment_plans.get(position));
                repayment_plan = repayment_plans.get(position);
                break;


        }
    }
}
