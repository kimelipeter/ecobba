package com.app.ecobba.Fragments.Transfer;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.app.ecobba.R;
import com.app.ecobba.Utils.BaseFragment;

import butterknife.ButterKnife;

public class MoneyFragment extends BaseFragment implements View.OnClickListener{

    int wallet = R.drawable.ic_wallet,
            phone = R.drawable.ic_phone,
            card = R.drawable.ic_credit_card;


    Context ctx;

    public MoneyFragment(){}

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View frag = inflater.inflate(R.layout.fragment_money,container,false);
        ButterKnife.bind(this,frag);
        ctx = getActivity();
        return frag;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }


    @Override
    public void onClick(View view) {

    }
}
