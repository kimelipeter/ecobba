package com.app.ecobba.Fragments.Group.Meeting;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.app.ecobba.Models.MeetingDetailsData.Test;
import com.app.ecobba.R;
import com.app.ecobba.common.rxApi.Api;
import com.app.ecobba.common.rxApi.RxApi;

import com.google.android.material.button.MaterialButton;
import com.google.gson.Gson;


import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class MeetingFragment extends Fragment implements View.OnClickListener {


    Context ctx;
    MeetingDetailsAdapter adapter;
    RecyclerView recyclerView;
    Fragment frag;
    String group_id,meeting_id;
    Bundle args;
    Fragment fragment = this;
    String meeting_status="",ready_for_collection="";
    @BindView(R.id.btnMakeContribution)
    MaterialButton btnMakeContribution;

    private Api api;

    @BindView(R.id.tvNoAvailableData)    TextView tvNoAvailableData;


    public MeetingFragment(){

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View frag = inflater.inflate(R.layout.fragment_meeting,container,false);
        ButterKnife.bind(this,frag);

        api = new RxApi().getApi();
        recyclerView = (RecyclerView) frag.findViewById(R.id.recyclerView);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(requireContext());
        adapter = new MeetingDetailsAdapter(new ArrayList(),requireContext(),fragment);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);

        return frag;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this,view);
        ctx = getActivity();
        frag = this;

        args = getArguments();
        assert args != null;
        meeting_id = args.getString("meeting_id");
        Log.e("Meeting_ID_IS","Res"+ " " + meeting_id);
        group_id = args.getString("group_id");
        Log.e("GROUP_ID_IS","Response"+ " " + group_id);
        meeting_status = args.getString("status");
        ready_for_collection = args.getString("ready_for_collection");
        assert meeting_status != null;

        btnMakeContribution.setOnClickListener(this);
        fetchGroupMeetingDetails();
    }

    private void fetchGroupMeetingDetails(){
        Call<Test> call = api.fetchmeetingdeatils(meeting_id);
        call.enqueue(new Callback<Test>() {
            @Override
            public void onResponse(Call<Test> call, Response<Test> response) {
             try {
                 if (response.body().getData().getMember_contributions().size()>0){
                     adapter.dataX = response.body().getData().getMember_contributions();
                     adapter.notifyDataSetChanged();
                 }

                 else {
                     tvNoAvailableData.setText(getResources().getString(R.string.no_available_data));
                     tvNoAvailableData.setVisibility(View.VISIBLE);
                 }
             } catch (Exception e) {
                 e.printStackTrace();
             }
            }

            @Override
            public void onFailure(Call<Test> call, Throwable t) {

            }
        });


    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btnMakeContribution:
                Bundle args = new Bundle();
                args.putString("meeting_id", meeting_id);
                args.putString("group_id", group_id);
                args.putString("status", meeting_status);
                NavHostFragment.findNavController(this).navigate(R.id.groupCollections,args);


            break;

        }
    }


}