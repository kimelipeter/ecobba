
package com.app.ecobba.Fragments.Shop.CreateShopResponse;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
@SuppressWarnings("unused")
public class CreateShopResponseApi {

    private String category;
    private String currency;
    private String description;
    private String email;
    private String img;
    private String location;
    private String name;
    private String phonenumber;
    private String picture;
    private String price;
    private String productname;
    private String shopname;


}
