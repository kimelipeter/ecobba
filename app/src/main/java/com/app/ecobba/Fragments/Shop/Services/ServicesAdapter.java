package com.app.ecobba.Fragments.Shop.Services;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.RecyclerView;

import com.app.ecobba.R;
import com.app.ecobba.recyclerview_models.services.Data;
import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;

import java.util.List;

public class ServicesAdapter extends RecyclerView.Adapter<ServicesAdapter.ViewHolder> {
    public List<Data> dataList;
    public Fragment fragment;
    public Context context;

    public ServicesAdapter(List<Data> dataList, Fragment fragment, Context context) {
        this.dataList = dataList;
        this.fragment = fragment;
        this.context = context;
    }

    @NonNull
    @Override
    public ServicesAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.services_list_item, parent, false);
        return new ServicesAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ServicesAdapter.ViewHolder holder, int position) {
        Data datum = dataList.get(position);
        holder.action_shops.setAnimation(AnimationUtils.loadAnimation(context, R.anim.fade_transition_animation));
        holder.tv_productname.setText(datum.getTitle());
        holder.tv_price.setText(datum.getPrice());
        holder.tv_category.setText(datum.getCategory());
        String service_image_url = datum.getService_image();
        String url_image = datum.getImage();
        Log.e("DATA_IMAGE", service_image_url + " " + url_image);
        String owner = datum.getUser().getName() + " \n" + datum.getUser().getOther_names();
        String profile = datum.getUser().getAvatar();

        holder.shopname.setText(owner);

        String product_id;
        product_id = String.valueOf(datum.getId());
        Log.e("product_id_is", product_id);
        String desc = datum.getDescription();
        String duration = datum.getDuration();
        Glide.with(context)
                .load(profile)
                .error(R.drawable.ic_user_primary).override(40, 40)
                .fitCenter().into(holder.profile_image);


        //  load image from the internet using Picasso
        Picasso.get().load(service_image_url).fit()
                .placeholder(R.color.black)
                .centerCrop().into(holder.product_img);
        holder.view_container.setOnClickListener((view -> {
            //send data to a fragment
            Bundle intent = new Bundle();
            intent.putString("img", datum.getService_image());
            intent.putString("productname", datum.getTitle());
            intent.putString("price", datum.getPrice());
            intent.putString("user_id", product_id);
            intent.putString("description", desc);
            intent.putString("duration", duration);
            intent.putString("profile", profile);
            intent.putString("owner", datum.getUser().getName() + " " + datum.getUser().getOther_names());
            NavHostFragment.findNavController(fragment).navigate(R.id.getServiceFragment, intent);
        }));


    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView tv_productname;
        TextView tv_price;
        TextView tv_category, shopname;
        ImageView product_img, profile_image;
        LinearLayout action_shops;
        androidx.constraintlayout.widget.ConstraintLayout view_container;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            action_shops = itemView.findViewById(R.id.action_shops);
            view_container = itemView.findViewById(R.id.shop_container);
            tv_productname = itemView.findViewById(R.id.productname);
            shopname = itemView.findViewById(R.id.shopname);
            tv_price = itemView.findViewById(R.id.price);
            tv_category = itemView.findViewById(R.id.category);
            product_img = itemView.findViewById(R.id.thumbnail_image);
            profile_image = itemView.findViewById(R.id.profile_image);
        }
    }
}
