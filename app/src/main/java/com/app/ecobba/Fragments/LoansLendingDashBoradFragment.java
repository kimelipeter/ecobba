package com.app.ecobba.Fragments;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.app.ecobba.Fragments.Borrowing.BorrowFragment;
import com.app.ecobba.Fragments.Lending.PackagesFragment;
import com.app.ecobba.Fragments.Web.LoansViewPagerAdapter;
import com.app.ecobba.R;
import com.app.ecobba.Utils.BaseFragment;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;

public class LoansLendingDashBoradFragment extends BaseFragment  {



    public LoansLendingDashBoradFragment(){}

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_loans_lending_dashborad,container,false);


        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        LoansViewPagerAdapter adapter = new LoansViewPagerAdapter(getChildFragmentManager(), getContext());

        ArrayList<Fragment> fragments = new ArrayList<>();
        fragments.add(new PackagesFragment());
        fragments.add(new BorrowFragment());
        adapter.setFragments(fragments);
        ViewPager viewPager = view.findViewById(R.id.pager2);
        viewPager.setAdapter(adapter);
        TabLayout tabLayout = view.findViewById(R.id.tablayout2);
        tabLayout.setupWithViewPager(viewPager);
    }
}