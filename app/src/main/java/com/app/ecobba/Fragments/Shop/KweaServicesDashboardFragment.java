package com.app.ecobba.Fragments.Shop;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.app.ecobba.Fragments.Borrowing.BorrowFragment;
import com.app.ecobba.Fragments.Lending.PackagesFragment;
import com.app.ecobba.R;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;

public class KweaServicesDashboardFragment  extends Fragment {
    public KweaServicesDashboardFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View frag = inflater.inflate(R.layout.fragment_kwea_services_dashboard,container,false);


        return frag;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        KweaServicesViewPagerAdapter adapter = new KweaServicesViewPagerAdapter(getChildFragmentManager(),getContext());
        ArrayList<Fragment> fragments = new ArrayList<>();
        fragments.add(new PackagesFragment());
        fragments.add(new BorrowFragment());
        adapter.setFragments(fragments);
        ViewPager viewPager = view.findViewById(R.id.pager2);
        viewPager.setAdapter(adapter);
        TabLayout tabLayout = view.findViewById(R.id.tablayout2);
        tabLayout.setupWithViewPager(viewPager);
    }
}
