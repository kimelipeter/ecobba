package com.app.ecobba.Fragments.chat

import android.annotation.SuppressLint
import android.app.*
import android.content.Context
import android.graphics.BitmapFactory
import android.os.Bundle
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.navigation.NavDeepLinkBuilder
import com.app.ecobba.Activities.ChatActivity
import com.app.ecobba.Application.GROUP_EMAIL
import com.app.ecobba.Application.MainApplication
import com.app.ecobba.BuildConfig
import com.app.ecobba.Database.ChatMessagesDao
import com.app.ecobba.Database.Db
import com.app.ecobba.Fragments.chat.data.Message
import com.app.ecobba.Fragments.chat.data.Participant
import com.app.ecobba.Fragments.chat.data.SendMessage
import com.app.ecobba.Models.User
import com.app.ecobba.R
import com.app.ecobba.Utils.RxUtils
import com.app.ecobba.common.SharedMethods
import com.app.ecobba.common.rxApi.ApiService
import com.app.ecobba.common.rxApi.RxApi
import com.google.gson.Gson
import io.reactivex.Single


class ChatViewModel(val app: Application) : AndroidViewModel(app) {

    val api: ApiService by lazy { RxApi().apiService }
    val chatDb: ChatMessagesDao by lazy { Db.instance.chatMessagesDao() }
    var loggedInUser = Gson().fromJson<User>(SharedMethods.getDefaults("USER-LOGIN", app), User::class.java)
    val chatContacts: MutableLiveData<HashSet<Participant>> = MutableLiveData()
    val chatChatMessages: MutableLiveData<HashSet<Message>> = MutableLiveData()
    val newChatMessages: MutableLiveData<HashSet<Message>> = MutableLiveData()
    val notifList = ArrayList<Notification>()


    //notifications
    lateinit var notificationManager: NotificationManager
    lateinit var notificationChannel: NotificationChannel
    lateinit var notification: Notification
    lateinit var build: Notification.Builder
    val new_message = "New Message"

    init {
        getMessage()
    }

    @SuppressLint("CheckResult")
    fun getLastTextMessage() {
        api.getChatMessages().compose(RxUtils.applyObservableIoSchedulers()).subscribe({ r ->
            chatContacts.postValue(chatContacts.value.let {
                var contacts = it ?: HashSet()
                contacts.addAll(
                        HashSet<Participant>().let {
                            var combined = HashSet<Participant>()
                            r.inbox.forEach {
                                combined.add(it.receiver.let { p ->
                                    p.lastMessage = it.message
                                    p
                                })
                                combined.add(it.sender.let { p ->
                                    p.lastMessage = it.message
                                    p
                                })
                            }
                            combined
                        }
                )

                contacts
            })

        }, {
        })
    }

    @SuppressLint("CheckResult")
    fun getMessage() {

        Log.d("WorkerChat", "getMessage Called")
        api.getChatMessages().compose(RxUtils.applyObservableIoSchedulers()).subscribe({ r ->

            chatContacts.postValue(chatContacts.value.let {
                var contacts = it ?: HashSet()
                contacts.addAll(
                        HashSet<Participant>().let {
                            var combined = HashSet<Participant>()
                            r.inbox.forEach {
                                combined.add(it.receiver.let { p ->
                                    p.lastMessage = it.message
                                    p
                                })
                                combined.add(it.sender.let { p ->
                                    p.lastMessage = it.message
                                    p
                                })
                            }
                            combined
                        }
                )
                contacts.filter { loggedInUser.id?.toInt() == it.id }
                contacts
            })
            chatChatMessages.postValue(chatChatMessages.value.let {
                var messages = it ?: HashSet()
                messages.addAll(r.inbox)
                messages.addAll(r.outbox)
                storeMessages(messages)
                messages
            })
            /**
             * Save to database if app is in background show notification
             */
        }, {
        })
    }

    @SuppressLint("CheckResult")
    fun storeMessages(messages: HashSet<Message>) {
        val listMessages = messages.toList()
        if (BuildConfig.DEBUG || !MainApplication.APP_IN_FORGROUND) {
            Single.concat(listMessages.map { chatDb.exists(it.id) }).compose(RxUtils.applyFlowableIoSchedulers()).toList()
                    .doFinally {
                        chatDb.insertMessages(listMessages).compose(RxUtils.applyCompletableIoSchedulers()).subscribe({ Log.e("DB_OPERATION", "saved") }, { Log.e("DB_OPERATION", "${it.localizedMessage}") })
                    }
                    .subscribe({
                        Log.e("DB_OPERATION", "${it.filter { it }.size}")
                        val toShowInNotification = it.mapIndexed { index, b -> if (!b) listMessages[index] else null }.filterNotNull()
                        showNotification(toShowInNotification)
                    }, {
                        Log.e("INCOMING_MSG", "${it?.localizedMessage}")
                    })
        } else {
            chatDb.insertMessages(listMessages).compose(RxUtils.applyCompletableIoSchedulers()).subscribe({ Log.e("DB_OPERATION", "saved") }, { Log.e("DB_OPERATION", "${it.localizedMessage}") })
        }


    }

    @SuppressLint("CheckResult")
    fun sendMessage(message: SendMessage) {
        api.sendMessage(message).doAfterNext {
            getMessage()
        }.compose(RxUtils.applyObservableIoSchedulers()).subscribe({ r ->
            chatChatMessages.postValue(chatChatMessages.value.let {
                var messages = it ?: HashSet()
                messages.add(r.message)
                messages
            })
        }, {
        })
    }

    private fun showNotification(messages: List<Message>) {

        if (messages.isEmpty())return
        Log.d("Notification_message", messages.toString())
        messages.forEach { message ->


            val bundle = Bundle()
            bundle.putInt("OTHER_PARTY", message.sender.id)

            val pendingIntent = NavDeepLinkBuilder(app.applicationContext)
                    .setComponentName(ChatActivity::class.java)
                    .setGraph(R.navigation.nav_chat)
                    .setDestination(R.id.chatMessagesFragment)
                    .setArguments(bundle)
                    .createPendingIntent()


            val newMessageNotification = NotificationCompat.Builder(app.applicationContext, "Grouped_notifs")
                    .setSmallIcon(R.drawable.ic_email)
                    .setContentTitle(message.sender.name)
                    .setContentText(message.message)
                    .setLargeIcon(BitmapFactory.decodeResource(app.resources, R.drawable.ic_message_notification))
                    .setGroup(GROUP_EMAIL)
                    .setContentIntent(pendingIntent)
                    .build()

            notifList.add(newMessageNotification)
        }

        val summaryNotification = NotificationCompat.Builder(app.applicationContext, "Grouped_notifs")
                .setContentTitle("Summary")
                //set content text to support devices running API level < 24
                .setContentText("Two new messages")
                .setSmallIcon(R.drawable.ic_email)
                //build summary info into InboxStyle template
                .setStyle(NotificationCompat.InboxStyle()
                        .setSummaryText("${notifList.size} new messages"))
                //specify which group this notification belongs to
                .setGroup(GROUP_EMAIL)
                //set this notification as the summary for the group
                .setGroupSummary(true)
                .build()

        (app.applicationContext.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager).apply {
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                createNotificationChannel(NotificationChannel("Grouped_notifs", "Messages Channel", NotificationManager.IMPORTANCE_DEFAULT))
            }

            Log.e("Number_of_messages", notifList.size.toString())
            notifList.forEachIndexed { index, notification ->
                notify(index, notification)
            }
            notify(123456, summaryNotification)
        }


    }


}