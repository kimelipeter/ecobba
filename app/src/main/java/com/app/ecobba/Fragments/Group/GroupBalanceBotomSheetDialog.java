package com.app.ecobba.Fragments.Group;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.app.ecobba.Models.GroupDetails.GroupDetailsResponse;
import com.app.ecobba.Models.UserProfile.Role;
import com.app.ecobba.Models.UserProfile.UserProfileResponse;
import com.app.ecobba.R;
import com.app.ecobba.common.rxApi.Api;
import com.app.ecobba.common.rxApi.RxApi;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GroupBalanceBotomSheetDialog extends BottomSheetDialogFragment {
    ImageView close;
    private Api api;
    TextView tvGroupName, tvName, tvGroupEmail, tvGroupPhoneNumber, tvPhysicalAddress,tvBankBranch,
            tvBankName,tv_account_number,tvHasBank;
    String group_id;

    public GroupBalanceBotomSheetDialog(String group_id) {

        this.group_id = group_id;

    }

    @Override
    public int getTheme() {
        return R.style.AppBottomSheetDialogTheme;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.group_balance_bottom_sheet, container, false);
        close = view.findViewById(R.id.close);
        tvGroupName = view.findViewById(R.id.tvGroupName);
        tvName = view.findViewById(R.id.tvName);
        tvGroupEmail = view.findViewById(R.id.tvGroupEmail);
        tvGroupPhoneNumber = view.findViewById(R.id.tvGroupPhoneNumber);
        tvPhysicalAddress = view.findViewById(R.id.tvPhysicalAddress);
        tvBankBranch = view.findViewById(R.id.tvBankBranch);
        tvBankName = view.findViewById(R.id.tvBankName);
        tv_account_number = view.findViewById(R.id.tv_account_number);
        tvHasBank = view.findViewById(R.id.tvHasBank);
        setStyle(STYLE_NORMAL, R.style.AppBottomSheetDialogTheme);
        api = new RxApi().getApi();

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        close.setOnClickListener(v -> {
            dismiss();
        });
        getUserProfile();
        fetchGroupDetails(group_id);
    }

    private void fetchGroupDetails(String group_id) {
        Call<GroupDetailsResponse> call = api.fetchGroupDetails(group_id);
        call.enqueue(new Callback<GroupDetailsResponse>() {
            @Override
            public void onResponse(Call<GroupDetailsResponse> call, Response<GroupDetailsResponse> response) {
                try {
                    if (response.body().getSuccess()) {
                        String group_Name = response.body().getData().getGroup().getName();
                        String group_email = response.body().getData().getGroup().getEmail();
                        String group_number = response.body().getData().getGroup().getMsisdn();
                        String physical_address = response.body().getData().getGroup().getPhysical_address();
                        String bank_name=response.body().getData().getGroup().getBank_name();
                        String bank_branch=response.body().getData().getGroup().getBank_branch();
                       String account_number=response.body().getData().getGroup().getAccount_no();
                       String has_bank= String.valueOf(response.body().getData().getGroup().getBank());
                        tvGroupName.setText(group_Name);
                        tvName.setText(group_Name);
                        tvGroupEmail.setText(group_email);
                        tvGroupPhoneNumber.setText(group_number);
                        tvPhysicalAddress.setText(physical_address);

                        if (bank_name == null){
                            tvBankName.setText(R.string.not_applicable);
                        }
                        else {
                            tvBankName.setText(bank_name);
                        }

                        if (bank_branch == null){
                            tvBankBranch.setText(R.string.not_applicable);
                        }
                        else {
                            tvBankBranch.setText(bank_branch);
                        }
                        if (account_number == null){
                            tv_account_number.setText(R.string.not_applicable);
                        }
                        else {
                            tv_account_number.setText(account_number);
                        }
                        if (has_bank.equals("0")){
                            tvHasBank.setText(R.string.no);
                        }
                        else if (has_bank.equals("2")){
                            tvHasBank.setText(R.string.yes);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<GroupDetailsResponse> call, Throwable t) {
                Log.e("FETCH_NOT_CALLED", t.getMessage());

            }
        });

    }


    private void getUserProfile() {
        Call<UserProfileResponse> call = api.getUserProfile();
        call.enqueue(new Callback<UserProfileResponse>() {
            @Override
            public void onResponse(Call<UserProfileResponse> call, Response<UserProfileResponse> response) {
                if (response.body().getSuccess()) {
                    List<Role> roles = response.body().getUser().getRoles();

                }
            }

            @Override
            public void onFailure(Call<UserProfileResponse> call, Throwable t) {


            }
        });
    }
}
