package com.app.ecobba.Fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.allenliu.badgeview.BadgeFactory;
import com.allenliu.badgeview.BadgeView;
import com.app.ecobba.Activities.SingleNotificationActivity;
import com.app.ecobba.Fragments.Shop.KweaFragment;
import com.app.ecobba.Models.Message;
import com.app.ecobba.Models.MessageList;
import com.app.ecobba.R;
import com.app.ecobba.Utils.BaseFragment;
import com.app.ecobba.common.SharedMethods;
import com.app.ecobba.recyclerview_adapters.GenericAdapter;
import com.app.ecobba.recyclerview_adapters.UserNotificationRecycler;
import com.app.ecobba.recyclerview_models.KweaRes.Datum;
import com.app.ecobba.recyclerview_models.UserNotifications;
import com.app.ecobba.recyclerview_models.loansNotification.Loans;
import com.app.ecobba.recyclerview_models.loansNotification.loansNotification;
import com.google.gson.Gson;
import com.snov.timeagolibrary.PrettyTimeAgo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.app.ecobba.common.ApisKtKt.getUserNotifications;
import static com.app.ecobba.common.AppConstantsKt.INITIATOR_GET_USER_NOTIFICATIONS;
import static com.app.ecobba.common.SharedMethods.HTTP_GET;
import static com.app.ecobba.common.SharedMethods.getDefaults;
import static com.app.ecobba.common.SharedMethods.getFromServer;
import static com.app.ecobba.common.SharedMethods.global_error_message;
import static com.app.ecobba.common.SharedMethods.runOnUI;
import static com.app.ecobba.common.SharedMethods.showSweetAlertDialogError;

public class NotificationFragment extends BaseFragment implements View.OnClickListener, SharedMethods.NetworkObjectListener, GenericAdapter.AdapterInterface<Loans,NotificationFragment.NotificationViewHolder> {


    @BindView(R.id.rvNotifications)
    RecyclerView rvNotifications;

    @BindView(R.id.rvSysNotifications)
    RecyclerView rvSysNotifications;

    List<UserNotifications> notifications;

    Context ctx;
    String loansMessage,loansDate;
    private GenericAdapter adapter;
    private RecyclerView rvMessages;
    String dates;
    String borrow_date;



    public NotificationFragment(){

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_notifications,container,false);
        unbinder = ButterKnife.bind(this,view);
        ctx = getContext();
        rvMessages = (RecyclerView) view.findViewById(R.id.rvNotifications);
        adapter = new GenericAdapter<Loans,NotificationViewHolder>(this, rvMessages);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        rvNotifications.setItemAnimator(new DefaultItemAnimator());
        rvNotifications.setLayoutManager(new LinearLayoutManager(ctx));

        rvSysNotifications.setItemAnimator(new DefaultItemAnimator());
        rvSysNotifications.setLayoutManager(new LinearLayoutManager(ctx));

        String token = getDefaults("token",ctx);
        getNotifications(token);
        getSystemMessages();
    }

    private void getNotifications(String token){
        Map<String, String> params = new HashMap<>();
        params.put("token", token);
        getFromServer(getUserNotifications,token,params,INITIATOR_GET_USER_NOTIFICATIONS,HTTP_GET,ctx,this);
    }

    @Override
    public void onClick(View view) {

    }

    @Override
    public void onDataReady(String initiator, String response) {
        switch (initiator) {
            case INITIATOR_GET_USER_NOTIFICATIONS:

                try {
                    loansNotification loansNotification= new Gson().fromJson(response, com.app.ecobba.recyclerview_models.loansNotification.loansNotification.class);
                                Log.e("TAG_LOANS_RESPONSE", "converted :" + loansNotification.toString());
                    loansNotification.getLoans();
                    adapter.addData(Collections.singletonList(loansNotification.getLoans()));
                }
                catch (Exception e){
                    e.printStackTrace();
                }

                break;
        }
    }

    @Override
    public void onDataLoaded(String initiator, JSONArray data) {
        Log.e("DATA_TAG", "loaded :" + data.toString());

    }

    @Override
    public void onNetworkError(String initiator, String errorCode, String errorMessage) {
        switch (initiator){
            case INITIATOR_GET_USER_NOTIFICATIONS:
                try{
                    runOnUI(() -> {
//                    hideDialog(ctx);
//                        showSweetAlertDialogError(errorMessage, ctx);
                    });
                }catch (Exception e){
//                  hideDialog(ctx);
//                    showSweetAlertDialogError(global_error_message, ctx);
                    Log.e(initiator, e.toString());
//                  Toast.makeText(ctx, "Login Error Response Exception" + e.toString(), Toast.LENGTH_LONG).show();
                }

                break;
        }
    }


    public void getSystemMessages(){
        new Thread(){

            @Override
            public void run() {

                String raw_messages = getDefaults("SYSMSG",ctx);
                MessageList messages;
                if (raw_messages!=null){
                    messages = new Gson().fromJson(raw_messages,MessageList.class);
                }else{
                    messages = new MessageList(new ArrayList<>());
                }

                runOnUI(()->{

                    List<UserNotifications> notifications = new ArrayList<>();

                    for (Message m:messages.getMessages()) {

                        notifications.add(new UserNotifications(m.getMessage(),m.getDate()));

                    }
                    Log.e("SYSMSG","--->"+notifications.size());
                    rvSysNotifications.setAdapter(new UserNotificationRecycler(notifications,ctx));

                });

            }
        }.start();

    }

    @Override
    public NotificationViewHolder createViewHolder(ViewGroup parent, int viewType) {
        return new NotificationViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_notification, parent, false));

    }

    @Override
    public void bindViewHolder(NotificationViewHolder viewholder, Loans datum, int position) {

     NotificationViewHolder holder = (NotificationViewHolder) viewholder;


        holder.rvNotificationsItem.setAnimation(AnimationUtils.loadAnimation(ctx, R.anim.fade_transition_animation));
        holder.message.setText(datum.getMessage());


        borrow_date=datum.getDate();
        Log.e("date_is",borrow_date);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        sdf.setTimeZone(TimeZone.getTimeZone("GMT"));

        try {
            Date date1 = sdf.parse(borrow_date);
            sdf = new SimpleDateFormat("dd MMMM yyyy");
            String date = sdf.format(date1);

            sdf =  new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

            Date time = sdf.parse(borrow_date);
            sdf = new SimpleDateFormat("h:mm a");

            String time1 = sdf.format(time);

            Log.e("DateAndTime", "Date " + date + " Time " + time1);
            holder.date.setText(date);
        } catch (ParseException e) {
            Log.e("DateAndTime", e.getLocalizedMessage());
            e.printStackTrace();
        }







        if (datum.getMessage().contains("has")){
            String amount = datum.getMessage().substring(0, datum.getMessage().indexOf(" has")).replace(",", "");
            String t = datum.getMessage().substring(datum.getMessage().indexOf(" titled") + 7);

            Log.e("Loan_Amount_is", "Amount " + amount + " Title " + t);
            Log.e("Amount_Value_is",amount);
            Log.e("Loan_Title_is",t);

            String NameChar=t;
            String sCharGroupName=NameChar.substring(0,1);
            // Log.e("First_letter_Name_is",sCharGroupName);

            String name=t;
            char ch=name.charAt(1);//returns the char value at the 0th index
            Log.e("First_letter_Name_is", String.valueOf(ch));
            holder.rvNotificationsItem.setOnClickListener((view ->{
                Toast toast = Toast.makeText(ctx,"Loan Details", Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
                Intent intent=new Intent(ctx, SingleNotificationActivity.class);
                //set values to intent
                intent.putExtra("loan_name_title",t);
                intent.putExtra("loan_amount",amount);
                intent.putExtra("date_borrowed",datum.getDate());
                // intent.putExtra("loan_char",String.valueOf(ch));
                ctx.startActivity(intent);
            }));
        }
        else if (datum.getMessage().contains("have")){
            String amount = datum.getMessage().substring(0, datum.getMessage().indexOf(" have")).replace(",", "");
            String t = datum.getMessage().substring(datum.getMessage().indexOf(" titled") + 5);
//
//            Log.e("Loan_Amount_is", "Amount " + amount + " Title " + t);
//            Log.e("Amount_Value_is",amount);
            Log.e("Loan_Title_is",t);

//            String NameChar=t;
//            String sCharGroupName=NameChar.substring(0,1);
//            // Log.e("First_letter_Name_is",sCharGroupName);
//
//            String name=t;
            //char ch=name.charAt(1);//returns the char value at the 0th index
           // Log.e("First_letter_Name_is", String.valueOf(ch));
            holder.rvNotificationsItem.setOnClickListener((view ->{
                Toast toast = Toast.makeText(ctx,"Loan Details", Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
                Intent intent=new Intent(ctx, SingleNotificationActivity.class);
                //set values to intent
                intent.putExtra("loan_name_title",t);
//                intent.putExtra("loan_amount",amount);
                intent.putExtra("date_borrowed",datum.getDate());
                // intent.putExtra("loan_char",String.valueOf(ch));
                ctx.startActivity(intent);
            }));
        }






    }


    class NotificationViewHolder extends RecyclerView.ViewHolder {


        TextView message,date;
        LinearLayout rvNotificationsItem;

        public NotificationViewHolder(@NonNull View itemView) {
            super(itemView);
            message = itemView.findViewById(R.id.tvMessage);
            date = itemView.findViewById(R.id.tvDate);
            rvNotificationsItem=itemView.findViewById(R.id.rvNotificationsItem);


        }
    }
}
