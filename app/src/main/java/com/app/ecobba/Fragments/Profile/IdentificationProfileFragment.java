package com.app.ecobba.Fragments.Profile;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;

import com.app.ecobba.Activities.RegistrationActivity;
import com.app.ecobba.Fragments.Profile.viewmodel.ProfileViewModel;
import com.app.ecobba.Models.RegistrationData.Doc;
import com.app.ecobba.Models.UserProfile.User;
import com.app.ecobba.R;
import com.app.ecobba.ViewModel.UserProfileViewModel;
import com.jaredrummler.materialspinner.MaterialSpinner;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import in.mayanknagwanshi.imagepicker.ImageSelectActivity;

import static android.app.Activity.RESULT_OK;

public class IdentificationProfileFragment extends Fragment implements View.OnClickListener {

    UserProfileViewModel userProfileViewModel;

    @BindViews({R.id.etDocumentNumber, R.id.etUploadDoc})
    List<EditText> inputs;
    //    @BindView(R.id.btUpload)
//    Button btUpload;
    EditText etUploadDoc;

    @BindView(R.id.msIdentification)
    MaterialSpinner msIdentification;

    @BindView(R.id.imDocument)
    ImageView ivDocument;
    File identification_document;
    String document_name;
    Uri path;

    private static int num = 6;
    private static final String PDF_DIRECTORY = "/ecobba";
    private static final int BUFFER_SIZE = 1024 * 2;


    Boolean created = false, init = false, populate = false;
    ArrayList<String> identification_drop_down, identification_id;
    String document_number = "", identification = "";
    int document_pos = 0;

    ProfileViewModel profileViewModel;
    File file2;
    Context ctx;
    String fileName;


    public IdentificationProfileFragment(Context ctx) {
        this.ctx = ctx;
        identification_drop_down = new ArrayList<>();
        identification_id = new ArrayList<>();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View frag = inflater.inflate(R.layout.fragment_registration_identification, container, false);
        ButterKnife.bind(this, frag);
        profileViewModel = ViewModelProviders.of(getActivity()).get(ProfileViewModel.class);
        userProfileViewModel = new ViewModelProvider(getActivity()).get(UserProfileViewModel.class);

        return frag;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
//        btUpload.setOnClickListener(this);

        etUploadDoc = view.findViewById(R.id.etUploadDoc);
        etUploadDoc.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onClick(View v) {

                openDirectory();
            }
        });
        created = true;
//        init();

        getIdentificationData();
    }


    @RequiresApi(api = Build.VERSION_CODES.O)
    public void openDirectory() {
        // Choose a directory using the system's file picker.
        String[] mimetypes = {"application/pdf"};
        Intent intent = new Intent();
        intent.setType("application/pdf");
        intent.putExtra(Intent.EXTRA_MIME_TYPES, mimetypes);
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Pdf"), 500);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent resultData) {
        super.onActivityResult(requestCode, resultCode, resultData);
        if (requestCode == 500 && resultCode == Activity.RESULT_OK) {
            Uri uri = null;
            if (resultData != null) {
                File file = requireContext().getCacheDir();
                file2 = new File(file, "temp" + ".pdf");
                Log.e("GAGGAGAGGAAG", file2.getAbsolutePath());
                try {
                    InputStream inputStream = requireContext().getContentResolver().openInputStream(resultData.getData());
                    OutputStream outputStream = new FileOutputStream(file2);
//                    CopyToKt.copy(inputStream, outputStream);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
                // cvpicker.setText(file2.getAbsolutePath());
            }
        }
    }

    public void getIdentificationData() {
        userProfileViewModel.getUserProfile();

        userProfileViewModel.getUserIdentificationDocs();

        userProfileViewModel.userProfileResponseLiveData().observe(getViewLifecycleOwner(), userProfileResponse -> {
           try {
               setData(userProfileResponse.getUser());
           } catch (Exception e) {
               e.printStackTrace();
           }
        });

        userProfileViewModel.userDocResponseLiveData().observe(getViewLifecycleOwner(), docs -> {
            try {
                setInitData(docs);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        });
    }

    public void init() {
        msIdentification.setItems(identification_drop_down);
        if (identification_id.contains(identification)) {
            document_pos = identification_id.indexOf(identification);

        }

        inputs.get(0).setText(document_number);
        msIdentification.setSelectedIndex(document_pos);
        etUploadDoc.setText(fileName);

    }

    public void setInitData(List<Doc> data) throws JSONException {
        identification_drop_down.add(ctx.getResources().getString(R.string.please_select_identification_type));
        identification_id.add("0");

        if (data.size() > 0) {
            for (int i = 0; i < data.size(); i++) {
                identification_drop_down.add(data.get(i).getName());
                identification_id.add(String.valueOf(data.get(i).getId()));
                init();

            }
        }
        init = true;
    }

    public void setData(User data) {
        identification = data.getDetail() != null ? String.valueOf(data.getDetail().getDoc_type()) : "";
        document_number = data.getDetail().getDoc_no();
        document_name = data.getIdentification_document();
         fileName = document_name.substring(document_name.lastIndexOf('/') + 1);
        Log.e("sdfghjklgcytqwerty", fileName);

    }

    public String getData() {
        if (created) {
            String data = ", \"doc_type\": " + '"' + identification_id.get(msIdentification.getSelectedIndex()) + '"' +
                    ", \"doc_no\": " + '"' + inputs.get(0).getText().toString() + '"' +
                    ", \"identification_document\": " + '"' + file2 + '"';
            return data;
        } else {
            return "";
        }

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
//            case R.id.etUploadDoc:
////                ImagePicker.Companion.with(this)
////                        .compress(1024)
////                        .start( (reqCode,data) ->{
////                            ivDocument.setImageURI(data.getData());
////                            return Unit.INSTANCE;
////                        });

        }
    }

}

