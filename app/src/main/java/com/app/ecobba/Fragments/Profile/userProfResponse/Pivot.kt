package com.app.ecobba.Fragments.Profile.userProfResponse

data class Pivot(
    val role_id: Int,
    val user_id: Int,
    val user_type: String
)