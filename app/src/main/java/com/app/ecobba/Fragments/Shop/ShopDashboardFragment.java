package com.app.ecobba.Fragments.Shop;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.ecobba.Fragments.Shop.Adapters.ShopDashboardAdapter;
import com.app.ecobba.Models.shopsdashboard.Data;

import com.app.ecobba.Models.shopsdashboard.shopsDataResponse;
import com.app.ecobba.Utils.SwipeDetectCallback;
import com.app.ecobba.common.rxApi.Api;
import com.app.ecobba.common.rxApi.RxApi;

import com.bumptech.glide.request.RequestOptions;

import com.app.ecobba.R;
import com.google.android.material.card.MaterialCardView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;


import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ShopDashboardFragment extends Fragment {
    private static final String TAG = "KWEA_FRAGMENT";


    private String token = "";
    private RecyclerView rvShops;
    private Api api;
    Fragment fragment = this;
    private MaterialCardView cardHeader;

    private RecyclerView.ViewHolder viewHolder;
    private ShopDashboardAdapter adapter;
    private List<Data> mData;
    RequestOptions options;
    private Context mContext;
    @BindView(R.id.tvNoAvailableData)
    TextView tvNoAvailableData;
    ProgressDialog progressDialog;

    public ShopDashboardFragment() {
        // Required empty public constructor
    }


    public static ShopDashboardFragment newInstance(String param1, String param2) {
        ShopDashboardFragment fragment = new ShopDashboardFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mContext = requireContext();
        View view = inflater.inflate(R.layout.fragment_dashbord_shop, container, false);
        ButterKnife.bind(this, view);
        rvShops = (RecyclerView) view.findViewById(R.id.recyclerViewShopList);

        adapter = new ShopDashboardAdapter(new ArrayList<>(), fragment, requireContext());
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(requireContext());
        rvShops.setLayoutManager(layoutManager);
        rvShops.setAdapter(adapter);
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(new SwipeDetectCallback(adapter));
        itemTouchHelper.attachToRecyclerView(rvShops);

        api = new RxApi().getApi();
        //rvShops.setHasFixedSize(true);
        FloatingActionButton fab = (FloatingActionButton) view.findViewById(R.id.create_myshop);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                NavHostFragment.findNavController(ShopDashboardFragment.this).navigate(R.id.addShopFragment);

            }
        });

        return view;

    }

    @Override
    public void onResume() {
        super.onResume();
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        progressDialog = new ProgressDialog(mContext);
        getshops();

    }

    private void getshops() {
        progressDialog.setMessage("Loading.Please wait....");
        progressDialog.setCancelable(false);
        progressDialog.show();
        Call<shopsDataResponse> call = api.fetchshops();
        call.enqueue(new Callback<shopsDataResponse>() {
            @Override
            public void onResponse(Call<shopsDataResponse> call, Response<shopsDataResponse> response) {

                try {
                    if (response.body().getSuccess()) {
                        progressDialog.dismiss();
                        if (response.body().getData().size() > 0) {

                            adapter.dataList = response.body().getData();
                            adapter.notifyDataSetChanged();

                        } else {
                            tvNoAvailableData.setText(getResources().getString(R.string.no_available_data));
                            tvNoAvailableData.setVisibility(View.VISIBLE);
                        }

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<shopsDataResponse> call, Throwable t) {

            }
        });


    }


}