package com.app.ecobba.Fragments.chat.data

data class ChatMessagesResponse(
        val inbox: List<Message>,
        val outbox: List<Message>
)