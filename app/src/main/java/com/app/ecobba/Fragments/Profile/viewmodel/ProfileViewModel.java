package com.app.ecobba.Fragments.Profile.viewmodel;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.app.ecobba.Models.User;

public class ProfileViewModel extends ViewModel {
    public MutableLiveData<User> liveResponse = new MutableLiveData<>();
    public ProfileViewModel(){
        liveResponse.observeForever(user -> {
            Log.e("user_observer","view model posted");
        });
    }
}
