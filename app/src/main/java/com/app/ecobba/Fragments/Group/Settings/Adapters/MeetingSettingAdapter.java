package com.app.ecobba.Fragments.Group.Settings.Adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;


import com.app.ecobba.Models.MeetingsSettingsData.Data;
import com.app.ecobba.R;

import java.util.List;

public class MeetingSettingAdapter extends RecyclerView.Adapter<MeetingSettingAdapter.ViewHolder> {
    public List<Data> dataList;
    public Context context;
    public Fragment fragment;

    public MeetingSettingAdapter(List<Data> dataList, Context context, Fragment fragment) {
        this.dataList = dataList;
        this.context = context;
        this.fragment = fragment;
    }

    @NonNull
    @Override
    public MeetingSettingAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.list_meeting_settings, parent, false);
        return new MeetingSettingAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MeetingSettingAdapter.ViewHolder holder, int position) {
        Data datum = dataList.get(position);

        try {
            holder.tvMeetingName.setText(datum.getName());
            holder.tvDay.setText(datum.getDay());
            holder.tvFrequency.setText(datum.getFrequency());
            holder.tvPeriods.setText(datum.getFinancial_period());
            holder.tvTime.setText(datum.getTime());
            holder.tvVenue.setText(datum.getVenue());

            String name = datum.getName().substring(0, 1);

            Log.e("First_letter_Name_is", name);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvMeetingName, tvVenue, tvFrequency, tvDay, tvTime, tvPeriods, meeting_char;
        CardView btnApplyLoan;
        Button btnApply;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvMeetingName = itemView.findViewById(R.id.tvMeetingName);
            tvVenue = itemView.findViewById(R.id.tvVenue);
            tvFrequency = itemView.findViewById(R.id.tvFrequency);
            tvDay = itemView.findViewById(R.id.tvDay);
            tvTime = itemView.findViewById(R.id.tvTime);
            tvPeriods = itemView.findViewById(R.id.tvPeriods);
            // meeting_char=itemView.findViewById(R.id.meeting_char);
        }
    }
}
