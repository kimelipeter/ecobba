package com.app.ecobba.Fragments.Group.Contributions.Adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.app.ecobba.Models.payment.AVAILABLECONTRIBUTIONS;

import com.app.ecobba.R;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;


public class AvailableContributionsAdapter extends RecyclerView.Adapter<AvailableContributionsAdapter.ViewHolder> {
    public List<AVAILABLECONTRIBUTIONS> datalist;
    public Context context;
    public Fragment fragment;
    public String user_currency;

    public AvailableContributionsAdapter(List<AVAILABLECONTRIBUTIONS> datalist, Context context, Fragment fragment,String user_currency) {
        this.datalist = datalist;
        this.context = context;
        this.fragment = fragment;
        this.user_currency = user_currency;
        Log.e("USER_CURRENCY",user_currency);

    }


    @NonNull
    @Override
    public AvailableContributionsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.list_items_available_contributions, parent, false);
        return new AvailableContributionsAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AvailableContributionsAdapter.ViewHolder holder, int position) {

        String name = datalist.get(position).getContribution_name();
        String type = datalist.get(position).getType();
        String status = datalist.get(position).getStatus();
        String cash =user_currency+" "+ datalist.get(position).getAmount();
        String dateString = datalist.get(position).getContribution_date();
        holder.tvContributionName.setText(name);

        holder.tvCash.setText(cash);
        if (status.equals("ended")) {
            holder.tvStatus.setText(R.string.ended);
        } else if (status.equals("active")) {
            holder.tvStatus.setText(R.string.active);
        }

        if (type.equals("society")){
            holder.tvType.setText(R.string.welfare_fee);
        }
        else if (type.equals("shares")){
            holder.tvType.setText(R.string.share);
        }


        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        sdf.setTimeZone(TimeZone.getTimeZone("GMT"));

        try {
            Date date1 = sdf.parse(dateString);
            sdf = new SimpleDateFormat("dd MMMM yyyy");
            String date = sdf.format(date1);

            sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");

            Date time = sdf.parse(dateString);
            sdf = new SimpleDateFormat("h:mm a");

            String time1 = sdf.format(time);

            Log.e("DateAndTime", "Date " + date + " Time " + time1);
//            holder.tvTimeDifference.setText(time1);
            holder.tvDateCreated.setText(date);
        } catch (ParseException e) {
            Log.e("DateAndTime", e.getLocalizedMessage());
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return datalist.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tvContributionName, tvStatus, tvCash, tvType, tvDateCreated;
        LinearLayout meetingLl;

        public ViewHolder(View itemView) {
            super(itemView);
            tvStatus = itemView.findViewById(R.id.tvStatus);
            tvCash = itemView.findViewById(R.id.tvCash);
            tvDateCreated = itemView.findViewById(R.id.tvDateCreated);
            tvContributionName = itemView.findViewById(R.id.tvContributionName);
            tvType = itemView.findViewById(R.id.tvType);
        }

    }

}
