package com.app.ecobba.Fragments.Group.Settings;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProvider;

import com.app.ecobba.R;
import com.app.ecobba.ViewModel.UserProfileViewModel;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ContributionSettingDetailsBotomSheetDialog extends BottomSheetDialogFragment {
    UserProfileViewModel userProfileViewModel;
    ImageView close;
    Context context;
    String name, min, max, frequency, value, day, fine, time, type, user_currency,source_type,telco,msisdn;

    @BindView(R.id.tvName)
    TextView tvName;
    @BindView(R.id.tvContributionName)
    TextView tvContributionName;
    @BindView(R.id.tvMinAmount)
    TextView tvMinAmount;
    @BindView(R.id.tvMaxAmount)
    TextView tvMaxAmount;
    @BindView(R.id.tvValue)
    TextView tvValue;
    @BindView(R.id.tvDay)
    TextView tvDay;
    @BindView(R.id.tvTime)
    TextView tvTime;
    @BindView(R.id.tvFine)
    TextView tvFine;
    @BindView(R.id.tvFrequency)
    TextView tvFrequency;
    @BindView(R.id.tvPayoutMethod)
    TextView tvPayoutMethod;
    @BindView(R.id.tvProvider)
    TextView tvProvider;
    @BindView(R.id.tvPhoneNumber)
    TextView tvPhoneNumber;
    @BindView(R.id.tvContributionType)
    TextView tvContributionType;

    public ContributionSettingDetailsBotomSheetDialog(String name, String min, String max, String frequency, String value, String day, String fine, String time, String type, String user_currency, String source_type, String telco, String msisdn) {
        this.name = name;
        this.min = min;
        this.max = max;
        this.frequency = frequency;
        this.value = value;
        this.day = day;
        this.fine = fine;
        this.time = time;
        this.type = type;
        this.user_currency = user_currency;
        this.source_type=source_type;
        this.telco=telco;
        this.msisdn=msisdn;
    }


    @Override
    public int getTheme() {
        return R.style.AppBottomSheetDialogTheme;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.contribution_bottom_sheet, container, false);
        ButterKnife.bind(this, view);
        context = requireActivity();
        close = view.findViewById(R.id.close);

        setStyle(STYLE_NORMAL, R.style.AppBottomSheetDialogTheme);
        userProfileViewModel = new ViewModelProvider(getActivity()).get(UserProfileViewModel.class);

        return view;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        close.setOnClickListener(v -> {
            dismiss();
        });
        tvName.setText(name);
        tvContributionName.setText(name);
        tvValue.setText(user_currency + " " + value);
        tvDay.setText(day);
        tvTime.setText(time);
        tvFine.setText(fine+"%");
        tvFrequency.setText(frequency);

        if (type.equals("hisa")){
            tvContributionType.setText(getResources().getString(R.string.share));
        }
        else if (type.equals("jamii")){
            tvContributionType.setText(getResources().getString(R.string.welfare));
        }

        if (source_type.equals("mobile_money")){
            tvPayoutMethod.setText(R.string.mobile_money);
        }

        if (telco.equals("safaricom_ke")){
            tvProvider.setText(R.string.safaricom_ke);
        }
        tvPhoneNumber.setText(msisdn);


        if (max == null) {
            tvMaxAmount.setText(getResources().getString(R.string.not_applicable));
        } else {
            tvMaxAmount.setText(user_currency + " " + max);
        }
        if (min == null) {
            tvMinAmount.setText(getResources().getString(R.string.not_applicable));
        } else {
            tvMinAmount.setText(user_currency + " " + min);
        }
    }
}
