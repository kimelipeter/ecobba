package com.app.ecobba.Fragments.Shop.MyKweaProducts;

import com.google.gson.annotations.SerializedName;

import retrofit2.http.Field;
import retrofit2.http.Path;

public class PatchKweaItem {

    private String item_name;
    private String item_category;
    private  String item_price;
    private  String item_currency;
    private  String item_description;

    public PatchKweaItem(String item_name, String item_category, String item_price, String item_currency, String item_description) {
        this.item_name = item_name;
        this.item_category = item_category;
        this.item_price = item_price;
        this.item_currency = item_currency;
        this.item_description = item_description;
    }

    public String getItem_name() {
        return item_name;
    }

    public void setItem_name(String item_name) {
        this.item_name = item_name;
    }

    public String getItem_category() {
        return item_category;
    }

    public void setItem_category(String item_category) {
        this.item_category = item_category;
    }

    public String getItem_price() {
        return item_price;
    }

    public void setItem_price(String item_price) {
        this.item_price = item_price;
    }

    public String getItem_currency() {
        return item_currency;
    }

    public void setItem_currency(String item_currency) {
        this.item_currency = item_currency;
    }

    public String getItem_description() {
        return item_description;
    }

    public void setItem_description(String item_description) {
        this.item_description = item_description;
    }
}
