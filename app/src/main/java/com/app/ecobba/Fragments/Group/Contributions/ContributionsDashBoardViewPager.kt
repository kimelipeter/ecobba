package com.app.ecobba.Fragments.Group.Contributions

import android.content.Context
import android.content.res.Resources
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.app.ecobba.R

class ContributionsDashBoardViewPager(fm: FragmentManager, val context: Context) :
        FragmentPagerAdapter(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {
    private var childFragments = ArrayList<Fragment>()
    var resources: Resources? = null

    override fun getItem(position: Int): Fragment {

        return when (position) {
            0 -> AvailableContributions()
            1 -> WelfareFeeFragment()
            2 -> ShareContributionsFragment()
            3 -> MemberShipFeeFragment()
            else -> AvailableContributions()
        }
    }

    fun setFragments(fragments: ArrayList<Fragment>) {
        childFragments = fragments
    }


    override fun getCount(): Int {
        return 4
        //Log.e("Viewpager_size", childFragments.toString())
    }

    override fun getPageTitle(position: Int): CharSequence? {

        var title: String? = null
        if (position == 0) {
            title = context.getString(R.string.available_contributions)

        } else if (position == 1) {
            title = context.getString(R.string.welfarefee)
        }
        else if (position == 2) {
            title = context.getString(R.string.share)
        }
        else if (position == 3) {
            title = context.getString(R.string.membership_fee)
        }
        return title
    }

    fun String.capitalizeWords(): String = split(" ").joinToString(" ") {
        it.capitalize()
    }
}
