package com.app.ecobba.Fragments.Group.Loans;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.ecobba.Activities.MainActivity;
import com.app.ecobba.Fragments.Group.ViewMoreFragments.ViewMoreMembersAdapter;
import com.app.ecobba.Models.GroupLoanApplication.groupLoanApplicationResponse;
import com.app.ecobba.Models.GroupMmembers.Data;
import com.app.ecobba.Models.GroupMmembers.groupMembersResponse;
import com.app.ecobba.Models.PaymentSources.AdapaySource;
import com.app.ecobba.Models.PaymentSources.paymentSourcesResponse;
import com.app.ecobba.Models.adapay.adapaySourcesResponse;
import com.app.ecobba.Models.adapay.telcos.telcosResponse;
import com.app.ecobba.R;
import com.app.ecobba.common.SharedMethods;
import com.app.ecobba.common.rxApi.Api;
import com.app.ecobba.common.rxApi.RxApi;
import com.google.gson.Gson;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


import static com.app.ecobba.common.ApisKtKt.confirmLoanApplication;
import static com.app.ecobba.common.ApisKtKt.loanApplication;

import static com.app.ecobba.common.AppConstantsKt.INITIATOR_APPLY_GROUP_LOAN;
import static com.app.ecobba.common.AppConstantsKt.INITIATOR_APPLY_LOAN;
import static com.app.ecobba.common.AppConstantsKt.INITIATOR_CONFIRM_APPLY_LOAN;
import static com.app.ecobba.common.AppConstantsKt.INITIATOR_GETLENDING_DATA;

import static com.app.ecobba.common.SharedMethods.HTTP_POST;

import static com.app.ecobba.common.SharedMethods.global_error_message;
import static com.app.ecobba.common.SharedMethods.postToServer;
import static com.app.ecobba.common.SharedMethods.runOnUI;
import static com.app.ecobba.common.SharedMethods.showAlertAndMoveToPage;
import static com.app.ecobba.common.SharedMethods.showSweetAlertDialogError;
import static com.app.ecobba.common.SharedMethods.showSweetAlertDialogSuccess;

public class ApplyGroupLoanFragment extends Fragment implements
        View.OnClickListener,
        SharedMethods.NetworkObjectListener {

    @BindView(R.id.etConfirmPassword)
    FrameLayout confirm;
    @BindView(R.id.vfLoanApplication)
    ViewFlipper vfLoanApplication;
    @BindViews({R.id.btConfirm, R.id.cancel_apply_loan, R.id.btApplyLoan, R.id.btApplyContinue, R.id.btContinue})
    List<Button> buttons;
    @BindView(R.id.sbApplyLoan)
    SeekBar sbApplyLoan;
    @BindView(R.id.pbApplyLoan)
    ProgressBar load;

    @BindView(R.id.input_loan_amount)
    EditText input_loan_amount;

    @BindView(R.id.reason_of_loan)
    EditText reason_of_loan;
    @BindView(R.id.input_loan_length)
    EditText input_loan_length;
    @BindView(R.id.input_loan_name)
    EditText input_loan_name;

    @BindView(R.id.tvMinAmount)
    TextView min_amount;
    @BindView(R.id.repayment_period)
    TextView repayment_period;
    @BindView(R.id.interest_rate)
    TextView interest_rate;
    @BindView(R.id.repayment_plan)
    TextView repayment_plan;
    @BindView(R.id.package_name)
    TextView package_name;
    @BindView(R.id.late_fines)
    TextView late_fines;
    @BindView(R.id.tvPeriod)
    TextView tvPeriod;
    @BindView(R.id.tv_loan_title_name)
    TextView tv_loan_title_name;
    @BindView(R.id.principle_amount)
    TextView tv_principle_amount;
    @BindView(R.id.total_interest_amount)
    TextView tv_total_interest_amount;
    @BindView(R.id.tv_payment_frequency)
    TextView tv_payment_frequency;
    @BindView(R.id.tv_currency)
    TextView tv_currency;
    @BindView(R.id.sub_totals_value)
    TextView tv_sub_totals_value;
    @BindView(R.id.processing_fee)
    TextView tv_processing_fee;
    @BindView(R.id.annual_interest_rate)
    TextView tv_annual_interest_rate;
    @BindView(R.id.tv_no_of_installments)
    TextView tv_no_of_installments;
    @BindView(R.id.grand_total)
    TextView tv_grand_total;
    @BindView(R.id.monthly_amount_deducted)
    TextView tv_monthly_amount_deducted;
    @BindView(R.id.tvMaxAmount)
    TextView tvMaxAmount;


    String loan_title_name, principle_amount, total_interest_amount, frequency, Loancurrency,
            installments, name, exchange_rate = "", prefix, created_at, updated_at, type, usage, telco, my_msisdn, my_token, source_default;
    Double sub_totals_value;
    Double instalment_amount_deducted;
    Double sum;
    Integer periodInterest;
    Integer totalFines;
    Integer periodFine;
    Double processing_fee;
    Double annual_interest_rate;
    Double totalAmount;
    int currency_id;
    int status;
    int country_id;
    int source_id;
    int user_id;


    String descriptionOfLoan, expectedScore, score, fineRate;


    ColorDrawable popupColor;
    @BindView(R.id.progress_dialog)
    ProgressBar progress_dialog;


    private String group_id, id, token, loan_name, fine_rate, loan_limit, interest, payment_frequency, payment_period, my_repayment_plan = "0";
    private Context context;
    String _reasonOfLoan, inputLoanAmount, loanLimit, _input_loan_length;
    Bundle args;
    Fragment fragment;
    List<String> phoneNumber = new ArrayList<>();

    private Api api;
    ArrayAdapter arrayAdapter;
    Spinner msisdn;
    private ArrayList<GroupMembers> groupMembers = new ArrayList<>();
    List<String> ada_pay_ids, telcos_adapay;
    LinearLayout linearLayoutBank;
    LinearLayout linearLayoutMobile, llLayoutMobileMpesa;
    Spinner payMethod, provider;
    String selected_type, selected_provider, _msisdn, ada_id = "";
    @BindView(R.id.recyclerViewMembers)
    RecyclerView recyclerViewMembers;

    List<Data> data = new ArrayList<>();
    GetGroupMembersGuarantorsAdapter adapter;

    public ApplyGroupLoanFragment() {

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View frag = inflater.inflate(R.layout.fragment_apply_group_loan, container, false);
        ButterKnife.bind(this, frag);
        msisdn = frag.findViewById(R.id.msisdn);
        payMethod = frag.findViewById(R.id.payMethod);
        provider = frag.findViewById(R.id.provider);

        msisdn = frag.findViewById(R.id.msisdn);
        linearLayoutMobile = frag.findViewById(R.id.linearLayoutMobile);
        llLayoutMobileMpesa = frag.findViewById(R.id.llLayoutMobileMpesa);
        linearLayoutBank = frag.findViewById(R.id.linearLayoutBank);
        popupColor = new ColorDrawable(getResources().getColor(R.color.colorSurface));
        recyclerViewMembers = frag.findViewById(R.id.recyclerViewMembers);
        fragment = this;


        adapter = new GetGroupMembersGuarantorsAdapter(data, requireContext(), fragment, new GetGroupMembersGuarantorsAdapter.OnItemCheckListener() {

            @Override
            public void onItemChecked(Data data) {
                phoneNumber.add(data.getMsisdn());
                Log.d("TAG", "onItemChecked: " + phoneNumber);
            }

            @Override
            public void onItemUnChecked(Data data) {
                phoneNumber.remove(data.getMsisdn());
                Log.d("TAG", "onItemChecked: Removed: " + phoneNumber);

            }

        });

        RecyclerView.LayoutManager layoutManager1 = new LinearLayoutManager(requireContext());
        recyclerViewMembers.setLayoutManager(layoutManager1);
        recyclerViewMembers.setAdapter(adapter);

        for (Button b : buttons) {
            b.setOnClickListener(this);
        }
        setPage(0);

        api = new RxApi().getApi();
        return frag;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ButterKnife.bind(this, view);
        context = getActivity();
        fragment = this;
        fetchAdaPaySourceType();
        fetchAdaPayTelcos();
        fetchUserPaymentSoucers();

        args = getArguments();
        assert args != null;
        group_id = args.getString("group_id");
        id = args.getString("id");
        String max = args.getString("min");
        String min = args.getString("min");
        loan_name = args.getString("loan_name");
        fine_rate = args.getString("fine_rate");
        loan_limit = args.getString("loan_limit");
        interest = args.getString("interest");
        payment_frequency = args.getString("payment_frequency");
        payment_period = args.getString("payment_period");
        loanLimit = args.getString("loanLimit");
        repayment_period.setText(payment_period);
        package_name.setText(loan_name);
        late_fines.setText(fine_rate + "%");

        interest_rate.setText(interest);
        min_amount.setText(min);
        tvMaxAmount.setText(max);
        token = SharedMethods.getDefaults("token", context);
        fetchGroupMembers(group_id);

        input_loan_name.setText(loan_name);
        if (payment_frequency.equals("1")) {
            tvPeriod.setText(getResources().getString(R.string.weekly));
            repayment_plan.setText(getResources().getString(R.string.weekly));
        } else if (payment_frequency.equals("3")) {
            tvPeriod.setText(getResources().getString(R.string.monthly));
            repayment_plan.setText(getResources().getString(R.string.monthly));
        } else if (payment_frequency.equals("4")) {
            tvPeriod.setText(getResources().getString(R.string.annually));
            repayment_plan.setText(getResources().getString(R.string.annually));
        }

    }


    private void fetchGroupMembers(String group_id) {
        progress_dialog.setVisibility(View.VISIBLE);
        Call<groupMembersResponse> call = api.getGroupMmebers(group_id);
        call.enqueue(new Callback<groupMembersResponse>() {
            @Override
            public void onResponse(Call<groupMembersResponse> call, Response<groupMembersResponse> response) {
                try {
                    Log.e("MEMBERS", "onResponse: " + response);
                    if (response.body().getSuccess()) {
                        progress_dialog.setVisibility(View.GONE);
                        if (response.body().getData().size() > 0) {
                            adapter.datalist = response.body().getData();
                            adapter.notifyDataSetChanged();
                        }
                    } else if (!response.body().getSuccess()) {
                        String api_error_message = "Sorry";
                        showSweetAlertDialogError(api_error_message, context);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<groupMembersResponse> call, Throwable t) {
                Log.e("FAILGED_TO_GET", "onFailure: " + t.getLocalizedMessage());
            }
        });
    }


    public void fetchAdaPaySourceType() {

        Call<adapaySourcesResponse> call = api.fetchAdapaySources();
        call.enqueue(new Callback<adapaySourcesResponse>() {
            @Override
            public void onResponse(Call<adapaySourcesResponse> call, Response<adapaySourcesResponse> response) {
                try {
                    if (response.body().getSuccess()) {
                        String mobile = response.body().getAdapay_source_types().getMobile_money();
                        String card = response.body().getAdapay_source_types().getCard_hosted();
                        String bank_transfer = response.body().getAdapay_source_types().getBank_transfer();

                        ada_pay_ids = new ArrayList<>();
                        ada_pay_ids.add(0, getResources().getString(R.string.please_select_type));
                        ada_pay_ids.add(1, mobile);
                        ada_pay_ids.add(2, card);
                        ada_pay_ids.add(3, bank_transfer);

                        ArrayAdapter<String> adapter =
                                new ArrayAdapter<String>(context, android.R.layout.simple_spinner_dropdown_item, ada_pay_ids);
                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                        payMethod.setAdapter(adapter);

                        payMethod.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                                String type = (String) parent.getItemAtPosition(position);

                                selected_type = "";
                                if (type.equals("Mobile Money")) {
                                    selected_type = "mobile_money";
                                    linearLayoutMobile.setVisibility(View.VISIBLE);
                                    linearLayoutBank.setVisibility(View.GONE);

                                } else if (type.equals("Master/Visa Card")) {
                                    selected_type = "card_hosted";
                                    linearLayoutBank.setVisibility(View.GONE);
                                    linearLayoutMobile.setVisibility(View.GONE);
                                } else if (type.equals("Bank Transfer")) {
                                    selected_type = "bank_transfer";
                                    linearLayoutBank.setVisibility(View.GONE);
                                    linearLayoutMobile.setVisibility(View.GONE);
                                } else {

                                    linearLayoutBank.setVisibility(View.GONE);
                                    linearLayoutMobile.setVisibility(View.GONE);
                                }
                                Log.e("Selected_type", String.valueOf(type));
                                Log.e("Selected_type2", selected_type);
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {

                            }

                        });

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<adapaySourcesResponse> call, Throwable t) {

            }
        });

    }

    public void fetchAdaPayTelcos() {
        Call<telcosResponse> call = api.fetchAdapayTelcos();
        call.enqueue(new Callback<telcosResponse>() {
            @Override
            public void onResponse(Call<telcosResponse> call, Response<telcosResponse> response) {
                try {
                    if (response.body().getSuccess()) {
                        String telcos = response.body().getAdapay_telcos().getSafaricom_ke();
                        telcos_adapay = new ArrayList<>();
                        telcos_adapay.add(0, getResources().getString(R.string.please_select_type));
                        telcos_adapay.add(1, telcos);
                        ArrayAdapter<String> adapter =
                                new ArrayAdapter<String>(context, android.R.layout.simple_spinner_dropdown_item, telcos_adapay);
                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        provider.setAdapter(adapter);
                        provider.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                String type = (String) parent.getItemAtPosition(position);
                                selected_provider = "";
                                if (type.equals("Mpesa")) {
                                    llLayoutMobileMpesa.setVisibility(View.VISIBLE);
                                    selected_provider = "safaricom_ke";
                                } else {
                                    llLayoutMobileMpesa.setVisibility(View.GONE);
                                }
                                Log.e("Selected_telcos", String.valueOf(type));
                                Log.e("Selected_telcos2", selected_provider);
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {

                            }
                        });


                    }
                } catch (Resources.NotFoundException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<telcosResponse> call, Throwable t) {
                t.getLocalizedMessage();
            }
        });
    }

    private void fetchUserPaymentSoucers() {
        Call<paymentSourcesResponse> call = api.getUserPayPrefences();
        call.enqueue(new Callback<paymentSourcesResponse>() {
            @Override
            public void onResponse(Call<paymentSourcesResponse> call, Response<paymentSourcesResponse> response) {

                try {
                    Log.e("TAG_payment_sources", "onResponse: " + response);
                    if (response.body().getSuccess()) {
                        if (response.body().getAdapay_sources().size() > 0) {
                            List<AdapaySource> sources = response.body().getAdapay_sources();
                            ArrayList<String> availables = new ArrayList<>();
                            // availables.add(0, getResources().getString(R.string.please_select_contribution));
                            for (int item = 0; item < sources.size(); item++) {
                                availables.add(sources.get(item).getMsisdn());
                            }
                            arrayAdapter = new ArrayAdapter(context, android.R.layout.simple_spinner_dropdown_item, availables);
                            arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            msisdn.setAdapter(arrayAdapter);
                            msisdn.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                    String item_resource_type = (String) parent.getItemAtPosition(position);

                                    _msisdn = sources.get(position).getMsisdn();
                                    Log.e("PHONE_NUMBER", _msisdn);

                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> parent) {

                                }
                            });
                        } else {

                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }


            }

            @Override
            public void onFailure(Call<paymentSourcesResponse> call, Throwable t) {

            }
        });
    }


    private void setPage(int n) {
        int m = vfLoanApplication.getChildCount() - 1;
        sbApplyLoan.setMax(m);
        if (n <= m && n >= 0) {
            vfLoanApplication.setDisplayedChild(n);
            sbApplyLoan.setProgress(n);
            if (n == m) {
                confirm.setVisibility(View.GONE);
            }
        }
    }

    private void toggleProg() {
        if (load.getVisibility() == View.GONE) {
            load.setVisibility(View.VISIBLE);
        } else {
            load.setVisibility(View.VISIBLE);
        }
    }


    private void loanApplicationToAPI() {

        String params = "{ \"group_id\":" + '"' + group_id + '"' +
                ", \"amount\": " + '"' + inputLoanAmount + '"' +
                ", \"length_of_loan\": " + '"' + _input_loan_length + '"' +
                ", \"loan_title\": " + '"' + loan_name + '"' +
                ", \"loan_description\": " + '"' + _reasonOfLoan + "\"," +
                "\"loan_source\": {"
                + "\"id\": \"" + ada_id + "\","
                + "\"method\": \"" + selected_type + "\","
                + "\"telco\": \"" + selected_provider + "\","
                + "\"msisdn\": \"" + _msisdn + "\"" +
                "}" +
                ", \"guarantors\": " + phoneNumber
                + "}\n" + "\n";
        Log.e("POST_PARAMS", params);

//        postToServer( applyGroupLoan  ,token, params , INITIATOR_APPLY_LOAN , HTTP_POST , ctx ,this);
        postToServer(loanApplication + id, token, params, INITIATOR_APPLY_GROUP_LOAN, HTTP_POST, context, this);

    }

    @Override
    public void onDataReady(String initiator, final String response) {
        switch (initiator) {
            case INITIATOR_APPLY_GROUP_LOAN:
                try {
                    runOnUI(new Runnable() {
                        @Override
                        public void run() {
                            Log.e("APPLY_LOAN", "Response is " + response);
//                            hideDialog(ctx);
                            JSONObject jObj = null;
                            try {
                                jObj = new JSONObject(response);
                                if (jObj.has("error")) {
                                    String errors = jObj.getString("error");
                                    showSweetAlertDialogError(errors, context);
                                } else { // if no error message was found in the response,
                                    boolean success = jObj.getBoolean("success");
                                    if (success) { // if the request was successfuls, got forward, else show error message

//                                        setPage(3);
                                        setPage(2);
//                                        apply_loan.setVisibility(View.GONE);
                                        confirm.setVisibility(View.VISIBLE);
                                        groupLoanApplicationResponse loan_esponse = new Gson().fromJson(response, groupLoanApplicationResponse.class);
                                        String message = loan_esponse.getMessage();
                                        loan_title_name = loan_esponse.getLoan_details().getTitleOfLoan();
                                        principle_amount = String.valueOf(loan_esponse.getLoan_details().getPrincipal());
                                        total_interest_amount = String.valueOf(loan_esponse.getLoan_details().getTotalInterest());
                                        showSweetAlertDialogSuccess(message, context);
                                        frequency = loan_esponse.getLoan_details().getPaymentFrequency();
                                        Loancurrency = loan_esponse.getLoan_details().getCurrencyUnit().getPrefix();
                                        sub_totals_value = loan_esponse.getLoan_details().getSubTotal();
                                        processing_fee = loan_esponse.getLoan_details().getServiceFee();
                                        annual_interest_rate = loan_esponse.getLoan_details().getInterestRate();
                                        installments = loan_esponse.getLoan_details().getLengthOfLoan();
                                        instalment_amount_deducted = loan_esponse.getLoan_details().getInstallmentAmount();
                                        descriptionOfLoan = loan_esponse.getLoan_details().getDescriptionOfLoan();
                                        expectedScore = String.valueOf(loan_esponse.getLoan_details().getExpectedScore());
                                        score = String.valueOf(loan_esponse.getLoan_details().getScore());
                                        totalAmount = loan_esponse.getLoan_details().getTotalAmount();
                                        periodInterest = loan_esponse.getLoan_details().getPeriodInterest();
                                        fineRate = loan_esponse.getLoan_details().getFineRate();
                                        totalFines = loan_esponse.getLoan_details().getTotalFines();
                                        periodFine = loan_esponse.getLoan_details().getPeriodFine();
                                        currency_id = loan_esponse.getLoan_details().getCurrencyUnit().getId();
                                        name = loan_esponse.getLoan_details().getCurrencyUnit().getName();
                                        exchange_rate = String.valueOf(loan_esponse.getLoan_details().getCurrencyUnit().getExchange_rate());
                                        prefix = loan_esponse.getLoan_details().getCurrencyUnit().getPrefix();
                                        status = loan_esponse.getLoan_details().getCurrencyUnit().getStatus();
                                        country_id = loan_esponse.getLoan_details().getCurrencyUnit().getCountry_id();
                                        created_at = loan_esponse.getLoan_details().getCurrencyUnit().getCreated_at();
                                        updated_at = loan_esponse.getLoan_details().getCurrencyUnit().getUpdated_at();
                                        source_id = loan_esponse.getLoan_details().getLoan_source().getId();
                                        type = loan_esponse.getLoan_details().getLoan_source().getType();
                                        usage = loan_esponse.getLoan_details().getLoan_source().getUsage();
                                        telco = loan_esponse.getLoan_details().getLoan_source().getTelco();
                                        my_msisdn = loan_esponse.getLoan_details().getLoan_source().getMsisdn();
                                        my_token = loan_esponse.getLoan_details().getLoan_source().getToken();
                                        source_default = (String) loan_esponse.getLoan_details().getLoan_source().getSource_default();
                                        user_id = loan_esponse.getLoan_details().getLoan_source().getUser_id();


                                        tv_loan_title_name.setText(loan_title_name);
                                        tv_principle_amount.setText(principle_amount);
                                        tv_total_interest_amount.setText(total_interest_amount);
                                        tv_payment_frequency.setText(frequency);
                                        tv_currency.setText(Loancurrency);
                                        tv_sub_totals_value.setText(String.valueOf(sub_totals_value));
                                        tv_processing_fee.setText(String.valueOf(processing_fee));
                                        tv_annual_interest_rate.setText(String.valueOf(annual_interest_rate));
                                        tv_no_of_installments.setText(String.valueOf(installments));


                                        sum = sub_totals_value + processing_fee;
                                        tv_grand_total.setText(String.valueOf(sum));
                                        tv_monthly_amount_deducted.setText(String.valueOf(instalment_amount_deducted));

//                                        confirm.setVisibility(View.VISIBLE);
                                    } else {
                                        String api_error_message = jObj.getString("data");
                                        showSweetAlertDialogError(api_error_message, context);
                                        NavHostFragment.findNavController(fragment).navigateUp();
                                    }
                                }

                            } catch (JSONException e) {
                                Log.e("ExceptionLOGIN-DATA", e.toString());
                                showSweetAlertDialogError(global_error_message, context);
                                e.printStackTrace();
//                                apply_loan.setVisibility(View.VISIBLE);
                            }
                        }
                    });
                } catch (Exception e) {

                    Log.e("Exception", e.toString());
                    showSweetAlertDialogError(global_error_message, context);
//                    Toast.makeText(ctx, "Login Data Response Exception" + e.toString(), Toast.LENGTH_LONG).show();
                }
                break;

            case INITIATOR_CONFIRM_APPLY_LOAN:
                try {
                    runOnUI(new Runnable() {
                        @Override
                        public void run() {
                            Log.e("CONFIRM_LOAN", "Response is " + response);
//                            hideDialog(ctx);
                            JSONObject jObj = null;
                            try {
                                jObj = new JSONObject(response);
                                if (jObj.has("errors")) {
                                    String errors = jObj.getString("errors");
                                    showSweetAlertDialogError(errors, context);
                                } else { // if no error message was found in the response,
                                    boolean success = jObj.getBoolean("success");
                                    if (success) { // if the request was successfuls, got forward, else show error message
                                        showAlertAndMoveToPage("Success, You application has been submitted", MainActivity.class, context);
                                        setPage(3);
                                    } else {
                                        String api_error_message = jObj.getString("message");
                                        showSweetAlertDialogError(api_error_message, context);
                                        toggleProg();
                                    }
                                }

                            } catch (JSONException e) {
                                Log.e("ExceptionLOGIN-DATA", e.toString());
                                showSweetAlertDialogError(global_error_message, context);
                                e.printStackTrace();
                            }
                        }
                    });
                } catch (Exception e) {
//                    hideDialog(ctx);
                    Log.e("Exception- LOGIN", e.toString());
                    showSweetAlertDialogError(global_error_message, context);
//                    Toast.makeText(ctx, "Login Data Response Exception" + e.toString(), Toast.LENGTH_LONG).show();
                }
                break;

        }
    }

    @Override
    public void onDataLoaded(String initiator, JSONArray data) {

    }

    @Override
    public void onNetworkError(String initiator, String errorCode, final String errorMessage) {
        switch (initiator) {
            case INITIATOR_APPLY_LOAN:
                try {
                    runOnUI(new Runnable() {
                        @Override
                        public void run() {
//                            hideDialog(ctx);
                            showSweetAlertDialogError(errorMessage, context);
                        }
                    });
                } catch (Exception e) {
//                    hideDialog(ctx);
                    showSweetAlertDialogError(global_error_message, context);
                    Log.e("Exception- LOGIN", e.toString());
//                    Toast.makeText(ctx, "Login Error Response Exception" + e.toString(), Toast.LENGTH_LONG).show();
                }

                break;
            case INITIATOR_GETLENDING_DATA:
                try {
                    runOnUI(new Runnable() {
                        @Override
                        public void run() {
//                            hideDialog(ctx);
                            showSweetAlertDialogError(errorMessage, context);
                        }
                    });
                } catch (Exception e) {
//                    hideDialog(ctx);
                    showSweetAlertDialogError(global_error_message, context);
                    Log.e("Exception", e.toString());
//                    Toast.makeText(ctx, "Login Error Response Exception" + e.toString(), Toast.LENGTH_LONG).show();
                }
                break;
            case INITIATOR_CONFIRM_APPLY_LOAN:
                try {
                    runOnUI(new Runnable() {
                        @Override
                        public void run() {
//                            hideDialog(ctx);
                            showSweetAlertDialogError(errorMessage, context);
                        }
                    });
                } catch (Exception e) {
//                    hideDialog(ctx);
                    showSweetAlertDialogError(global_error_message, context);
                    Log.e("Exception-LOGIN", e.toString());
//                    Toast.makeText(ctx, "Login Error Response Exception" + e.toString(), Toast.LENGTH_LONG).show();
                }

                break;

        }
    }

    private boolean validateFormFields() {
        boolean valid = true;

        _reasonOfLoan = reason_of_loan.getText().toString();
        _input_loan_length = input_loan_length.getText().toString();

        inputLoanAmount = input_loan_amount.getText().toString();


        if (_reasonOfLoan.isEmpty()) {
            reason_of_loan.setError(getString(R.string.emptyField));
            valid = false;
        }
        if (_input_loan_length.isEmpty()) {
            input_loan_length.setError(getString(R.string.emptyField));
            valid = false;
        } else {
            reason_of_loan.setError(null);
        }


        if (inputLoanAmount.isEmpty()) {
            input_loan_amount.setError(getString(R.string.emptyField));
            valid = false;
        }

        return valid;
    }

    private boolean validateMembersCount() {
        boolean valid = true;

        if (phoneNumber.size() < 2) {
            Log.e("daadadad", "validateMembersCount: " + phoneNumber);
            valid = false;
        }

        return valid;
    }


    private void confirmLoanPackageApplication() {

        String params = "{ \"titleOfLoan\":" + '"' + loan_title_name + '"' +
                ", \"descriptionOfLoan\": " + '"' + descriptionOfLoan + '"' +
                ", \"expectedScore\": " + '"' + expectedScore + '"' +
                ", \"score\": " + '"' + score + '"' +
                ", \"principal\": " + '"' + principle_amount + '"' +
                ", \"subTotal\": " + '"' + sub_totals_value + '"' +
                ", \"totalAmount\": " + '"' + totalAmount + '"' +
                ", \"installmentAmount\": " + '"' + instalment_amount_deducted + '"' +
                ", \"lengthOfLoan\": " + '"' + installments + '"' +
                ", \"paymentFrequency\": " + '"' + frequency + '"' +
                ", \"interestRate\": " + '"' + annual_interest_rate + '"' +
                ", \"totalInterest\": " + '"' + total_interest_amount + '"' +
                ", \"periodInterest\": " + '"' + periodInterest + '"' +
                ", \"fineRate\": " + '"' + fineRate + '"' +
                ", \"totalFines\": " + '"' + totalFines + '"' +
                ", \"periodFine\": " + '"' + periodFine + '"' +
                ", \"serviceFee\": " + '"' + processing_fee + "\"," +
                "\"currencyUnit\": {"
                + "\"id\": \"" + currency_id + "\","
                + "\"name\": \"" + name + "\","
                + "\"exchange_rate\": \"" + exchange_rate + "\","
                + "\"prefix\": \"" + prefix + "\","
                + "\"status\": \"" + status + "\","
                + "\"country_id\": \"" + country_id + "\","
                + "\"created_at\": \"" + created_at + "\","
                + "\"updated_at\": \"" + updated_at + "\"" +
                "},"
                + "\"loan_source\": {"
                + "\"id\": \"" + source_id + "\","
                + "\"type\": \"" + type + "\","
                + "\"usage\": \"" + usage + "\","
                + "\"telco\": \"" + telco + "\","
                + "\"msisdn\": \"" + my_msisdn + "\","
                + "\"token\": \"" + my_token + "\","
                + "\"source_default\": \"" + source_default + "\","
                + "\"user_id\": \"" + user_id + "\","
                + "\"group_id\": \"" + group_id + "\","
                + "\"created_at\": \"" + created_at + "\","
                + "\"updated_at\": \"" + updated_at + "\"" +
                "}" +
                ", \"guarantors\": " + phoneNumber
                + "}\n" + "\n";
        Log.e("POST_CONFIRM_PARAMS", params);


        postToServer(confirmLoanApplication + id, token, params, INITIATOR_CONFIRM_APPLY_LOAN, HTTP_POST, context, this);
    }


    @Override
    public void onClick(View view) {

        Log.e("Group Loan", "Button Pressed");
        switch (view.getId()) {
            case R.id.btApplyLoan:
                if (validateMembersCount()) {
                    view.setVisibility(View.GONE);
                    loanApplicationToAPI();
                } else {
                    Toast.makeText(context, getString(R.string.guarantorCountisLessThan2), Toast.LENGTH_LONG).show();
                }
                break;
            case R.id.btConfirm:
                view.setEnabled(false);
                toggleProg();
                Log.e("Loan Confirm", "PRESSED");
                confirmLoanPackageApplication();
                break;
            case R.id.cancel_apply_loan:
                NavHostFragment.findNavController(this).navigateUp();
                break;
            case R.id.btApplyContinue:
                if (validateFormFields()) {
                    view.setVisibility(View.GONE);
                    buttons.get(2).setVisibility(View.VISIBLE);
                    Log.e("Loan Continue", "PRESSED");
                    setPage(1);
                }
                break;
            case R.id.btContinue:
                NavHostFragment.findNavController(this).navigateUp();
                break;
        }
    }

}
