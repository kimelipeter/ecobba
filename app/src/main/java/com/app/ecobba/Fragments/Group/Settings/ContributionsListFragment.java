package com.app.ecobba.Fragments.Group.Settings;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.app.ecobba.Fragments.Group.Settings.Adapters.ContributionsFeesAdapter;
import com.app.ecobba.Models.MeetingsSettingsContibutions.contributionFeesSettingsResponse;
import com.app.ecobba.Models.UserProfile.Detail;
import com.app.ecobba.R;
import com.app.ecobba.Utils.BaseFragment;
import com.app.ecobba.ViewModel.UserProfileViewModel;
import com.app.ecobba.common.SharedMethods;
import com.app.ecobba.common.rxApi.Api;
import com.app.ecobba.common.rxApi.RxApi;

import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.chip.Chip;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.app.ecobba.common.SharedMethods.global_error_message;
import static com.app.ecobba.common.SharedMethods.showSweetAlertDialogError;

public class ContributionsListFragment extends BaseFragment implements View.OnClickListener,
        AppBarLayout.OnOffsetChangedListener {


    Context context;

    Bundle args;
    String token, api_tail, group_id, setting_id;
    Fragment fragment;
    private RecyclerView recyclerView;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    @BindView(R.id.addShares)
    Chip addShares;
    @BindView(R.id.addSociety)
    Chip addSociety;
    @BindView(R.id.swipe_container)
    SwipeRefreshLayout mSwipeRefreshLayout;
    @BindView(R.id.tvNoAvailableData)
    TextView tvNoAvailableData;
    ContributionsFeesAdapter adapter;
    private Api api;
    UserProfileViewModel userProfileViewModel;
    String user_currency;



    public ContributionsListFragment() {

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.contributions_list_fragment, container, false);
        ButterKnife.bind(this, view);
        context = requireActivity();
        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
        userProfileViewModel = new ViewModelProvider(getActivity()).get(UserProfileViewModel.class);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        args = getArguments();
        context = requireActivity();
        token = SharedMethods.getDefaults("token", context);
        assert args != null;
        api_tail = args.getString("args");
        group_id = args.getString("group_id");
        Log.e("CON_group_id_IS", group_id);
        setting_id = args.getString("id");
        Log.e("api_tail", api_tail);

        addSociety.setOnClickListener(this);
        addShares.setOnClickListener(this);
        recyclerView.addItemDecoration(new DividerItemDecoration(getContext(),
                DividerItemDecoration.VERTICAL));
        adapter = new ContributionsFeesAdapter(new ArrayList<>(), requireContext(), fragment,"");
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(requireContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
        api = new RxApi().getApi();
        getData(setting_id, group_id);
        fetchUserProfile();
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                Toast.makeText(context,"Refresh", Toast.LENGTH_LONG).show();
                getData(setting_id, group_id);
                mSwipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary,
                        android.R.color.holo_green_dark,
                        android.R.color.holo_orange_dark,
                        android.R.color.holo_blue_dark);

                mSwipeRefreshLayout.setRefreshing(false);
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.addShares:
                Bundle args_ = new Bundle();
                args_.putString("group_id", group_id);
                args_.putString("args", api_tail);
                args_.putString("id", setting_id);
                NavHostFragment.findNavController(this).navigate(R.id.contributionsSettingFragment, args_);
                break;
            case R.id.addSociety:
                Bundle args = new Bundle();
                args.putString("group_id", group_id);
                args.putString("args", api_tail);
                args.putString("id", setting_id);
                NavHostFragment.findNavController(this).navigate(R.id.createSocietyContributionsSettingsFragment, args);
                break;
        }

    }
    private void fetchUserProfile() {
        userProfileViewModel.getUserProfile();
        userProfileViewModel.userProfileResponseLiveData().observe(getViewLifecycleOwner(), userProfileResponse -> {
            try {
                if (userProfileResponse.getSuccess()) {
                    Detail userDetails = userProfileResponse.getUser().getDetail();
                    String name = userProfileResponse.getUser().getName();

                    user_currency = userProfileResponse.getUser().getWallet().getCurrency().getPrefix();

                    user_currency = userProfileResponse.getUser().getWallet().getCurrency().getPrefix();
                    adapter.user_currency = user_currency;
                    adapter.notifyDataSetChanged();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    private void getData(String setting_id, String group_id) {
        Call<contributionFeesSettingsResponse> call = api.getContributionsSettings(setting_id, group_id);
        call.enqueue(new Callback<contributionFeesSettingsResponse>() {
            @Override
            public void onResponse(Call<contributionFeesSettingsResponse> call, Response<contributionFeesSettingsResponse> response) {
                try {
                    if (response.body().getSuccess()) {
                        progressBar.setVisibility(View.GONE);
                        if (response.body().getCurrent_settings().getData().size() > 0) {
                            adapter.dataList = response.body().getCurrent_settings().getData();
                            adapter.notifyDataSetChanged();
                        } else {
                            tvNoAvailableData.setText(getResources().getString(R.string.no_available_data));
                            tvNoAvailableData.setVisibility(View.VISIBLE);
                        }
                    } else {
                        showSweetAlertDialogError(global_error_message, context);
                        NavHostFragment.findNavController(fragment).navigateUp();

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<contributionFeesSettingsResponse> call, Throwable t) {
             t.getLocalizedMessage();

            }
        });
    }



}
