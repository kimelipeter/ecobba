package com.app.ecobba.Fragments.Web

import android.content.Context
import android.content.res.Resources
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.app.ecobba.Fragments.Borrowing.BorrowFragment
import com.app.ecobba.Fragments.Lending.PackagesFragment
import com.app.ecobba.R

class LoansViewPagerAdapter(fm: FragmentManager, val context: Context) :
        FragmentPagerAdapter(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {
    private var childFragments = ArrayList<Fragment>()
    var resources: Resources?=null



    override fun getItem(position: Int): Fragment {

        return when(position){
            0 -> PackagesFragment()
            1 -> BorrowFragment()
            else -> PackagesFragment()
        }
    }

    fun setFragments(fragments:ArrayList<Fragment>){
        childFragments = fragments
    }


    override fun getCount(): Int {
        return 2
        //Log.e("Viewpager_size", childFragments.toString())
    }

    override fun getPageTitle(position: Int): CharSequence? {

        var title: String?=null
        if (position == 0) {
            title=context.resources.getString(R.string.lending)
        } else if (position == 1) {
            title= context.resources.getString(R.string.borrowing)
        }
        return title
    }

    fun String.capitalizeWords(): String=split(" ").joinToString(" ") { it.capitalize() }
}
