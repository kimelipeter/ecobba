package com.app.ecobba.Fragments.Shop.Services;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.ecobba.Fragments.Shop.Adapters.ShopDashboardAdapter;
import com.app.ecobba.R;
import com.app.ecobba.Utils.BaseFragment;
import com.app.ecobba.common.SharedMethods;
import com.app.ecobba.common.rxApi.Api;
import com.app.ecobba.common.rxApi.RxApi;
import com.app.ecobba.recyclerview_adapters.GenericAdapter;
import com.app.ecobba.recyclerview_models.services.Data;
import com.app.ecobba.recyclerview_models.services.ServicesListResponse;
import com.bumptech.glide.Glide;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.app.ecobba.common.ApisKtKt.getservices;
import static com.app.ecobba.common.AppConstantsKt.INITIATOR_GET_SERVICES;
import static com.app.ecobba.common.SharedMethods.HTTP_GET;
import static com.app.ecobba.common.SharedMethods.getDefaults;
import static com.app.ecobba.common.SharedMethods.getFromServer;
import static com.app.ecobba.common.SharedMethods.global_error_message;
import static com.app.ecobba.common.SharedMethods.runOnUI;
import static com.app.ecobba.common.SharedMethods.showSweetAlertDialogError;

public class Services extends BaseFragment implements View.OnClickListener {
    @BindView(R.id.create)
    FloatingActionButton create;
    Context ctx;
    String token;
    ServicesAdapter adapter;
    private static final String TAG = "services_data";
    private RecyclerView rvServices;
    private Api api;
    Fragment fragment = this;

    @BindView(R.id.tvNoAvailableData)
    TextView tvNoAvailableData;
    ProgressDialog progressDialog;


    public Services() {
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_kwea_services, container, false);

        rvServices = (RecyclerView) view.findViewById(R.id.recyclerviewid);
        adapter = new ServicesAdapter(new ArrayList<>(), fragment, requireContext());
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(requireContext(), 2);
        rvServices.setLayoutManager(layoutManager);
        rvServices.setAdapter(adapter);
        api = new RxApi().getApi();
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        ctx = getActivity();
        ButterKnife.bind(this, view);
        create.setOnClickListener(this);
        progressDialog = new ProgressDialog(ctx);
        getservices();

    }

    private void getservices() {
        progressDialog.setMessage("Loading.Please wait....");
        progressDialog.show();
        Call<ServicesListResponse> call = api.getservices();
        call.enqueue(new Callback<ServicesListResponse>() {
            @Override
            public void onResponse(Call<ServicesListResponse> call, Response<ServicesListResponse> response) {

                try {
                    Log.e("SERVICES_TAG", "onResponse: " + response);
                    if (response.body().getSuccess()) {
                        progressDialog.dismiss();
                        if (response.body().getData().size() > 0) {
                            adapter.dataList = response.body().getData();
                            adapter.notifyDataSetChanged();
                        } else {
                            tvNoAvailableData.setText(getResources().getString(R.string.no_available_data));
                            tvNoAvailableData.setVisibility(View.VISIBLE);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ServicesListResponse> call, Throwable t) {
                Log.e(TAG, "onFailure: " + t.getLocalizedMessage());
            }
        });

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.create:
                NavHostFragment.findNavController(this).navigate(R.id.createServices);
        }

    }


    @Override
    public void onResume() {
        super.onResume();
    }

}
