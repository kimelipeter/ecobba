package com.app.ecobba.Fragments.Group.Loans;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;


import com.app.ecobba.Models.GroupLoans.getGroupLoansResponse;
import com.app.ecobba.Models.UserProfile.Detail;
import com.app.ecobba.Models.UserProfile.Role;
import com.app.ecobba.Models.UserProfile.User;
import com.app.ecobba.R;
import com.app.ecobba.ViewModel.UserProfileViewModel;
import com.app.ecobba.common.SharedMethods;
import com.app.ecobba.common.rxApi.Api;
import com.app.ecobba.common.rxApi.RxApi;
import com.app.ecobba.recyclerview_adapters.GroupsAvailableLoansRecycler;
import com.google.gson.Gson;


import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GroupLoansFragment extends Fragment implements

        View.OnClickListener {
    @BindView(R.id.recyclerViewAvailableLoans)
    RecyclerView recyclerViewAvailableLoans;


    @BindView(R.id.tvNoAvailableLoans)
    TextView tvNoAvailableLoans;


    @BindView(R.id.progress_dialog)
    ProgressBar progress_dialog;

    String group_id, group_name;
    Context context;
    Fragment fragment;

    private Api api;
    GroupsAvailableLoansRecycler adapter;
    @BindView(R.id.swipe_container)
    SwipeRefreshLayout mSwipeRefreshLayout;
    UserProfileViewModel userProfileViewModel;
    String user_currency;


    public GroupLoansFragment() {

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View frag = inflater.inflate(R.layout.fragment_group_loans, container, false);
        ButterKnife.bind(this, frag);
        context = getActivity();
        fragment = this;
        api = new RxApi().getApi();
        userProfileViewModel = new ViewModelProvider(getActivity()).get(UserProfileViewModel.class);

        recyclerViewAvailableLoans = frag.findViewById(R.id.recyclerViewAvailableLoans);
        recyclerViewAvailableLoans.addItemDecoration(new DividerItemDecoration(getContext(),
                DividerItemDecoration.VERTICAL));
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
        adapter = new GroupsAvailableLoansRecycler(new ArrayList(), context, fragment,"");
        recyclerViewAvailableLoans.setLayoutManager(layoutManager);
        recyclerViewAvailableLoans.setAdapter(adapter);
        return frag;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        SharedPreferences sh = getContext().getSharedPreferences("PREFERENCE", Context.MODE_PRIVATE);
        group_id = sh.getString("group_id", "");
        group_name = sh.getString("group_name", "");
        fetchGroupLoans(group_id);
        fetchUserProfile();
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                Toast.makeText(context,"Refresh", Toast.LENGTH_LONG).show();
                fetchGroupLoans(group_id);
                mSwipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary,
                        android.R.color.holo_green_dark,
                        android.R.color.holo_orange_dark,
                        android.R.color.holo_blue_dark);

                mSwipeRefreshLayout.setRefreshing(false);
            }
        });

    }

    @Override
    public void onClick(View view) {

    }
    private void fetchUserProfile() {
        userProfileViewModel.getUserProfile();
        userProfileViewModel.userProfileResponseLiveData().observe(getViewLifecycleOwner(), userProfileResponse -> {
            try {
                if (userProfileResponse.getSuccess()) {

                    user_currency = userProfileResponse.getUser().getWallet().getCurrency().getPrefix();

                    user_currency = userProfileResponse.getUser().getWallet().getCurrency().getPrefix();
                    adapter.user_currency = user_currency;
                    adapter.notifyDataSetChanged();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    private void fetchGroupLoans(String group_id) {
        progress_dialog.setVisibility(View.VISIBLE);
        Call<getGroupLoansResponse> call = api.getGroupLoans(group_id);
        call.enqueue(new Callback<getGroupLoansResponse>() {
            @Override
            public void onResponse(Call<getGroupLoansResponse> call, Response<getGroupLoansResponse> response) {
                try {
                    if (response.body().getSuccess()) {
                        Log.e("TAG_GROUP_LOANS_DATA", "onResponse: " + response);
                        progress_dialog.setVisibility(View.GONE);
                        if (response.body().getPackages().size() > 0) {
                            adapter.datalist = response.body().getPackages();
                            adapter.notifyDataSetChanged();
                        } else {

                            tvNoAvailableLoans.setText(getResources().getString(R.string.no_available_data));
                            tvNoAvailableLoans.setVisibility(View.VISIBLE);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<getGroupLoansResponse> call, Throwable t) {
                Log.e("ERROR_ON_DATA", "onFailure: " + t.getLocalizedMessage());

            }
        });
    }
}
