package com.app.ecobba.Fragments.Group.Loans;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.app.ecobba.Models.ProcessLoan.ProcessLoanResponse;
import com.app.ecobba.R;
import com.app.ecobba.Utils.BaseFragment;
import com.app.ecobba.common.rxApi.Api;
import com.app.ecobba.common.rxApi.RxApi;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.material.chip.Chip;
import com.google.android.material.shape.CornerFamily;
import com.google.android.material.shape.ShapeAppearanceModel;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.app.ecobba.common.SharedMethods.showSweetAlertDialogError;
import static com.app.ecobba.common.SharedMethods.showSweetAlertDialogSuccess;

public class LoanDetailsFragment extends BaseFragment implements View.OnClickListener {
    Fragment fragment = this;
    Context context;
    Bundle args;
    String loan_borrower, loan_amount, loan_name,
            loan_status, date_borrowed, user_currency, email, msisdn,
            no_of_installments, payback_date, principal, repayment_plan,
            group_name, package_name, package_type, payment_amount,
            interest_accrued, fine_accrued, payment_date, user_profile;
    int loan_id;
    String action;
    String message;
    @BindView(R.id.tvName)
    TextView tvName;
    @BindView(R.id.tvLoanName)
    TextView tvLoanName;
    @BindView(R.id.tvDate)
    TextView tvDate;
    @BindView(R.id.tvAmount)
    TextView tvAmount;
    @BindView(R.id.tvEmail)
    TextView tvEmail;
    @BindView(R.id.tvNumber)
    TextView tvNumber;
    @BindView(R.id.tvPayBackDate)
    TextView tvPayBackDate;
    @BindView(R.id.tv_no_of_installments)
    TextView tv_no_of_installments;
    @BindView(R.id.tvStatus)
    TextView tvStatus;
    @BindView(R.id.tvRepaymentPlan)
    TextView tvRepaymentPlan;
    @BindView(R.id.tvPrincipal)
    TextView tvPrincipal;
    @BindView(R.id.tvPaymentAmount)
    TextView tvPaymentAmount;
    @BindView(R.id.tvInterestAccrued)
    TextView tvInterestAccrued;
    @BindView(R.id.tvFinAccrued)
    TextView tvFinAccrued;
    @BindView(R.id.tvPaymentDate)
    TextView tvPaymentDate;
    @BindView(R.id.tvLoanPackageType)
    TextView tvLoanPackageType;
    @BindView(R.id.tvLoanPackageGroup)
    TextView tvLoanPackageGroup;
    @BindView(R.id.llLoanDetails)
    FrameLayout llLoanDetails;
    @BindView(R.id.llLoanDetailPayment)
    FrameLayout llLoanDetailPayment;

    @BindView(R.id.yes)
    Chip tvYes;
    @BindView(R.id.no)
    Chip tvNo;
    @BindView(R.id.Chipdisburse)
    Chip Chipdisburse;
    @BindView(R.id.ChipManualDisburse)
    Chip ChipManualDisburse;
    @BindView(R.id.ChipMarkAsPaid)
    Chip ChipMarkAsPaid;
    @BindView(R.id.ChipDefaulted)
    Chip ChipDefaulted;
    @BindView(R.id.imageApproved)
    ImageView imageApproved;
    @BindView(R.id.profile_image)
    CircleImageView profile_image;
    private ProgressDialog progressDialog;

    private Api api;


    public LoanDetailsFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View frag = inflater.inflate(R.layout.fragment_loan_details, container, false);
        ButterKnife.bind(this, frag);
        context = getActivity();
        api = new RxApi().getApi();

        args = getArguments();
        assert args != null;
        loan_borrower = args.getString("loan_borrower");
        loan_amount = args.getString("loan_amount");
        loan_name = args.getString("loan_name");
        loan_status = args.getString("loan_status");
        date_borrowed = args.getString("date_borrowed");
        user_currency = args.getString("user_currency");
        email = args.getString("email");
        msisdn = args.getString("msisdn");
        no_of_installments = args.getString("no_of_installments");
        payback_date = args.getString("payback_date");
        principal = args.getString("principal");
        repayment_plan = args.getString("repayment_plan");
        group_name = args.getString("group_name");
        package_name = args.getString("package_name");
        package_type = args.getString("package_type");
        payment_amount = args.getString("payment_amount");
        interest_accrued = args.getString("interest_accrued");
        fine_accrued = args.getString("fine_accrued");
        payment_date = args.getString("payment_date");
        user_profile = args.getString("user_profile");
        loan_id = args.getInt("loan_id");
        Log.e("loan_id_TAG", "onCreateView: " + loan_id);
        return frag;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        context = getActivity();
        tvYes.setOnClickListener(this);
        tvNo.setOnClickListener(this);
        Chipdisburse.setOnClickListener(this);
        ChipManualDisburse.setOnClickListener(this);
        ChipMarkAsPaid.setOnClickListener(this);
        ChipDefaulted.setOnClickListener(this);

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        sdf.setTimeZone(TimeZone.getTimeZone("GMT"));

        try {
            Date date1 = sdf.parse(payback_date);
            sdf = new SimpleDateFormat("dd MMMM yyyy");
            String date = sdf.format(date1);

            sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");

            Date time = sdf.parse(date_borrowed);
            sdf = new SimpleDateFormat("dd MMMM yyyy");

            String time1 = sdf.format(time);


            sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");

            Date time2 = sdf.parse(payment_date);
            sdf = new SimpleDateFormat("dd MMMM yyyy");

            String time3 = sdf.format(time2);

            Log.e("DateAndTime", "Date " + date + " Time " + time1 + " date 3 " + time3);
            tvPayBackDate.setText(date);
            tvDate.setText(time1);
            tvPaymentDate.setText(time3);
        } catch (ParseException e) {
            Log.e("DateAndTime", e.getLocalizedMessage());
            e.printStackTrace();
        }


        tvName.setText(loan_borrower);
        tvLoanName.setText(loan_name);
        tvAmount.setText(user_currency + " " + loan_amount);
        tvNumber.setText(msisdn);
        tvEmail.setText(email);
        tv_no_of_installments.setText(no_of_installments);
        tvStatus.setText(loan_status);

        if (loan_status.equals("pending")) {
            tvStatus.setText(R.string.pending);
            tvStatus.setBackgroundResource(R.drawable.pending_status);
            tvStatus.setTextColor(context.getResources().getColor(R.color.white));
            llLoanDetails.setVisibility(View.VISIBLE);
        } else if (loan_status.equals("approved")) {
            tvStatus.setText(R.string.approved);
            tvStatus.setBackgroundResource(R.drawable.rounded_corner);
            tvStatus.setTextColor(context.getResources().getColor(R.color.white));
            imageApproved.setVisibility(View.VISIBLE);
            llLoanDetails.setVisibility(View.VISIBLE);
            Chipdisburse.setVisibility(View.VISIBLE);
            ChipManualDisburse.setVisibility(View.VISIBLE);
            tvYes.setVisibility(View.GONE);
            tvNo.setVisibility(View.GONE);
        } else if (loan_status.equals("declined")) {
            tvStatus.setText(R.string.declined);
            tvStatus.setBackgroundResource(R.drawable.declined_status);
            tvStatus.setTextColor(context.getResources().getColor(R.color.white));
            llLoanDetails.setVisibility(View.GONE);
        } else if (loan_status.equals("disbursed")) {

            tvStatus.setText(R.string.disbursed);
            tvStatus.setBackgroundResource(R.drawable.rounded_corner);
            tvStatus.setTextColor(context.getResources().getColor(R.color.white));
            llLoanDetails.setVisibility(View.GONE);
            llLoanDetailPayment.setVisibility(View.VISIBLE);
        } else if (loan_status.equals("paid")) {

            tvStatus.setText(R.string.paid);
            tvStatus.setBackgroundResource(R.drawable.rounded_corner);
            tvStatus.setTextColor(context.getResources().getColor(R.color.white));
            llLoanDetails.setVisibility(View.GONE);
        } else if (loan_status.equals("defaulted")) {
            tvStatus.setText(R.string.defaulted);
            tvStatus.setTextColor(context.getResources().getColor(R.color.white));
            tvStatus.setBackgroundResource(R.drawable.rounded_corner);
        }

        tvPrincipal.setText(user_currency + " " + principal);


        if (repayment_plan.equals("1")) {

            tvRepaymentPlan.setText(R.string.weekly);
        } else if (repayment_plan.equals("3")) {
            tvRepaymentPlan.setText(R.string.monthly);
        } else if (repayment_plan.equals("4")) {
            tvRepaymentPlan.setText(R.string.annually);
        }
        tvLoanPackageGroup.setText(group_name);

        tvPaymentAmount.setText(user_currency + " " + payment_amount);
        tvInterestAccrued.setText(user_currency + " " + interest_accrued);
        tvFinAccrued.setText(user_currency + " " + fine_accrued);
        if (package_type.equals("group_package")) {
            tvLoanPackageType.setText(R.string.group_package);
        }


        Glide.with(context)
                .load(user_profile)
                .apply(new RequestOptions().centerCrop().circleCrop())
                .into(profile_image);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.yes:
                action = "accept";
                message = "";
                progressDialog = ProgressDialog.show(context, "", "Please Wait...", true);
                ProcessLoanRequest(loan_id, action, message);
                break;
            case R.id.no:
                action = "reject";
                message="You can not Pay the loan";
                progressDialog = ProgressDialog.show(context, "", "Please Wait...", true);
                ProcessLoanRequest(loan_id, action,message);
                break;
            case R.id.Chipdisburse:
                action = "disburse";
                message = "";
                message = "";
                progressDialog = ProgressDialog.show(context, "", "Please Wait...", true);
                ProcessLoanRequest(loan_id, action, message);
                break;
            case R.id.ChipManualDisburse:
                action = "manual_disburse";
                message = "";
                progressDialog = ProgressDialog.show(context, "", "Please Wait...", true);
                ProcessLoanRequest(loan_id, action,message);
                break;
            case R.id.ChipMarkAsPaid:
                action = "manual_paid";
                progressDialog = ProgressDialog.show(context, "", "Please Wait...", true);
                ProcessLoanRequest(loan_id, action,message);
                break;
            case R.id.ChipDefaulted:
                action = "defaulted";
                progressDialog = ProgressDialog.show(context, "", "Please Wait...", true);
                ProcessLoanRequest(loan_id, action,message);
                break;


        }

    }

    private void ProcessLoanRequest(int loan_id, String action,String message) {

        Call<ProcessLoanResponse> call = api.ProcessUserLoan(loan_id, action, message);
        call.enqueue(new Callback<ProcessLoanResponse>() {
            @Override
            public void onResponse(Call<ProcessLoanResponse> call, Response<ProcessLoanResponse> response) {

                try {
//                    Log.e("ggg", "onResponse: "+response);
                    if (response.body().getSuccess()) {
                        progressDialog.dismiss();
                        showSweetAlertDialogSuccess(response.body().getMessage(), context);
                    } else {
                        showSweetAlertDialogError("Something went wrong", context);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    progressDialog.dismiss();
                    showSweetAlertDialogError(response.message(), context);
                }

            }

            @Override
            public void onFailure(Call<ProcessLoanResponse> call, Throwable t) {
                Log.e("ERROR_TAG", "onFailure: " + t.getLocalizedMessage());
                progressDialog.dismiss();
                showSweetAlertDialogError(t.getLocalizedMessage(), context);
            }
        });
    }
}
