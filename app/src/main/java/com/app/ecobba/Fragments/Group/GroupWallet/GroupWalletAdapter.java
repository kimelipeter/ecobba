package com.app.ecobba.Fragments.Group.GroupWallet;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;


import com.app.ecobba.Models.GroupWallet.AdapaySource;
import com.app.ecobba.R;
import com.mikhaellopez.circularimageview.CircularImageView;

import java.util.List;


public class GroupWalletAdapter  extends RecyclerView.Adapter<GroupWalletAdapter.ViewHolder> {
    public List<AdapaySource> datalist;
    public Context context;
    public Fragment fragment;

    public GroupWalletAdapter(List<AdapaySource> datalist, Context context, Fragment fragment) {
        this.datalist = datalist;
        this.context = context;
        this.fragment = fragment;
    }

    @NonNull
    @Override
    public GroupWalletAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.payment_sources_list_item, parent, false);
        return new GroupWalletAdapter.ViewHolder(view);
    }

    @SuppressLint("ResourceType")
    @Override
    public void onBindViewHolder(@NonNull GroupWalletAdapter.ViewHolder holder, int position) {

        String telco = datalist.get(position).getTelco();
        String type = datalist.get(position).getType();
        if (telco.equals("safaricom_ke")) {
            holder.tvTelco.setText("Mpesa");
            holder.mpesa.setBackgroundResource(R.drawable.mpesa);
        }
        if (type.equals("mobile_money")) {
            holder.tvType.setText("Mobile Money");
        }
        holder.tvMsisdn.setText(datalist.get(position).getMsisdn());
    }

    @Override
    public int getItemCount() {
        return datalist.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvTelco, tvType, tvMsisdn;
        CircularImageView mpesa;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvTelco = itemView.findViewById(R.id.tvTelco);
            tvType = itemView.findViewById(R.id.tvType);
            tvMsisdn = itemView.findViewById(R.id.tvMsisdn);
            mpesa=itemView.findViewById(R.id.mpesa);
        }
    }
}
