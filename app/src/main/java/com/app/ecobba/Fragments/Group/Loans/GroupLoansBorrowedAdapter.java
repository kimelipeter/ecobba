package com.app.ecobba.Fragments.Group.Loans;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;


import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.RecyclerView;


import com.app.ecobba.Fragments.Group.Settings.ContributionSettingDetailsBotomSheetDialog;
import com.app.ecobba.Models.GroupLoansBorrowed.CurrentLoan;
import com.app.ecobba.Models.GroupLoansBorrowed.LoanSchedule;
import com.app.ecobba.R;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class GroupLoansBorrowedAdapter extends RecyclerView.Adapter<GroupLoansBorrowedAdapter.ViewHolder> {
    public List<CurrentLoan> datalist;
    public Context context;
    public Fragment fragment;
    public String user_currency;

    public GroupLoansBorrowedAdapter(List<CurrentLoan> datalist, Context context, Fragment fragment, String user_currency) {
        this.datalist = datalist;
        this.context = context;
        this.fragment = fragment;
        this.user_currency = user_currency;
    }

    @NonNull
    @Override
    public GroupLoansBorrowedAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.list_item_group_loans_borrowed, parent, false);
        return new GroupLoansBorrowedAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull GroupLoansBorrowedAdapter.ViewHolder holder, int position) {

        Bundle args = new Bundle();
        String loan_borrower = datalist.get(position).getUser().getName() + " " + datalist.get(position).getUser().getOther_names();
        String loan_amount = String.valueOf(datalist.get(position).getAmount());
        String loan_name = datalist.get(position).getLoan_title();
        String loan_status = datalist.get(position).getStatus();
        String date_borrowed = datalist.get(position).getCreated_at();
        String email = datalist.get(position).getUser().getEmail();
        String msisdn = datalist.get(position).getUser().getMsisdn();
        String no_of_installments = datalist.get(position).getDetail().getNo_of_installments();
        String payback_date = datalist.get(position).getDetail().getPayback_date();
        String principal = String.valueOf(datalist.get(position).getDetail().getPrincipal());
        String repayment_plan = datalist.get(position).getDetail().getRepayment_plan();
        String group_name = datalist.get(position).getGroup().getName();
        String package_name = datalist.get(position).getLoan_package().getName();
        String package_type = datalist.get(position).getLoan_package().getPackage_type();
        int loan_id=datalist.get(position).getDetail().getLoan_id();

        String user_profile=datalist.get(position).getUser().getAvatar();
        List<LoanSchedule> loanSchedules = datalist.get(position).getLoan_schedule();

        for(int i=0 ; i< loanSchedules.size() ; i++)
        {
            String payment_amount = String.valueOf(datalist.get(position).getLoan_schedule().get(i).getPayment_amount());
            String interest_accrued = String.valueOf(datalist.get(position).getLoan_schedule().get(i).getInterest_accrued());
            String fine_accrued = String.valueOf(datalist.get(position).getLoan_schedule().get(i).getFine_accrued());
            String payment_date = datalist.get(position).getLoan_schedule().get(i).getPayment_date();

            args.putString("payment_amount", payment_amount);
            args.putString("interest_accrued", interest_accrued);
            args.putString("fine_accrued", fine_accrued);
            args.putString("payment_date", payment_date);
        }






        holder.tvName.setText(loan_borrower);
        holder.tvLoanAmount.setText(user_currency + " " + loan_amount);
        holder.tvLoanName.setText(loan_name);

        if (loan_status.equals("pending")) {
            holder.tvStatus.setText(R.string.pending);
            holder.tvStatus.setTextColor(context.getResources().getColor(R.color.white));
            holder.tvStatus.setBackgroundResource(R.drawable.pending_status);
        } else if (loan_status.equals("approved")) {
            holder.tvStatus.setText(R.string.approved);
            holder.tvStatus.setTextColor(context.getResources().getColor(R.color.white));
            holder.tvStatus.setBackgroundResource(R.drawable.rounded_corner);
        } else if (loan_status.equals("declined")) {
            holder.tvStatus.setText(R.string.declined);
            holder.tvStatus.setTextColor(context.getResources().getColor(R.color.white));
            holder.tvStatus.setBackgroundResource(R.drawable.declined_status);
        }else if (loan_status.equals("paid")) {
            holder.tvStatus.setText(R.string.paid);
            holder.tvStatus.setTextColor(context.getResources().getColor(R.color.white));
            holder.tvStatus.setBackgroundResource(R.drawable.rounded_corner);
        }
        else if (loan_status.equals("disbursed")) {
            holder.tvStatus.setText(R.string.disbursed);
            holder.tvStatus.setTextColor(context.getResources().getColor(R.color.white));
            holder.tvStatus.setBackgroundResource(R.drawable.rounded_corner);
        }
        else if (loan_status.equals("defaulted")) {
            holder.tvStatus.setText(R.string.defaulted);
            holder.tvStatus.setTextColor(context.getResources().getColor(R.color.white));
            holder.tvStatus.setBackgroundResource(R.drawable.rounded_corner);
        }


        Glide.with(context)
                .load(user_profile)
                .apply(new RequestOptions().centerCrop().circleCrop())
                .into(holder.profile_image);



        args.putString("loan_borrower", loan_borrower);
        args.putString("loan_amount", loan_amount);
        args.putString("loan_name", loan_name);
        args.putString("loan_status", loan_status);
        args.putString("date_borrowed", date_borrowed);
        args.putString("user_currency", user_currency);
        args.putString("email", email);
        args.putString("msisdn", msisdn);
        args.putString("no_of_installments", no_of_installments);
        args.putString("payback_date", payback_date);
        args.putString("principal", principal);
        args.putString("repayment_plan", repayment_plan);
        args.putString("group_name", group_name);
        args.putString("package_name", package_name);
        args.putString("package_type", package_type);
        args.putString("user_profile", user_profile);
        args.putInt("loan_id",loan_id);
        holder.loanLl.setOnClickListener((view) -> {
            NavHostFragment.findNavController(fragment).navigate(R.id.loanDetailsFragment, args);


        });


    }

    @Override
    public int getItemCount() {
        return datalist.size();
//        Log.e("DATA_SIZE", "getItemCount: "+ datalist.size() );
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvName, tvLoanAmount, tvLoanName, tvStatus;
        LinearLayout loanLl;
        CircleImageView profile_image;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.tvName);
            tvLoanAmount = itemView.findViewById(R.id.tvLoanAmount);
            tvLoanName = itemView.findViewById(R.id.tvLoanName);
            tvStatus = itemView.findViewById(R.id.tvStatus);
            loanLl = itemView.findViewById(R.id.llContact);
            profile_image = itemView.findViewById(R.id.profile_image);
        }
    }
}
