
package com.app.ecobba.Fragments.Profile.UserProfile;

import lombok.Data;

@Data
@SuppressWarnings("unused")
public class Wallet {

    private long balance;
    private String createdAt;
    private long id;
    private long status;
    private String updatedAt;
    private long userId;

}
