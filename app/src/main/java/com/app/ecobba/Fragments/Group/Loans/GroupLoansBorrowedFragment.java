package com.app.ecobba.Fragments.Group.Loans;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.ecobba.Models.GroupLoansBorrowed.groupLoansBorrowedResponse;
import com.app.ecobba.R;
import com.app.ecobba.Utils.BaseFragment;
import com.app.ecobba.ViewModel.UserProfileViewModel;
import com.app.ecobba.common.rxApi.Api;
import com.app.ecobba.common.rxApi.RxApi;
import com.google.android.material.appbar.AppBarLayout;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.app.ecobba.common.SharedMethods.showSweetAlertDialogError;

public class GroupLoansBorrowedFragment extends BaseFragment implements
        AppBarLayout.OnOffsetChangedListener {
    private Api api;
    Fragment fragment = this;
    Context context;
    private RecyclerView recyclerView;
    UserProfileViewModel userProfileViewModel;
    GroupLoansBorrowedAdapter adapter;
    String group_id, user_currency;
    @BindView(R.id.progress_dialog)
    ProgressBar progress_dialog;
    @BindView(R.id.tvNoAvailableLoans)
    TextView tvNoAvailableLoans;

    public GroupLoansBorrowedFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_group_loans_borrowed, container, false);
        ButterKnife.bind(this, view);
        context = getActivity();
        userProfileViewModel = new ViewModelProvider(getActivity()).get(UserProfileViewModel.class);
        recyclerView = view.findViewById(R.id.recyclerView);
        recyclerView.addItemDecoration(new DividerItemDecoration(getContext(),
                DividerItemDecoration.VERTICAL));
        adapter = new GroupLoansBorrowedAdapter(new ArrayList<>(), requireContext(), fragment, "");
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(requireContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
        api = new RxApi().getApi();
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        context = getActivity();
        SharedPreferences sh = getContext().getSharedPreferences("PREFERENCE", Context.MODE_PRIVATE);
        group_id = sh.getString("group_id", "");
        Log.e("GROUP_ID_VAL", group_id);
        fetchGroupLoansBorrowed(group_id);
        fetchUsersData();
    }

    private void fetchGroupLoansBorrowed(String group_id) {
        progress_dialog.setVisibility(View.VISIBLE);
        Call<groupLoansBorrowedResponse> call = api.getGroupLoansborrowed(group_id);
        call.enqueue(new Callback<groupLoansBorrowedResponse>() {
            @Override
            public void onResponse(Call<groupLoansBorrowedResponse> call, Response<groupLoansBorrowedResponse> response) {
                try {

                    if (response.body().getSuccess()) {
                        progress_dialog.setVisibility(View.GONE);
                        if (response.body().getCurrent_loans().size() > 0) {
                            adapter.datalist = response.body().getCurrent_loans();
                            adapter.notifyDataSetChanged();
                        } else {
                            tvNoAvailableLoans.setText(getResources().getString(R.string.no_available_data));
                            tvNoAvailableLoans.setVisibility(View.VISIBLE);
                        }
                    }
                    else if (!response.body().getSuccess()){
                        String api_error_message = response.body().getMessage();
                        showSweetAlertDialogError(api_error_message, context);
                    }
                } catch (Exception e) {
                    e.printStackTrace();

                }
            }

            @Override
            public void onFailure(Call<groupLoansBorrowedResponse> call, Throwable t) {
//                t.getLocalizedMessage();
                Log.e("gfdtdtddtd", "onResponse: "+t.getLocalizedMessage() );
            }
        });
    }

    private void fetchUsersData() {
        userProfileViewModel.getUserProfile();
        userProfileViewModel.userProfileResponseLiveData().observe(getViewLifecycleOwner(), userProfileResponse -> {
            try {
                if (userProfileResponse.getSuccess()) {
                    user_currency = userProfileResponse.getUser().getWallet().getCurrency().getPrefix();
                    adapter.user_currency = user_currency;
                    adapter.notifyDataSetChanged();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }
}
