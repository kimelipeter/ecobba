package com.app.ecobba.Fragments.Profile.response

import com.google.gson.annotations.SerializedName
import lombok.Data

@Data
class Role {
    @SerializedName(value = "created_at", alternate = ["createdAt"])
    var createdAt: String? = null
    var description: String? = null

    @SerializedName(value = "display_name", alternate = ["displayName"])
    var displayName: String? = null
    var id = 0
    var name: String? = null

    @SerializedName(value = "updated_at", alternate = ["updatedAt"])
    var updatedAt: String? = null
}