package com.app.ecobba.Fragments.Profile;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;

import com.app.ecobba.Fragments.Profile.viewmodel.ProfileViewModel;
import com.app.ecobba.Models.UserProfile.Detail;
import com.app.ecobba.Models.UserProfile.User;
import com.app.ecobba.R;
import com.app.ecobba.ViewModel.UserProfileViewModel;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import butterknife.BindViews;
import butterknife.ButterKnife;

public class ContactProfileFragment extends Fragment {

    @BindViews({R.id.etPhone,
            R.id.etEmail})
    List<EditText> inputs;

    Boolean created = false, init = false, populate = false;
    String phone = "", email = "";
    Context ctx;;
    UserProfileViewModel userProfileViewModel;

    public ContactProfileFragment(Context ctx) {
        this.ctx = ctx;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View frag = inflater.inflate(R.layout.fragment_registration_contact, container, false);
        ButterKnife.bind(this, frag);

        userProfileViewModel = new ViewModelProvider(this).get(UserProfileViewModel.class);


        return frag;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        created = true;
        getContactData();
    }

    public void getContactData() {
        userProfileViewModel.getUserProfile();

        userProfileViewModel.userProfileResponseLiveData().observe(getViewLifecycleOwner(),
                userProfileResponse -> {
                  //  Log.d("KKKKK", "getContactData: " + userProfileResponse.getUser().getEmail());
                    try {
                        setData(userProfileResponse.getUser());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                });
    }


    public void init() {
        inputs.get(0).setText(phone);
        inputs.get(1).setText(email);

    }

    public void setData(User data) {
        phone = data.getMsisdn();
        email = data.getEmail();

        Log.e("CONTACT_TAG", "setData: "+"pHONE nUMBER is "+phone+" and Email is "+email );
        init();
    }

    public String getData() {
        if (created) {
            String data = ", \"email\": " + '"' + inputs.get(1).getText().toString() + '"' +
                    ", \"msisdn\": " + '"' + inputs.get(0).getText().toString() + '"';

            return data;
        } else {
            return "";
        }
    }
}
