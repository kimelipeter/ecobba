package com.app.ecobba.Fragments.Profile;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;

import com.app.ecobba.Fragments.Profile.viewmodel.ProfileViewModel;
import com.app.ecobba.Models.RegistrationData.Classe;
import com.app.ecobba.Models.UserProfile.User;
import com.app.ecobba.R;
import com.app.ecobba.ViewModel.UserProfileViewModel;
import com.jaredrummler.materialspinner.MaterialSpinner;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class OccupationProfileFragment extends Fragment {

    @BindView(R.id.msIncomeClass)
    MaterialSpinner msIncomeClass;
    @BindView(R.id.etOccupation)
    EditText etOccupation;

    ArrayList<String> income_drop_down, income_id;
    String income = "", occupation = "";
    int income_pos = 0;
    Boolean created = false, init = false, populate = false;
    ProfileViewModel profileViewModel;
    Context ctx;

    UserProfileViewModel userProfileViewModel;

    public OccupationProfileFragment(Context ctx) {
        this.ctx = ctx;
        income_drop_down = new ArrayList<>();
        income_id = new ArrayList<>();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View frag = inflater.inflate(R.layout.fragment_registration_occupation, container, false);
        ButterKnife.bind(this, frag);

        userProfileViewModel = new ViewModelProvider(this).get(UserProfileViewModel.class);
        getOccupationData();
        return frag;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        created = true;

    }

    public void getOccupationData() {
        userProfileViewModel.getUserProfile();

        userProfileViewModel.userProfileResponseLiveData().observe(getViewLifecycleOwner(), userProfileResponse -> {
            try {
                setData(userProfileResponse.getUser());
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

        userProfileViewModel.getUserIncome();

        userProfileViewModel.userIncome().observe(getViewLifecycleOwner(), response -> {
            setIncomeData(response);
        });

    }

    public void setIncomeData(List<Classe> data) {
        income_drop_down.add(ctx.getResources().getString(R.string.please_select_income_class));
        income_id.add("0");

        if (data.size() > 0) {
            for (int i = 0; i < data.size(); i++) {
                income_drop_down.add(data.get(i).getName());
                income_id.add(String.valueOf(data.get(i).getId()));
                msIncomeClass.setItems(income_drop_down);
                if (income_id.contains(income)) {
                    income_pos = income_id.indexOf(income);
                    msIncomeClass.setSelectedIndex(income_pos);
                }

            }
        }
    }


    public void setData(User data) {
        occupation = data.getDetail().getOccupation();
        income = data.getDetail().getIncome().getName();

        etOccupation.setText(occupation);

    }



    public String getData() {
        if (created) {
            String data = ", \"occupation\": " + '"' + etOccupation.getText().toString() + '"' +
                    ", \"income\": " + '"' + income_id.get(msIncomeClass.getSelectedIndex()) + '"';
            return data;
        } else {
            return "";
        }

    }
}
