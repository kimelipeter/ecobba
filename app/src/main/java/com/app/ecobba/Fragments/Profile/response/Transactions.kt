package com.app.ecobba.Fragments.Profile.response

import lombok.Data

@Data
class Transactions {
    var credit = 0
    var debit = 0
}