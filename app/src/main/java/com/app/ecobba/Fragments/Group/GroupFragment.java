package com.app.ecobba.Fragments.Group;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.ecobba.Models.GroupDetails.GroupDetailsResponse;
import com.app.ecobba.Models.GroupMmembers.groupMembersResponse;
import com.app.ecobba.Models.GroupTransaction.AllTransaction;
import com.app.ecobba.Models.GroupTransaction.GroupTransactionResponse;
import com.app.ecobba.Models.Group_Transaction.groupTransactionResponse;
import com.app.ecobba.Models.User;
import com.app.ecobba.Models.UserProfile.Role;
import com.app.ecobba.Models.UserProfile.UserProfileResponse;
import com.app.ecobba.Models.payment.contributionResponse;
import com.app.ecobba.R;
import com.app.ecobba.common.SharedMethods;
import com.app.ecobba.common.rxApi.Api;
import com.app.ecobba.common.rxApi.RxApi;
import com.app.ecobba.recyclerview_adapters.GroupMembersRecyclerAdapter;
import com.app.ecobba.recyclerview_adapters.GroupTransactionsListRecycler;
import com.app.ecobba.recyclerview_models.GroupMembersListAdapter;
import com.app.ecobba.recyclerview_models.GroupTransactionsAdapter;
import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.button.MaterialButton;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


import static com.app.ecobba.common.SharedMethods.showSweetAlertDialogError;

public class GroupFragment extends Fragment implements View.OnClickListener,

        AppBarLayout.OnOffsetChangedListener {

    @BindViews({R.id.addMember, R.id.settings, R.id.addLoan})
    List<FloatingActionButton> options;
    @BindView(R.id.loans)
    FloatingActionButton loans;
    @BindView(R.id.meetings)
    FloatingActionButton meetings;
    @BindView(R.id.contributions)
    FloatingActionButton contributions;

    @BindView(R.id.menu_red)
    FloatingActionMenu menu;
    @BindView(R.id.groupBalanceLL)
    LinearLayout groupBalanceLL;
    @BindView(R.id.btnGroupWallet)
    CardView btnGroupWallet;
    @BindView(R.id.tvTotalFines)
    TextView tvTotalFines;
    @BindView(R.id.tvTotalFees)
    TextView tvTotalFees;
    @BindView(R.id.groupBalance)
    TextView groupBalance;


    @BindView(R.id.tvNoAvailableDataTrans)
    TextView getTvNoAvailableDataTrans;
    @BindView(R.id.tvNoAvailableDataMembers)
    TextView tvNoAvailableDataMembers;
    @BindView(R.id.vfGroup)
    ViewFlipper vfGroup;
    @BindView(R.id.recyclerViewMembers)
    RecyclerView recyclerViewMembers;
    @BindView(R.id.recyclerViewTrans)
    RecyclerView recyclerViewTrans;
    @BindView(R.id.viewAllMembers)
    TextView viewAllMembers;
    @BindView(R.id.viewAllTRansactions)
    TextView viewAllTRansactions;

    @BindView(R.id.ablProfile)
    AppBarLayout ablProfile;
    @BindView(R.id.tvTotalHisa)
    TextView tvTotalHisa;
    @BindView(R.id.tvTotalJamii)
    TextView tvTotalJamii;
    @BindView(R.id.tvGroupName)
    TextView tvGroupName;
    @BindView(R.id.tvTotalMembers)
    TextView tvTotalMembers;

    @BindView(R.id.progress_dialog)
    ProgressBar progress_dialog;

    public String currencyValue;
    String group_id, group_name, token;
    Context context;
    Bundle args;
    Fragment fragment;
    GroupBalanceBotomSheetDialog groupBalanceBotomSheetDialog;

    String totalFines, totalFees, totalGroupBalance, groupMembersCount, totalHisa, totalJamii;
    GroupTransactionsListRecycler adapter;
    GroupMembersRecyclerAdapter adapter_member;
    String member_login_role, role_id;

    boolean isCoordinator = false;
    User loggedInUser;

    boolean isLoggedUserAdmin = false;
    private Api api;
    String group_Name;


    public GroupFragment() {

    }

    int val = 0, numOfItems;
    boolean transparent = true;

    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int i) {

        if (i < val) {
            if (transparent) {
                ((View) getActivity().findViewById(R.id.toolbar)).setBackgroundResource(R.color.colorSurface);
                transparent = false;
                if (menu.isOpened()) {
                    menu.close(true);
                }
                menu.setVisibility(View.GONE);
            }

        } else if (i > val) {

            if (!transparent) {
                ((View) getActivity().findViewById(R.id.toolbar)).setBackgroundResource(R.color.colorSurfaceAlpha);
                transparent = true;
                menu.setVisibility(View.VISIBLE);
            }
        }
        menu.setTranslationX(-i);
        menu.setTranslationY(-i);

        val = i;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_group, container, false);
        api = new RxApi().getApi();

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        context = getActivity();
        ButterKnife.bind(this, view);
        getUserProfile();
        options.get(0).setImageResource(R.drawable.ic_add_user);
        options.get(1).setImageResource(R.drawable.ic_settings_primary);
        options.get(2).setImageResource(R.drawable.ic_create_package);
        loans.setImageResource(R.drawable.ic_package3);
        meetings.setImageResource(R.drawable.ic_meeting);
        contributions.setImageResource(R.drawable.ic_baseline_money);
        viewAllMembers.setOnClickListener(this);
        viewAllTRansactions.setOnClickListener(this);
        groupBalanceLL.setOnClickListener(this);
        tvGroupName.setOnClickListener(this);


        btnGroupWallet.setOnClickListener(this);


        fragment = this;
        recyclerViewTrans = view.findViewById(R.id.recyclerViewTrans);
        recyclerViewTrans.addItemDecoration(new DividerItemDecoration(getContext(),
                DividerItemDecoration.VERTICAL));
        adapter = new GroupTransactionsListRecycler(new ArrayList<>(), requireContext(), fragment, numOfItems);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(requireContext());
        recyclerViewTrans.setLayoutManager(layoutManager);
        recyclerViewTrans.setAdapter(adapter);
        //view members
        recyclerViewMembers = view.findViewById(R.id.recyclerViewMembers);

        adapter_member = new GroupMembersRecyclerAdapter(new ArrayList<>(), requireContext(), fragment);
        RecyclerView.LayoutManager layoutManager1 = new GridLayoutManager(requireContext(), 3);
        recyclerViewMembers.setLayoutManager(layoutManager1);
        recyclerViewMembers.setAdapter(adapter_member);
        role_id = SharedMethods.getDefaults("role", context);
        Log.e("role_member", "onViewCreated: " + role_id);
        ablProfile.addOnOffsetChangedListener(this);

        for (FloatingActionButton floatingActionButton : options) {
            floatingActionButton.setOnClickListener(this);
        }
        loans.setOnClickListener(this);
        meetings.setOnClickListener(this);
        contributions.setOnClickListener(this);

        args = getArguments();
        assert args != null;
        group_id = args.getString("id");
        groupMembersCount = args.getString("groupMembersCount");
        tvTotalMembers.setText(groupMembersCount);
        totalFines = "0";


        fetchGroupDetails(group_id);
        getContributions(group_id);
        getMembers(group_id);
        getTransactions(group_id);


    }

    private void getUserProfile() {
        Call<UserProfileResponse> call = api.getUserProfile();
        call.enqueue(new Callback<UserProfileResponse>() {
            @Override
            public void onResponse(Call<UserProfileResponse> call, Response<UserProfileResponse> response) {
                try {
                    Log.e("dfghj", "onResponse: " + response);
                    if (response.body().getSuccess()) {
                        List<Role> roles = response.body().getUser().getRoles();
                        currencyValue = response.body().getUser().getWallet().getCurrency().getPrefix();
                        for (com.app.ecobba.Models.UserProfile.Role role1 : roles) {
                            member_login_role = role1.getName();
                            if (member_login_role.contains("group-member")) {
                                options.get(0).setVisibility(View.GONE);
                                options.get(1).setVisibility(View.GONE);
                                options.get(2).setVisibility(View.GONE);
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<UserProfileResponse> call, Throwable t) {
                t.getLocalizedMessage();
                Log.e("FAILED_EDDDD", "onFailure: " + t.getLocalizedMessage());

            }
        });
    }

    private void getContributions(String group_id) {
        Call<contributionResponse> call = api.getContributions(group_id);
        call.enqueue(new Callback<contributionResponse>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(Call<contributionResponse> call, Response<contributionResponse> response) {
                Log.e("TRANS_DATA", "onResponse: " + response);
                try {
                    if (response.body().getSuccess()) {
                        totalGroupBalance = String.valueOf(response.body().getData().getContributions().getTOTAL_CONTRIBUTED());
                        totalFees = String.valueOf(response.body().getData().getContributions().getFEES().getTotal());
                        totalHisa = String.valueOf(response.body().getData().getContributions().getSHARES().getTotal());
                        totalJamii = String.valueOf(response.body().getData().getContributions().getSOCIETY().getTotal());
                        groupBalance.setText(currencyValue + " " + totalGroupBalance);
                        tvTotalHisa.setText(currencyValue + " " + totalHisa);
                        tvTotalJamii.setText(currencyValue + " " + totalJamii);
                        tvTotalFees.setText(currencyValue + " " + totalFees);
                        tvTotalFines.setText(currencyValue + " " + totalFines);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<contributionResponse> call, Throwable t) {
                Log.e("CONTRIBUTION_ERROR", "onFailure: " + t.getLocalizedMessage());
            }
        });
    }

    private void GroupBalance() {
        groupBalanceBotomSheetDialog = new GroupBalanceBotomSheetDialog(group_id);
        groupBalanceBotomSheetDialog.show(getChildFragmentManager(), "TAG");
    }

    private void fetchGroupDetails(String group_id) {
        Call<GroupDetailsResponse> call = api.fetchGroupDetails(group_id);
        call.enqueue(new Callback<GroupDetailsResponse>() {
            @Override
            public void onResponse(Call<GroupDetailsResponse> call, Response<GroupDetailsResponse> response) {
                try {
                    if (response.body().getSuccess()) {
                         group_Name = response.body().getData().getGroup().getName();
                        tvGroupName.setText(group_Name);

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<GroupDetailsResponse> call, Throwable t) {
                Log.e("FETCH_NOT_CALLED", t.getMessage());

            }
        });

    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.addLoan:
                Bundle args_ = new Bundle();
                args_.putString("group_id", group_id);
                NavHostFragment.findNavController(this).navigate(R.id.createPackageFragment, args_);
                break;
            case R.id.addMember:
                Bundle args = new Bundle();
                args.putString("group_id", group_id);
                NavHostFragment.findNavController(this).navigate(R.id.createMemberFragment, args);
                break;
            case R.id.meetings:
                Bundle arguments = new Bundle();
                arguments.putString("group_id", group_id);
                SharedPreferences sharedpreferences = getContext().getSharedPreferences("PREFERENCE", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedpreferences.edit();
                editor.putString("group_id", group_id);
                editor.commit();
                // meetingsFragment
                NavHostFragment.findNavController(this).navigate(R.id.fragment_meetings_dashboard);
                break;
            case R.id.loans:
                Bundle _arguments = new Bundle();
                _arguments.putString("group_id", group_id);
                _arguments.putString("group_name", group_name);
                SharedPreferences sharedpref = getContext().getSharedPreferences("PREFERENCE", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor2 = sharedpref.edit();
                editor2.putString("group_id", group_id);
                editor2.putString("group_name", group_name);
                editor2.commit();
                NavHostFragment.findNavController(this).navigate(R.id.groupLoansDashBoard, _arguments);
                break;
            case R.id.contributions:
                Bundle data = new Bundle();
                data.putString("group_id", group_id);
                data.putString("group_name", group_name);
                SharedPreferences sharedpreferences1 = getContext().getSharedPreferences("PREFERENCE", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor1 = sharedpreferences1.edit();
                editor1.putString("group_id", group_id);
                editor1.commit();
                NavHostFragment.findNavController(this).navigate(R.id.contributionsDashBoardFragment);
                break;
            case R.id.settings:
                Bundle __arguments = new Bundle();
                __arguments.putString("group_id", group_id);
                __arguments.putString("group_name", group_Name);
                NavHostFragment.findNavController(this).navigate(R.id.groupSettingsFragments, __arguments);
                break;

            case R.id.btnGroupWallet:

                if (member_login_role.contains("group-admin")) {
                    Bundle bundle = new Bundle();
                    bundle.putString("group_id", group_id);
                    bundle.putString("group_name", group_name);
                    NavHostFragment.findNavController(this).navigate(R.id.groupWalletPaymentSourcesFragment, bundle);
                } else {
                    Toast toast = Toast.makeText(context, R.string.only_grou_admin, Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                }
                break;
            case R.id.viewAllTRansactions:
                Bundle ____arguments = new Bundle();
                ____arguments.putString("group_id", group_id);
                ____arguments.putString("group_name", group_name);
                NavHostFragment.findNavController(this).navigate(R.id.viewAllTRansactions, ____arguments);
                break;
            case R.id.viewAllMembers:
                Bundle ___arguments = new Bundle();
                ___arguments.putString("group_id", group_id);
                ___arguments.putString("group_name", group_name);
                NavHostFragment.findNavController(this).navigate(R.id.viewAllMembers, ___arguments);
                break;
            case R.id.groupBalanceLL:
                GroupBalance();
                break;
            case R.id.tvGroupName:
                GroupBalance();
                break;
        }
    }


    private void getTransactions(String group_id) {
        progress_dialog.setVisibility(View.VISIBLE);
        Call<groupTransactionResponse> call = api.getGroupTransactions(group_id);
        call.enqueue(new Callback<groupTransactionResponse>() {
            @Override
            public void onResponse(Call<groupTransactionResponse> call, Response<groupTransactionResponse> response) {
                Log.e("DATA_LOG_RES", "DATA IS" + response);
                try {
                    if (response.body().getSuccess()) {
                        progress_dialog.setVisibility(View.GONE);
                        if (response.body().getData().getAll_transactions().size() > 0) {
                            adapter.datalist = response.body().getData().getAll_transactions();
                            adapter.notifyDataSetChanged();
                        } else {
                            getTvNoAvailableDataTrans.setText(getResources().getString(R.string.no_available_data));
                            getTvNoAvailableDataTrans.setVisibility(View.VISIBLE);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<groupTransactionResponse> call, Throwable t) {

            }
        });
    }

    private void getMembers(String group_id) {
        Call<groupMembersResponse> call = api.getGroupMmebers(group_id);
        call.enqueue(new Callback<groupMembersResponse>() {
            @Override
            public void onResponse(Call<groupMembersResponse> call, Response<groupMembersResponse> response) {
                try {
                    if (response.body().getSuccess()) {
                        if (response.body().getData().size() > 0) {
                            adapter_member.datalist = response.body().getData();
                            adapter_member.notifyDataSetChanged();
                        } else {
                            tvNoAvailableDataMembers.setText(getResources().getString(R.string.no_available_data));
                            tvNoAvailableDataMembers.setVisibility(View.VISIBLE);

                        }
                    } else if (!response.body().getSuccess()) {
                        String api_error_message = "Sorry";
                        showSweetAlertDialogError(api_error_message, context);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<groupMembersResponse> call, Throwable t) {

            }
        });
    }

}
