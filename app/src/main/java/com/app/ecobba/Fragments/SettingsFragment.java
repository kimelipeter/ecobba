package com.app.ecobba.Fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.app.ecobba.Activities.AppSettingsActivity;
import com.app.ecobba.Dialogs.PinDialogFragment;
import com.app.ecobba.R;
import com.app.ecobba.common.SharedMethods;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SettingsFragment extends Fragment implements CompoundButton.OnCheckedChangeListener,View.OnClickListener {

    @BindView(R.id.sLogin)
    Switch login;

    @BindView(R.id.sBack)
    Switch back;

    @BindView(R.id.tvSet)
    TextView pinSet;

    @BindView(R.id.btnChangePin)
    Button changePin;

    @BindView(R.id.userSerttingsLLayout)
    LinearLayout userSerttingsLLayout;

    Context ctx;

    boolean _pinSet = false;

    public SettingsFragment(){

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View frag = inflater.inflate(R.layout.fragment_settings,container,false);
        ButterKnife.bind(this,frag);
        ctx = getActivity();
        login.setOnCheckedChangeListener(this);
        back.setOnCheckedChangeListener(this);
        changePin.setOnClickListener(this);
        userSerttingsLLayout.setOnClickListener(this);
        return frag;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        String remember = SharedMethods.getDefaults("remember",ctx),
                background = SharedMethods.getDefaults("background",ctx);
        if (remember!=null){
            login.setChecked(true);
        }
        if (background!=null){
            back.setChecked(true);
        }

        String pin = SharedMethods.getDefaults("PIN",ctx);
        if (pin == null || pin.isEmpty()){
            pinSet.setText(getString(R.string.not_set));
            _pinSet = false;
        }else {
            pinSet.setText(getString(R.string.set));
            _pinSet = true;
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
        switch (compoundButton.getId()){
            case R.id.sLogin:
                if (b){
                    SharedMethods.storePrefs("remember","yes",ctx);
                }else{
                    SharedMethods.deletePrefs("remember",ctx);
                }
                break;
            case R.id.sBack:
                if (b){
                    SharedMethods.storePrefs("background","yes",ctx);
                }else{
                    SharedMethods.deletePrefs("background",ctx);
                }
                break;
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btnChangePin:
                new PinDialogFragment(_pinSet).SetOnConfirm(()->{
                    getActivity().recreate();
                }).show(getFragmentManager(),"PIN CHANGE");
                break;

            case R.id.userSerttingsLLayout:
                Intent intent =new Intent(ctx, AppSettingsActivity.class);
                startActivity(intent);
                break;
        }
    }
}
