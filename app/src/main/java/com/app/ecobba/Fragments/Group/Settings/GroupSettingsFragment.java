package com.app.ecobba.Fragments.Group.Settings;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.ecobba.Fragments.Group.Settings.Adapters.GroupSettingsAdapter;
import com.app.ecobba.Models.Group_Settings.groupSettingsResponse;
import com.app.ecobba.R;
import com.app.ecobba.common.SharedMethods;
import com.app.ecobba.common.rxApi.Api;
import com.app.ecobba.common.rxApi.RxApi;
import com.google.android.material.appbar.AppBarLayout;


import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GroupSettingsFragment extends Fragment implements View.OnClickListener,
        AppBarLayout.OnOffsetChangedListener{

    @BindView(R.id.tvGroupName)
    TextView tvGroupName;

    @BindView(R.id.tvNoDataError)
    TextView tvNoData;

    @BindView(R.id.rvSettings)
    RecyclerView rvSettings;

    @BindView(R.id.ablProfile)
    AppBarLayout ablProfile;



    Context context;
    Fragment fragment;
    GroupSettingsAdapter adapter;
    String token,group_name;
    public  String group_id;
    Bundle args;
    private Api api;
    ProgressDialog progressDialog;

    public GroupSettingsFragment(){

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View frag = inflater.inflate(R.layout.fragment_group_settings,container,false);
        ButterKnife.bind(this,frag);
        context = getActivity();
        fragment=this;
        token = SharedMethods.getDefaults("token",context);
        api=new RxApi().getApi();
        args = getArguments();
        assert getArguments() != null;
        group_id = getArguments().getString("group_id");
        ablProfile.addOnOffsetChangedListener(this);
        adapter = new GroupSettingsAdapter(new ArrayList<>(), requireContext(), fragment,group_id,token);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(requireContext());
        rvSettings.setLayoutManager(layoutManager);
        rvSettings.setAdapter(adapter);
        return frag;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        context = getActivity();


        progressDialog = new ProgressDialog(context);
        assert args != null;
        group_name  =args.getString("group_name");
        tvGroupName.setText(group_name);
        getSettingsFromServer(group_id);

    }

    private void getSettingsFromServer(String group_id){
        progressDialog.setMessage("Loading.Please wait....");
        progressDialog.show();
        Call<groupSettingsResponse> call = api.GetgroupSettings(group_id);
        call.enqueue(new Callback<groupSettingsResponse>() {
            @SuppressLint("NotifyDataSetChanged")
            @Override
            public void onResponse(Call<groupSettingsResponse> call, Response<groupSettingsResponse> response) {
                if (response.body().getSuccess()) {
                    progressDialog.dismiss();
                    if (response.body().getSettings().size() > 0) {
                        adapter.datalist = response.body().getSettings();
                        adapter.notifyDataSetChanged();

                    } else {
//                        tvNoAvailableData.setText(getResources().getString(R.string.no_available_data));
//                        tvNoAvailableData.setVisibility(View.VISIBLE);
                    }
                }
            }

            @Override
            public void onFailure(Call<groupSettingsResponse> call, Throwable t) {

            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){

        }
    }



    int val = 0;
    boolean transparent = true;

    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int i) {

        if (i<val){
            if (transparent) {
                ((View) getActivity().findViewById(R.id.toolbar)).setBackgroundResource(R.color.colorSurface);
                transparent = false;
            }
        }else if(i > val){

            if (!transparent) {
                ((View) getActivity().findViewById(R.id.toolbar)).setBackgroundResource(R.color.colorSurfaceAlpha);
                transparent = true;
            }
        }

        val = i;
    }
}
