package com.app.ecobba.Fragments.Borrowing.apply;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.app.ecobba.Activities.MainActivity;
import com.app.ecobba.R;
import com.app.ecobba.common.SharedMethods;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.app.ecobba.common.ApisKtKt.confirmLoanApplication;
import static com.app.ecobba.common.AppConstantsKt.INITIATOR_APPLY_LOAN;
import static com.app.ecobba.common.AppConstantsKt.INITIATOR_CONFIRM_APPLY_LOAN;
import static com.app.ecobba.common.SharedMethods.HTTP_POST;
import static com.app.ecobba.common.SharedMethods.global_error_message;
import static com.app.ecobba.common.SharedMethods.postToServer;
import static com.app.ecobba.common.SharedMethods.runOnUI;
import static com.app.ecobba.common.SharedMethods.showAlertAndMoveToPage;
import static com.app.ecobba.common.SharedMethods.showSweetAlertDialogError;

public class LoanConfirmationScreen extends Fragment implements  SharedMethods.NetworkObjectListener {

    Context ctx;
    @BindView(R.id.cancel_apply_loan)
    LinearLayout cancel_apply_loan;
    @BindView(R.id.btApplyLoan)
    LinearLayout apply_loan;
    @BindView(R.id.tv_loan_title_name)
    TextView tv_loan_title_name;
    @BindView(R.id.tv_no_of_installments)
    TextView tv_no_of_installments;
    @BindView(R.id.tv_payment_frequency)
    TextView tv_payment_frequency;
    @BindView(R.id.tv_currency)
    TextView tv_currency;
    @BindView(R.id.principle_amount)
    TextView principle_amount;
    @BindView(R.id.total_interest_amount)
    TextView total_interest_amount;
    @BindView(R.id.monthly_amount_deducted)
    TextView monthly_amount_deducted;
    @BindView(R.id.annual_interest_rate)
    TextView annual_interest_rate;
    @BindView(R.id.sub_totals_value)
    TextView sub_totals_value;
    @BindView(R.id.processing_fee)
    TextView processing_fee;
    @BindView(R.id.grand_total)
    TextView grand_total;
    String regex = "(?<=[\\d])(,)(?=[\\d])";
    Pattern p = Pattern.compile(regex);
    String strtotalAmount,strsubTotalAmount,str_installmentAmount,str_ex_principal;

    String id,ext_principle,subTotal,totalAmount,totalInterest,serviceFee,annualInterest,currencyUnit,amount,loan_title,length_of_loan,repayment_plan_id;
    String installmentAmount,titleOfLoan,lengthOfLoan,expectedScore,score,walletBalance,paymentFrequency;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.apply_for_loan_confirmation_screen, container, false);
        ButterKnife.bind(this, v);
        ctx = getActivity();

         final String token = SharedMethods.getDefaults("token", ctx);
         assert getArguments() != null;
         id = getArguments().getString("id");
         ext_principle = getArguments().getString("principal" );
         subTotal = getArguments().getString("subTotal" );
         totalAmount = getArguments().getString("totalAmount" );
        Log.e("Loan amount",totalAmount);
         totalInterest = getArguments().getString("totalInterest" );
         installmentAmount = getArguments().getString("installmentAmount" );
         titleOfLoan = getArguments().getString("titleOfLoan" );
         lengthOfLoan = getArguments().getString("lengthOfLoan" );
         expectedScore = getArguments().getString("expectedScore" );
         score = getArguments().getString("score" );
         walletBalance = getArguments().getString("walletBalance" );
         serviceFee = getArguments().getString("serviceFee" );
         paymentFrequency = getArguments().getString("paymentFrequency" );
         annualInterest = getArguments().getString("annualInterest" );
         currencyUnit = getArguments().getString("currencyUnit" );
         loan_title =   getArguments().getString("loan_title" );
//         amount =   getArguments().getString("amount" );
//         length_of_loan =   getArguments().getString("length_of_loan" );
        repayment_plan_id = getArguments().getString("repayment_plan_id" );


        tv_loan_title_name.setText(titleOfLoan);
//        tv_no_of_installments.setText();
        tv_payment_frequency.setText(paymentFrequency);
        tv_currency.setText(currencyUnit);
        ///

        strsubTotalAmount = subTotal;
        Matcher msubtotal = p.matcher(strtotalAmount);
        strsubTotalAmount = msubtotal.replaceAll("");
        Log.e("My subtotal Amount is:",strsubTotalAmount);
        sub_totals_value.setText(strsubTotalAmount);
        ///

        processing_fee.setText(serviceFee);
        ///
//        strtotalAmount = totalAmount;
//        Matcher m = p.matcher(strtotalAmount);
//         strtotalAmount = m.replaceAll("");
        Log.e("grand_total",totalAmount);
        grand_total.setText(totalAmount);
      ///
        annual_interest_rate.setText(annualInterest);

        ///

        str_installmentAmount = installmentAmount;
        Matcher m_installmentAmount= p.matcher(strtotalAmount);
        str_installmentAmount = m_installmentAmount.replaceAll("");
        Log.e("My subtotal Amount is:",str_installmentAmount);
        sub_totals_value.setText(str_installmentAmount);
        ///

        total_interest_amount.setText(totalInterest);

        ///

        str_ex_principal = ext_principle;
        Matcher m_ext_principle= p.matcher(str_ex_principal);
        str_ex_principal = m_ext_principle.replaceAll("");
        Log.e("Ex-Principal Amount:",str_ex_principal);
        principle_amount.setText(str_installmentAmount);
        ///
//        Log.e("Ex-Principal Amount",ext_principle);
//        principle_amount.setText(ext_principle);

        View.OnClickListener apply_loan_ClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //your listener code here
                // call loan confirmation api and go create a package
                confirmLoanPackageApplication(token);
            }
        };
        View.OnClickListener cancel_apply_loan_ClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //your listener code here
//                Intent homeIntent = new Intent(ctx, HomePageBottomNav.class);
//                startActivity(homeIntent);
            }
        };

        apply_loan.setOnClickListener(apply_loan_ClickListener);
        cancel_apply_loan.setOnClickListener(cancel_apply_loan_ClickListener);

        return v;
    }

    public void confirmLoanPackageApplication(String token){
        String params = "{ \"principal\":" + '"' + str_ex_principal + '"' +
                ", \"subTotal\": " + '"' + strsubTotalAmount + '"' +
                ", \"totalAmount\": " + '"' + strtotalAmount + '"' +
                ", \"totalInterest\": " + '"' + totalInterest + '"' +
                ", \"installmentAmount\": " + '"' + str_installmentAmount + '"' +
                ", \"titleOfLoan\": " + '"' + titleOfLoan + '"' +
                ", \"lengthOfLoan\": " + '"' + lengthOfLoan + '"' +
                ", \"expectedScore\": " + '"' + expectedScore + '"' +
                ", \"score\": " + '"' + score + '"' +
                ", \"walletBalance\": " + '"' + walletBalance + '"' +
                ", \"serviceFee\": " + '"' + serviceFee + '"' +
                ", \"paymentFrequency\": " + '"' + paymentFrequency + '"' +
                ", \"annualInterest\": " + '"' + annualInterest + '"' +
                ", \"currencyUnit\": " + '"' + currencyUnit + '"' +
                "}\n";
        Log.e("POST PARAMS", params);
        postToServer( confirmLoanApplication +"/"+id ,token, params , INITIATOR_CONFIRM_APPLY_LOAN , HTTP_POST , ctx ,this);
    }

//
//    public void confirmLoanPackageApplication(String token){
//        String params = "{ \"principal\":" + '"' + ext_principle + '"' +
//                ", \"subTotal\": " + '"' + subTotal + '"' +
//                ", \"lengthOfLoan\": " + '"' + lengthOfLoan + '"' +
//                ", \"totalInterest\": " + '"' + totalInterest + '"' +
//                ", \"totalAmount\": " + '"' + totalAmount + '"' +
//                ", \"expectedScore\": " + '"' + expectedScore + '"' +
//                ", \"installmentAmount\": " + '"' + installmentAmount + '"' +
//                ", \"titleOfLoan\": " + '"' + titleOfLoan + '"' +
//                ", \"score\": " + '"' + score + '"' +
//                ", \"walletBalance\": " + '"' + walletBalance + '"' +
//                ", \"serviceFee\": " + '"' + serviceFee + '"' +
//                ", \"paymentFrequency\": " + '"' + paymentFrequency + '"' +
//                ", \"annualInterest\": " + '"' + annualInterest + '"' +
//                ", \"currencyUnit\": " + '"' + currencyUnit + '"' +
//                ", \"loan_title\": " + '"' + titleOfLoan + '"' +
//                ", \"length_of_loan\": " + '"' + length_of_loan + '"' +
//                ", \"payment_frequency\": " + '"' + repayment_plan_id + '"' +
//                ", \"amount\": " + '"' + amount + '"' +
//                "}\n" + "\n";
//        Log.e("POST PARAMS", params);
//        postToServer( confirmLoanApplication +"/"+id ,token, params , INITIATOR_CONFIRM_APPLY_LOAN , HTTP_POST , ctx ,this);
//    }




    @Override
    public void onDataReady(String initiator, final String response) {
        switch (initiator) {
            case INITIATOR_CONFIRM_APPLY_LOAN:
                try {
                    runOnUI(new Runnable() {
                        @Override
                        public void run() {
                            Log.e("APPLY_LOAN RESPONSE", "Response is " + response);
//                            hideDialog(ctx);
                            JSONObject jObj = null;
                            try {
                                jObj = new JSONObject(response);
                                if (jObj.has("errors")) {
                                    String errors = jObj.getString("errors");
                                    showSweetAlertDialogError(errors, ctx);
                                } else { // if no error message was found in the response,
                                    boolean success = jObj.getBoolean("success");
                                    if (success) { // if the request was successfuls, got forward, else show error message
                                        showAlertAndMoveToPage("Success, You application has been submitted", MainActivity.class,ctx);
                                    } else {
                                        String api_error_message = jObj.getString("message");
                                        showSweetAlertDialogError(api_error_message, ctx);
                                    }
                                }

                            } catch (JSONException e) {
                                Log.e("ExceptionLOGIN-DATA", e.toString());
                                showSweetAlertDialogError(global_error_message, ctx);
                                e.printStackTrace();
                            }
                        }
                    });
                } catch (Exception e) {
//                    hideDialog(ctx);
                    Log.e("Exception- LOGIN", e.toString());
                    showSweetAlertDialogError(global_error_message, ctx);
//                    Toast.makeText(ctx, "Login Data Response Exception" + e.toString(), Toast.LENGTH_LONG).show();
                }
                break;

        }
    }

    @Override
    public void onDataLoaded(String initiator, JSONArray data) {

    }

    @Override
    public void onNetworkError(String initiator, String errorCode, final String errorMessage) {
        switch (initiator) {
            case INITIATOR_APPLY_LOAN:
                try {
                    runOnUI(new Runnable() {
                        @Override
                        public void run() {
//                            hideDialog(ctx);
                            showSweetAlertDialogError(errorMessage, ctx);
                        }
                    });
                } catch (Exception e) {
//                    hideDialog(ctx);
                    showSweetAlertDialogError(global_error_message, ctx);
                    Log.e("Exception- LOGIN", e.toString());
//                    Toast.makeText(ctx, "Login Error Response Exception" + e.toString(), Toast.LENGTH_LONG).show();
                }

                break;
        }
    }
}



