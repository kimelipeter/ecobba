package com.app.ecobba.Fragments;

import android.annotation.SuppressLint;
import android.app.KeyguardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.text.SpannableStringBuilder;
import android.util.Log;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;
import androidx.core.widget.ContentLoadingProgressBar;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;

import com.app.ecobba.Activities.MainActivity;
import com.app.ecobba.Models.UserProfile.Detail;
import com.app.ecobba.ViewModel.DashBoardViewModel;
import com.app.ecobba.ViewModel.UserProfileViewModel;
import com.app.ecobba.common.CheckInternetMethod;
import com.app.ecobba.databinding.FragmentDashBasicBinding;
import com.bumptech.glide.Glide;
import com.app.ecobba.Dialogs.NextLoanPayment;
import com.app.ecobba.R;
import com.app.ecobba.Utils.BaseFragment;
import com.app.ecobba.Utils.ExtensionsKt;
import com.google.android.material.navigation.NavigationView;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import java.lang.reflect.Field;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import de.hdodenhof.circleimageview.CircleImageView;

import static android.app.Activity.RESULT_OK;
import static android.content.Context.KEYGUARD_SERVICE;

import static com.app.ecobba.common.ApisKtKt.getUserReports;


public class DashboardFragment<nextDate> extends BaseFragment implements
        View.OnClickListener {

    @BindView(R.id.parentLinear)
    LinearLayout parentLinear;

    int lastLineaInt;
    int lastCardInt;
    boolean wasFirstOne = false;

    boolean previouslyClicked = false;
    String days = "Days";
    SpannableStringBuilder spannableStringBuilder;

    NextLoanPayment nextLoanPayment;
    @BindViews({
//            R.id.deposit,
//            R.id.withdraw,
            R.id.lending,
//            R.id.loansBtn,
            R.id.transaction,
//            R.id.utilities_btn,
//            R.id.reports,
//            R.id.wallet,
            R.id.tranfer
//            R.id.market
    })
    List<Button> options;
    private Context ctx;
    @BindView(R.id.tvBalance)
    TextView tvBalance;
    @BindView(R.id.cvShop)
    CardView cvShop;
    @BindView(R.id.cvGroups)
    CardView cvGroups;
    @BindView(R.id.cvWallet)
    CardView cvWallet;
    @BindView(R.id.cardviewLoans)
    CardView cardviewLoans;
    @BindView(R.id.cvUtilities)
    CardView cvUtilities;
    @BindView(R.id.cvReports)
    CardView cvReports;

    @BindView(R.id.nextLoanLL)
    LinearLayout nextLoanLL;
    String token;
    private Unbinder unbinder;
    public static String imagetodashboard;
    String name_msisdn;
    Bundle args = new Bundle();

    @BindView(R.id.my_groups)
    TextView my_groups;

    @BindView(R.id.cpbDaysCountDown)
    ContentLoadingProgressBar cpbDaysCountDown;

    @BindView(R.id.tvNextPaymentDay)
    TextView tvNextPaymentDay;
    @BindView(R.id.tvAmount)
    TextView tvAmount;
    TextView tvMyGroups;

    int state = 0;

    @BindView(R.id.lockBalance)
    ImageView lockBalance;
    Date date1, date2, date4;
    public String date, date3, date5;

    private static int CODE_AUTHENTICATION_VERIFICATION_BALANCE = 241;
    private static int CODE_AUTHENTICATION_VERIFICATION_WALLET = 242;
    private String balanceGlobal;
    private boolean hasnotUnlocked = true;
    public String currencyValue;
    public int loanNextAmount;
    public String namePackage, loannameTitle;

    DashBoardViewModel dashBoardViewModel;
    FragmentDashBasicBinding basicBinding;
    int groupsCreated, groupsJoined, allGroups;

    UserProfileViewModel userProfileViewModel;

    public DashboardFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View frag = inflater.inflate(R.layout.fragment_dash_basic, container, false);
        dashBoardViewModel = new ViewModelProvider(this).get(DashBoardViewModel.class);
        userProfileViewModel = new ViewModelProvider(getActivity()).get(UserProfileViewModel.class);
        unbinder = ButterKnife.bind(this, frag);
        for (Button b : options) {
            b.setOnClickListener(this);
        }
        return frag;

    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        basicBinding = FragmentDashBasicBinding.bind(view);
        dashBoardViewModel = new ViewModelProvider(this).get(DashBoardViewModel.class);
        ctx = getActivity();
        fetchUsersData();
        networkBuilderDialog();
        ButterKnife.bind(this, view);
        getUserSummary();
        hasnotUnlocked = true;
        ExtensionsKt.startInitialWorker(requireContext());

        lockBalance.setOnClickListener(v -> {

            if (hasnotUnlocked) {
                callKeyGuard();
            }
        });

        tvAmount.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                nextloanpayment();
            }

        });

        cvShop.setOnClickListener((view1 -> {

            setBackground(1, 1, false);

            NavHostFragment.findNavController(this).navigate(R.id.kweaServicesDashboard);
        }));
        cvGroups.setOnClickListener((view1 -> {
            setBackground(0, 0, true);
            NavHostFragment.findNavController(this).navigate(R.id.groupsFragment);
        }));
        cvWallet.setOnClickListener((view1 -> {
            setBackground(0, 1, false);
            //  NavHostFragment.findNavController(this).navigate(R.id.walletFragment,args);
            KeyguardManager km = (KeyguardManager) requireContext().getSystemService(KEYGUARD_SERVICE);
            if (km.isKeyguardSecure()) {
                Intent i = km.createConfirmDeviceCredentialIntent(getString(R.string.login_to_wallet), getString(R.string.check_device));
                String phone = name_msisdn;
                args.putString("msisdn", phone);
                Log.e("fghjkl", phone);
                startActivityForResult(i, CODE_AUTHENTICATION_VERIFICATION_WALLET);

                // NavHostFragment.findNavController(this).navigate(R.id.walletFragment,args);
            } else {

                String phone = name_msisdn;
                args.putString("msisdn", phone);
                Log.e("fghjkl", phone);
                NavHostFragment.findNavController(this).navigate(R.id.walletFragmentDasboard, args);
            }

        }));
        cardviewLoans.setOnClickListener((view1 -> {
            setBackground(1, 0, false);
            NavHostFragment.findNavController(this).navigate(R.id.loans_lending_dashboard);
        }));
        cvReports.setOnClickListener((view1 -> {
            setBackground(2, 3, false);
            Bundle _args = new Bundle();
            _args.putString("url", getUserReports);
            _args.putBoolean("show", true);
            NavHostFragment.findNavController(this).navigate(R.id.reportsFragment, _args);
        }));
        cvUtilities.setOnClickListener((view1 -> {
            setBackground(2, 0, false);
            NavHostFragment.findNavController(this).navigate(R.id.buy_Airtime);
        }));

    }

    private void callKeyGuard() {

        KeyguardManager km = (KeyguardManager) requireContext().getSystemService(KEYGUARD_SERVICE);
        if (km.isKeyguardSecure()) {

            Intent i = km.createConfirmDeviceCredentialIntent(getString(R.string.login_to_wallet), getString(R.string.check_device));
            startActivityForResult(i, CODE_AUTHENTICATION_VERIFICATION_BALANCE);
        } else {
            tvBalance.setText(balanceGlobal);
            hasnotUnlocked = false;
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == CODE_AUTHENTICATION_VERIFICATION_BALANCE) {
            tvBalance.setText(balanceGlobal);
            hasnotUnlocked = false;
        } else if (resultCode == RESULT_OK && requestCode == CODE_AUTHENTICATION_VERIFICATION_WALLET) {

            NavHostFragment.findNavController(this).navigate(R.id.walletFragmentDasboard, args);
        } else {
            Toast.makeText(ctx, R.string.un_able, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        fetchUsersData();
        if (previouslyClicked) {
            LinearLayout child = (LinearLayout) parentLinear.getChildAt(lastLineaInt);

            CardView cardView = (CardView) child.getChildAt(lastCardInt);
            cardView.setBackgroundResource(R.drawable.card_edge_primary);

            LinearLayout linearLayout;

            if (wasFirstOne) {
                linearLayout = (LinearLayout) cardView.getChildAt(1);
            } else {
                linearLayout = (LinearLayout) cardView.getChildAt(0);
            }
            TextView textView = (TextView) linearLayout.getChildAt(0);
            TextView textView2 = (TextView) linearLayout.getChildAt(1);
            textView2.setTextColor(getResources().getColor(R.color.white));
            textView.setBackground(getResources().getDrawable(R.drawable.circle_side_pri));
        }

        //  fetchUsersData();
    }

    private void nextloanpayment() {
        nextLoanPayment = new NextLoanPayment(date, namePackage, loanNextAmount, loannameTitle);
        nextLoanPayment.show(getChildFragmentManager(), "TaG");
    }

    private void getUserSummary() {
        dashBoardViewModel.getUserSummaryResponseMutableLiveData().observe(getViewLifecycleOwner(), userSummaryResponse -> {
            try {
                if (userSummaryResponse.getSuccess()) {
                    allGroups = userSummaryResponse.getLoans().getMygroups() + userSummaryResponse.getLoans().getGroupsjoined();
                    basicBinding.myGroups.setText(String.valueOf(allGroups));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

        dashBoardViewModel.getDashBoard();
    }

    private void setBackground(int linearIndex, int cardIndex, Boolean isFirstOne) {
        if (previouslyClicked) {
            LinearLayout child = (LinearLayout) parentLinear.getChildAt(lastLineaInt);
            CardView cardView = (CardView) child.getChildAt(lastCardInt);
            cardView.setBackgroundResource(R.drawable.card_edge);
            LinearLayout linearLayout;
            if (wasFirstOne) {
                linearLayout = (LinearLayout) cardView.getChildAt(1);
            } else {
                linearLayout = (LinearLayout) cardView.getChildAt(0);
            }
            TextView textView = (TextView) linearLayout.getChildAt(0);
            TextView textView2 = (TextView) linearLayout.getChildAt(1);
            textView2.setTextColor(getResources().getColor(R.color.black));
            textView.setBackground(getResources().getDrawable(R.drawable.circle_side_sec));
        }

        LinearLayout child = (LinearLayout) parentLinear.getChildAt(linearIndex);
        CardView cardView = (CardView) child.getChildAt(cardIndex);
        cardView.setBackgroundResource(R.drawable.card_edge_primary);
        LinearLayout linearLayout;
        if (isFirstOne) {
            linearLayout = (LinearLayout) cardView.getChildAt(1);
        } else {
            linearLayout = (LinearLayout) cardView.getChildAt(0);
        }
        wasFirstOne = isFirstOne;
        TextView textView = (TextView) linearLayout.getChildAt(0);
        TextView textView2 = (TextView) linearLayout.getChildAt(1);
        textView2.setTextColor(getResources().getColor(R.color.white));
        textView.setBackground(getResources().getDrawable(R.drawable.circle_side_pri));

        lastCardInt = cardIndex;
        lastLineaInt = linearIndex;
        previouslyClicked = true;
    }

    public void networkBuilderDialog() {


        if (CheckInternetMethod.isNetworkAvailable(ctx)){
           fetchUsersData();
           getUserSummary();}
        else{
            CheckInternetMethod.showAlert("Internet Connectivity Failure", getActivity());}
    }


    private void fetchUsersData() {
        userProfileViewModel.getUserProfile();
        userProfileViewModel.userProfileResponseLiveData().observe(getViewLifecycleOwner(), userProfileResponse -> {
            try {
                if (userProfileResponse.getSuccess()) {
                    Detail userDetails = userProfileResponse.getUser().getDetail();
                    String name = userProfileResponse.getUser().getName();
                    String other_names = userProfileResponse.getUser().getOther_names();
                    name_msisdn = userProfileResponse.getUser().getMsisdn();
                    currencyValue = String.valueOf(userProfileResponse.getUser().getDetail().getCountry());
                    try {
                        namePackage = userProfileResponse.getUser().getNext_loan_repayment().getLoan().getLoan_title();
                        String payBackDate = userProfileResponse.getUser().getNext_loan_repayment().getPayment_date();
                        String dateCreated = userProfileResponse.getUser().getNext_loan_repayment().getCreated_at();
                        String countdown = String.valueOf(userProfileResponse.getUser().getNext_loan_repayment().getCountdown());
                        TextView days_remaining = getActivity().findViewById(R.id.days_remaining);
                        days_remaining.setText(countdown);
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
                        sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
                        SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
                        sdf2.setTimeZone(TimeZone.getTimeZone("GMT"));
                        SimpleDateFormat sdf3 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
                        sdf3.setTimeZone(TimeZone.getTimeZone("GMT"));
                        try {
                            date1 = sdf.parse(payBackDate);
                            sdf = new SimpleDateFormat("dd MMMM yyyy");
                            date = sdf.format(date1);
                            date2 = sdf2.parse(dateCreated);
                            sdf2 = new SimpleDateFormat("dd MM yyyy");
                            date3 = sdf2.format(date2);
                            Log.e("date_created_is", date3);
                            date4 = sdf3.parse(payBackDate);
                            sdf3 = new SimpleDateFormat("dd MM yyyy");
                            date5 = sdf3.format(date4);
                            Log.e("date_payback", date5);
                            String dateBefore = date3;
                            String dateAfter = date5;
                            Log.e("number_of_days", dateAfter + dateBefore);
                            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd");
                            LocalDateTime now = LocalDateTime.now();
                            Log.e("Today_is", dtf.format(now));
                            try {
                                Date dateBefore2 = sdf3.parse(dateAfter);
                                Date dateAfter2 = sdf3.parse(dateBefore);
                                long difference = dateBefore2.getTime() - dateAfter2.getTime();
                                int daysBetween = (int) (difference / (1000 * 60 * 60 * 24));
                                progressShow(daysBetween, Integer.parseInt(countdown));
                                Log.e("deference_is", String.valueOf(daysBetween));
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");

                            TextView tvNextPaymentDay = getActivity().findViewById(R.id.tvNextPaymentDay);
                            tvNextPaymentDay.setText(date);
                        } catch (ParseException e) {
                            Log.e("DateAndTime", e.getLocalizedMessage());
                            e.printStackTrace();
                        }

                        if (userProfileResponse.getUser().getNext_loan_repayment().getLoan() != null) {
                            nextLoanLL.setVisibility(View.VISIBLE);
                            loannameTitle = userProfileResponse.getUser().getNext_loan_repayment().getLoan().getLoan_title();
                            loanNextAmount = userProfileResponse.getUser().getNext_loan_repayment().getLoan().getAmount();
                        }


                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    String avatar = userProfileResponse.getUser().getAvatar();
                    Log.e("Avater", avatar);
                    if (userProfileResponse.getUser().getWallet() != null) {
                        int balance = userProfileResponse.getUser().getWallet().getBalance();
                        String MyCurrency = userProfileResponse.getUser().getWallet().getCurrency().getPrefix();
                        currencyValue = MyCurrency;
                        TextView tvAmount = getActivity().findViewById(R.id.tvAmount);
                        tvAmount.setText("-" + currencyValue + " " + loanNextAmount);
                        TextView tvWalletCurrency = getActivity().findViewById(R.id.currencyWallet);
                        tvWalletCurrency.setText(currencyValue);
                        TextView tvWalletBalance = getActivity().findViewById(R.id.revenue);
                        try {
                            tvWalletBalance.setText(new DecimalFormat("0.00").format(balance));
                            balanceGlobal = new DecimalFormat("0.00").format(balance);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    if (userProfileResponse.getUser().getDetail() != null) {

                        Pair<Integer, Integer> profile = showProfileProgress(userDetails);


                        int max = profile.second;
                        int progress = profile.first;

                        float percentage = ((float) progress) / max;

                        Log.e("PERCENTAGE", String.valueOf(percentage));

                        ((ProgressBar) getActivity().findViewById(R.id.pbProfile)).setMax(max);

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                            ((ProgressBar) getActivity().findViewById(R.id.pbProfile)).setProgress(progress, true);
                        } else {
                            ((ProgressBar) getActivity().findViewById(R.id.pbProfile)).setProgress(progress);
                        }


                        if (percentage > 0.80) {
                            getActivity().findViewById(R.id.cvProfile).setVisibility(View.GONE);
                        } else {


                        }
                    }
                    int credit_score = userProfileResponse.getUser().getCredit_score();
                    String imageUrl = userProfileResponse.getUser().getAvatar();
                    imagetodashboard = imageUrl;
                    NavigationView navigationView = requireActivity().findViewById(R.id.nav_view);
                    CircleImageView imageView2 = navigationView.getHeaderView(0).findViewById(R.id.profile_image);

                    Glide.with(this).load(imageUrl)
                            //.error(new RequestBuilder<>())
                            .placeholder(R.drawable.ic_user_primary)
                            .circleCrop().into(imageView2);

                    String full_names = name + " " + other_names;
                    String user_hash_tag = "@" + name + "_" + other_names;
                    // SHOW ON NAVIGATION VIEW - uncomment below code
                    Log.e("use_hash_tag_name", user_hash_tag);
                    TextView tvEmail = getActivity().findViewById(R.id.emailContact);
                    TextView tvUsername = getActivity().findViewById(R.id.userName);
                    CircleImageView imageView = getActivity().findViewById(R.id.profile_image);
                    TextView tvCurrencyVal = getActivity().findViewById(R.id.currency_val);
                    TextView tvCreditScore = getActivity().findViewById(R.id.tvCreditScore);
                    if (credit_score < 5) {
                        tvCreditScore.setTextColor(getResources().getColor(R.color.red));
                    } else if (credit_score > 5 && credit_score < 7.5) {
                        tvCreditScore.setTextColor(getResources().getColor(R.color.orange));
                    } else {
                        tvCreditScore.setTextColor(getResources().getColor(R.color.green));
                    }
                    tvCreditScore.setText(new DecimalFormat("0.00").format(credit_score));
                    tvEmail.setText(user_hash_tag);
                    tvUsername.setText(full_names);
                    tvCurrencyVal.setText(currencyValue);
                    //set Image Using Picasso
                    Picasso.get().load(imageUrl)
                            .placeholder(R.drawable.ic_user_primary).error(R.drawable.ic_user_primary)
                            .into(imageView);


                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });


    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.lending:
                NavHostFragment.findNavController(this).navigate(R.id.packagesFragment);
                break;
            case R.id.transaction:
                NavHostFragment.findNavController(this).navigate(R.id.transactionsFragment);
                break;
        }
    }

    public void progressShow(int maximumDays, int remainingDays) {
        cpbDaysCountDown.setMax(maximumDays);
        cpbDaysCountDown.setProgress(remainingDays);
    }

    @SuppressLint("LongLogTag")
    public Pair<Integer, Integer> showProfileProgress(Detail userDetails) {
        Log.e("PROFILE_PROGRESS", new Gson().toJson(userDetails));
        Field[] fields = userDetails.getClass().getDeclaredFields();

        int total = fields.length - 1;
        int filled = 0;
        for (Field field : fields) {
            field.setAccessible(true);
            try {
                String val = String.valueOf(field.get(userDetails));
                Log.e("ALL_FIELDS_ARE", val);
                if (val.equals("0")) {
                    Log.e("PROFILE_PROGRESS->0", val);
                } else if (val.equals("null")) {
                    Log.e("PROFILE_PROGRESS->null", val);
                } else if (!val.isEmpty()) {
                    Log.e("PROFILE_PROGRESS->notempty", val);
                    filled++;
                }
            } catch (IllegalAccessException e) {
                e.printStackTrace();
                Log.e("PROFILE_PROGRESS", e.toString());
            }
        }

        float val = filled / total;
        Log.e("PROFILE_PROGRESS", val + "-" + filled + "/" + total);
        return new Pair<Integer, Integer>(filled, total);
    }
}
