package com.app.ecobba.Fragments.Borrowing.Borrowed.ApiResponse

data class FetchMyLoansResponse(
    val loans: List<Loan>,
    val message: String,
    val success: Boolean
)