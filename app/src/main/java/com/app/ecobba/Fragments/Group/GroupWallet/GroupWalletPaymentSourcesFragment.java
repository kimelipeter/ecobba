package com.app.ecobba.Fragments.Group.GroupWallet;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.app.ecobba.Fragments.Wallet.PaymentSourcesAdapter;
import com.app.ecobba.Fragments.Wallet.SetPaymentSourceDialog;
import com.app.ecobba.Models.GroupWallet.groupWalletResponse;
import com.app.ecobba.Models.PaymentSources.paymentSourcesResponse;
import com.app.ecobba.Models.UserProfile.Detail;
import com.app.ecobba.R;
import com.app.ecobba.Utils.BaseFragment;
import com.app.ecobba.ViewModel.UserProfileViewModel;
import com.app.ecobba.common.rxApi.Api;
import com.app.ecobba.common.rxApi.RxApi;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GroupWalletPaymentSourcesFragment extends BaseFragment implements View.OnClickListener {
    @BindView(R.id.btnSetPaymentPreference)
    Button btnSetPaymentPreference;
    private Api api;
    UserProfileViewModel userProfileViewModel;
    @BindView(R.id.create)
    FloatingActionButton create;
    String group_id, type, telco, msisdn, group_name;
    Context context;
    Fragment fragment;
    GroupWalletAdapter adapter;
    public String avatar;
    Bundle args;

    RecyclerView recyclerView;
    SwipeRefreshLayout mSwipeRefreshLayout;

    public GroupWalletPaymentSourcesFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_payment_sources, container, false);
        mSwipeRefreshLayout = view.findViewById(R.id.swipe_container);
        api = new RxApi().getApi();
        userProfileViewModel = new ViewModelProvider(getActivity()).get(UserProfileViewModel.class);

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        context = getActivity();
        args = getArguments();
        assert args != null;
        group_id = args.getString("group_id");
        Log.d("PARSED_DATA", args.toString());
        group_name = args.getString("group_name");

        create.setOnClickListener(this);
        btnSetPaymentPreference.setOnClickListener(this);
        recyclerView = view.findViewById(R.id.recyclerView);

        recyclerView.addItemDecoration(new DividerItemDecoration(getContext(),
                DividerItemDecoration.VERTICAL));
        adapter = new GroupWalletAdapter(new ArrayList<>(), requireContext(), fragment);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(requireContext());
        recyclerView.setLayoutManager(layoutManager);

        recyclerView.setAdapter(adapter);
        fetchUsersData();
        fetchUserPaymentSoucers(group_id);

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // Toast.makeText(ctx,"Refreshing", Toast.LENGTH_LONG).show();
                fetchUserPaymentSoucers(group_id);
                mSwipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary,
                        android.R.color.holo_green_dark,
                        android.R.color.holo_orange_dark,
                        android.R.color.holo_blue_dark);

                mSwipeRefreshLayout.setRefreshing(false);
            }
        });
    }

    private void fetchUserPaymentSoucers(String group_id) {
        Call<groupWalletResponse> call = api.getGroupWalletPaymentSources(group_id);
        call.enqueue(new Callback<groupWalletResponse>() {
            @Override
            public void onResponse(Call<groupWalletResponse> call, Response<groupWalletResponse> response) {
                Log.e("TAG_payment_sources", "onResponse: " + response);
                if (response.body().getSuccess()) {
                    if (response.body().getAdapay_sources().size() > 0) {
                        adapter.datalist = response.body().getAdapay_sources();
                        adapter.notifyDataSetChanged();
                    } else {

                    }
                }


            }

            @Override
            public void onFailure(Call<groupWalletResponse> call, Throwable t) {

            }
        });
    }

    private void fetchUsersData() {
        userProfileViewModel.getUserProfile();
        userProfileViewModel.userProfileResponseLiveData().observe(getViewLifecycleOwner(), userProfileResponse -> {
            try {
                if (userProfileResponse.getSuccess()) {
                    Detail userDetails = userProfileResponse.getUser().getDetail();
                    String name = userProfileResponse.getUser().getName();
                    avatar = userProfileResponse.getUser().getAvatar();
                    Log.e("USER_AVATAR", "fetchUsersData: " + avatar);
                    msisdn = userProfileResponse.getUser().getMsisdn();
                    Log.e("USER_PHONE_NO", msisdn);

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.create:

                Bundle arg = new Bundle();
                arg.putString("group_id", group_id);
                SetGroupWallet setPaymentSourceDialog = new SetGroupWallet(group_id);
                setPaymentSourceDialog.show(getActivity().getSupportFragmentManager(), "PAYMENT_TAG_SOURCE");
                break;
//            case R.id.btnSetPaymentPreference:
//
//                Bundle bundle = new Bundle();
//                bundle.putString("msisdn", msisdn);
//                SetPaymentSourceDialog setPaymentSourceDialogs = new SetPaymentSourceDialog(msisdn);
//                setPaymentSourceDialogs.show(getActivity().getSupportFragmentManager(), "PAYMENT_TAG");
//                break;
        }
    }
}
