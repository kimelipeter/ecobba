
package com.app.ecobba.Fragments.Group.GroupModel;

import lombok.Data;

@Data
@SuppressWarnings("unused")
public class RegionDetail {

    private long countryId;
    private String createdAt;
    private long id;
    private Object levelFour;
    private String levelOne;
    private Object levelThree;
    private String levelTwo;
    private String updatedAt;

}
