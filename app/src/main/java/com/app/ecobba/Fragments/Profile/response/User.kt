package com.app.ecobba.Fragments.Profile.response

import com.google.gson.annotations.SerializedName
import lombok.Data

@Data
class User {
    @SerializedName(value = "account_no", alternate = ["accountNo"])
    var accountNo: String? = ""
    @SerializedName(value = "account_verified", alternate = ["accountVerified"])
    var accountVerified = 0
    var avatar: String? = ""
    @SerializedName(value = "created_at", alternate = ["createdAt"])
    var createdAt: String? = ""
    @SerializedName(value = "credit_score", alternate = ["creditScore"])
    var creditScore = 0
    @SerializedName(value = "customer_username", alternate = ["customerUsername"])
    var customerUsername: String? = ""
    @SerializedName(value = "deleted_at", alternate = ["deletedAt"])
    var deletedAt: String? = ""
    var detail: Detail? = null
    var email: String? = ""
    @SerializedName(value = "email_verified_at", alternate = ["emailVerifiedAt"])
    var emailVerifiedAt: String? = ""
    var id = 0
    @SerializedName(value = "identification_document", alternate = ["identificationDocument"])
    var identificationDocument: String? = ""
    var msisdn: String? = ""
    var name: String? = ""
    //    String organization;
    @SerializedName(value = "other_names", alternate = ["otherNames"])
    var otherNames: String? = ""
    var roles: List<Role>? = null
    var status = 0
    var token: String? = ""
    var transactions: Transactions? = null
    var organization: Organization? = null
    @SerializedName(value = "updated_at", alternate = ["updatedAt"])
    var updatedAt: String? =""
    var verified = 0
    var wallet: Wallet? = null
}