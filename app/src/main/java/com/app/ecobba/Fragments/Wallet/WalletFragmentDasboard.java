package com.app.ecobba.Fragments.Wallet;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;
import com.app.ecobba.R;
import com.app.ecobba.Utils.BaseFragment;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;

public class WalletFragmentDasboard extends BaseFragment {
    public WalletFragmentDasboard() {
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_wallet_dashboard,container,false);
        return view;
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        WalletViewPagerAdapter adapter = new WalletViewPagerAdapter(getChildFragmentManager(),getContext());
        ArrayList<Fragment> fragments = new ArrayList<>();
        fragments.add(new WalletFragment());
        fragments.add(new PaymentSources());
        adapter.setFragments(fragments);
        ViewPager viewPager = view.findViewById(R.id.pager2);
        viewPager.setAdapter(adapter);
        TabLayout tabLayout = view.findViewById(R.id.tablayout2);
        tabLayout.setupWithViewPager(viewPager);
    }
}
