package com.app.ecobba.Fragments.chat.data

data class SendMessage(
    val message: String,
    val `receiver`: String
)