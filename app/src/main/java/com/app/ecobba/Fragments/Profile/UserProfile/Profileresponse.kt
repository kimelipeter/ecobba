package com.app.ecobba.Fragments.Profile.UserProfile

import com.app.ecobba.Fragments.Profile.response.User
import lombok.Data

@Data
class Profileresponse {
    var message: String? = null
    var success: Boolean? = null
    var user: User? = null
}