package com.app.ecobba.Fragments.Lending;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.app.ecobba.Activities.MainActivity;
import com.app.ecobba.Models.LendingData.Currency;
import com.app.ecobba.Models.LendingData.Repaymentplan;
import com.app.ecobba.Models.LendingData.lendingDataResponse;
import com.app.ecobba.Models.PaymentSources.AdapaySource;
import com.app.ecobba.Models.PaymentSources.paymentSourcesResponse;
import com.app.ecobba.Models.adapay.adapaySourcesResponse;
import com.app.ecobba.Models.adapay.telcos.telcosResponse;
import com.app.ecobba.R;
import com.app.ecobba.common.SharedMethods;
import com.app.ecobba.common.rxApi.Api;
import com.app.ecobba.common.rxApi.RxApi;
import com.flipboard.bottomsheet.BottomSheetLayout;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.jaredrummler.materialspinner.MaterialSpinner;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.app.ecobba.common.ApisKtKt.createGroupPackage;
import static com.app.ecobba.common.ApisKtKt.createPackage;
import static com.app.ecobba.common.ApisKtKt.getLendingData;
import static com.app.ecobba.common.AppConstantsKt.INITIATOR_CREATE_LOAN_PACKAGE;
import static com.app.ecobba.common.AppConstantsKt.INITIATOR_GETCREATE_LOAN_DATA;
import static com.app.ecobba.common.SharedMethods.HTTP_GET;
import static com.app.ecobba.common.SharedMethods.HTTP_POST;
import static com.app.ecobba.common.SharedMethods.getFromServer;
import static com.app.ecobba.common.SharedMethods.global_error_message;
import static com.app.ecobba.common.SharedMethods.postToServer;
import static com.app.ecobba.common.SharedMethods.runOnUI;
import static com.app.ecobba.common.SharedMethods.showAlertAndMoveToPage;
import static com.app.ecobba.common.SharedMethods.showSweetAlertDialogError;

public class CreatePackageFragment extends Fragment implements
        SharedMethods.NetworkObjectListener,
        View.OnClickListener, MaterialSpinner.OnItemSelectedListener {

    @BindView(R.id.name)
    EditText name;
    @BindView(R.id.repaymentPlan)
    MaterialSpinner repaymentPlan;
    @BindView(R.id.insureLoan)
    MaterialSpinner insureLoan;
    @BindView(R.id.currency)
    MaterialSpinner currency;
    @BindView(R.id.minCreditScore)
    EditText minCreditScore;
    @BindView(R.id.minAmount)
    EditText minAmount;
    @BindView(R.id.maxAmount)
    EditText maxAmount;
    @BindView(R.id.repayment_period)
    EditText repayment_period;
    @BindView(R.id.interestRate)
    EditText interestRate;
    @BindView(R.id.fineRate)
    EditText fineRate;
    @BindView(R.id.description)
    EditText description;
    @BindView(R.id.help)
    Button help;
    @BindView(R.id.create)
    Button create;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    BottomSheetLayout helpSheet;
    private Api api;

    @BindViews({
            R.id.helptext1,
            R.id.helptext2,
            R.id.helptext3,
            R.id.helptext4,
    })
    List<TextView> helpText;
    @BindViews({
            R.id.help1,
            R.id.help2,
            R.id.help3,
            R.id.help4
    })
    List<Button> helpButtons;

    BottomSheetBehavior bottomSheetBehavior;
    ColorDrawable popupColor;
    private Unbinder unbinder;

    String currency_id_toapi = "0";
    String insure_loan_status_id_toapi = "0", repayment_plan = "0", token;
    List<String> currencies_ids, repayment_plans, insure_statuses;
    Context ctx;

    String min_score, _name, min_amount, max_amount, interest, item_visibility, _description, selected_type, selected_provider, _msisdn, _finerate;

    String group_id = "0",ada_id="";
    Spinner spinner_package_visibilty, msisdn;

    List<String> ada_pay_ids, telcos_adapay;
    LinearLayout linearLayoutBank;
    LinearLayout linearLayoutMobile, llLayoutMobileMpesa;
    Spinner payMethod, provider;
    ArrayAdapter arrayAdapter;
    String _repayment_period;

    public CreatePackageFragment() {
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View frag = inflater.inflate(R.layout.fragment_create_package, container, false);
        unbinder = ButterKnife.bind(this, frag);
        api = new RxApi().getApi();
        payMethod = frag.findViewById(R.id.payMethod);
        provider = frag.findViewById(R.id.provider);

        msisdn = frag.findViewById(R.id.msisdn);
        linearLayoutMobile = frag.findViewById(R.id.linearLayoutMobile);
        llLayoutMobileMpesa = frag.findViewById(R.id.llLayoutMobileMpesa);
        linearLayoutBank = frag.findViewById(R.id.linearLayoutBank);
        popupColor = new ColorDrawable(getResources().getColor(R.color.colorSurface));
        helpSheet = getActivity().findViewById(R.id.bottomSheetLayout);
        helpSheet.setShouldDimContentView(true);
        helpSheet.setPeekOnDismiss(true);
        helpSheet.setPeekSheetTranslation(500);
        helpSheet.setUseHardwareLayerWhileAnimating(true);
        for (Button b : helpButtons) {
            b.setOnClickListener(this);
        }
        Bundle args = getArguments();

        if (args != null) {
            group_id = args.getString("group_id") == null ? "0" : args.getString("group_id");
            Log.e("GROUP_ID", group_id);
        }
        spinner_package_visibilty = frag.findViewById(R.id.package_visibilty);
        return frag;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ctx = getActivity();
        token = SharedMethods.getDefaults("token", ctx);
        fetchFormDropDownData();
        fetchAdaPaySourceType();
        fetchAdaPayTelcos();
        fetchUserPaymentSoucers();
        currency.setOnItemSelectedListener(this);
        insureLoan.setOnItemSelectedListener(this);
        repaymentPlan.setOnItemSelectedListener(this);
        create.setOnClickListener(this);

        repaymentPlan.getPopupWindow().setBackgroundDrawable(popupColor);
//        insureLoan.setItems(getResources().getStringArray(R.array.insure));
        insureLoan.getPopupWindow().setBackgroundDrawable(popupColor);
//        currency.setItems(getResources().getStringArray(R.array.currency));
        currency.getPopupWindow().setBackgroundDrawable(popupColor);
        help.setOnClickListener(this);
        token = SharedMethods.getDefaults("token", ctx);
        spinner_package_visibilty.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                item_visibility = adapterView.getItemAtPosition(i).toString().toLowerCase();
                Log.e("item_visibility", item_visibility);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.help:
                if (!helpSheet.isSheetShowing()) {
                    helpSheet.showWithSheetView(getLayoutInflater().inflate(R.layout.help_create_loanpackage, helpSheet, false));
                }
                break;
            case R.id.help1:
                toggleVisibility(helpText.get(0));
                break;
            case R.id.help2:
                toggleVisibility(helpText.get(1));
                break;
            case R.id.help3:
                toggleVisibility(helpText.get(2));
                break;
            case R.id.help4:
                toggleVisibility(helpText.get(3));
                break;
            case R.id.create:
                if (validateFormFields()) {
                    createPackageToAPI();
                }
                break;
        }
    }

    private boolean validateFormFields() {
        boolean valid = true;

        min_score = minCreditScore.getText().toString();
        _name = name.getText().toString();
        min_amount = minAmount.getText().toString();
        max_amount = maxAmount.getText().toString();
        _repayment_period = repayment_period.getText().toString();
        interest = interestRate.getText().toString();
        _finerate = fineRate.getText().toString();
        _description = description.getText().toString();

        if (_repayment_period.isEmpty()) {
            repayment_period.setError(getString(R.string.emptyField));
            valid = false;
        }
        if (min_score.isEmpty()) {
            minCreditScore.setError(getString(R.string.emptyField));
            valid = false;
        }
        if (_finerate.isEmpty()) {
            fineRate.setError(getString(R.string.emptyField));
            valid = false;
        }

        if (_name.isEmpty()) {
            name.setError(getString(R.string.emptyField));
            valid = false;
        }

        if (min_amount.isEmpty()) {
            minAmount.setError(getString(R.string.emptyField));
            valid = false;
        }

        if (max_amount.isEmpty()) {
            maxAmount.setError(getString(R.string.emptyField));
            valid = false;
        }

        if (interest.isEmpty()) {
            interestRate.setError(getString(R.string.emptyField));
            valid = false;
        }

        if (_description.isEmpty()) {
            description.setError(getString(R.string.emptyField));
            valid = false;
        }

        if (currency_id_toapi.equals("0")
                || insure_loan_status_id_toapi.equals("2")
                || repayment_plan.equals("0")) {
            valid = false;
            Toast.makeText(ctx, getResources().getString(R.string.fill_in_all_fields), Toast.LENGTH_LONG).show();
        }

        return valid;
    }

    private void toggleVisibility(View view) {
        if (view.getVisibility() == View.VISIBLE) {
            view.setVisibility(View.GONE);
        } else {
            view.setVisibility(View.VISIBLE);
        }
    }

    public void createPackageToAPI() {
        progressBar.setVisibility(View.VISIBLE);
        String params = "{ \"name\":" + '"' + _name + '"' +
                ", \"repayment_plan\": " + '"' + repayment_plan + '"' +
                ", \"repayment_period\": " + '"' + _repayment_period + '"' +
                ", \"min_score\": " + '"' + min_score + '"' +
                ", \"insured\": " + '"' + insure_loan_status_id_toapi + '"' +
                ", \"min_amount\": " + '"' + min_amount + '"' +
                ", \"max_amount\": " + '"' + max_amount + '"' +
                ", \"currency\": " + '"' + currency_id_toapi + '"' +
                ", \"interest\": " + '"' + interest + '"' +
                ", \"fine\": " + '"' + _finerate + '"' +
                ", \"package_visibilty\": " + '"' + item_visibility + '"' +
                ", \"description\": " + '"' + _description + "\"," +
                "\"package_source\": {"
                + "\"id\": \"" + ada_id + "\","
                + "\"method\": \"" + selected_type + "\","
                + "\"telco\": \"" + selected_provider + "\","
                + "\"msisdn\": \"" + _msisdn + "\"" +
                "}"
                + "}\n" + "\n";
        Log.e("POST_PARAMS_DATA", params);
        if (group_id.equals("0")) {
            postToServer(createPackage, token, params, INITIATOR_CREATE_LOAN_PACKAGE, HTTP_POST, ctx, this);
        } else {
            postToServer(createGroupPackage + group_id, token, params, INITIATOR_CREATE_LOAN_PACKAGE, HTTP_POST, ctx, this);
        }
    }

    private void fetchUserPaymentSoucers() {
        Call<paymentSourcesResponse> call = api.getUserPayPrefences();
        call.enqueue(new Callback<paymentSourcesResponse>() {
            @Override
            public void onResponse(Call<paymentSourcesResponse> call, Response<paymentSourcesResponse> response) {
                Log.e("TAG_payment_sources", "onResponse: " + response);
                if (response.body().getSuccess()) {
                    if (response.body().getAdapay_sources().size() > 0) {
                        List<AdapaySource> sources = response.body().getAdapay_sources();
                        ArrayList<String> availables = new ArrayList<>();
                        // availables.add(0, getResources().getString(R.string.please_select_contribution));
                        for (int item = 0; item < sources.size(); item++) {
                            availables.add(sources.get(item).getMsisdn());
                        }
                        arrayAdapter = new ArrayAdapter(ctx, android.R.layout.simple_spinner_dropdown_item, availables);
                        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        msisdn.setAdapter(arrayAdapter);
                        msisdn.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                String item_resource_type = (String) parent.getItemAtPosition(position);

                                _msisdn = sources.get(position).getMsisdn();
                                Log.e("PHONE_NUMBER", _msisdn);

                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {

                            }
                        });
                    } else {

                    }
                }


            }

            @Override
            public void onFailure(Call<paymentSourcesResponse> call, Throwable t) {

            }
        });
    }

    public void fetchAdaPaySourceType() {

        Call<adapaySourcesResponse> call = api.fetchAdapaySources();
        call.enqueue(new Callback<adapaySourcesResponse>() {
            @Override
            public void onResponse(Call<adapaySourcesResponse> call, Response<adapaySourcesResponse> response) {

                Log.e("SOLANI_DATA", "onResponse: " + response.body().getAdapay_source_types());
                String mobile = response.body().getAdapay_source_types().getMobile_money();
                String card = response.body().getAdapay_source_types().getCard_hosted();
                String bank_transfer = response.body().getAdapay_source_types().getBank_transfer();

                ada_pay_ids = new ArrayList<>();
                ada_pay_ids.add(0, getResources().getString(R.string.please_select_type));
                ada_pay_ids.add(1, mobile);
                ada_pay_ids.add(2, card);
                ada_pay_ids.add(3, bank_transfer);

                ArrayAdapter<String> adapter =
                        new ArrayAdapter<String>(ctx, android.R.layout.simple_spinner_dropdown_item, ada_pay_ids);
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                payMethod.setAdapter(adapter);

                payMethod.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                        String type = (String) parent.getItemAtPosition(position);

                        selected_type = "";
                        if (type.equals("Mobile Money")) {
                            selected_type = "mobile_money";
                            linearLayoutMobile.setVisibility(View.VISIBLE);
                            linearLayoutBank.setVisibility(View.GONE);

                        } else if (type.equals("Master/Visa Card")) {
                            selected_type = "card_hosted";
                            linearLayoutBank.setVisibility(View.GONE);
                            linearLayoutMobile.setVisibility(View.GONE);
                        } else if (type.equals("Bank Transfer")) {
                            selected_type = "bank_transfer";
                            linearLayoutBank.setVisibility(View.GONE);
                            linearLayoutMobile.setVisibility(View.GONE);
                        } else {

                            linearLayoutBank.setVisibility(View.GONE);
                            linearLayoutMobile.setVisibility(View.GONE);
                        }
                        Log.e("Selected_type", String.valueOf(type));
                        Log.e("Selected_type2", selected_type);
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }

                });


            }

            @Override
            public void onFailure(Call<adapaySourcesResponse> call, Throwable t) {

            }
        });

    }

    public void fetchAdaPayTelcos() {
        Call<telcosResponse> call = api.fetchAdapayTelcos();
        call.enqueue(new Callback<telcosResponse>() {
            @Override
            public void onResponse(Call<telcosResponse> call, Response<telcosResponse> response) {
                if (response.body().getSuccess() == true) {
                    String telcos = response.body().getAdapay_telcos().getSafaricom_ke();
                    telcos_adapay = new ArrayList<>();
                    telcos_adapay.add(0, getResources().getString(R.string.please_select_type));
                    telcos_adapay.add(1, telcos);
                    ArrayAdapter<String> adapter =
                            new ArrayAdapter<String>(ctx, android.R.layout.simple_spinner_dropdown_item, telcos_adapay);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    provider.setAdapter(adapter);
                    provider.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            String type = (String) parent.getItemAtPosition(position);
                            selected_provider = "";
                            if (type.equals("Mpesa")) {
                                llLayoutMobileMpesa.setVisibility(View.VISIBLE);
                                selected_provider = "safaricom_ke";
                            } else {
                                llLayoutMobileMpesa.setVisibility(View.GONE);
                            }
                            Log.e("Selected_telcos", String.valueOf(type));
                            Log.e("Selected_telcos2", selected_provider);
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });


                }
            }

            @Override
            public void onFailure(Call<telcosResponse> call, Throwable t) {

            }
        });
    }

    public void fetchFormDropDownData() {

        Call<lendingDataResponse> call = api.getLendingData();
        call.enqueue(new Callback<lendingDataResponse>() {
            @Override
            public void onResponse(Call<lendingDataResponse> call, Response<lendingDataResponse> response) {
                if (response.body().getSuccess()) {
                    if (response.body().getMessage().getCurrencies().size() > 0) {

                        repayment_plans = new ArrayList<>();
                        insure_statuses = new ArrayList<>();
                        currencies_ids = new ArrayList<>();
                        List<Currency> currencies = response.body().getMessage().getCurrencies();
                        ArrayList<String> currencies_drop_down = new ArrayList<String>();
                        currencies_drop_down.add(getResources().getString(R.string.please_select_currency));
                        currencies_ids.add("0");
                        if (currencies.size() > 0) {
                            for (int i = 0; i < currencies.size(); i++) {
                                String id = String.valueOf(currencies.get(i).getId());
                                Log.e("currency_Id_data", id);
                                String name = currencies.get(i).getName();
                                currencies_drop_down.add(name);
                                currencies_ids.add(id);

                            }

                            currency.setItems(currencies_drop_down);
                            currency.setSelectedIndex(0);
                        }
                    }

                    if (response.body().getMessage().getRepaymentplans().size() > 0) {

                        List<Repaymentplan> repayment_classes = response.body().getMessage().getRepaymentplans();
                        ArrayList<String> repaymentplans_drop_down = new ArrayList<String>();
                        repaymentplans_drop_down.add(getResources().getString(R.string.select_plan));
                        repayment_plans.add("0");
                        for (int i = 0; i < repayment_classes.size(); i++) {
                            String id = String.valueOf(repayment_classes.get(i).getId());
                            Log.e("Id_data", id);
                            String name = repayment_classes.get(i).getName();
                            repaymentplans_drop_down.add(name);
                            repayment_plans.add(name.toLowerCase());

                        }
                        repaymentPlan.setItems(repaymentplans_drop_down);
                        repaymentPlan.setSelectedIndex(0);
                    }

                    ArrayList<String> insured_status_drop_down = new ArrayList<String>();
                    insured_status_drop_down.add(getResources().getString(R.string.please_select_insurance));
                    insure_statuses.add("2");
                    insured_status_drop_down.add("Yes");
                    insure_statuses.add("1");
                    insured_status_drop_down.add("No");
                    insure_statuses.add("0");
                    insureLoan.setItems(insured_status_drop_down);
                    insureLoan.setSelectedIndex(0);
                }


            }

            @Override
            public void onFailure(Call<lendingDataResponse> call, Throwable t) {

            }
        });
    }


    @Override
    public void onDataReady(String initiator, final String response) {
        switch (initiator) {

            case INITIATOR_CREATE_LOAN_PACKAGE:
                try {
                    runOnUI(new Runnable() {
                        @Override
                        public void run() {
                            Log.e("CREATE_LOAN_", "Response is " + response);
                            progressBar.setVisibility(View.GONE);
                            JSONObject jObj = null;
                            try {
                                jObj = new JSONObject(response);
                                if (jObj.has("errors")) {
                                    String errors = jObj.getString("errors");
                                    showSweetAlertDialogError(errors, ctx);
                                } else { // if no error message was found in the response,
                                    boolean success = jObj.getBoolean("success");
                                    if (success) {
                                        String api_success_message = jObj.getString("message");
                                        showAlertAndMoveToPage(api_success_message, MainActivity.class, ctx);
                                    } else {
                                        String api_error_message = jObj.getString("message");
                                        showSweetAlertDialogError(api_error_message, ctx);
                                    }
                                }

                            } catch (JSONException e) {
                                Log.e("Exception-DATA", e.toString());
                                showSweetAlertDialogError(global_error_message, ctx);
                                e.printStackTrace();
                            }
                        }
                    });
                } catch (Exception e) {
                    progressBar.setVisibility(View.GONE);
                    Log.e("Exception_+CREATE_LOAN_", e.toString());
                    showSweetAlertDialogError(global_error_message, ctx);
//                    Toast.makeText(ctx, "Login Data Response Exception" + e.toString(), Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

    @Override
    public void onDataLoaded(String initiator, JSONArray data) {

    }

    @Override
    public void onNetworkError(String initiator, String errorCode, final String errorMessage) {
        switch (initiator) {

            case INITIATOR_CREATE_LOAN_PACKAGE:
                try {
                    runOnUI(new Runnable() {
                        @Override
                        public void run() {
                            progressBar.setVisibility(View.GONE);
                            showSweetAlertDialogError(errorMessage, ctx);
                        }
                    });
                } catch (Exception e) {
                    progressBar.setVisibility(View.GONE);
                    showSweetAlertDialogError(global_error_message, ctx);
                    Log.e("Exception- HOME", e.toString());
//                    Toast.makeText(ctx, "Login Error Response Exception" + e.toString(), Toast.LENGTH_LONG).show();
                }

                break;
        }
    }


    @Override
    public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
        switch (view.getId()) {
            case R.id.repaymentPlan:
                Log.d("Repayment plan id is ", repayment_plans.get(position));
                repayment_plan = repayment_plans.get(position);
                break;
            case R.id.insureLoan:
                insure_loan_status_id_toapi = insure_statuses.get(position);
                Log.d("Insure_loan_status : ", "" + insure_loan_status_id_toapi);
                break;
            case R.id.currency:
                Log.d("Currency_id_is ", currencies_ids.get(position));
                currency_id_toapi = currencies_ids.get(position);
                break;

        }
    }


}
