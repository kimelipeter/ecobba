package com.app.ecobba.Fragments.Resources

import android.content.Context
import android.content.res.Resources
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.app.ecobba.R

class ResourcesViewPagerAdapter (fm: FragmentManager,val context: Context) :
        FragmentPagerAdapter(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {
    private var childFragments = ArrayList<Fragment>()
    var resources: Resources?=null

    override fun getItem(position: Int): Fragment {

        return when(position){
            0 -> News()
            1 -> Articles()
            2-> Trainings()
            else -> News()
        }
    }

    fun setFragments(fragments:ArrayList<Fragment>){
        childFragments = fragments
    }


    override fun getCount(): Int {
        return 3
        //Log.e("Viewpager_size", childFragments.toString())
    }

    override fun getPageTitle(position: Int): CharSequence? {

        var title: String?=null
        if (position == 0) {
            title= context.resources.getString(R.string.news)
        } else if (position == 1) {
            title=context.resources.getString(R.string.articles)
        }
        else if (position == 2) {
            title=context.resources.getString(R.string.trainings)
        }
        return title
    }

    fun String.capitalizeWords(): String=split(" ").joinToString(" ") { it.capitalize() }
}