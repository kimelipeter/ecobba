
package com.app.ecobba.Fragments.Profile.UserProfile;

import java.util.List;
import lombok.Data;

@Data
@SuppressWarnings("unused")
public class User {

    private String accountNo;
    private long accountVerified;
    private String avatar;
    private String createdAt;
    private long creditScore;
    private String customerUsername;
    private String deletedAt;
    private Detail detail;
    private String email;
    private String emailVerifiedAt;
    private long id;
    private String identificationDocument;
    private String msisdn;
    private String name;
    private Organization organization;
    private String otherNames;
    private List<Role> roles;
    private long status;
    private String token;
    private Transactions transactions;
    private String updatedAt;
    private long verified;
    private Wallet wallet;

}
