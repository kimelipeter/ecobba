package com.app.ecobba.Fragments.Wallet;

public class SetWalletPrerence {
    private String group_id;
    private String type;
    private  String telco;
    private  String msisdn;


    public SetWalletPrerence(String group_id, String type, String telco, String msisdn) {
        this.group_id = group_id;
        this.type = type;
        this.telco = telco;
        this.msisdn = msisdn;
    }

    public String getGroup_id() {
        return group_id;
    }

    public void setGroup_id(String group_id) {
        this.group_id = group_id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTelco() {
        return telco;
    }

    public void setTelco(String telco) {
        this.telco = telco;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }
}
