package com.app.ecobba.Fragments.Profile.MyProfile

data class UserDataprofile(
    val message: String,
    val success: Boolean,
    val user: User
)