package com.app.ecobba.Fragments.Borrowing.Borrowed.ApiResponse

data class Detail(
    val amount_payable: Float,
    val balance: Float,
    val charge_per_installment: Float ,
    val created_at: String,
    val fines_balance: Int,
    val fines_paid: Int,
    val fines_per_period: Int,
    val id: Int,
    val interest_balance: Int,
    val interest_charge_frequency: String,
    val interest_due: Float,
    val interest_paid: Int,
    val loan_id: Int,
    val next_payment_date: String,
    val no_of_installments: String,
    val package_name: String,
    val payback_date: String,
    val payment_dates: Any,
    val penalty_due: Int,
    val principal_balance: Int,
    val principal_due: Int,
    val principal_paid: Int,
    val purpose: Any,
    val total_fines_payable: Int,
    val updated_at: String
)