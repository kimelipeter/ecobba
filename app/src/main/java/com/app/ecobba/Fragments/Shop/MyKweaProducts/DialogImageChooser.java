package com.app.ecobba.Fragments.Shop.MyKweaProducts;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.core.app.ActivityCompat;
import androidx.core.content.FileProvider;

import com.app.ecobba.BuildConfig;
import com.app.ecobba.R;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;



public class DialogImageChooser extends BottomSheetDialogFragment {

DialogImageChooser.GetImage getImage;
    public DialogImageChooser(DialogImageChooser.GetImage getImage, boolean allowMultiple) {
        this.getImage = getImage;
    }
    File cameraImage;

    public static String ImageFilePath;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.image_choose_bottom_sheet, container, false);
        view.findViewById(R.id.tvClickImageText).setOnClickListener(new View.OnClickListener() {@
                Override
        public void onClick(View view) {
            if(ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[] {
                        Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE
                }, 2000);
            } else {
                captureFromCamera();
            }
        }
        });
        view.findViewById(R.id.tvChooseGalleryText).setOnClickListener(new View.OnClickListener() {@
                Override
        public void onClick(View view) {
            if(ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[] {
                        Manifest.permission.READ_EXTERNAL_STORAGE
                }, 2000);
            } else {
                startGallery();
            }
        }
        });
        return view;
    }
    public interface GetImage {
        void setGalleryImage(Uri imageUri);
        void setCameraImage(String filePath);
        void setImageFile(File file);
       String ffff= ImageFilePath;
    }@
            Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == Activity.RESULT_OK) {
            if(requestCode == 1000) {
                Uri returnUri = data.getData();
                getImage.setGalleryImage(returnUri);
                File file = new File(returnUri.getPath());//create path from uri
                String[] proj = { MediaStore.Images.Media.DATA };
                Cursor cursor = getContext().getContentResolver().query(returnUri, proj, null, null, null);
                int column_index
                        = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                cursor.moveToFirst();
                 ImageFilePath=  cursor.getString(column_index);
                //final String[] split = file.getPath().split(":");//split the path.
//               String filePath = split[1];//assign it to a string(your choice).
                Log.e("GALLERY_IMAGE_PATH",ImageFilePath);
                Bitmap bitmapImage = null;
            }
            if(requestCode == 1002) {
                if(cameraImage != null) {
                    getImage.setImageFile(cameraImage);
                }
                getImage.setCameraImage(cameraFilePath);
            }
        }
    }
    private void startGallery() {
        Intent cameraIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        cameraIntent.setType("image/*");
        if(cameraIntent.resolveActivity(getActivity().getPackageManager()) != null) {
            startActivityForResult(cameraIntent, 1000);
        }
    }

    private String cameraFilePath;
    private File createImageFile() throws IOException {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM), "Camera");
        File image = File.createTempFile(imageFileName, /* prefix */ ".jpg", /* suffix */ storageDir /* directory */ );
        ImageFilePath = "file://" + image.getAbsolutePath();
        Log.e("PUPLIC_IMAGE",ImageFilePath);
        cameraImage = image;
        return image;
    }
    private void captureFromCamera() {
        try {
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, FileProvider.getUriForFile(getContext(), BuildConfig.APPLICATION_ID + ".provider", createImageFile()));
            startActivityForResult(intent, 1002);
        } catch(IOException ex) {
            ex.printStackTrace();
        }
    }
}
