package com.app.ecobba.Fragments.Borrowing.Borrowed;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import com.app.ecobba.Models.UserLoans.Loan;

import com.app.ecobba.Models.UserLoans.userLoansResponse;
import com.app.ecobba.Models.UserProfile.Detail;
import com.app.ecobba.R;
import com.app.ecobba.Utils.BaseFragment;
import com.app.ecobba.ViewModel.UserProfileViewModel;
import com.app.ecobba.common.SharedMethods;
import com.app.ecobba.common.rxApi.Api;
import com.app.ecobba.common.rxApi.RxApi;
import com.app.ecobba.recyclerview_adapters.MyLoansRecycler;
import com.google.gson.Gson;

import org.json.JSONArray;


import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class LoansFragment extends BaseFragment implements View.OnClickListener {

    //    @BindView(R.id.rvLoans)
    RecyclerView rvLoans;

    @BindView(R.id.tvApproved)
    TextView tvApproved;

    @BindView(R.id.tvPending)
    TextView tvPending;

    @BindView(R.id.tvTotal)
    TextView tvTotal;



    String token;
    @BindView(R.id.tvNoAvailableLoans)
    TextView tvNoAvailableLoans;
    Fragment fragment = this;
    Context context;
    private Api api;
    MyLoansRecycler adapter;
    @BindView(R.id.progress_dialog)
    ProgressBar progress_dialog;
    UserProfileViewModel userProfileViewModel;
    String user_currency;

    public LoansFragment() {
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_borrowed_loans, container, false);
        ButterKnife.bind(this, view);
        context = getActivity();
        userProfileViewModel = new ViewModelProvider(getActivity()).get(UserProfileViewModel.class);
        token = SharedMethods.getDefaults("token", context);
        api = new RxApi().getApi();
        rvLoans = view.findViewById(R.id.rvLoans);
        rvLoans.addItemDecoration(new DividerItemDecoration(getContext(),
                DividerItemDecoration.VERTICAL));
        adapter = new MyLoansRecycler(new ArrayList<>(), requireContext(), fragment, "");
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(requireContext());
        rvLoans.setLayoutManager(layoutManager);
        rvLoans.setAdapter(adapter);
        api = new RxApi().getApi();
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        context = getActivity();
        context = getActivity();
        fetchLoans();
        fetchUserProfile();

    }

    private void fetchLoans() {
        progress_dialog.setVisibility(View.VISIBLE);
        Call<userLoansResponse> call = api.getUserLoans();
        call.enqueue(new Callback<userLoansResponse>() {
            @Override
            public void onResponse(Call<userLoansResponse> call, Response<userLoansResponse> response) {
                if (response.body().getSuccess()) {
                    Log.e("USER_LOANS_RESPONSE", "onResponse: " + response);
                    progress_dialog.setVisibility(View.GONE);
                    if (response.body().getLoans().size() > 0) {
                        adapter.datalist = response.body().getLoans();
                        adapter.notifyDataSetChanged();

//                        int approved = 0, pending = 0;
//                        if (response.body().getLoans().get().getStatus() == 0) {
//                            pending++;
//                            tvPending.setText(String.valueOf(pending));
//                        } else if (response.body().getLoans().get().getStatus() == 1) {
//                            approved++;
//                            tvApproved.setText(String.valueOf(approved));
//
//                        }
//

                    } else {
                        tvNoAvailableLoans.setText(getResources().getString(R.string.no_available_loans_borrowed));
                        tvNoAvailableLoans.setVisibility(View.VISIBLE);
                    }
                }
            }

            @Override
            public void onFailure(Call<userLoansResponse> call, Throwable t) {
                t.getLocalizedMessage();
                Log.e("ERROR_LOANS", t.getLocalizedMessage());
            }
        });
    }

    private void fetchUserProfile() {
        userProfileViewModel.getUserProfile();
        userProfileViewModel.userProfileResponseLiveData().observe(getViewLifecycleOwner(), userProfileResponse -> {
            try {
                if (userProfileResponse.getSuccess()) {
                    Detail userDetails = userProfileResponse.getUser().getDetail();
                    String name = userProfileResponse.getUser().getName();

                    user_currency = userProfileResponse.getUser().getWallet().getCurrency().getPrefix();
                    Log.e("MUDHDGDGD", user_currency);
//                    currencyValue = String.valueOf(userProfileResponse.getUser().getDetail().getCountry());
                    user_currency = userProfileResponse.getUser().getWallet().getCurrency().getPrefix();
                    adapter.user_currency = user_currency;
                    adapter.notifyDataSetChanged();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    @Override
    public void onClick(View view) {

    }

}
