package com.app.ecobba.Fragments;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.Log;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;

import com.app.ecobba.Models.UserProfile.Detail;
import com.app.ecobba.ViewModel.DashBoardViewModel;
import com.app.ecobba.ViewModel.UserProfileViewModel;
import com.budiyev.android.circularprogressbar.CircularProgressBar;
import com.app.ecobba.R;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.jaredrummler.materialspinner.MaterialSpinner;
import com.mikhaellopez.circularimageview.CircularImageView;

import java.lang.reflect.Field;
import java.text.DecimalFormat;


import butterknife.BindView;
import butterknife.ButterKnife;

import static com.app.ecobba.common.SharedMethods.showSweetAlertDialogError;

public class AccountFragment extends Fragment implements
        View.OnClickListener,
        MaterialSpinner.OnItemSelectedListener {

    //    @BindView(R.id.language)
//    MaterialSpinner language;
    Context ctx;

    @BindView(R.id.tvUserbalance)
    TextView tvUserbalance;
    @BindView(R.id.userNames)
    TextView userNames;
    @BindView(R.id.tv_email_address)
    TextView tv_email_address;
    @BindView(R.id.tv_account_number)
    TextView tv_account_number;
    @BindView(R.id.myGroups)
    TextView myGroups;
    @BindView(R.id.groupsJoined)
    TextView groupsJoined;
    @BindView(R.id.loansBorrowed)
    TextView loansBorrowed;
    @BindView(R.id.loanPackages)
    TextView loanPackages;
    @BindView(R.id.btEdit)
    Button btEdit;
    //    @BindView(R.id.survey) Button survey;
    @BindView(R.id.cpbProfile)
    CircularProgressBar cpbProfile;

    TextView tvMyGroups;
    @BindView(R.id.ll_wallet)
    LinearLayout ll_wallet;
    @BindView(R.id.ll_MyGroups)
    LinearLayout ll_MyGroups;
    @BindView(R.id.ll_MyLoanPackages)
    LinearLayout ll_MyLoanPackages;
    @BindView(R.id.ll_LoansBorrowed)
    LinearLayout ll_LoansBorrowed;
    @BindView(R.id.ll_language)
    LinearLayout ll_language;
    UserProfileViewModel userProfileViewModel;
    CircularImageView user_prof;

    DashBoardViewModel dashBoardViewModel;


    public AccountFragment() {

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View frag = inflater.inflate(R.layout.fragment_account_profile, container, false);
        ButterKnife.bind(this, frag);
        btEdit.setOnClickListener(this);
        ll_wallet.setOnClickListener(this);
        ll_MyGroups.setOnClickListener(this);
        ll_MyLoanPackages.setOnClickListener(this);
        ll_LoansBorrowed.setOnClickListener(this);
        user_prof = frag.findViewById(R.id.user_prof);
        //survey.setOnClickListener(this);
        tvMyGroups = getActivity().findViewById(R.id.tvCreditScore);
//        language.setItems(getResources().getStringArray(R.array.languages));
        userProfileViewModel = new ViewModelProvider(getActivity()).get(UserProfileViewModel.class);
        dashBoardViewModel = new ViewModelProvider(this).get(DashBoardViewModel.class);
        return frag;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ctx = getActivity();
        fetchUsersData();
        fetchUsersSummary();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btEdit:
                NavHostFragment.findNavController(this).navigate(R.id.editProfileFragment);
                break;

            case R.id.ll_wallet:
                NavHostFragment.findNavController(this).navigate(R.id.walletFragment);
                break;
            case R.id.ll_MyGroups:
                NavHostFragment.findNavController(this).navigate(R.id.groupsFragment);
                break;
            case R.id.ll_MyLoanPackages:
                NavHostFragment.findNavController(this).navigate(R.id.loans_lending_dashboard);
                break;

            case R.id.ll_LoansBorrowed:
                NavHostFragment.findNavController(this).navigate(R.id.loansFragment);
                break;
        }
    }

    @Override
    public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
        Context context;
        Resources resources;
        if (position == 1) {

        }
    }


    public void fetchUsersData() {
        userProfileViewModel.getUserProfile();
        userProfileViewModel.userProfileResponseLiveData().observe(getViewLifecycleOwner(), userProfileResponse -> {

           try {
               if (userProfileResponse.getSuccess()) {
                   Detail userDetails = userProfileResponse.getUser().getDetail();
                   String imageUri = userProfileResponse.getUser().getAvatar();
                   Log.e("USER_AVATAR", imageUri);
                   Glide.with(requireContext()).load(imageUri).centerCrop().into(user_prof);
                   String currency=userProfileResponse.getUser().getWallet().getCurrency().getPrefix();
                   int user_balance = userProfileResponse.getUser().getWallet().getBalance();
                   int credit_score = userProfileResponse.getUser().getCredit_score();
                   String account_number = String.valueOf(userProfileResponse.getUser().getAccount_no());
                   String email_address = userProfileResponse.getUser().getEmail();
                   String name = userProfileResponse.getUser().getName();
                   String other_names = userProfileResponse.getUser().getOther_names();
                   tvUserbalance.setText(currency+" " +new DecimalFormat("0.00").format(user_balance));
                   String full_name = name + " " + other_names;
                   userNames.setText(full_name);
                   tv_email_address.setText(email_address);
                   if (!account_number.equals("null")) {
                       tv_account_number.setText(account_number);
                   }

                   TextView tvCreditScore = getActivity().findViewById(R.id.tvCreditScore);
                   if (credit_score < 5) {
                       tvCreditScore.setTextColor(getResources().getColor(R.color.red));
                   } else if (credit_score > 5 && credit_score < 7.5) {
                       tvCreditScore.setTextColor(getResources().getColor(R.color.orange));
                   } else {
                       tvCreditScore.setTextColor(getResources().getColor(R.color.green));
                   }
                   tvCreditScore.setText(new DecimalFormat("0.00").format(credit_score));
                   Pair<Integer, Integer> profile = showProfileProgress(userDetails);

                   int max = profile.second;
                   int progress = profile.first;
                   float perc = ((float) progress / (float) max) * 100;
                   int percentage = (int) perc;
                   Log.e("PROGRESS_profile", percentage + "-" + progress + "/" + max);
                   cpbProfile.setMaximum(max);
                   cpbProfile.setProgress(progress);
                   if (progress > 90) {
                       getActivity().findViewById(R.id.cvProfile).setVisibility(View.GONE);
                   } else {

                   }


               } else {

                   showSweetAlertDialogError("Something went Wrong", ctx);
               }
           } catch (Exception e) {
               e.printStackTrace();
           }
        });
    }

    public void fetchUsersSummary() {
        dashBoardViewModel.getUserSummaryResponseMutableLiveData().observe(getViewLifecycleOwner(), userSummaryResponse -> {
            try {
                if (userSummaryResponse.getSuccess()) {
                    Log.d("DDDD", "onResponse: DDDD: " + userSummaryResponse.getLoans().getMygroups());
                    String mygroups = String.valueOf(userSummaryResponse.getLoans().getMygroups());
                    String groupsjoined = String.valueOf(userSummaryResponse.getLoans().getGroupsjoined());
                    String mypackages = String.valueOf(userSummaryResponse.getLoans().getMypackages());
                    String myloans = String.valueOf(userSummaryResponse.getLoans().getMyloans());
                    myGroups.setText(mygroups);
                    groupsJoined.setText(groupsjoined);
                    loansBorrowed.setText(myloans);
                    loanPackages.setText(mypackages);

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

        dashBoardViewModel.getDashBoard();
    }


    @SuppressLint("LongLogTag")
    public Pair<Integer, Integer> showProfileProgress(Detail userDetails) {
        Log.e("PROFILE_PROGRESS", new Gson().toJson(userDetails));
        Field[] fields = userDetails.getClass().getDeclaredFields();
        int total = fields.length - 1;
        int filled = 0;
        for (Field field : fields) {
            field.setAccessible(true);
            try {
                String val = String.valueOf(field.get(userDetails));
                if (val.equals("0")) {
                    Log.e("PROFILE_PROGRESS->0", val);
                } else if (val.equals("null")) {
                    Log.e("PROFILE_PROGRESS->null", val);
                } else if (!val.isEmpty()) {
                    Log.e("PROFILE_PROGRESS->notempty", val);
                    filled++;
                }
            } catch (IllegalAccessException e) {
                e.printStackTrace();
                Log.e("PROFILE_PROGRESS", e.toString());
            }
        }

        float val = filled / total;
        Log.e("PROFILE_PROGRESS", val + "-" + filled + "/" + total);
        return new Pair<Integer, Integer>(filled, total);
    }
}
