
package com.app.ecobba.Fragments.Profile.UserProfile;

import lombok.Data;

@Data
@SuppressWarnings("unused")
public class Pivot {

    private long roleId;
    private long userId;
    private String userType;

}
