package com.app.ecobba.Fragments.Group.Loans;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;
import com.app.ecobba.R;
import com.google.android.material.tabs.TabLayout;


import java.util.ArrayList;

public class GroupLoansDashBoard extends Fragment {
    String group_id,group_name;
    TextView textViewGroupName;

    public GroupLoansDashBoard() {
    }
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_group_loans_dashboard,container,false);
//        textViewGroupName=view.findViewById(R.id.tvName);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        SharedPreferences sh = getContext().getSharedPreferences("PREFERENCE", Context.MODE_PRIVATE);
        group_id = sh.getString("group_id", "");
        group_name = sh.getString("group_name", "");

//        textViewGroupName.setText(group_name);
        GroupLoansViewPager adapter = new GroupLoansViewPager(getChildFragmentManager(),getContext());
        ArrayList<Fragment> fragments = new ArrayList<>();
        fragments.add(new GroupLoansFragment());
        fragments.add(new GroupLoansBorrowedFragment());
        adapter.setFragments(fragments);
        ViewPager viewPager = view.findViewById(R.id.pager2);
        viewPager.setAdapter(adapter);
        TabLayout tabLayout = view.findViewById(R.id.tablayout2);
        tabLayout.setupWithViewPager(viewPager);
    }
}
