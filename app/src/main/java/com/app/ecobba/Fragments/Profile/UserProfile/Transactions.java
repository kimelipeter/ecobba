
package com.app.ecobba.Fragments.Profile.UserProfile;

import lombok.Data;

@Data
@SuppressWarnings("unused")
public class Transactions {

    private long credit;
    private long debit;

}
