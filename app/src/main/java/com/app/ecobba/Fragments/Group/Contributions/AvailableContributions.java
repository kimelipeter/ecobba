package com.app.ecobba.Fragments.Group.Contributions;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.ecobba.Dialogs.DepositRequestDialog;
import com.app.ecobba.Fragments.Group.Contributions.Adapters.AvailableContributionsAdapter;
import com.app.ecobba.Models.payment.contributionResponse;
import com.app.ecobba.R;
import com.app.ecobba.Utils.BaseFragment;
import com.app.ecobba.ViewModel.UserProfileViewModel;
import com.app.ecobba.common.rxApi.Api;
import com.app.ecobba.common.rxApi.RxApi;

import com.github.ybq.android.spinkit.SpinKitView;
import com.github.ybq.android.spinkit.style.Circle;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;


import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AvailableContributions extends BaseFragment implements View.OnClickListener {
    Context context;
    private RecyclerView recyclerView;
    Bundle data;
    private Api api;
    Fragment fragment = this;

    AvailableContributionsAdapter adapter;
    String group_id,name,other_name,full_name,msisdn,user_currency;
    @BindView(R.id.tvNoAvailableData)
    TextView tvNoAvailableData;
    @BindView(R.id.create)
    FloatingActionButton create;
    SpinKitView spinKitView;
    UserProfileViewModel userProfileViewModel;


    public AvailableContributions() {
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_available_contributions, container, false);
        ButterKnife.bind(this, view);
        context = getActivity();
        userProfileViewModel = new ViewModelProvider(getActivity()).get(UserProfileViewModel.class);
        spinKitView = (SpinKitView) view.findViewById(R.id.spin_kit);
        Circle circle = new Circle();
        spinKitView.setIndeterminateDrawable(circle);
        recyclerView = view.findViewById(R.id.recyclerView);
        recyclerView.addItemDecoration(new DividerItemDecoration(getContext(),
                DividerItemDecoration.VERTICAL));
        adapter = new AvailableContributionsAdapter(new ArrayList<>(), requireContext(), fragment,"");
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(requireContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
        api = new RxApi().getApi();
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        context = getActivity();
        SharedPreferences sh = getContext().getSharedPreferences("PREFERENCE", Context.MODE_PRIVATE);
        group_id = sh.getString("group_id", "");
        Log.e("GROUP_ID_VAL", group_id);
        fetchAvailableContributions(group_id);
        fetchUsersData();
        create.setOnClickListener(this);
    }
    private void fetchAvailableContributions(String group_id) {
        spinKitView.setVisibility(View.VISIBLE);
        Call<contributionResponse> call = api.getAvailableContributions(group_id);
        call.enqueue(new Callback<contributionResponse>() {
            @Override
            public void onResponse(Call<contributionResponse> call, Response<contributionResponse> response) {
               try {
                   if (response.body().getSuccess()) {
                       spinKitView.setVisibility(View.GONE);
                       if (response.body().getData().getContributions().getAVAILABLE_CONTRIBUTIONS().size() > 0) {
                           adapter.datalist = response.body().getData().getContributions().getAVAILABLE_CONTRIBUTIONS();
                           adapter.notifyDataSetChanged();
                       }
                       else {
                           spinKitView.setVisibility(View.GONE);
                           tvNoAvailableData.setText(getResources().getString(R.string.no_contribution_available));
                           tvNoAvailableData.setVisibility(View.VISIBLE);
                       }
                   }
               } catch (Exception e) {
                   e.printStackTrace();
               }
            }

            @Override
            public void onFailure(Call<contributionResponse> call, Throwable t) {

            }
        });

    }
    private void fetchUsersData() {
        userProfileViewModel.getUserProfile();
        userProfileViewModel.userProfileResponseLiveData().observe(getViewLifecycleOwner(), userProfileResponse -> {
            try {
                if (userProfileResponse.getSuccess()) {
                    name = userProfileResponse.getUser().getName();
                    other_name=userProfileResponse.getUser().getOther_names();
                    msisdn=userProfileResponse.getUser().getMsisdn();
                    full_name=name+" "+other_name;
                    Log.e("user_details",full_name);
                    user_currency=userProfileResponse.getUser().getWallet().getCurrency().getPrefix();
                    adapter.user_currency = user_currency;
                    adapter.notifyDataSetChanged();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }




    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.create:

                Bundle arg = new Bundle();
                arg.putString("full_name", full_name );
                arg.putString("group_id",group_id);
                arg.putString("msisdn",msisdn);
                MakeContributionDialog makeContributionDialog = new MakeContributionDialog(full_name,group_id,msisdn);
                makeContributionDialog.show(getActivity().getSupportFragmentManager(),"DEPOSIT_TAG");
        }
    }

}
