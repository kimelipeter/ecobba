package com.app.ecobba.Fragments.chat

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.NavHostFragment
import androidx.recyclerview.widget.RecyclerView
import com.app.ecobba.Fragments.chat.data.Participant
import com.app.ecobba.R
import com.app.ecobba.Utils.BaseFragment
import com.app.ecobba.recyclerview_adapters.GenericAdapter
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.fragment_chat_contact.*
import kotlinx.android.synthetic.main.list_item_chat_participants.view.*

class ChatContactsFragment : BaseFragment(){

    val chatViewModel : ChatViewModel by lazy { ViewModelProviders.of(requireActivity()).get(ChatViewModel::class.java) }
    inner class ViewModel(override val containerView: View):RecyclerView.ViewHolder(containerView),LayoutContainer
    lateinit var  adapter : GenericAdapter<Participant,ViewModel>
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_chat_contact,container,false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        toolbar?.setNavigationOnClickListener {
            requireActivity().onBackPressed()
        }

        adapter = GenericAdapter(object : GenericAdapter.AdapterInterface<Participant,ViewModel>{
            override fun createViewHolder(parent: ViewGroup, viewType: Int): ViewModel {
                return ViewModel(LayoutInflater.from(parent.context).inflate(R.layout.list_item_chat_participants,parent,false))
            }

            override fun bindViewHolder(viewholder: ViewModel?, datum: Participant?, position: Int) {
                viewholder?.containerView?.run {
                    tvName.text = datum?.name
                    var owner_name: String? = datum?.name
                    textViewMessage.text = datum?.lastMessage
                    Log.e("Ownername_is ", owner_name.toString());
                    llContact.setOnClickListener {
                        NavHostFragment.findNavController(this@ChatContactsFragment).navigate(R.id.chatMessagesFragment,Bundle().let {
                            it.putInt("OTHER_PARTY",datum?.id ?: 0)
                            it.putString("owner_name",owner_name)
                            it
                        })
                    }
                }
            }

        }, rvContacts)

        chatViewModel.chatContacts.observe(viewLifecycleOwner, Observer {
            adapter.setData(it.toList())
        })

        tiSearch.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun afterTextChanged(p0: Editable?) {
                if(p0.toString().isNullOrBlank()){
                    adapter.setData(chatViewModel.chatContacts.value?.toList())
                }else{
                    adapter.setData(chatViewModel.chatContacts.value?.filter { it.name.toLowerCase().startsWith(p0.toString().toLowerCase()) }?.toList())
                }
            }
        })


    }
}