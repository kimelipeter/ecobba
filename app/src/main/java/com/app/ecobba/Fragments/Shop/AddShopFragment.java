package com.app.ecobba.Fragments.Shop;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.fragment.NavHostFragment;

import com.app.ecobba.Activities.MainActivity;

import com.app.ecobba.Models.User;
import com.app.ecobba.Models.UserDetails;
import com.app.ecobba.Models.UserProfile.UserProfileResponse;
import com.app.ecobba.Models.shopsdashboard.shopsDataResponse;
import com.app.ecobba.R;
import com.app.ecobba.common.SharedMethods;
import com.app.ecobba.common.rxApi.Api;
import com.app.ecobba.common.rxApi.RxApi;
import com.google.gson.Gson;
import com.jaredrummler.materialspinner.MaterialSpinner;
import com.rilixtech.widget.countrycodepicker.Country;
import com.rilixtech.widget.countrycodepicker.CountryCodePicker;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.app.ecobba.common.SharedMethods.showSweetAlertDialogSuccess;

public class AddShopFragment extends Fragment implements
        View.OnClickListener {

    private Context ctx;
    private String _shop_name;
    private String _phone_number;
    private String _id_location;
    private String _email_account;
    public String email_address, user_phone_number;
    NavController navController;

    private EditText email_account, shop_name, phone_number, id_location;
    ColorDrawable popupColor;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.create)
    Button create;
    CountryCodePicker ccp;
    private Api api;
    public ProgressDialog progressDialog;

    public AddShopFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View frag = inflater.inflate(R.layout.fragment_add_shop, container, false);
        ButterKnife.bind(this, frag);
        popupColor = new ColorDrawable(getResources().getColor(R.color.colorSurface));
        api = new RxApi().getApi();
        return frag;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ctx = getActivity();
        create.setOnClickListener(this);
        email_account = view.findViewById(R.id.email_account);
        email_account.setText(email_address);
        shop_name = view.findViewById(R.id.shop_name);
        phone_number = view.findViewById(R.id.phone_number);
        phone_number.setText(user_phone_number);
        id_location = view.findViewById(R.id.shop_location);
        fetchUsersData();
        ccp = (CountryCodePicker) view.findViewById(R.id.ccp);
        ccp.setOnCountryChangeListener(new CountryCodePicker.OnCountryChangeListener() {
            @Override
            public void onCountrySelected(Country selectedCountry) {
                Toast.makeText(getContext(), "Updated country to " + ccp.getSelectedCountryName(), Toast.LENGTH_SHORT).show();
                String code = ccp.getSelectedCountryCode();
                Log.e("Code_country", code);
            }
        });

    }

    private void fetchUsersData() {
        Call<UserProfileResponse> call = api.getUserProfile();
        call.enqueue(new Callback<UserProfileResponse>() {
            @Override
            public void onResponse(Call<UserProfileResponse> call, Response<UserProfileResponse> response) {
                if (response.body().getSuccess()) {
                    user_phone_number = response.body().getUser().getMsisdn();
                    Log.e("user_phone_number", user_phone_number);
                    phone_number.setText(user_phone_number);
                    email_address = response.body().getUser().getEmail();
                    Log.e("user_mail", email_address);

                    email_account.setText(email_address);

                }
            }

            @Override
            public void onFailure(Call<UserProfileResponse> call, Throwable t) {

            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.create:
                if (validateFormFields()) {
                    createShopToAPI();
                }
                break;
        }
    }

    private boolean validEmail(String _email_account) {
        Pattern pattern = Patterns.EMAIL_ADDRESS;
        return pattern.matcher(_email_account).matches();
    }

    private boolean validateFormFields() {
        boolean valid = true;
        _email_account = email_account.getText().toString();
        _shop_name = shop_name.getText().toString();
        _phone_number = phone_number.getText().toString();
        _id_location = id_location.getText().toString();

        if (_email_account.isEmpty() || !validEmail(_email_account)) {
            email_account.setError(getString(R.string.entervalidemail));
            Toast.makeText(getContext(), "Enter valid e-mail!", Toast.LENGTH_LONG).show();
            valid = false;
        }

        if (_shop_name.isEmpty()) {
            shop_name.setError(getString(R.string.emptyField));
            valid = false;
        }

        if (_phone_number.isEmpty()) {
            phone_number.setError(getString(R.string.emptyField));
            valid = false;
        }
        if (_id_location.isEmpty()) {
            id_location.setError(getString(R.string.emptyField));
            valid = false;
        }
        return valid;
    }

    private void createShopToAPI() {
        this.progressDialog = ProgressDialog.show(getContext(), null, getString(R.string.progress_login));
        CreateShopKwea createShopKwea = new CreateShopKwea(_shop_name, _email_account, _phone_number, _id_location);
        Call<ResponseBody> call = api.CreateShop(createShopKwea);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                // show Success
                progressDialog.dismiss();
                String api_message = _shop_name + " " + "has been created successfully";
                showSweetAlertDialogSuccess(api_message, ctx);
                NavHostFragment.findNavController(AddShopFragment.this).navigate(R.id.shopDashboardFragment);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e("GAFAFAFAFAFAF", t.getLocalizedMessage());

            }
        });

    }

}
