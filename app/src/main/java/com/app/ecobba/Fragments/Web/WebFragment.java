package com.app.ecobba.Fragments.Web;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.app.ecobba.R;
import com.app.ecobba.common.SharedMethods;

import org.json.JSONArray;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.app.ecobba.common.ApisKtKt.getUserReports;
import static com.app.ecobba.common.AppConstantsKt.INITIATOR_REPORTS;
import static com.app.ecobba.common.SharedMethods.HTTP_GET;
import static com.app.ecobba.common.SharedMethods.global_error_message;
import static com.app.ecobba.common.SharedMethods.runOnUI;
import static com.app.ecobba.common.SharedMethods.showSweetAlertDialogError;

public class WebFragment extends Fragment implements SharedMethods.NetworkObjectListener{

    @BindView(R.id.wvWeb)
    WebView wvWeb;
    @BindView(R.id.flComingSoon)
    FrameLayout comingSoon;
    String token;

    String url = getUserReports;
    Boolean show = false;
    Bundle args;
    Context ctx;
    Map<String, String> params;

    public WebFragment(){

    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View frag = inflater.inflate(R.layout.fragment_webview,container,false);
        ButterKnife.bind(this,frag);
        ctx = getContext();
        args = getArguments();
        token= SharedMethods.getDefaults("token",ctx);
        params = new HashMap<>();
        params.put("token" , token);
        if (args!=null) {
            url = args.getString("url", url);
            show = args.getBoolean("show", false);
        }
        Log.e("WEB",url);
        if (!show){
            comingSoon.setVisibility(View.VISIBLE);
            wvWeb.setVisibility(View.GONE);
        }else{
            comingSoon.setVisibility(View.GONE);
            wvWeb.setVisibility(View.VISIBLE);
        }
        getData();
        return frag;
    }

    private void getData(){
        SharedMethods.getFromServer(url,token,params,INITIATOR_REPORTS,HTTP_GET,ctx,this);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        wvWeb.getSettings().setJavaScriptEnabled(true);
        wvWeb.getSettings().setJavaScriptCanOpenWindowsAutomatically(false);
        wvWeb.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView view, String request) {
                view.loadUrl(request,params);
                return false;
            }

        });
    }

    @Override
    public void onDataReady(String initiator, String response) {
        switch (initiator) {
            case INITIATOR_REPORTS:
                Log.e(INITIATOR_REPORTS,response);
                runOnUI(()->{
                    if (response==null){
                        comingSoon.setVisibility(View.VISIBLE);
                        wvWeb.setVisibility(View.GONE);
                    }else {
                        loadPage(response);
                    }
                });

                break;
        }
    }

    @Override
    public void onDataLoaded(String initiator, JSONArray data) {

    }

    @Override
    public void onNetworkError(String initiator, String errorCode, String errorMessage) {
        switch (initiator) {
            case INITIATOR_REPORTS:
                try {
                    runOnUI(() -> {
//                    hideDialog(ctx);
                        showSweetAlertDialogError(errorMessage, ctx);
                    });
                } catch (Exception e) {
//                  hideDialog(ctx);
                    showSweetAlertDialogError(global_error_message, ctx);
                    Log.e("Exception- HOME", e.toString());
//                  Toast.makeText(ctx, "Login Error Response Exception" + e.toString(), Toast.LENGTH_LONG).show();
                }

                break;
        }
    }

    private void loadPage(String data){
       wvWeb.loadDataWithBaseURL(url,data,null, "UTF-8",null);
    }
}
