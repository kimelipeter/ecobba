package com.app.ecobba.Fragments.Group.Settings.Adapters;

import static com.app.ecobba.common.ApisKtKt.disableSetting;
import static com.app.ecobba.common.ApisKtKt.enableSetting;
import static com.app.ecobba.common.AppConstantsKt.INITIATOR_DISABLE_SETTING;
import static com.app.ecobba.common.AppConstantsKt.INITIATOR_ENABLE_SETTING;
import static com.app.ecobba.common.SharedMethods.HTTP_POST;
import static com.app.ecobba.common.SharedMethods.global_error_message;
import static com.app.ecobba.common.SharedMethods.postToServer;
import static com.app.ecobba.common.SharedMethods.runOnUI;
import static com.app.ecobba.common.SharedMethods.showSweetAlertDialogError;
import static com.app.ecobba.common.SharedMethods.showSweetAlertDialogSuccess;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.core.graphics.drawable.DrawableCompat;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.RecyclerView;

import com.app.ecobba.Models.Group_Settings.Setting;
import com.app.ecobba.R;
import com.app.ecobba.common.SharedMethods;
import com.app.ecobba.recyclerview_adapters.GroupsSettingsListRecycler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GroupSettingsAdapter extends RecyclerView.Adapter<GroupSettingsAdapter.ViewHolder> implements SharedMethods.NetworkObjectListener {

    public List<Setting> datalist;
    public Context context;
    public Fragment fragment;
    public String group_id;
    public String token;
    NavController navigate;

    int groupID=180;

    public GroupSettingsAdapter(List<Setting> datalist, Context context, Fragment fragment, String group_id, String token) {
        this.datalist = datalist;
        this.context = context;
        this.fragment = fragment;
        this.group_id = group_id;
        this.token=token;
        navigate = NavHostFragment.findNavController(fragment);

    }

    @NonNull
    @Override
    public GroupSettingsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.list_item_group_setting, parent, false);
        return new GroupSettingsAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull GroupSettingsAdapter.ViewHolder holder, int position) {
        holder.tvName.setText(datalist.get(position).getName());
        holder.tvDescription.setText(datalist.get(position).getDescription());
        int setting_id=datalist.get(position).getId();
        if (setting_id==1){
            holder.imageViewSetting.setBackgroundResource(R.drawable.ic_meetings);
        }
        else if (setting_id==2){
            holder.imageViewSetting.setBackgroundResource(R.drawable.ic_contribution);
        }
        else if (setting_id==3){
            holder.imageViewSetting.setBackgroundResource(R.drawable.ic_loans_settings);
        }
        else if (setting_id==4){
            holder.imageViewSetting.setBackgroundResource(R.drawable.ic_fines);
        }
        else if (setting_id==5){
            holder.imageViewSetting.setBackgroundResource(R.drawable.ic_membership);
        }
        else if (setting_id==6){
            holder.imageViewSetting.setBackgroundResource(R.drawable.user_image);
        }

        String status= String.valueOf(datalist.get(position).getStatus());
        Log.e("TAG_STATUS", "onBindViewHolder: "+status);


        holder.swEnable.setChecked(datalist.get(position).isActivated(groupID));
        holder.swEnable.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                String params = "{" +
                        "\"setting_id\":" + '"' + setting_id + '"'+
                        ",\"group_id\":" + '"' + group_id + '"'+
                        "}";
                Log.e("CHANGE_SETTING",params);
                if (b){

                    postToServer(enableSetting,token,params,INITIATOR_ENABLE_SETTING,HTTP_POST,context, GroupSettingsAdapter.this);
                }else{
                    Map<String, String> _params = new HashMap<String, String>();
                    _params.put("token", token);
                    _params.put("param",params);
                    Log.e("CHANGE_SETTING",_params.toString());
                    postToServer(disableSetting,token,params,INITIATOR_DISABLE_SETTING,HTTP_POST,context,GroupSettingsAdapter.this);
                }
            }
        });

        holder.llSetting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle args = new Bundle();
                args.putString("args",setting_id+"/"+group_id);
                args.putString("id", String.valueOf(setting_id));
                args.putString("group_id",group_id);
                Log.e("args",args.getString("args"));
                switch(setting_id){
                    case 1:

                        if (status.equals("true"))
                        {
                            navigate.navigate(R.id.meetingSettingFragment,args);

                        }
                        else {
                            showSweetAlertDialogError("Kindly Activate the Setting before Proceeding",context);
                        }
                        break;
                    case 2:
                        navigate.navigate(R.id.contributionsSettingFragment,args);
                        navigate.navigate(R.id.contributionsListFragment2,args);
                        break;
                    case 3:
                        navigate.navigate(R.id.loansSettingFragment,args);
                        break;
                    case 4:
                        navigate.navigate(R.id.finesSettingFragment,args);
                        break;
                    case 5:
                        navigate.navigate(R.id.feesSettingFragment,args);
                        break;
                    case 6:
                        showSweetAlertDialogError("Not Available",context);
                        break;
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return datalist.size();
    }
    @Override
    public void onDataReady(String initiator, String response) {
        if (callback!=null){
            callback.settingChanged();
        }
        switch (initiator){
            case INITIATOR_ENABLE_SETTING:
                try{
                    runOnUI(new Runnable() {
                        @Override
                        public void run() {
                            Log.e(initiator,response);
                            try {
                                JSONObject jObj = new JSONObject(response);
                                if (jObj.has("errors")) {
                                    String errors = jObj.getString("errors");
                                    showSweetAlertDialogError(errors, context);
                                } else { // if no error message was found in the response,
                                    boolean success = jObj.getBoolean("success");
                                    String api_error_message = jObj.getString("message");
                                    showSweetAlertDialogSuccess(api_error_message, context);
                                }
                            }catch (JSONException e){
                                e.printStackTrace();
                                Log.e(initiator,e.toString());
                                showSweetAlertDialogError(global_error_message, context);
                            }
                        }
                    });
                }catch (Exception e){
                    e.printStackTrace();
                    Log.e(initiator,e.toString());
                    showSweetAlertDialogError(global_error_message, context);
                }
                break;
            case INITIATOR_DISABLE_SETTING:
                try{
                    runOnUI(new Runnable() {
                        @Override
                        public void run() {
                            Log.e("initiator_res",response);
                            try {
                                JSONObject jObj = new JSONObject(response);
                                if (jObj.has("errors")) {
                                    String errors = jObj.getString("errors");
                                    showSweetAlertDialogError(errors, context);
                                } else { // if no error message was found in the response,
                                    boolean success = jObj.getBoolean("success");
                                    String api_error_message = jObj.getString("message");
                                    showSweetAlertDialogSuccess(api_error_message, context);
                                }
                            }catch (JSONException e){
                                e.printStackTrace();
                                Log.e(initiator,e.toString());
                                showSweetAlertDialogError(global_error_message, context);
                            }
                        }
                    });
                }catch (Exception e){
                    e.printStackTrace();
                    Log.e(initiator,e.toString());
                    showSweetAlertDialogError(global_error_message, context);
                }
                break;
        }

    }

    public interface OnSettingChanged{
        void settingChanged();
    }

    private GroupSettingsAdapter.OnSettingChanged callback;

    public GroupSettingsAdapter setOnSettingChanged(GroupSettingsAdapter.OnSettingChanged onSettingChanged){
        callback=onSettingChanged;
        return this;
    }

    @Override
    public void onDataLoaded(String initiator, JSONArray data) {

    }

    @Override
    public void onNetworkError(String initiator, String errorCode, String errorMessage) {
        try{
            runOnUI(new Runnable() {
                @Override
                public void run() {
//                                    hideDialog(ctx);
                    showSweetAlertDialogError(errorMessage, context);
                }
            });
        }catch (Exception e){
//                            hideDialog(ctx);
            showSweetAlertDialogError(global_error_message, context);
            Log.e(initiator, e.toString());
//                    Toast.makeText(ctx, "Login Error Response Exception" + e.toString(), Toast.LENGTH_LONG).show();
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tvName, tvDescription;
        Switch swEnable;
        LinearLayout llSetting;
       public ImageView imageViewSetting;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.tvSettingName);
            tvDescription = itemView.findViewById(R.id.tvSettingDescription);
            swEnable = itemView.findViewById(R.id.swEnable);
            llSetting = itemView.findViewById(R.id.llSetting);
            imageViewSetting = itemView.findViewById(R.id.imageViewSetting);
        }
    }
}
