package com.app.ecobba.Fragments.Profile.response

import com.google.gson.annotations.SerializedName
import lombok.Data

@Data
class Detail {
    var address: String? = null

    @SerializedName(value = "branch_id", alternate = ["branchId"])
    var branchId: String? = null
    var city: String? = null
    var country: String? = null

    @SerializedName(value = "created_at", alternate = ["createdAt"])
    var createdAt: String? = null
    var dob: String? = null

    @SerializedName(value = "doc_no", alternate = ["docNo"])
    var docNo: String? = null

    @SerializedName(value = "doc_type", alternate = ["docType"])
    var docType: Long = 0
    var gender: Long = 0

    @SerializedName(value = "group_id", alternate = ["groupId"])
    var groupId: String? = null
    var id: Long = 0
    var income: Any? = null

    @SerializedName(value = "marital_status", alternate = ["maritalStatus"])
    var maritalStatus: Long = 0
    var occupation: String? = null

    @SerializedName(value = "org_id", alternate = ["orgId"])
    var orgId: String? = null

    @SerializedName(value = "postal_code", alternate = ["postalCode"])
    var postalCode: String? = null
    var residence: Long = 0
    var state: String? = null

    @SerializedName(value = "updated_at", alternate = ["updatedAt"])
    var updatedAt: String? = null

    @SerializedName(value = "user_id", alternate = ["userId"])
    var userId: Long = 0
}