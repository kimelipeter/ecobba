package com.app.ecobba.Fragments.Borrowing;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.app.ecobba.Fragments.Borrowing.Adapters.AvailableLoansAdapter;
import com.app.ecobba.Fragments.Group.Meeting.MeetingDetailsAdapter;
import com.app.ecobba.Fragments.Shop.ShopDashboardFragment;
import com.app.ecobba.Models.AvailableLoans.AvailableLoansResponse;

import com.app.ecobba.R;
import com.app.ecobba.Utils.BaseFragment;
import com.app.ecobba.Utils.SwipeDetectCallback;
import com.app.ecobba.common.SharedMethods;
import com.app.ecobba.common.rxApi.Api;
import com.app.ecobba.common.rxApi.RxApi;

import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;


import java.util.ArrayList;


import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class BorrowFragment extends BaseFragment  implements View.OnClickListener {

    Context context;
    RecyclerView recyclerView;
    Fragment fragment=this;
    @BindView(R.id.progressBar) ProgressBar progressBar;

    @BindView(R.id.tvNoAvailableData)   TextView tvNoAvailableData;
//    @BindView(R.id.btFilter)
//    Button filter;
    @BindView(R.id.myLoans)
    FloatingActionButton myLoans;
    private Api api;
    public AvailableLoansAdapter adapter;
    ProgressDialog progressDialog;
    SwipeRefreshLayout mSwipeRefreshLayout;
    public BorrowFragment(){

    }
    public static BorrowFragment newInstance(String param1, String param2) {
        BorrowFragment fragment = new BorrowFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_borrowing,container,false);
        unbinder = ButterKnife.bind(this,view);
        context = requireContext();
        mSwipeRefreshLayout = view.findViewById(R.id.swipe_container);
        recyclerView = view.findViewById(R.id.recyclerView);
        api = new RxApi().getApi();
        recyclerView.addItemDecoration(new DividerItemDecoration(getContext(),
                DividerItemDecoration.VERTICAL));
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
        adapter = new AvailableLoansAdapter(new ArrayList(), fragment, context);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        myLoans.setImageResource(R.drawable.ic_baseline_money);
        myLoans.setOnClickListener(this);
        progressDialog = new ProgressDialog(context);
       fetchLoans();

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                Toast.makeText(context, "Refreshing", Toast.LENGTH_LONG).show();
                fetchLoans();
                mSwipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary,
                        android.R.color.holo_green_dark,
                        android.R.color.holo_orange_dark,
                        android.R.color.holo_blue_dark);

                mSwipeRefreshLayout.setRefreshing(false);
            }
        });
    }
    @Override
    public void onResume() {
        super.onResume();
        fetchLoans();
    }



    private void fetchLoans(){
        progressDialog.setMessage("Loading.Please wait....");
        progressDialog.show();
        Call<AvailableLoansResponse> call = api.getLatestPackages();
        call.enqueue(new Callback<AvailableLoansResponse>() {
            @Override
            public void onResponse(Call<AvailableLoansResponse> call, Response<AvailableLoansResponse> response) {
                Log.e("GET_RESPONSE", "onResponse: "+ response);
                if (response.body().getSuccess()){
                    progressDialog.dismiss();
                    if (response.body().getPackages().size()>0){

                        adapter.dataList = response.body().getPackages();
                        adapter.notifyDataSetChanged();

                    }
                    else {
                        tvNoAvailableData.setText(getResources().getString(R.string.no_available_data));
                        tvNoAvailableData.setVisibility(View.VISIBLE);
                    }

                }

            }

            @Override
            public void onFailure(Call<AvailableLoansResponse> call, Throwable t) {
                Log.e("LOAN_ERROR",t.getLocalizedMessage());

            }



        });

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.myLoans:
                NavHostFragment.findNavController(BorrowFragment.this).navigate(R.id.loansFragment);
        }
    }
}




