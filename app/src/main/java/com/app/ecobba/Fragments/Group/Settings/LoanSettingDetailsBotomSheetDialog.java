package com.app.ecobba.Fragments.Group.Settings;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import androidx.lifecycle.ViewModelProvider;

import com.app.ecobba.R;
import com.app.ecobba.ViewModel.UserProfileViewModel;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;


import butterknife.ButterKnife;


public class LoanSettingDetailsBotomSheetDialog extends BottomSheetDialogFragment {
    ImageView close;
    TextView tvLoanName, tvName, tvMaxAmount, tvMinAmount,tvRepaymentPlan,tvFine,tvInterest,tvRepaymentPeriod;
    UserProfileViewModel userProfileViewModel;

    String name, type, min, max,repayment_plan,repayment_period,interest,fine;
    String user_currency;
    Context context;

    public LoanSettingDetailsBotomSheetDialog(String name, String type, String min, String max, String user_currency,
                                              String repayment_plan, String repayment_period,String interest,String fine) {
        this.name = name;
        this.type = type;
        this.min = min;
        this.max = max;
        this.user_currency = user_currency;
        this.repayment_period=repayment_period;
        this.repayment_plan=repayment_plan;
        this.interest=interest;
        this.fine=fine;
    }

    @Override
    public int getTheme() {
        return R.style.AppBottomSheetDialogTheme;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.loan_bottom_sheet, container, false);
        close = view.findViewById(R.id.close);
        tvLoanName = view.findViewById(R.id.tvLoanName);
        tvName = view.findViewById(R.id.tvName);
        tvMaxAmount = view.findViewById(R.id.tvMaxAmount);
        tvMinAmount = view.findViewById(R.id.tvMinAmount);
        tvInterest=view.findViewById(R.id.tvInterest);
        tvRepaymentPlan=view.findViewById(R.id.tvRepaymentPlan);
        tvFine=view.findViewById(R.id.tvFine);
        tvRepaymentPeriod=view.findViewById(R.id.tvRepaymentPeriod);


        setStyle(STYLE_NORMAL, R.style.AppBottomSheetDialogTheme);
        userProfileViewModel = new ViewModelProvider(getActivity()).get(UserProfileViewModel.class);

        return view;
    }


    @SuppressLint("SetTextI18n")
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        context = getActivity();
        close.setOnClickListener(v -> {
            dismiss();
        });
        tvLoanName.setText(name);
        tvName.setText(name);
        tvMinAmount.setText(user_currency + " " + min);
        tvMaxAmount.setText(user_currency + " " + max);
        tvRepaymentPlan.setText(repayment_plan);
        tvFine.setText(fine+"%");
        tvInterest.setText(user_currency + " " + interest);
        tvRepaymentPeriod.setText(repayment_period);
    }


}
