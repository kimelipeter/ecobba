package com.app.ecobba.Fragments.Transfer;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.app.ecobba.R;
import com.google.android.material.textfield.TextInputEditText;
import com.jaredrummler.materialspinner.MaterialSpinner;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TransferFragment extends Fragment implements MaterialSpinner.OnItemSelectedListener{

    @BindView(R.id.msFrom)
    MaterialSpinner msFrom;

    @BindView(R.id.msTo)
    MaterialSpinner msTo;

    @BindView(R.id.tiAmount)
    TextInputEditText tiAmount;

    @BindView(R.id.tiPin)
    TextInputEditText tiPin;

    int wallet = R.drawable.ic_wallet,
            phone = R.drawable.ic_phone,
            card = R.drawable.ic_credit_card;


    String[] from,
            to;

    Context ctx;

    public TransferFragment(Context ctx){this.ctx = ctx;}

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View frag = inflater.inflate(R.layout.fragment_money_transfer,container,false);
        ButterKnife.bind(this,frag);
        from = getResources().getStringArray(R.array.transfer_from);
        to = getResources().getStringArray(R.array.transfer_to);

        return frag;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        msFrom.setItems(from);
        msTo.setItems(to);
        msFrom.setOnItemSelectedListener(this);
        msTo.setOnItemSelectedListener(this);
    }

    @Override
    public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
        switch (view.getId()){
            case R.id.msFrom:
                switch (position){
                    case 0:
                        ((ImageView)getActivity().findViewById(R.id.ivFrom)).setImageResource(phone);
                        break;
                    case 1:
                        ((ImageView)getActivity().findViewById(R.id.ivFrom)).setImageResource(wallet);
                        break;
                    case 2:
                        ((ImageView)getActivity().findViewById(R.id.ivFrom)).setImageResource(card);
                        break;
                }

                break;
            case R.id.msTo:
                switch (position){
                    case 0:
                        ((ImageView)getActivity().findViewById(R.id.ivTo)).setImageResource(wallet);
                        break;
                    case 1:
                        ((ImageView)getActivity().findViewById(R.id.ivTo)).setImageResource(phone);
                        break;
                }
                break;
        }
    }

    public interface OnToAndFromChanged{
        void from(int from);
        void to(int to);
    }

    private OnToAndFromChanged callback;

    public TransferFragment SetOnOptionsChangedListener(OnToAndFromChanged c){
        return this;
    }

}
