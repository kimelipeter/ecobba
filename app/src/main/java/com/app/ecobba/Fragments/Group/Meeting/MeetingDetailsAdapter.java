package com.app.ecobba.Fragments.Group.Meeting;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.app.ecobba.Models.MeetingDetailsData.ContributionX;
import com.app.ecobba.Models.MeetingDetailsData.DataX;
import com.app.ecobba.Models.MeetingDetailsData.MemberContributionX;
import com.app.ecobba.R;
import com.app.ecobba.recyclerview_models.MeetingDetails.Data;

import java.util.List;

public class MeetingDetailsAdapter extends RecyclerView.Adapter<MeetingDetailsAdapter.ViewHolder> {

   public  List<MemberContributionX> dataX;
  Context context;
     Fragment fragment;

    public MeetingDetailsAdapter( List<MemberContributionX>dataX, Context context, Fragment fragment) {
        this.dataX = dataX;
        this.context = context;
        this.fragment = fragment;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.meeting_list_items, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        try {

            Log.e("NAME_CON", dataX.get(position).getContribution().getContribution_name());
            dataX.get(position).getStatus();
            holder.contribution_type.setText(""+ dataX.get(position).getContribution().getContribution_name());
            holder.shares_bought.setText(""+dataX.get(position).getShares_bought());
            holder.total_amount.setText(  String.valueOf(dataX.get(position).getTotal_amount()));

        }catch (Exception e){
            e.printStackTrace();
            holder.shares_bought.setText("0");
        }



    }

    @Override
    public int getItemCount() {
        return dataX.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView member_name,contribution_type,shares_bought,total_amount;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            member_name = itemView.findViewById(R.id.tvMemberName);
            contribution_type = itemView.findViewById(R.id.tvContributionType);
            shares_bought = itemView.findViewById(R.id.tvSharesBought);
            total_amount = itemView.findViewById(R.id.tvTotalAmount);
        }
    }
}
