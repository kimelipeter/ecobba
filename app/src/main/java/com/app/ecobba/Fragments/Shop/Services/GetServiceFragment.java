package com.app.ecobba.Fragments.Shop.Services;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.fragment.NavHostFragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.ecobba.Activities.ChatActivity;
import com.app.ecobba.Activities.KweaActivity;
import com.app.ecobba.Models.UserProfile.Detail;
import com.app.ecobba.R;
import com.app.ecobba.Utils.BaseFragment;
import com.app.ecobba.ViewModel.UserProfileViewModel;
import com.app.ecobba.common.FCMCHAT.ChatMessages;
import com.app.ecobba.common.FCMCHAT.ChatViewModel;
import com.app.ecobba.common.FCMCHAT.Participants;
import com.app.ecobba.common.SharedMethods;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.util.ArrayList;

import butterknife.ButterKnife;
import kotlin.Unit;

import static com.app.ecobba.common.SharedMethods.showSweetAlertDialogError;

public class GetServiceFragment extends BaseFragment {
    ImageView img, profile_image;
    Button contact_seller;

    TextView tv_name, tv_price, tv_description, tv_shopname, tv_shop_address;
    String currency,price;
    Context context;
    UserProfileViewModel userProfileViewModel;
    private ChatViewModel chatViewModel;

    public GetServiceFragment() {
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_get_service, container, false);
        ButterKnife.bind(this, view);
        context = getActivity();
        //ini views
        tv_name = view.findViewById(R.id.aa_anime_name);
        tv_price = view.findViewById(R.id.aa_price);
        tv_description = view.findViewById(R.id.aa_description);
        tv_shopname = view.findViewById(R.id.shopname);
        img = view.findViewById(R.id.aa_thumbnail);
        profile_image = view.findViewById(R.id.profile_image);
        tv_shop_address = view.findViewById(R.id.item_shop_adress);
        Toolbar toolbar = view.findViewById(R.id.toolbar);
        contact_seller = view.findViewById(R.id.contact_seller);
        toolbar.setNavigationIcon(R.drawable.ic_baseline_arrow_back_24);
        userProfileViewModel = new ViewModelProvider(getActivity()).get(UserProfileViewModel.class);
        chatViewModel = ViewModelProviders.of(this).get(ChatViewModel.class);
        //fetchUsersData();
        return view;

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        context=getActivity();
        fetchUsersData();

        //Recieve Data
        String name = getArguments().getString("productname");
        String duration = getArguments().getString("duration");
        String description = getArguments().getString("description");
         price = getArguments().getString("price");
        assert price != null;
        Log.e("item_price_is", price);
        String image_url = getArguments().getString("img");
        String item_currency = getArguments().getString("item_currency");
        String owner = getArguments().getString("owner");
        String profile = getArguments().getString("profile");
        String item_owner_id = getArguments().getString("user_id");
        assert item_owner_id != null;
        Log.e("user_owner_id_is", item_owner_id);


        // setting values to each view
        tv_name.setText(name);
        tv_description.setText(description);
        tv_shop_address.setText(duration);
        tv_shopname.setText(owner);

        //set Image Using Picasso
        Picasso.get().load(image_url).fit().centerCrop().into(img);
        Picasso.get().load(profile).fit().centerCrop().into(profile_image);

        contact_seller.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int user1 = Math.min(SharedMethods.getUser(context), Integer.parseInt(item_owner_id));
                int user2 = Math.max(SharedMethods.getUser(context), Integer.parseInt(item_owner_id));
                String chatId = user1 + "&&" + user2;
                //List<Participants> participants = new ArrayList<>();
                ArrayList<Participants> participants = new ArrayList<>();
                participants.add(new Participants(SharedMethods.getUser(context), "Kimeli Peter", " "));
                participants.add(new Participants(Integer.parseInt(item_owner_id), "Mutonya George", " "));
                chatViewModel.createChat(chatId, participants, "Mutonya George", " ", chat -> {
                    Bundle bundle = new Bundle();
                    Log.e("CHATS", new Gson().toJson(chat));
                    bundle.putString("CHATM", new Gson().toJson(chat));

                    return Unit.INSTANCE;
                });


            }
        });

    }

    public void fetchUsersData() {
        userProfileViewModel.getUserProfile();
        userProfileViewModel.userProfileResponseLiveData().observe(getViewLifecycleOwner(), userProfileResponse -> {

            if (userProfileResponse.getSuccess()) {
                //  Detail userDetails = userProfileResponse.getUser().getDetail();

                currency = userProfileResponse.getUser().getWallet().getCurrency().getPrefix();
                Log.e("USER_CURRENCY",currency);
                tv_price.setText(currency+ " "+price);


            } else {

                showSweetAlertDialogError("Something went Wrong", context);
            }
        });
    }

}
