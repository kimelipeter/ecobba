package com.app.ecobba.Fragments.Profile.userProfResponse

data class Wallet(
    val balance: Int,
    val created_at: String,
    val id: Int,
    val status: Int,
    val updated_at: String,
    val user_id: Int
)