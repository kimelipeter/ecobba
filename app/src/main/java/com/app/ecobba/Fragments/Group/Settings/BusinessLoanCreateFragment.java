package com.app.ecobba.Fragments.Group.Settings;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.ecobba.Activities.MainActivity;
import com.app.ecobba.Fragments.Group.Loans.GetGroupMembersGuarantorsAdapter;
import com.app.ecobba.Models.GroupLoanApplication.groupLoanApplicationResponse;
import com.app.ecobba.Models.GroupMmembers.Data;
import com.app.ecobba.Models.GroupMmembers.groupMembersResponse;
import com.app.ecobba.Models.GroupWallet.AdapaySource;
import com.app.ecobba.Models.GroupWallet.groupWalletResponse;
import com.app.ecobba.Models.LendingData.Repaymentplan;
import com.app.ecobba.Models.LendingData.lendingDataResponse;
import com.app.ecobba.Models.adapay.adapaySourcesResponse;
import com.app.ecobba.Models.adapay.telcos.telcosResponse;
import com.app.ecobba.R;
import com.app.ecobba.common.SharedMethods;
import com.app.ecobba.common.rxApi.Api;
import com.app.ecobba.common.rxApi.RxApi;
import com.google.android.material.chip.Chip;
import com.google.gson.Gson;
import com.jaredrummler.materialspinner.MaterialSpinner;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.app.ecobba.common.ApisKtKt.storeLoanSettings;
import static com.app.ecobba.common.AppConstantsKt.INITIATOR_STORE_LOAN_SETTINGS;
import static com.app.ecobba.common.SharedMethods.HTTP_POST;
import static com.app.ecobba.common.SharedMethods.global_error_message;
import static com.app.ecobba.common.SharedMethods.postToServer;
import static com.app.ecobba.common.SharedMethods.runOnUI;
import static com.app.ecobba.common.SharedMethods.showSweetAlertDialogError;
import static com.app.ecobba.common.SharedMethods.showSweetAlertDialogSuccess;

public class BusinessLoanCreateFragment extends Fragment implements View.OnClickListener, SharedMethods.NetworkObjectListener, MaterialSpinner.OnItemSelectedListener {
    Context context;
    Fragment fragment;
    private Api api;
    Spinner payMethod, provider, msisdn;
    LinearLayout linearLayoutMobile, llLayoutMobileMpesa, linearLayoutBank;
    ;
    ArrayAdapter arrayAdapter;
    List<String> repayment_plans;
    List<String> ada_pay_ids, telcos_adapay;
    String _msisdn, type, telco, selected_type, selected_provider, day;
    Bundle args;
    String token, api_tail, setting_id, group_id, repayment_plan = "0", loan_type;
    String ada_id = "";
    String visibility = "private";
    String setting_index = "";
    @BindView(R.id.repaymentPlan)
    MaterialSpinner repaymentPlan;
    ColorDrawable popupColor;
    @BindView(R.id.recyclerViewMembers)
    RecyclerView recyclerViewMembers;
    List<Data> data = new ArrayList<>();
    GetGroupMembersGuarantorsAdapter adapter;
    List<String> phoneNumber = new ArrayList<>();
    @BindView(R.id.progress_dialog)
    ProgressBar progress_dialog;
    @BindView(R.id.vfLoanApplication)
    ViewFlipper vfLoanApplication;
    @BindView(R.id.sbApplyLoan)
    SeekBar sbApplyLoan;
    @BindView(R.id.pbApplyLoan)
    ProgressBar load;
    @BindView(R.id.btApplyContinue)
    Chip btApplyContinue;
    @BindView(R.id.tvLoanName)
    EditText tvLoanName;
    @BindView(R.id.tvMinimum)
    EditText tvMinimum;
    @BindView(R.id.tvMax)
    EditText tvMax;
    @BindView(R.id.tv_interest_rate)
    EditText tv_interest_rate;
    @BindView(R.id.tvRepaymentPeriod)
    EditText tvRepaymentPeriod;
    @BindView(R.id.tvFine)
    EditText tvFine;
    @BindView(R.id.tvMinimumGuarantors)
    EditText tvMinimumGuarantors;
    @BindView(R.id.FllFinish)
    FrameLayout FllFinish;

    @BindView(R.id.btnBusinessLoan)
    Chip btnBusinessLoan;
    @BindView(R.id.btnWelfareLoan)
    Chip btnWelfareLoan;
    @BindView(R.id.loan_typeName)
    TextView loan_typeName;
    String _loanName, _minimum, _maximum, interest_rate, repaymentPeriod, _fine, _minimumGuarantors;

    public BusinessLoanCreateFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View frag = inflater.inflate(R.layout.fragment_create_business_loan, container, false);
        ButterKnife.bind(this, frag);
        context = getContext();
        fragment = this;
        recyclerViewMembers = frag.findViewById(R.id.recyclerViewMembers);
        payMethod = frag.findViewById(R.id.payMethod);
        provider = frag.findViewById(R.id.provider);


        msisdn = frag.findViewById(R.id.msisdn);
        linearLayoutMobile = frag.findViewById(R.id.linearLayoutMobile);
        llLayoutMobileMpesa = frag.findViewById(R.id.llLayoutMobileMpesa);
        linearLayoutBank = frag.findViewById(R.id.linearLayoutBank);
        api = new RxApi().getApi();

        adapter = new GetGroupMembersGuarantorsAdapter(data, requireContext(), fragment, new GetGroupMembersGuarantorsAdapter.OnItemCheckListener() {

            @Override
            public void onItemChecked(Data data) {
                phoneNumber.add(data.getMsisdn());
                Log.d("TAG", "onItemChecked: " + phoneNumber);
            }

            @Override
            public void onItemUnChecked(Data data) {
                phoneNumber.remove(data.getMsisdn());
                Log.d("TAG", "onItemChecked: Removed: " + phoneNumber);

            }

        });

        RecyclerView.LayoutManager layoutManager1 = new LinearLayoutManager(requireContext());
        recyclerViewMembers.setLayoutManager(layoutManager1);
        recyclerViewMembers.setAdapter(adapter);
        setPage(0);
        btApplyContinue.setOnClickListener(this);
        btnBusinessLoan.setOnClickListener(this);
        btnWelfareLoan.setOnClickListener(this);
        return frag;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        context = getActivity();
        fragment = this;
        token = SharedMethods.getDefaults("token", context);
        args = getArguments();
        assert args != null;
        api_tail = args.getString("args");
        group_id = args.getString("group_id");
        setting_id = args.getString("id");
        loan_type = args.getString("loan_type");
        if (loan_type.equals("jamii")) {
            btnWelfareLoan.setVisibility(View.VISIBLE);
            loan_typeName.setText(R.string.welfare_loan);
            btApplyContinue.setText(R.string.next);
        } else if (loan_type.equals("biashara")){
            btnBusinessLoan.setVisibility(View.VISIBLE);
            loan_typeName.setText(R.string.businness_loan);
            btApplyContinue.setText(R.string.next);
        }
        repaymentPlan.setOnItemSelectedListener(this);
        repaymentPlan.getPopupWindow().setBackgroundDrawable(popupColor);
        fetchAdaPaySourceType();
        fetchAdaPayTelcos();
        fetchUserPaymentSoucers(group_id);
        fetchGroupMembers(group_id);
        fetchFormDropDownData();
    }

    private void setPage(int n) {
        int m = vfLoanApplication.getChildCount() - 1;
        sbApplyLoan.setMax(m);
        if (n <= m && n >= 0) {
            vfLoanApplication.setDisplayedChild(n);
            sbApplyLoan.setProgress(n);
//            if (n == m) {
//                confirm.setVisibility(View.GONE);
//            }
        }
    }

    private void toggleProg() {
        if (load.getVisibility() == View.GONE) {
            load.setVisibility(View.VISIBLE);
        } else {
            load.setVisibility(View.VISIBLE);
        }
    }

    private void fetchGroupMembers(String group_id) {
        progress_dialog.setVisibility(View.VISIBLE);
        Call<groupMembersResponse> call = api.getGroupMmebers(group_id);
        call.enqueue(new Callback<groupMembersResponse>() {
            @Override
            public void onResponse(Call<groupMembersResponse> call, Response<groupMembersResponse> response) {
                try {
                    Log.e("MEMBERS", "onResponse: " + response);
                    if (response.body().getSuccess()) {
                        progress_dialog.setVisibility(View.GONE);
                        if (response.body().getData().size() > 0) {
                            adapter.datalist = response.body().getData();
                            adapter.notifyDataSetChanged();
                        }
                    } else if (!response.body().getSuccess()) {
                        String api_error_message = "Sorry";
                        showSweetAlertDialogError(api_error_message, context);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<groupMembersResponse> call, Throwable t) {
                Log.e("FAILGED_TO_GET", "onFailure: " + t.getLocalizedMessage());
            }
        });
    }

    public void fetchAdaPaySourceType() {

        Call<adapaySourcesResponse> call = api.fetchAdapaySources();
        call.enqueue(new Callback<adapaySourcesResponse>() {
            @Override
            public void onResponse(Call<adapaySourcesResponse> call, Response<adapaySourcesResponse> response) {

                try {
                    if (response.body().getSuccess()) {
                        String mobile = response.body().getAdapay_source_types().getMobile_money();
                        String card = response.body().getAdapay_source_types().getCard_hosted();
                        String bank_transfer = response.body().getAdapay_source_types().getBank_transfer();

                        ada_pay_ids = new ArrayList<>();
                        ada_pay_ids.add(0, getResources().getString(R.string.please_select_type));
                        ada_pay_ids.add(1, mobile);
                        ada_pay_ids.add(2, card);
                        ada_pay_ids.add(3, bank_transfer);

                        ArrayAdapter<String> adapter =
                                new ArrayAdapter<String>(context, android.R.layout.simple_spinner_dropdown_item, ada_pay_ids);
                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                        payMethod.setAdapter(adapter);

                        payMethod.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                                String type = (String) parent.getItemAtPosition(position);

                                selected_type = "";
                                if (type.equals("Mobile Money")) {
                                    selected_type = "mobile_money";
                                    linearLayoutMobile.setVisibility(View.VISIBLE);
//                                            linearLayoutBank.setVisibility(View.GONE);

                                }
//                                        else if (type.equals("Master/Visa Card")){
//                                            selected_type = "card_hosted";
//                                            linearLayoutBank.setVisibility(View.GONE);
//                                            linearLayoutMobile.setVisibility(View.GONE);
//                                        }
//                                        else if (type.equals("Bank Transfer")){
//                                            selected_type = "bank_transfer";
//                                            linearLayoutBank.setVisibility(View.GONE);
//                                            linearLayoutMobile.setVisibility(View.GONE);
//                                        }
                                else {
//
                                    //    linearLayoutBank.setVisibility(View.GONE);
                                    linearLayoutMobile.setVisibility(View.GONE);
                                }
                                Log.e("Selected_type", String.valueOf(type));
                                Log.e("Selected_type2", selected_type);
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {

                            }

                        });
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }


            }

            @Override
            public void onFailure(Call<adapaySourcesResponse> call, Throwable t) {

            }
        });

    }

    private void fetchUserPaymentSoucers(String group_id) {
        Call<groupWalletResponse> call = api.getGroupWalletPaymentSources(group_id);
        call.enqueue(new Callback<groupWalletResponse>() {
            @Override
            public void onResponse(Call<groupWalletResponse> call, Response<groupWalletResponse> response) {
                try {
                    if (response.body().getSuccess()) {
                        if (response.body().getAdapay_sources().size() > 0) {

                            List<AdapaySource> sources = response.body().getAdapay_sources();
                            ArrayList<String> availables = new ArrayList<>();
                            // availables.add(0, getResources().getString(R.string.please_select_contribution));
                            for (int item = 0; item < sources.size(); item++) {
                                availables.add(sources.get(item).getMsisdn());
                            }
                            arrayAdapter = new ArrayAdapter(context, android.R.layout.simple_spinner_dropdown_item, availables);
                            arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            msisdn.setAdapter(arrayAdapter);
                            msisdn.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                    String item_resource_type = (String) parent.getItemAtPosition(position);

                                    _msisdn = sources.get(position).getMsisdn();
                                    Log.e("PHONE_NUMBER", _msisdn);

                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> parent) {

                                }
                            });
                        }

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }


            }

            @Override
            public void onFailure(Call<groupWalletResponse> call, Throwable t) {

            }
        });
    }

    public void fetchAdaPayTelcos() {
        Call<telcosResponse> call = api.fetchAdapayTelcos();
        call.enqueue(new Callback<telcosResponse>() {
            @Override
            public void onResponse(Call<telcosResponse> call, Response<telcosResponse> response) {
                try {
                    if (response.body().getSuccess()) {
                        String telcos = response.body().getAdapay_telcos().getSafaricom_ke();
                        telcos_adapay = new ArrayList<>();
                        telcos_adapay.add(0, getResources().getString(R.string.please_select_type));
                        telcos_adapay.add(1, telcos);
                        ArrayAdapter<String> adapter =
                                new ArrayAdapter<String>(context, android.R.layout.simple_spinner_dropdown_item, telcos_adapay);
                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        provider.setAdapter(adapter);
                        provider.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                String type = (String) parent.getItemAtPosition(position);
                                selected_provider = "";
                                if (type.equals("Mpesa")) {
                                    llLayoutMobileMpesa.setVisibility(View.VISIBLE);
                                    selected_provider = "safaricom_ke";
                                } else {
                                    llLayoutMobileMpesa.setVisibility(View.GONE);
                                }
                                Log.e("Selected_telcos", String.valueOf(type));
                                Log.e("Selected_telcos2", selected_provider);
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {

                            }
                        });


                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<telcosResponse> call, Throwable t) {
                t.getLocalizedMessage();

            }
        });
    }

    public void fetchFormDropDownData() {
        Call<lendingDataResponse> call = api.getLendingData();
        call.enqueue(new Callback<lendingDataResponse>() {
            @Override
            public void onResponse(Call<lendingDataResponse> call, Response<lendingDataResponse> response) {
                try {
                    if (response.body().getSuccess()) {
                        if (response.body().getMessage().getRepaymentplans().size() > 0) {
                            repayment_plans = new ArrayList<>();
                            List<Repaymentplan> repayment_classes = response.body().getMessage().getRepaymentplans();
                            ArrayList<String> repaymentplans_drop_down = new ArrayList<String>();
                            repaymentplans_drop_down.add(getResources().getString(R.string.select_plan));
                            repayment_plans.add("0");
                            for (int i = 0; i < repayment_classes.size(); i++) {
                                String id = String.valueOf(repayment_classes.get(i).getId());
                                String name = repayment_classes.get(i).getName();
                                repaymentplans_drop_down.add(name);
                                repayment_plans.add(name);

                            }
                            repaymentPlan.setItems(repaymentplans_drop_down);
                            repaymentPlan.setSelectedIndex(0);
                        }

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }


            }

            @Override
            public void onFailure(Call<lendingDataResponse> call, Throwable t) {

            }
        });
    }

    private boolean validateMembersCount() {
        boolean valid = true;
        if (phoneNumber.size() < 1) {
            Log.e("daadadad", "validateMembersCount: " + phoneNumber);
            valid = false;
        }

        return valid;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btApplyContinue:
                if (validateFormFields()) {
                    v.setVisibility(View.GONE);
                    FllFinish.setVisibility(View.VISIBLE);
                    setPage(1);
                }

                break;
            case R.id.btnBusinessLoan:
                if (validateMembersCount()) {
                    v.setVisibility(View.GONE);
                    v.setEnabled(false);
                    toggleProg();
                    postLoanSetting();
                } else {
                    Toast.makeText(context, getString(R.string.loan_approvers), Toast.LENGTH_LONG).show();
                }

                break;

            case R.id.btnWelfareLoan:
                if (!validateMembersCount()) {
                    Toast.makeText(context, getString(R.string.loan_approvers), Toast.LENGTH_LONG).show();
                } else {
                    v.setVisibility(View.GONE);
                    v.setEnabled(false);
                    toggleProg();
                    postLoanSetting();
                }

                break;
        }

    }

    private void postLoanSetting() {

        String params = "{ \"loan_type\":" + '"' + loan_type + '"' +
                ", \"loan_name\": " + '"' + _loanName + '"' +
                ", \"min_amount\": " + '"' + _minimum + '"' +
                ", \"max_amount\": " + '"' + _maximum + '"' +
                ", \"repayment_plan\": " + '"' + repayment_plan + '"' +
                ", \"repayment_period\": " + '"' + repaymentPeriod + '"' +
                ", \"interest\": " + '"' + interest_rate + '"' +
                ", \"fine\": " + '"' + _fine + '"' +
                ", \"group_id\": " + '"' + group_id + '"' +
                ", \"setting_id\": " + '"' + setting_id + '"' +
                ", \"setting_index\": " + '"' + setting_index + '"' +
                ", \"min_guarantors\": " + '"' + _minimumGuarantors + '"' +
                ", \"visibility\": " + '"' + visibility + "\"," +
                "\"payout_source\": {"
                + "\"id\": \"" + ada_id + "\","
                + "\"method\": \"" + selected_type + "\","
                + "\"telco\": \"" + selected_provider + "\","
                + "\"msisdn\": \"" + _msisdn + "\"" +
                "}" +
                ", \"loan_approvers\": " + phoneNumber
                + "}\n" + "\n";
        Log.e("POST_PARAMS", params);

        postToServer(storeLoanSettings, token, params, INITIATOR_STORE_LOAN_SETTINGS, HTTP_POST, context, this);
    }

    private boolean validateFormFields() {
        boolean valid = true;
        _loanName = tvLoanName.getText().toString();
        _minimum = tvMinimum.getText().toString();
        _maximum = tvMax.getText().toString();
        interest_rate = tv_interest_rate.getText().toString();
        repaymentPeriod = tvRepaymentPeriod.getText().toString();
        _fine = tvFine.getText().toString();
        _minimumGuarantors = tvMinimumGuarantors.getText().toString();


        if (_loanName.isEmpty()) {
            tvLoanName.setError(getString(R.string.emptyField));
            valid = false;
        }
        if (_minimum.isEmpty()) {
            tvMinimum.setError(getString(R.string.emptyField));
            valid = false;
        }
        if (_maximum.isEmpty()) {
            tvMax.setError(getString(R.string.emptyField));
            valid = false;
        }
        if (interest_rate.isEmpty()) {
            tv_interest_rate.setError(getString(R.string.emptyField));
            valid = false;
        }
        if (repaymentPeriod.isEmpty()) {
            tvRepaymentPeriod.setError(getString(R.string.emptyField));
            valid = false;
        }
        if (_fine.isEmpty()) {
            tvFine.setError(getString(R.string.emptyField));
            valid = false;
        }
        if (_minimumGuarantors.isEmpty()) {
            tvMinimumGuarantors.setError(getString(R.string.emptyField));
            valid = false;
        }
        return valid;
    }

    @Override
    public void onDataReady(String initiator, String response) {
        switch (initiator) {
            case INITIATOR_STORE_LOAN_SETTINGS:
                try {
                    runOnUI(new Runnable() {
                        @Override
                        public void run() {
                            Log.e("CONFIRM_LOAN", "Response is " + response);
                            JSONObject jObj = null;
                            try {
                                jObj = new JSONObject(response);
                                if (jObj.has("errors")) {
                                    String errors = jObj.getString("errors");
                                    showSweetAlertDialogError(errors, context);
                                } else { // if no error message was found in the response,
                                    boolean success = jObj.getBoolean("success");
                                    if (success) { // if the request was successfuls, got forward, else show error message
                                        showSweetAlertDialogSuccess("Loan settings saved Successfully", context);
                                        NavHostFragment.findNavController(getParentFragment()).navigateUp();
                                        setPage(2);
                                    } else {
                                        String api_error_message = jObj.getString("message");
                                        showSweetAlertDialogError(api_error_message, context);
                                        toggleProg();
                                    }
                                }

                            } catch (JSONException e) {
                                showSweetAlertDialogError(global_error_message, context);
                                e.printStackTrace();
                            }
                        }
                    });
                } catch (Exception e) {

                    showSweetAlertDialogError(global_error_message, context);
                }
                break;
        }

    }

    @Override
    public void onDataLoaded(String initiator, JSONArray data) {

    }

    @Override
    public void onNetworkError(String initiator, String errorCode, String errorMessage) {
        switch (initiator) {
            case INITIATOR_STORE_LOAN_SETTINGS:
                try {
                    runOnUI(new Runnable() {
                        @Override
                        public void run() {
                            showSweetAlertDialogError(errorMessage, context);
                        }
                    });
                } catch (Exception e) {
                    showSweetAlertDialogError(global_error_message, context);

                }

                break;
        }

    }

    @Override
    public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
        switch (view.getId()) {
            case R.id.repaymentPlan:
                Log.d("Repayment plan id is ", repayment_plans.get(position));
                repayment_plan = repayment_plans.get(position);
                break;


        }
    }

}
