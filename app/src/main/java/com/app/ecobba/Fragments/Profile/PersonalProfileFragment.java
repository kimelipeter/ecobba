package com.app.ecobba.Fragments.Profile;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;

import com.app.ecobba.Fragments.Profile.viewmodel.ProfileViewModel;
import com.app.ecobba.Models.RegistrationData.Gender;
import com.app.ecobba.Models.RegistrationData.MaritalStatuse;
import com.app.ecobba.Models.UserProfile.User;
import com.app.ecobba.R;
import com.app.ecobba.ViewModel.UserProfileViewModel;
import com.jaredrummler.materialspinner.MaterialSpinner;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;

public class PersonalProfileFragment extends Fragment implements View.OnClickListener {

    ProfileViewModel profileViewModel;

    UserProfileViewModel userProfileViewModel;

    @BindView(R.id.msGender)
    MaterialSpinner msGender;

    @BindView(R.id.msMaritalStatus)
    MaterialSpinner msMaritalStatus;

    @BindViews({
            R.id.etFirstName,
            R.id.etOtherName
    })
    List<EditText> inputs;
    public static EditText dateOfBirth;
    public static String dob;

    ArrayList<String> gender_id, marital_status_id, gender_drop_down, marital_status_drop_down;
    String first = "", second = "", marital = "0", gender = "0";
    Boolean created = false, init = false, populate = false;
    int gender_pos = 0, marital_status_pos = 0;
    Context ctx;

    public PersonalProfileFragment(Context ctx) {
        this.ctx = ctx;
        gender_id = new ArrayList<>();
        marital_status_id = new ArrayList<>();
        gender_drop_down = new ArrayList<>();
        marital_status_drop_down = new ArrayList<>();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View frag = inflater.inflate(R.layout.fragment_registration_personal, container, false);
        ButterKnife.bind(this, frag);
//        btCalender.setOnClickListener(this);
        profileViewModel = ViewModelProviders.of(getActivity()).get(ProfileViewModel.class);
        userProfileViewModel = new ViewModelProvider(getActivity()).get(UserProfileViewModel.class);
        return frag;
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        created = true;
        getProfileData();
        setMsMaritalStatus();

        dateOfBirth = view.findViewById(R.id.eTxDateOfBirth);
        dateOfBirth.setOnClickListener(this);
//        init();
    }
    public void getProfileData() {
        userProfileViewModel.getUserProfile();
        userProfileViewModel.userProfileResponseLiveData().observe(getViewLifecycleOwner(), userProfileResponse -> {
            try {
                setData(userProfileResponse.getUser());
            } catch (Exception e) {
                e.printStackTrace();
            }

        });

    }

    public void init() {
        inputs.get(0).setText(first);
        inputs.get(1).setText(second);
        dateOfBirth.setText(dob);
        msGender.setSelectedIndex(gender_pos);
        msMaritalStatus.setSelectedIndex(marital_status_pos);

    }
    public void setInitData(List<MaritalStatuse> maritalStatuses) throws JSONException {
        marital_status_drop_down.add(ctx.getResources().getString(R.string.please_select_marital_status));
        marital_status_id.add("0");

        if (maritalStatuses.size() > 0) {
            for (int i = 0; i < maritalStatuses.size(); i++) {
                marital_status_drop_down.add(maritalStatuses.get(i).getName());
                marital_status_id.add(String.valueOf(maritalStatuses.get(i).getId()));
                msMaritalStatus.setItems(marital_status_drop_down);
            }
        }

    }

    public void setInitGender(List<Gender> data) throws JSONException {
        gender_drop_down.add(ctx.getResources().getString(R.string.please_select_gender));
        gender_id.add("0");

        if (data.size() > 0) {
            for (int i = 0; i < data.size(); i++) {
                gender_drop_down.add(data.get(i).getName());
                gender_id.add(String.valueOf(data.get(i).getId()));
                msGender.setItems(gender_drop_down);

            }
        }

    }

    public void setData(User data) {
        marital = data.getDetail() != null ? String.valueOf(data.getDetail().getMarital_status()) : "";
        gender = data.getDetail() != null ? String.valueOf(data.getDetail().getGender()) : "";

        if (marital_status_id.contains(marital) || gender_id.contains(gender)) {
            marital_status_pos = marital_status_id.indexOf(marital);
            gender_pos = gender_id.indexOf(gender);
            init();

        }

        data.getName();
        first = data.getName();
        data.getOther_names();
        second = data.getOther_names();
        data.getDetail();
        dob = data.getDetail().getDob();
        populate = true;
    }

    public void setMsMaritalStatus() {
        userProfileViewModel.getUserMaritalStatus();

        userProfileViewModel.getUserGender();

        userProfileViewModel.userMaritalResponseLiveData().observe(getViewLifecycleOwner(),
                response -> {
                    try {
                        setInitData(response);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                });

        userProfileViewModel.userGenderResponseLiveData().observe(getViewLifecycleOwner(),
                response -> {
                    try {
                        setInitGender(response);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                });
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.eTxDateOfBirth:
                DialogFragment dialogfragment = new DatePickerDialogTheme();

                dialogfragment.show(getFragmentManager(), "Date Picker");
                break;
        }
    }

    public static class DatePickerDialogTheme extends DialogFragment implements DatePickerDialog.OnDateSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final Calendar calendar = Calendar.getInstance();
            int year = calendar.get(Calendar.YEAR);
            int month = calendar.get(Calendar.MONTH);
            int day = calendar.get(Calendar.DAY_OF_MONTH);
            DatePickerDialog datepickerdialog = new DatePickerDialog(getActivity(),
                    AlertDialog.THEME_HOLO_LIGHT, this, year - 18, month, day);
            //following line to restrict future date selection
            datepickerdialog.getDatePicker().setMaxDate(System.currentTimeMillis());
            datepickerdialog.show();

            return datepickerdialog;
        }

        public void onDateSet(DatePicker view, int year, int month, int day) {
            EditText editText = getActivity().findViewById(R.id.eTxDateOfBirth);
            String string = String.valueOf(day) + "-" + String.valueOf(month + 1) + "-" + String.valueOf(year);
            editText.setText(string);
            try {
                Date date = new SimpleDateFormat("dd-MM-yyyy").parse(string);
                final Calendar datePickerCal = Calendar.getInstance();
                datePickerCal.set(year, month, day);
                dob = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).format(datePickerCal.getTime());
            } catch (ParseException e) {
                e.printStackTrace();
            }

        }
    }


    public String getData() {
        if (created) {
            String data = "\"name\":" + '"' + inputs.get(0).getText().toString() + '"' +
                    ", \"other_names\": " + '"' + inputs.get(1).getText().toString() + '"' +
                    ", \"dob\": " + '"' + dob + '"' +
                    ", \"gender\": " + '"' + gender_id.get(msGender.getSelectedIndex()) + '"' +
                    ", \"marital_status\": " + '"' + marital_status_id.get(msMaritalStatus.getSelectedIndex()) + '"';
            return data;
        } else {
            return "";
        }
    }


}
