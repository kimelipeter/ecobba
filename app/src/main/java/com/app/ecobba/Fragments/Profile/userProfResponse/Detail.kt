package com.app.ecobba.Fragments.Profile.userProfResponse

data class Detail(
    val address: String,
    val branch_id: String,
    val city: String,
    val country: Int,
    val created_at: String,
    val dob: String,
    val doc_no: String,
    val doc_type: Int,
    val gender: Int,
    val group_id: String,
    val id: Int,
    val income: Int,
    val marital_status: Int,
    val occupation: String,
    val org_id: String,
    val postal_code: String,
    val residence: Int,
    val state: String,
    val updated_at: String,
    val user_id: Int
)