
package com.app.ecobba.Fragments.Group.GroupModel;


@lombok.Data
@SuppressWarnings("unused")
public class CreateGroupModel {

    private Data data;
    private String message;
    private Boolean success;

}
