package com.app.ecobba.Fragments.Group.Meeting;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.app.ecobba.R;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;

public class MeetingsDashboardFragment  extends Fragment {


    public MeetingsDashboardFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_meetings_dashboard,container,false);


        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        MeetingsDashboardViewPager adapter = new MeetingsDashboardViewPager(getChildFragmentManager(),getContext());
        ArrayList<Fragment> fragments = new ArrayList<>();
        fragments.add(new MeetingsFragment());
        fragments.add(new MeetingsFragment());
        adapter.setFragments(fragments);
        ViewPager viewPager = view.findViewById(R.id.pager2);
        viewPager.setAdapter(adapter);
        TabLayout tabLayout = view.findViewById(R.id.tablayout2);
        tabLayout.setupWithViewPager(viewPager);
    }
}


