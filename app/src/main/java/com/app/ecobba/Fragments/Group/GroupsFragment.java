package com.app.ecobba.Fragments.Group;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.app.ecobba.Models.Groups.groupsResponse;
import com.app.ecobba.R;
import com.app.ecobba.common.SharedMethods;
import com.app.ecobba.common.rxApi.Api;
import com.app.ecobba.common.rxApi.RxApi;
import com.app.ecobba.recyclerview_adapters.GroupMembersRecyclerAdapter;
import com.app.ecobba.recyclerview_adapters.GroupsListRecycler;
import com.app.ecobba.recyclerview_models.GroupsListAdapter;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class GroupsFragment extends Fragment implements
        View.OnClickListener,
        AppBarLayout.OnOffsetChangedListener {
    GroupsListRecycler adapter;
    Context context;
    Fragment fragment;
    RecyclerView recyclerView;
    @BindView(R.id.tvNoAvailableData)
    TextView tvNoAvailableData;

    @BindView(R.id.ablProfile)
    AppBarLayout ablProfile;

    @BindView(R.id.create)
    FloatingActionButton create;
    SwipeRefreshLayout mSwipeRefreshLayout;
    private Api api;
    ProgressDialog progressDialog;

    public GroupsFragment() {

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_groups, container, false);
        api = new RxApi().getApi();
        recyclerView = view.findViewById(R.id.recyclerView);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        context = getActivity();
        progressDialog = new ProgressDialog(context);
        fragment = this;
        mSwipeRefreshLayout = view.findViewById(R.id.swipe_container);
        adapter = new GroupsListRecycler(new ArrayList<>(), requireContext(), fragment);
        RecyclerView.LayoutManager layoutManager1 = new LinearLayoutManager(requireContext());
        recyclerView.setLayoutManager(layoutManager1);
        recyclerView.setAdapter(adapter);
        create.setOnClickListener(this);
        ablProfile.addOnOffsetChangedListener(this);
        fetchGroupDetails();
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                Toast.makeText(context, "Refreshing", Toast.LENGTH_LONG).show();
                fetchGroupDetails();
                mSwipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary,
                        android.R.color.holo_green_dark,
                        android.R.color.holo_orange_dark,
                        android.R.color.holo_blue_dark);

                mSwipeRefreshLayout.setRefreshing(false);
            }
        });

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.create:
                NavHostFragment.findNavController(this).navigate(R.id.groupCreateFragment);
                break;
            case R.id.card:
                NavHostFragment.findNavController(this).navigate(R.id.groupFragment);
                break;
            case R.id.viewcard:
                NavHostFragment.findNavController(this).navigate(R.id.groupFragment);
                break;
        }
    }

    private void fetchGroupDetails() {
        progressDialog.setMessage("Loading.Please wait....");
        progressDialog.show();
        Call<groupsResponse> call = api.getUserGroups();
        call.enqueue(new Callback<groupsResponse>() {
            @Override
            public void onResponse(Call<groupsResponse> call, Response<groupsResponse> response) {
                try {
                    if (response.body().getSuccess()) {
                        progressDialog.dismiss();
                        if (response.body().getGroups().size() > 0) {
                            adapter.datalist = response.body().getGroups();
                            adapter.notifyDataSetChanged();
                        } else {
                            tvNoAvailableData.setText(getResources().getString(R.string.no_available_data));
                            tvNoAvailableData.setVisibility(View.VISIBLE);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<groupsResponse> call, Throwable t) {

            }
        });
    }


    int val = 0;
    boolean transparent = true;

    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int i) {

        if (i < val) {
            if (transparent) {
                ((View) getActivity().findViewById(R.id.toolbar)).setBackgroundResource(R.color.colorSurface);
                transparent = false;
                create.hide();
            }
        } else if (i > val) {

            if (!transparent) {
                ((View) getActivity().findViewById(R.id.toolbar)).setBackgroundResource(R.color.colorSurfaceAlpha);
                transparent = true;
                create.show();
            }
        }

        val = i;
    }


}
