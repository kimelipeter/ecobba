package com.app.ecobba.Fragments.chat.data

data class MessageRequest(
    val inbox: String = "",
    val outbox: String = ""
)