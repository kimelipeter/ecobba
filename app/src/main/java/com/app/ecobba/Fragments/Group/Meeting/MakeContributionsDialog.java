package com.app.ecobba.Fragments.Group.Meeting;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Binder;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.app.ecobba.R;
import com.jaredrummler.materialspinner.MaterialSpinner;

import butterknife.ButterKnife;
import butterknife.Unbinder;

public class MakeContributionsDialog extends DialogFragment implements
        View.OnClickListener, MaterialSpinner.OnItemSelectedListener<String> {
      private Unbinder unbinder;
         Context ctx;

    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.make_contribution_dialog, null);
        ctx = getActivity();

//        close = view.findViewById(R.id.close);
        unbinder = ButterKnife.bind(this, view);

//        editTextMsdn = view.findViewById(R.id.editTextMsdn);
//        editTextMsdn.setText(phonrnumber);
//        spinner = view.findViewById(R.id.payMethod);
//        provider=view.findViewById(R.id.provider);
//        deposit.setOnClickListener(this);
//        currency.setOnItemSelectedListener(this);
//        popupColor = new ColorDrawable(getResources().getColor(R.color.colorSurface));
//        currency.getPopupWindow().setBackgroundDrawable(popupColor);
//
//        close.setOnClickListener(v -> dismiss());
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(view);


        return builder.create();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public void onItemSelected(MaterialSpinner view, int position, long id, String item) {

    }
}
