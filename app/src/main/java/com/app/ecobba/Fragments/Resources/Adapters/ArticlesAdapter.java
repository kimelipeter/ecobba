package com.app.ecobba.Fragments.Resources.Adapters;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.VideoView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.app.ecobba.Fragments.Resources.ResourceActivity;
import com.app.ecobba.Models.Resources.Articles.Data;
import com.app.ecobba.R;

import com.snov.timeagolibrary.PrettyTimeAgo;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import de.hdodenhof.circleimageview.CircleImageView;


public class ArticlesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public List<Data> dataList;
    public Context context;
    public Fragment fragment;
    int imageView = 1;
    int videoView = 2;
    private MediaController ctlr;
    long datepower;
    private ProgressDialog bar;

    public ArticlesAdapter(List<Data> dataList, Context context, Fragment fragment) {
        this.dataList = dataList;
        this.context = context;
        this.fragment = fragment;
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        LayoutInflater mInflater = LayoutInflater.from(context);

        if (viewType == imageView) {
            view = mInflater.inflate(R.layout.article_list_item, parent, false);
            return new ArticlesAdapter.ImageViewHolder(view);
        } else {
            view = mInflater.inflate(R.layout.article_list_item_video, parent, false);
            return new ArticlesAdapter.VideoViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        Data data = dataList.get(position);

        if (holder.getItemViewType() == imageView) {
            ArticlesAdapter.ImageViewHolder imageViewHolder = (ArticlesAdapter.ImageViewHolder) holder;
            imageViewHolder.bind(data);
        } else {
            ArticlesAdapter.VideoViewHolder videoViewHolder = (ArticlesAdapter.VideoViewHolder) holder;
            videoViewHolder.bind(data);
        }

    }


    @Override
    public int getItemCount() {
        return dataList.size();
    }

    @Override
    public int getItemViewType(int position) {
        Data data = dataList.get(position);

        if (data.getVideourl() == null) {
            return imageView;
        } else {
            return videoView;
        }
    }

    public class ImageViewHolder extends RecyclerView.ViewHolder {
        TextView tv_productname;
        TextView tv_category;
        TextView tvName;
        TextView tvTitle;
        TextView tvDate;
        ImageView product_img;
        CircleImageView profile_image;
        TextView tvDescription;
        LinearLayout action_shops;
        androidx.constraintlayout.widget.ConstraintLayout view_container;

        public ImageViewHolder(View itemView) {
            super(itemView);
            action_shops = itemView.findViewById(R.id.action_shops);
            view_container = itemView.findViewById(R.id.shop_container);
            tv_productname = itemView.findViewById(R.id.productname);
            tv_category = itemView.findViewById(R.id.category);
            tvName = itemView.findViewById(R.id.tvName);
            product_img = itemView.findViewById(R.id.thumbnail_image);
            profile_image = itemView.findViewById(R.id.profile_image);
            tvDescription = itemView.findViewById(R.id.tvDescription);
            tvDate = itemView.findViewById(R.id.tvDate);
            tvTitle = itemView.findViewById(R.id.tvTitle);
        }

        public void bind(Data data) {
            String owner_name=data.getUser().getName() + " " + data.getUser().getOther_names();
            String content=data.getContent().replace("<p>","").replace("</p>","");
            action_shops.setAnimation(AnimationUtils.loadAnimation(context, R.anim.fade_transition_animation));
            tv_productname.setText(data.getName());
            tv_category.setText(data.getCategory().toUpperCase());
            tvDescription.setText(content);
            tvName.setText(owner_name);
            tvTitle.setText(data.getName());
            String date_posted = data.getCreated_at();
            Log.e("DATE_POSTED_IS", date_posted);

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.ENGLISH);
            sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
            Date testDate = null;
            try {
                testDate = sdf.parse(date_posted);
            } catch (ParseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            Log.e("BEg", String.valueOf(datepower));
            datepower = testDate.getTime();
            Log.e("Datw", String.valueOf(+datepower));
            PrettyTimeAgo timeAgo = new PrettyTimeAgo();
            String result = timeAgo.getTimeAgo(datepower);
            tvDate.setText(result);
            String service_image_url = data.getImageurl();

            //  load image from the internet using Picasso
            String owner_profile = data.getUser().getAvatar();
            Picasso.get().load(owner_profile).fit().centerCrop().into(profile_image);

            Log.e("DATA_IMAGE_ARICLES", service_image_url);

            String product_id;
            product_id = String.valueOf(data.getId());
            Log.e("product_id_is", product_id);

            Picasso.get()
                    .load(new File(service_image_url))
                    .placeholder(R.drawable.background_gradient_black)

                    .into(product_img);
            Picasso.get()
                    .load(service_image_url)
                    .placeholder(R.drawable.background_gradient_black)
                    .into(product_img);

            itemView.setOnClickListener(v -> {
                Intent intent = new Intent(context, ResourceActivity.class);
                intent.putExtra("img", service_image_url);
                intent.putExtra("productname", data.getName());
                intent.putExtra("content", content);
                intent.putExtra("user_id", data.getUser_id());
                intent.putExtra("category", data.getCategory());
                intent.putExtra("owner_name", owner_name);
                intent.putExtra("owner_profile", owner_profile);
                intent.putExtra("result", result);
                context.startActivity(intent);

            });


        }
    }

    public class VideoViewHolder extends RecyclerView.ViewHolder {
        TextView tv_productname;
        TextView tv_category;
        TextView tvDescription;
        TextView tvDate;
        TextView tvName;
        TextView tvTitle;
        VideoView videoView;
        CircleImageView profile_image;
        LinearLayout action_shops;
        androidx.constraintlayout.widget.ConstraintLayout view_container;

        public VideoViewHolder(View itemView) {
            super(itemView);
            action_shops = itemView.findViewById(R.id.action_shops);
            view_container = itemView.findViewById(R.id.shop_container);
            tv_productname = itemView.findViewById(R.id.productname);
            tv_category = itemView.findViewById(R.id.category);
            videoView = itemView.findViewById(R.id.thumbnail_image);
            tvDescription = itemView.findViewById(R.id.tvDescription);
            tvDate = itemView.findViewById(R.id.tvDate);
            tvName = itemView.findViewById(R.id.tvName);
            tvTitle = itemView.findViewById(R.id.tvTitle);
            profile_image = itemView.findViewById(R.id.profile_image);
        }

        public void bind(Data data) {
            bar = new ProgressDialog(context);
            bar.setTitle("Connecting server");
            bar.setMessage("Please Wait... ");
            bar.setCancelable(false);
            bar.show();
            String owner_name=data.getUser().getName() + " " + data.getUser().getOther_names();
            String content=data.getContent().replace("<p>","").replace("</p>","");
            action_shops.setAnimation(AnimationUtils.loadAnimation(context, R.anim.fade_transition_animation));
            tv_productname.setText(data.getName());
            tvTitle.setText(data.getName());
            tv_category.setText(data.getCategory().toUpperCase());
            tvDescription.setText(content);
            tvName.setText(owner_name);
            //  load image from the internet using Picasso
            String owner_profile = data.getUser().getAvatar();
            Picasso.get().load(owner_profile).fit().centerCrop().into(profile_image);
            //String path_url = "https://commondatastorage.googleapis.com/gtv-videos-bucket/sample/Sintel.mp4";
            String path_url = data.getVideourl();
            Log.e("DATA_VIDEO_URL", path_url);

            String product_id;
            product_id = String.valueOf(data.getId());
            Log.e("product_id_is", product_id);
            String date_posted = data.getCreated_at();
            Log.e("DATE_POSTED_IS", date_posted);

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.ENGLISH);
            sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
            Date testDate = null;
            try {
                testDate = sdf.parse(date_posted);
            } catch (ParseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            Log.e("BEg", String.valueOf(datepower));
            datepower = testDate.getTime();
            Log.e("Datw", String.valueOf(+datepower));
            PrettyTimeAgo timeAgo = new PrettyTimeAgo();
            String result = timeAgo.getTimeAgo(datepower);
            tvDate.setText(result);

            if (bar.isShowing()) {
                videoView.setVideoURI(Uri.parse(path_url)); //the string of the URL mentioned above
                videoView.start();
                ctlr = new MediaController(context);
                ctlr.setMediaPlayer(videoView);
                videoView.setMediaController(ctlr);
                videoView.requestFocus();
            }
            bar.dismiss();
//            itemView.setOnClickListener(v -> {
//                pass data here
//            });

        }
    }


}