package com.app.ecobba.Fragments.Profile.Avatar;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.util.Pair;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.AppCompatButton;
import androidx.core.content.FileProvider;
import androidx.lifecycle.ViewModelProvider;

import com.app.ecobba.BuildConfig;
import com.app.ecobba.Fragments.Shop.MyKweaProducts.CreateNewItemActivity;
import com.app.ecobba.Models.UserProfile.Detail;
import com.app.ecobba.R;
import com.app.ecobba.ViewModel.UserProfileViewModel;
import com.app.ecobba.common.rxApi.Api;
import com.app.ecobba.common.rxApi.RxApi;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.android.material.snackbar.Snackbar;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.mikhaellopez.circularimageview.CircularImageView;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;

public class UpdateAvatarDialog extends BottomSheetDialogFragment {
    CircularImageView circularImageView;
    ImageView close;
    AppCompatButton btUpdate;
    File filetoupload = null;

    UserProfileViewModel userProfileViewModel;

    CircularImageView user_prof;
    static final int REQUEST_TAKE_PHOTO = 101;
    static final int REQUEST_GALLERY_PHOTO = 102;
    File mPhotoFile;

    Context ctx;
    private Api api;
    ProgressDialog progressDialog;
    TextView tvChangeAvatar;


    public UpdateAvatarDialog() {
    }

    @Override
    public int getTheme() {
        return R.style.AppBottomSheetDialogTheme;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.update_avatar_bottom_sheet, container, false);
        close = view.findViewById(R.id.close);
        user_prof = view.findViewById(R.id.user_prof);
        tvChangeAvatar = view.findViewById(R.id.tvChangeAvatar);
        setStyle(STYLE_NORMAL, R.style.AppBottomSheetDialogTheme);
        userProfileViewModel = new ViewModelProvider(getActivity()).get(UserProfileViewModel.class);
        user_prof.setOnClickListener(v -> {
            selectImage();
        });
        api = new RxApi().getApi();
        btUpdate = view.findViewById(R.id.btUpdate);
        btUpdate.setOnClickListener(v -> {
            updateAvatar();
        });
        return view;
    }

    private void updateAvatar() {
        progressDialog = ProgressDialog.show(getActivity(), "", "Please Wait...", true);
        RequestBody requestBody = RequestBody.create(MediaType.parse("image/*"), filetoupload);
        MultipartBody.Part body = MultipartBody.Part.createFormData("avatar", filetoupload.getName(), requestBody);

        Call<ResponseBody> call = api.postChangeAvatar(body);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                if (response.isSuccessful()) {
                    dismiss();
                }
                progressDialog.dismiss();

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                t.printStackTrace();
                progressDialog.dismiss();
                Log.e("ERROR_MESSAGE", t.getLocalizedMessage());

            }
        });
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        close.setOnClickListener(v -> {
            dismiss();
        });
        userData();
    }


    public void userData() {
        userProfileViewModel.getUserProfile();
        userProfileViewModel.userProfileResponseLiveData().observe(getViewLifecycleOwner(), userProfileResponse -> {
            if (userProfileResponse.getSuccess()) {
                Detail userDetails = userProfileResponse.getUser().getDetail();
                String imageUri = userProfileResponse.getUser().getAvatar();
                Log.e("USER_AVATAR", imageUri);

                Glide.with(requireContext()).load(imageUri).centerCrop().into(user_prof);

            }
        });
    }


    private void selectImage() {
        final CharSequence[] items = {
                //"Take Photo",
                "Choose from Gallery",
                "Cancel"
        };
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setItems(items, (dialog, item) -> {
            if (items[item].equals("Choose from Gallery")) {
                requestStoragePermission(false);
            } else if (items[item].equals("Cancel")) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

    private void requestStoragePermission(boolean isCamera) {
        Dexter.withActivity(getActivity())
                .withPermissions(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        // check if all permissions are granted
                        if (report.areAllPermissionsGranted()) {
                            if (isCamera) {
                                startCamera();
                            } else {
                                chooseGallery();
                            }
                        }
                        // check for permanent denial of any permission
                        if (report.isAnyPermissionPermanentlyDenied()) {
                            // show alert dialog navigating to Settings
                            chooseGallery();
                        }
                    }

                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }

                })
                .withErrorListener(
                        error -> Toast.makeText(ctx, "Error occurred! ", Toast.LENGTH_SHORT)
                                .show())
                .onSameThread()
                .check();
    }

    private void chooseGallery() {
        Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        pickPhoto.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        startActivityForResult(pickPhoto, REQUEST_GALLERY_PHOTO);

    }

    private void startCamera() {
        mPhotoFile = newFile();
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
            if (mPhotoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(ctx,
                        BuildConfig.APPLICATION_ID + ".provider", mPhotoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
            }
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_TAKE_PHOTO) {
                // tvSelectedFilePath.setText(mPhotoFile.getAbsolutePath());
                Log.e("CAMERA_PIC_PATH", mPhotoFile.getAbsolutePath());
                Glide.with(getActivity())
                        .load(mPhotoFile)
                        .apply(new RequestOptions().centerCrop().circleCrop())
                        .into(user_prof);
            } else if (requestCode == REQUEST_GALLERY_PHOTO) {
                Uri selectedImage = data.getData();
                // tvSelectedFilePath.setText(getRealPathFromUri(selectedImage));
                Log.e("PIC_PATH", getRealPathFromUri(selectedImage));
                filetoupload = new File(getRealPathFromUri(selectedImage));
                Glide.with(getActivity())
                        .load(getRealPathFromUri(selectedImage))
                        .apply(new RequestOptions().centerCrop().circleCrop())
                        .into(user_prof);
            }
        }
    }

    public File newFile() {
        Calendar cal = Calendar.getInstance();
        long timeInMillis = cal.getTimeInMillis();
        String mFileName = String.valueOf(timeInMillis) + ".jpeg";
        File mFilePath = getFilePath();
        try {
            File newFile = new File(mFilePath.getAbsolutePath(), mFileName);
            newFile.createNewFile();
            return newFile;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public File getFilePath() {
        return getActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES);

    }


    public String getRealPathFromUri(Uri contentUri) {
        Cursor cursor = null;
        try {
            String[] proj = {MediaStore.Images.Media.DATA};
            cursor = getActivity().getContentResolver().query(contentUri, proj, null, null, null);
            assert cursor != null;
            int columnIndex = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(columnIndex);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }
}
