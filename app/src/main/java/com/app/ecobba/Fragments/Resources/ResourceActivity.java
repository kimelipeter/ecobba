package com.app.ecobba.Fragments.Resources;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.ecobba.R;
import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;

public class ResourceActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_resource);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_baseline_arrow_back_24);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        //Recieve Data
        String img = getIntent().getExtras().getString("img");
        String productname = getIntent().getExtras().getString("productname");
        String content = getIntent().getExtras().getString("content");
        String user_id = getIntent().getExtras().getString("user_id");
        String category = getIntent().getExtras().getString("category");
        String owner_profile = getIntent().getExtras().getString("owner_profile");
        String owner_name = getIntent().getExtras().getString("owner_name");
        String result = getIntent().getExtras().getString("result");
        Log.e("DATA_RECIEVED", img + productname + content);


        //ini views
        TextView tv_name = findViewById(R.id.productname);
        TextView tv_content = findViewById(R.id.content);
        TextView tv_category = findViewById(R.id.category);
        TextView tv_owner_name = findViewById(R.id.owner_name);
        ImageView imageView = findViewById(R.id.img);
        ImageView imageView1 = findViewById(R.id.profile_image);

        // setting values to each view
        tv_name.setText(productname);
        tv_content.setText(content);
        tv_category.setText(category.toUpperCase());
        // tv_owner_name.setText(owner_name);
        tv_owner_name.setText("Posted " + " " + result + "  by " + owner_name);

        Glide.with(this)
                .load(img)
                .error(R.drawable.ic_shop).override(200, 250)
                .fitCenter().into(imageView);
        Picasso.get().load(owner_profile).fit().centerCrop().into(imageView1);
    }
}