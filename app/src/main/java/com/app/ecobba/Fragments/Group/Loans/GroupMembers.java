package com.app.ecobba.Fragments.Group.Loans;

public class GroupMembers {

    private String username;
    private String phonenumber;


    public GroupMembers(String username, String phonenumber) {
        this.username = username;
        this.phonenumber = phonenumber;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPhonenumber() {
        return phonenumber;
    }

    public void setPhonenumber(String phonenumber) {
        this.phonenumber = phonenumber;
    }
}
