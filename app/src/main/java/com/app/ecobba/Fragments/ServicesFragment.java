package com.app.ecobba.Fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.app.ecobba.R;
import com.google.android.material.appbar.AppBarLayout;

import java.util.List;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;

public class ServicesFragment extends Fragment implements
        AppBarLayout.OnOffsetChangedListener,
        View.OnClickListener{


    @BindView(R.id.ablProfile)
    AppBarLayout ablProfile;


    List<ImageView> arrows;


    List<TextView> info;

    @BindViews({

            R.id.btnUtilities,

    })
    List<Button> buttons;

    @BindView(R.id.Utilities)
    LinearLayout utilities;

    @BindView(R.id.Resources)
    LinearLayout resources;

    public ServicesFragment(){}

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View frag = inflater.inflate(R.layout.fragment_services,container,false);
        ButterKnife.bind(this,frag);
        ablProfile.addOnOffsetChangedListener(this);
        for (Button b: buttons){
            b.setOnClickListener(this);
        }
        return frag;
    }

    int val = 0;
    boolean transparent = true;

    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int i) {

        if (i<val){
            if (transparent) {
                ((View) getActivity().findViewById(R.id.toolbar)).setBackgroundResource(R.color.colorSurface);
                transparent = false;
            }
        }else if(i > val){

            if (!transparent) {
                ((View) getActivity().findViewById(R.id.toolbar)).setBackgroundResource(R.color.colorSurfaceAlpha);
                transparent = true;
            }
        }

        val = i;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){

            case R.id.btnUtilities:
                utilities.setVisibility(View.VISIBLE);
                resources.setVisibility(View.GONE);
                break;

        }
    }

    private void showInfo(int n){
        int _n=0;
        for (TextView text:info){
            _n++;
            if (_n==n){
                text.setVisibility(View.VISIBLE);
            }else {
                text.setVisibility(View.GONE);
            }
        }
    }

}
