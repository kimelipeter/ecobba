package com.app.ecobba.Fragments.Group.Settings.Adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.app.ecobba.Models.MeetingsSettingsFines.Data;
import com.app.ecobba.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

public class FinesSettingsAdapter extends RecyclerView.Adapter<FinesSettingsAdapter.ViewHolder> {
    public List<Data> datalist;
    public Context context;
    public Fragment fragment;
    public String user_currency;

    public FinesSettingsAdapter(List<Data> datalist, Context context, Fragment fragment,String user_currency) {
        this.datalist = datalist;
        this.context = context;
        this.fragment = fragment;
        this.user_currency=user_currency;
    }

    @NonNull
    @Override
    public FinesSettingsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.list_item_fine, parent, false);
        return new FinesSettingsAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull FinesSettingsAdapter.ViewHolder holder, int position) {
        String fine_name=datalist.get(position).getName();
        String fine_amount=datalist.get(position).getAmount();
        String dateString=datalist.get(position).getSource().getCreated_at();
        holder.name.setText(fine_name);
        holder.amount.setText(user_currency+" "+fine_amount);

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        sdf.setTimeZone(TimeZone.getTimeZone("GMT"));

        try {
            Date date1 = sdf.parse(dateString);
            sdf = new SimpleDateFormat("dd MMMM yyyy");
            String date = sdf.format(date1);

            sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");

            Date time = sdf.parse(dateString);
            sdf = new SimpleDateFormat("h:mm a");

            String time1 = sdf.format(time);

            Log.e("DateAndTime", "Date " + date + " Time " + time1);
//            holder.tvTimeDifference.setText(time1);
            holder.tvDateCreated.setText(date);
        } catch (ParseException e) {
            Log.e("DateAndTime", e.getLocalizedMessage());
            e.printStackTrace();
        }


    }

    @Override
    public int getItemCount() {
        return datalist.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView name,amount,tvDateCreated;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.tvFineName);
            amount = itemView.findViewById(R.id.tvFineAmount);
            tvDateCreated = itemView.findViewById(R.id.tvDateCreated);
        }
    }
}
