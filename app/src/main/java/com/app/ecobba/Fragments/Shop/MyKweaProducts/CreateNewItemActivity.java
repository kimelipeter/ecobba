package com.app.ecobba.Fragments.Shop.MyKweaProducts;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.navigation.fragment.NavHostFragment;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.app.ecobba.Fragments.Shop.AddShopFragment;
import com.app.ecobba.R;
import com.app.ecobba.common.ApisKtKt;
import com.app.ecobba.common.SharedMethods;
import com.app.ecobba.common.rxApi.Api;
import com.app.ecobba.common.rxApi.RxApi;
import com.bumptech.glide.Glide;
import com.google.gson.JsonObject;
import com.jaredrummler.materialspinner.MaterialSpinner;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.ProgressCallback;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.mayanknagwanshi.imagepicker.ImageSelectActivity;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.app.ecobba.common.AppConstantsKt.INITIATOR_ADD_SHOP;
import static com.app.ecobba.common.AppConstantsKt.INITIATOR_CREATE_KWEA_SHOP_ITEM;
import static com.app.ecobba.common.SharedMethods.HTTP_POST;
import static com.app.ecobba.common.SharedMethods.global_error_message;
import static com.app.ecobba.common.SharedMethods.postToServer;
import static com.app.ecobba.common.SharedMethods.runOnUI;
import static com.app.ecobba.common.SharedMethods.showAlertAndMoveToPage;
import static com.app.ecobba.common.SharedMethods.showSweetAlertDialogError;
import static com.app.ecobba.common.SharedMethods.showSweetAlertDialogSuccess;

public class CreateNewItemActivity extends AppCompatActivity implements
        MaterialSpinner.OnItemSelectedListener, View.OnClickListener,

        CompoundButton.OnCheckedChangeListener {
    EditText item_name, item_price, item_description;
    Spinner currency, category;
    String item_currency, item_category;
    ImageView uploadImage;
    TextView choseImage;
    private String _item_name, _item_price, _item_description, _item_category, _item_currency;
    private String token = "";
    private Context ctx;
    String shop_id_number;
    ProgressBar uploadProgressBar;
    Bitmap selectedImage;
    ProgressBar spinner;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    private ArrayList<String> arrayList = new ArrayList<String>();
    private String filePath1;
    File fileToUpload;

    @BindView(R.id.create)
    Button create;

    @BindView(R.id.imDocument)
    ImageView ivDocument;

    ImageView imageViewUpload;
    public ProgressDialog progressDialog;


    int MY_PERMISSIONS_REQUEST_CAMERA = 0;
    private static final int PICK_FROM_GALLERY = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_new_item);
        setupToolbar();
//        ////start of camera access persmission
//        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
//                == PackageManager.PERMISSION_DENIED)
//
//
//            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, MY_PERMISSIONS_REQUEST_CAMERA);
//
//        ////end of camera access persmission

        try {
            if (ActivityCompat.checkSelfPermission(CreateNewItemActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(CreateNewItemActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, PICK_FROM_GALLERY);
            } else {
                Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(galleryIntent, PICK_FROM_GALLERY);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        ButterKnife.bind(this);
        token = SharedMethods.getDefaults("token", this);
        shop_id_number = getIntent().getExtras().getString("shop_id");
        Log.e("shop_id_number_is", shop_id_number);
        create = (Button) findViewById(R.id.create);
        item_name = findViewById(R.id.item_name);
        uploadImage = findViewById(R.id.post_image);
        item_price = findViewById(R.id.item_price);
        item_description = findViewById(R.id.item_description);
        choseImage = findViewById(R.id.choose_image_text);
        currency = (Spinner) findViewById(R.id.item_currency);
        currency.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                Toast.makeText(ctx, "This is " +
                        adapterView.getItemAtPosition(i).toString(), Toast.LENGTH_LONG).show();
                item_currency= adapterView.getItemAtPosition(i).toString();

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        category = (Spinner) findViewById(R.id.item_category);
        category.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                Toast.makeText(ctx, "This is " +
                        adapterView.getItemAtPosition(i).toString(), Toast.LENGTH_LONG).show();
                item_category= adapterView.getItemAtPosition(i).toString();

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        imageViewUpload = findViewById(R.id.imDocument);


        spinner = (ProgressBar) findViewById(R.id.progressBar);
        spinner.setVisibility(View.GONE);
        choseImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//
//                Intent intent = new Intent(CreateNewItemActivity.this, ImageSelectActivity.class);
//                intent.putExtra(ImageSelectActivity.FLAG_COMPRESS, false);//default is true
//                intent.putExtra(ImageSelectActivity.FLAG_CAMERA, true);//default is true
//                intent.putExtra(ImageSelectActivity.FLAG_GALLERY, true);//default is true
//                startActivityForResult(intent, 1213);

                Intent intent = new Intent();
                intent.setType("image/*");
                intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent,"Select Picture"), 1213);


            }
        });
        imageViewUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//
//                Intent intent = new Intent(CreateNewItemActivity.this, ImageSelectActivity.class);
//                intent.putExtra(ImageSelectActivity.FLAG_COMPRESS, false);//default is true
//                intent.putExtra(ImageSelectActivity.FLAG_CAMERA, false);//default is true
//                intent.putExtra(ImageSelectActivity.FLAG_GALLERY, true);//default is true
//                // intent.putExtra(ImageSelectActivity.M)
//                startActivityForResult(intent, 1213);
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent,"Select Picture"), 1213);


            }
        });
        ctx = getApplicationContext();
        token = SharedMethods.getDefaults("token", ctx);
        ButterKnife.bind(this);
        create.setOnClickListener(this);

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case PICK_FROM_GALLERY:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(galleryIntent, PICK_FROM_GALLERY);
                } else {
                    //do something like displaying a message that he didn`t allow the app to access gallery and you wont be able to let him select from gallery
                    Toast toast = Toast.makeText(CreateNewItemActivity.this, "You didn`t allow the app to access gallery and you wont be able to select from gallery", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                }
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

//        if (requestCode == 1213 && resultCode == Activity.RESULT_OK) {
//          // filePath1 = data.getStringExtra(ImageSelectActivity.RESULT_FILE_PATH);
//            //selectedImage = BitmapFactory.decodeFile(filePath1);
////            Log.e("image_path_is", filePath1);
//  //          fileToUpload = new File(filePath1);
//    //        arrayList.add(filePath1);
//            String result=data.getStringExtra("result");
//            Log.e("asdfghjkljhgsdfghj",result);

        if (requestCode == 1213 && resultCode == RESULT_OK  && data != null){

            arrayList = new ArrayList<>();

            if(data.getClipData() != null){

                int count = data.getClipData().getItemCount();
                for (int i=0; i<count; i++){

                    Uri imageUri = data.getClipData().getItemAt(i).getUri();
                    getImageFilePath(imageUri);

                }
            }
            else if(data.getData() != null){


                Uri imgUri = data.getData();
                getImageFilePath(imgUri);
            }

        }



    }

    public void getImageFilePath(Uri imgUri) {

        File file = new File(imgUri.getPath());
        String[] filePath = file.getPath().split(":");
        String image_id = filePath[filePath.length - 1];

        Cursor cursor = getContentResolver().query(android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI, null, MediaStore.Images.Media._ID + " = ? ", new String[]{image_id}, null);
        if (cursor!=null) {
            cursor.moveToFirst();
         String   imagePath = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));
            arrayList.add(imagePath);
            Log.e("uthembeni",imagePath);
            Glide.with(ctx).load(arrayList.get(0)).into(ivDocument);
            cursor.close();
        }
    }

    private boolean validateFormFields() {
        boolean valid = true;
        _item_name = item_name.getText().toString().trim();
        _item_price = item_price.getText().toString().trim();
        _item_description = item_description.getText().toString().trim();
        _item_currency = item_currency.trim();
        _item_category = item_category.trim();


        if (_item_name.isEmpty()) {
            item_name.setError(getString(R.string.emptyField));
            valid = false;
        }

        if (_item_price.isEmpty()) {
            item_price.setError(getString(R.string.emptyField));
            valid = false;
        }
        if (_item_description.isEmpty()) {
            item_description.setError(getString(R.string.emptyField));
            valid = false;
        }


        return valid;
    }

    private void uploadShopItem() {

        this.progressDialog = ProgressDialog.show(this, null, getString(R.string.progress_login));
        MultipartBody.Part[] body = new MultipartBody.Part[arrayList.size()];

        for (int i = 0; i < arrayList.size(); i++) {
            File files = new File(arrayList.get(i));
            Log.e("sdfghjk", files.getAbsolutePath());
            RequestBody requestfiles = RequestBody.create(MediaType.parse("image/*"), files);
            body[i] = MultipartBody.Part.createFormData("item_images[]", files.getName(), requestfiles);

        }

        RequestBody itemname = RequestBody.create(MultipartBody.FORM, _item_name);
        RequestBody item_category = RequestBody.create(MultipartBody.FORM, _item_category);
        RequestBody item_price = RequestBody.create(MultipartBody.FORM, _item_price);
        RequestBody item_currency = RequestBody.create(MultipartBody.FORM, _item_currency);
        RequestBody item_description = RequestBody.create(MultipartBody.FORM, _item_description);
        RequestBody kwea_id = RequestBody.create(MultipartBody.FORM, shop_id_number);

        Api api = new RxApi().getApi();
        Call<ResponseBody> call = api.uploadKweaitem(body, itemname, item_category, item_price, item_currency, item_description, kwea_id);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                if (response.isSuccessful()) {
//                    progressDialog.dismiss();
//                    String api_message = itemname+" "+"has been created successfully";
//                    showSweetAlertDialogSuccess(api_message, ctx);
                    onBackPressed();
                }
                CreateNewItemActivity.this.progressDialog.dismiss();

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                t.printStackTrace();
                CreateNewItemActivity.this.progressDialog.dismiss();

            }
        });
    }






    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

    }

    @Override
    public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {

    }

    public void setupToolbar() {
        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        final ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(getResources().getDrawable(R.drawable.ic_back));
            getSupportActionBar().setTitle(R.string.add_kwea_items);
            toolbar.setTitleTextColor(getResources().getColor(R.color.white));


        }

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.create:
                progressBar.setVisibility(View.VISIBLE);
                        if (validateFormFields()) {
                            uploadShopItem();
                        }

        //createKweaShopItemToAPI();
//    }
        break;
}

}

@Override
public void onPointerCaptureChanged(boolean hasCapture){

        }
        }