package com.app.ecobba.Fragments.Wallet;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.ecobba.Dialogs.DepositFragment;
import com.app.ecobba.Dialogs.DepositRequestDialog;
import com.app.ecobba.Dialogs.WalletTransfer;
import com.app.ecobba.Models.LatestTransaction.LatestTransactionResponse;
import com.app.ecobba.Models.LatestTransaction.Transaction;
import com.app.ecobba.Models.NetworksResponse;
import com.app.ecobba.Models.UserProfile.Detail;
import com.app.ecobba.Models.UserWallet.walletResponse;
import com.app.ecobba.R;
import com.app.ecobba.Utils.BaseFragment;
import com.app.ecobba.ViewModel.UserProfileViewModel;
import com.app.ecobba.common.SharedMethods;
import com.app.ecobba.common.rxApi.Api;
import com.app.ecobba.common.rxApi.RxApi;
import com.app.ecobba.recyclerview_adapters.GroupsListRecycler;
import com.app.ecobba.recyclerview_adapters.MyTransactionsRecycler;
import com.app.ecobba.recyclerview_models.UserTransactionsAdapter;
import com.bumptech.glide.Glide;
import com.google.android.material.navigation.NavigationView;
import com.google.gson.Gson;
import com.leavjenn.smoothdaterangepicker.date.SmoothDateRangePickerFragment;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import kotlin.Unit;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class WalletFragment extends BaseFragment implements
        View.OnClickListener,
        SmoothDateRangePickerFragment.OnDateRangeSetListener {

    RecyclerView recyclerView;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    @BindView(R.id.tvBalance)
    TextView tvBalance;
//    @BindView(R.id.tvTotalDebits)
//    TextView tvTotalDebits;
//    @BindView(R.id.tvTotalCredits)
//    TextView tvTotalCredits;
    @BindView(R.id.deposit)
    CardView deposit;
    @BindView(R.id.tvNoAvailableData)
    TextView tvNoAvailableData;
    @BindView(R.id.currency)
    TextView currency;

    @BindViews({
            R.id.deposit,
            R.id.withdraw})
    List<CardView> buttons;

    String token;
    public String user_currency;
    int balance;
    public static String currencyValue;

    Bundle args;
    @BindView(R.id.pbWallet)
    ProgressBar pbWallet;
    @BindView(R.id.llwallet)
    LinearLayout llwallet;
    @BindView(R.id.llTransaction)
    LinearLayout llTransaction;
    private Api api;
    Context context;
    Fragment fragment;
    MyTransactionsRecycler adapter;
    UserProfileViewModel userProfileViewModel;

    public WalletFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_wallet, container, false);
        api = new RxApi().getApi();
        userProfileViewModel = new ViewModelProvider(getActivity()).get(UserProfileViewModel.class);
        return view;
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        context = getActivity();
        deposit.setOnClickListener(this);

        for (CardView b : buttons) {
            b.setOnClickListener(this);
        }

        recyclerView = view.findViewById(R.id.recyclerView);
        fragment = this;
        adapter = new MyTransactionsRecycler(new ArrayList<>(), requireContext(), fragment, "");
        RecyclerView.LayoutManager layoutManager1 = new LinearLayoutManager(requireContext());
        recyclerView.setLayoutManager(layoutManager1);
        recyclerView.setAdapter(adapter);
        fetchUsersData();
        fetchUserProfile();

    }

    @Override
    public void onResume() {
        super.onResume();

    }

    private void fetchUserProfile() {
        userProfileViewModel.getUserProfile();
        userProfileViewModel.userProfileResponseLiveData().observe(getViewLifecycleOwner(), userProfileResponse -> {
            try {
                if (userProfileResponse.getSuccess()) {
                    Detail userDetails = userProfileResponse.getUser().getDetail();
                    String name = userProfileResponse.getUser().getName();

                    user_currency = userProfileResponse.getUser().getWallet().getCurrency().getPrefix();
                    Log.e("MUDHDGDGD", user_currency);
                    balance = userProfileResponse.getUser().getWallet().getBalance();
                    tvBalance.setText(new DecimalFormat("0.00").format(balance));
                    currency.setText(user_currency);
//                    currencyValue = String.valueOf(userProfileResponse.getUser().getDetail().getCountry());
                    user_currency = userProfileResponse.getUser().getWallet().getCurrency().getPrefix();
                    adapter.user_currency = user_currency;
                    adapter.notifyDataSetChanged();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    private void fetchUsersData() {
        pbWallet.setVisibility(View.VISIBLE);
        Call<walletResponse> call = api.getUserWalletTransaction();
        call.enqueue(new Callback<walletResponse>() {
            @Override
            public void onResponse(Call<walletResponse> call, Response<walletResponse> response) {
               try {
                   if (response.body().getSuccess() == true) {
                       if (response.body().getTransactions().size() > 0) {
                           pbWallet.setVisibility(View.GONE);
                           adapter.datalist = response.body().getTransactions();
                           adapter.notifyDataSetChanged();
                           pbWallet.setVisibility(View.GONE);
                           llwallet.setVisibility(View.GONE);
                           llTransaction.setVisibility(View.VISIBLE);
                       } else {
                           pbWallet.setVisibility(View.GONE);
                           tvNoAvailableData.setText(getResources().getString(R.string.no_available_data));
                           tvNoAvailableData.setVisibility(View.VISIBLE);
                           pbWallet.setVisibility(View.GONE);
                           llwallet.setVisibility(View.GONE);
                           llTransaction.setVisibility(View.VISIBLE);
                       }
                   }
               } catch (Exception e) {
                   e.printStackTrace();
               }
            }

            @Override
            public void onFailure(Call<walletResponse> call, Throwable t) {

            }
        });
    }


    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.withdraw:
                Bundle args = new Bundle();
                args.putString("msdin", user_currency);
                WalletTransfer walletTransfer = new WalletTransfer(user_currency);
                walletTransfer.show(getActivity().getSupportFragmentManager(), "TAG");

                break;

        }
    }

    @Override
    public void onDateRangeSet(SmoothDateRangePickerFragment view, int yearStart, int monthStart, int dayStart, int yearEnd, int monthEnd, int dayEnd) {

    }


}
