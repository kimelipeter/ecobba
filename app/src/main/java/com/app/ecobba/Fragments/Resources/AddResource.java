package com.app.ecobba.Fragments.Resources;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.loader.content.CursorLoader;

import com.app.ecobba.Fragments.Shop.MyKweaProducts.CreateNewItemActivity;
import com.app.ecobba.R;
import com.app.ecobba.common.ApisKtKt;
import com.app.ecobba.common.SharedMethods;
import com.app.ecobba.recyclerview_models.resources.categories.Data;
import com.google.gson.JsonObject;
import com.jaredrummler.materialspinner.MaterialSpinner;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.async.http.body.FileBody;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.ProgressCallback;
import com.koushikdutta.ion.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Year;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.mayanknagwanshi.imagepicker.ImageSelectActivity;

import static android.app.Activity.RESULT_OK;
import static com.app.ecobba.common.ApisKtKt.getResoursesAttachmentTypes;
import static com.app.ecobba.common.ApisKtKt.getResoursesCategories;
import static com.app.ecobba.common.ApisKtKt.getResoursesType;
import static com.app.ecobba.common.AppConstantsKt.INITIATOR_GET_RESOURCES_ATTACHMENT_TYPES;
import static com.app.ecobba.common.AppConstantsKt.INITIATOR_GET_RESOURCES_CATEGORIES;
import static com.app.ecobba.common.AppConstantsKt.INITIATOR_GET_RESOURCES_TYPES;

import static com.app.ecobba.common.SharedMethods.HTTP_GET;
import static com.app.ecobba.common.SharedMethods.getFromServer;
import static com.app.ecobba.common.SharedMethods.global_error_message;
import static com.app.ecobba.common.SharedMethods.runOnUI;
import static com.app.ecobba.common.SharedMethods.showSweetAlertDialogError;

public class AddResource extends Fragment implements SharedMethods.NetworkObjectListener, MaterialSpinner.OnItemSelectedListener, View.OnClickListener {
    String token, end_date, filePath1;
    String url = ApisKtKt.createResource;
    String filetype = " ";
    Context ctx;
    String contentype = " ";
    @BindView(R.id.create)
    Button create;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    //@BindView(R.id.resource_type)
    Spinner resource_type, file_media, category;
    EditText etEndDate, etFileImage, etFileVideo, etTitle, etRedourceUrl, etDesc;
    int MY_PERMISSIONS_REQUEST_CAMERA = 0;
    Bitmap selectedImage;
    File fileToUpload;
    LinearLayout layoutFiles;
    private ArrayList<String> arrayList = new ArrayList<String>();
    private String _title, _desc, item_resource_type, item_resource_cat, item_file_media;

    private static final int SELECT_VIDEO = 3;
    private static final int SELECT_PHOTO = 123;

    public AddResource() {
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.add_resource_fragment, container, false);
        ctx = getActivity();
        ButterKnife.bind(this, view);
        create.setOnClickListener(this);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        ctx = getActivity();
        ButterKnife.bind(this, view);
        token = SharedMethods.getDefaults("token", ctx);
        etTitle = view.findViewById(R.id.etTitle);
        etRedourceUrl = view.findViewById(R.id.etRedourceUrl);
        etDesc = view.findViewById(R.id.etDesc);
        resource_type = view.findViewById(R.id.resource_type);
        file_media = view.findViewById(R.id.file_media);
        category = view.findViewById(R.id.category);
        etEndDate = view.findViewById(R.id.etEndDate);
        etEndDate.setOnClickListener(this);
        etFileImage = view.findViewById(R.id.etFileImage);
        etFileVideo = view.findViewById(R.id.etFileVideo);
        fetchResourceTypes(token);
        fetchResourceAttachmentTypes(token);
        fetchResources(token);

        layoutFiles = view.findViewById(R.id.layoutFiles);

        etFileImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SELECT_PHOTO == 123) {
                    Intent intent = new Intent(ctx, ImageSelectActivity.class);
                    intent.putExtra(ImageSelectActivity.FLAG_COMPRESS, false);//default is true
                    intent.putExtra(ImageSelectActivity.FLAG_CAMERA, true);//default is true
                    intent.putExtra(ImageSelectActivity.FLAG_GALLERY, true);//default is true
                    startActivityForResult(intent, SELECT_PHOTO);
                }


            }
        });
        etFileVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SELECT_VIDEO == 3) {
//                    Intent intent = new Intent();
//                    intent.setType("video/*");
//                    intent.setAction(Intent.ACTION_GET_CONTENT);
//                    startActivityForResult(Intent.createChooser(intent, "Select a Video "), SELECT_VIDEO);
//
//                    String[] mimeTypes = {"video/mp4"};
//
//                    Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
//                    intent.addCategory(Intent.CATEGORY_OPENABLE);
//
//                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
//                        intent.setType(mimeTypes.length == 1 ? mimeTypes[0] : "*/*");
//                        if (mimeTypes.length > 0) {
//                            intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
//                        }
//                    } else {
//                        String mimeTypesStr = "";
//
//                        for (String mimeType : mimeTypes) {
//                            mimeTypesStr += mimeType + "|";
//                        }
//
//                        intent.setType(mimeTypesStr.substring(0, mimeTypesStr.length() - 1));
//                    }
//                    startActivityForResult(Intent.createChooser(intent, ""), SELECT_VIDEO);

                    @SuppressLint("IntentReset")
                    Intent intent = new Intent(
                            Intent.ACTION_PICK,
                            android.provider.MediaStore.Video.Media.EXTERNAL_CONTENT_URI);
                    intent.setType("video/*");
                    startActivityForResult(intent, SELECT_VIDEO);
                }

            }
        });
        category.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                item_resource_cat = (String) parent.getItemAtPosition(position);
                // Notify the selected item text
                Toast.makeText
                        (getActivity(), "Selected : " + item_resource_cat, Toast.LENGTH_SHORT)
                        .show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        ////start of camera access persmission
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA)
                == PackageManager.PERMISSION_DENIED)


            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CAMERA}, MY_PERMISSIONS_REQUEST_CAMERA);

        ////end of camera access persmission

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            if (requestCode == SELECT_PHOTO) {
                filePath1 = data.getStringExtra(ImageSelectActivity.RESULT_FILE_PATH);
                selectedImage = BitmapFactory.decodeFile(filePath1);
                Log.e("image path is", filePath1);
                fileToUpload = new File(filePath1);
                arrayList.add(filePath1);
                String filename = filePath1.substring(filePath1.lastIndexOf("/") + 1);
                filetype = fileToUpload.getAbsolutePath();

                etFileImage.setText(filename);
//            ivDocument.setImageBitmap(selectedImage);

            } else if (requestCode == SELECT_VIDEO) {
                Uri videofilePath = data.getData();

                String realPath;
//                String filename = "VID_" + videofilePath.toString().substring(videofilePath.toString().lastIndexOf("/") + 1);
//            Log.e("VIDEO_PATH_FILE", String.valueOf(videofilePath.getPath()));
//                etFileVideo.setText(filename);

//               File file = new File(videofilePath.getPath());
//                Log.e("VIDEO_PATH_FILEs", String.valueOf(file.getAbsolutePath()));
//                CursorLoader cursorLoader = new CursorLoader(getActivity().getApplicationContext(), videofilePath, null, null, null,null);
//                Cursor cursor = cursorLoader.loadInBackground();
//                int idx = cursor.getColumnIndex(MediaStore.Files.FileColumns.DATA);
//                cursor.moveToFirst();
//                realPath = cursor.getString(idx);
//                Log.e("debinfProdAct", "Real file path in physical device "+realPath);
//                cursor.close();

                String[] filePath = {MediaStore.Video.Media.DATA};
                Cursor c = getActivity().getContentResolver().query(videofilePath, filePath,
                        null, null, null);
                c.moveToFirst();
                int columnIndex = c.getColumnIndex(filePath[0]);
                String videoPath = c.getString(columnIndex);
                Log.e("PATH_VID", videoPath);

                fileToUpload = new File(videoPath);
                int file_size = Integer.parseInt(String.valueOf(fileToUpload.length() / 1024));
                Log.e("VIDEO_SIZE_FILE", String.valueOf(file_size));
                filetype = fileToUpload.getAbsolutePath();
                String filename = videoPath.substring(videoPath.lastIndexOf("/") + 1);
                etFileVideo.setText(filename);


            }
        }

    }

    public void fetchResourceTypes(String token) {
        Map<String, String> params = new HashMap<>();
        params.put("token", token);
        getFromServer(getResoursesType, token, params, INITIATOR_GET_RESOURCES_TYPES, HTTP_GET, ctx, this);
    }

    public void fetchResourceAttachmentTypes(String token) {
        Map<String, String> params = new HashMap<>();
        params.put("token", token);
        getFromServer(getResoursesAttachmentTypes, token, params, INITIATOR_GET_RESOURCES_ATTACHMENT_TYPES, HTTP_GET, ctx, this);
    }

    public void fetchResources(String token) {
        Map<String, String> params = new HashMap<>();
        params.put("token", token);
        getFromServer(getResoursesCategories, token, params, INITIATOR_GET_RESOURCES_CATEGORIES, HTTP_GET, ctx, this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.etEndDate:
                pickDate(etEndDate);
                break;
            case R.id.create:
                if (validateFormFields()) {
                    createaResource();
                }
                break;
        }

    }

    private void createaResource() {


        if (filetype.contains("mp4") || filetype.contains("video")) {
            contentype = "video/mp4";
            Log.e("Contentvideo", contentype);

        } else if (filetype.contains("jpeg") || filetype.contains("picture")) {
            contentype = "image/jpeg";
            Log.e("Contentimage", contentype);
        }


        ProgressDialog mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setMessage("Please Wait....");
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        mProgressDialog.setCancelable(true);

        mProgressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                // downloadTask.cancel(true);
            }
        });
        mProgressDialog.show();

//==============ION part
        Ion.with(getActivity())
                .load("POST", url)
                .setLogging("Uploading", Log.DEBUG)

                .uploadProgressDialog(mProgressDialog)
                .uploadProgressHandler(new ProgressCallback() {
                    @Override
                    public void onProgress(long uploaded, long total) {
                        mProgressDialog.setIndeterminate(true);
                        mProgressDialog.setMax((int) total);
                        mProgressDialog.setProgress((int) uploaded);

                    }
                })
                .addHeader("Accept", "application/json")
                .addHeader("Authorization", "Bearer " + token)
                .setMultipartParameter("name", _title)
                .setMultipartParameter("type", item_resource_type)
                .setMultipartParameter("category", item_resource_cat)
                .setMultipartParameter("datesingle", end_date)
                .setMultipartParameter("link", "")
                .setMultipartParameter("attachmenttype", item_file_media)
                .setMultipartParameter("content", _desc)
                .setMultipartFile("attachments", contentype, fileToUpload)
                .setMultipartParameter("identification_document", "")
                .asString()
                .withResponse()
                .setCallback(new FutureCallback<Response<String>>() {
                    @Override
                    public void onCompleted(Exception e, Response<String> result) {
                        if (result != null) {
                            //Upload Success
                            Log.e("Upload_data_is", String.valueOf(result.getRequest()));
                            //   Toast.makeText(ctx, result.getHeaders().code(), Toast.LENGTH_LONG).show();
                            Toast.makeText(ctx, "Uploaded Successfully", Toast.LENGTH_LONG).show();

//                                Intent intent= new Intent(CreateNewItemActivity.this, KweaItemsActivity.class);
//                                intent.putExtra("shop_id_number",shop_id_number);
//                                startActivity(intent);
                        } else {
                            //Upload Failed
                            progressBar.setVisibility(View.GONE);
                            Toast.makeText(ctx, "Error uploading", Toast.LENGTH_LONG).show();
                            Log.e("ERROR_UPLOAD " + e.toString(), String.valueOf(e.getMessage()));

                        }
                        mProgressDialog.dismiss();
                        return;
                    }

//                    @Override
//                    public void onCompleted(Exception e, Response <String> result) {
//                        // do stuff with the result or error
//
//                    }
                });


    }

    private boolean validateFormFields() {
        boolean valid = true;
        _title = etTitle.getText().toString().trim();
        _desc = etDesc.getText().toString().trim();
        end_date = etEndDate.getText().toString();
        Log.e("POST_DATA", _title + _desc + end_date + fileToUpload + item_resource_type + item_resource_cat + item_file_media);


        if (_title.isEmpty()) {
            etTitle.setError(getString(R.string.emptyField));
            valid = false;
        }
        if (_desc.isEmpty()) {
            etDesc.setError(getString(R.string.emptyField));
            valid = false;
        }
        if (end_date.isEmpty()) {
            etEndDate.setError(getString(R.string.emptyField));
            valid = false;
        }
        if (file_media.getSelectedItem().toString().trim() == "Select Option") {

            Toast.makeText(ctx,"Select Option", Toast.LENGTH_SHORT).show();
        }
        return valid;

    }

    private Date pickDate(EditText view) {
        final Date[] date = new Date[1];
        Calendar c = Calendar.getInstance();
        int mYear = Year.now().getValue();
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);
        new DatePickerDialog(getContext(),
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker,
                                          int year,
                                          int monthOfYear,
                                          int dayOfMonth) {
                        String string = String.valueOf(year) + "/" + String.valueOf(monthOfYear + 1) + "/" + String.valueOf(dayOfMonth);

                        try {
                            date[0] = new SimpleDateFormat("dd/MM/yyyy").parse(string);
                            final Calendar datePickerCal = Calendar.getInstance();
                            datePickerCal.set(monthOfYear + 1, dayOfMonth, year);
                            end_date = new SimpleDateFormat("MM/dd/yyyy", Locale.ENGLISH).format(datePickerCal.getTime());
                            view.setText(end_date);
//                            dob = date[0];
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                    }
                }, mYear, mMonth, mDay)
                .show();
        return date[0];
    }

    @Override
    public void onDataReady(String initiator, String response) {
        switch (initiator) {
            case INITIATOR_GET_RESOURCES_TYPES:
                try {
                    runOnUI(new Runnable() {
                        @Override
                        public void run() {
                            Log.e("REG_DATA RESPONSE", "Response is " + response);

                            JSONObject jObj = null;

//                            currencies_ids = new ArrayList<>();

                            try {
                                jObj = new JSONObject(response);
                                if (jObj.has("errors")) {
                                    String errors = jObj.getString("errors");
                                    showSweetAlertDialogError(errors, ctx);
                                } else { // if no error message was found in the response,
                                    boolean success = jObj.getBoolean("success");
                                    if (success) { // if the request was successfuls, got forward, else show error message
//                                        JSONObject data = jObj.getJSONObject("message");
                                        JSONArray resource_types = jObj.getJSONArray("data");
                                        Log.e("cur_data", String.valueOf(resource_types));

                                        ArrayList<String> types_drop_down = new ArrayList<String>();
                                        types_drop_down.add(getResources().getString(R.string.please_select_type));
                                        for (int i = 0; i < resource_types.length(); i++) {
                                            String str = (String) resource_types.get(i);
                                            types_drop_down.add(str);
                                        }
                                        //
//
                                        if (resource_types.length() > 0) {


                                            ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(requireContext(), android.R.layout.simple_spinner_item, types_drop_down);
                                            resource_type.setAdapter(arrayAdapter);
                                            resource_type.setSelection(0);
                                            resource_type.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                                @Override
                                                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                                    item_resource_type = (String) parent.getItemAtPosition(position);
                                                    Log.d("ARRAY", "onItemSelected: Adapter Items: " + arrayAdapter.toString());

//
                                                    Log.e("type_selected_is", "Item " + item_resource_type + " Position " + position);
                                                    // Toast.makeText(parent.getContext(),"Selected currency is ", Integer.parseInt(item));
                                                }

                                                @Override
                                                public void onNothingSelected(AdapterView<?> parent) {

                                                }

                                            });
                                        }


                                    } else {
                                        String api_error_message = jObj.getString("message");
                                        showSweetAlertDialogError(api_error_message, ctx);
                                    }
                                }

                            } catch (JSONException e) {
                                Log.e("ExceptionLOGIN-DATA", e.toString());
                                showSweetAlertDialogError(global_error_message, ctx);
                                e.printStackTrace();
                            }
                        }
                    });
                } catch (Exception e) {
//                            hideDialog(ctx);
                    Log.e("Exception- HOME", e.toString());
                    showSweetAlertDialogError(global_error_message, ctx);
//                    Toast.makeText(ctx, "Login Data Response Exception" + e.toString(), Toast.LENGTH_LONG).show();
                }
                break;
            case INITIATOR_GET_RESOURCES_ATTACHMENT_TYPES:
                try {
                    runOnUI(new Runnable() {
                        @Override
                        public void run() {
                            Log.e("DATA_RESPONSE", "Response is " + response);

                            JSONObject jObj = null;

//                            currencies_ids = new ArrayList<>();

                            try {
                                jObj = new JSONObject(response);
                                if (jObj.has("errors")) {
                                    String errors = jObj.getString("errors");
                                    showSweetAlertDialogError(errors, ctx);
                                } else { // if no error message was found in the response,
                                    boolean success = jObj.getBoolean("success");
                                    if (success) { // if the request was successfuls, got forward, else show error message
//                                        JSONObject data = jObj.getJSONObject("message");
                                        JSONArray resource_types = jObj.getJSONArray("data");
                                        Log.e("file_data_is", String.valueOf(resource_types));

                                        ArrayList<String> types_drop_down = new ArrayList<String>();
                                        types_drop_down.add(getResources().getString(R.string.please_select_type));
                                        for (int i = 0; i < resource_types.length(); i++) {
                                            String str = (String) resource_types.get(i);
                                            types_drop_down.add(str);
                                        }
                                        //
//
                                        if (resource_types.length() > 0) {


                                            ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(requireContext(), android.R.layout.simple_spinner_item, types_drop_down);
                                            file_media.setAdapter(arrayAdapter);
                                            file_media.setSelection(0);
                                            file_media.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                                @Override
                                                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                                    item_file_media = (String) parent.getItemAtPosition(position);

                                                    if (item_file_media.equals("image")) {
                                                        layoutFiles.setVisibility(View.VISIBLE);
                                                        etFileImage.setVisibility(View.VISIBLE);
                                                        etFileVideo.setVisibility(View.GONE);
                                                    } else if (item_file_media.equals("video")) {
                                                        layoutFiles.setVisibility(View.VISIBLE);
                                                        etFileVideo.setVisibility(View.VISIBLE);
                                                        etFileImage.setVisibility(View.GONE);
                                                    } else {
                                                        layoutFiles.setVisibility(View.GONE);
                                                    }
                                                    Log.d("ARRAY", "onItemSelected: Adapter Items: " + arrayAdapter.toString());

//
                                                    Log.e("type_selected_is", "Item " + item_file_media);
                                                    // Toast.makeText(parent.getContext(),"Selected currency is ", Integer.parseInt(item));
                                                }

                                                @Override
                                                public void onNothingSelected(AdapterView<?> parent) {

                                                }

                                            });
                                        }


                                    } else {
                                        String api_error_message = jObj.getString("message");
                                        showSweetAlertDialogError(api_error_message, ctx);
                                    }
                                }

                            } catch (JSONException e) {
                                Log.e("ExceptionLOGIN-DATA", e.toString());
                                showSweetAlertDialogError(global_error_message, ctx);
                                e.printStackTrace();
                            }
                        }
                    });
                } catch (Exception e) {
//                            hideDialog(ctx);
                    Log.e("Exception- HOME", e.toString());
                    showSweetAlertDialogError(global_error_message, ctx);
//                    Toast.makeText(ctx, "Login Data Response Exception" + e.toString(), Toast.LENGTH_LONG).show();
                }
                break;

        }


    }

    @Override
    public void onDataLoaded(String initiator, JSONArray data) {

    }

    @Override
    public void onNetworkError(String initiator, String errorCode, String errorMessage) {

    }

    @Override
    public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {

    }
}
