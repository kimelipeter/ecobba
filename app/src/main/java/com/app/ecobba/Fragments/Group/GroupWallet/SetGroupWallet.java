package com.app.ecobba.Fragments.Group.GroupWallet;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.app.ecobba.Models.adapay.adapaySourcesResponse;
import com.app.ecobba.Models.adapay.telcos.telcosResponse;
import com.app.ecobba.R;
import com.app.ecobba.common.SharedMethods;
import com.app.ecobba.common.rxApi.Api;
import com.app.ecobba.common.rxApi.RxApi;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.app.ecobba.common.ApisKtKt.setPaymentMethod;
import static com.app.ecobba.common.AppConstantsKt.INITIATOR_SET_PAYMENT_METHOD;
import static com.app.ecobba.common.SharedMethods.HTTP_POST;
import static com.app.ecobba.common.SharedMethods.global_error_message;
import static com.app.ecobba.common.SharedMethods.postToServer;
import static com.app.ecobba.common.SharedMethods.runOnUI;
import static com.app.ecobba.common.SharedMethods.showSweetAlertDialogError;
import static com.app.ecobba.common.SharedMethods.showSweetAlertDialogSuccess;

public class SetGroupWallet extends DialogFragment implements
        SharedMethods.NetworkObjectListener, View.OnClickListener {

    @BindView(R.id.close)
    Button close;

    @BindView(R.id.submit)
    Button submit;
    @BindView(R.id.editTextMsdn)
    EditText editTextMsdn;
    Context ctx;
    private Api api;
    String token;
    private Unbinder unbinder;
    ArrayAdapter arrayAdapter;
    LinearLayout linearLayoutMobile, llLayoutMobileMpesa;
    ColorDrawable popupColor;
    List<String> currencies_ids, ada_pay_ids, currency_prefix, telcos_adapay;
    String  type, telco, msisdn, selected_type, selected_provider;

    Spinner payMethod, provider;
    String _group_id, _type, _telco, _msisdn;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;


    public SetGroupWallet(String group_id) {

        this._group_id = group_id;
        Log.e("TAG_group_id", "SetPaymentSourceDialog: " + _group_id);
    }


    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.set_preference_dialog, null);
        ctx = getActivity();
        api = new RxApi().getApi();
        token = SharedMethods.getDefaults("token", ctx);

        fetchAdaPaySourceType();
        fetchAdaPayTelcos();

        close = view.findViewById(R.id.close);
        unbinder = ButterKnife.bind(this, view);
        payMethod = view.findViewById(R.id.payMethod);
        provider = view.findViewById(R.id.provider);
        editTextMsdn.setText(_msisdn);
        _msisdn = editTextMsdn.getText().toString();
        _type = selected_type;
        _telco = selected_provider;
        submit.setOnClickListener(this);

//        payMethod.setOnItemSelectedListener(this);
        api = new RxApi().getApi();
        linearLayoutMobile = view.findViewById(R.id.linearLayoutMobile);
        llLayoutMobileMpesa = view.findViewById(R.id.llLayoutMobileMpesa);
        popupColor = new ColorDrawable(getResources().getColor(R.color.colorSurface));
//        payMethod.getPopupWindow().setBackgroundDrawable(popupColor);
        close.setOnClickListener(v -> dismiss());

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(view);


        return builder.create();
    }

    private void postWalletPreference() {
        progressBar.setVisibility(View.VISIBLE);

        String params = "{ \"group_id\":" + '"' + _group_id + '"' +
                ", \"type\": " + '"' + selected_type + '"' +
                ", \"telco\": " + '"' + selected_provider + '"' +
                ", \"msisdn\": " + '"' + _msisdn + '"' +
                "}\n" + "\n";
        Log.e("POST_PARAMS", params);
        //depositfromMpesa
        postToServer(setPaymentMethod, token, params, INITIATOR_SET_PAYMENT_METHOD, HTTP_POST, ctx, this);
    }

    public void fetchAdaPaySourceType() {

        Call<adapaySourcesResponse> call = api.fetchAdapaySources();
        call.enqueue(new Callback<adapaySourcesResponse>() {
            @Override
            public void onResponse(Call<adapaySourcesResponse> call, Response<adapaySourcesResponse> response) {

                Log.e("SOLANI_DATA", "onResponse: " + response.body().getAdapay_source_types());
                String mobile = response.body().getAdapay_source_types().getMobile_money();
                String card = response.body().getAdapay_source_types().getCard_hosted();
                String bank_transfer = response.body().getAdapay_source_types().getBank_transfer();

                ada_pay_ids = new ArrayList<>();
                ada_pay_ids.add(0, getResources().getString(R.string.please_select_type));
                ada_pay_ids.add(1, mobile);
                ada_pay_ids.add(2, card);
                ada_pay_ids.add(3, bank_transfer);

                ArrayAdapter<String> adapter =
                        new ArrayAdapter<String>(ctx, android.R.layout.simple_spinner_dropdown_item, ada_pay_ids);
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                payMethod.setAdapter(adapter);

                payMethod.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                        String type = (String) parent.getItemAtPosition(position);

                        selected_type = "";
                        if (type.equals("Mobile Money")) {
                            selected_type = "mobile_money";
                            linearLayoutMobile.setVisibility(View.VISIBLE);
//                                            linearLayoutBank.setVisibility(View.GONE);

                        }
//                                        else if (type.equals("Master/Visa Card")){
//                                            selected_type = "card_hosted";
//                                            linearLayoutBank.setVisibility(View.GONE);
//                                            linearLayoutMobile.setVisibility(View.GONE);
//                                        }
//                                        else if (type.equals("Bank Transfer")){
//                                            selected_type = "bank_transfer";
//                                            linearLayoutBank.setVisibility(View.GONE);
//                                            linearLayoutMobile.setVisibility(View.GONE);
//                                        }
                        else {
//
                            //    linearLayoutBank.setVisibility(View.GONE);
                            linearLayoutMobile.setVisibility(View.GONE);
                        }
                        Log.e("Selected_type", String.valueOf(type));
                        Log.e("Selected_type2", selected_type);
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }

                });


            }

            @Override
            public void onFailure(Call<adapaySourcesResponse> call, Throwable t) {

            }
        });

    }


    public void fetchAdaPayTelcos() {
        Call<telcosResponse> call = api.fetchAdapayTelcos();
        call.enqueue(new Callback<telcosResponse>() {
            @Override
            public void onResponse(Call<telcosResponse> call, Response<telcosResponse> response) {
                if (response.body().getSuccess() == true) {
                    String telcos = response.body().getAdapay_telcos().getSafaricom_ke();
                    telcos_adapay = new ArrayList<>();
                    telcos_adapay.add(0, getResources().getString(R.string.please_select_type));
                    telcos_adapay.add(1, telcos);
                    ArrayAdapter<String> adapter =
                            new ArrayAdapter<String>(ctx, android.R.layout.simple_spinner_dropdown_item, telcos_adapay);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    provider.setAdapter(adapter);
                    provider.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            String type = (String) parent.getItemAtPosition(position);
                            selected_provider = "";
                            if (type.equals("Mpesa")) {
                                llLayoutMobileMpesa.setVisibility(View.VISIBLE);
                                selected_provider = "safaricom_ke";
                            } else {
                                llLayoutMobileMpesa.setVisibility(View.GONE);
                            }
                            Log.e("Selected_telcos", String.valueOf(type));
                            Log.e("Selected_telcos2", selected_provider);
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });


                }
            }

            @Override
            public void onFailure(Call<telcosResponse> call, Throwable t) {

            }
        });
    }

    private boolean validateFormFields() {
        boolean valid = true;
        _msisdn = editTextMsdn.getText().toString();
        Log.e("msisdn", _msisdn);
        if (_msisdn.isEmpty()) {
            editTextMsdn.setError(getString(R.string.emptyField));
            valid = false;
        }


        return valid;

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.submit:
                if (validateFormFields()) {
                    postWalletPreference();
                }
                break;


        }
    }


    @Override
    public void onDataReady(String initiator, String response) {
        switch (initiator) {
            case INITIATOR_SET_PAYMENT_METHOD:
                try {
                    runOnUI(new Runnable() {
                        @Override
                        public void run() {
                            Log.e("SET_PAYMENT_SOURCE", "Response is " + response);
                            progressBar.setVisibility(View.GONE);
                            JSONObject jObj = null;
                            try {
                                jObj = new JSONObject(response);
                                if (jObj.has("errors")) {
                                    String errors = jObj.getString("errors");
                                    showSweetAlertDialogError(errors, ctx);
                                } else { // if no error message was found in the response,
                                    boolean success = jObj.getBoolean("success");
                                    if (success) {
                                        dismiss();
                                        String api_message = jObj.getString("message");
                                        showSweetAlertDialogSuccess(api_message, ctx);

                                    } else {
                                        dismiss();
                                        String api_error_message = jObj.getString("message");
                                        showSweetAlertDialogError(api_error_message, ctx);
                                    }
                                }

                            } catch (JSONException e) {
                                Log.e("Exception-DATA", e.toString());
                                showSweetAlertDialogError(global_error_message, ctx);
                                e.printStackTrace();
                            }
                        }
                    });
                } catch (Exception e) {
                    progressBar.setVisibility(View.GONE);
                    Log.e("Exception_PAY_SOURCE", e.toString());
                    showSweetAlertDialogError(global_error_message, ctx);
//                    Toast.makeText(ctx, "Login Data Response Exception" + e.toString(), Toast.LENGTH_LONG).show();
                }
                break;
        }

    }

    @Override
    public void onDataLoaded(String initiator, JSONArray data) {

    }

    @Override
    public void onNetworkError(String initiator, String errorCode, String errorMessage) {

        switch (initiator) {
            case INITIATOR_SET_PAYMENT_METHOD:
                try {
                    runOnUI(new Runnable() {
                        @Override
                        public void run() {
//                                    hideDialog(ctx);
                            showSweetAlertDialogError(errorMessage, ctx);
                        }
                    });
                } catch (Exception e) {
//                            hideDialog(ctx);
                    showSweetAlertDialogError(global_error_message, ctx);
                    Log.e("Exception- HOME", e.toString());
//                    Toast.makeText(ctx, "Login Error Response Exception" + e.toString(), Toast.LENGTH_LONG).show();
                }

                break;

        }

    }
}
