
package com.app.ecobba.Fragments.Group.GroupModel;

import lombok.Data;

@Data
@SuppressWarnings("unused")
public class LevelOne {

    private long countryId;
    private String createdAt;
    private long id;
    private String name;
    private String updatedAt;

}
