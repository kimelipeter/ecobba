package com.app.ecobba.Fragments.Profile.response

data class Currency(
        val name: String,
        val prefix: String
)