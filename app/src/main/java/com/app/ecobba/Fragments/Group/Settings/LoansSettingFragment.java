package com.app.ecobba.Fragments.Group.Settings;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.app.ecobba.Fragments.Group.Settings.Adapters.LoansSettingsAdapter;
import com.app.ecobba.Models.MeetingsSettingsLoans.LoansSettingsResponse;
import com.app.ecobba.Models.UserProfile.Detail;
import com.app.ecobba.R;
import com.app.ecobba.ViewModel.UserProfileViewModel;
import com.app.ecobba.common.rxApi.Api;
import com.app.ecobba.common.rxApi.RxApi;
import com.google.android.material.chip.Chip;


import java.util.ArrayList;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class LoansSettingFragment extends Fragment implements View.OnClickListener{

    Fragment fragment;
    Context context;
    String api_tail;
    Bundle args;
    String group_id,setting_id,loan_type;
    LoansSettingsAdapter adapter;
    private Api api;

    @BindView(R.id.tvNoAvailableData)
    TextView tvNoAvailableData;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.swipe_container)
    SwipeRefreshLayout mSwipeRefreshLayout;

    UserProfileViewModel userProfileViewModel;

    @BindView(R.id.businessLoan)
    Chip businessLoan;
    @BindView(R.id.welFareLoan)
    Chip welFareLoan;
    String user_currency;

    public LoansSettingFragment(){

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_group_setting_loans,container,false);
        ButterKnife.bind(this,view);
        context = getActivity();
        Bundle args = getArguments();
        group_id = args.getString("group_id");
        setting_id = args.getString("id");
        fragment = this;
        userProfileViewModel = new ViewModelProvider(getActivity()).get(UserProfileViewModel.class);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        businessLoan.setOnClickListener(this);
        welFareLoan.setOnClickListener(this);

        args = getArguments();
        assert args!=null;
        api_tail = args.getString("args");
        api=new RxApi().getApi();

        recyclerView.addItemDecoration(new DividerItemDecoration(getContext(),
                DividerItemDecoration.VERTICAL));
        adapter = new LoansSettingsAdapter(new ArrayList<>(), requireContext(), fragment,"");
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(requireContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
        getData(setting_id,group_id);
        fetchUserProfile();
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                 Toast.makeText(context,"Refresh", Toast.LENGTH_LONG).show();
                getData(setting_id, group_id);
                mSwipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary,
                        android.R.color.holo_green_dark,
                        android.R.color.holo_orange_dark,
                        android.R.color.holo_blue_dark);

                mSwipeRefreshLayout.setRefreshing(false);
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.welFareLoan:
                loan_type="jamii";

                Bundle bundle = new Bundle();
                bundle.putString("group_id", group_id);
                bundle.putString("args", api_tail);
                bundle.putString("id", setting_id);
                bundle.putString("loan_type", loan_type);
                NavHostFragment.findNavController(this).navigate(R.id.businessLoanCreateFragment,bundle);
                break;
            case R.id.businessLoan:
                loan_type="biashara";
                Bundle bundle1 = new Bundle();
                bundle1.putString("group_id", group_id);
                bundle1.putString("args", api_tail);
                bundle1.putString("id", setting_id);
                bundle1.putString("loan_type", loan_type);
                NavHostFragment.findNavController(this).navigate(R.id.businessLoanCreateFragment,bundle1);
                break;
        }
    }
    private void fetchUserProfile() {
        userProfileViewModel.getUserProfile();
        userProfileViewModel.userProfileResponseLiveData().observe(getViewLifecycleOwner(), userProfileResponse -> {
            try {
                if (userProfileResponse.getSuccess()) {
                    Detail userDetails = userProfileResponse.getUser().getDetail();
                    String name = userProfileResponse.getUser().getName();

                    user_currency = userProfileResponse.getUser().getWallet().getCurrency().getPrefix();
                    Log.e("MUDHDGDGD", user_currency);

                    user_currency = userProfileResponse.getUser().getWallet().getCurrency().getPrefix();
                    adapter.user_currency = user_currency;
                    adapter.notifyDataSetChanged();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }


    private void getData(String setting_id, String group_id){
        Call<LoansSettingsResponse> call = api.getLoansSettings(setting_id, group_id);
        call.enqueue(new Callback<LoansSettingsResponse>() {
            @Override
            public void onResponse(Call<LoansSettingsResponse> call, Response<LoansSettingsResponse> response) {
                progressBar.setVisibility(View.GONE);
                try {
                    if (response.body().getSuccess()) {
                        if (response.body().getCurrent_settings().getData().size() > 0) {

                            adapter.dataList=response.body().getCurrent_settings().getData();
                            adapter.notifyDataSetChanged();
                        } else {
                            progressBar.setVisibility(View.GONE);
                            tvNoAvailableData.setText(getResources().getString(R.string.no_available_data));
                            tvNoAvailableData.setVisibility(View.VISIBLE);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<LoansSettingsResponse> call, Throwable t) {

            }
        });
    }


}
