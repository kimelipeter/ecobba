package com.app.ecobba.Fragments.Group.Contributions.Adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.app.ecobba.Models.payment.ContributionXX;
import com.app.ecobba.R;
import com.bumptech.glide.Glide;
import com.mikhaellopez.circularimageview.CircularImageView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

public class ShareContributionsAdapter extends RecyclerView.Adapter<ShareContributionsAdapter.ViewHolder> {
    public List<ContributionXX> datalist;
    public Context context;
    public Fragment fragment;
    public String user_currency;

    public ShareContributionsAdapter(List<ContributionXX> datalist, Context context, Fragment fragment,String user_currency) {
        this.datalist = datalist;
        this.context = context;
        this.fragment = fragment;
        this.user_currency=user_currency;
    }

    @NonNull
    @Override
    public ShareContributionsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.list_item_member_contributions, parent, false);
        return new ShareContributionsAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ShareContributionsAdapter.ViewHolder holder, int position) {
        ContributionXX contributionXX = datalist.get(position);

        String amount = contributionXX.getTotal_amount();
        String dateString = contributionXX.getCreated_at();
        String status = contributionXX.getStatus();
        String name = contributionXX.getUser().getName();
        String other_name = contributionXX.getUser().getOther_names();
        String member_avatar = contributionXX.getUser().getAvatar();
        Log.e("SHARE_AVATAR",member_avatar);

        holder.tvName.setText(name + " " + other_name);
        holder.tvUserName.setText("@" + name + "_" + other_name);
        holder.tvAmount.setText(user_currency+" "+amount);

        if (status.equals("paid")) {
            holder.tvStatus.setText(R.string.paid);
        }
        Glide.with(fragment.requireContext()).load(member_avatar).centerCrop().into(holder.user_prof);

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
        try {
            Date date1 = sdf.parse(dateString);
            sdf = new SimpleDateFormat("dd MMMM yyyy");
            String date = sdf.format(date1);
            sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
            Date time = sdf.parse(dateString);
            sdf = new SimpleDateFormat("h:mm a");
            String time1 = sdf.format(time);
            Log.e("DateAndTime", "Date " + date + " Time " + time1);
//            holder.tvTimeDifference.setText(time1);
            holder.tvDate.setText(date);
        } catch (ParseException e) {
            Log.e("DateAndTime", e.getLocalizedMessage());
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return datalist.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tvName, tvStatus, tvAmount, tvUserName, tvDate;
        public CircularImageView user_prof;

        public ViewHolder(View itemView) {
            super(itemView);
            tvStatus = itemView.findViewById(R.id.tvStatus);
            tvAmount = itemView.findViewById(R.id.tvAmount);
            tvDate = itemView.findViewById(R.id.tvDate);
            tvName = itemView.findViewById(R.id.tvName);
            tvUserName = itemView.findViewById(R.id.tvUserName);
            user_prof = itemView.findViewById(R.id.circularImageView);
        }
    }
}
