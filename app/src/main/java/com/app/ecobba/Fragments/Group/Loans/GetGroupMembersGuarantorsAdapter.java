package com.app.ecobba.Fragments.Group.Loans;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.RecyclerView;

import com.app.ecobba.Models.GroupMmembers.Data;

import com.app.ecobba.R;

import com.mikhaellopez.circularimageview.CircularImageView;
import com.squareup.picasso.Picasso;

import java.util.List;

public class GetGroupMembersGuarantorsAdapter extends RecyclerView.Adapter<GetGroupMembersGuarantorsAdapter.ViewHolder> {

    public List<Data> datalist;
    public Context context;
    public Fragment fragment;
    private Data data;

    public interface OnItemCheckListener {
        void onItemChecked(Data data);

        void onItemUnChecked(Data data);
    }

    @NonNull
    private OnItemCheckListener onItemClick;


    public GetGroupMembersGuarantorsAdapter(List<Data> datalist, Context context, Fragment fragment, @NonNull OnItemCheckListener onItemCheckListener) {
        this.datalist = datalist;
        this.context = context;
        this.fragment = fragment;
        this.onItemClick = onItemCheckListener;
    }

    @Override
    public GetGroupMembersGuarantorsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.list_item_group_members, parent, false);
        return new GetGroupMembersGuarantorsAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final GetGroupMembersGuarantorsAdapter.ViewHolder holder, final int position) {
        final Data data = datalist.get(position);
        final String member_avatar = datalist.get(position).getAvatar();
        final String tv_group_member_name = datalist.get(position).getName() + " " + datalist.get(position).getOther_names();
        final String group_member_phone_number = datalist.get(position).getMsisdn();

        holder.dataSet(data);
        holder.setOnClickListener(view -> {
            holder.group_member.setChecked(
                    !holder.group_member.isChecked());
            if ((holder).group_member.isChecked()) {
                onItemClick.onItemChecked(data);
            } else {
                onItemClick.onItemUnChecked(data);
            }
        });


    }


    @Override
    public int getItemCount() {
        return datalist.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        CheckBox group_member;
        public CircularImageView circularImageView;
        View itemView;

        Data data1;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            this.itemView = itemView;
            group_member = itemView.findViewById(R.id.group_member);
            group_member.setClickable(false);
            data1 = data;

        }

        public void dataSet(Data data) {
            group_member.setText(data.getName() + " " + data.getOther_names());
            ;
        }

        public void setOnClickListener(View.OnClickListener onClickListener) {
            itemView.setOnClickListener(onClickListener);
        }
    }
}