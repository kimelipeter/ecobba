package com.app.ecobba.Fragments.Group.Meeting;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

import com.app.ecobba.R;
import com.app.ecobba.common.SharedMethods;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Year;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.app.ecobba.common.ApisKtKt.postGroupMeetings;
import static com.app.ecobba.common.AppConstantsKt.INITIATOR_SCHEDULE_MEETING;
import static com.app.ecobba.common.SharedMethods.HTTP_POST;
import static com.app.ecobba.common.SharedMethods.global_error_message;
import static com.app.ecobba.common.SharedMethods.postToServer;
import static com.app.ecobba.common.SharedMethods.runOnUI;
import static com.app.ecobba.common.SharedMethods.showSweetAlertDialogError;
import static com.app.ecobba.common.SharedMethods.showSweetAlertDialogSuccess;

public class FragmentCreateMeeting extends Fragment implements SharedMethods.NetworkObjectListener, View.OnClickListener {

    @BindView(R.id.etMeetingName)
    EditText etMeetingName;
    @BindView(R.id.etMeetingVenue)
    EditText etMeetingVenue;
    @BindView(R.id.etMeetingDescription)
    EditText etMeetingDescription;
    @BindView(R.id.progressBar) ProgressBar progressBar;
    EditText etMeetingDate, etMeetingTime;
    Context ctx;
    String token, meeting_date, meeting_time,group_id;
    String _meeting_name,_venue,_description;
    @BindView(R.id.create) Button create;
    Bundle args;

    public FragmentCreateMeeting() {
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_create_meeting, container, false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        ctx = getActivity();
        args = getArguments();
        assert args != null;
        group_id = args.getString("group_id");
        Log.e("GROUP_ID_VAL",group_id);

        token = SharedMethods.getDefaults("token", ctx);

        etMeetingDate = view.findViewById(R.id.etMeetingDate);
        etMeetingDate.setOnClickListener(this);
        etMeetingTime = view.findViewById(R.id.etMeetingTime);
        etMeetingTime.setOnClickListener(this);
        create.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.etMeetingDate:
                pickDate(etMeetingDate);
                break;
            case R.id.etMeetingTime:
                pickTime(etMeetingTime);
                break;
            case R.id.create:
                if(validateFormFields()) {
                    createMeetingToAPI();
                }
                break;
        }

    }

    private void createMeetingToAPI() {
            progressBar.setVisibility(View.VISIBLE);

            String params = "{ \"name\":" + '"' + _meeting_name + '"' +
                    ", \"date\": " + '"' + meeting_date + '"' +
                    ", \"time\": " + '"' + meeting_time + '"' +
                    ", \"venue\": " + '"' + _venue + '"' +
                    ", \"description\": " + '"' + _description + '"' +
                    ", \"group_id\": " + '"' + group_id + '"' +

                    "}\n" + "\n";
            Log.e("POST PARAMS", params);

                //scheduleMeeting
                postToServer(postGroupMeetings, token, params, INITIATOR_SCHEDULE_MEETING, HTTP_POST, ctx, this);


    }


    private boolean validateFormFields() {
        boolean valid = true;
        _meeting_name=etMeetingName.getText().toString();
        _venue=etMeetingVenue.getText().toString();
        _description=etMeetingDescription.getText().toString();
        if (_meeting_name.isEmpty()) {
            etMeetingName.setError(getString(R.string.emptyField));
            valid = false;
        }
        if (_venue.isEmpty()) {
            etMeetingVenue.setError(getString(R.string.emptyField));
            valid = false;
        }
        if (_description.isEmpty()) {
            etMeetingDescription.setError(getString(R.string.emptyField));
            valid = false;
        }
        return valid;
    }

    private void pickTime(EditText view) {

        new TimePickerDialog(ctx, new TimePickerDialog.OnTimeSetListener() {

            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {

                String time = hourOfDay + ":" + minute;

                try {
                    final SimpleDateFormat sdf = new SimpleDateFormat("H:mm");
                    final Date dateObj = sdf.parse(time);
                    //meeting_time = new SimpleDateFormat("hh:mm aa").format(dateObj);

                    meeting_time = new SimpleDateFormat("hh:mm").format(dateObj);
                    etMeetingTime.setText(meeting_time);

                    Toast.makeText(ctx, meeting_time, Toast.LENGTH_LONG).show();

                } catch (final ParseException e) {
                    e.printStackTrace();
                }

            }
        }, 0, 0, true).show();

    }

    private Date pickDate(EditText view) {
        final Date[] date = new Date[1];
        Calendar c = Calendar.getInstance();
        int mYear = Year.now().getValue();
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);
        new DatePickerDialog(getContext(),
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker,
                                          int year,
                                          int monthOfYear,
                                          int dayOfMonth) {
                        String string = String.valueOf(year) + "/" + String.valueOf(monthOfYear + 1) + "/" + String.valueOf(dayOfMonth);

                        try {
                            date[0] = new SimpleDateFormat("dd/MM/yyyy").parse(string);
                            final Calendar datePickerCal = Calendar.getInstance();
                            datePickerCal.set(year, monthOfYear + 1, dayOfMonth);
                            meeting_date = new SimpleDateFormat("MM/dd/yyyy", Locale.ENGLISH).format(datePickerCal.getTime());
                            view.setText(meeting_date);
//                            dob = date[0];
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                    }
                }, mYear, mMonth, mDay)
                .show();
        return date[0];
    }

    @Override
    public void onDataReady(String initiator, String response) {
        switch (initiator){
            case INITIATOR_SCHEDULE_MEETING:
                try {
                    runOnUI(new Runnable() {
                        @Override
                        public void run() {
                            Log.e("SCHEDULE_GROUP_MEETING", "Response is " + response);
                            progressBar.setVisibility(View.GONE);
                            JSONObject jObj = null;
                            try {
                                jObj = new JSONObject(response);
                                if (jObj.has("errors")) {
                                    String errors = jObj.getString("errors");
                                    showSweetAlertDialogError(errors, ctx);
                                } else { // if no error message was found in the response,
                                    boolean success = jObj.getBoolean("success");
                                    if (success) {
                                        String api_message = jObj.getString("message");
                                        showSweetAlertDialogSuccess(api_message,ctx);
                                        NavHostFragment.findNavController(FragmentCreateMeeting.this).navigateUp();

                                    } else {
                                        String api_error_message = jObj.getString("message");
                                        showSweetAlertDialogError(api_error_message, ctx);
                                    }
                                }

                            } catch (JSONException e) {
                                Log.e("Exception-DATA", e.toString());
                                showSweetAlertDialogError(global_error_message, ctx);
                                e.printStackTrace();
                            }
                        }
                    });
                } catch (Exception e) {
                    progressBar.setVisibility(View.GONE);
                    Log.e("Exception_+GROUP_MEMBER", e.toString());
                    showSweetAlertDialogError(global_error_message, ctx);
//                    Toast.makeText(ctx, "Login Data Response Exception" + e.toString(), Toast.LENGTH_LONG).show();
                }
                break;
        }

    }

    @Override
    public void onDataLoaded(String initiator, JSONArray data) {

    }

    @Override
    public void onNetworkError(String initiator, String errorCode, String errorMessage) {
        switch (initiator){
            case INITIATOR_SCHEDULE_MEETING:
                try{
                    runOnUI(new Runnable() {
                        @Override
                        public void run() {
                            progressBar.setVisibility(View.GONE);
                            showSweetAlertDialogError(errorMessage, ctx);
                        }
                    });
                }catch (Exception e){
                    progressBar.setVisibility(View.GONE);
                    showSweetAlertDialogError(global_error_message, ctx);
                    Log.e("Exception- HOME", e.toString());
//                    Toast.makeText(ctx, "Login Error Response Exception" + e.toString(), Toast.LENGTH_LONG).show();
                }

                break;
        }

    }

}
