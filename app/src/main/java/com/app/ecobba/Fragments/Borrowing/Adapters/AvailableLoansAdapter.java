package com.app.ecobba.Fragments.Borrowing.Adapters;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;

import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.RecyclerView;


import com.app.ecobba.Models.AvailableLoans.Package;
import com.app.ecobba.R;

import java.util.List;
import java.util.Random;



public class AvailableLoansAdapter extends RecyclerView.Adapter<AvailableLoansAdapter.ViewHolder> {
    public List<Package> dataList;
    public Fragment fragment;
    public Context context;

    public AvailableLoansAdapter(List<Package> dataList, Fragment fragment, Context context) {
        this.dataList = dataList;
        this.fragment = fragment;
        this.context = context;
    }

    @NonNull
    @Override
    public AvailableLoansAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.list_item_loanpackage_view, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AvailableLoansAdapter.ViewHolder holder, int position) {
        Package loans = dataList.get(position);
        String loan_name = loans.getName();
        int id = dataList.get(position).getId();

        String min_amount = String.valueOf(dataList.get(position).getMin_amount());
        String max_amount = String.valueOf(dataList.get(position).getMax_amount());
        String interest_pa = String.valueOf(dataList.get(position).getInterest_rate());
        String insured_status = String.valueOf(dataList.get(position).getStatus());
        String repayment_plan = dataList.get(position).getRepayment_plan();
        String currency = dataList.get(position).getPackage_currency().getPrefix();

        holder.tv_loan_name.setText(loan_name);
        String loanNameChar = loan_name;
        String sCharloanName = loanNameChar.substring(0, 1);
        Log.e("LOAN_NAME_IS :", sCharloanName);
        holder.tvFirstName.setText(sCharloanName.toUpperCase());
        Random r = new Random();
        int red = r.nextInt(255 - 0 + 1) + 0;
        int green = r.nextInt(255 - 0 + 1) + 0;
        int blue = r.nextInt(255 - 0 + 1) + 0;

        GradientDrawable draw = new GradientDrawable();
        draw.setShape(GradientDrawable.OVAL);
        draw.setColor(Color.rgb(red, green, blue));
        holder.tvFirstName.setBackground(draw);

        if (repayment_plan.equals("3")) {
            holder.repaymentPlan.setText(context.getResources().getString(R.string.repay_plan) + ": " + "Monthly");
        } else if (repayment_plan.equals("2")) {
            holder.repaymentPlan.setText(context.getResources().getString(R.string.repay_plan) + ": " + "Bi-Weekly");
        } else if (repayment_plan.equals("4")) {
            holder.repaymentPlan.setText(context.getResources().getString(R.string.repay_plan) + ": " + "Anually");
        } else if (repayment_plan.equals("1")) {
            holder.repaymentPlan.setText(context.getResources().getString(R.string.repay_plan) + ": " + "Weekly");
        } else if (repayment_plan.equals("weekly")) {
            holder.repaymentPlan.setText(context.getResources().getString(R.string.repay_plan) + ": " + "Weekly");
        } else if (repayment_plan.equals("monthly")) {
            holder.repaymentPlan.setText(context.getResources().getString(R.string.repay_plan) + ": " + "Monthly");
        }


        holder.LLloan.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onClick(View v) {
                Log.e("GOTO_APLLY", loan_name);

                Bundle args = new Bundle();
                args.putString("id", String.valueOf(id));
                args.putString("loan_name", loan_name);
                args.putString("min_amount", min_amount);
                args.putString("max_amount", max_amount);
                args.putString("interest_pa", interest_pa);
                args.putString("insured_status", insured_status);
                args.putString("repayment_plan", repayment_plan);
                args.putString("currency", currency);

                NavHostFragment.findNavController(fragment).navigate(R.id.applyLoanFragment, args);
            }
        });

    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_loan_name, repaymentPlan, tvFirstName;

        LinearLayout LLloan;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            tv_loan_name = itemView.findViewById(R.id.tvLoanName);
            repaymentPlan = itemView.findViewById(R.id.tvRepayPlan);
            LLloan = itemView.findViewById(R.id.llContact);
            tvFirstName = itemView.findViewById(R.id.tvFirstName);

        }
    }

}
