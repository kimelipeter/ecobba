package com.app.ecobba.Fragments.Profile;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.app.ecobba.R;
import com.app.ecobba.ViewModel.UserProfileViewModel;

import java.util.List;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;

public class AccountProfile extends Fragment {

    @BindView(R.id.etConfirm)
    Button complete;
    @BindViews({R.id.etPassword,R.id.etConfirmPassword})
    List<EditText> inputs;

    Boolean created = false,populate = false;
    String password="",confirm="";
    Context ctx;
    UserProfileViewModel userProfileViewModel;

    public AccountProfile(Context ctx){
        this.ctx = ctx;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View frag = inflater.inflate(R.layout.fragment_registration_password,container,false);
        ButterKnife.bind(this,frag);
        complete.setVisibility(View.GONE);
        return frag;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        created = true;
        init();
        getAccountData();
    }

    public void getAccountData(){
        userProfileViewModel = new ViewModelProvider(getActivity()).get(UserProfileViewModel.class);

        userProfileViewModel.getUserProfile();

        userProfileViewModel.userProfileResponseLiveData().observe(getViewLifecycleOwner(), userProfileResponse -> {
//            setData(userProfileResponse.getUser());
        });
    }

    public void init(){
        if (created){

        }
    }

    public String getData()throws NullPointerException{
        if (created) {
            try {
                String data = ", \"password\": " + '"' + inputs.get(0).getText().toString() + '"' +
                        ", \"password_confirmation\": " + '"' + inputs.get(1).getText().toString() + '"';
                return data;
            }catch (Exception e){
                e.printStackTrace();
                return "";
            }

        }else{
            return "";
        }
    }

}
