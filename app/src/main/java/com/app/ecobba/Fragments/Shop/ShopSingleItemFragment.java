package com.app.ecobba.Fragments.Shop;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.SearchView;
import android.widget.TextView;

import com.app.ecobba.Fragments.Shop.MyKweaProducts.CreateNewItemActivity;
import com.app.ecobba.Models.SingleShopItems.SingleShoItemsResponse;
import com.app.ecobba.common.rxApi.Api;
import com.app.ecobba.common.rxApi.RxApi;
import com.bumptech.glide.request.RequestOptions;
import com.app.ecobba.Models.KweaItems.Item;
import com.app.ecobba.R;
import com.app.ecobba.Utils.BaseFragment;
import com.app.ecobba.recyclerview_adapters.SingleShopAdapter;
import com.github.ybq.android.spinkit.SpinKitView;
import com.github.ybq.android.spinkit.sprite.Sprite;
import com.github.ybq.android.spinkit.style.ChasingDots;
import com.github.ybq.android.spinkit.style.Circle;
import com.github.ybq.android.spinkit.style.DoubleBounce;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.app.ecobba.common.SharedMethods.HTTP_GET;
import static com.app.ecobba.common.SharedMethods.getFromServer;
import static com.app.ecobba.common.SharedMethods.showSweetAlertDialogError;
import static com.app.ecobba.common.SharedMethods.showSweetAlertDialogSuccess;

public class ShopSingleItemFragment extends BaseFragment {
    private RecyclerView rvShops;
    private SearchView searchView;
    private Api api;
    private RecyclerView.ViewHolder viewHolder;
    private SingleShopAdapter adapter;
    private List<Item> mShops;
    RequestOptions options;
    private Context mContext;
    Intent intent;
    @BindView(R.id.tvNoAvailableData)
    TextView tvNoAvailableData;
//    @BindView(R.id.progressBar)
//    ProgressBar progressBar;
//    @BindView(R.id.spin_kit)
//    ProgressBar progressBar;

    NavController navHostFragment;
    FloatingActionButton fabAdd;
    SwipeRefreshLayout mSwipeRefreshLayout;

    Bundle arguments;
    String shop_id_number, shop_name;
    ShopSingleItemFragment ctx = this;
    private static final String TAG = "KWEA_SHOP_ITEMS";
    SpinKitView spinKitView;

    public ShopSingleItemFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_shop_single_item, container, false);
        mSwipeRefreshLayout = view.findViewById(R.id.swipe_container);
        shop_id_number = getArguments().getString("shop_id_number");
        shop_name = getArguments().getString("name");

        api = new RxApi().getApi();
        Log.e("shop_id_number_hhhhhh", shop_id_number + shop_name);
        mContext = requireContext();
        TextView textViewShop_name = view.findViewById(R.id.tVshop_name);
        textViewShop_name.setText(shop_name);
        rvShops = (RecyclerView) view.findViewById(R.id.recyclerViewKweaShopList);
        rvShops.addItemDecoration(new DividerItemDecoration(getContext(),
                DividerItemDecoration.VERTICAL));
        adapter = new SingleShopAdapter(new ArrayList<>(), this, requireContext());
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(requireContext());
        rvShops.setLayoutManager(layoutManager);
        rvShops.setAdapter(adapter);

         spinKitView = (SpinKitView) view.findViewById(R.id.spin_kit);
        Circle circle = new Circle();
        spinKitView.setIndeterminateDrawable(circle);

        return view;

    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        fabAdd = view.findViewById(R.id.fabAdd);
        fabAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(requireContext(), CreateNewItemActivity.class);
                intent.putExtra("shop_id", shop_id_number);
                startActivity(intent);
            }
        });

        mContext = getActivity();
        ButterKnife.bind(this, view);

        fetchsingleitem(shop_id_number);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // Toast.makeText(ctx,"Refreshing", Toast.LENGTH_LONG).show();
                loadRecyclerViewData();
                mSwipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary,
                        android.R.color.holo_green_dark,
                        android.R.color.holo_orange_dark,
                        android.R.color.holo_blue_dark);

                mSwipeRefreshLayout.setRefreshing(false);
            }
        });

    }

    private void loadRecyclerViewData() {
        fetchsingleitem(shop_id_number);
    }

    private void fetchsingleitem(String shop_id_number) {
        spinKitView.setVisibility(View.VISIBLE);
        Call<SingleShoItemsResponse> call = api.fetchsingleshop(shop_id_number);
        call.enqueue(new Callback<SingleShoItemsResponse>() {
            @Override
            public void onResponse(Call<SingleShoItemsResponse> call, Response<SingleShoItemsResponse> response) {
               try {
                   if (response.body().getSuccess()) {
                       spinKitView.setVisibility(View.GONE);
                       //  showSweetAlertDialogSuccess("Great job!",getActivity());
                       if (response.body().getData().getItems().size() > 0) {
                           adapter.datalist = response.body().getData().getItems();
                           adapter.notifyDataSetChanged();
                       } else {
                           tvNoAvailableData.setText(getResources().getString(R.string.no_available_data));
                           tvNoAvailableData.setVisibility(View.VISIBLE);
                       }
                   }
                   else if (response.code()==500){
                       spinKitView.setVisibility(View.GONE);
                       showSweetAlertDialogError("Server error",getActivity());
                   }
               } catch (Exception e) {
                   e.printStackTrace();
               }
            }

            @Override
            public void onFailure(Call<SingleShoItemsResponse> call, Throwable t) {
                Log.e(TAG, "onFailure: "+t.getLocalizedMessage());
            }
        });


    }


}