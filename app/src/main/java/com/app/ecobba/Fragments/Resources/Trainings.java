package com.app.ecobba.Fragments.Resources;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import com.app.ecobba.Fragments.Resources.Adapters.TrainingsAdapter;
import com.app.ecobba.Models.Resources.Trainings.TrainingResponse;
import com.app.ecobba.R;
import com.app.ecobba.common.SharedMethods;
import com.app.ecobba.common.rxApi.Api;
import com.app.ecobba.common.rxApi.RxApi;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import java.util.ArrayList;


import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;




public class Trainings extends Fragment implements View.OnClickListener {
    @BindView(R.id.create)
    FloatingActionButton create;
    Context context;
    Fragment fragment;
    String token;
    TrainingsAdapter adapter;
    private static final String TAG = "trainings_data";
    private RecyclerView rvServices;
    SwipeRefreshLayout mSwipeRefreshLayout;
    long datepower;
    private Api api;
    ProgressDialog progressDialog;
    @BindView(R.id.tvNoAvailableData)
    TextView tvNoAvailableData;

    public Trainings() {
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_trainings, container, false);

        rvServices = (RecyclerView) view.findViewById(R.id.recyclerviewid);
        mSwipeRefreshLayout = view.findViewById(R.id.swipe_container);
        adapter = new TrainingsAdapter(new ArrayList<>(), requireContext(), fragment);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(requireContext());
        rvServices.setLayoutManager(layoutManager);
        rvServices.setAdapter(adapter);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                Toast.makeText(context, "Refreshing", Toast.LENGTH_LONG).show();
                loadRecyclerViewData();
                mSwipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary,
                        android.R.color.holo_green_dark,
                        android.R.color.holo_orange_dark,
                        android.R.color.holo_blue_dark);

                mSwipeRefreshLayout.setRefreshing(false);
            }
        });
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        context = getActivity();
        token = SharedMethods.getDefaults("token", context);
        api = new RxApi().getApi();
        ButterKnife.bind(this, view);
        progressDialog = new ProgressDialog(context);
        create.setOnClickListener(this);
        loadRecyclerViewData();

    }

    private void loadRecyclerViewData() {
        progressDialog.setMessage("Loading.Please wait....");
        progressDialog.show();
        Call<TrainingResponse> call = api.getTrainingResources();
        call.enqueue(new Callback<TrainingResponse>() {
            @Override
            public void onResponse(Call<TrainingResponse> call, Response<TrainingResponse> response) {
                try {
                    if (response.body().getSuccess()) {
                        progressDialog.dismiss();
                        if (response.body().getData().size() > 0) {
                            adapter.dataList = response.body().getData();
                            adapter.notifyDataSetChanged();
                        } else {
                            tvNoAvailableData.setText(getResources().getString(R.string.no_available_data));
                            tvNoAvailableData.setVisibility(View.VISIBLE);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<TrainingResponse> call, Throwable t) {

            }
        });

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.create:
                NavHostFragment.findNavController(this).navigate(R.id.addResources);
        }

    }

    @Override
    public void onResume() {
        super.onResume();
        loadRecyclerViewData();
    }

}


