package com.app.ecobba.Fragments.Group.ViewMoreFragments;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.ecobba.Models.Group_Transaction.groupTransactionResponse;
import com.app.ecobba.Models.UserProfile.Role;
import com.app.ecobba.Models.UserProfile.UserProfileResponse;
import com.app.ecobba.R;
import com.app.ecobba.common.SharedMethods;
import com.app.ecobba.common.rxApi.Api;
import com.app.ecobba.common.rxApi.RxApi;
import com.app.ecobba.recyclerview_adapters.GroupTransactionsListRecycler;
import com.app.ecobba.recyclerview_models.GroupTransactionsAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.app.ecobba.common.ApisKtKt.getGroupTransactions;
import static com.app.ecobba.common.AppConstantsKt.INITIATOR_GET_GROUP_TRANSACTIONS;
import static com.app.ecobba.common.SharedMethods.HTTP_GET;
import static com.app.ecobba.common.SharedMethods.getFromServer;
import static com.app.ecobba.common.SharedMethods.global_error_message;
import static com.app.ecobba.common.SharedMethods.runOnUI;
import static com.app.ecobba.common.SharedMethods.showSweetAlertDialogError;

public class ViewMoreTransactions extends Fragment implements View.OnClickListener {

    @BindView(R.id.recyclerViewTrans)
    RecyclerView recyclerViewTrans;
    @BindView(R.id.tvGroupName)
    TextView tvGroupName;
    @BindView(R.id.tvNoAvailableDataTrans)
    TextView getTvNoAvailableDataTrans;
    String group_id, group_name;
    Context context;
    Bundle args;
    private Api api;
    String currencyValue;
    ViewMoreTransactionAdapter adapter;
    Fragment fragment;
    int numOfItems;
    @BindView(R.id.progress_dialog)
    ProgressBar progress_dialog;

    public ViewMoreTransactions() {

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.view_more_grouptransactions, container, false);
        recyclerViewTrans = view.findViewById(R.id.recyclerViewTrans);
        recyclerViewTrans.addItemDecoration(new DividerItemDecoration(getContext(),
                DividerItemDecoration.VERTICAL));
        adapter = new ViewMoreTransactionAdapter(new ArrayList<>(), requireContext(), fragment, numOfItems);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(requireContext());
        recyclerViewTrans.setLayoutManager(layoutManager);
        recyclerViewTrans.setAdapter(adapter);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        context = getActivity();
        ButterKnife.bind(this, view);
        api = new RxApi().getApi();
        getUserProfile();
        args = getArguments();
        assert args != null;
        group_id = args.getString("group_id");
        group_name = args.getString("group_name");
        tvGroupName.setText(group_name);
        fetchGroupTransactions(group_id);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

        }
    }

    private void getUserProfile() {
        Call<UserProfileResponse> call = api.getUserProfile();
        call.enqueue(new Callback<UserProfileResponse>() {
            @Override
            public void onResponse(Call<UserProfileResponse> call, Response<UserProfileResponse> response) {
                try {
                    if (response.body().getSuccess()) {
                        List<Role> roles = response.body().getUser().getRoles();
                        currencyValue = response.body().getUser().getWallet().getCurrency().getPrefix();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<UserProfileResponse> call, Throwable t) {


            }
        });
    }


    private void fetchGroupTransactions( String group_id) {
        progress_dialog.setVisibility(View.VISIBLE);
        Call<groupTransactionResponse> call = api.getGroupTransactions(group_id);
        call.enqueue(new Callback<groupTransactionResponse>() {
            @Override
            public void onResponse(Call<groupTransactionResponse> call, Response<groupTransactionResponse> response) {
                Log.e("DATA_LOG_RES","DATA IS" +response);
                try {
                    if (response.body().getSuccess()) {
                        progress_dialog.setVisibility(View.GONE);
                        if (response.body().getData().getAll_transactions().size() > 0) {
                            adapter.datalist = response.body().getData().getAll_transactions();
                            adapter.notifyDataSetChanged();
                        }
                        else {
                            getTvNoAvailableDataTrans.setText(getResources().getString(R.string.no_available_data));
                            getTvNoAvailableDataTrans.setVisibility(View.VISIBLE);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<groupTransactionResponse> call, Throwable t) {

            }
        });
    }


}
