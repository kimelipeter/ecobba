package com.app.ecobba.Fragments.Profile;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;

import com.app.ecobba.Fragments.Profile.viewmodel.ProfileViewModel;
import com.app.ecobba.Models.LevelOne;
import com.app.ecobba.Models.RegionDetail;
import com.app.ecobba.Models.RegistrationData.Country;
import com.app.ecobba.Models.RegistrationData.ResidentType;
import com.app.ecobba.Models.UserProfile.User;
import com.app.ecobba.R;
import com.app.ecobba.ViewModel.UserProfileViewModel;
import com.app.ecobba.common.SharedMethods;
import com.google.gson.Gson;
import com.jaredrummler.materialspinner.MaterialSpinner;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;

import static com.app.ecobba.common.ApisKtKt.getLevelThreeData;
import static com.app.ecobba.common.ApisKtKt.getLevelTwoData;
import static com.app.ecobba.common.ApisKtKt.getRegistrationData;
import static com.app.ecobba.common.AppConstantsKt.INITIATOR_GET_LEVEL_ONE_DATA;
import static com.app.ecobba.common.AppConstantsKt.INITIATOR_GET_LEVEL_THREE_DATA;
import static com.app.ecobba.common.AppConstantsKt.INITIATOR_GET_LEVEL_TWO_DATA;
import static com.app.ecobba.common.SharedMethods.HTTP_GET;
import static com.app.ecobba.common.SharedMethods.getFromServer;
import static com.app.ecobba.common.SharedMethods.global_error_message;
import static com.app.ecobba.common.SharedMethods.runOnUI;
import static com.app.ecobba.common.SharedMethods.showSweetAlertDialogError;


public class AddressProfileFragment extends Fragment implements SharedMethods.NetworkObjectListener, MaterialSpinner.OnItemSelectedListener {
    @BindView(R.id.msCountry)
    MaterialSpinner msCountry;

    @BindView(R.id.msResidentType)
    MaterialSpinner msResidentType;

//    @BindView(R.id.mkoa)
//    MaterialSpinner mkoa;
//
//    @BindView(R.id.wilaya)
//    MaterialSpinner wilaya;
//
//    @BindView(R.id.kata)
//    MaterialSpinner kata;
//
//    @BindView(R.id.mtaa)
//    TextInputEditText mtaa;


    @BindViews({R.id.etCity,
            R.id.etState,
            R.id.etPostalCode,
            R.id.etAddress
//            R.id.mtaa
    })
    List<EditText> inputs;


    ArrayList<String> resident_type_ids;
    ArrayList<String> countries_drop_down;
    ArrayList<String> countries_id;
    ArrayList<String> resident_types_statuses_drop_down;

    Boolean created = false, init = false, populate = false;
    String country = "", resType = "", city = "", state = "", postal = "", address = "";
    //    _mkoa="",_wilaya="",_kata=""
//            _mtaa="";
    String country_id_toapi;
    //    String country_id_toapi,selectedMkoaValue="0",
//            selectedWilayaValue="0",
//            selectedKataValue="0";
    private List<String> level_ones_ids, level_two_ids, level_three_ids;
    RegionDetail region;

    int country_pos = 0, resident_type_pos = 0;
    ProfileViewModel profileViewModel;
    Context ctx;
    String token;

    UserProfileViewModel userProfileViewModel;

    public AddressProfileFragment(Context ctx) {
        this.ctx = ctx;
        resident_type_ids = new ArrayList<>();
        countries_id = new ArrayList<>();
        countries_drop_down = new ArrayList<String>();
        resident_types_statuses_drop_down = new ArrayList<String>();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View frag = inflater.inflate(R.layout.fragment_registration_address, container, false);
        ButterKnife.bind(this, frag);
        Log.e("EDIT_CREATED", "ADDRESS PROFILE");
        created = true;
        token = SharedMethods.getDefaults("token", ctx);
        msCountry.setOnItemSelectedListener(this);

        userProfileViewModel = new ViewModelProvider(getActivity()).get(UserProfileViewModel.class);

        return frag;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getAddressData();
        setCountry();
        setResidency();
    }

    public void getAddressData() {
        userProfileViewModel.getUserProfile();

        userProfileViewModel.userProfileResponseLiveData().observe(getViewLifecycleOwner(), userProfileResponse -> {
          try {
              setData(userProfileResponse.getUser());
          } catch (Exception e) {
              e.printStackTrace();
          }

        });

    }

    public void init(User data) {
        resType = data.getDetail() != null ? String.valueOf(data.getDetail().getResidence()) : "";
        country = data.getDetail() != null ? String.valueOf(data.getDetail().getCountry()) : "";
        Log.e("GET_COUNTRY",country);
        Log.e("GET_RESIDENCE_TYPE",resType);

        if (resident_type_ids.contains(resType)) {
            resident_type_pos = resident_type_ids.indexOf(resType);
            msResidentType.setSelectedIndex(resident_type_pos);

        }

        if (countries_id.contains(country)) {
            country_pos = countries_id.indexOf(country);
            msCountry.setSelectedIndex(country_pos);
        }

        inputs.get(0).setText(city);
        inputs.get(1).setText(state);
        inputs.get(2).setText(postal);
        inputs.get(3).setText(address);
    }

    public void setCountry() {
        userProfileViewModel.getUserCountry();

        userProfileViewModel.userCountryResponseLiveData().observe(getViewLifecycleOwner(), countries -> {
            try {
                setCountryData(countries);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        });
    }

    public void setResidency() {
        userProfileViewModel.getUserResidentType();

        userProfileViewModel.userResidentResponseLiveData().observe(getViewLifecycleOwner(), residents -> {
            try {
                setResidentData(residents);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        });

    }

    public void setCountryData(List<Country> data) throws JSONException {
        countries_drop_down.add(ctx.getResources().getString(R.string.please_select_country));
        countries_id.add("0");
        if (data.size() > 0) {
            for (int i = 0; i < data.size(); i++) {
                countries_drop_down.add(data.get(i).getName());
                countries_id.add(String.valueOf(data.get(i).getId()));
                msCountry.setItems(countries_drop_down);
            }
        }

    }

    public void setResidentData(List<ResidentType> data) throws JSONException {
        resident_types_statuses_drop_down.add(ctx.getResources().getString(R.string.please_select_typeof_residency));
        resident_type_ids.add("0");
        if (data.size() > 0) {
            for (int i = 0; i < data.size(); i++) {
                resident_types_statuses_drop_down.add(data.get(i).getName());
                resident_type_ids.add(String.valueOf(data.get(i).getId()));
                msResidentType.setItems(resident_types_statuses_drop_down);
            }
        }

    }

    public void setData(User data) {
        city = data.getDetail().getCity();

        state = data.getDetail().getState();

        postal = data.getDetail().getPostal_code();

        address = data.getDetail().getAddress();

        resType = String.valueOf(data.getDetail().getResidence());

        country = String.valueOf(data.getDetail().getCountry());


        init(data);

    }

    public String getData() {
        if (created) {
            String data = ", \"country\": " + '"' + countries_id.get(msCountry.getSelectedIndex()) + '"' +
                    ", \"residence\": " + '"' + resident_type_ids.get(msResidentType.getSelectedIndex()) + '"' +
                    ", \"city\": " + '"' + inputs.get(0).getText().toString() + '"' +
                    ", \"state\": " + '"' + inputs.get(1).getText().toString() + '"' +
//                    ", \"level_one\": " + '"' + level_ones_ids.get(mkoa.getSelectedIndex()) + '"' +
//                    ", \"level_two\": " + '"' + level_two_ids.get(wilaya.getSelectedIndex()) + '"' +
//                    ", \"level_three\": " + '"' + level_three_ids.get(kata.getSelectedIndex()) + '"' +
//                    ", \"mtaa\": " + '"' + inputs.get(4).getText().toString() + '"' +
                    ", \"postal_code\": " + '"' + inputs.get(2).getText().toString() + '"' +
                    ", \"address\": " + '"' + inputs.get(3).getText().toString() + '"';


            return data;
        } else {
            return "";
        }
    }

    private void setLocations(String country_id) {
        getMkoaData(country_id);

    }

    private void getMkoaData(String countr_id) {
        Map<String, String> params = new HashMap<String, String>();
        params.put("token", token);
        getFromServer(getRegistrationData + "/" + countr_id, token, params, INITIATOR_GET_LEVEL_ONE_DATA, HTTP_GET, ctx, this);

    }

    private void getWilayaData(String mkoa_id) {
        Map<String, String> params = new HashMap<String, String>();
        params.put("token", token);
        getFromServer(getLevelTwoData + "/" + mkoa_id, token, params, INITIATOR_GET_LEVEL_TWO_DATA, HTTP_GET, ctx, this);
    }

    private void getKataData(String id) {
        Map<String, String> params = new HashMap<String, String>();
        params.put("token", token);
        getFromServer(getLevelThreeData + "/" + id, token, params, INITIATOR_GET_LEVEL_THREE_DATA, HTTP_GET, ctx, this);
    }

    @Override
    public void onDataReady(String initiator, String response) {
        switch (initiator) {
            case INITIATOR_GET_LEVEL_ONE_DATA:
                try {
                    runOnUI(() -> {
                        Log.e(initiator, "Response is " + response);
                        JSONObject jObj = null;
                        try {
                            jObj = new JSONObject(response);
                            if (jObj.has("errors")) {
                                String errors = jObj.getString("errors");
                                showSweetAlertDialogError(errors, ctx);
                            } else {
                                boolean success = jObj.getBoolean("success");
                                if (success) {

                                    if (jObj.has("data") && !jObj.isNull("data")) {
                                        level_ones_ids = new ArrayList<>();
                                        level_ones_ids.add("0");
                                        List<String> mkoaString = new ArrayList<>();
                                        mkoaString.add(getString(R.string.please_select_mkoa));
                                        region = new Gson().fromJson(jObj.getJSONObject("data").toString(), RegionDetail.class);
                                        for (LevelOne levelOne : region.getLevel_ones()) {
                                            level_ones_ids.add(levelOne.getId());
                                            mkoaString.add(levelOne.getName());
                                        }

//                                        int position = level_ones_ids.indexOf(_mkoa);
//
//                                        mkoa.setItems(mkoaString);
//                                        if (position>=0 && position<mkoaString.size()) {
//                                            mkoa.setSelectedIndex(position);
//                                        }

                                    }

                                } else {
                                    String api_error_message = jObj.getString("message");
                                    showSweetAlertDialogError(api_error_message, ctx);
                                }
                            }
                        } catch (JSONException e) {
                            Log.e("Exception-DATA", e.toString());
                            showSweetAlertDialogError(global_error_message, ctx);
                            e.printStackTrace();
                        }
                    });
                } catch (Exception e) {
                    Log.e(initiator, e.toString());
                    showSweetAlertDialogError(global_error_message, ctx);
                }
                break;
            case INITIATOR_GET_LEVEL_TWO_DATA:
                try {
                    runOnUI(new Runnable() {
                        @Override
                        public void run() {
                            Log.e("CREATE_GROUP_MEMBER", "Response is " + response);
                            JSONObject jObj = null;
                            try {
                                jObj = new JSONObject(response);
                                if (jObj.has("errors")) {
                                    String errors = jObj.getString("errors");
                                    showSweetAlertDialogError(errors, ctx);
                                } else { // if no error message was found in the response,
                                    boolean success = jObj.getBoolean("success");
                                    if (success) {
                                        level_two_ids = new ArrayList<>();
//                                        JSONObject level_two = jObj.getJSONObject("data");
                                        JSONArray level_two = jObj.getJSONArray("data");
                                        ArrayList<String> level_two_drop_down = new ArrayList<String>();
                                        level_two_drop_down.add(getResources().getString(R.string.please_select_wilaya));
                                        level_two_ids.add("0");
                                        if (level_two.length() > 0) {
                                            for (int i = 0; i < level_two.length(); i++) {
                                                Log.e("FINAL DATA IS : ", level_two.toString());
                                                JSONObject jsonObject = level_two.getJSONObject(i);
                                                String id = jsonObject.getString("id");
                                                String name = jsonObject.getString("name");
                                                level_two_drop_down.add(name);
                                                level_two_ids.add(id);
                                            }
//                                            int position = level_two_ids.indexOf(_wilaya);
//
//                                            wilaya.setItems(level_two_drop_down);
//                                            if (position>=0 && position<level_two_drop_down.size()) {
//                                                wilaya.setSelectedIndex(position);
//                                            }
                                        }
                                    } else {
                                        String api_error_message = jObj.getString("message");
                                        showSweetAlertDialogError(api_error_message, ctx);
                                    }
                                }

                            } catch (JSONException e) {
                                Log.e("Exception-DATA", e.toString());
                                showSweetAlertDialogError(global_error_message, ctx);
                                e.printStackTrace();
                            }
                        }
                    });
                } catch (Exception e) {
                    Log.e("Exception_+GROUP_MEMBER", e.toString());
                    showSweetAlertDialogError(global_error_message, ctx);
                }
                break;
            case INITIATOR_GET_LEVEL_THREE_DATA:
                try {
                    runOnUI(new Runnable() {
                        @Override
                        public void run() {
                            Log.e("CREATE_GROUP_MEMBER", "Response is " + response);
//                            hideDialog(ctx);
                            JSONObject jObj = null;
                            try {
                                jObj = new JSONObject(response);
                                if (jObj.has("errors")) {
                                    String errors = jObj.getString("errors");
                                    showSweetAlertDialogError(errors, ctx);
                                } else { // if no error message was found in the response,
                                    boolean success = jObj.getBoolean("success");
                                    if (success) {
                                        level_three_ids = new ArrayList<>();
//                                        JSONObject level_two = jObj.getJSONObject("data");
                                        JSONArray level_two = jObj.getJSONArray("data");
                                        ArrayList<String> level_three_drop_down = new ArrayList<String>();
                                        level_three_drop_down.add(getResources().getString(R.string.please_select_kata));
                                        level_three_ids.add("0");
                                        if (level_two.length() > 0) {
                                            for (int i = 0; i < level_two.length(); i++) {
                                                Log.e("FINAL DATA IS : ", level_two.toString());
                                                JSONObject jsonObject = level_two.getJSONObject(i);
                                                String id = jsonObject.getString("id");
                                                String name = jsonObject.getString("name");
                                                level_three_drop_down.add(name);
                                                level_three_ids.add(id);
                                            }
//                                            int position = level_three_ids.indexOf(_kata);
//                                            kata.setItems(level_three_drop_down);
//                                            if (position>=0 && position<level_three_drop_down.size()) {
//                                                kata.setSelectedIndex(position);
//                                            }
                                        }
                                    } else {
                                        String api_error_message = jObj.getString("message");
                                        showSweetAlertDialogError(api_error_message, ctx);
                                    }
                                }

                            } catch (JSONException e) {
                                Log.e("Exception-DATA", e.toString());
                                showSweetAlertDialogError(global_error_message, ctx);
                                e.printStackTrace();
                            }
                        }
                    });
                } catch (Exception e) {
//                    hideDialog(ctx);
                    Log.e("Exception_+GROUP_MEMBER", e.toString());
                    showSweetAlertDialogError(global_error_message, ctx);
//                    Toast.makeText(ctx, "Login Data Response Exception" + e.toString(), Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

    @Override
    public void onDataLoaded(String initiator, JSONArray data) {

    }

    @Override
    public void onNetworkError(String initiator, String errorCode, String errorMessage) {

    }

    @Override
    public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
        switch (view.getId()) {
            case R.id.msCountry:
                country_id_toapi = countries_id.get(position);
                getMkoaData(country_id_toapi);
                break;
//            case R.id.mkoa:
////                selectedMkoaValue  = mkoa.getSelectedIndex();
//                selectedMkoaValue= level_ones_ids.get(position);
//                getWilayaData(level_ones_ids.get(position));
//                Log.d("MKOA Status id:",""+level_ones_ids.get(position) );
//                break;
//            case R.id.wilaya:
////                selectedWilayaValue  = wilaya.getSelectedIndex();
//                selectedWilayaValue= level_two_ids.get(position);
//                getKataData(level_two_ids.get(position));
//                Log.d("WILAYA Status id:",""+ level_two_ids.get(position) );
//                break;
//            case R.id.kata:
////                selectedKataValue  = kata.getSelectedIndex();
//                selectedKataValue= level_three_ids.get(position);
//                Log.d("KATA Status id:",""+ level_three_ids.get(position) );
//                break;
        }
    }
}